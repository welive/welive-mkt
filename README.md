# README #

This project is a Liferay 6.2 portlet

# What is this repository for? #

WeLive Marketplace (MKT)

## Requirements  
| Framework                                                                        |Version  |Licence             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip/download) |6.2 GA4 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |

## Libraries

MKT is based on the following software libraries and frameworks.

|Framework                           |Version       |License                                      |
|------------------------------------|--------------|---------------------------------------------|
|commons httpclient| 3.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|httpclient| 4.2.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|httpcore| 4.2.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|jena-arq| 2.11.0 | [The JSON License](https://www.json.org/license.html)|
|jena-core| 2.11.0 | [The JSON License](https://www.json.org/license.html)|
|[Jena IRI](http://jena.sf.net/iri) |0.8 |[BSD-style license](http://jena.sourceforge.net/iri/license.html) |
|[jersey-client](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[jersey-core](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[Jersey core common](https://jersey.java.net) |2.1 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[JSTL API](http://jcp.org/en/jsr/detail?id=52) |1.2 |[CDDL+GPL](https://glassfish.dev.java.net/public/CDDL+GPL.html) |
|[jQuery](https://jquery.org/) | 2.1.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Materialize](http://materializecss.com)| 0.97.6 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Material Design Lite](https://github.com/google/material-design-lite)| 1.0.4|[MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |
|openrdf-sesame| 2.7.10-one|[Aduna BSD license](http://repo.aduna-software.org/legal/aduna-bsd.txt) |
|xercesImpl |  2.11.0|[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|xml-apis | |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |

## Dependencies
|Library                             |Project       |License                                      |
|------------------------------------|--------------|---------------------------------------------|
|Challenge62-portlet-service.jar| welive-oia | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|CitizenDataVault-portlet-service.jar| welive-cdv-portlet | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|MetamodelParser-3.11.4.jar| welive-metamodel-parser | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|-| welive-apibodyenabler-hook| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|-| welive-language-liferay-hook| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|-| welive-liferay-theme| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|-| welive-liferay-social-theme| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|

# How do I get set up? #

For details about configuration/installation you can see "D2.4 - WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V2"

# Who do I talk to? #

ENG team
Giovanni Aiello, Antonino Sirchia, Filippo Giuffrida


### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)