/--------README----------/
Stato di avanzamento del wizard:
 	- E' possibile indicare consumes e produces partendo from Scratch (da testare tutte le combinazioni)
 	- La selezione di consumes / produces condiziona il tipo di parametri che è possibile inserire (l'utente viene avvisato che il loro cambiamento comporta la perdita delle info inserite)
 	- Partendo da un descrittore il wizard viene pre-caricato correttamente (testare altre combinazioni di descrittori)
 	- Se lo swagger non è compliant con la specifica dovrebbe essere avvisato l'utente (lato parser) invece viene pre-caricato il form con valori che sarebbe impossibile mettere from scratch:
 		-es. formData e body non possono coesistere secondo la specifica swagger, e di fatto il wizard partendo da 0 non permette la loro co esistenza. Se si crea uno swagger che li prevede
 			 entrambi però questo viene passato al wizard (l'oggetto javascript c'è ma non viene visualizzato correttamente nella UI) . Una possibile soluzione sarebbe mettere a valle del parser un validatore di swagger 
 	- Da testare il download dello swagger dal dettaglio dell'artefatto (probabile perdita di informazioni consumes/produces)
 	-Pubblicazione su test e dev		 