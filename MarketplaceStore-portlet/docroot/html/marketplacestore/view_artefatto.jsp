<%@page import="it.eng.rspa.marketplace.ConfUtil"%>
<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="it.eng.rspa.marketplace.controller.ViewArtefattoController"%>
<%@page import="it.eng.sesame.InteractionPointBean"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.model.Categoria"%>
<%@page import="it.eng.rspa.usdl.model.TermsAndCondition"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="it.eng.rspa.usdl.utils.RDFUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Portlet"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@include file="/html/marketplacestore/include/init.jsp"%>
<%@include file="/html/checkPublicUser.jspf" %>

<% ViewArtefattoController.action(request); %>

<c:choose>
	<c:when test="${! empty redirectNow}">
		<script> location.href="${redirectNow}"; </script>
	</c:when>
	
	<c:when test="${notfound}">
		<%@include file="include/notfound.jspf" %>
	</c:when>
	
	<c:otherwise>
		
		<%	
			String wizardPortletId = ConfUtil.getString("wizardPortletId");
	
			long editor_plid = PortalUtil.getPlidFromPortletId(layout.getGroupId(), layout.isPrivateLayout(), wizardPortletId);
			String editurl = "javascript:void(0);";
			PortletURL wizard_url = PortletURLFactoryUtil.create(renderRequest, wizardPortletId, editor_plid, PortletRequest.RENDER_PHASE);
						wizard_url.setWindowState(WindowState.MAXIMIZED);
						wizard_url.setPortletMode(PortletMode.VIEW);
						wizard_url.setParameter("jspPage", "/html/publicationwizard/view.jsp");
						wizard_url.setParameter("resourcePrimaryKey", (String)request.getAttribute("resourcePrimKey"));
						
			if((Boolean)request.getAttribute("isPSA")){
				wizard_url.setParameter("catsOfType", "psa");
				editurl = wizard_url.toString();
			}
			else if((Boolean)request.getAttribute("isDataset")){
				editurl = "/ods";
			}
			else{
				wizard_url.setParameter("catsOfType", "bblocks");
				editurl = wizard_url.toString();
			}
		
			
			request.setAttribute("bUrl", (String)request.getParameter("bUrl")); //Chiedere a Nino
			if((String)request.getParameter("bUrl") == null) request.setAttribute("bUrl", "/html/marketplacestore/view.jsp");
		%>
	
		<jsp:useBean id="artefatto" scope="request" class="it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoImpl"/>
		
		<%Portlet portlet = PortletLocalServiceUtil.getPortletById(company.getCompanyId(), portletDisplay.getId());%>
		<liferay-util:html-top>
			<link href="<%= PortalUtil.getStaticResourceURL(request, PortalUtil.getPathContext(request) + "/css/marketplace/view_artefatto.css", portlet.getTimestamp()) %>" rel="stylesheet" type="text/css" />
		</liferay-util:html-top>
		
		<portlet:renderURL var="backURL">
			<portlet:param name="jspPage" value="/html/marketplacestore/view.jsp" />
		</portlet:renderURL>
		
		<portlet:renderURL var="portletHomeURL">
			<portlet:param name="jspPage" value="/html/marketplacestore/view.jsp" />
		</portlet:renderURL>
		
		<portlet:actionURL name="deleteArtifact" var="rimuovi_artefatto">
			<portlet:param name="jspPage" value="${bUrl}" />
			<portlet:param name="idArtefatto" value="${resourcePrimKey}" />
		</portlet:actionURL>
		
		<portlet:resourceURL var="resURL"/>
		
		<div class="materialize">
		
			<div id="modal-delete-confirm" class="modal">
				<div class="modal-content">					
					<p><liferay-ui:message key="are-you-sure-you-want-to-delete-this"/></p> 
				</div>
				<div class="modal-footer">
					<a href="${rimuovi_artefatto}" class="modal-action modal-close waves-effect waves-green btn-flat">
						<liferay-ui:message key="delete"/>
					</a>
					<a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-green btn-flat">
						<liferay-ui:message key="cancel"/>
					</a>
				</div>
			</div>
			<!-- <a href="#modal-delete-confirm" class="waves-effect waves-light btn" id="modal-delete-confirm-trigger"></a>  -->
		
			<div class="section row valign-wrapper">
				<div class="col s4 valign left">
<%-- 					<a class="waves-effect btn-flat tooltipped" href="${backURL}" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="back"/>"> --%>
					<a class="waves-effect btn-flat tooltipped" href="javascript:history.back()" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="back"/>"> 
						<i class="icon-arrow-left left"></i>
						<liferay-ui:message key="back"/>
					</a>
				</div>
				<div class="col s4 valign center"></div>
				<div class="col s4 valign right"></div>
			</div>
			
			
			<div class="row">
				
				<div class="col s12 l8">
<!-- Titolo artefatto -->
					<div class="col s12">
						<p class="artefactTitle">
							${artefatto.title}
						</p>
					</div>
<!-- Anagrafe artefatto					 -->
					<div class="row">
						<div class="col s12 m3">
							<img src="${imgURl}"/>
							<br><br>
						</div>
						<div class="col s12 m9">
							<div class="row">
								<p>
									<label class="inlineLbl"><liferay-ui:message key="author"/>:&nbsp;</label>
									
										<c:choose>
										    <c:when test="${isAuthorityOrAdminOrDeletedUser}">
										        <span >
													${authorName}
												</span>
										    </c:when>    
										    <c:otherwise>
										    	<span class="mailTo">
													<a href='/web/${authorScreenName}/so/profile'>${authorName}</a>
												</span>
										    </c:otherwise>
									</c:choose>
									
									
								</p>
								
								<c:if test="${!(empty artefatto.providerName)}">
									<p>
										<label class="inlineLbl">
											<liferay-ui:message key="provided"/>:&nbsp;
										</label>
										<span>${artefatto.providerName}</span>
									</p>
								</c:if>
								
								<c:if test="${!(empty artefatto.owner)}">
									<p>
										<label class="inlineLbl">
											<liferay-ui:message key="owner"/>:&nbsp;
										</label>
										<span>${artefatto.owner}</span>
									</p>
								</c:if>
								
								<p>
									<label class="inlineLbl">
										<liferay-ui:message key="published_in"/>:&nbsp;
									</label>
									<span><fmt:formatDate pattern="dd/MM/yyyy" value="${artefatto.date}" /></span>
								</p>
								
								<p>
									<label class="inlineLbl">
										<liferay-ui:message key="type"/>:&nbsp;
									</label>
									<span>
										<liferay-ui:message key="${categoryName}"/>
									</span>
								</p>
								
								<c:if test="${ isPublished && isDataset }">
									<p>
										<label class="inlineLbl"><liferay-ui:message key="mkt.ods-name"/>:&nbsp;</label>
										<a target="_blank" href="${eid}"><liferay-ui:message key="resource"/> <i class="icon-external-link"></i></a> 
									</p>
								</c:if>
								
								<c:if test="${!isPublished}">
									<p>
										<label class="inlineLbl"><liferay-ui:message key="status"/>:&nbsp;</label>
										<span><liferay-ui:message key='<%=WorkflowConstants.getStatusLabel(artefatto.getStatus()).toLowerCase() %>'/></span>
									</p>
								</c:if>
								<c:if test="${ isPublished && !isPSA && !isDataset }">
									<p>
										<label class="inlineLbl"><liferay-ui:message key="level"/>:&nbsp;</label>
										<a target="_blank" href="/faq/en/user-guide/developer/bb/fivestar-model.html">
											<img class="level_img tooltipped hide" src="" data-contextpath = "<%=request.getContextPath()%>" 
												 data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="level" /> 1"   data-tooltipbase="<liferay-ui:message key="level" />"/>
										</a>
									</p>
								</c:if>
								
<!-- 		DOCUMENTATION LINK 					-->
									<p>
										<label class="inlineLbl"><liferay-ui:message key="mkt.documentation-page"/>:&nbsp;</label>
										<c:choose>
											<c:when test="${empty docPage}">
												None
											</c:when>
											<c:otherwise>
												<a href ="${docPage}" target="_blank">${docPage}</a> 
											</c:otherwise>
										</c:choose>
									</p>
								
											
								
							</div>
						</div>
					</div>
<!-- Tags					 -->
					<div class="row"> 
						<liferay-ui:asset-tags-summary className="<%= Artefatto.class.getName() %>" classPK="${artefatto.artefattoId}" />
					</div>
<!-- Rating					 -->
					<c:if test="${!isDraft}">
						<div class="row">
							<c:choose>
								<c:when test="${isDataset}">
									<div class="col s12">
										<div class="row">
											<c:if test="${userIsLoggedIn}">
												<div id="my-rating" class="col">
													<div class="my-rating-label"><liferay-ui:message key="your-rating"/></div>
													<span class="rating-content template">&nbsp;
														<i class="rating-element core-color-text text-color-1 fa fa-star"></i>
														<i class="rating-element core-color-text text-color-1 fa fa-star-o"></i>
													</span>
													<div class="async-load-spinner hide left-align"><i class="fa fa-spinner fa-spin"></i></div>
												</div>
											</c:if>
											<div id="rating" class="col">
												<div class="rating-label"><liferay-ui:message key="average"/>&nbsp;(&nbsp;<span id="n-votes">0</span>&nbsp;<liferay-ui:message key="votes"/>&nbsp;)</div>
												<span class="rating-content template">&nbsp;
													<i class="rating-element core-color-text text-color-1 fa fa-star"></i>
													<i class="rating-element core-color-text text-color-1 fa fa-star-o"></i>
												</span>
												<div class="async-load-spinner hide left-align"><i class="fa fa-spinner fa-spin"></i></div>
											</div>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<div>
										<liferay-ui:ratings className="<%=Artefatto.class.getName() %>" classPK="${artefatto.artefattoId}" />
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</c:if>
					
					<c:if test="${(categoryName == restname || categoryName == soapname || categoryName == noname) && !(empty artefatto.webpage)}">
						<p>
							<liferay-ui:message key="read-docs"/>
							<c:choose>
								<c:when test="${fn:startsWith(artefatto.webpage, 'http://') || fn:startsWith(artefatto.webpage, 'https://') }">
									<a href="${artefatto.webpage}" target="_blank">
										<liferay-ui:message key="here"/>.
									</a>
								</c:when>
								<c:otherwise>
									<a href="http://${artefatto.webpage}" target="_blank">
										<liferay-ui:message key="here"/>.
									</a>
								</c:otherwise>
							</c:choose>
						</p>
					</c:if>
					
<!-- Buttons Big					 -->
					<div class="row">
						
						<c:if test="${isPublished || isDraft}">
							
							<c:choose>
								
								<c:when test="${categoryName == restname || categoryName == soapname}">
									<c:choose>
										<c:when test="${userIsLoggedIn}">
											<a target="_blank" id="downloadlusdl" class="btn btn-large waves-effect waves-light green lighten-1" href="javascript:void(0);">
												<i class="material-icons left">file_download</i>
												<liferay-ui:message key="download"/>
											</a>
											
											<script>
												$('#downloadlusdl').click(function(){
													
													var self = $(this);
													
													var namespace = jQuery('#portletnamespace').val();
													var input = {};
														input[namespace+'idArtefatto']=${resourcePrimKey} ;
														input[namespace+'action']='preparedescriptor';
													
													$.post('${resURL}', input)
													.done(function(resp){
														var o = JSON.parse(resp);
														if(o.error){ alert(o.msg); }
														else{ location.href=o.redirect; }
													});
												
													return false;
												})
											</script>
											
										</c:when>
										<c:otherwise>
											<a target="_blank" class="disabled btn btn-large waves-effect waves-light green lighten-1" href="javascript:void(0);">
												<i class="material-icons left">file_download</i>
												<liferay-ui:message key="download"/>
											</a>
										</c:otherwise>
									</c:choose>
								</c:when>
								
								<c:when test="${categoryName == webapp}">
								
									<c:choose>
										<c:when test="${fn:startsWith(artefatto.url, 'http://') || fn:startsWith(artefatto.url, 'https://') }">
											<a target="_blank" class="btn btn-large waves-effect waves-light green lighten-1" href="${artefatto.url}">
												<liferay-ui:message key="visit-homepage"/>&nbsp;<em><liferay-ui:message key="application"/></em><i class="material-icons right">send</i>
											</a>
										</c:when>
										<c:otherwise>
											<a target="_blank" class="btn btn-large waves-effect waves-light green lighten-1" href="http://${artefatto.url}">
												<liferay-ui:message key="visit-homepage"/>&nbsp;<em><liferay-ui:message key="application"/></em><i class="material-icons right">send</i>
											</a>
										</c:otherwise>
									</c:choose>
								
									
								</c:when>
								
								<c:when test="${categoryName == appmobile}">
								
									<c:choose>
										<c:when test="${fn:startsWith(artefatto.url, 'http://') || fn:startsWith(artefatto.url, 'https://') }">
											<a target="_blank" class="btn btn-large waves-effect waves-light green lighten-1" href="${artefatto.url}">
												<i class="material-icons left">system_update_alt</i><liferay-ui:message key="install"/>
											</a>
										</c:when>
										<c:otherwise>
											<a target="_blank" class="btn btn-large waves-effect waves-light green lighten-1" href="http://${artefatto.url}">
												<i class="material-icons left">system_update_alt</i><liferay-ui:message key="install"/>
											</a>
										</c:otherwise>
									</c:choose>
									
								</c:when>
								
								<c:otherwise>
									<!-- Nothing -->
								</c:otherwise>
								
							</c:choose>
							
						</c:if>
						
					</div>
					
<!-- Buttons Small					 -->
					<div class="row">
<!-- 						<div class="col s12"> -->
							<c:if test="${isDraft && (isPublisher || isAdmin || isOwner)}">
								
									<a class="btn waves-effect waves-light orange lighten-2" href="<%=editurl%>">
										<i class="material-icons left">mode_edit</i><liferay-ui:message key="edit"/>
									</a>
								
							</c:if>
							
							<c:if test="${isAdmin || isOwner}">
								
									<a class="btn waves-effect waves-light red lighten-1"  href="#modal-delete-confirm" id="modal-delete-confirm-trigger"> <!-- href="${rimuovi_artefatto}" -->
										<i class="material-icons left">delete</i><liferay-ui:message key="delete"/>
									</a>
								
							</c:if>
<!-- 						</div> -->
					</div>
					
					<c:if test="${categoryName == dataset}">
						<div class="row">
							<h5><liferay-ui:message key="resources"/></h5>
							<div class="divider"></div>
						</div>
						<%
							Set<InteractionPointBean> intMap = new HashSet<InteractionPointBean>();
							Object paramReader = request.getAttribute("readerRDF");
							if(paramReader!=null && paramReader instanceof ReaderRDF){
								ReaderRDF readerRDF=(ReaderRDF)request.getAttribute("readerRDF");
								intMap = readerRDF.getInteractions();
							}
							
							pageContext.setAttribute("intMap", intMap);
							
						%>
						<div class="row">
							
								<ul class="collection resourceslist">
									<li class="collection-item avatar template hide">
										<i class="fa fa-database left circle"></i>
								     	<span class="title"></span>
								      	<p class="description"></p>
								      	<a target="_blank" href="javascript:void(0);" class="secondary-content urlcontent">
								      		<i class="material-icons left">file_download</i>
							      		</a>
								    </li>
								   <c:choose>
									    <c:when test="${!empty intMap}">
											<c:forEach var="ipb" items="${intMap}">
												<!-- Logic -->
												<c:if test="${ipb!=null}">
													<c:choose>
														<c:when test="${!empty ipb.wadl}">
															<c:set scope="page" var="wadl" value="${ipb.wadl}"/>
														</c:when>
														<c:otherwise>
															<c:set scope="page" var="wadl" value="${ipb.url}"/>
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${!empty ipb.title}">
															<c:set scope="page" var="title" value="${ipb.title}"/>
														</c:when>
														<c:otherwise>
															<c:set scope="page" var="title" value="${wadl}"/>
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${!empty ipb.description}">
															<c:set scope="page" var="description" value="${ipb.description}"/>
														</c:when>
														<c:otherwise>
															<c:set scope="page" var="description" value=""/>
														</c:otherwise>
													</c:choose>
													<!-- end Logic -->
													
													<li class="collection-item avatar">
														<i class="fa fa-database left circle"></i>
												     	<span class="title">${title}</span>
												      	<p>${description}</p>
												      	<a target="_blank" href="${wadl}" class="secondary-content">
												      		<i class="material-icons">file_download</i>
											      		</a>
												    </li>
												</c:if>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<div class="hide alert alert-info noresources"><liferay-ui:message key="no-intpoint-founded" /></div>
										</c:otherwise>
									</c:choose>
								</ul>
							
						</div>
					</c:if>
					
					<c:if test="${!empty listaCoppie}">
						<br>
						<div class="row">
							<div class="slider col s12">
						    	<ul class="slides">
						    		<c:forEach var="img" items="${listaCoppie}" >
						    			<c:catch var ="exception">
							    			<% DLFileEntry dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(((ImmagineArt)pageContext.getAttribute("img")).getDlImageId()); %>
								    		<li>
									        	<img src="${remoteInterface}${pathDocumentLibrary}<%=dlf.getUuid()%>">
											</li>
						    			</c:catch>
					    			</c:forEach>
						        </ul>
							</div>
							<script>
								jQuery(document).ready(function(){
									$('.slider').slider({full_width:true});
								});
							</script>
						</div>
					</c:if>
				</div>
<!-- Recommended				 -->
				<div id="recommended" class="col s12 l4">
					<div class="section-rec">
						
							<c:if test="${artefatto.status == 0}">
								<div class="recommendations">
									<div class="recommendations-list">
										<h5 class="sectionRecommends core-color color-2 white-text"><liferay-ui:message key="artvsart"/></h5>
										<c:choose>
											<c:when test="${empty sugg}">
												<div id="no-recommended-alert" class="alert alert-info"><liferay-ui:message key="no_results" /></div>
											</c:when>
											<c:otherwise>
												<ul class="collection">
												<c:forEach var="a" items="${sugg}">
									    			<%try{
													    Artefatto art = (Artefatto)pageContext.getAttribute("a");
													    PortletURL rowURL = renderResponse.createRenderURL();
													    rowURL.setParameter("jspPage", "/html/marketplacestore/view_artefatto.jsp");
													    rowURL.setParameter("resourcePrimKey", String.valueOf(art.getArtefattoId()));
													    
													    String imgpath = request.getContextPath()+"/icons/";
														String catname = CategoriaLocalServiceUtil.getCategoria(art.getCategoriamkpId()).getNomeCategoria();
														if(catname.equalsIgnoreCase(PortletProps.get("Category.rest")))
															imgpath += "icone_servizi_rest-01.png";
														else if(catname.equalsIgnoreCase(PortletProps.get("Category.soap")))
															imgpath += "icone_servizi_soap-01.png";
														else if(catname.equalsIgnoreCase(PortletProps.get("Category.dataset")))
															imgpath += "icone_dataset-01.png";
														else if(catname.equalsIgnoreCase(PortletProps.get("Category.appmobile")))
															imgpath += "icone_app_android-01.png";
														else if(catname.equalsIgnoreCase(PortletProps.get("Category.appweb")))
															imgpath += "icone_app_web-01.png";
														else
															imgpath += "defaultIcon.png";
												    	long imgId =  art.getImgId();
												    	if(imgId > -1){
														    ImmagineArt imgArt = ImmagineArtLocalServiceUtil.getImmagineArt(imgId);
														    if(imgArt!=null){
														    	DLFileEntry dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(imgArt.getDlImageId());
														    	if(dlf!=null){
															    	imgpath = 	request.getAttribute("remoteInterface").toString()
															    			+	request.getAttribute("pathDocumentLibrary").toString()
															    			+	dlf.getUuid();
														    	}
														    } 
													    }%>
													    
														    <li class="collection-item avatar">
														      <img src="<%=imgpath%>" alt="" class="circle">
														      <span class="title"><a href="<%=rowURL%>" target="_blank">${a.title}</a></span>
														      <p class="truncate">
														      	<i class="typename"><%=catname%></i><br>
														      	${a.abstractDescription}
														      </p>
<!-- 														      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a> -->
														    </li>
														
														
													<%} catch(Exception e){
														System.out.println(e.getClass()+": "+e.getMessage());
														/*Skip this recommendation*/ 
													} %>
												</c:forEach>
												</ul>
											</c:otherwise>
									  	</c:choose>
								  	</div>
							  	</div>
							</c:if>
						
					</div>
				</div>
			</div>

<!-- Dettagli /Descrizione /OfferingPricing /Terms&Conditions		 -->
			<div class="row">
				<div class="col s12">
				
					<div class="row" >
<!-- Dettagli -->
						<h5><liferay-ui:message key="details"/></h5>
<%-- 							<i><c:out value="${abs}" /></i> --%>
<%-- 						<c:if test="${abs!=desc}"> --%>
<!-- 							<br/> -->
							<p><c:out value="${desc}" /></p>
<%-- 						</c:if> --%>
<!-- Offering						 -->
						<!-- Offerings retrieved via RDF -->
						<c:if test="${!empty sos}">
							<br/>
							<h5><liferay-ui:message key="offering"/></h5>
							<div class="row">
								<div class="col s11">
									<c:forEach var="so" items="${sos}">
										<% it.eng.rspa.usdl.model.ServiceOffering so = (it.eng.rspa.usdl.model.ServiceOffering)pageContext.getAttribute("so"); %>
										<div class="offering generic_item">
											<div id="title">
												<strong><%=so.getTitle().getValue()%></strong>
												<p><%=so.getDescription().getValue().replaceAll("\\\\n", "\\n")%></p>
												<p>
													<strong><%=so.getHasServiceLevelProfile().getValue().getTitle().getValue()%></strong>:&nbsp;
													<%=so.getHasServiceLevelProfile().getValue().getDescription().getValue().replaceAll("\\\\n", "\\n")%>
												</p>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>
						
						<!-- Offerings retrieved via json LUSDL -->
						<c:if test="${!empty lusdlsos}">
							<br/>
							<h5><liferay-ui:message key="offering"/></h5>
							<div class="row">
								<div class="col s11">
									<c:forEach var="lusdlso" items="${lusdlsos}">
										<% it.eng.metamodel.definitions.ServiceOffering lusdlso = (it.eng.metamodel.definitions.ServiceOffering)pageContext.getAttribute("lusdlso"); %>
										<div class="offering generic_item">
											<div id="title">
												<strong><%=lusdlso.getTitle()%></strong>
												<p><%=lusdlso.getDescription().replaceAll("\\\\n", "\\n")%></p>
												<em><a href='<%=lusdlso.getUrl()%>' target="_blank"><%=lusdlso.getUrl()%></a></em>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>
<!-- Terms						 -->
						<!-- Terms and conditions retrieved via RDF -->
				 		<c:if test="${!empty terms}">
				 			<br/>
				 			<h5><liferay-ui:message key="terms"/></h5>
				 			<div class="row">
								<div class="col">
									<c:forEach var="term" items="${terms}">
						 				<%it.eng.rspa.usdl.model.License t = (it.eng.rspa.usdl.model.License)pageContext.getAttribute("term"); %>
						 				<div class="generic_item">
											<a href="<%=t.getUrl().getValue()%>">
												<strong><%= t.getTitle().getValue() %></strong>
											</a>
											<p>
												<%=t.getDescription().getValue().replaceAll("\\\\n", "\\n")%>
											</p>
										</div>
						 			</c:forEach>
					 			</div>
				 			</div>
				 		</c:if>
				 		
				 		<!-- Licenses retrieved via json LUSDL -->
				 		<c:if test="${!empty lusdllicenses}">
				 			<br/>
				 			<h5><liferay-ui:message key="terms"/></h5>
				 			<div class="row">
								<div class="col">
									<c:forEach var="lusdl_lic" items="${lusdllicenses}">
						 				<%it.eng.metamodel.definitions.License l = (it.eng.metamodel.definitions.License)pageContext.getAttribute("lusdl_lic"); %>
						 				<div class="generic_item">
											<a href="<%=l.getUrl()%>">
												<strong><%= l.getTitle() %></strong>
											</a>
											<p>
												<%=l.getDescription().replaceAll("\\\\n", "\\n")%>
											</p>
										</div>
						 			</c:forEach>
					 			</div>
				 			</div>
				 		</c:if>
						
					</div>
					<div class="row">
						<a href="#" id="readmorebutton" class="btn hide col s8 offset-s2"><liferay-ui:message key="read-more"/></a>
					</div>
				</div>
				<div class="divider"></div>
				
<!-- Operations -->
				<c:if test="${(categoryName == restname || categoryName == soapname || categoryName == noname) && hasmetamodel}">					
					<%@include file="include/operations.jspf" %>
				</c:if>
				
<!-- Dependences -->
				<%@include file="include/dependencies.jspf" %>
			</div>
			
			<c:if test="${hasidea}">
				<div class="row">
					<h5><liferay-ui:message key="ideas"/></h5>
					<ul class="collection">
						<c:forEach items="${ideas}" var="entry">
						   <li class="collection-item avatar">
									<img src="/documents/10181/0/idea-logo/78f80153-0844-4e82-8aa1-2e893bec6c36?t=1470388941769" alt="" class="circle">
									<a class="title" target="_blank" href="/ideas_explorer/-/ideas_explorer_contest/${entry.key}/view">
										${entry.value}
									</a>
									<p><liferay-ui:message key="idea-id"/>:&nbsp;${entry.key}</p>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			
<!-- Commenti  -->
			<c:if test="${!isDraft}">
				<portlet:actionURL name="invokeTaglibDiscussion" var="discussionURL"/> 
				
				<hr/>
				<liferay-ui:panel-container extended="false" id="artefattoCommentsPanelContainer" persistState="true">
					<liferay-ui:panel collapsible="true" extended="true" id="artefattoCommentsPanel" persistState="true" title='comments'>
						<liferay-ui:discussion className="<%= Artefatto.class.getName() %>"
							classPK="${artefatto.artefattoId}" redirect="<%=themeDisplay.getURLCurrent()%>"
							formAction="${discussionURL}" userId="<%=themeDisplay.getUserId()%>" />
					</liferay-ui:panel>
				</liferay-ui:panel-container>
			</c:if>
			
		</div>
		<input type="hidden" id="portletnamespace" value="<portlet:namespace/>"/>
		



		<script type="text/javascript">
		
			function setstars(container, score){
				
				var template = container.find('.template').first();
				
				var ratingcontent = template.clone();
				ratingcontent.removeClass('template');
				
				var fastar = ratingcontent.find(".fa-star").clone();
			    var fastaro = ratingcontent.find(".fa-star-o").clone();
			    ratingcontent.empty();
			    for(var i=0; i<score; i++){
			    	ratingcontent.append("&nbsp;");
			    	ratingcontent.append(fastar.clone());
			    }
			    var remainder = 5-score;
				for(var i=0; i<remainder; i++){
					ratingcontent.append("&nbsp;");
					ratingcontent.append(fastaro.clone());
				}
				
				container.append(ratingcontent);
			}
		
			function showAvgStars(data){
				setstars($('#rating'), data.avgscore);
				$('#rating #n-votes').text(data.nscores);
			}
			
			function showMyStars(data){
				setstars($('#my-rating'), data.myscore);
			}
			
			function appendresources(resources){
				console.log("appending");
				var appender = $('.resourceslist');
				var template = appender.find('.template');
				
				if(resources.length == 0){
					$('.noresources').removeClass("hide");
				}
				
				for(var i in resources){
					var curres = resources[i];
					var item = template.clone();
					item.removeClass('template hide');
					
					item.find('.title').text(curres.name);
					item.find('.description').text(curres.description);
					item.find('.urlcontent').attr("href", curres.url);
					
					appender.append(item);
				}
			}
			
			var myrating = 0;
			$(document).on('mouseenter', '#my-rating .fa-star,#my-rating .fa-star-o', function(){
				$(this).prevAll().andSelf().each(function(i){
					if($(this).hasClass('fa-star-o')){
						$(this).toggleClass('fa-star').toggleClass('fa-star-o');
					}
				});
				$(this).nextAll().each(function(i){
					if($(this).hasClass('fa-star')){
						$(this).toggleClass('fa-star').toggleClass('fa-star-o');
					}
				});
			});
			
			$(document).on('click', '#my-rating .fa-star,#my-rating .fa-star-o', function(){
				myrating = $(this).index() + 1;
				var namespace = jQuery('#portletnamespace').val();
				var aid = ${artefatto.artefattoId};
				var uid = ${artefatto.userId};
				var input = {};
					input[namespace+'artefactid']=aid ;
					input[namespace+'userid']=<%=themeDisplay.getUserId()%> ;
					input[namespace+'rating']=myrating ;
					input[namespace+'action']='storeRating';
				
				jQuery.post('${resURL}', input)
						.done(function(response){
							
						});
				
			});
			
			$(document).on('mouseleave', '#my-rating .rating-content', function(){
				$(this).find('.fa').each(function(i){
					if(i<myrating){ $(this).removeClass('fa-star-o').addClass('fa-star'); }
					else{ $(this).removeClass('fa-star').addClass('fa-star-o'); }
				});
			});
			
			$(document).ready(function(){
				
				var artefact_level = ${level}
// 				console.log("artefact_level "+artefact_level)
				
				if (artefact_level > 0){
					
					var levImg = $('.level_img');
					levImg.removeClass('hide');
					levImg.attr('src', levImg.data('contextpath')+'\\icons\\'+artefact_level+'_level.png');
					levImg.attr('data-tooltip',  levImg.data('tooltipbase')+" "+artefact_level);
				}
				
				
				
				
				$('#modal-delete-confirm-trigger').leanModal({
					dismissable: false
				});
				
				jQuery('.readmoredetails').dotdotdot({
					height: 60,
					callback: function(isTruncated, orgContent){
						if(isTruncated){
							jQuery('#readmorebutton').removeClass("hide");
						}
					}
				});
				
				var jsStatus = ${artefatto.status};
				if(jsStatus == 0){
					if($( document ).width() > 992){
						var dynamicWidth = $('#recommended').width();
						var rangebottom = $('#recommended').closest('.row').offset().top + $('#recommended').closest('.row').outerHeight(true);
						var recommendationsHeight = $('.recommendations').outerHeight(true);
						$('.recommendations').pushpin({ 
							top: $('.recommendations').offset().top-32,
							offset: 40,
							bottom: (rangebottom - recommendationsHeight)
						});
// 						$('.pin-top').width(dynamicWidth);
						$('.pinned').width(dynamicWidth);
// 						$('.pin-bottom').width(dynamicWidth);
					}
				}
				
				jQuery(document).on('click', '#readmorebutton', function(){
					var node = jQuery('.readmoredetails');
					node.trigger("destroy");
					$(this).hide();
					return false;
				});
				
				
				<%
					String tit = ((Artefatto)request.getAttribute("artefatto")).getTitle();
					String repTit = tit.replace("'", "\\'");
				%>
				
				updateBreadcrumbs('<%=repTit%>');
				
				var isDataset = ${isDataset};
				if(isDataset){
					var namespace = jQuery('#portletnamespace').val();
					var aid = ${artefatto.artefattoId};
					var input = {};
						input[namespace+'artefactid']=aid ;
						input[namespace+'action']='alignDatasetMetadata';
					
					$('.async-load-spinner').removeClass('hide');						
					jQuery.post('${resURL}', input)
							.done(function(response){
								var resp = JSON.parse(response);
								console.log(resp);
								showAvgStars(resp.rating);
								$('#rating .async-load-spinner').addClass('hide');
								showMyStars(resp.rating);
								myrating = resp.rating.myscore;
								$('#my-rating .async-load-spinner').addClass('hide');
								
								appendresources(resp.resources);
							});
				}
				
				
			})
			
			$(window).load(function() {
				var scrollStop = $('#recommended').offset().top;
				var dynamicWidth = $('#recommended').width();
				
				$(window).on('resize', function () {
					dynamicWidth = $('#recommended').width();
					$(".recommendations").width(dynamicWidth);
					
					if($( document ).width() <= 992){
						$('.recommendations').pushpin('remove');
					}
				});
				
				$(window).on('scroll', function () {
					
					if($(window).scrollTop() >= scrollStop){
						$('.recommendations').width(dynamicWidth);
					}else{
// 						$('.pinned').width('auto');
					}
				});
			});
		</script>
		
		
	</c:otherwise>
</c:choose>