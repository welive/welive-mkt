<%@page import="it.eng.rspa.marketplace.ConfUtil"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator" %>
<%@page import="com.liferay.portal.kernel.util.ParamUtil" %>

<%@page import="com.liferay.portal.util.PortletKeys"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portlet.PortalPreferences"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>

<%@page import="com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil" %>
<%@page import="com.liferay.portlet.ratings.model.RatingsStats" %>

<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil" %>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil" %>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.model.Categoria" %>
<%@page import="it.eng.rspa.marketplace.category.*" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/html/marketplacestore/include/init.jsp"%>
<%@ include file="/html/checkPublicUser.jspf" %>

<% if(request.getSession().getAttribute("LIFERAY_SHARED_pilot")==null){%>
		<script>
			location.href = "/";
		</script>
	<%  return;
	}
	boolean isLogged = themeDisplay.isSignedIn() && user!=null && !user.isDefaultUser();
	boolean recommendationsEnabled = isLogged && ConfUtil.getBoolean("de.enabled");
	
	boolean canPublish = false;
	if(isLogged){
		canPublish = MyMarketplaceUtility.isDeveloper(user);
		//|| MyMarketplaceUtility.isAuthority(user);
	}
	
	//Setup artefacts counter
	int all_counter = ArtefattoLocalServiceUtil.getArtefactsByPilotStatus(pilotId, WorkflowConstants.STATUS_APPROVED).size();
		pageContext.setAttribute("all_counter",all_counter);
	int bb_counter = ArtefattoLocalServiceUtil.getBuildingBlocks(pilotId).size();
		pageContext.setAttribute("bb_counter",bb_counter);
	int psa_counter = ArtefattoLocalServiceUtil.getPublicServices(pilotId).size();
		pageContext.setAttribute("psa_counter",psa_counter);
	int ds_counter = ArtefattoLocalServiceUtil.getDatasets(pilotId).size();
		pageContext.setAttribute("ds_counter",ds_counter);
	
	pageContext.setAttribute("isLogged", isLogged);
	pageContext.setAttribute("canPublish", canPublish);
	pageContext.setAttribute("recommendationsEnabled", recommendationsEnabled);
	
	boolean isBilbaoPilot = MyMarketplaceUtility.isBilbaoPilot(pilotId);
	boolean isUusimaaPilot = MyMarketplaceUtility.isUusimaaPilot(pilotId);
	boolean isNovisadPilot = MyMarketplaceUtility.isNovisadPilot(pilotId);
	boolean isTrentoPilot = MyMarketplaceUtility.isTrentoPilot(pilotId);
	
	pageContext.setAttribute("isBilbaoPilot", isBilbaoPilot);
	pageContext.setAttribute("isUusimaaPilot", isUusimaaPilot);
	pageContext.setAttribute("isNovisadPilot", isNovisadPilot);
	pageContext.setAttribute("isTrentoPilot", isTrentoPilot);

%>

<%-- URLs --%>
	<liferay-portlet:renderURL var="elencoMieiArtefatti">
		<liferay-portlet:param name="jspPage" value="/html/marketplacestore/elencoMieiArtefatti.jsp"/>
	</liferay-portlet:renderURL>
	
	<portlet:resourceURL id="ajaxUrl" var="ajaxUrl"/>
<%-- End URLs --%>

<div class="materialize">
		
	<!-- Head 1 -->
	<div class="section">
		<div class="row head-section">
		
			<div class="col s3 valign left">
				<a class="waves-effect btn-flat tooltipped" href="/" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  			</a>
			</div>
		
			<div class="col s6 search center ">
				<c:if test="${isLogged && canPublish}">				
					<a class="waves-effect btn core-color color-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="mine" />" href="${elencoMieiArtefatti}">
						<i class="material-icons left">face</i><liferay-ui:message key="mine" />
					</a>
					<a class="waves-effect btn core-color color-2 tooltipped modal-trigger" href="#modal1" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="new_art"/>">
						<span class="hide-on-small-only"><i class="material-icons left">add</i><liferay-ui:message key="new_art"/></span>
					</a>
					<%@ include file="/html/marketplacestore/include/new-artefact-modal.jspf" %>
				</c:if>
		    </div>
			
			<div class="col s3 valign center">	
			
			</div>
			
		</div>
	</div>
		<!-- END Head 1 -->
	
	<!-- 	Head 2 -->
	<div class="searchContainer container section">
		<div class="row head-section">
			
		
			<div class="col s12 m8 search valign ">
	          <div class="search-wrapper card">
	            <input id="search"><i class="material-icons searchLensGo">search</i>
	            <div class="search-results"></div>
	          </div>
		    </div>
			
			<div class="col s12 m4 valign center">	
				
<!-- 			Select Order -->

					<div class="input-field orderfilter">
						<select id="orderByMenu" class="icons">
					      <option value="title" class="circle"><liferay-ui:message key="title"/></option>
					      <option value="date" class="circle"><liferay-ui:message key="date"/></option>
					      <option value="level" class="circle"><liferay-ui:message key="level"/></option>
					      <option value="ratings" class="circle"><liferay-ui:message key="ratings"/></option>
					    </select>
					    <label><liferay-ui:message key="order-by"/></label>
	    			</div>

			</div>
			
		</div>
	</div>
		<!-- END Head 2 -->
		
			
		<!-- Artefacts Sections -->
	<div class="artefactContainer container">
		<div class="row tabContent top-margin">
			

			<!-- Filters Section -->
			<div class="filter-section col s12 m12 l3 z-depth-0">
					<%@include file="include/filters.jspf" %>
			</div>

			<!-- Recommended Section -->
			<c:if test="${isLogged}">
				<div class="recommended-section col s12 m12 l9 .hide" >
					<h5 class="sectionTitle"><liferay-ui:message key="for-you" /></h5>
					<ul class="storeList row" id="recommended">
						
					</ul>
					<div class="top-margin async-load-spinner hide center-align"><%@ include file="../loader.jspf" %></div>
					<div id="no-recommended-alert" style="opacity:0;" class="alert alert-info"><liferay-ui:message key="no_results" /></div>

				</div>
				
			</c:if>
			
			<%-- Recent Section --%>
			<div class="recent-section col s12 m12 l9">
				<h5 class="sectionTitle"><liferay-ui:message key="mkt.artefacts" /></h5>
 			
			 	<ul class="storeList row" id="recentList">
			 		
			 		<li id="template" class="market_item col s12 m6 l6">
					 
					    <div class="card horizontal restyledMKP">
					      <div class="card-image">
							<img class="cover_art" src="#" />
					      </div>
					      <div class="card-stacked">
					        <div class="card-content">
					          	<p class="li-item title_art"></p>
								<p class="li-item type"></p>
								<div class="li-item other_info">&nbsp;
									<div class="rating_art">&nbsp;
										<i class="fa fa-star core-color-text text-color-1"></i>
										<i class="fa fa-star-o core-color-text text-color-1"></i>
									</div>
									<div class="level_art">
										<img class="level_img right tooltipped hide" src="<%=request.getContextPath()%>\icons\1_level.png" data-contextpath = "<%=request.getContextPath()%>" 
											 data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="level" /> 1"   data-tooltipbase="<liferay-ui:message key="level" />" />
									</div>
								</div>
								<!-- Da visualizzare span Recommended (ponendo opacity=1) se l'artefatto Consigliato per l'utente (chiedere Nino)-->
								<!--  <span class="li-item recommended chip core-color color-1 white-text hide"><liferay-ui:message key="recommended" /></span> -->
					        </div>
					        <div class="card-action">
					          <a class="right" href="#"><liferay-ui:message key="details" /></a> 
					        </div>
					      </div>
					    </div>
					    
					</li>
				</ul>
				
				<div class="top-margin async-load-spinner hide center-align"><%@ include file="../loader.jspf" %></div>
				<div id="no-artefacts-alert" style="opacity:0;" class="alert alert-info"><liferay-ui:message key="no_results" /></div>
			</div>
			
		</div>
	</div>
	<!-- END Artefacts Sections -->
	

	
</div>
<input type="hidden" id='portletnamespace' name='namespace' value='<portlet:namespace/>'/>


<script>

	/*Global variables for ajax on scroll.*/
	var start_index = 0;
	var finished = false;
	var ajaxAllowed = true;
	var canpublish = ${canPublish};
	var isBilbaoPilot = ${isBilbaoPilot};
	var isUusimaaPilot = ${isUusimaaPilot};
	var isNovisadPilot = ${isNovisadPilot};
	var isTrentoPilot = ${isTrentoPilot};
	var recommendationsEnabled = ${recommendationsEnabled};
	
	var selectedPilots = {/*setted in context by jstl*/
			'Bilbao': isBilbaoPilot,
			'Uusimaa': isUusimaaPilot,
			'Novisad': isNovisadPilot,
			'Trento': isTrentoPilot,
	};
	
	var types = {
			'bblocks': canpublish,
			'psa': true,
			'dataset':canpublish
			};
	
	var pilot = '<%=pilotId%>';
	
	var recommended_request = null;
	var recent_request = null;
	
	var orderby = 'title';
	
	var searchby = '';
	
	function reinit(recommenderEnabled){
		
		checkFilterInStorage();
		
		 if (timeout) {  
			    clearTimeout(timeout);
		 }
		
		start_index = 0;
		finished = false;
		
		$('#no-artefacts-alert').css('opacity', 0);
		jQuery('.market_item').not('#template').remove();
		
		types["bblocks"] = $('#cat1').is(":checked");
		types["psa"] = $('#cat2').is(":checked");
		types["dataset"] = $('#cat3').is(":checked");
		
		selectedPilots["Bilbao"] = $('#esPilot').is(":checked");
		selectedPilots["Uusimaa"] = $('#fiPilot').is(":checked");
		selectedPilots["Novisad"] = $('#srPilot').is(":checked");
		selectedPilots["Trento"] = $('#itPilot').is(":checked");
		
		orderby = $('#orderByMenu option:selected').val();
		
		searchby = $("#search").val();
		
		ajaxAllowed=true;
		loadArtefacts('${ajaxUrl}', start_index, false);
		
		if (recommenderEnabled){
			getRecommendations();
		}
		 
		
		return;
	}
	
	<%Long ccuid = Long.parseLong(user.getExpandoBridge().getAttribute("CCUserID").toString());
	if(recommendationsEnabled){%>
		function loadRecommendedArtefacts(latitude, longitude){
			
			var input = {};
				input[namespace+'types'] = types;
				input[namespace+'action'] = 'recommend';
				input[namespace+'userId'] =  '<%=ccuid%>';
				input[namespace+'lat'] = latitude;
				input[namespace+'lng'] = longitude;
			
			$('.recommended-section .async-load-spinner').removeClass('hide');

			/*with this control, if start a new Ajax request, the earlier is aborted */	
			if(!(recommended_request == null)){
				recommended_request.abort();
				recommended_request = null;
			}
			
			recommended_request = jQuery.post('${ajaxUrl}', input)
				.done(function(response){
						var resp = JSON.parse(response);
						$('.recommended-section .async-load-spinner').addClass('hide');
						appendRecommendedArtefacts(resp);
						recommended_request = null;
				});
		}
	<%}%>
	var ajaxUrl = '${ajaxUrl}';
	var namespace = "";
	jQuery(document).ready(function(){
		namespace = jQuery('#portletnamespace').val();
		
		// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
		$('a.modal-trigger[href="#modal1"]').leanModal();
		
		reinit(recommendationsEnabled); /*start the Ajax call*/
		
		<%if(recommendationsEnabled){%> getRecommendations(); <%}
		else{%> $('#no-recommended-alert').animate({ 'opacity': 1}, 300); <%}%>
		
		jQuery(window).scroll(function () {
			var desiredheight = jQuery(document).height()/2;
			if (jQuery(window).height() + jQuery(window).scrollTop() >= desiredheight && !finished && ajaxAllowed){
				
				loadArtefacts('${ajaxUrl}', start_index, true);
			}
		   	else{  /*console.log("ajax not Allowed");*/ }
		});
		
	});
	
	
	$(document).on('change', '#orderByMenu', function(){
			reinit(recommendationsEnabled);/*start the Ajax call*/
	});
	
	$(document).on('click', '.searchLensGo', function(){
		reinit(recommendationsEnabled);/*start the Ajax call*/
	});
	
	$( "#search" ).blur(function() {
		reinit(recommendationsEnabled);/*start the Ajax call*/
	});
	
	
	var timeout = null;
	jQuery(document).on('keyup', '#search', function(){
		
		//start the search after 2 seconds
		 if (timeout) {  
			    clearTimeout(timeout);
		 }
		 
		 timeout = setTimeout(function() {
			 reinit(recommendationsEnabled);
		  }, 2000);
	}); 

	//start search on "enter/Invio"
	$(document).on('keypress', '#search', function(event){
		var key = event.which || event.keyCode;
		if ( key == 13 )
			reinit(recommendationsEnabled);
	});
	
	
	
	function PushDataItem(eventname) {
	    this.ccUserID = <%=ccuid%>;
	    this.eventName = eventname;
	    this.entries = new Array();
	}

	function PushEntry(key, value){
		this.key = key;
		this.value = value;
	}

	$("#search").focusin(function() {
		$('.search-wrapper').addClass("focused");
	});
	$("#search").focusout(function() {
		$('.search-wrapper').removeClass("focused");
	});
	
	
	//    Toggle Containers on page
	function toggleContainer(){
		$(".searchContainer, .artefactContainer").each(function(){
			if($( window ).width() <= 1400){
				
	  		  	$(this).addClass('no-container');
	   		  	$(this).removeClass('container');
				
			}else {
	    	  	$(this).removeClass('no-container');
	    	  	$(this).addClass('container');
		    }
		})
	}
	
	$(document).ready(function(){
		toggleContainer();
	});
	$(window).resize(function(){
		toggleContainer();
	})
	
        
</script>