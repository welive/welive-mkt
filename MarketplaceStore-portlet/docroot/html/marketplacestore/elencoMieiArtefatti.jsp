<%@page import="com.liferay.portal.kernel.util.StringPool"%>

<%-- <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> --%>
<%@ include file="/html/marketplacestore/include/init.jsp"%>

<%@ include file="/html/checkPrivateUser.jspf" %>

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->

<%
	boolean canPublish = MyMarketplaceUtility.isDeveloper(user);
	String orgName = null;
	
	Role rLeader = RoleLocalServiceUtil.getRole(user.getCompanyId(), PortletProps.get("role.companyleader"));
	if(user.getRoles().contains(rLeader)){
		List<Organization> orgs = user.getOrganizations();
		for(Organization org : orgs){
			if(Boolean.parseBoolean(org.getExpandoBridge().getAttribute("isCompany").toString())){
				orgName = org.getName();
			}
		}
	}
	
	if(!canPublish){%>
		<script>
			location.href="/marketplace";
		</script>
<%	return;
	}
	
	String pathDocumentLibrary = themeDisplay.getPortalURL()
			+ themeDisplay.getPathContext() + "/documents/"
			+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
	
	User selUser = themeDisplay.getUser();
	
// 	PortletURL portletHomeURL = renderResponse.createRenderURL();
// 	portletHomeURL.setParameter("jspPage", "/html/marketplacestore/view.jsp");
	
// 	PortletURL elencoMieiArtefatti = renderResponse.createRenderURL();
// 	elencoMieiArtefatti.setParameter("jspPage", "/html/marketplacestore/elencoMieiArtefatti.jsp");
	
PortletURL rowURL = renderResponse.createRenderURL();
rowURL.setParameter("jspPage", "/html/marketplacestore/view_artefatto.jsp");
rowURL.setParameter("bUrl", "/html/marketplacestore/elencoMieiArtefatti.jsp");
%>

<%-- <liferay-portlet:renderURL var="rowURL"> --%>
<%-- 	<liferay-portlet:param name="jspPage" value="/html/marketplacestore/view_artefatto.jsp"/> --%>
<%-- </liferay-portlet:renderURL> --%>

<liferay-portlet:renderURL var="elencoMieiArtefatti">
	<liferay-portlet:param name="jspPage" value="/html/marketplacestore/elencoMieiArtefatti.jsp"/>
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="portletHomeURL">
	<liferay-portlet:param name="jspPage" value="/html/marketplacestore/view.jsp"/>
</liferay-portlet:renderURL>


<div class="materialize">

	<div class="section row valign-wrapper">
		<div class="col s4 valign left">
			<a class="waves-effect btn-flat tooltipped" href="${portletHomeURL}" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="back"/>">
				<i class="icon-arrow-left left"></i>
				<liferay-ui:message key="back"/>
			</a>
		</div>
		<div class="col s4 valign center">
			<h3 class="flow-text strong"><liferay-ui:message key="mine"/></h3>
			<div class="divider"></div>
		</div>
		<div class="col s4 valign right"></div>
	</div>
	
	<div class="row">
		<div class="col s4 offset-s4 valign center">
			<a class="waves-effect btn core-color color-2 tooltipped modal-trigger" href="#modal1" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="new_art"/>">
				<span class="hide-on-small-only"><liferay-ui:message key="new_art"/></span>
			</a>
			<%@ include file="/html/marketplacestore/include/new-artefact-modal.jspf" %>
		</div>
	</div>
	
	<div class="row">
		<div class="col s12">
			<ul class="tabs row">
				<li class="tab col s4">
					<a class="active" href="#published">
						<liferay-ui:message key="published"/>
						<liferay-ui:icon-help message="mypublished_tooltip"/>
					</a>
				</li>
				<li class="tab col s4">
					<a href="#draft">
						<liferay-ui:message key="draft"/>
						<liferay-ui:icon-help message="mydraft_tooltip"/>
					</a>
				</li>
<!-- 				<li class="tab col s4"> -->
<!-- 					<a href="#other"> -->
<%-- 						<liferay-ui:message key="other"/> --%>
<%-- 						<liferay-ui:icon-help message="myother_tooltip"/> --%>
<!-- 					</a> -->
<!-- 				</li> -->
			</ul>
		</div>
		
		<div id="published" class="col s12 dashboardsections">
			<%@ include file="/html/marketplacestore/include/myartefacts/published.jspf" %>
		</div>
		
		<div id="draft" class="col s12 dashboardsections">
			<%@ include file="/html/marketplacestore/include/myartefacts/drafts.jspf" %>
		</div>
		
<!-- 		<div id="other" class="col s12 dashboardsections"> -->
<%-- 			<%@ include file="/html/marketplacestore/include/myartefacts/others.jspf" %> --%>
<!-- 		</div> -->
		
	</div>
</div>

<script>
	jQuery(document).ready(function(){
		$('.modal-trigger').leanModal();
		updateBreadcrumbs('<liferay-ui:message key="mine"/>');
	    $('ul.tabs').tabs({
	    	onShow: function(selected){
	    		console.log(selected);
	    	}
	    });
	});
	
	
</script>