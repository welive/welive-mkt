<div class="materialize">
	<div class="alert alert-error center">
		
		<i class="fa fa-5x fa-exclamation-triangle" aria-hidden="true"></i>
		
		<h1 style="color:#b50303;">404</h1>
		<h3 style="color:#b50303;">Not found!</h3>
		
		<p > <%=request.getAttribute("error-message") %></p>
	</div>
</div>