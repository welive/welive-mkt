<%@page import="it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.model.MarketplaceConf"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ include file="/html/marketplacestore/include/init.jsp" %>

<portlet:defineObjects />

<%  MarketplaceConf conf = null;

    long confId = ParamUtil.getLong(request, "confId");

    if (confId > 0) {
    	conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(confId);
    }

    String redirect = ParamUtil.getString(request, "redirect");
%>
<liferay-portlet:actionURL portletConfiguration="true" var="setPropertyURL" name="setProperty"/>

<liferay-ui:header backURL="<%= redirect %>"
    				title='<%= (conf != null) ? conf.getKey() : "New Property" %>' />

<aui:form action="<%=setPropertyURL%>" method="POST" name="setProps">

	<aui:input type="hidden" name="confId" value='<%= (conf != null) ? conf.getConfId() : ""%>'/>

    <aui:input type="text" label="Key" name="key" 
     			value='<%= (conf != null) ? conf.getKey() : ""%>'>
     	<aui:validator name="required"/>
    </aui:input>
     
    <aui:input type="text" label="Type" name="type" 
     			value='<%= (conf != null) ? conf.getType() : ""%>'>
     	<aui:validator name="required"/>
    </aui:input>

	<aui:input type="text" label="Value" name="value" 
         		value='<%= (conf != null) ? conf.getValue() : ""%>'>
         <aui:validator name="required"/>
	</aui:input>
	
	<aui:input type="textarea" label="Options" name="options" 
         		value='<%= (conf != null) ? conf.getOptions() : ""%>'/>

	<aui:button type="submit"/>
</aui:form>