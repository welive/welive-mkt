<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil"%>
<%@page import="it.eng.rspa.marketplace.artefatto.model.MarketplaceConf"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>

<%@ page import="it.eng.rspa.marketplace.category.*" %>
<%@ page import="it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil"%>
<%@ page import="it.eng.rspa.marketplace.artefatto.model.Categoria" %>

<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.FileReader"%>
<%@ page import="java.io.IOException"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ include file="/html/marketplacestore/include/init.jsp" %>

<% String redirect = PortalUtil.getCurrentURL(renderRequest);%>
<portlet:defineObjects />

<portlet:renderURL var="managedataURL">
    <portlet:param name="mvcPath" value="/html/marketplaceadmin/manage_data.jsp" />
    <portlet:param name="redirect" value="<%= redirect %>" />
</portlet:renderURL>

<aui:button-row>
    <portlet:renderURL var="addPropertyURL">
        <portlet:param name="mvcPath" value="/html/marketplaceadmin/upsert_property.jsp" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:renderURL>

    <aui:button onClick='${addPropertyURL}' value="New Property" />
</aui:button-row>

<liferay-ui:search-container emptyResultsMessage="No property found">
    <liferay-ui:search-container-results
        results="<%= MarketplaceConfLocalServiceUtil.getMarketplaceConfs(QueryUtil.ALL_POS, QueryUtil.ALL_POS) %>"
        total="<%= MarketplaceConfLocalServiceUtil.getMarketplaceConfsCount() %>" />

    <liferay-ui:search-container-row
        className="MarketplaceConf"
        keyProperty="confId"
        modelVar="conf" escapedModel="true">
        
        <liferay-ui:search-container-column-text
            name="key"
            value="<%= conf.getKey()%>"/>
            
		<liferay-ui:search-container-column-text
            name="type"
            value="<%= conf.getType() %>"/>
        
        <liferay-ui:search-container-column-text
            name="value"
            value="<%= conf.getValue() %>"/>
        
        <liferay-ui:search-container-column-jsp
     		align="right"
     		path="/html/marketplaceadmin/property_actions.jsp"/>
        
    </liferay-ui:search-container-row>

    <liferay-ui:search-iterator />
</liferay-ui:search-container>

<hr/>
<a href="<%=managedataURL%>">Manage Marketplace data</a>
