<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="it.eng.rspa.marketplace.category.Category"%>
<%@page import="it.eng.rspa.marketplace.artefatto.model.Categoria"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ include file="/html/marketplacestore/include/init.jsp" %>

<portlet:defineObjects />

<portlet:renderURL var="backURL" />
<liferay-ui:header backURL="<%= backURL %>" title='Data management' />


<%@ include file="sections/categoriesManagement.jspf" %>

<%@ include file="sections/dbCleanup.jspf" %>

<%@ include file="sections/datasetManagement.jspf" %>

