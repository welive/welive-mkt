<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>

<%@ page import="it.eng.rspa.marketplace.artefatto.model.Categoria" %>
<%@ page import="it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil" %>

<%
    Categoria cat = null;

    long catId = ParamUtil.getLong(request, "catId");

    if (catId > 0) {
    	cat = CategoriaLocalServiceUtil.getCategoria(catId);
    }

    String redirect = ParamUtil.getString(request, "redirect");
%>

<aui:model-context bean="<%= cat %>" model="<%= Categoria.class %>" />
<portlet:renderURL var="viewCatURL" />
<portlet:actionURL name='<%= cat == null ? "addCategory" : "updateCategory" %>' 
					var="editLocationURL" 
					windowState="normal" />

<liferay-ui:header
    backURL="<%= viewCatURL %>"
    title='<%= (cat != null) ? cat.getNomeCategoria() : "new-category" %>'
/>

<aui:form action="<%= editLocationURL %>" method="POST" name="fm">
    <aui:fieldset>
        <aui:input name="redirect" type="hidden" value="<%= redirect %>" />

        <aui:input name="catId" type="hidden" value='<%= cat == null ? "" : cat.getIdCategoria() %>'/>

        <aui:input type="text" label="category-name" 
        			name="newCategoryName" value='<%= cat == null ? "" : cat.getNomeCategoria() %>'>
        	<aui:validator name="required"/>
        </aui:input>

        <aui:input type="text" label="application_conf" 
        			name="newCategorySupports" value='<%= cat == null ? "" : cat.getSupports() %>' />

    </aui:fieldset>

    <aui:button-row>
        <aui:button type="submit" />

        <aui:button onClick="<%= viewCatURL %>"  type="cancel" />
    </aui:button-row>
</aui:form>