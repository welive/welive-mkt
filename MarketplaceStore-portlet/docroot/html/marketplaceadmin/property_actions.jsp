<%@page import="it.eng.rspa.marketplace.artefatto.model.MarketplaceConf"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%@ page import="it.eng.rspa.marketplace.artefatto.model.Categoria"%>

<portlet:defineObjects />

<%
    ResultRow row = (ResultRow) request
            .getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    MarketplaceConf conf = (MarketplaceConf) row.getObject();

    String name = MarketplaceConf.class.getName();
    long confId = conf.getConfId();

    String redirect = PortalUtil.getCurrentURL(renderRequest);
%>
 
<liferay-ui:icon-menu>
	
	<portlet:renderURL var="editURL">
        <portlet:param name="mvcPath" value="/html/marketplaceadmin/upsert_property.jsp" />
        <portlet:param name="confId" value="<%= String.valueOf(confId) %>" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:renderURL>
    <liferay-ui:icon image="edit" url="<%= editURL.toString() %>" />
    
     <portlet:actionURL name="deleteProperty" var="deleteURL">
        <portlet:param name="confId" value="<%= String.valueOf(confId) %>" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:actionURL>
    <liferay-ui:icon-delete url="<%= deleteURL.toString() %>" />
</liferay-ui:icon-menu>