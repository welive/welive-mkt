<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="it.eng.rspa.wizard.controller.WizardController"%>
<%@page import="it.eng.rspa.wizard.controller.AjaxActions"%>

<%@page import="java.util.Set"%>

<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Portlet"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>

<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %> 
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<% 
	WizardController.action(request, response);
	Portlet portlet = PortletLocalServiceUtil.getPortletById(company.getCompanyId(), portletDisplay.getId());
	String scriptJs = "/js/publicationwizard/functions.js?"+System.currentTimeMillis(); //TODO RIMUOVERE IN PRODUZIONE e riportare nell'inclusione dello script solo "/js/publicationwizard/functions.js"
%>

<c:choose>
	<c:when test="${notfound}">
		<%@include file="../notfound.jsp" %>
	</c:when>
	<c:otherwise>
		<liferay-util:html-top>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			<script src="<%= PortalUtil.getStaticResourceURL(request, PortalUtil.getPathContext(request) + scriptJs, portlet.getTimestamp()) %>"></script>
			<script src="<%= PortalUtil.getStaticResourceURL(request, PortalUtil.getPathContext(request) + "/js/marketplace/bundle.min.js", portlet.getTimestamp()) %>" type="text/javascript" ></script>
			<style>
				.materialize .paramtab input,textarea{
					margin-bottom:0;
				}
			</style>
			
		</liferay-util:html-top>

		<portlet:resourceURL var="resURL"/>
		<portlet:actionURL name="deleteArtefact" var="delete_artefact">
			<portlet:param name="vcpubblication" value="${vcpubblication}"/>
		</portlet:actionURL>
		<portlet:actionURL name="saveArtefact" var="save_artefact">
			<portlet:param name="vcpubblication" value="${vcpubblication}"/>
		</portlet:actionURL>
		<portlet:actionURL name="publishArtefact" var="publish_artefact">
			<portlet:param name="vcpubblication" value="${vcpubblication}"/>
		</portlet:actionURL>
		
		<div class="materialize">
			
			<div id="modal-confirm" class="modal">
				<div class="modal-content">
					<h5 class="green-text text-darken-2 center-align">
						<span class="row erroricon hide"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></span>
						<span class="row confirmationicon" id="levelIcon"><img id="artifactLevel" alt="level" src="/MarketplaceStore-portlet/icons/1_level.png"  width="15%"/></span>
						<span class="row confirmationicon hide" id="upIcon"><i class="fa fa-2x fa-thumbs-up" aria-hidden="true"></i></span>
						<span class="row confirmmessage"></span>
					</h5>
				</div>
				<div class="modal-footer">
					<a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-green btn-flat">
						<liferay-ui:message key="ok"/>
					</a>
				</div>
			</div>
			<a href="#modal-confirm" class="hide" id="modal-confirm-trigger"></a>
 
			<div id="modal-level-confirm" class="modal">
				<div class="modal-content">
					<h4 class="center-align">						
						Evaluation
					</h4>			
					<p class="row confirmmessage" style="margin-bottom:0;"></p>
					It will be visible in the marketplace but it does not indicate useful information that allows developers to use it.
					<p>We suggest you to fix the following problems in order to get a Level 3 evaluation:</p>
					<ol id="missing-fields" style="list-style-type: disc;">
						
					</ol>				
					<span style="font-size: 0.8rem;">
								<!--<liferay-ui:message key="schema-example"/> -->(To know more about the 5 star metamodel click
								<a href="/faq/en/user-guide/developer/implementing-a-solution.html#metamodel" target="_blank"><liferay-ui:message key="here"/></a> )
					</span>	
					<p>Are you sure to submit this artefact?</p>
				</div>
				<div class="modal-footer">
					<a href="javascript:fireSubmit();" class="modal-action modal-close waves-effect waves-green btn-flat">
						<liferay-ui:message key="submit"/>
					</a>
					<a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-green btn-flat">
						<liferay-ui:message key="cancel"/>
					</a>
				</div>
			</div>
			<a href="#modal-level-confirm" class="hide" id="modal-level-confirm-trigger"></a>
			 
			<c:if test="${action_return}">
				<script>
					$(document).ready(function(){
						var artid = ${artefattoId};
						var action_button = "";
						var actionreturl = '${actionrethome}';
						var idselector = '<%=request.getParameter("msg") %>';
						var msg = $('#'+idselector).text();
						//--ArtifactLevel--
						
						var level = -1;
						if('${level}'!=null) level= '${level}';
						console.log("[POST-SUBMIT] LIVELLO ARTEFATTO: "+level);
						var anchorLevel = $('#artifactLevel');
						var anchorLevelUrl = "/MarketplaceStore-portlet/icons/"+level+"_level.png";
						//_________________
						var anchor = $('#modal-confirm .modal-footer a');
						var anchorurl = "javascript:void(0);";
						if(!actionreturl){
							action_button = "<liferay-ui:message key='show-artefact'/>";
							anchorurl = '/web/guest/marketplace/-/marketplace/view/'+artid;
						}
						else{
							action_button = "<liferay-ui:message key='ok'/>";
							anchorurl = "/web/guest/marketplace";
						}
						
						anchor.attr('href', anchorurl);
						anchor.text(action_button);
						
						anchorLevel.attr('src', anchorLevelUrl);
						
						if(level<=0) { //If is not a BB
							$("#levelIcon").addClass("hide");
							$("#upIcon").removeClass("hide");
						}
							
						$('#modal-confirm .confirmmessage').text(msg);

						$('#modal-confirm').openModal({ dismissible: false });
					});
				</script>
			</c:if>
			
			<div class="section row valign-wrapper">
				<div class="col s4 m2 valign center">
					<a class="waves-effect btn-flat tooltipped" href="${backurl}" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="back"/>">
						<i class="icon-arrow-left left"></i>
						<liferay-ui:message key="back"/>
					</a>
				</div>
				<div class="hide-on-small-only col m7 valign center"></div>
				<div class="col s4 m3 valign center">
				</div>
			</div>	
			
			<div class="row">
				<div class="col s12 m2">
					<%@include file="sections/leftcolumn.jspf" %>
				</div>
				
				
				<div class="col s12 m7">
					<%@include file="sections/centercolumn.jspf" %>
				</div>
				
				<div class="col s12 m3">
					<%@include file="sections/rightcolumn.jspf" %>
				</div>
			</div>
			
			<div class="divider bottom-margin"></div>
			<div id="mainactions" class="row">
				
				<form method="POST" class="col s4 m3" id="deleteform" action="${delete_artefact}" onsubmit="javascript:return ondeleteartefact(this);">
					<input type="hidden" class="artid" name="<portlet:namespace/>idArtefatto" value="-1"/>
					
					<button type="submit" form="deleteform" class="waves-effect waves-light btn core-color color-1">
						<i class="material-icons left">delete</i>
						<span class="hide-on-small-only"><liferay-ui:message key="cancel"/></span>
						<i class="fa fa-spinner hide fa-spin" aria-hidden="true"></i>
					</button>
				</form>
				
				<form method="POST" class="col s4 m3" id="saveform" action="${save_artefact}" onsubmit="javascript:return ninofun(this);">
					<input type="hidden" class="model" name="<portlet:namespace/>lusdlmodel"/>
					<input type="hidden" class="artid" name="<portlet:namespace/>idArtefatto"/>
					<input type="hidden" class="isbb" name="<portlet:namespace/>isbb" value="${isbb}"/>
					<input type="hidden" class="ispsa" name="<portlet:namespace/>ispsa" value="${ispsa}"/>
					
					<button type="submit" form="saveform" class="waves-effect waves-light btn core-color color-1">
						<i class="material-icons left">save</i>
						<span class="hide-on-med-and-down"><liferay-ui:message key="save"/></span>
						<i class="fa fa-spinner hide fa-spin" aria-hidden="true"></i>
					</button>
				</form>
				
				<form method="POST" class="col s4 m3" id="publishform" action="${publish_artefact}" onsubmit="javascript:return isValid(this);">
					<input type="hidden" class="model" name="<portlet:namespace/>lusdlmodel"/>
					<input type="hidden" class="artid" name="<portlet:namespace/>idArtefatto"/>
					<input type="hidden" class="isbb" name="<portlet:namespace/>isbb" value="${isbb}"/>
					<input type="hidden" class="ispsa" name="<portlet:namespace/>ispsa" value="${ispsa}"/>
					<input type="hidden" class="level" name="<portlet:namespace/>level" value="" />
					<input type="hidden" class="isAdmin" name="<portlet:namespace/>isAdmin" value="${isAdmin}" />
					
					<button type="submit" form="publishform" class="waves-effect waves-light btn core-color color-1" id="publish-click">
						<i class="material-icons left">check</i>
						<span class="hide-on-med-and-down"><liferay-ui:message key="publish"/></span>
						<i class="fa fa-spinner hide fa-spin" aria-hidden="true"></i>
					</button>
				</form>
		  	</div>

			<c:if test="${showmodal}">
				<%@include file="includes/descriptorModal.jspf" %>
			</c:if>
		</div>
		
		<div class="hide errormsgs">
			<div id="notitle" ><liferay-ui:message key="error.no-title"/></div>
			<div id="notype"><liferay-ui:message key="error.no-type"/></div>
			<div id="nolang"><liferay-ui:message key="error.no-lang"/></div>
			<div id="nourl"><liferay-ui:message key="error.no-url"/></div>
			<div id="nooperation" ><liferay-ui:message key="error.no-operation"/></div>
			<div id="nooperationname" ><liferay-ui:message key="error.no-operation-name"/></div>
			<div id="nooperationmethod" ><liferay-ui:message key="error.no-operation-method"/></div>
			<div id="nooperationpath" ><liferay-ui:message key="error.no-operation-path"/></div>
			<div id="nooperationoutput" ><liferay-ui:message key="error.no-operation-output"/></div>
			<div id="noofferingtitle" ><liferay-ui:message key="error.no-offering-title"/></div>
			<div id="deletion-complete"><liferay-ui:message key="deletion-complete"/></div>
			<div id="save-complete"><liferay-ui:message key="save-complete"/></div>
			<div id="publication-complete"><liferay-ui:message key="publication-complete"/></div>
			<div id="artifact-confirm"><liferay-ui:message key="artifact-confirm"/></div>
		</div>
		
		<div class ="hide missingfields">
			<div id="protocol-missing" ><liferay-ui:message key="type"/></div>
			<div id="language-missing" ><liferay-ui:message key="language"/></div>
			<div id="description-missing" ><liferay-ui:message key="details"/></div>
			<div id="provider-missing" ><liferay-ui:message key="provider"/></div>
			<div id="basic-operation-missing" ><liferay-ui:message key="intpoint"/></div>
			<div id="form-data-detected" >Form data parameters are not supported by the Visual Composer</div>
			<div id="input-content-type-incompatible" >The Visual Composer supports only application/json and application/xml body parameter</div>
			<div id="output-content-type-incompatible">The Visual Composer supports only textual responses</div>
		</div>
		
		<input type="hidden" id="portletnamespace" value="<portlet:namespace/>"/>
		<portlet:resourceURL var="resURL"/>
		<script>
			var modal = new Object();
			
			var idArtefatto = ${artefattoId};
			var showmodal = ${showmodal};
			var portletnamespace;
			var obj;
			var isbb = ${isbb};
			var ispsa = ${ispsa};
			var isadmin = '${isAdmin}';
			var ajaxUrl = "${resURL}";
			
			function ondeleteartefact(e){
				var form = $(e);
					form.find('input.artid').val(idArtefatto);
				
				return confirm('<liferay-ui:message key="all-changes-discard"/>');
			}
			
			$(window).resize(function(){
				refreshPushpins();
			});
			
			jQuery(document).ready(function(){
				
				portletnamespace = jQuery('#portletnamespace').val();
				var sDescriptor = '${descriptor}';
				var pilot = '${pilot}';	
				//console.log("DESCRIPTOR: "+sDescriptor);
				$('#modal-confirm-trigger').leanModal({
					dismissable: false
				});
				
				$('#modal-level-confirm-trigger').leanModal({
					dismissable: false
				});
				
				if(isbb){ 	
					console.log("isadmin:"+isadmin);
					if(isadmin=='true') {						
						pilot = $('#metamodel-field-pilot').val();
					}	
					obj = new BuildingBlock('${authorName}', '${currDate}', '${authorMbox}', pilot);
					if(typeof sDescriptor !== 'undefined'){
						var trimmeddescriptor = sDescriptor.replace(/[\s\n\r]+/g, ' ').trim();
						if(trimmeddescriptor != ''){
							var m = JSON.parse(trimmeddescriptor);
							obj = new BuildingBlock('${authorName}', '${currDate}', '${authorMbox}', pilot, m);
							prefill_form(obj);
						}
						
					}
				}
				else if(ispsa){ 
					obj = new PublicServiceApplication('${authorName}', '${currDate}', '${authorMbox}', '${pilot}'); 
					if(typeof sDescriptor !== 'undefined'){
						var trimmeddescriptor = sDescriptor.replace(/[\s\n\r]+/g, ' ').trim();
						if(trimmeddescriptor != ''){
							var m = JSON.parse(sDescriptor);
							obj = new PublicServiceApplication('${authorName}', '${currDate}', '${authorMbox}', '${pilot}', m); 
							prefill_form(obj);
						}
					}
				}
				
				var owner = new Entity();
					owner.businessRole = BusinessRole.OWNER;
					owner.title = $('#metamodel-field-owner').val();
					
				obj.hasBusinessRole.push(owner);
				jQuery('.businessrole-field[data-role="OWNER"]').attr('data-brid', obj.hasBusinessRole.length -1);
				
				if(showmodal){
					$('.modal#descriptorModal').openModal();
				}
			});
			
			$(document).on('input', '.businessrole-field', function(){
				updateBusinessRole($(this));
			});
			
			$(document).on('input', '.pilot-field', function() {
				obj.pilot = $('#metamodel-field-pilot').val();
				console.log("isAdmin value: "+ '${isAdmin}');
			});
			
			$(document).on('blur', 'input.form-field,textarea.form-field', function(){
				handleChange($(this));
			});
			
			$(document).on('change', 'select.form-field', function(){
				handleChange($(this));
			});
			
			$(document).on('click', '.overlayable-wrapper', uploadFileClickHandler);
			
			function oninpointmodalready(){
				
				var modaltrigger = $('.modal-trigger[data-clicked="true"]');
				var modaltriggerid = modaltrigger.attr('href');
				
				if(modaltriggerid == '#modal1'){
					//Shows all the input parameters of the current operation
					$('#body-form').addClass('hide');					
					$('#formdata-enabled').prop('checked', true);
					$('#formdata-form').addClass("hidden");		
					$('#modal1').find('#open-service').prop('checked', false).change();
					
					for(var i in modal.focusedmodel.hasInput){
						var _uid = new Date().getTime();
						modal.focusedmodel.hasInput[i]._uid = _uid;
						var p = modal.focusedmodel.hasInput[i];
						var id_selector = "#"+p._in;
						appendParameter($(id_selector), p);
					}
				}
				
				if(modaltriggerid == '#modal2'){
					
					var produces = modal.focusedmodel.produces;
					
					if(produces.includes("application/octet-stream") && produces.length == 1)
						$('#colSchema').addClass("hide");
					else
						$('#colSchema').removeClass("hide");
					
					//Shows all the output parameters of the current operation					
					for(var i in modal.focusedmodel.hasOutput){
						var _uid = new Date().getTime();
						modal.focusedmodel.hasOutput[i]._uid = _uid;
						var r = modal.focusedmodel.hasOutput[i];
						appendResponse(r);
					}
				}
				
				if(modaltriggerid == '#modal3'){
					//Shows the data related to the security measure
					$('#securitygeneral').find('input, select, textarea').prop('disabled', false);
					$('#securityscopes').find('input[type="checkbox"]').prop('disabled', false);
					
					var security = modal.focusedmodel.hasSecurityMeasure;
					if(security != null && !(typeof security === 'undefined')){
						$('#modal3').find('#open-service').prop('checked', false).change();
						$('#modal3').find('input[name="title"]').val(security.title).change();
						$('#modal3').find('textarea[name="description"]').val(security.description).change();
						$('#modal3').find('select[name="protocolType"]').val(security.protocolType).change();
						
						//The scopes will be populated everytime they are loaded
					} else if(security == null) {
						$('#modal3').find('select[name="protocolType"]').val("NONE").change();
					}
				}
			}
			
			function oninpointmodalclose(){
				for(var index in obj.hasOperation){
					if(obj.hasOperation[index]._uid == modal.focusedmodel._uid){
						obj.hasOperation[index] = modal.focusedmodel;
					}
				}
				
				var modaltrigger = $('.modal-trigger[data-clicked="true"]');
				modaltrigger.removeAttr("data-clicked");
				
				var modaltriggerid = modaltrigger.attr('href');
				
				delete modal;
				modal = new Object();
				
				if(modaltriggerid == '#modal1'){
					clearAllParams(); //Removes all params for future modal visualization
				}
				if(modaltriggerid == '#modal2'){
					clearAllResponses(); //Removes all responses for future modal visualization
				}
				if(modaltriggerid == '#modal3'){
					
					//Clears the "General form"
					$('#modal3').find('input[type="text"], textarea, select').val("").prop('disabled', true);
					$('#modal3').find('#open-service').prop('checked', true);
					
					clearScopes(); //Removes all scopes for future modal visualization
				}
			}
			
			function prefill_title(model){
				//Prefill title
				jQuery('#metamodel-field-title').val(model.title);
			}
			
			function prefill_businessroles(model){
				//Prefill provider and owner
				for(var i in model.hasBusinessRole){
					try{
						var role = model.hasBusinessRole[i].businessRole.toUpperCase();
						jQuery('#general').find('input[data-role="'+role+'"]').val(model.hasBusinessRole[i].title);
						jQuery('.businessrole-field[data-role="'+role+'"]').attr('data-brid', i);
					}
					catch(error){}
				}
			}
			
			function prefill_owner(model) {
				
			}
			
			function prefill_type(model){
				//Prefill type
				var cat_tmp = "";
				try{
					if(typeof Type[model.type.toUpperCase()] === 'undefined'){ cat_tmp = model.type; }
					else{ cat_tmp = Type[model.type.toUpperCase()]; }
				}
				catch(error){}
				jQuery('#metamodel-field-type').val(cat_tmp).change();
			}
			
			function prefill_lang(model){
				//Prefill type
				var lang_tmp = "";
				try{
					if(typeof model.language === 'undefined' || model.language == null || model.language.trim().length <= 0){
						lang_tmp = "";
					}
					else{ 
						lang_tmp = model.language;
						jQuery('#metamodel-field-language').val(lang_tmp).change();
					}
				}
				catch(error){}
			}
			
			function prefill_docs(model){
				//Prefill docs
				jQuery('#metamodel-field-page').val(model.page);
			}
			
			function prefill_url(model){
				//Prefill docs
				jQuery('#metamodel-field-url').val(model.url);
			}
			
			function prefill_tags(model){
				//Prefill tags
				for(var t in model.tags){
					addTag(model.tags[t], false);
				}
			}
			
			function prefill_description(model){
				//Prefill description
				jQuery('#textarea1').text(model.description);
			}
			
			function prefill_consumes(opdom,currop){
				var consumes = currop.consumes;
				//SE IL CONSUMES NON RIENTRA TRA QUELLI PIU' USATI
				if(consumes != "" &&
				   consumes != "application/json" &&
				   consumes != "application/xml" &&
				   consumes != "multipart/form-data" &&
				   consumes != "application/x-www-form-urlencoded"  ) {
					
						opdom.find('.opconsumes').val("other").trigger("change");
						opdom.find('.other-consumes').val(consumes).trigger("change");
				} else {
					opdom.find('.opconsumes').val(consumes);
				}
			}
			
			function prefill_produces(opdom,currop){
				var produces = currop.produces;
				var finalProduces = [];
				var otherP;
				var p;
				for(var i in produces) {
					p = produces[i];
					console.log(":::::::::"+p);
					//SE IL CONSUMES NON RIENTRA TRA QUELLI PIU' USATI
					if(		p != "application/json" &&
							p != "application/xml" &&
							p != "application/octet-stream" &&
							p != "text"  ) {
																									
								if (finalProduces.indexOf("other") == -1) {
									finalProduces.push("other");
								}
								
								otherP = opdom.find('.other-produces').val().trim();
								if(otherP.length > 0) {
									otherP = otherP + ';'+p.trim();
									opdom.find('.other-produces').val(otherP); 
								}
								else opdom.find('.other-produces').val(p);
								
								opdom.find('.other-produces').trigger("change");
								
					} else {
						if (finalProduces.indexOf(p) == -1) {
							finalProduces.push(p);
						}
					}
				}
				if(typeof finalProduces !== 'undefined' && finalProduces.length > 0){
					/*console.log(":::PRODUCES:::"+finalProduces);*/
					opdom.find('.opproduces').val(finalProduces).trigger("change",[true]);	
				}
				
			}
			
			function prefill_interactionpoints(model){
				//Prefill Interaction Points
				var interactionpoints = jQuery('#intpoint');
				for(var op in model.hasOperation) {
					var _uid = new Date().getTime();
					var currop = model.hasOperation[op];
						currop["_uid"] = _uid;
					console.log("CURRENT OPERATION: "+JSON.stringify(currop));
					var opdom = addentry(interactionpoints, oninpointmodalready, oninpointmodalclose);
						opdom.attr('data-itemid', _uid);
						opdom.find('.oppath').val(currop.endpoint);
						opdom.find('.optitle').val(currop.title);
						opdom.find('.opdescription').text(currop.description).trigger("autoresize");
						prefill_produces(opdom,currop);
						//opdom.find('.opproduces').val(currop.produces);
						prefill_consumes(opdom,currop);
						//opdom.find('.opconsumes').val(currop.consumes);
						opdom.find('.opmethod').val(currop.method).change();
						opdom.find('.ninputs').text(currop.hasInput.length);
						opdom.find('.noutputs').text(currop.hasOutput.length);
					
						//Normalize schemas
						//outputs
						for(var j in currop.hasOutput){
							var curroutput = currop.hasOutput[j];
							//Escapes quotes, removes new line, carriage return and multiple spaces
							curroutput.schema = curroutput.schema.replace(new RegExp('"', 'g'), '\"')
																	.replace(/[\n\r\s]+/g, ' ')
																	.trim();
						}
						
						//inputs
						for(var j in currop.hasInput){
							var currinput = currop.hasInput[j];
							if(currinput._in == 'body'){
								//Escapes quotes, removes new line, carriage return and multiple spaces
								currinput.schema = currinput.schema.replace(new RegExp('"', 'g'), '\"')
																		.replace(/[\n\r\s]+/g, ' ')
																		.trim();
							}
						}
				}
			}
			
			function prefill_offerings(model){
				//Prefill offerings
				for(var i in model.hasServiceOffering){
					
					var _uid = new Date().getTime();
					var curr = model.hasServiceOffering[i];
						curr["_uid"] = _uid;
					
					var offentry = addentry(jQuery('#offering'));
						offentry.attr('data-itemid', _uid);
						
					offentry.find('input[name="title"]').val(curr.title);
					offentry.find('textarea[name="description"]').text(curr.description).trigger('autoresize');
					offentry.find('input[name="url"]').val(curr.url);
					
				}
			}
			
			function prefill_licenses(model){
				//Prefill licenses
				for(var i in model.hasLegalCondition){
					
					var _uid = new Date().getTime(); 
					var curr = model.hasLegalCondition[i];
						curr["_uid"] = _uid;
					
					var licentry = addentry(jQuery('#license'));
						licentry.attr('data-itemid', _uid);
					
					var select = licentry.find("select");
					var license_exists = licentry.find('option[value="'+curr.title.toUpperCase()+'"]').length > 0;
					
					if(license_exists){ select.val(curr.title).change(); }
					else{ select.val("OTHER").change(); }
				
				}
			}
			
			function prefill_dependencies(model){
				//Prefill dependencies
				var tmp = new Array();
				for(var i in model.uses){
					tmp.push(model.uses[i].id);
				}
				
				delete model.uses;
				model.uses = new Array();
				
				var postdata = new Object();
					postdata['action'] = '<%=AjaxActions.action.GET_DEPENDENCY_DATA%>';
					postdata['dependencyIDs'] = tmp.join(",");
				
				jQuery.post(ajaxUrl, postdata)
						.done(function(resp){
							console.log(resp);
							var jResp = JSON.parse(resp);
							console.log(jResp);
							
							for(var c in jResp.artefacts){
								addDependency(jResp.artefacts[c]);
							}
							
						});
			}
			
			function prefill_form(model){
				
				prefill_title(model);
				
				prefill_businessroles(model);
				
				prefill_type(model);
				
				prefill_lang(model);
				
				prefill_docs(model);
				
				prefill_tags(model);
				
				prefill_description(model);
				
				prefill_offerings(model);
				
				prefill_licenses(model);
				
				prefill_dependencies(model);
				
				if(ispsa)
					prefill_url(model);
				else if(isbb)
					prefill_interactionpoints(model);
			}
			
			function appendscopes(scopes, appender){
				
				var t = $('#templatecontainer').find('.scope-template');
				
				
				if(scopes==null || typeof scopes === 'undefined' || scopes.length == 0){
					var item = t.clone();
					if(appender.closest('.modal').find('#open-service').is(':checked')){
						item.find('input[type="checkbox"]').prop('disabled', true);
					}
					item.removeClass('scope-template hide');
					item.html('<em>'+
								'<i class="fa fa-info-circle" aria-hidden="true"></i>'+
								'<liferay-ui:message key="no-scope-here"/>'+
								'</em>');
					appender.append(item);
					return;
				}
				
				for(var j in scopes){
					var s = scopes[j];
					
					var item = t.clone();
					if(appender.closest('.modal').find('#open-service').is(':checked')){
						item.find('input[type="checkbox"]').prop('disabled', true);
					}
					item.removeClass('scope-template hide');
					item.find('.scope-id').text(s.id);
					item.find('.scope-desc').text(s.description);
						
					var checkbox = item.find('input[type="checkbox"]');
					checkbox.attr('data-accesstype', s.access_type);
					checkbox.val(s.id);
					
					//Setup checkbox and label ID
					//Necessary only for materialize theme
					var editedId = s.id.replace(/\./gi, "");
					var checkid = checkbox.attr('id');
					checkbox.attr('id', editedId);
					checkbox.parent().find('label').attr('for', editedId);
					checkbox.parent().find('label').attr('data-tooltip', s.id);
						
					appender.append(item);
				}
			}
			
			function initmodals(e, onready, onclose){
				var trigger = e.find('.modal-trigger');
				
				trigger.each(function(i,x){
					
					trigger.leanModal({
						dismissible: false,
						in_duration: 900, // Transition in duration
						starting_top: '4%', // Starting top style attribute
						ending_top: '10%', // Ending top style attribute
						ready: function() {
							var t = e.find('.modal-trigger[data-clicked="true"]');
							var modelset = e.attr('data-modelset');
							var itemid = e.attr('data-itemid');
							for(var i in obj[modelset]){
								if(obj[modelset][i]._uid == itemid){
									modal.focusedmodel = obj[modelset][i];
								}
							}
							
							var modalid = t.attr('href');
							$(".modal-title .param_opname").text(modal.focusedmodel.title);
							
							//if the modal has tabs
							var tabs = $(modalid+' ul.tabs');
							if(tabs.length > 0){
								//Initialize tabs
								var selectedtab = tabs.find('li').first().find('a').attr('href').substring(1);
								$(modalid+' ul.tabs').tabs('select_tab', selectedtab);
							}
							
							//Initialize select 
							$(modalid+' select').material_select();
							
							if(modalid == "#modal3"){
								//If it is the Security profiles modal, it loads asyncronously the Scopes list  
								$.post(ajaxUrl, {
									'action':'<%=AjaxActions.action.GETSCOPES%>'
								})
								.done(function(resp){
									var appender = $(modalid+" .scopelist");
									
									var jResp = JSON.parse(resp);
									intpoint.g_permissions = jResp;
									
									var permissions = jResp.permissions;
		
									var t1 = $('#templatecontainer').find('.scope-area-template');
									
									for(var i in permissions){
										var c = permissions[i];
										var item = t1.clone();
										item.removeClass('scope-area-template hide');
										item.find('.collapsible-header').html("<b>"+c.name+"</b> - <em>"+c.description+"</em>");
										item.attr('data-permid', i);
										
										appender.append(item);
									}
									
								});
							}
							
							if(typeof onready !== "undefined")
								onready.call(this);
							
						}, // Callback for Modal open
						complete: onclose // Callback for Modal close
					});
				});
			}
			
			function inittooltips(e){
				var tooltipped = e.find('.tooltipped');
				if(tooltipped.length>0){
					tooltipped.tooltip();
				}
			}
			
			function initselects(e){
				e.find('select').material_select();
			}
			
			function handleChange(ap){
				var trigger = ap.closest('.f-edit-wrapper').find('.f-edit-trigger');
				var val = ap.val().trim();
				
				obj[ap.attr('name')]=val;
				trigger.text(val);
				
				if(trigger.length && trigger.text().length>0){
					ap.toggleClass("hide");
					trigger.toggleClass("hide");
				}
				return true;	
			}
		</script>
	</c:otherwise>
</c:choose>
<script>
$(document).ready(function(){
	/* Disable breadcrumb to avoid page loading resulting in parameter loss and JS error */
	disableLastBreadcrumb();			
});
</script>	