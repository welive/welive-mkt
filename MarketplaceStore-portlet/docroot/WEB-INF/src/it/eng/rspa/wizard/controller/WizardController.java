package it.eng.rspa.wizard.controller;

import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.UserCdv;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.service.LanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserCdvLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.wizard.WizardUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletMode;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class WizardController {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static Log _log = LogFactoryUtil.getLog(WizardController.class);
	
	private static void forwardToView(HttpServletRequest request, Map<String, Object> params){
		Set<Map.Entry<String, Object>> entries = params.entrySet();
		for(Map.Entry<String, Object> param : entries){
			request.setAttribute(param.getKey(), param.getValue());
		}
		return;
	}
	
	@SuppressWarnings("unchecked")
	public static void action(HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> params = new HashMap<String, Object>();
							params.put("artefattoId", -1L);
							
		ThemeDisplay td = WizardUtils.getThemeDisplay(request);
		
		String now = sdf.format(GregorianCalendar.getInstance().getTime());
		String cats = request.getParameter("catsOfType");
		String categories = "";
		boolean isbb = false;
		boolean ispsa = false;
		boolean isAdmin = false;
		
		if(Validator.isNotNull(cats)){
			categories = PortletProps.get("Category.catsOfType."+cats);
			isbb = cats.equalsIgnoreCase("bblocks");
			ispsa = cats.equalsIgnoreCase("psa");
		}
		
		long artid = -1L;
		boolean action_return = false;
		
		Object actionreturn = request.getAttribute("action-return");
		String rpk = request.getParameter("resourcePrimaryKey");
		
		if(Validator.isNotNull(actionreturn)){
			action_return = (Boolean)actionreturn;
			artid = (Long)request.getAttribute("action-return-id");
		}
		else if(Validator.isNotNull(rpk)){
			artid = Long.parseLong(rpk);
		}
		params.put("action_return", action_return);
		
		if(Validator.isNotNull(request.getAttribute("actionrethome"))){
			params.put("actionrethome", true);
		}
		
		if(artid > 0){
			try{
				Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(artid);
				String lusdlmodel = a.getLusdlmodel();
				
				//Lato client, in javascript, utilizzo gli apici singoli per delimitare la stringa json,
				//pertanto devo fare l'escape degli apici interni alla stringa stessa.
				lusdlmodel = lusdlmodel.replace("\'", "\\\'")//Faccio l'escape degli apostrofi
									   .replace("\"", "\\\"") //Faccio l'escape delle virgolette
										.replace("\\n", " "); //tolgo gli a capo che danno errore JS sul JSON
				
				params.put("artefattoId", artid);
				params.put("descriptor", lusdlmodel);
				params.put("uploadedDescriptor", true);
				
				JSONArray images = MyMarketplaceUtility.getArtefactImagesDetails(artid, td);
				params.put("imagesArray", images);
				
			}
			catch(Exception e){
				params.put("notfound", true);
				params.put("error-message", e.getClass().getSimpleName());
				forwardToView(request, params);
				return;
			}
		}
		else if(Validator.isNotNull(request.getAttribute("descriptor"))){
			String descriptor = (String) request.getAttribute("descriptor");
			if(Validator.isNotNull(descriptor)){
				descriptor = descriptor.replace("'", "\'");//Faccio l'escape degli apostrofi
				
				params.put("descriptor", descriptor);
			}
		}
		
		boolean uploadedDescriptor = false;
		Object sud = request.getAttribute("uploadedDescriptor");
		if(Validator.isNotNull(sud)){
			uploadedDescriptor = (Boolean)sud;
		}
		String backurl = "javascript:window.history.back();";
		
		try{
			String marketplacePortletId = ConfUtil.getString("marketplacePortletId");
			long mkt_plid = PortalUtil.getPlidFromPortletId(td.getLayout().getGroupId(), td.getLayout().isPrivateLayout(), marketplacePortletId);
			PortletURL mkt_home = PortletURLFactoryUtil.create(request, marketplacePortletId, mkt_plid, PortletRequest.RENDER_PHASE);
			mkt_home.setWindowState(WindowState.MAXIMIZED);
			mkt_home.setPortletMode(PortletMode.VIEW);
			mkt_home.setParameter("jspPage", "/html/marketplacestore/view.jsp");

			backurl = mkt_home.toString();
		}
		catch(Exception e){
			_log.warn("Error getting backurl: "+e.getMessage());
		}
		
		User portalUser = td.getUser();
		long uid = portalUser.getUserId();
		long ccuid = Long.parseLong(portalUser.getExpandoBridge().getAttribute("CCUserID").toString());
		
		
		params.put("backurl", backurl);
		params.put("cats", cats);
		params.put("isbb", isbb);
		params.put("ispsa", ispsa);
		params.put("uploadedDescriptor", uploadedDescriptor);
		params.put("showmodal",  isbb && !uploadedDescriptor && !action_return && artid<=0);
		
		try{ 
			//Recupero i dati dal CDV
			UserCdv user = UserCdvLocalServiceUtil.getUserCdvByPK(ccuid, uid);
			if(Validator.isNull(user)){
				throw new Exception("User ("+ccuid+", "+uid+") doesn't exist in CDV.");
			}
			
			List<UserLanguage> uls = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(uid);
			
			Set<String> langsname = new HashSet<String>();
			for(UserLanguage ul : uls){
				try{
					long langid = ul.getLanguageId();
					Language l = LanguageLocalServiceUtil.getLanguage(langid);
					langsname.add(l.getName());
				}
				catch(Exception e){
					_log.warn("Error getting language: "+e);
				}
			}
			String langs = StringUtils.join(langsname, ",");
			
			params.put("authorName", user.getName()+" "+user.getSurname());
			params.put("authorMbox", user.getEmail());
			params.put("pilot", user.getPilot());
			params.put("langs", langs);
			
		}
		catch(Exception e){
			//Se avviene qualche errore recupero i dati dal portale
			_log.warn("Error getting the user information: " + e);
			
			params.put("authorName", portalUser.getFullName());
			params.put("authorMbox", portalUser.getEmailAddress());
			
			String langs = "english";
			String pilotId = "";
			String[] pilots = ((String[])portalUser.getExpandoBridge().getAttribute("pilot"));
			if(pilots!=null && pilots.length > 0) pilotId = pilots[0];
			params.put("pilot", pilotId);
			
			String pilotLangs = ConfUtil.getString("lang.default."+pilotId.toLowerCase());
			langs =  langs+ ","+pilotLangs;
			params.put("langs", langs);
			
			
		}
		
		params.put("currDate", now);
		params.put("categories", categories);
		
		List<String> warns = (List<String>) request.getAttribute("descriptor-warnings"); 
		if(Validator.isNotNull(warns)){
			_log.warn(warns.size()+" warnings found");
			params.put("warnings", warns);
		}
		
		User u = null;
		try{
			u = UserLocalServiceUtil.getUser(uid);
			List<Organization> companies = new ArrayList<Organization>();
			for(Organization org : u.getOrganizations()){
				Boolean isCompany = Boolean.parseBoolean(org.getExpandoBridge().getAttribute("isCompany").toString()); 
				if(isCompany){ 
					companies.add(org); 
				}
			}
			
			params.put("orgs", companies);
		}
		catch(Exception e){ 
			_log.error(e);
		}
		
		try{
			if(u!=null){
				Role rLeader = RoleLocalServiceUtil.getRole(u.getCompanyId(), PortletProps.get("role.companyleader"));
				params.put("isLeader", u.getRoles().contains(rLeader));
			}
			else{ params.put("isLeader", false); }
		}
		catch(Exception e){ 
			e.printStackTrace();
		}
				
		try{
			User currentUser = null;
			try{ currentUser = PortalUtil.getUser(request);}
			catch(Exception e){ _log .warn(e); }
			Role admin = RoleLocalServiceUtil.getRole(PortalUtil.getCompanyId(request), PortletProps.get("role.admin"));
			isAdmin = currentUser.getRoles().contains(admin);
			
			params.put("isAdmin", isAdmin);
		}
		catch(Exception e){ e.printStackTrace(); }
		
		//Pilots
		List<String> pilots = new ArrayList<String>();
		pilots.add("Trento");
		pilots.add("Bilbao");
		pilots.add("Novi Sad");
		pilots.add("Region on Uusimaa-Helsinki");
		pilots.add("ALL");
		params.put("pilots", pilots);
		//_______________+
		
		
		forwardToView(request, params);
	}
	
}
