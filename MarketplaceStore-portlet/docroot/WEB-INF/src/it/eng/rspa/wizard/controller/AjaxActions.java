package it.eng.rspa.wizard.controller;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import javax.portlet.ResourceRequest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.wizard.model.DependencySearchItem;

public abstract class AjaxActions {
	
	public static enum action{GETSCOPES, SEARCH_DEPENDENCY, GET_DEPENDENCY_DATA, UPLOAD_COVER, UPLOAD_GALLERY_ITEM, REMOVE_GALLERY_ITEM};
	
	private static String getArtefactImagePath(ResourceRequest request, Artefatto artefatto){
		
		String baseURl = request.getContextPath();
		try {
			String catname = "Other";
			try{ catname = CategoriaLocalServiceUtil.getCategoria(artefatto.getCategoriamkpId()).getNomeCategoria(); }
			catch(Exception e) {}
			
			if(catname.equalsIgnoreCase(PortletProps.get("Category.rest")))
				baseURl += "/icons/icone_servizi_rest-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.soap")))
				baseURl += "/icons/icone_servizi_soap-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.dataset")))
				baseURl += "/icons/icone_dataset-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appmobile")))
				baseURl += "/icons/icone_app_android-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appweb")))
				baseURl += "/icons/icone_app_web-01.png";
			else
				baseURl += "/icons/defaultIcon.png";
			
			ImmagineArt coverimg = null;
			DLFileEntry dlf = null;
		
			if(artefatto.getImgId()>0){
				coverimg=ImmagineArtLocalServiceUtil.getImmagineArt(artefatto.getImgId()); 
			}
			else if (ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).size() > 0) { 
				coverimg = ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).get(0);
			}
		
			if(coverimg!=null){
				dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(coverimg.getDlImageId());
				ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
				String remoteInterface = themeDisplay.getPortalURL() + themeDisplay.getPathContext();
				String pathDocumentLibrary = StringPool.SLASH + "documents" + StringPool.SLASH + themeDisplay.getScopeGroupId() + StringPool.SLASH;
				
				baseURl = remoteInterface + pathDocumentLibrary + dlf.getUuid();
			}
		}
		catch (Exception e) { e.printStackTrace(); }
		
		return baseURl;
	}
	
	public static JSONObject searchDependency(ResourceRequest request){
		Gson gson = new Gson();
		
		//The search keyword
		String keyword = request.getParameter("keyword");
		
		//The stringified list of artefact id to be excluded because already associated
		String sExclude = request.getParameter("exclude");
		
		//Convert the stringified list into a java collection through Gson
		Type collectionType = new TypeToken<Collection<String>>(){}.getType();
		Collection<String> jExclude = gson.fromJson(sExclude, collectionType);
		
		//Get all artefacts that match the search keyword
		List<Artefatto> arts = ArtefattoLocalServiceUtil.getArtefattiByKeyword(keyword);
		
		JSONObject resp = JSONFactoryUtil.createJSONObject();
		JSONArray artefacts = JSONFactoryUtil.createJSONArray();
		
		for(Artefatto a:arts){ 
			if(!jExclude.contains(String.valueOf(a.getArtefattoId()))){
				try{
					String typeName = "Other";
					try{typeName = CategoriaLocalServiceUtil.getCategoria(a.getCategoriamkpId()).getNomeCategoria();}
					catch(Exception e){}
					DependencySearchItem item = new DependencySearchItem();
									item.setArtefactId(a.getArtefattoId());
									item.setName(a.getTitle());
									item.setType(typeName);
									item.setLinkImage(getArtefactImagePath(request, a));
					
					JSONObject jItem = JSONFactoryUtil.createJSONObject(gson.toJson(item));
					artefacts.put(jItem);
				}
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		
		resp.put("artefacts", artefacts);		
		return resp;
	}
	
	public static JSONObject getDependencyData(ResourceRequest request, String[] in){
		Gson gson = new Gson();
		
		JSONObject resp = JSONFactoryUtil.createJSONObject();
		JSONArray artefacts = JSONFactoryUtil.createJSONArray();
		
		for(String currid : in){
			System.out.println("\t"+currid);
			try{
				Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(Long.valueOf(currid));
				String typeName = "Other";
				try{typeName = CategoriaLocalServiceUtil.getCategoria(a.getCategoriamkpId()).getNomeCategoria();}
				catch(Exception e){}
				DependencySearchItem item = new DependencySearchItem();
								item.setArtefactId(a.getArtefattoId());
								item.setName(a.getTitle());
								item.setType(typeName);
								item.setLinkImage(getArtefactImagePath(request, a));
				
				JSONObject jItem = JSONFactoryUtil.createJSONObject(gson.toJson(item));
				artefacts.put(jItem);
			}
			catch(Exception e){
				System.out.println(e);
			}
		}
		
		resp.put("artefacts", artefacts);		
		return resp;
		
	}

}
