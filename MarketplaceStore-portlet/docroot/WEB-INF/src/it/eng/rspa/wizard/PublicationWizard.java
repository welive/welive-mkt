package it.eng.rspa.wizard;

import it.eng.metamodel.definitions.Artefact;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.utils.RestUtils;
import it.eng.rspa.wizard.controller.AjaxActions;
import it.eng.rspa.wizard.utils.ArtifactEvaluationUtils;
import it.eng.rspa.wizard.utils.ArtifactValutationUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PublicationWizard
 */

public class PublicationWizard extends MVCPortlet {
	
	public void uploadDescriptor(ActionRequest actionRequest, ActionResponse actionResponse){
		
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		String cats = ParamUtil.getString(uploadPortletRequest, "cats");
		String hasDescriptor = ParamUtil.getString(uploadPortletRequest, "hasdescriptor");
		String type = ParamUtil.getString(uploadPortletRequest, "descriptorType");
		
		if(!cats.equalsIgnoreCase("bblocks") || hasDescriptor.equalsIgnoreCase("no") || Validator.isNull(type)){
			actionResponse.setRenderParameter("catsOfType", cats);
			return;
		}
		
		String urlDescriptor = ParamUtil.getString(uploadPortletRequest, "descriptorURL");
		File fDescriptor = uploadPortletRequest.getFile("descriptorfile");
		
		String descriptor = "";
		if(Validator.isNotNull(fDescriptor) && fDescriptor.exists() && fDescriptor.isFile()){
			try { descriptor = IOUtils.toString(new FileInputStream(fDescriptor), "UTF-8"); } 
			catch (Exception e) { System.out.println("Error: "+e.getMessage()); }	
		}
		else if(Validator.isNotNull(urlDescriptor)){
			urlDescriptor = urlDescriptor.trim();
			try { descriptor = WizardUtils.getRemoteFileString(urlDescriptor); }
			catch(Exception e){ System.out.println("Error. "+e.getMessage()); }
		}
		
		try{
			
			ParserResponse response = MetamodelParser.parse(descriptor, MetamodelParser.SourceType.valueOf(type.toUpperCase()));
			
			System.out.println("Errors:");
			for(String err : response.getErrors()){
				System.out.println("\t"+err);
			}
	
			System.out.println("Warnings:");
			for(String warn : response.getWarnings()){
				System.out.println("\t"+warn);
			}
			
			BuildingBlock bb = response.getBuildingBlock();
			
			if(Validator.isNotNull(bb)){
				
				String jBBmetamodel = bb.writeAsJsonString();
				
				//Lato client, in javascript, utilizzo gli apici singoli per delimitare la stringa json,
				//pertanto devo fare l'escape degli apici interni alla stringa stessa.
				jBBmetamodel = jBBmetamodel
									.replace("\\", "\\\\") //Faccio l'escape dei backslash di escape
									.replace("'", "\\'"); //Faccio l'escape degli apostrofi
				
				actionRequest.setAttribute("uploadedDescriptor", true);
				actionRequest.setAttribute("descriptor", jBBmetamodel);
				actionRequest.setAttribute("descriptor-warnings", response.getWarnings());
			}
			else{ 
				String msg = response.getErrors().get(0);
				throw new Exception(msg);
			}
		}
		catch(Exception e){
			SessionErrors.add(actionRequest, "invalid-descriptor");
			String msg = e.getMessage();
			actionResponse.setRenderParameter("invalid-descriptor-error-msg", msg);
		}
		
		actionResponse.setRenderParameter("catsOfType", cats);
	}
 
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) 
			throws IOException, PortletException {
		
		
		UploadPortletRequest uploadrequest = PortalUtil.getUploadPortletRequest(resourceRequest);
		AjaxActions.action action = AjaxActions.action.valueOf(uploadrequest.getParameter("action"));
		JSONObject resp = null;
		switch(action){
			case GETSCOPES:
				try{
					String s = RestUtils.consumeGet(ConfUtil.getString("aac.permission.url"));
					resourceResponse.getWriter().write(s);
				}
				catch(Exception e){
					System.out.println("ERRORE AJAX");
					resourceResponse.getWriter().write(e.getMessage());
				}
				break;
			
			case SEARCH_DEPENDENCY:
				resp = AjaxActions.searchDependency(resourceRequest);
				resourceResponse.getWriter().write(resp.toString());
				break;
			
			case UPLOAD_COVER:
				try{
					File file = uploadrequest.getFile("cover"); 
					
					if(Validator.isNotNull(file) && file.isFile() && file.exists() && file.canRead()){
						User user =  PortalUtil.getUser(uploadrequest);
						if (user==null){
							System.out.println("User not logged in");
							throw new Exception("User not logged in");
						}
						
						Artefatto a = WizardUtils.trovaCreaArtefatto(uploadrequest);
						DLFileEntry dlFileEntry = WizardUtils.storeArtefactImage(uploadrequest, a, file, true);
						String imgPath = WizardUtils.getDocumentLibraryBaseUrl(uploadrequest) + dlFileEntry.getUuid();
						
						resp = JSONFactoryUtil.createJSONObject();
						resp.put("artefactid", a.getArtefattoId());
						resp.put("imagepath", imgPath);
						
						resourceResponse.getWriter().write(resp.toString());
					}
					else{
						long artId = Long.parseLong(uploadrequest.getParameter("idArtefatto"));
						if(artId != -1L){
							Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(artId);
							if(Validator.isNotNull(a)){
								long imgid = a.getImgId();
								if(imgid != -1L){
									ImmagineArt ia = ImmagineArtLocalServiceUtil.getImmagineArt(imgid);
									DLFileEntryLocalServiceUtil.deleteDLFileEntry(ia.getDlImageId());
									ImmagineArtLocalServiceUtil.deleteImmagineArt(ia);
									
									a.setImgId(-1L);
									ArtefattoLocalServiceUtil.updateArtefatto(a);
									
									resp = JSONFactoryUtil.createJSONObject();
									resp.put("artefactid", a.getArtefattoId());
									resp.put("imagepath", uploadrequest.getParameter("defaultimage"));
									
									resourceResponse.getWriter().write(resp.toString());
								}
							}
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					System.out.println("Error. "+e.getMessage());
				}
				break;
				
			case UPLOAD_GALLERY_ITEM:
				try{
					File file = uploadrequest.getFile("img"); 
					
					if(Validator.isNotNull(file)){
						User user =  PortalUtil.getUser(uploadrequest);
						if (user==null){
							System.out.println("User not logged in");
							throw new Exception("User not logged in");
						}
						
						Artefatto a = WizardUtils.trovaCreaArtefatto(uploadrequest);
						DLFileEntry dlFileEntry = WizardUtils.storeArtefactImage(uploadrequest, a, file, false);
						String imgPath = WizardUtils.getDocumentLibraryBaseUrl(uploadrequest) + dlFileEntry.getUuid();
						
						resp = JSONFactoryUtil.createJSONObject();
						resp.put("artefactid", a.getArtefattoId());
						resp.put("itemid", dlFileEntry.getFileEntryId());
						resp.put("itemurl", imgPath);
						
						resourceResponse.getWriter().write(resp.toString());
					}
				}
				catch(Exception e){
					System.out.println("Error. "+e.getMessage());
				}
				break;
				
			case GET_DEPENDENCY_DATA:
//				System.out.println("Getting dependency data");
//				WizardUtils.dumpRequest(uploadrequest);
				String tmp = ParamUtil.getString(uploadrequest, "dependencyIDs");
				JSONObject r = null;
				
				if(Validator.isNotNull(tmp)){
					String[] ids = tmp.split(",");
					r = AjaxActions.getDependencyData(resourceRequest, ids);
				}
				else{
					r = JSONFactoryUtil.createJSONObject();
					r.put("artefacts", JSONFactoryUtil.createJSONArray());
				}
				
				resourceResponse.getWriter().write(r.toString());
				break;
			
			case REMOVE_GALLERY_ITEM:
				try{
					long imgid = Long.parseLong(uploadrequest.getParameter("imgid"));
					long artid = Long.parseLong(uploadrequest.getParameter("idArtefatto"));
					
					DLFileEntryLocalServiceUtil.deleteDLFileEntry(imgid);
					List<ImmagineArt> arts = ImmagineArtLocalServiceUtil.findByArtefattoId(artid);
					for(ImmagineArt art : arts){
						if(art.getDlImageId() == imgid){
							ImmagineArtLocalServiceUtil.deleteImmagineArt(art);
							break;
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
				break;
			
			default:
				System.out.println("No action defined for "+action);
				break;
		}
		
	}
	
	public void deleteArtefact(ActionRequest actionRequest, ActionResponse actionResponse){
		
		System.out.println("deleting artefact");
//		WizardUtils.dumpRequest(PortalUtil.getUploadPortletRequest(actionRequest));
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		Long artefattoid = ParamUtil.getLong(uploadPortletRequest, "idArtefatto");
		if(artefattoid != -1L){
			try{ 
				ArtefattoLocalServiceUtil.deleteArtefatto(artefattoid); 
				ArtifactLevelsLocalServiceUtil.deleteArtifactLevels(artefattoid);			
			}
			catch(Exception e){ e.printStackTrace(); }
			
		}
		
		actionRequest.setAttribute("action-return", true);
		actionRequest.setAttribute("action-return-id", -1L);
		actionRequest.setAttribute("actionrethome", true);
		actionResponse.setRenderParameter("msg", "deletion-complete");
		
		return;
	}
	
	public void saveArtefact(ActionRequest actionRequest, ActionResponse actionResponse) {
//		WizardUtils.dumpRequest(PortalUtil.getUploadPortletRequest(actionRequest));
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		String lusdlmodel = ParamUtil.getString(uploadPortletRequest, "lusdlmodel");
		Long artefattoid = ParamUtil.getLong(uploadPortletRequest, "idArtefatto");
		
		ParserResponse response = null;
		try {
			response = MetamodelParser.parse(lusdlmodel, MetamodelParser.SourceType.METAMODEL);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Artefact lusdlArt = null;
		Class<?> lusdlclass = null;
		if(ParamUtil.getBoolean(uploadPortletRequest, "isbb")){
			lusdlclass = BuildingBlock.class;
			lusdlArt = response.getBuildingBlock();
			
			System.out.println("Warnings:");
			for(String warn : response.getWarnings()){
				System.out.println("\t"+warn);
			}
			
			if(lusdlArt==null){
				System.out.println("Errors:");
				for(String err : response.getErrors()){
					System.out.println("\t"+err);
				}
			}
			
		}
		if(ParamUtil.getBoolean(uploadPortletRequest, "ispsa")){
			lusdlclass = PublicServiceApplication.class;
			try{ lusdlArt = MetamodelParser.parsePSA(lusdlmodel); }
			catch(Exception e){ e.printStackTrace(); }
		}

		if(lusdlArt!=null){
			try{
				User u = PortalUtil.getUser(uploadPortletRequest);
				Artefatto art_tmp = WizardUtils.setupArtefact(artefattoid, lusdlArt, lusdlclass, u, -1L);
				artefattoid = art_tmp.getArtefattoId();
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		actionRequest.setAttribute("action-return", true);
		actionRequest.setAttribute("action-return-id", artefattoid);
		actionRequest.setAttribute("descriptor", lusdlmodel);
		actionRequest.setAttribute("uploadedDescriptor", true);
		actionResponse.setRenderParameter("msg", "save-complete");
		return;
	}
	
	public void publishArtefact(ActionRequest actionRequest, ActionResponse actionResponse) {
//		WizardUtils.dumpRequest(PortalUtil.getUploadPortletRequest(actionRequest));
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		String lusdlmodel = ParamUtil.getString(uploadPortletRequest, "lusdlmodel");
		Long artefattoid = ParamUtil.getLong(uploadPortletRequest, "idArtefatto");
		int artifactLevel = -1;
		System.out.println("--lusdlmodel--"+lusdlmodel+"---FINE_--");
		ParserResponse response = null;
		try {
			response = MetamodelParser.parse(lusdlmodel, MetamodelParser.SourceType.METAMODEL);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Artefact lusdlArt = null;
		Class<?> lusdlclass = null;
		if(ParamUtil.getBoolean(uploadPortletRequest, "isbb")){
			lusdlclass = BuildingBlock.class;
			lusdlArt = response.getBuildingBlock();
			System.out.println("Warnings:");
			for(String warn : response.getWarnings()){
				System.out.println("\t"+warn);
			}
			
			if(lusdlArt == null){
				System.out.println("Errors:");
				for(String err : response.getErrors()){
					System.out.println("\t"+err);
				}
			}
		}
		
		if(ParamUtil.getBoolean(uploadPortletRequest, "ispsa")){
			lusdlclass = PublicServiceApplication.class;
			try{ lusdlArt = MetamodelParser.parsePSA(lusdlmodel); }
			catch(Exception e){ e.printStackTrace(); }
		}
		
		

		if(lusdlArt!=null){
			try{
				
				User u = PortalUtil.getUser(uploadPortletRequest);
				
				//Validazione BB
				if(lusdlArt instanceof BuildingBlock) {
					lusdlArt = ArtifactValutationUtils.setDefaultOutput((BuildingBlock) lusdlArt);
					lusdlArt = ArtifactValutationUtils.fixBody((BuildingBlock)lusdlArt); // Per esigenza del Visual Composer
					lusdlArt = ArtifactValutationUtils.fixSecurity((BuildingBlock)lusdlArt); // Se viene specificata un operation con security e non viene inserito il parameter header, viene inserito lato server manualmente
					if(!ArtifactValutationUtils.isArtifactValid(lusdlArt.writeAsJsonString())) {
						System.out.println("ARTEFATTO BLOCCATO NON TRATTABILE");
						return; //SE L'ARTEFATTO NON E' VALIDO NON FACCIO FARE L'INSERT NEL DB
					}
					
					
					if(!lusdlArt.getPilot().equals(MyMarketplaceUtility.getPilot(u, uploadPortletRequest)) && !ParamUtil.getBoolean(uploadPortletRequest, "isAdmin")) {		
						System.out.println("Pilota modificato illegalmente");
						return; //IL PILOTA E' STATO MODIFICATO SENZA CHE L'UTENTE SIA UN ADMIN
					}
					
					
					
				}
				
				//-____________________________________________________________-
				Artefatto art_tmp = WizardUtils.setupArtefact(artefattoid, lusdlArt, lusdlclass, u,  -1L);
				art_tmp = ArtefattoLocalServiceUtil.publishArtefatto(art_tmp.getArtefattoId());
				artefattoid = art_tmp.getArtefattoId();
				//--------------------------------
				//Valutazione BB
				if(lusdlArt instanceof BuildingBlock) {
					System.out.println("TODO START HERE----Valutazione Artefatto ID: "+artefattoid);
					artifactLevel = (int) ArtifactEvaluationUtils.getArtifactLevelById(artefattoid, false).get("artefactLevel");
					System.out.println("LIVELLO ARTEFATTO: "+artifactLevel);
					ArtifactLevels level = ArtifactLevelsLocalServiceUtil.addArtifactLevels(artefattoid,artifactLevel,"");
					System.out.println("LIVELLO ARTEFATTO PUBBLICATO: "+level);
				}
				//_------------------------------_
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		actionRequest.setAttribute("action-return", true);
		actionRequest.setAttribute("action-return-id", artefattoid);
		actionRequest.setAttribute("descriptor", lusdlmodel);
		actionRequest.setAttribute("uploadedDescriptor", true);
		
		if(ParamUtil.getBoolean(uploadPortletRequest, "isbb")) 
			actionRequest.setAttribute("level",artifactLevel);		
		
		actionResponse.setRenderParameter("msg", "publication-complete");
		return;
	}

}
