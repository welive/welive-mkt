package it.eng.rspa.wizard;

import it.eng.metamodel.definitions.Artefact;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dependency;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.wizard.WizardActions;
import it.eng.rspa.marketplace.wizard.WizardActions.DLFOLDER_TYPE;
import it.eng.sesame.Crud;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.upload.UploadRequest;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;

public abstract class WizardUtils {
	
	public static final String DLFOLDER_NAME_MARKETPLACE = "MarketplaceDLFolder";
	public static final String DLFOLDER_MARKETPLACE_DESCRIPTION="DLFolder Marketplace e Artefatto";
	public static final String DLFOLDER_ARTIFACT_IMAGES = "images";
	public static final String NEW_ARTIFACT_TITLE = "new";
	
	public static final int MAX_MB_DESCRIPTOR_SIZE = 25;
	public static final int MAX_BYTE_DESCRIPTOR_SIZE = MAX_MB_DESCRIPTOR_SIZE*1024*1024; 
	
	public static void dumpRequest(UploadRequest request){
		Enumeration<String> params = request.getParameterNames();
		while(params.hasMoreElements()){
			String p = params.nextElement();
			System.out.println(p+": "+request.getParameter(p));
		}
	}
	
	public static ThemeDisplay getThemeDisplay(ServletRequest request){
		return (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}
	
	public static String getDocumentLibraryBaseUrl(ServletRequest request){
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		return themeDisplay.getPortalURL() + themeDisplay.getPathContext() 
					+ StringPool.SLASH + "documents" + StringPool.SLASH 
					+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
	}
	
	public static Artefatto newArtefact(long companyId, long groupId, long userId, Long[] orgIds) 
			throws SystemException{
		
		Artefatto artefatto = ArtefattoLocalServiceUtil.createArtefatto(CounterLocalServiceUtil.increment());
		
		artefatto.setTitle(NEW_ARTIFACT_TITLE);
		artefatto.setRepositoryRDF(Crud.getRepositoryURL());
		artefatto.setCompanyId(companyId);
		artefatto.setGroupId(groupId); 
		artefatto.setUserId(userId);
		artefatto.setDate(GregorianCalendar.getInstance().getTime());
		
		int idImgIconDefault=-1;
		artefatto.setImgId(idImgIconDefault);
		
		artefatto.setStatus(WorkflowConstants.STATUS_DRAFT);
		
		ArtefattoLocalServiceUtil.updateArtefatto(artefatto);
		
		ArtifactOrganizations ao = new ArtifactOrganizationsImpl();
		for(int i=0; i<orgIds.length; i++){
			ao.setArtifactId(artefatto.getArtefattoId());
			ao.setOrganizationId(orgIds[i]);
			ao.setStatus(WorkflowConstants.STATUS_DRAFT);
			ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations(ao);
		}
		
		return artefatto;
	}
	
	/**
	 * Recupera la DLfolder, se non esiste la crea.
	 * @param actionRequest
	 * @param scopeGroupId
	 * @param parentFolder
	 * @param nomeFolder
	 * @param descrFolder
	 * @return la folder trovata o creata
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static DLFolder trovaCreaDLFolder(UploadPortletRequest actionRequest,
									long scopeGroupId, 
									long parentFolder, 
									String nomeFolder, 
									String descrFolder, 
									DLFOLDER_TYPE subtype ) 
			throws Exception{
		
		
		DLFolder  folder= DLFolderLocalServiceUtil.fetchFolder(scopeGroupId, parentFolder, nomeFolder);
		if (folder==null){
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(),actionRequest);

			Folder folderA=   DLAppServiceUtil.addFolder(scopeGroupId, parentFolder,  nomeFolder, descrFolder, serviceContext);
			if(subtype == DLFOLDER_TYPE.MAINIMAGES || subtype == DLFOLDER_TYPE.SUBIMAGES){				
				String[] publicPermission =  {ActionKeys.VIEW, ActionKeys.ACCESS};
				ThemeDisplay themeDisplay = getThemeDisplay(actionRequest);
				setPermission(themeDisplay.getUser(), Long.toString(folderA.getFolderId()), DLFolder.class.getName(), publicPermission);
			}
			folder=DLFolderLocalServiceUtil.getDLFolder(folderA.getFolderId());
			
		}

		return folder;
	}
	
	
	public static void setPermission(User user,
									String resourceId, 
									String resourceType,
									String[] actionIdWiew) 
		throws Exception{
		
		String[] actionIdAllFile =  {ActionKeys.VIEW,
									ActionKeys.ADD_DISCUSSION,
									ActionKeys.DELETE,
									ActionKeys.DELETE_DISCUSSION,
									ActionKeys.PERMISSIONS,
									ActionKeys.UPDATE,
									ActionKeys.UPDATE_DISCUSSION};
		
		String[] actionIdAllFolder =  {ActionKeys.VIEW,
									ActionKeys.ACCESS,
									ActionKeys.ADD_DOCUMENT,
									ActionKeys.ADD_SHORTCUT,
									ActionKeys.ADD_SUBFOLDER,
									ActionKeys.DELETE,
									ActionKeys.PERMISSIONS,
									ActionKeys.UPDATE};
		
		String[] publicActions_file = {ActionKeys.VIEW};
		String[] publicActions_folder = {ActionKeys.VIEW, ActionKeys.ACCESS};
		
		
		List<Role> roles = RoleLocalServiceUtil.getRoles(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<Role> rolesIt = roles.iterator();
		
		while(rolesIt.hasNext()){
		
		Role role = rolesIt.next();	
		String[] perm;
		if(!role.getName().equals("Owner")){ //Se l'utente non � il proprietario
			if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
				perm = Arrays.copyOf(publicActions_file, publicActions_file.length);
			}
			else if(resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){
				perm = Arrays.copyOf(publicActions_folder, publicActions_folder.length);
			}
			else{
				throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Tipo di risorsa non valida");
			}
			
		}
		else{ //Se l'utente � il proprietario
			if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
				perm = Arrays.copyOf(actionIdAllFile, actionIdAllFile.length);
			}else if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){	
				perm = Arrays.copyOf(actionIdAllFolder, actionIdAllFolder.length);
			}
			else{
				throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Tipo di risorsa non valida");
			}
		}
		try{
			ResourcePermissionServiceUtil.setIndividualResourcePermissions(
					user.getGroupId(),
					user.getCompanyId(), //companyId
					resourceType, //name
					resourceId, //primKey
					role.getPrimaryKey(), //roleId
					perm);
		}
		catch(Exception e){ throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Permesso negato."); }
		}
	}
	
	public static DLFileEntry storeArtefactImage(UploadPortletRequest request, Artefatto a, File file, boolean isCover) 
			throws Exception{
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
//		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),request);
		
		Long idArtefatto = a.getArtefattoId();
		
		//Se si sta caricando un'immagine si deve caricarla nella cartella adibita alle immagini
		//DLMarketplaceFolder->DLFOLDER_ARTIFACT_IMAGES->idArtefatto
		DLFolder dlFolder = (DLFolder) WizardUtils._getDLMarketplacepSubFolder(themeDisplay, request, WizardActions.DLFOLDER_ARTIFACT_IMAGES);
		dlFolder = WizardUtils.trovaCreaDLFolder(request,
									themeDisplay.getScopeGroupId(),
									dlFolder.getFolderId(),
									String.valueOf(idArtefatto), 
									"Cartella contenente le immagini dell'artefatto "+idArtefatto,
									DLFOLDER_TYPE.SUBIMAGES);
		
//		User user = themeDisplay.getUser();
//		InputStream inputStream = new FileInputStream(file);
//		
//		DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(
//				user.getUserId(),
//				themeDisplay.getScopeGroupId(),//user.getGroupId(), 
//				dlFolder.getRepositoryId(), 
//				dlFolder.getFolderId(),
//				file.getName(), 
//				MimeTypesUtil.getContentType(file), 
//				file.getName(), 
//				"descrizione del file", 
//				"",
//				0, 
//				new HashMap<String, Fields>(),//fieldsMap
//				file,
//				inputStream,//fileInfo.getFile()					  
//				file.length(),
//				serviceContext);
//
//		DLFileEntryLocalServiceUtil.updateFileEntry(
//				user.getUserId(),
//				dlFileEntry.getFileEntryId(), 
//				file.getName(), 
//				MimeTypesUtil.getContentType(file),
//				file.getName(), 
//				"descrizione del file",  
//				"",
//				true, 
//				dlFileEntry.getFileEntryTypeId(), 
//				new HashMap<String, Fields>(),
//				file,
//				inputStream,
//				file.length(),
//				serviceContext);
//		
//		String[] publicActions_file =  {ActionKeys.VIEW};
//		WizardUtils.setPermission(user, Long.toString(dlFileEntry.getFileEntryId()), DLFileEntry.class.getName(), publicActions_file);
		
		DLFileEntry dlFileEntry = MyMarketplaceUtility.storeDLFile(request, file, dlFolder);
		
		ImmagineArt img = ImmagineArtLocalServiceUtil.createImmagineArt( CounterLocalServiceUtil.increment(ImmagineArt.class.getName()) );
		img.setDlImageId(dlFileEntry.getFileEntryId());
		img.setDescrizione("descrizione");
		img.setArtefattoIdent(idArtefatto);
		
		ImmagineArtLocalServiceUtil.updateImmagineArt(img);
		
		if(isCover){
			a.setImgId(img.getImageId());
			ArtefattoLocalServiceUtil.updateArtefatto(a);
		}
		
		return dlFileEntry;
		
	}
	
	public static DLFolder _getMarketplaceFolder(ThemeDisplay themeDisplay, UploadPortletRequest actionRequest) 
			throws Exception{
		
		return trovaCreaDLFolder(actionRequest, 
								themeDisplay.getScopeGroupId(), 
								0L, 
								DLFOLDER_NAME_MARKETPLACE, 
								DLFOLDER_MARKETPLACE_DESCRIPTION, 
								DLFOLDER_TYPE.MAINMARKET);

	}
	
	public static DLFolder  _getDLMarketplacepSubFolder(ThemeDisplay themeDisplay, UploadPortletRequest actionRequest, String nomefolder ) 
			throws Exception{

		if(nomefolder==null){ throw new NullPointerException("Invalid parameter: nomefolder"); }
		if(themeDisplay==null){ throw new NullPointerException("Invalid parameter: themeDisplay"); }
		if(actionRequest==null){ throw new NullPointerException("Invalid parameter: actionRequest"); }
		
		DLFolder  folder=_getMarketplaceFolder(themeDisplay, actionRequest);

		if(nomefolder==DLFOLDER_ARTIFACT_IMAGES){
			folder= trovaCreaDLFolder(actionRequest,themeDisplay.getScopeGroupId(), folder.getFolderId(), nomefolder, nomefolder, DLFOLDER_TYPE.MAINIMAGES);
		}else{
			folder= trovaCreaDLFolder(actionRequest,themeDisplay.getScopeGroupId(), folder.getFolderId(), nomefolder, nomefolder, DLFOLDER_TYPE.MAINARTIFACT);
		}
		return folder;
	}

	public static Artefatto trovaCreaArtefatto(UploadPortletRequest uploadrequest) 
			throws Exception{
		
		Artefatto a = null;
		Long idArtefatto = Long.parseLong(uploadrequest.getParameter("idArtefatto"));
		if(idArtefatto == -1L){
			ThemeDisplay themeDisplay = getThemeDisplay(uploadrequest);
			User user = themeDisplay.getUser();
			a = WizardUtils.newArtefact(user.getCompanyId(), user.getGroupId(), user.getUserId(), new Long[]{});
			idArtefatto = a.getArtefattoId();
		}
		else{ a = ArtefattoLocalServiceUtil.fetchArtefatto(idArtefatto); }
		
		return a;
	}
	
	public static String getRemoteFileString(String sUrl) throws IOException{
		
		URL url = new URL(sUrl);
		InputStream urlStream  = url.openStream();
		
		byte buffer[] = new byte[1024];
        int numRead = urlStream.read(buffer);
        String content = new String(buffer, 0, numRead);

        while ((numRead != -1) && (content.length() < WizardUtils.MAX_BYTE_DESCRIPTOR_SIZE)) {
            numRead = urlStream.read(buffer);
            if (numRead != -1) {
                String newContent = new String(buffer, 0, numRead);
                content += newContent;
            }
        }
        
        return content;
	}

	public static Artefatto setupArtefact(long artefattoid, Artefact lusdlart, Class<?> lusdlclass, User u, Long ideaid) throws Exception{

		Artefatto art = null;
		String provider = "";
		String owner = "";
		long catid = -1;
		String url = "";
		
		List<Dependency> deps = new ArrayList<Dependency>();
		
		if(lusdlclass == BuildingBlock.class){
			BuildingBlock bb = (BuildingBlock)lusdlart;
			
			if(bb.getType() != null && bb.getType().trim().length() > 0) 
				catid = Category.getMapByName().get(bb.getType()).getIdCategoria();
			
			deps = bb.getUsedBBs();
			
		}
		else if(lusdlclass == PublicServiceApplication.class){
			PublicServiceApplication psa = (PublicServiceApplication)lusdlart;
			if(psa.getType() != null && psa.getType().trim().length() > 0)
				catid = Category.getMapByName().get(psa.getType()).getIdCategoria();
			url = psa.getUrl();
			
			deps = psa.getUsedBBs();
		}
		
		//Check dependencies
		for(Dependency d : deps){
			long depid = d.getId();
			Artefatto depArt = ArtefattoLocalServiceUtil.getArtefatto(depid);
			if(depArt.getStatus() != WorkflowConstants.STATUS_APPROVED){
				throw new Exception("Invalid dependency supplied: "+d.getTitle() + "[" + d.getId() + "]");
			}
		}
		
		for(Entity e : lusdlart.getBusinessRoles()){
			if("provider".equalsIgnoreCase(e.getBusinessRole())){
				provider = e.getTitle();
			}
			if("owner".equalsIgnoreCase(e.getBusinessRole())) {
				owner = e.getTitle();
			}
		}
		
		Set<Long> orgIds = new HashSet<Long>();
		if(!owner.equalsIgnoreCase(u.getFullName())){
			try{
				Organization org = OrganizationLocalServiceUtil.getOrganization(u.getCompanyId(), owner);
				orgIds.add(org.getOrganizationId());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		
		if(artefattoid == -1L){
			art = WizardUtils.newArtefact(u.getCompanyId(), u.getGroupId(), u.getUserId(), orgIds.toArray(new Long[]{}));
		}
		else{  art = ArtefattoLocalServiceUtil.getArtefatto(artefattoid); }
		
		art.setTitle(lusdlart.getTitle());
		art.setDescription(lusdlart.getDescription());
		art.setAbstractDescription(lusdlart.getDescription());
		
		String sDate = lusdlart.getCreated();
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
		art.setDate(date);
		
		art.setStatus(WorkflowConstants.STATUS_DRAFT);
		
		art.setPilotid(lusdlart.getPilot());
		
		art.setLanguage(lusdlart.getLang());
		
		art.setProviderName(provider);
		
		art.setOwner(owner);
		
		String lusdl = lusdlart.writeAsJsonString();
		art.setLusdlmodel(lusdl);
		
		art.setCategoriamkpId(catid);
		art.setUrl(url);
		
		ArtefattoLocalServiceUtil.updateArtefatto(art);
		
		//Update dependencies
		for(Dependency d : deps){
			Dependencies alreadyPresentDependency = DependenciesLocalServiceUtil.getDependencyByIDsCouple(art.getArtefattoId(), d.getId());
			
			if(Validator.isNull(alreadyPresentDependency)){
				Dependencies currdep = DependenciesLocalServiceUtil.createDependencies(CounterLocalServiceUtil.increment(Dependencies.class.getName()));
				currdep.setArtefactId(art.getArtefattoId());
				currdep.setDependsFrom(d.getId());
				
				DependenciesLocalServiceUtil.addDependencies(currdep);
			}
			
		}
		
		//Update tags
		List<String> cleanTags = new ArrayList<String>();
		
		final String regex = "[&'@\\\\\\]\\}:,=>\\/<\\[\\{%|\\\"+#`?;\\/*~]+";
		final Pattern pattern = Pattern.compile(regex);
		final String subst = "";
		
		List<String> tags = lusdlart.getTags();
		for(String tag : tags){
			Matcher matcher = pattern.matcher(tag);
			cleanTags.add(matcher.replaceAll(subst));
		}
		
		String[] arrcleanTags = cleanTags.toArray(new String[0]);
		try{
			AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(u.getUserId(),
																u.getGroupId(),
																Artefatto.class.getName(), 
																art.getArtefattoId(), 
																new long[]{},
																arrcleanTags);
			ae.setVisible(false);
			AssetEntryLocalServiceUtil.updateAssetEntry(ae);
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		
		if(ideaid!=null && ideaid >= 0){
			try{
				CLSArtifacts ideaart = CLSArtifactsLocalServiceUtil.createCLSArtifacts(new CLSArtifactsPK(ideaid, art.getArtefattoId()));
				CLSArtifactsLocalServiceUtil.addCLSArtifacts(ideaart);
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		
		return art;
	}
}
