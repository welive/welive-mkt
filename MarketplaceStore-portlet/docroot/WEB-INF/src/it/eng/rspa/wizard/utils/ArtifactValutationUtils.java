package it.eng.rspa.wizard.utils;
import it.eng.metamodel.definitions.AuthenticationMeasure;
import it.eng.metamodel.definitions.Operation;
import it.eng.metamodel.definitions.Parameter;
import it.eng.metamodel.definitions.Response;
import it.eng.metamodel.definitions.SecurityMeasure;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.eng.metamodel.definitions.BuildingBlock;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jena.atlas.json.JSON;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;


public abstract class ArtifactValutationUtils {
	
	private static boolean isValidJSON(String test) {
	   try {
		   JSON.parse(StringEscapeUtils.unescapeHtml4(test));
		   return true;
	   } catch (Exception e) {
		   System.out.println("[Validation] invalidJSON");
		   return false;	   
	   }
	}
	
	private static boolean isValidOutput (JSONObject obj) {
		boolean isValid = true;
		try {
			JSONArray hasOperation = JSONFactoryUtil.createJSONArray();
			hasOperation = obj.getJSONArray("hasOutput");
			for(int i=0;i<hasOperation.length();i++) {
				JSONObject curr = hasOperation.getJSONObject(i);
				System.out.println(curr);
				if( (!curr.has("schema") || (!curr.has("code") || curr.getInt("code") < 100 || curr.getInt("code") > 999)))
						isValid = false;	
			}
		} catch (Exception e) {
			e.printStackTrace();
			isValid = false;
		}
		if(!isValid) System.out.println("[Validator]InvalidOutput");
		return isValid;
	}
	
	private static String replaceParameter (String url) {
		boolean b = true;
		String p = "(/\\{[^/]+\\}/)|(/\\{[^/]+\\}$)";
		
		if(url!=null) {
			while(b) {				
				Pattern paramRegEx = Pattern.compile(p);
				Matcher m = paramRegEx.matcher(url);
				b = m.find();
				
				if(b) {
					url = url.replaceAll("(/\\{[^/]+\\}/)|(/\\{[^/]+\\}$)", "/"); 				
				}				
			}
		}				
		return url;
	}
	
	private static boolean isOperationValid(JSONObject obj) {
		boolean isValid = true;
		String urlRegEx = "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		try {
			JSONArray hasOperation = JSONFactoryUtil.createJSONArray();
			hasOperation = obj.getJSONArray("hasOperation");			
			for(int i=0;i<hasOperation.length();i++) {
				JSONObject curr = hasOperation.getJSONObject(i);
				
				String endpoint = curr.getString("endpoint").trim();
				if(endpoint!=null && endpoint.length()>0) endpoint = endpoint.split("\\?")[0];
				
				System.out.println("TEST 1 - title:"+curr.has("title") + " - length"+curr.getString("title").trim().length());
				System.out.println("TEST 2 - method:"+curr.has("method") + " - length"+curr.getString("method").trim().length());
				System.out.println("TEST 3 - endpoint:"+curr.has("endpoint") + " - length"+replaceParameter(endpoint).matches(urlRegEx));
				System.out.println("TEST 4 - hasOutput:"+curr.has("hasOutput") + " - length"+curr.getJSONArray("hasOutput").length());
				
				if( (!curr.has("title") || curr.getString("title").trim().length()==0)           ||
				    (!curr.has("method") || curr.getString("method").trim().length()==0)         ||
				    //(!curr.has("endpoint") || curr.getString("endpoint").trim().length()==0)   ||
				    (!curr.has("hasOutput") || curr.getJSONArray("hasOutput").length() == 0)     || 
				    (!curr.has("endpoint") || !replaceParameter (endpoint).matches(urlRegEx))     ||
				    //(!curr.has("endpoint") || !curr.getString("endpoint").replaceAll("(/\\{[^/]+\\}/)|(/\\{[^/]+\\}$)", "/").matches(urlRegEx))     ||
				    (!isValidOutput(curr))
				   ) isValid = false;	//TODO Probably it's better a break here
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			isValid = false;
		}
		if(!isValid) System.out.println("[Validator]InvalidOperation");
		return isValid;
	}
	
	public static boolean isArtifactValid (String lusdlmodel) {
		try {
			System.out.println(lusdlmodel);
			
			JSONObject jsonModel = JSONFactoryUtil.createJSONObject(lusdlmodel);
			System.out.println("[Validator]AUTHOR VALID: "+ArtifactEvaluationUtils.isAuthorValid(jsonModel));
			System.out.println("[Validator]OWNER VALID: "+ArtifactEvaluationUtils.isOwnerValid(jsonModel));
			System.out.println("[Validator]PILOT VALID: "+ArtifactEvaluationUtils.isPilotValid(jsonModel));
			System.out.println("[Validator]TITLE VALID: "+ArtifactEvaluationUtils.isTitleValid(jsonModel));
			if( ArtifactEvaluationUtils.isAuthorValid(jsonModel) &&
				ArtifactEvaluationUtils.isOwnerValid(jsonModel)  &&
				ArtifactEvaluationUtils.isPilotValid(jsonModel)  &&
				ArtifactEvaluationUtils.isTitleValid(jsonModel)  &&
				isOperationValid(jsonModel)
			) return true;
			else return false;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	


	public static BuildingBlock setDefaultOutput (BuildingBlock data) {
		List<Operation> operations = data.getOperations();
		for(Operation entry : operations) {
			List<Response> responses = entry.getResponses();
			if(responses.size() == 0) {
				//Add Default Operation
				Response defaultOutput = new Response();
				defaultOutput.setCode(200);
				defaultOutput.setDescription("OK");
				//defaultOutput.setSchema("{}");
				defaultOutput.setSchema("");
				defaultOutput.setType("");
				
				responses.add(defaultOutput);
				entry.setResponses(responses);
				System.out.println("[PRE-VALIDATION]Added default Operation");
			}
		}
		data.setOperations(operations);
		System.out.println("[PRE-VALIDATION]Return to main validator");
		return data;
	}

	public static BuildingBlock fixBody(BuildingBlock data) {
		List<Operation> operations = data.getOperations();
		boolean changed = false;
		
		for(Operation entry : operations) {
			String method= entry.getMethod();			
			List<Parameter> params = entry.getParameters();
			
			for(Parameter p : params) {
				if("body".equals(p.getIn()) && !"body".equals(p.getName()) && ("POST".equalsIgnoreCase(method) || 
												"PUT".equalsIgnoreCase(method) || 
												"DELETE".equalsIgnoreCase(method) || 
												"PATCH".equalsIgnoreCase(method) ||
												"UPDATE".equalsIgnoreCase(method)
												)) 
				{
					p.setName("body");
					System.out.println("BODY FIXED");
					changed=true;
				}
					
			}
			
			if(changed) entry.setParameters(params);
			
		}
		if(changed) data.setOperations(operations);
		return data;
	}
	
	public static BuildingBlock fixSecurity(BuildingBlock data) {
		List<Operation> operations = data.getOperations();
//		List<Operation> tmpOperations = new ArrayList<Operation>();
//		int index = 0;
		for(Operation entry : operations) {
			SecurityMeasure s = entry.getSecurityMeasure();
			String msg = "";
			if(s!=null) {
				if(s instanceof AuthenticationMeasure) {
					AuthenticationMeasure auth = (AuthenticationMeasure) s;
					if("BASIC".compareToIgnoreCase(auth.getProtocolType()) == 0 || "OAUTH2".compareToIgnoreCase(auth.getProtocolType()) == 0) {

						if("BASIC".compareToIgnoreCase(auth.getProtocolType()) == 0) msg="\"Basic \" + a user access token.";
						else msg="\"Bearer \" + a user access token."; 
					}
					
					//FIX 2 - Parser
					if("OAUTH2".compareToIgnoreCase(auth.getProtocolType()) == 0) {
						auth.setTitle("weliveUserAuth");
						entry.setSecurityMeasure(auth);
					}
					
					boolean flag = false;
					for(Parameter i : entry.getParameters()) {
						if("authorization".compareToIgnoreCase(i.getName()) == 0) flag = true;					
					}
					
					if(!flag) {
						System.out.println("AUTH PARAMETER ADDED");
						Parameter p = new Parameter();
						p.setIn("header");
						p.setName("Authorization");
						p.setRequired(true);
						p.setType("string");
						p.setSchema("{}");
						p.setDescription(msg);
						
						List<Parameter> list = entry.getParameters();
						list.add(p);
						entry.setParameters(list);
//						tmpOperations.add(entry);
					}
					
				}			
			}	
//			index+=1;
		}
//		operations.addAll(tmpOperations);
		return data;
	}
	
	
}
