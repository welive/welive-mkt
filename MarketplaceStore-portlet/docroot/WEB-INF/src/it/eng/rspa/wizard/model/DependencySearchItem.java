package it.eng.rspa.wizard.model;

public class DependencySearchItem {

	private Long artefactId;
	private String name;
	private String linkImage;
	private String type;
	
	public DependencySearchItem(){}
	
	public DependencySearchItem(Long artefactId, String name, String linkImage, String type) {
		this.artefactId = artefactId;
		this.name = name;
		this.linkImage = linkImage;
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLinkImage() {
		return linkImage;
	}
	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getArtefactId() {
		return artefactId;
	}
	public void setArtefactId(Long artefactId) {
		this.artefactId = artefactId;
	}
	
	
	
	
}
