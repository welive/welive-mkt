package it.eng.rspa.wizard.utils;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dependency;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.definitions.Operation;
import it.eng.metamodel.definitions.Parameter;
import it.eng.metamodel.definitions.Response;
import it.eng.metamodel.definitions.SecurityMeasure;
import it.eng.metamodel.definitions.ServiceOffering;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.portlet.PortletProps;

public abstract class ArtifactEvaluationUtils {
	
	private ArtifactEvaluationUtils() {
		//EMPTY CONSTRUCTOR
	}
	
	protected static boolean isTitleValid (JSONObject obj) {
		try {
			String title = obj.getString("title");
			if( title == null || title.isEmpty() || title.trim().length() == 0) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	protected static boolean isAuthorValid (JSONObject obj) {
		
		try {
			JSONArray businessRole = JSONFactoryUtil.createJSONArray();
			businessRole = obj.getJSONArray("hasBusinessRole");
			for(int i=0;i<businessRole.length();i++) {
				JSONObject item = businessRole.getJSONObject(i);
				if("Author".equalsIgnoreCase(item.getString("businessRole")) && item.getString("title").trim().length()>0) {
					return true;
				}
			}
			return false;
		} 
		catch(Exception e) {
			return false;
		}
	}

	protected static boolean isOwnerValid (JSONObject obj) {
			
		try {
			JSONArray businessRole = JSONFactoryUtil.createJSONArray();
			businessRole = obj.getJSONArray("hasBusinessRole");
			for(int i=0;i<businessRole.length();i++) {
				JSONObject item = businessRole.getJSONObject(i);
				if("Owner".equalsIgnoreCase(item.getString("businessRole")) && item.getString("title").trim().length()>0) {
					return true;
				}
			}
			return false;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	protected static boolean isPilotValid (JSONObject obj) {
		try {
			String pilot = obj.getString("pilot");
			if( pilot == null || pilot.isEmpty() ) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isTypeValid (JSONObject obj) {
		try {
			String type = obj.getString("type");
			if( type.compareTo(PortletProps.get("Category.rest"))!=0 && type.compareTo(PortletProps.get("Category.soap"))!=0 ) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isDescriptionValid (JSONObject obj) {
		try {
			String description = obj.getString("description");
			if( description == null || description.isEmpty() || description.trim().length() == 0 ) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isLanguageValid (JSONObject obj) {
		try {
			String language = obj.getString("language");
			if( language == null || language.isEmpty() || language.trim().length() == 0) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isProviderValid (JSONObject obj) {
		try {
			JSONArray businessRole = JSONFactoryUtil.createJSONArray();
			businessRole = obj.getJSONArray("hasBusinessRole");
			for(int i=0;i<businessRole.length();i++) {
				JSONObject item = businessRole.getJSONObject(i);
				if("Owner".equalsIgnoreCase(item.getString("businessRole")) && item.getString("title").trim().length()>0) {
					return true;
				}
			}
			return false;
		} catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isRDFValid (JSONObject obj) {
		try {
			String rdf = obj.getString("rdf");
			if( rdf == null || rdf.isEmpty() ) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isValidOperation (JSONObject obj) {
		try {
			JSONArray hasOperation = obj.getJSONArray("hasOperation");
			if(hasOperation.length() == 0) {
				return false;
			} else return true;
		} 
		catch(Exception e) {
			return false;
		}
	}
	
	private static boolean isVcCompliant(JSONObject obj) {
		boolean isValid = true;
		try {
			JSONArray hasOperation = JSONFactoryUtil.createJSONArray();
			hasOperation = obj.getJSONArray("hasOperation");			
			for(int i=0;i<hasOperation.length();i++) {
				JSONObject curr = hasOperation.getJSONObject(i);				
				String consumes = curr.getString("consumes");
				
				if("application/json".compareToIgnoreCase(consumes) != 0 && "application/xml".compareToIgnoreCase(consumes) != 0 && consumes.trim().length() > 0) {
					return false;
				}
				
				JSONArray produces = curr.getJSONArray("produces");
				for(int j = 0; j<produces.length();j++) {
					if("application/octet-stream".compareToIgnoreCase(produces.getString(j)) == 0 || "other".compareToIgnoreCase(produces.getString(j)) == 0 ) {
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			isValid = false;
		}
		return isValid;
	}
	
	private static int getLevel(int[] levels) {
		int level = 0;
		for (int i = 0; i< levels.length; i++) {
			if(levels[i] == 1)
				level++;
			else 
				break;			
		}	
		return level;
	}
	
	private static HashMap<String,Object> evaluate (JSONObject obj,boolean isApiRequest) {
		
		int[] levels = new int[5];
		List<String> problems= new ArrayList<String>();
		HashMap<String, Object> result= new HashMap<String,Object>();
		Arrays.fill(levels,1);
				
		//LEVEL 1 REQUIREMENTS
			
		if( !isOwnerValid(obj) ) {
			//Owner is a Level 1 Requirement
			System.out.println("OWNER MISSING");
			problems.add("Owner missing");
			levels[0] = 0;
		}
		
		if(!isAuthorValid(obj)) {
			//Author is a Level 1 Requirement
			System.out.println("AUTHOR MISSING");
			problems.add("Author missing");
			levels[0] = 0;
		}
			
		if( !isTitleValid(obj) ) {
			//Title is a Level 1 Requirement
			System.out.println("TITLE MISSING");
			problems.add("Owner missing");
			levels[0] = 0;
		}
			
		if( !isPilotValid(obj) ) {
			//Pilot is a Level 1 Requirement
			System.out.println("PILOT MISSING");
			problems.add("Pilot Missing");
			levels[0] = 0;
		}
			
		//LEVEL 2 REQUIREMENTS
						
		if(!isTypeValid(obj) ) {
			//Type is a Level 2 Requirement
			System.out.println("TYPE MISSING");
			problems.add("Type missing");
			levels[1] = 0;
		}
			
		//LEVEL 3 REQUIREMENTS
			
		//*-USDL Minimum set
		if(!isDescriptionValid(obj) ) {
			//Description is a Level 3 Requirement
			System.out.println("DESCRIPTION MISSING");
			problems.add("Description missing");
			levels[2] = 0;
		}
			
		if(!isLanguageValid(obj) ) {
			//Language is a Level 3 Requirement
			System.out.println("LANGUAGE MISSING");
			problems.add("Language missing");
			levels[2] = 0;
		}
			
		if(!isProviderValid(obj) ) {
			//Provider is a Level 3 Requirement
			System.out.println("PROVIDER MISSING");
			problems.add("Provider missing");
			levels[2] = 0;
		}
			
		if(isApiRequest && isRDFValid(obj) ) {
			System.out.println("RDF MISSING");
			problems.add("Rdf missing");
			//RDF is a Level 3 Requirement (Only for API Request)
			levels[2] = 0;
		}
		
		if(!isVcCompliant(obj)) {
			System.out.println("THIS BUILDING BLOCK IS NOT VC COMPLIANT");
			problems.add("This Building Block Is Not VC Compliant");
			//The Building Block have to be VC Compliant in order to optain a 3 Star Evaluation
			levels[2] = 0;
		}
		//___________________________
			
		//*- Interection point 
			
		if(!isValidOperation(obj)) {
			System.out.println("OPERATIONS MISSING");
			problems.add("Invalid Operations");
			//At least 1 entry point is required for a Level 3 Artifact
			levels[2] = 0;
		}
			
		//FOR FUTURE IMPLEMENTATION
		//--------------
		levels[3] = 0;
		levels[4] = 0;
		//______________
		
		//Get Artifact Level
		result.put("artefactLevel", getLevel(levels));
		result.put("problems", tokenize(problems));
		return result;
	}
	
	private static String tokenize(List<String> problems) {
		String msg = "";
		for(String s: problems) {
			if(msg.length()!=0) { msg+=", ";}
		    msg+=s+"";
		}
		
		return msg.toString();
	}

	//Access point
	public static HashMap<String,Object> getArtifactLevelById (long artifactId, boolean isApiRequest) {
		HashMap<String,Object> response = new HashMap<String, Object>();
		int level = 0;			
		try {
				//GET THE LUSDL MODEL FROM DB
				String lusdlModel = ArtefattoLocalServiceUtil.getArtefatto(artifactId).getLusdlmodel();
				//CONVERT TO JSON
				JSONObject artifactData = JSONFactoryUtil.createJSONObject(lusdlModel);
				//EVALUATE THE ARTIFACT
				response = evaluate(artifactData,isApiRequest);		
				level = (int) evaluate(artifactData,true).get("artefactLevel");	
				if(level == 0) throw new Exception("Level 0 Detected!");
			
		} catch (Exception e) {
			e.printStackTrace();
			response.put("artefactLevel", -1);
		}
		return response;	
	}
	
	public static int getArtifactLevelByJSON (String data) {
		int level = 0;			
		try {
				
				//CONVERT TO JSON OBJECT
				JSONObject artifactData = JSONFactoryUtil.createJSONObject(data);
				//EVALUATE THE ARTIFACT
				level = (int) evaluate(artifactData,true).get("artefactLevel");			
				if(level == 0) throw new Exception("Level 0 Detected!");
			
		} catch (Exception e) {
			e.printStackTrace();
			level = -1;
		}
		return level;
	}
	
	
	@Deprecated
	public static int Object2JSON (long artifactId, BuildingBlock bb) {
		int level = -1;
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		try{
			obj.put("created",bb.getCreated());
			obj.put("description", bb.getDescription());
			obj.put("descriptorType", bb.getDescriptorType());
			obj.put("descriptorUrl", bb.getDescriptorUrl());
			
			//BusinessRole Mapping (Author, Owner, Provider)
			for(Entity entry : bb.getBusinessRoles() ) {
				JSONObject businessRole = JSONFactoryUtil.createJSONObject();
				
				businessRole.put("businessRole", entry.getBusinessRole());
				businessRole.put("mbox", entry.getMbox());
				businessRole.put("page", entry.getPage());
				businessRole.put("title", entry.getTitle());
				
				obj.put("hasBusinessRole", businessRole);
			}
			
			//Legal Condition Mapping
			for(License entry : bb.getLegalConditions()) {
				JSONObject legalCondition = JSONFactoryUtil.createJSONObject();
				
				legalCondition.put("description", entry.getDescription());
				legalCondition.put("title", entry.getTitle());
				legalCondition.put("url", entry.getUrl());
				
				obj.put("hasLegalCondition", legalCondition);				
			}
			
			//Operation Mapping
			for(Operation entry : bb.getOperations()) {
				JSONObject operation = JSONFactoryUtil.createJSONObject();
				operation.put("consumes", entry.getConsumes());
				operation.put("description", entry.getDescription());
				
				//Input Entry
				for(Parameter inputEntry : entry.getParameters()) {
					JSONObject input = JSONFactoryUtil.createJSONObject();
					
					input.put("description", inputEntry.getDescription());
					input.put("in", inputEntry.getIn());
					input.put("name", inputEntry.getName());
					input.put("schema", inputEntry.getSchema());
					input.put("type", inputEntry.getType());
					
					//TODO check JSON Schema
					operation.put("hasInput", input);
				}
				//Output Entry
				for(Response outputEntry : entry.getResponses()) {
					JSONObject output = JSONFactoryUtil.createJSONObject();
					
					output.put("code", outputEntry.getCode());
					output.put("description", outputEntry.getCode());
					output.put("schema", outputEntry.getSchema());
					output.put("type", outputEntry.getType());
					
					operation.put("hasOutput", output);
				}
				//Security Entry----------------------------------------
				SecurityMeasure securityEntry = entry.getSecurityMeasure();
				JSONObject security = JSONFactoryUtil.createJSONObject();
				
				security.put("description", securityEntry.getDescription());
				security.put("title", securityEntry.getTitle());
				
				operation.put("hasSecurityMeasure", security);
				//____________________________________________
				operation.put("method", entry.getMethod());
				operation.put("path", entry.getEndpoint());
				
				String produceJson = new Gson().toJson(entry.getProduces());
				operation.put("produces", produceJson);
				
				operation.put("title", entry.getTitle());
				
				obj.put("hasOperation", operation);
			}
			
			//Offerings Mapping
			for(ServiceOffering entry : bb.getServiceOfferings()) {
				JSONObject offering = JSONFactoryUtil.createJSONObject();
				
				offering.put("description", entry.getDescription());
				offering.put("title", entry.getTitle());
				offering.put("url", entry.getUrl());
				
				obj.put("hasServiceOffering", offering);
			}
			
			obj.put("language", bb.getLang());
			obj.put("page", bb.getPage());			
			obj.put("pilot", bb.getPilot());
			
			//Tags Mapping
			for(String tagEntry : bb.getTags()) {
				obj.put("tags", tagEntry);
			}
			
			obj.put("title", bb.getTitle());
			obj.put("type", bb.getType());
			
			//Dependencies mapping
			for(Dependency entry: bb.getUsedBBs()) {
				JSONObject dependency = JSONFactoryUtil.createJSONObject();
				
				dependency.put("id", entry.getId());
				dependency.put("title", entry.getTitle());
				
				obj.put("uses", dependency);
			}
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return level;
	}
}
