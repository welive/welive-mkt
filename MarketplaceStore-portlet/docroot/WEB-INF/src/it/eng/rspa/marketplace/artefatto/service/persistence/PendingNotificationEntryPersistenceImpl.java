/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException;
import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;
import it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the pending notification entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see PendingNotificationEntryPersistence
 * @see PendingNotificationEntryUtil
 * @generated
 */
public class PendingNotificationEntryPersistenceImpl extends BasePersistenceImpl<PendingNotificationEntry>
	implements PendingNotificationEntryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PendingNotificationEntryUtil} to access the pending notification entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PendingNotificationEntryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryModelImpl.FINDER_CACHE_ENABLED,
			PendingNotificationEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryModelImpl.FINDER_CACHE_ENABLED,
			PendingNotificationEntryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public PendingNotificationEntryPersistenceImpl() {
		setModelClass(PendingNotificationEntry.class);
	}

	/**
	 * Caches the pending notification entry in the entity cache if it is enabled.
	 *
	 * @param pendingNotificationEntry the pending notification entry
	 */
	@Override
	public void cacheResult(PendingNotificationEntry pendingNotificationEntry) {
		EntityCacheUtil.putResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryImpl.class,
			pendingNotificationEntry.getPrimaryKey(), pendingNotificationEntry);

		pendingNotificationEntry.resetOriginalValues();
	}

	/**
	 * Caches the pending notification entries in the entity cache if it is enabled.
	 *
	 * @param pendingNotificationEntries the pending notification entries
	 */
	@Override
	public void cacheResult(
		List<PendingNotificationEntry> pendingNotificationEntries) {
		for (PendingNotificationEntry pendingNotificationEntry : pendingNotificationEntries) {
			if (EntityCacheUtil.getResult(
						PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
						PendingNotificationEntryImpl.class,
						pendingNotificationEntry.getPrimaryKey()) == null) {
				cacheResult(pendingNotificationEntry);
			}
			else {
				pendingNotificationEntry.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all pending notification entries.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PendingNotificationEntryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PendingNotificationEntryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the pending notification entry.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PendingNotificationEntry pendingNotificationEntry) {
		EntityCacheUtil.removeResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryImpl.class,
			pendingNotificationEntry.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<PendingNotificationEntry> pendingNotificationEntries) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PendingNotificationEntry pendingNotificationEntry : pendingNotificationEntries) {
			EntityCacheUtil.removeResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
				PendingNotificationEntryImpl.class,
				pendingNotificationEntry.getPrimaryKey());
		}
	}

	/**
	 * Creates a new pending notification entry with the primary key. Does not add the pending notification entry to the database.
	 *
	 * @param notificationId the primary key for the new pending notification entry
	 * @return the new pending notification entry
	 */
	@Override
	public PendingNotificationEntry create(long notificationId) {
		PendingNotificationEntry pendingNotificationEntry = new PendingNotificationEntryImpl();

		pendingNotificationEntry.setNew(true);
		pendingNotificationEntry.setPrimaryKey(notificationId);

		return pendingNotificationEntry;
	}

	/**
	 * Removes the pending notification entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationId the primary key of the pending notification entry
	 * @return the pending notification entry that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry remove(long notificationId)
		throws NoSuchPendingNotificationEntryException, SystemException {
		return remove((Serializable)notificationId);
	}

	/**
	 * Removes the pending notification entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the pending notification entry
	 * @return the pending notification entry that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry remove(Serializable primaryKey)
		throws NoSuchPendingNotificationEntryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PendingNotificationEntry pendingNotificationEntry = (PendingNotificationEntry)session.get(PendingNotificationEntryImpl.class,
					primaryKey);

			if (pendingNotificationEntry == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPendingNotificationEntryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(pendingNotificationEntry);
		}
		catch (NoSuchPendingNotificationEntryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PendingNotificationEntry removeImpl(
		PendingNotificationEntry pendingNotificationEntry)
		throws SystemException {
		pendingNotificationEntry = toUnwrappedModel(pendingNotificationEntry);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pendingNotificationEntry)) {
				pendingNotificationEntry = (PendingNotificationEntry)session.get(PendingNotificationEntryImpl.class,
						pendingNotificationEntry.getPrimaryKeyObj());
			}

			if (pendingNotificationEntry != null) {
				session.delete(pendingNotificationEntry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (pendingNotificationEntry != null) {
			clearCache(pendingNotificationEntry);
		}

		return pendingNotificationEntry;
	}

	@Override
	public PendingNotificationEntry updateImpl(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws SystemException {
		pendingNotificationEntry = toUnwrappedModel(pendingNotificationEntry);

		boolean isNew = pendingNotificationEntry.isNew();

		Session session = null;

		try {
			session = openSession();

			if (pendingNotificationEntry.isNew()) {
				session.save(pendingNotificationEntry);

				pendingNotificationEntry.setNew(false);
			}
			else {
				session.merge(pendingNotificationEntry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
			PendingNotificationEntryImpl.class,
			pendingNotificationEntry.getPrimaryKey(), pendingNotificationEntry);

		return pendingNotificationEntry;
	}

	protected PendingNotificationEntry toUnwrappedModel(
		PendingNotificationEntry pendingNotificationEntry) {
		if (pendingNotificationEntry instanceof PendingNotificationEntryImpl) {
			return pendingNotificationEntry;
		}

		PendingNotificationEntryImpl pendingNotificationEntryImpl = new PendingNotificationEntryImpl();

		pendingNotificationEntryImpl.setNew(pendingNotificationEntry.isNew());
		pendingNotificationEntryImpl.setPrimaryKey(pendingNotificationEntry.getPrimaryKey());

		pendingNotificationEntryImpl.setNotificationId(pendingNotificationEntry.getNotificationId());
		pendingNotificationEntryImpl.setTimestamp(pendingNotificationEntry.getTimestamp());
		pendingNotificationEntryImpl.setArtefactId(pendingNotificationEntry.getArtefactId());
		pendingNotificationEntryImpl.setAction(pendingNotificationEntry.getAction());
		pendingNotificationEntryImpl.setTargetComponent(pendingNotificationEntry.getTargetComponent());
		pendingNotificationEntryImpl.setOptions(pendingNotificationEntry.getOptions());

		return pendingNotificationEntryImpl;
	}

	/**
	 * Returns the pending notification entry with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the pending notification entry
	 * @return the pending notification entry
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPendingNotificationEntryException, SystemException {
		PendingNotificationEntry pendingNotificationEntry = fetchByPrimaryKey(primaryKey);

		if (pendingNotificationEntry == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPendingNotificationEntryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return pendingNotificationEntry;
	}

	/**
	 * Returns the pending notification entry with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException} if it could not be found.
	 *
	 * @param notificationId the primary key of the pending notification entry
	 * @return the pending notification entry
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry findByPrimaryKey(long notificationId)
		throws NoSuchPendingNotificationEntryException, SystemException {
		return findByPrimaryKey((Serializable)notificationId);
	}

	/**
	 * Returns the pending notification entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the pending notification entry
	 * @return the pending notification entry, or <code>null</code> if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PendingNotificationEntry pendingNotificationEntry = (PendingNotificationEntry)EntityCacheUtil.getResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
				PendingNotificationEntryImpl.class, primaryKey);

		if (pendingNotificationEntry == _nullPendingNotificationEntry) {
			return null;
		}

		if (pendingNotificationEntry == null) {
			Session session = null;

			try {
				session = openSession();

				pendingNotificationEntry = (PendingNotificationEntry)session.get(PendingNotificationEntryImpl.class,
						primaryKey);

				if (pendingNotificationEntry != null) {
					cacheResult(pendingNotificationEntry);
				}
				else {
					EntityCacheUtil.putResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
						PendingNotificationEntryImpl.class, primaryKey,
						_nullPendingNotificationEntry);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PendingNotificationEntryModelImpl.ENTITY_CACHE_ENABLED,
					PendingNotificationEntryImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return pendingNotificationEntry;
	}

	/**
	 * Returns the pending notification entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationId the primary key of the pending notification entry
	 * @return the pending notification entry, or <code>null</code> if a pending notification entry with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PendingNotificationEntry fetchByPrimaryKey(long notificationId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)notificationId);
	}

	/**
	 * Returns all the pending notification entries.
	 *
	 * @return the pending notification entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PendingNotificationEntry> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the pending notification entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pending notification entries
	 * @param end the upper bound of the range of pending notification entries (not inclusive)
	 * @return the range of pending notification entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PendingNotificationEntry> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the pending notification entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pending notification entries
	 * @param end the upper bound of the range of pending notification entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of pending notification entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PendingNotificationEntry> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PendingNotificationEntry> list = (List<PendingNotificationEntry>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PENDINGNOTIFICATIONENTRY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PENDINGNOTIFICATIONENTRY;

				if (pagination) {
					sql = sql.concat(PendingNotificationEntryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PendingNotificationEntry>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PendingNotificationEntry>(list);
				}
				else {
					list = (List<PendingNotificationEntry>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the pending notification entries from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PendingNotificationEntry pendingNotificationEntry : findAll()) {
			remove(pendingNotificationEntry);
		}
	}

	/**
	 * Returns the number of pending notification entries.
	 *
	 * @return the number of pending notification entries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PENDINGNOTIFICATIONENTRY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the pending notification entry persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PendingNotificationEntry>> listenersList = new ArrayList<ModelListener<PendingNotificationEntry>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PendingNotificationEntry>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PendingNotificationEntryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PENDINGNOTIFICATIONENTRY = "SELECT pendingNotificationEntry FROM PendingNotificationEntry pendingNotificationEntry";
	private static final String _SQL_COUNT_PENDINGNOTIFICATIONENTRY = "SELECT COUNT(pendingNotificationEntry) FROM PendingNotificationEntry pendingNotificationEntry";
	private static final String _ORDER_BY_ENTITY_ALIAS = "pendingNotificationEntry.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PendingNotificationEntry exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PendingNotificationEntryPersistenceImpl.class);
	private static PendingNotificationEntry _nullPendingNotificationEntry = new PendingNotificationEntryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PendingNotificationEntry> toCacheModel() {
				return _nullPendingNotificationEntryCacheModel;
			}
		};

	private static CacheModel<PendingNotificationEntry> _nullPendingNotificationEntryCacheModel =
		new CacheModel<PendingNotificationEntry>() {
			@Override
			public PendingNotificationEntry toEntityModel() {
				return _nullPendingNotificationEntry;
			}
		};
}