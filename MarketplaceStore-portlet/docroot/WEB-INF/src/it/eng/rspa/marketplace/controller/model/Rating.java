package it.eng.rspa.marketplace.controller.model;

public class Rating{
	
	private int myscore;
	private int avgscore;
	private int nscores;
	
	public Rating(int myscore, int avgscore, int nscores){
		this.setMyscore(myscore);
		this.setAvgscore(avgscore);
		this.setNscores(nscores);
	}
	
	public Rating() {
		this.setMyscore(0);
		this.setAvgscore(0);
		this.setNscores(0);
	}
	
	public int getMyscore() {
		return myscore;
	}
	public void setMyscore(int myscore) {
		this.myscore = myscore;
	}
	public int getAvgscore() {
		return avgscore;
	}
	public void setAvgscore(int avgscore) {
		this.avgscore = avgscore;
	}

	public int getNscores() {
		return nscores;
	}
	public void setNscores(int nscores) {
		this.nscores = nscores;
	}
	
}
