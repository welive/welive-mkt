/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author eng
 * @see SearchServiceHttp
 * @see it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil
 * @generated
 */
public class SearchServiceSoap {
	public static java.lang.String getAllArtefatti(java.lang.String pilotID,
		java.lang.String artefactTypes) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getAllArtefatti(pilotID,
					artefactTypes);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getAllArtefatti(java.lang.String pilotID,
		java.lang.String artefactTypes, java.lang.String level)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getAllArtefatti(pilotID,
					artefactTypes, level);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getAllArtefattiV2(java.lang.String pilotID,
		java.lang.String artefactTypes) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getAllArtefattiV2(pilotID,
					artefactTypes);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getArtefattiByKeywords(keywords,
					artefactTypes, pilot);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot, java.lang.String level)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getArtefattiByKeywords(keywords,
					artefactTypes, pilot, level);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getArtefattiByKeywordsV2(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getArtefattiByKeywordsV2(keywords,
					artefactTypes, pilot);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getModelsById(java.lang.String[] ids)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getModelsById(ids);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getArtefact(long artefactid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getArtefact(artefactid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getArtefactV2(long artefactid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.getArtefactV2(artefactid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String createDataset(java.lang.String datasetId,
		java.lang.String title, java.lang.String description,
		java.lang.String resourceRdf, java.lang.String webpage,
		java.lang.String ccUserId, java.lang.String providerName,
		java.lang.String language) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.createDataset(datasetId,
					title, description, resourceRdf, webpage, ccUserId,
					providerName, language);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String createService(java.lang.String title,
		java.lang.String description, java.lang.String category,
		java.lang.String resourceRdf, java.lang.String endpoint,
		java.lang.String webpage, java.lang.String ccUserId,
		java.lang.String providerName, java.lang.String language)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.createService(title,
					description, category, resourceRdf, endpoint, webpage,
					ccUserId, providerName, language);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String setupArtefact(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.setupArtefact(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateDataset(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.updateDataset(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String newDataset_v2(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.newDataset_v2(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deleteartefact(long artefactid)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.deleteartefact(artefactid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateArtefact(long artefactid,
		java.lang.String body) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = SearchServiceUtil.updateArtefact(artefactid,
					body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(SearchServiceSoap.class);
}