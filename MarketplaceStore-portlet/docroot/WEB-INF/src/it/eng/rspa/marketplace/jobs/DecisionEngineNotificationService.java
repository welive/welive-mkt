package it.eng.rspa.marketplace.jobs;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;

import javax.ws.rs.core.MediaType;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class DecisionEngineNotificationService {

	private static Log _log = LogFactoryUtil.getLog(DecisionEngineNotificationService.class);
	
	public static boolean sendNotification(String method, Artefatto a, JSONObject body){
		
		long catId = a.getCategoriamkpId();
		String t = "AndroidApplication";
		try { t = CategoriaLocalServiceUtil.getCategoria(catId).getNomeCategoria().replaceAll(" ", "");} 
		catch (Exception e) {
			t = "BuildingBlock";
		}
		String type = PortletProps.get("Category.DEMapping."+t);
		
		if (Validator.isNotNull(type)){
			return sendNotificationRest ( method,  a,  body, type);
		}else{
			
			//workaround if category is null, try all 3 categories
			
			boolean bBB = sendNotificationRest ( method,  a,  body, PortletProps.get("Category.DEMapping.OSGibundle"));//building-block 
			boolean bApp = sendNotificationRest ( method,  a,  body, PortletProps.get("Category.DEMapping.AndroidApplication"));//app
			boolean bDat = sendNotificationRest ( method,  a,  body, PortletProps.get("Category.DEMapping.Dataset"));//dataset
			
			boolean retBtry = bBB || bApp || bDat;
			
			return retBtry;
			
		}
		
	}
	
	/**
	 * @param method
	 * @param a
	 * @param body
	 * @param type
	 * @return
	 */
	private static boolean sendNotificationRest (String method, Artefatto a, JSONObject body, String type){
		
		Client client = Client.create();
		client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
		
		WebResource.Builder webResource = client.resource(ConfUtil.getString("de.url")+type+"/"+a.getArtefattoId()).type(MediaType.APPLICATION_JSON);
		
		try{
			if(method.equalsIgnoreCase("put")){
				ClientResponse resp = webResource.put(ClientResponse.class, body.toString());
				if(resp.getStatus()>=300){
					throw new Exception("Sending " + body.toString() + ".\nReceived response code "+resp.getStatus() + " and body "+resp.getEntity(String.class));
				}
				String respString = resp.getEntity(String.class);
				_log.info("Decision engine responded: "+respString);
				
				return true;
			}
			else if(method.equalsIgnoreCase("delete")){
				ClientResponse resp = webResource.delete(ClientResponse.class);
				if(resp.getStatus()>=300){
					throw new Exception("Sending " + body + ".\nReceived response code "+resp.getStatus() + " and body "+resp.getEntity(String.class));
				}
				
				return true;
			}
		}
		catch(Exception e){
			_log.error(e.getClass() + " - Error while sending notification [" + method + "] " + ConfUtil.getString("de.url") + type + "/" + a.getArtefattoId()+": "+": "+e.getMessage());
			return false;
		}
		_log.warn("\tUndefined http method "+method+".");
		
		return false;
		
	}
	
	
}
