package it.eng.rspa.marketplace.utils;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public abstract class RestUtils {

	public static String consumePost(String url, Object body) throws Exception{
		
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, body);
		
		if(resp.getStatus()!=200 && resp.getStatus()!=301)
			throw new Exception("URL "+url+" responded with status "+resp.getStatus());
		
		String r = resp.getEntity(String.class);
		System.out.println(r);
		
		return r;
		
	}
	
	public static String consumeGet(String url) throws Exception{
		
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		
		if(resp.getStatus()!=200 && resp.getStatus()!=301)
			throw new Exception("URL "+url+" responded with status "+resp.getStatus());
		
		String r = resp.getEntity(String.class);
//		System.out.println(r);
		
		return r;
		
	}
	
	public static String consumeGet(String url, HTTPBasicAuthFilter token) throws Exception{
		
		Client client = Client.create();
		client.addFilter(token);
		WebResource webResource = client.resource(url);
		ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		
		if(resp.getStatus()!=200 && resp.getStatus()!=301)
			throw new Exception("URL "+url+" responded with status "+resp.getStatus());
		
		String r = resp.getEntity(String.class);
		System.out.println(r);
		
		return r;
		
	}
	
}
