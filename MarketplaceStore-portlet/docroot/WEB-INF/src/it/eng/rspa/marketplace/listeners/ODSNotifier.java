package it.eng.rspa.marketplace.listeners;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;
import it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryImpl;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.PendingNotificationEntryLocalServiceUtil;
import it.eng.rspa.marketplace.listeners.model.WeLiveComponent;
import it.eng.rspa.marketplace.listeners.model.WeLiveNotificationAction;

public class ODSNotifier implements iArtefactListener {

	private static String datasetCatName = PortletProps.get("Category.dataset");
	
	@Override
	public void onArtefactPublish(Artefatto a) {
		return;
	}

	@Override
	public void onArtefactDelete(Artefatto a) {
		Categoria cat = CategoriaLocalServiceUtil.getCategoriaByName(datasetCatName);
		long datasetCatId = cat.getIdCategoria();
		
		if(datasetCatId != a.getCategoriamkpId())
			return;
		
		try{
			PendingNotificationEntry pne = new PendingNotificationEntryImpl();
			pne.setNotificationId(CounterLocalServiceUtil.increment(PendingNotificationEntry.class.getName()));
			pne.setArtefactId(a.getArtefattoId());
			pne.setAction(WeLiveNotificationAction.ArtefactRemoved.toString());
			pne.setTargetComponent(WeLiveComponent.ODS.toString());
			pne.setTimestamp(GregorianCalendar.getInstance().getTimeInMillis());
			
			Map<String, String> options = new HashMap<String,String>();
			
			User u = UserLocalServiceUtil.getUser(a.getUserId());
			String sCCuid = u.getExpandoBridge().getAttribute("CCUserID").toString();
			options.put("ccuid", sCCuid);
			
			String jopt = (new Gson()).toJson(options);
			pne.setOptions(jopt);
			
			PendingNotificationEntryLocalServiceUtil.addPendingNotificationEntry(pne);
			
			System.out.println("Storing: "+pne);
		}
		catch(Exception e){
			System.out.println("Unable to add the artefact to the pending notification registry.\n"+e.getClass()+" "+e.getMessage());
		}
		
		return;
	}

	@Override
	public void onArtefactUpdate(Artefatto a) {
		return;
	}

}
