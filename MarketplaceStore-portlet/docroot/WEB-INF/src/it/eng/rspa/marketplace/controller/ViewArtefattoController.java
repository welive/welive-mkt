package it.eng.rspa.marketplace.controller;

import it.eng.metamodel.definitions.Artefact;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.controller.model.Rating;
import it.eng.rspa.marketplace.controller.model.Resource;
import it.eng.rspa.marketplace.utils.RestUtils;
import it.eng.rspa.usdl.model.ServiceOffering;
import it.eng.rspa.usdl.model.TermsAndCondition;
import it.eng.rspa.usdl.utils.MyUtils;
import it.eng.rspa.usdl.utils.RDFUtils;
import it.eng.sesame.Crud;
import it.eng.sesame.ReaderRDF;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class ViewArtefattoController extends CommonController{
	
	private static Log _log = LogFactoryUtil.getLog(ViewArtefattoController.class);

	private static JSONObject getDatasetRating(User user, Artefatto a) 
			throws Exception{
		
		String sResp = "";
		if(user==null || user.isDefaultUser()){
			sResp = RestUtils.consumeGet(ConfUtil.getString("ods.url")+a.getEId());
		}
		else{
			Long ccuid = Long.parseLong(user.getExpandoBridge().getAttribute("CCUserID").toString());
			
			sResp = RestUtils.consumeGet(ConfUtil.getString("ods.url")+a.getEId()+"/rating/user/"+ccuid, MyMarketplaceUtility.getBasicAuthenticationUser());
		}
		
		JSONObject jResp = JSONFactoryUtil.createJSONObject(sResp);
		
		return jResp;
	}
	
	private static JSONObject getDatasetMetadata(Artefatto a)
			throws Exception{
		
		String uri = ConfUtil.getString("ods.url")+a.getEId();
		String sResp = RestUtils.consumeGet(uri, MyMarketplaceUtility.getBasicAuthenticationUser());
		
		return JSONFactoryUtil.createJSONObject(sResp);
	}
	
	public static Set<Resource> getResources(Artefatto a) {
		
		Set<Resource> out = new HashSet<Resource>();
		try{
			
			JSONObject jResp = getDatasetMetadata(a);
			
			if(!jResp.getBoolean("success") || !jResp.has("result")){
				throw new Exception("Unable to retrieve the dataset resources");
			}
			
			JSONObject result = jResp.getJSONObject("result");
			if(!result.has("resources")){
				throw new Exception("Unable to retrieve the dataset resources");
			}
			
			JSONArray resources = result.getJSONArray("resources");
			for(int i =0; i<resources.length(); i++){
				try{
					JSONObject jres = resources.getJSONObject(i);
					
					String name = jres.getString("name");
					String description = jres.getString("description");
					String url = jres.getString("url");
					
					Resource r = new Resource();
					r.setDescription(description);
					r.setName(name);
					r.setUrl(url);
					
					out.add(r);
				}
				catch(Exception e){
					_log.warn(e);
				}
			}
		}
		catch(Exception e){
			out = new HashSet<Resource>();
			_log.error(e);
		}
		
		return out;
		
	}
	
	public static Rating alignDatasetRating(Artefatto a, User u){
		
		int avScore = a.getExtRating();
		Rating out = new Rating(0, avScore, 1);
		
		try{
			
			JSONObject jResp = getDatasetRating(u, a);
			
			if(jResp.getBoolean("success") && jResp.has("result")){
				JSONObject result = jResp.getJSONObject("result");
				if(result.has("avg_rating")){ 
					try{
					    avScore = (int)Math.round(result.getDouble("avg_rating"));
					    a.setExtRating(avScore);
					    ArtefattoLocalServiceUtil.updateArtefatto(a);
					    out.setAvgscore(avScore);
					}
					catch(Exception e){ _log.warn(e); }
				}
				else if(result.has("ratings")){
					try{
						avScore = (int)Math.round(result.getDouble("ratings"));
						a.setExtRating(avScore);
						ArtefattoLocalServiceUtil.updateArtefatto(a);
						out.setAvgscore(avScore);
					}
					catch(Exception e){ _log.warn(e); }
				}
				
				if(result.has("rating count")){
					try{ out.setNscores(result.getInt("rating count")); }
					catch(Exception e){ _log.warn(e); }
				}
				
				if(result.has("user_rating")){
					try{
						JSONObject userrating = result.getJSONObject("user_rating");
						if(userrating.has("rating")){
							out.setMyscore(userrating.getInt("rating"));
						}
					}
					catch(Exception e){ _log.warn(e); }
				}
			}
		}
		catch(Exception e){ _log.warn(e); }
		
		return out;
	}
	
	public static void action(HttpServletRequest request){
		User currentUser = null;
		try{ currentUser = PortalUtil.getUser(request);}
		catch(Exception e){ _log.error(e.getClass()+": "+e.getMessage()); }
		
		boolean userIsLoggedIn = currentUser!=null && !currentUser.isDefaultUser();
		
		try{
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);			
			String remoteInterface = themeDisplay.getPortalURL() + themeDisplay.getPathContext();
			
			String pathDocumentLibrary= StringPool.SLASH+"documents"+StringPool.SLASH
			+ themeDisplay.getScopeGroupId() + StringPool.SLASH; 

			String resourcePrimKey = request.getParameter("resourcePrimKey");
			if (resourcePrimKey==null){ resourcePrimKey=request.getAttribute("resourcePrimKey")+""; }
			long artefattoId= Long.parseLong(resourcePrimKey);
			Artefatto artefatto = ArtefattoLocalServiceUtil.getArtefatto(artefattoId);
			
			String odsResURL = MyUtils.getODSUrl(themeDisplay, artefatto.getEId());
			
			
			if(artefatto.getStatus() == WorkflowConstants.STATUS_IN_TRASH)
				throw new NoSuchArtefattoException("Artefact "+artefattoId+" has been deleted");
			
			String categoryName = "Other";
			if(Validator.isNotNull(artefatto.getCategoriamkpId()) && artefatto.getCategoriamkpId()>=0){
				categoryName = Category.getMapById().get(artefatto.getCategoriamkpId()).getNomeCategoria();				
			}
			String[] appsNames = PortletProps.get("Category.catsOfType.psa").split(",");
			String[] dsNames = PortletProps.get("Category.catsOfType.ds").split(",");
			Set<Long> appsCatIds = new HashSet<Long>();
			for(String id:appsNames){
				appsCatIds.add(Category.getMapByName().get(id).getIdCategoria()); 
			}
			Set<Long> dsCatIds = new HashSet<Long>();
			for(String id:dsNames){ 
				dsCatIds.add(Category.getMapByName().get(id).getIdCategoria()); 
			}
			
			boolean isPSA = appsCatIds.contains(new Long(artefatto.getCategoriamkpId()));
			boolean isDataset = dsCatIds.contains(new Long(artefatto.getCategoriamkpId()));
			boolean isDraft = (artefatto.getStatus() == WorkflowConstants.STATUS_DRAFT);
			boolean isPublished = (artefatto.getStatus() == WorkflowConstants.STATUS_APPROVED);
			boolean isPublisher = false;
			boolean isOwner = false;
			boolean isAdmin = false;
			
			User autore = null; 
			try{ autore = UserLocalServiceUtil.getUser(artefatto.getUserId()); }
			catch(Exception e){
				e.printStackTrace();
			}
			
			boolean inOrganization = false;
			if(currentUser!=null && !currentUser.isDefaultUser()){
				isPublisher = (currentUser.getUserId() == artefatto.getUserId());
				isAdmin = false;
				try{
					Role admin = RoleLocalServiceUtil.getRole(PortalUtil.getCompanyId(request), PortletProps.get("role.admin"));
					isAdmin = currentUser.getRoles().contains(admin);
				}
				catch(Exception e){ e.printStackTrace(); }
				
				try{
					List<ArtifactOrganizations> artorgs = ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByArtifactId(artefattoId);
					if(!artorgs.isEmpty()){
						Organization org = OrganizationLocalServiceUtil.getOrganization(artorgs.get(0).getOrganizationId());
						inOrganization = currentUser.getOrganizations().contains(org);
					}
				}
				catch(Exception e){ e.printStackTrace(); }
				
				if(currentUser.getFullName().trim().equalsIgnoreCase(artefatto.getOwner().trim()) ||
				   currentUser.getUserId() == artefatto.getUserId()
				){
					isOwner = true;
				}
				else{
					try{
						Role r_cLeader = RoleLocalServiceUtil.getRole(PortalUtil.getCompanyId(request), PortletProps.get("role.companyleader"));
						isOwner = inOrganization && currentUser.getRoles().contains(r_cLeader);
					}
					catch(Exception e){ e.printStackTrace(); }
				}
			}
			
			if(!isPublished && !isPublisher && !isAdmin && !inOrganization){
				throw new NoSuchArtefattoException("");
			}
			
			if(artefatto.isIsPrivate() && !isPublisher && !isAdmin && !inOrganization){
				throw new NoSuchArtefattoException("");
			}
			
		 	String resourceRDF = artefatto.getResourceRDF();
		 	String lusdlmodel = artefatto.getLusdlmodel();
			if(lusdlmodel.equals("") && !resourceRDF.equals("")){
			 	Crud c = new Crud(); 
				ByteArrayOutputStream os = c.exportRDFOutputStream(resourceRDF);
				ReaderRDF readerRDF = new ReaderRDF(os);
				//il readerRDF viene usato per valorizzare i capi dei tab
				request.setAttribute("readerRDF", readerRDF);
		 	}
		 	
			List <ImmagineArt>listaCoppie=new ArrayList<ImmagineArt>();
			try{ listaCoppie=ImmagineArtLocalServiceUtil.findByArtefattoId(artefattoId); }
			catch(Exception e){
				artefattoId=-1;
				e.printStackTrace();
			}

			String imgURl = request.getContextPath()+"/icons/";
			String catname = "other";
			if(artefatto.getCategoriamkpId()>0){
				catname = CategoriaLocalServiceUtil.getCategoria(artefatto.getCategoriamkpId()).getNomeCategoria();
			}
			
			if(catname.equalsIgnoreCase(PortletProps.get("Category.rest")))
				imgURl += "icone_servizi_rest-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.soap")))
				imgURl += "icone_servizi_soap-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.dataset")))
				imgURl += "icone_dataset-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appmobile")))
				imgURl += "icone_app_android-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appweb")))
				imgURl += "icone_app_web-01.png";
			else
				imgURl += "defaultIcon.png";
			
			ImmagineArt coverimg = null;
			DLFileEntry dlf = null;
			
			try { 
				if(artefatto.getImgId()>0){
					coverimg=ImmagineArtLocalServiceUtil.getImmagineArt(artefatto.getImgId()); 
				}
				else if (ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).size() > 0) { 
					coverimg = ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).get(0);
				}
			
				if(coverimg!=null){
					dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(coverimg.getDlImageId());
					imgURl = remoteInterface + pathDocumentLibrary + dlf.getUuid();
					
					//Non mostro l'immagine rappresentativa nel carosello
					if(listaCoppie!=null && listaCoppie.size()>0){
						List<ImmagineArt> imgs = new ArrayList<ImmagineArt>(listaCoppie);
						imgs.remove(coverimg);
						listaCoppie = new ArrayList<ImmagineArt>(imgs);
					}
				}
			}
			catch (Exception e) { 
				e.printStackTrace();
			}	
			String authorName = "Unregistered User";
			String authorUsername = null;
			boolean isAuthorityOrAdminOrDeletedUser = false;
			if(autore!=null){
				authorName = autore.getFullName();
				authorUsername = autore.getScreenName();
				isAuthorityOrAdminOrDeletedUser = MyMarketplaceUtility.isAuthority(autore) || MyMarketplaceUtility.isDeletedUser(autore.getUserId()) || PortalUtil.isOmniadmin(autore.getUserId());
				
			}
			
			
			String catsOfType = PortletProps.get("Category.catsOfType.inverse."+categoryName.replaceAll(" ", ""));
			
			
			/** Get licenses and Offerings from rdf lusdl metamodel **/
			
			Set<ServiceOffering> sos = new HashSet<ServiceOffering>();
			Set<TermsAndCondition> terms = new HashSet<TermsAndCondition>();
			ReaderRDF readerRDF = null;
			Object paramReader = request.getAttribute("readerRDF");
			if(paramReader!=null && paramReader instanceof ReaderRDF){
				readerRDF=(ReaderRDF)request.getAttribute("readerRDF");
				sos = RDFUtils.getServiceOfferings(readerRDF);
				terms = RDFUtils.getTerms(readerRDF);
			}
			
			
			/** Get licenses and Offerings from json lusdl metamodel **/
			Boolean hasmetamodel = false;
			Set<License> licenses = new HashSet<License>();
			Set<it.eng.metamodel.definitions.ServiceOffering> offerings = new HashSet<it.eng.metamodel.definitions.ServiceOffering>();
			String lusdl = artefatto.getLusdlmodel();
			Artefact lusdlart = null;
			String docPage ="";
			
			if(Validator.isNotNull(lusdl) && !lusdl.trim().equalsIgnoreCase("")){
				try{
					
					if(isDataset){
						lusdlart = MetamodelParser.parseDataset(lusdl);
						hasmetamodel = true;
					}
					else if(isPSA){
						lusdlart = MetamodelParser.parsePSA(lusdl);
						hasmetamodel = true;
					}
					else{
						ParserResponse parseresponse = MetamodelParser.parse(lusdl, MetamodelParser.SourceType.METAMODEL);
						if(Validator.isNotNull(parseresponse))
							lusdlart = parseresponse.getBuildingBlock();
						hasmetamodel = true;
					}
					
					//TODO Rimuovere una volta fixato il parser
					if(Validator.isNotNull(lusdlart)) {
						licenses.addAll(lusdlart.getLegalConditions());
						offerings.addAll(lusdlart.getServiceOfferings());
						docPage = lusdlart.getPage();
						
					}
					
				}
				catch(Exception e){
					_log.error(e);
				}
				
			}
			
			/** Retrieve related Idea metadata **/
			request.setAttribute("hasidea", false);
			List<CLSArtifacts> idearts = CLSArtifactsLocalServiceUtil.getArtifactsByArtifactId(artefattoId);
			Map<Long, String> ideas = new HashMap<Long, String>();
			if(Validator.isNotNull(idearts) && !idearts.isEmpty()){
				request.setAttribute("hasidea", true);
				for(CLSArtifacts art : idearts){
					try{
						CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(art.getIdeaId());
						ideas.put(art.getIdeaId(), idea.getIdeaTitle());
					}
					catch(Exception e){ _log.error(e); }
				}
			}
			request.setAttribute("ideas", ideas);
			
			/** Retrieve Dependencies **/
			List<Dependencies> deps = DependenciesLocalServiceUtil.getDependenciesByArtefactId(artefattoId);
			List<HashMap<String, String>> depArts = new ArrayList<HashMap<String, String>>();
			if(Validator.isNotNull(deps)){
				for(Dependencies d : deps){
					try{
						Artefatto depArt = ArtefattoLocalServiceUtil.getArtefatto(d.getDependsFrom());
						HashMap<String, String> jdepArt = new HashMap<String, String>();
								jdepArt.put("title", depArt.getTitle());
								jdepArt.put("artefattoId", String.valueOf(depArt.getArtefattoId()));
								
						//Recupero l'immagine dell'artefatto
						ImmagineArt imgart = null;
						String imgUrl = "";
						try {
							if(depArt.getImgId() == -1){
								
								List<ImmagineArt> imgsart = ImmagineArtLocalServiceUtil.getService().findByArtefattoId(depArt.getArtefattoId());
								if(!imgsart.isEmpty()){
									imgart = imgsart.get(0);
								}
								
							}
							else{ imgart = ImmagineArtLocalServiceUtil.getImmagineArt(depArt.getImgId()); }
							
							
							if(Validator.isNotNull(imgart)){
								long dlid = imgart.getDlImageId();
								imgUrl = pathDocumentLibrary + DLFileEntryLocalServiceUtil.getDLFileEntry(dlid).getUuid();
							}
							else{
								throw new Exception("No image specified for the artefact");
							}
							
						}
						catch(Exception e){
							
							imgUrl = request.getContextPath()+"/icons/";
							
							String depCatname ="";
							if(depArt.getCategoriamkpId()>0){
								 depCatname = CategoriaLocalServiceUtil.getCategoria(depArt.getCategoriamkpId()).getNomeCategoria();
							}
							
							
							
							if(depCatname.equalsIgnoreCase(PortletProps.get("Category.rest")))
								imgUrl += "icone_servizi_rest-01.png";
							else if(depCatname.equalsIgnoreCase(PortletProps.get("Category.soap")))
								imgUrl += "icone_servizi_soap-01.png";
							else if(depCatname.equalsIgnoreCase(PortletProps.get("Category.dataset")))
								imgUrl += "icone_dataset-01.png";
							else if(depCatname.equalsIgnoreCase(PortletProps.get("Category.appmobile")))
								imgUrl += "icone_app_android-01.png";
							else if(depCatname.equalsIgnoreCase(PortletProps.get("Category.appweb")))
								imgUrl += "icone_app_web-01.png";
							else
								imgUrl += "defaultIcon.png";
							
						}	
						
						try{ jdepArt.put("imgUrl", imgUrl); }
						catch(Exception e){ e.printStackTrace(); }
								
						depArts.add(jdepArt);
					}
					catch(Exception e){ _log.warn(e); }
				}
			}
			request.setAttribute("depArtefacts", depArts);
			
			int level = -1;
			if(artefatto.getStatus() == WorkflowConstants.STATUS_APPROVED){
				try{ level = ArtifactLevelsLocalServiceUtil.getArtifactLevels(artefattoId).getLevel(); }
				catch(Exception e){ _log.warn(e); }
			}
			request.setAttribute("level", level);
			
			/** Get recommendations **/
			List<Artefatto> sugg = new ArrayList<Artefatto>();
			try{ sugg = MyMarketplaceUtility.getSuggestedArtefacts(artefatto.getArtefattoId()); }
			catch(Exception e){ _log.warn(e); }
			
			request.setAttribute("abs", StringEscapeUtils.unescapeHtml4(artefatto.getAbstractDescription())
																				.replaceAll("&apos;","'")
																				.replaceAll("(\\\\n|\\\\r)+", " ")
																				.replaceAll("(\\\\t)+", " "));
			request.setAttribute("desc", StringEscapeUtils.unescapeHtml4(artefatto.getDescription())
																				.replaceAll("&apos;", "'")
																				.replaceAll("(\\\\n|\\\\r)+", " ")
																				.replaceAll("(\\\\t)+", " "));
			request.setAttribute("resourcePrimKey", resourcePrimKey);
			request.setAttribute("imgURl", imgURl);
			request.setAttribute("isPSA", isPSA);
			request.setAttribute("isDataset", isDataset);
			request.setAttribute("isDraft", isDraft);
			request.setAttribute("isPublished", isPublished);
			request.setAttribute("isPublisher", isPublisher);
			request.setAttribute("isAdmin", isAdmin);
			request.setAttribute("catsOfType", catsOfType);
			request.setAttribute("categoryName", categoryName);
			request.setAttribute("authorName", authorName);
			request.setAttribute("authorScreenName", authorUsername);
			request.setAttribute("artefatto", artefatto);
			request.setAttribute("sugg", sugg);
			request.setAttribute("listaCoppie", listaCoppie);
			request.setAttribute("sos", sos); //via rdf
			request.setAttribute("terms", terms); //via rdf
			request.setAttribute("inOrganization", inOrganization);
			request.setAttribute("isOwner", isOwner);
			request.setAttribute("eid", odsResURL );	
			
			request.setAttribute("lusdlartefact", lusdlart); //via json lusdl
			request.setAttribute("lusdlsos", offerings); //via json lusdl
			request.setAttribute("lusdllicenses", licenses); //via json lusdl
			request.setAttribute("docPage", docPage); //via json lusdl
			
			//utils
			request.setAttribute("hasmetamodel", hasmetamodel);
			request.setAttribute("remoteInterface", remoteInterface);
			request.setAttribute("pathDocumentLibrary", pathDocumentLibrary);
			request.setAttribute("restname", PortletProps.get("Category.rest"));
			request.setAttribute("soapname", PortletProps.get("Category.soap"));
			request.setAttribute("noname", "Other");
			request.setAttribute("webapp", PortletProps.get("Category.appweb"));
			request.setAttribute("appmobile", PortletProps.get("Category.appmobile"));
			request.setAttribute("dataset", PortletProps.get("Category.dataset"));
			request.setAttribute("userIsLoggedIn", userIsLoggedIn);	
			request.setAttribute("isAuthorityOrAdminOrDeletedUser", isAuthorityOrAdminOrDeletedUser);	
			
			
		}
		catch(NoSuchArtefattoException ae){
			request.setAttribute("notfound", true);
			_log.error(ae);
		}
		catch(Exception e){
			e.printStackTrace();
			_log.error(e); 
		}
	}
}
