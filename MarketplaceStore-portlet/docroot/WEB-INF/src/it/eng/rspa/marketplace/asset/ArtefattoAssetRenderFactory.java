package it.eng.rspa.marketplace.asset;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.BaseAssetRendererFactory;

public class ArtefattoAssetRenderFactory extends BaseAssetRendererFactory {
 
	    @Override
	    public AssetRenderer getAssetRenderer(long artefattoId, int type) throws PortalException, SystemException {
	    	Artefatto art =	ArtefattoLocalServiceUtil.getArtefatto(artefattoId);
	        return new ArtefattoAssetRenderer(art);
	    }

	    @Override
	    public String getClassName() { 
	        return Artefatto.class.getName();
	    }

	    @Override
	    public String getType() { 
	        return "Artefatto";
	    }
}
