package it.eng.rspa.marketplace.listeners.model;

public enum WeLiveNotificationAction {
	ArtefactPublished("ArtefactPublished"),
	ArtefactRemoved("ArtefactRemoved"),
	ArtefactModifed("ArtefactModifed"),
	ArtefactRated("ArtefactRated"),
	ArtefactRateRemoved("ArtefactRateRemoved"),
	ArtefactRateModifed("ArtefactRateModifed");	

    private final String text;

    /**
     * @param text
     */
    private WeLiveNotificationAction(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
