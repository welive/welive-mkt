/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the immagine art service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ImmagineArtPersistence
 * @see ImmagineArtUtil
 * @generated
 */
public class ImmagineArtPersistenceImpl extends BasePersistenceImpl<ImmagineArt>
	implements ImmagineArtPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ImmagineArtUtil} to access the immagine art persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ImmagineArtImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IMAGEIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByImageIdentifier",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IMAGEIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByImageIdentifier",
			new String[] { Long.class.getName() },
			ImmagineArtModelImpl.IMAGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IMAGEIDENTIFIER = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByImageIdentifier", new String[] { Long.class.getName() });

	/**
	 * Returns all the immagine arts where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @return the matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByImageIdentifier(long imageId)
		throws SystemException {
		return findByImageIdentifier(imageId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the immagine arts where imageId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param imageId the image ID
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @return the range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByImageIdentifier(long imageId, int start,
		int end) throws SystemException {
		return findByImageIdentifier(imageId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the immagine arts where imageId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param imageId the image ID
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByImageIdentifier(long imageId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IMAGEIDENTIFIER;
			finderArgs = new Object[] { imageId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IMAGEIDENTIFIER;
			finderArgs = new Object[] { imageId, start, end, orderByComparator };
		}

		List<ImmagineArt> list = (List<ImmagineArt>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ImmagineArt immagineArt : list) {
				if ((imageId != immagineArt.getImageId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_IMAGEIDENTIFIER_IMAGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ImmagineArtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(imageId);

				if (!pagination) {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ImmagineArt>(list);
				}
				else {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first immagine art in the ordered set where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByImageIdentifier_First(long imageId,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByImageIdentifier_First(imageId,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("imageId=");
		msg.append(imageId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the first immagine art in the ordered set where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByImageIdentifier_First(long imageId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ImmagineArt> list = findByImageIdentifier(imageId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last immagine art in the ordered set where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByImageIdentifier_Last(long imageId,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByImageIdentifier_Last(imageId,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("imageId=");
		msg.append(imageId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the last immagine art in the ordered set where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByImageIdentifier_Last(long imageId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByImageIdentifier(imageId);

		if (count == 0) {
			return null;
		}

		List<ImmagineArt> list = findByImageIdentifier(imageId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the immagine arts where imageId = &#63; from the database.
	 *
	 * @param imageId the image ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByImageIdentifier(long imageId) throws SystemException {
		for (ImmagineArt immagineArt : findByImageIdentifier(imageId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(immagineArt);
		}
	}

	/**
	 * Returns the number of immagine arts where imageId = &#63;.
	 *
	 * @param imageId the image ID
	 * @return the number of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByImageIdentifier(long imageId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IMAGEIDENTIFIER;

		Object[] finderArgs = new Object[] { imageId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_IMAGEIDENTIFIER_IMAGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(imageId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IMAGEIDENTIFIER_IMAGEID_2 = "immagineArt.imageId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDlImageIdentifier",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByDlImageIdentifier", new String[] { Long.class.getName() },
			ImmagineArtModelImpl.DLIMAGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DLIMAGEIDENTIFIER = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDlImageIdentifier", new String[] { Long.class.getName() });

	/**
	 * Returns all the immagine arts where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @return the matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByDlImageIdentifier(long dlImageId)
		throws SystemException {
		return findByDlImageIdentifier(dlImageId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the immagine arts where dlImageId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dlImageId the dl image ID
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @return the range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByDlImageIdentifier(long dlImageId, int start,
		int end) throws SystemException {
		return findByDlImageIdentifier(dlImageId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the immagine arts where dlImageId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dlImageId the dl image ID
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByDlImageIdentifier(long dlImageId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER;
			finderArgs = new Object[] { dlImageId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER;
			finderArgs = new Object[] { dlImageId, start, end, orderByComparator };
		}

		List<ImmagineArt> list = (List<ImmagineArt>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ImmagineArt immagineArt : list) {
				if ((dlImageId != immagineArt.getDlImageId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_DLIMAGEIDENTIFIER_DLIMAGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ImmagineArtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dlImageId);

				if (!pagination) {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ImmagineArt>(list);
				}
				else {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first immagine art in the ordered set where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByDlImageIdentifier_First(long dlImageId,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByDlImageIdentifier_First(dlImageId,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dlImageId=");
		msg.append(dlImageId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the first immagine art in the ordered set where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByDlImageIdentifier_First(long dlImageId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ImmagineArt> list = findByDlImageIdentifier(dlImageId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last immagine art in the ordered set where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByDlImageIdentifier_Last(long dlImageId,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByDlImageIdentifier_Last(dlImageId,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dlImageId=");
		msg.append(dlImageId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the last immagine art in the ordered set where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByDlImageIdentifier_Last(long dlImageId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByDlImageIdentifier(dlImageId);

		if (count == 0) {
			return null;
		}

		List<ImmagineArt> list = findByDlImageIdentifier(dlImageId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the immagine arts before and after the current immagine art in the ordered set where dlImageId = &#63;.
	 *
	 * @param imageId the primary key of the current immagine art
	 * @param dlImageId the dl image ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt[] findByDlImageIdentifier_PrevAndNext(long imageId,
		long dlImageId, OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = findByPrimaryKey(imageId);

		Session session = null;

		try {
			session = openSession();

			ImmagineArt[] array = new ImmagineArtImpl[3];

			array[0] = getByDlImageIdentifier_PrevAndNext(session, immagineArt,
					dlImageId, orderByComparator, true);

			array[1] = immagineArt;

			array[2] = getByDlImageIdentifier_PrevAndNext(session, immagineArt,
					dlImageId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ImmagineArt getByDlImageIdentifier_PrevAndNext(Session session,
		ImmagineArt immagineArt, long dlImageId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_IMMAGINEART_WHERE);

		query.append(_FINDER_COLUMN_DLIMAGEIDENTIFIER_DLIMAGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ImmagineArtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dlImageId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(immagineArt);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ImmagineArt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the immagine arts where dlImageId = &#63; from the database.
	 *
	 * @param dlImageId the dl image ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByDlImageIdentifier(long dlImageId)
		throws SystemException {
		for (ImmagineArt immagineArt : findByDlImageIdentifier(dlImageId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(immagineArt);
		}
	}

	/**
	 * Returns the number of immagine arts where dlImageId = &#63;.
	 *
	 * @param dlImageId the dl image ID
	 * @return the number of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDlImageIdentifier(long dlImageId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DLIMAGEIDENTIFIER;

		Object[] finderArgs = new Object[] { dlImageId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_DLIMAGEIDENTIFIER_DLIMAGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dlImageId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DLIMAGEIDENTIFIER_DLIMAGEID_2 = "immagineArt.dlImageId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByArtefattoIdentifier",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER =
		new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, ImmagineArtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByArtefattoIdentifier", new String[] { Long.class.getName() },
			ImmagineArtModelImpl.ARTEFATTOIDENT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTEFATTOIDENTIFIER = new FinderPath(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByArtefattoIdentifier", new String[] { Long.class.getName() });

	/**
	 * Returns all the immagine arts where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @return the matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByArtefattoIdentifier(long artefattoIdent)
		throws SystemException {
		return findByArtefattoIdentifier(artefattoIdent, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the immagine arts where artefattoIdent = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @return the range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByArtefattoIdentifier(long artefattoIdent,
		int start, int end) throws SystemException {
		return findByArtefattoIdentifier(artefattoIdent, start, end, null);
	}

	/**
	 * Returns an ordered range of all the immagine arts where artefattoIdent = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findByArtefattoIdentifier(long artefattoIdent,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER;
			finderArgs = new Object[] { artefattoIdent };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER;
			finderArgs = new Object[] {
					artefattoIdent,
					
					start, end, orderByComparator
				};
		}

		List<ImmagineArt> list = (List<ImmagineArt>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ImmagineArt immagineArt : list) {
				if ((artefattoIdent != immagineArt.getArtefattoIdent())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_ARTEFATTOIDENTIFIER_ARTEFATTOIDENT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ImmagineArtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefattoIdent);

				if (!pagination) {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ImmagineArt>(list);
				}
				else {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByArtefattoIdentifier_First(long artefattoIdent,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByArtefattoIdentifier_First(artefattoIdent,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artefattoIdent=");
		msg.append(artefattoIdent);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByArtefattoIdentifier_First(long artefattoIdent,
		OrderByComparator orderByComparator) throws SystemException {
		List<ImmagineArt> list = findByArtefattoIdentifier(artefattoIdent, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByArtefattoIdentifier_Last(long artefattoIdent,
		OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByArtefattoIdentifier_Last(artefattoIdent,
				orderByComparator);

		if (immagineArt != null) {
			return immagineArt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artefattoIdent=");
		msg.append(artefattoIdent);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchImmagineArtException(msg.toString());
	}

	/**
	 * Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByArtefattoIdentifier_Last(long artefattoIdent,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArtefattoIdentifier(artefattoIdent);

		if (count == 0) {
			return null;
		}

		List<ImmagineArt> list = findByArtefattoIdentifier(artefattoIdent,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the immagine arts before and after the current immagine art in the ordered set where artefattoIdent = &#63;.
	 *
	 * @param imageId the primary key of the current immagine art
	 * @param artefattoIdent the artefatto ident
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt[] findByArtefattoIdentifier_PrevAndNext(long imageId,
		long artefattoIdent, OrderByComparator orderByComparator)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = findByPrimaryKey(imageId);

		Session session = null;

		try {
			session = openSession();

			ImmagineArt[] array = new ImmagineArtImpl[3];

			array[0] = getByArtefattoIdentifier_PrevAndNext(session,
					immagineArt, artefattoIdent, orderByComparator, true);

			array[1] = immagineArt;

			array[2] = getByArtefattoIdentifier_PrevAndNext(session,
					immagineArt, artefattoIdent, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ImmagineArt getByArtefattoIdentifier_PrevAndNext(
		Session session, ImmagineArt immagineArt, long artefattoIdent,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_IMMAGINEART_WHERE);

		query.append(_FINDER_COLUMN_ARTEFATTOIDENTIFIER_ARTEFATTOIDENT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ImmagineArtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(artefattoIdent);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(immagineArt);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ImmagineArt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the immagine arts where artefattoIdent = &#63; from the database.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArtefattoIdentifier(long artefattoIdent)
		throws SystemException {
		for (ImmagineArt immagineArt : findByArtefattoIdentifier(
				artefattoIdent, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(immagineArt);
		}
	}

	/**
	 * Returns the number of immagine arts where artefattoIdent = &#63;.
	 *
	 * @param artefattoIdent the artefatto ident
	 * @return the number of matching immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArtefattoIdentifier(long artefattoIdent)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTEFATTOIDENTIFIER;

		Object[] finderArgs = new Object[] { artefattoIdent };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IMMAGINEART_WHERE);

			query.append(_FINDER_COLUMN_ARTEFATTOIDENTIFIER_ARTEFATTOIDENT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefattoIdent);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTEFATTOIDENTIFIER_ARTEFATTOIDENT_2 =
		"immagineArt.artefattoIdent = ?";

	public ImmagineArtPersistenceImpl() {
		setModelClass(ImmagineArt.class);
	}

	/**
	 * Caches the immagine art in the entity cache if it is enabled.
	 *
	 * @param immagineArt the immagine art
	 */
	@Override
	public void cacheResult(ImmagineArt immagineArt) {
		EntityCacheUtil.putResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtImpl.class, immagineArt.getPrimaryKey(), immagineArt);

		immagineArt.resetOriginalValues();
	}

	/**
	 * Caches the immagine arts in the entity cache if it is enabled.
	 *
	 * @param immagineArts the immagine arts
	 */
	@Override
	public void cacheResult(List<ImmagineArt> immagineArts) {
		for (ImmagineArt immagineArt : immagineArts) {
			if (EntityCacheUtil.getResult(
						ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
						ImmagineArtImpl.class, immagineArt.getPrimaryKey()) == null) {
				cacheResult(immagineArt);
			}
			else {
				immagineArt.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all immagine arts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ImmagineArtImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ImmagineArtImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the immagine art.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ImmagineArt immagineArt) {
		EntityCacheUtil.removeResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtImpl.class, immagineArt.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ImmagineArt> immagineArts) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ImmagineArt immagineArt : immagineArts) {
			EntityCacheUtil.removeResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
				ImmagineArtImpl.class, immagineArt.getPrimaryKey());
		}
	}

	/**
	 * Creates a new immagine art with the primary key. Does not add the immagine art to the database.
	 *
	 * @param imageId the primary key for the new immagine art
	 * @return the new immagine art
	 */
	@Override
	public ImmagineArt create(long imageId) {
		ImmagineArt immagineArt = new ImmagineArtImpl();

		immagineArt.setNew(true);
		immagineArt.setPrimaryKey(imageId);

		return immagineArt;
	}

	/**
	 * Removes the immagine art with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param imageId the primary key of the immagine art
	 * @return the immagine art that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt remove(long imageId)
		throws NoSuchImmagineArtException, SystemException {
		return remove((Serializable)imageId);
	}

	/**
	 * Removes the immagine art with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the immagine art
	 * @return the immagine art that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt remove(Serializable primaryKey)
		throws NoSuchImmagineArtException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ImmagineArt immagineArt = (ImmagineArt)session.get(ImmagineArtImpl.class,
					primaryKey);

			if (immagineArt == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchImmagineArtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(immagineArt);
		}
		catch (NoSuchImmagineArtException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ImmagineArt removeImpl(ImmagineArt immagineArt)
		throws SystemException {
		immagineArt = toUnwrappedModel(immagineArt);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(immagineArt)) {
				immagineArt = (ImmagineArt)session.get(ImmagineArtImpl.class,
						immagineArt.getPrimaryKeyObj());
			}

			if (immagineArt != null) {
				session.delete(immagineArt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (immagineArt != null) {
			clearCache(immagineArt);
		}

		return immagineArt;
	}

	@Override
	public ImmagineArt updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws SystemException {
		immagineArt = toUnwrappedModel(immagineArt);

		boolean isNew = immagineArt.isNew();

		ImmagineArtModelImpl immagineArtModelImpl = (ImmagineArtModelImpl)immagineArt;

		Session session = null;

		try {
			session = openSession();

			if (immagineArt.isNew()) {
				session.save(immagineArt);

				immagineArt.setNew(false);
			}
			else {
				session.merge(immagineArt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ImmagineArtModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((immagineArtModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IMAGEIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						immagineArtModelImpl.getOriginalImageId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IMAGEIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IMAGEIDENTIFIER,
					args);

				args = new Object[] { immagineArtModelImpl.getImageId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IMAGEIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IMAGEIDENTIFIER,
					args);
			}

			if ((immagineArtModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						immagineArtModelImpl.getOriginalDlImageId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DLIMAGEIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER,
					args);

				args = new Object[] { immagineArtModelImpl.getDlImageId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DLIMAGEIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DLIMAGEIDENTIFIER,
					args);
			}

			if ((immagineArtModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						immagineArtModelImpl.getOriginalArtefattoIdent()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTEFATTOIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER,
					args);

				args = new Object[] { immagineArtModelImpl.getArtefattoIdent() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTEFATTOIDENTIFIER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFATTOIDENTIFIER,
					args);
			}
		}

		EntityCacheUtil.putResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
			ImmagineArtImpl.class, immagineArt.getPrimaryKey(), immagineArt);

		return immagineArt;
	}

	protected ImmagineArt toUnwrappedModel(ImmagineArt immagineArt) {
		if (immagineArt instanceof ImmagineArtImpl) {
			return immagineArt;
		}

		ImmagineArtImpl immagineArtImpl = new ImmagineArtImpl();

		immagineArtImpl.setNew(immagineArt.isNew());
		immagineArtImpl.setPrimaryKey(immagineArt.getPrimaryKey());

		immagineArtImpl.setImageId(immagineArt.getImageId());
		immagineArtImpl.setDlImageId(immagineArt.getDlImageId());
		immagineArtImpl.setDescrizione(immagineArt.getDescrizione());
		immagineArtImpl.setArtefattoIdent(immagineArt.getArtefattoIdent());

		return immagineArtImpl;
	}

	/**
	 * Returns the immagine art with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the immagine art
	 * @return the immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByPrimaryKey(Serializable primaryKey)
		throws NoSuchImmagineArtException, SystemException {
		ImmagineArt immagineArt = fetchByPrimaryKey(primaryKey);

		if (immagineArt == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchImmagineArtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return immagineArt;
	}

	/**
	 * Returns the immagine art with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException} if it could not be found.
	 *
	 * @param imageId the primary key of the immagine art
	 * @return the immagine art
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt findByPrimaryKey(long imageId)
		throws NoSuchImmagineArtException, SystemException {
		return findByPrimaryKey((Serializable)imageId);
	}

	/**
	 * Returns the immagine art with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the immagine art
	 * @return the immagine art, or <code>null</code> if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ImmagineArt immagineArt = (ImmagineArt)EntityCacheUtil.getResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
				ImmagineArtImpl.class, primaryKey);

		if (immagineArt == _nullImmagineArt) {
			return null;
		}

		if (immagineArt == null) {
			Session session = null;

			try {
				session = openSession();

				immagineArt = (ImmagineArt)session.get(ImmagineArtImpl.class,
						primaryKey);

				if (immagineArt != null) {
					cacheResult(immagineArt);
				}
				else {
					EntityCacheUtil.putResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
						ImmagineArtImpl.class, primaryKey, _nullImmagineArt);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ImmagineArtModelImpl.ENTITY_CACHE_ENABLED,
					ImmagineArtImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return immagineArt;
	}

	/**
	 * Returns the immagine art with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param imageId the primary key of the immagine art
	 * @return the immagine art, or <code>null</code> if a immagine art with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImmagineArt fetchByPrimaryKey(long imageId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)imageId);
	}

	/**
	 * Returns all the immagine arts.
	 *
	 * @return the immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the immagine arts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @return the range of immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the immagine arts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of immagine arts
	 * @param end the upper bound of the range of immagine arts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImmagineArt> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ImmagineArt> list = (List<ImmagineArt>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_IMMAGINEART);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_IMMAGINEART;

				if (pagination) {
					sql = sql.concat(ImmagineArtModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ImmagineArt>(list);
				}
				else {
					list = (List<ImmagineArt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the immagine arts from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ImmagineArt immagineArt : findAll()) {
			remove(immagineArt);
		}
	}

	/**
	 * Returns the number of immagine arts.
	 *
	 * @return the number of immagine arts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_IMMAGINEART);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the immagine art persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.ImmagineArt")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ImmagineArt>> listenersList = new ArrayList<ModelListener<ImmagineArt>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ImmagineArt>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ImmagineArtImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_IMMAGINEART = "SELECT immagineArt FROM ImmagineArt immagineArt";
	private static final String _SQL_SELECT_IMMAGINEART_WHERE = "SELECT immagineArt FROM ImmagineArt immagineArt WHERE ";
	private static final String _SQL_COUNT_IMMAGINEART = "SELECT COUNT(immagineArt) FROM ImmagineArt immagineArt";
	private static final String _SQL_COUNT_IMMAGINEART_WHERE = "SELECT COUNT(immagineArt) FROM ImmagineArt immagineArt WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "immagineArt.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ImmagineArt exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ImmagineArt exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ImmagineArtPersistenceImpl.class);
	private static ImmagineArt _nullImmagineArt = new ImmagineArtImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ImmagineArt> toCacheModel() {
				return _nullImmagineArtCacheModel;
			}
		};

	private static CacheModel<ImmagineArt> _nullImmagineArtCacheModel = new CacheModel<ImmagineArt>() {
			@Override
			public ImmagineArt toEntityModel() {
				return _nullImmagineArt;
			}
		};
}