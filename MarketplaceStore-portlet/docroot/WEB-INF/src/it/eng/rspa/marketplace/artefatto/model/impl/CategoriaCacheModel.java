/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.Categoria;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Categoria in entity cache.
 *
 * @author eng
 * @see Categoria
 * @generated
 */
public class CategoriaCacheModel implements CacheModel<Categoria>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idCategoria=");
		sb.append(idCategoria);
		sb.append(", nomeCategoria=");
		sb.append(nomeCategoria);
		sb.append(", supports=");
		sb.append(supports);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Categoria toEntityModel() {
		CategoriaImpl categoriaImpl = new CategoriaImpl();

		categoriaImpl.setIdCategoria(idCategoria);

		if (nomeCategoria == null) {
			categoriaImpl.setNomeCategoria(StringPool.BLANK);
		}
		else {
			categoriaImpl.setNomeCategoria(nomeCategoria);
		}

		if (supports == null) {
			categoriaImpl.setSupports(StringPool.BLANK);
		}
		else {
			categoriaImpl.setSupports(supports);
		}

		categoriaImpl.resetOriginalValues();

		return categoriaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idCategoria = objectInput.readLong();
		nomeCategoria = objectInput.readUTF();
		supports = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idCategoria);

		if (nomeCategoria == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nomeCategoria);
		}

		if (supports == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(supports);
		}
	}

	public long idCategoria;
	public String nomeCategoria;
	public String supports;
}