/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.util.List;

import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.service.base.CategoriaLocalServiceBaseImpl;

/**
 * The implementation of the categoria local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.CategoriaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.CategoriaLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil
 */
public class CategoriaLocalServiceImpl extends CategoriaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil} to access the categoria local service.
	 */
	
	public Categoria getCategoriaByName(String nomeCategoria){
		Categoria out=null; 
		try { 
			return categoriaPersistence.findBycategoryName(nomeCategoria);

		} catch (Exception e) {
			e.printStackTrace();
		} 

		return out;
	}
	
	public List<Categoria> getCategorie(){
		
		List<Categoria> out=null; 		
		
		try{ out = categoriaPersistence.findAll(); }
		catch(Exception e){ e.printStackTrace(); }
		
		return out;
	}
	
}