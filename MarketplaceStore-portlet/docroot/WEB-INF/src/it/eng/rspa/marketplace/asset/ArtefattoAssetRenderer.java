package it.eng.rspa.marketplace.asset;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portlet.asset.model.BaseAssetRenderer;

public class ArtefattoAssetRenderer extends  BaseAssetRenderer {
	
	public static final String ARTEFATTO_ENTRY = "Artefatto";
	Artefatto artefatto = null;

	public ArtefattoAssetRenderer(Artefatto artefattoIn) {
		this.artefatto = artefattoIn;
	}

	@Override
	public long getClassPK() {
		return artefatto.getPrimaryKey();
	}

	@Override
	public long getGroupId() {
		return artefatto.getGroupId(); 
	}

	@Override
	public String getSummary(Locale locale) {
		System.out.println("ArtefattoAssetRenderer.getSummary");
		return artefatto.getDescription();

	}

	@Override
	public String getTitle(Locale locale) {
		System.out.println("ArtefattoAssetRenderer.getTitle");
		return artefatto.getTitle();

	}

	@Override
	public long getUserId() {
		return artefatto.getUserId();
	}

	public String getUuid() {
		return artefatto.getUuid();
	}

	@Override
	public String getUserName() {
		return artefatto.getUserId()+"";
	}

	@Override
	public String getURLViewInContext(
			LiferayPortletRequest liferayPortletRequest,
			LiferayPortletResponse liferayPortletResponse,
			String noSuchEntryRedirect) throws Exception {
		

		return "/web/guest/marketplace/-/marketplace/view/"+artefatto.getArtefattoId();
	}

	@Override
	public String render(
			RenderRequest request,
			RenderResponse response,
			String template)
					throws Exception {
		
		// passo in pagina la entity  
		if (template.equals(TEMPLATE_FULL_CONTENT)) {
			request.setAttribute(ARTEFATTO_ENTRY, this.artefatto);
			
			System.out.println("ok");
			
			System.out.println("ArtefattoAssetRenderer.render "+"/html/marketplacestore/" + template + ".jsp");
			
			return "/html/marketplacestore/" + template + ".jsp";
		}
		else {
			return null;
		}
	}

	@Override
	public String getClassName() {
		return this.artefatto.getClass().getName();
	}

}
