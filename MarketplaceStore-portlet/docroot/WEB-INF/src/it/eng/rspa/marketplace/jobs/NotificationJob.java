package it.eng.rspa.marketplace.jobs;

import java.util.List;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.PendingNotificationEntryLocalServiceUtil;
import it.eng.rspa.marketplace.jobs.messagecomposer.NotificationMessageComposer;
import it.eng.rspa.marketplace.listeners.model.WeLiveComponent;
import it.eng.rspa.marketplace.listeners.model.WeLiveNotificationAction;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;

public class NotificationJob implements MessageListener {
	
	private static boolean locked = false;
	private static final int dequeueRate = 100;
	
	private void setPermissionChecker() throws Exception{
		User admin = UserLocalServiceUtil.getUserByEmailAddress( MyMarketplaceUtility.getDefaultCompanyId(), ConfUtil.getString("api.user.email"));
		PermissionChecker checker = PermissionCheckerFactoryUtil.create(admin);
		PermissionThreadLocal.setPermissionChecker(checker);
	}
	
	private static JSONObject prepareMessage(WeLiveComponent component, WeLiveNotificationAction action, Artefatto a, String opts){
		switch(component){
		
			case LOGGING_BB:
				if(ConfUtil.getBoolean("lbb.enabled") && ConfUtil.getBoolean("job.notification.lbb.enabled")){
					return NotificationMessageComposer.prepareLoggingMessage(action, a, opts);
				}
				else return null;
				
			case DECISION_ENGINE:
				if(ConfUtil.getBoolean("de.enabled") && ConfUtil.getBoolean("job.notification.de.enabled")){
					return NotificationMessageComposer.prepareDecisionEngineMessage(action, a, opts);
				}
				else return null;
			case ODS:
				if(ConfUtil.getBoolean("ods.enabled") && ConfUtil.getBoolean("job.notification.ods.enabled")){
					return NotificationMessageComposer.prepareODSMessage(action, a, opts);
				}
				else return null;
			default: return null;
		}
	}
	
	private static boolean sendNotification(WeLiveComponent component, WeLiveNotificationAction action, JSONObject message, Artefatto a, long timestamp, String opts){
		
		switch(component){
			case LOGGING_BB:
				if(ConfUtil.getBoolean("lbb.enabled") && ConfUtil.getBoolean("job.notification.lbb.enabled")){
					return LoggingBuildingBlockNotificationService.sendNotification(action.toString(), message, timestamp);
				}
				else{ return false; }
				
			case DECISION_ENGINE:
				if(ConfUtil.getBoolean("de.enabled") && ConfUtil.getBoolean("job.notification.de.enabled")){
					switch(action){
						case ArtefactPublished:
							return DecisionEngineNotificationService.sendNotification("put", a, message);
						case ArtefactModifed:
							return DecisionEngineNotificationService.sendNotification("put", a, message);
						case ArtefactRemoved:
							return DecisionEngineNotificationService.sendNotification("delete", a, message);
						default: 
							System.out.println("Decision Engine doesn't support the action "+action);
							return true;
					}
				}
				else{ return false; }
			case ODS:
				if(ConfUtil.getBoolean("ods.enabled") && ConfUtil.getBoolean("job.notification.ods.enabled")){
					switch(action){
						case ArtefactRated:
						case ArtefactRateModifed:
							return OdsNotificationService.sendNotification("put", a, message, opts);
						case ArtefactRemoved:
							return OdsNotificationService.sendNotification("delete", a, message, opts);
						case ArtefactRateRemoved:
						default:
							System.out.println("Ods doesn't support the action "+action);
							return true;
					}
				}
				else{ return false; }
			
			default:
				return true;
		}
	}
	
	private void lockJob(){
		locked = true;
		return;
	}
	
	private void unlockJob(){
		locked = false;
		return;
	}
	
	@Override
	public void receive(Message arg0) throws MessageListenerException {
		
		boolean active = ConfUtil.getBoolean("job.notification.enabled");
		if(!active || locked) return;
		lockJob();
		
		try {
			setPermissionChecker();
			int dequeued = 0;
			List<PendingNotificationEntry> pendings = PendingNotificationEntryLocalServiceUtil.getPendingNotificationEntries(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			/*
			 *	In first place the artefact is labeled as IN_TRASH
			 *	After the notifications the artefact is removed permanently
			 *	This set keeps track of the artefacts that have to be removed permanently
			*/
//			Set<Artefatto> removableArtefacts = new HashSet<Artefatto>();			 
			
			for(PendingNotificationEntry pne : pendings){
				try{
					Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(pne.getArtefactId());
					WeLiveComponent targetComponent = WeLiveComponent.valueOf(pne.getTargetComponent());
					WeLiveNotificationAction action = WeLiveNotificationAction.valueOf(pne.getAction());
					String opts = pne.getOptions();
					JSONObject message = NotificationJob.prepareMessage(targetComponent, action, a, opts);
					if(targetComponent.equals(WeLiveComponent.LOGGING_BB) && message==null){
						System.out.println("Unable to send notification <"+targetComponent+", "+action+", "+a.getArtefattoId()+">: The message cannot be arranged correctly");
						continue;
					}
					
					boolean notified = sendNotification(targetComponent, action, message, a, pne.getTimestamp(), opts);
					if(notified){
						//Se la notifica � stata inviata correttamente 
						PendingNotificationEntryLocalServiceUtil.deletePendingNotificationEntry(pne);
//						if(pne.getAction().equalsIgnoreCase(WeLiveNotificationAction.ArtefactRemoved.toString())){
//							removableArtefacts.add(a);
//						}
						dequeued++;
					}
					else{ System.out.println("Unable to send notification <"+targetComponent+", "+action+", "+a.getArtefattoId()+">"); }
					
					if(dequeued>=dequeueRate){
						break;
					}
						
				}
				catch(Exception e){ e.printStackTrace(); }
			}
			
//			for(Artefatto a:removableArtefacts){
//				try{ ArtefattoLocalServiceUtil.permanentDeleteArtefatto(a); }
//				catch(Exception e){ System.out.println(e); }
//			}
			
		}
		catch (Exception e) { e.printStackTrace(); }
		finally{ unlockJob(); }
		
	 	return;
	}

}
