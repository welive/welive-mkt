/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.base.ArtefattoLocalServiceBaseImpl;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.listeners.CDVNotifier;
import it.eng.rspa.marketplace.listeners.DecisionEngineNotifier;
import it.eng.rspa.marketplace.listeners.LoggingNotifier;
import it.eng.rspa.marketplace.listeners.ODSNotifier;
import it.eng.rspa.marketplace.listeners.OIANotifier;
import it.eng.rspa.marketplace.listeners.iArtefactListener;
import it.eng.rspa.marketplace.utils.ArtefactComparatorByRating;
import it.eng.rspa.usdl.utils.EditorUtils;
import it.eng.sesame.Crud;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * The implementation of the artefatto local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.ArtefattoLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil
 */
public class ArtefattoLocalServiceImpl extends ArtefattoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil} to access the artefatto local service.
	 */

	private static Set<iArtefactListener> listeners = new HashSet<iArtefactListener>();
	
	static{
		listeners.add(new LoggingNotifier());
		listeners.add(new DecisionEngineNotifier());
		listeners.add(new ODSNotifier());
		listeners.add(new OIANotifier());
		listeners.add(new CDVNotifier());
	}

	public Artefatto publishArtefatto(long idArtefatto) throws SystemException, PortalException{
		
		Artefatto a=ArtefattoLocalServiceUtil.getArtefatto(idArtefatto); 
		a.setStatus(WorkflowConstants.STATUS_APPROVED);
		
		Artefatto published = ArtefattoLocalServiceUtil.updateArtefatto(a);
		
		for(iArtefactListener listener : listeners){
			try{ listener.onArtefactPublish(published); }
			catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
		List<ArtifactOrganizations> lit = ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByArtifactId(a.getArtefattoId());
		Iterator <ArtifactOrganizations> it = lit.iterator();
		
		while(it.hasNext()){
			ArtifactOrganizations ao = it.next();
			ao.setStatus(WorkflowConstants.STATUS_APPROVED);
			ArtifactOrganizationsLocalServiceUtil.updateArtifactOrganizations(ao);
		}
		
		return published;
	}
	
	public Artefatto addArtefatto(Artefatto newArtefatto, long userId, String[] tags) throws SystemException, PortalException {
		super.addArtefatto(newArtefatto);

		Artefatto artefatto = artefattoPersistence.create( counterLocalService.increment( Artefatto.class.getName() ) );

		artefatto.setCompanyId(newArtefatto.getCompanyId());
		artefatto.setGroupId(newArtefatto.getGroupId());
		artefatto.setUserId(userId);
		artefatto.setDate(newArtefatto.getDate());
		artefatto.setDescription(newArtefatto.getDescription());

		artefatto.setStatus(WorkflowConstants.STATUS_DRAFT);

		String pilotId = "";
		try{
			User u = UserLocalServiceUtil.getUser(userId);
			pilotId = ((String[])u.getExpandoBridge().getAttribute("pilot"))[0];
		}
		catch(Exception e){ e.printStackTrace(); }
		
		artefatto.setPilotid(pilotId);
		
		artefattoPersistence.update(artefatto);
		resourceLocalService.addResources(
				newArtefatto.getCompanyId(), newArtefatto.getGroupId(), userId,
				Artefatto.class.getName(), artefatto.getPrimaryKey(), false,
				true, true);

		assetEntryLocalService.updateEntry( userId, artefatto.getGroupId(), Artefatto.class.getName(), artefatto.getArtefattoId(), new long[]{}, tags);


		return artefatto;
	}
	
	public Artefatto addArtefatto(Artefatto newArtefatto, long userId, ServiceContext serviceContext)
					throws SystemException, PortalException {
		super.addArtefatto(newArtefatto);

		Artefatto artefatto = artefattoPersistence.create(
				counterLocalService.increment(
						Artefatto.class.getName()));

		artefatto.setCompanyId(newArtefatto.getCompanyId());
		artefatto.setGroupId(newArtefatto.getGroupId());
		artefatto.setUserId(serviceContext.getUserId());
		artefatto.setDate(newArtefatto.getDate());
		artefatto.setDescription(newArtefatto.getDescription());

		artefatto.setStatus(WorkflowConstants.STATUS_DRAFT);

		String pilotId = "";
		try{
			User u = UserLocalServiceUtil.getUser(serviceContext.getUserId());
			pilotId = ((String[])u.getExpandoBridge().getAttribute("pilot"))[0];			
		}
		catch(Exception e){ e.printStackTrace(); }
		
		artefatto.setPilotid(pilotId);
		
		artefattoPersistence.update(artefatto, serviceContext);
		resourceLocalService.addResources(
				newArtefatto.getCompanyId(), newArtefatto.getGroupId(), userId,
				Artefatto.class.getName(), artefatto.getPrimaryKey(), false,
				true, true);

		assetEntryLocalService.updateEntry(
				userId, artefatto.getGroupId(), Artefatto.class.getName(),
				artefatto.getArtefattoId(), serviceContext.getAssetCategoryIds(),
				serviceContext.getAssetTagNames());


		return artefatto;
	}

	public Artefatto permanentDeleteArtefatto(Artefatto artefatto) throws SystemException    { 
		long id = artefatto.getArtefattoId();
		long companyId = artefatto.getCompanyId();

		try {
			resourceLocalService.deleteResource(
					companyId, Artefatto.class.getName(),
					ResourceConstants.SCOPE_INDIVIDUAL, artefatto.getPrimaryKey());
			assetEntryLocalService.deleteEntry(
					Artefatto.class.getName(), artefatto.getArtefattoId());

			artifactOrganizationsPersistence.removeByArtifactId(id);
			
			String resourceRDF = artefatto.getResourceRDF();
			//Elimina artefatto
			artefattoPersistence.remove(artefatto);
			if(resourceRDF!=null && !resourceRDF.equalsIgnoreCase("")){
				Crud crud = new Crud();
				try { crud.clearContext(resourceRDF); } 
				catch (Exception e1) { 
					e1.printStackTrace(); 
				}
			}
			
		} catch (PortalException e) {
			e.printStackTrace();
			throw new SystemException(e.getMessage());			
		}
		
		return artefatto;
	}

	public Artefatto deleteArtefatto(Artefatto artefatto) throws SystemException    { 

		try {
			
			if(artefatto.getStatus() == WorkflowConstants.STATUS_IN_TRASH){
				return artefatto;
			}
			
			//Imposto l'artefatto allo stato INACTIVE
			artefatto.setStatus(WorkflowConstants.STATUS_IN_TRASH);
			ArtefattoLocalServiceUtil.updateArtefatto(artefatto);
			
			for(iArtefactListener listener : listeners){
				try{ listener.onArtefactDelete(artefatto); }
				catch(Exception e){e.printStackTrace();}
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(e.getMessage());			
		}
		return artefatto ;
	}

	@Override
	public Artefatto deleteArtefatto(long progettoId)
			throws PortalException, SystemException {
		Artefatto artefatto = getArtefatto(progettoId);
		deleteArtefatto(artefatto);
		
		return artefatto;
	}

	public List<Artefatto> getArtefattiByPilotAndStatus(String pilotId, int status){
		try{ return artefattoPersistence.findBystatusAndpilotId(status, pilotId); }
		catch(Exception e){ return new ArrayList<Artefatto>(); }
	}
	
	public List<Artefatto> getArtefattiByPilotAndStatus(String pilotId, int status, String orderByCampo, String orderType){

		try{
			boolean orderByAsc = false;
			if (orderType.equals("asc")) { orderByAsc = true; }

			OrderByComparator orderByComparator=OrderByComparatorFactoryUtil.create(ArtefattoImpl.TABLE_NAME, orderByCampo,orderByAsc ); 
			return getArtefattoPersistence().findBystatusAndpilotId(status, pilotId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		}
		catch (SystemException e) { return new ArrayList<Artefatto>(); } 
		
	}

	public List<Artefatto> getArtefattoByDescriptionRDF(String description){
		
		List<Artefatto> out=null; 
		try { out= artefattoFinder.findByDescriptionRDF(description); } 
		catch (SystemException e) { e.printStackTrace(); } 

		return out;
	}
	
	public List<Artefatto> getArtefattoByResourceRDF(String resourceRDF){
		
		List<Artefatto> out=new ArrayList<Artefatto>(); 
		try { out= artefattoPersistence.findByResourceRDF(resourceRDF); } 
		catch (SystemException e) { System.out.println(e.getMessage()); } 

		return out;
	}

	public List<Artefatto> getArtefattiByTitle(String title){
		try{ return artefattoPersistence.findByTitle(title); }
		catch(Exception e){ return new ArrayList<Artefatto>(); }
	}
	/*public List<Artefatto> getArtefattoByByDescr_Cat(String description,long categoria){
		List<Artefatto> out=null; 
		try { 
			out= getArtefattoPersistence().findByDescr_Cat(description, categoria);
			//System.out.println("getArtefattoByByDescr_Cat ");
		} catch (SystemException e) {
			e.printStackTrace();
		} 

		return out;
	}*/

	public List<Artefatto>findArtefattoByStatus(int status){
		try { return getArtefattoPersistence().findByStatus(status); } 
		catch (SystemException e){ return new ArrayList<Artefatto>(); } 
	}

	public List<Artefatto> getArtefactsByPilotStatus(String pilot, int status, int start, int end){
		List<Artefatto> out;
		try { out = getArtefattoPersistence().findBystatusAndpilotId(status, pilot); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
		if(out.isEmpty()) return out;
		if(start >= out.size()) return new ArrayList<Artefatto>();
		return out.subList(start, Math.min(end, out.size()));
	}
	
	public List<Artefatto> getArtefactsByPilotStatus(String pilot, int status){
		List<Artefatto> out = new ArrayList<Artefatto>();
		try { out = getArtefattoPersistence().findBystatusAndpilotId(status, pilot); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
		
		return out;
	}

	public List<Artefatto> findArtefattoByStatus(int status,String orderByCampo,String orderType ){
		List<Artefatto> out=null; 
		try { 			
			boolean orderByAsc = false;

			if (orderType.equals("asc")) {
				orderByAsc = true;
			}

			OrderByComparator orderByComparator=OrderByComparatorFactoryUtil.create(ArtefattoImpl.TABLE_NAME, orderByCampo,orderByAsc ); 
			out= getArtefattoPersistence().findByStatus(status,-1,-1, orderByComparator);

		} catch (SystemException e) {
			e.printStackTrace();
		} 

		return out;
	}


	public List<Artefatto>findArtefattoByUserId_Status(long userId, int status)
			throws PortalException, SystemException, RemoteException {
		List<Artefatto> out=null;
		 
		out=getArtefattoPersistence().findByUserId_Status(userId, status);
		return out;
	}
	
	public List<Artefatto>findArtefattoByCategory_Status(long categoryId, int status)
			throws PortalException, SystemException, RemoteException {
		List<Artefatto> out=null;
		 
		out=getArtefattoPersistence().findByCategoria_Status(categoryId, status);
		return out;
	}
	
	public List<Artefatto>findArtefattoByPilot_Category_Status(String pilot, long categoryId, int status)
			throws PortalException, SystemException, RemoteException {
		
		List<Artefatto> out=null;
		out=getArtefattoPersistence().findByPilot_Categoria_Status(pilot, categoryId, status);
		return out;
	}
 
	public List<Artefatto>findArtefattoByCompanyId(long companyId){
		List<Artefatto> out=null; 
		try { 
			OrderByComparator orderByComparator=OrderByComparatorFactoryUtil.create(ArtefattoImpl.TABLE_NAME, "status", true); 		
			out= getArtefattoPersistence().findByCompanyId(companyId,-1,-1, orderByComparator);
	
		} catch (SystemException e) {
			e.printStackTrace();
		}
	
		return out;
	}

	public List<Artefatto>findArtefattoByCompanyId(long companyId,String orderByCampo, boolean isAsc ){
		List<Artefatto> out=null; 
		try {  
	
			OrderByComparator orderByComparator=OrderByComparatorFactoryUtil.create(ArtefattoImpl.TABLE_NAME, orderByCampo, isAsc); 
	
			out= getArtefattoPersistence().findByCompanyId(companyId,-1,-1, orderByComparator);
	
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	
		return out;
	}

	public List<Artefatto>findArtefattoByCompanyId(long companyId,String orderByCampo,String orderType ){
		List<Artefatto> out=null; 
		try { 			
			out= getArtefattoPersistence().findByCompanyId(companyId,-1,-1, getOrderByComparator(ArtefattoImpl.TABLE_NAME, orderByCampo,orderType));			
	
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	
		return out;
	}

	public List<Artefatto>findArtefattoByCategory(long category){
		List<Artefatto> out=null; 
		try { 			
			out= getArtefattoPersistence().findByCategoria(category);		
	
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	
		return out;
	}
	
	public List<Artefatto>findArtefattoByCategory(long category, int start, int end){
		List<Artefatto> out=null; 
		try {
			out= getArtefattoPersistence().findByCategoria(category, start, end);
	
		} catch (SystemException e){
			e.printStackTrace();
		} 
	
		return out;
	}
	
	public List<Artefatto>findArtefattiByIds(List<Long> ids){
		List<Artefatto> out= new ArrayList<Artefatto>();
		Iterator<Long> idsIt = ids.iterator();	
		while(idsIt.hasNext()){
			try {
				out.add(getArtefatto(idsIt.next()));
			} catch (Exception e){
				e.printStackTrace();
			} 
		}
	
		return out;
	}
	
	public List<Artefatto> getArtefattiByLangStatus(String lang, int status){
		try { return getArtefattoPersistence().findBystatusAndLanguage(status, lang); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }	
	}
	
	public List<Artefatto> getArtefattiByPilotLangStatus(String pilot, String lang, int status){
		try { return getArtefattoPersistence().findBypilotStatusAndLanguage(pilot, status, lang); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }	
	}
	
	public List<Artefatto> getArtefattiByLangStatus(String lang, int status, int start, int end){
		try { return getArtefattoPersistence().findBystatusAndLanguage(status, lang, start, end); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
	}
	
	public List<Artefatto> getArtefattiByPilotLangStatus(String pilot, String lang, int status, int start, int end){
		try { return getArtefattoPersistence().findBypilotStatusAndLanguage(pilot, status, lang, start, end); } 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
	}
	
	public List<Artefatto> getArtefattiByKeyword(String keyword){
	
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		if(keyword==null){
			return out;
		}
		Artefatto a = null;
		List<Artefatto> list;
		try {
			list = findArtefattoByStatus(WorkflowConstants.STATUS_APPROVED);
		} catch (Exception e) {
			e.printStackTrace();
			return out;
		}
		Iterator<Artefatto> it = list.iterator();
		
		while(it.hasNext()){
			try{
				a = it.next();
	
				if(a.getTitle().toLowerCase().contains(keyword.toLowerCase()) || a.getAbstractDescription().toLowerCase().contains(keyword.toLowerCase())){
					out.add(a);
				}
			}
			catch(Exception e){
				System.out.println(e.getMessage());
			}
			
		}
		
		return out;
	}

	private OrderByComparator getOrderByComparator(String tableName,String orderByCampo,String orderType){
		boolean orderByAsc = false;
	
		if (orderType.equals("asc")) {
			orderByAsc = true;
		}
	
		OrderByComparator orderByComparator = OrderByComparatorFactoryUtil.create(tableName, orderByCampo,orderByAsc ); 
		return orderByComparator;
	}
	
	public List<Artefatto> getBuildingBlocks(String pilotId){

		List<Artefatto> out = new ArrayList<Artefatto>();
		
		if(pilotId==null || pilotId.equals("")){
			try {
				out.addAll(findArtefattoByCategory_Status(
						Category.getMapByName().get("SOAP Web Service").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
			
			try {
				out.addAll(findArtefattoByCategory_Status(
						Category.getMapByName().get("RESTful Web Service").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
		}
		else{
			try {
				out.addAll(findArtefattoByPilot_Category_Status(
						pilotId,
						Category.getMapByName().get("SOAP Web Service").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
			
			try {
				out.addAll(findArtefattoByPilot_Category_Status(
						pilotId,
						Category.getMapByName().get("RESTful Web Service").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
		}

		return out;
		
	}
	
	public List<Artefatto> getBuildingBlocks(String pilotId, int startId, int endId){
		List<Artefatto> out = getBuildingBlocks(pilotId);
		if(out.isEmpty()) return out;
		if(startId >= out.size()) return new ArrayList<Artefatto>();
		return out.subList(startId,  Math.min(endId, out.size()));
	}
	
	public List<Artefatto> getBuildingBlocksByKeyword(String pilotId, String keyword){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		Iterator<Artefatto> bbs = getBuildingBlocks(pilotId).iterator();
		while(bbs.hasNext()){
			Artefatto a = bbs.next();
			if(a.getTitle().contains(keyword) || a.getAbstractDescription().contains(keyword) || a.getDescription().contains(keyword)){
				out.add(a);
			}
		}
		return out;
	}
	
	public List<Artefatto> getBuildingBlocksByLangStatus(String lang, int status){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("SOAP Web Service").getIdCategoria(), lang)); }
		catch(Exception e){ /* Do nothing */ }
		
		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("RESTful Web Service").getIdCategoria(), lang)); }
		catch(Exception e){ /* Do nothing */ }
		
//		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("OSGi bundle").getIdCategoria(), lang)); }
//		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public List<Artefatto> getBuildingBlocksByPilotLangStatus(String pilot, String lang, int status, int start, int end){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("SOAP Web Service").getIdCategoria(), lang, start, end)); }
		catch(Exception e){ /* Do nothing */ }
		
		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("RESTful Web Service").getIdCategoria(), lang, start, end)); }
		catch(Exception e){ /* Do nothing */ }
		
//		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("OSGi bundle").getIdCategoria(), lang, start, end)); }
//		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public List<Artefatto> getPublicServices(String pilot){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		if(pilot==null || pilot.isEmpty()){
			try {
				out.addAll(findArtefattoByCategory_Status(
						Category.getMapByName().get("Web Application").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
			
//			try{
//				out.addAll(findArtefattoByCategory_Status(
//						Category.getMapByName().get("Open Social Gadget").getIdCategoria(), 
//						WorkflowConstants.STATUS_APPROVED));
//			}
//			catch(Exception e){ e.printStackTrace(); }
			
			try{
				out.addAll(findArtefattoByCategory_Status(
						Category.getMapByName().get("Android Application").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		else{
			try {
				out.addAll(findArtefattoByPilot_Category_Status(
						pilot,
						Category.getMapByName().get("Web Application").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
			
//			try{
//				out.addAll(findArtefattoByPilot_Category_Status(
//						pilot,
//						Category.getMapByName().get("Open Social Gadget").getIdCategoria(), 
//						WorkflowConstants.STATUS_APPROVED));
//			}
//			catch(Exception e){ e.printStackTrace(); }
			
			try{
				out.addAll(findArtefattoByPilot_Category_Status(
						pilot,
						Category.getMapByName().get("Android Application").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		
		return out;
	}
	
	public List<Artefatto> getPublicServices(String pilot, int starId, int endId){
		List<Artefatto> out = getPublicServices(pilot);
		if(out.isEmpty()) return out;
		if(starId >= out.size()) return new ArrayList<Artefatto>();
		return out.subList(starId,  Math.min(endId, out.size()));
	}
	
	public List<Artefatto> getPublicServicesByKeyword(String pilot, String keyword){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		Iterator<Artefatto> pss = getPublicServices(pilot).iterator();
		while(pss.hasNext()){
			Artefatto a = pss.next();
			if(a.getTitle().contains(keyword) || a.getAbstractDescription().contains(keyword) || a.getDescription().contains(keyword)){
				out.add(a);
			}
		}
		return out;
	}
	
	public List<Artefatto> getPublicServicesByLangStatus(String lang, int status){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("Web Application").getIdCategoria(), lang)); }
		catch(Exception e){ /* Do nothing */ }
		
//		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("Open Social Gadget").getIdCategoria(), lang)); }
//		catch(Exception e){ /* Do nothing */ }
		
		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("Android Application").getIdCategoria(), lang)); }
		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public List<Artefatto> getPublicServicesByPilotLangStatus(String pilot, String lang, int status, int start, int end){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("Web Application").getIdCategoria(), lang, start, end)); }
		catch(Exception e){ /* Do nothing */ }
		
		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("Android Application").getIdCategoria(), lang, start, end)); }
		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public List<Artefatto> getDatasets(String pilot){
		List<Artefatto> out = new ArrayList<Artefatto>();
		if(pilot==null || pilot.isEmpty()){
			try {
				out.addAll(findArtefattoByCategory_Status(
						Category.getMapByName().get("Dataset").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
		}
		else{
			try {
				out.addAll(findArtefattoByPilot_Category_Status(
						pilot,
						Category.getMapByName().get("Dataset").getIdCategoria(), 
						WorkflowConstants.STATUS_APPROVED));
			} 
			catch (Exception e) { e.printStackTrace(); }
		}
		return out;
	}
	
	public List<Artefatto> getDatasets(String pilot, int startId, int endId){
		List<Artefatto> out = getDatasets(pilot);
		if(out.isEmpty()) return out;
		if(startId >= out.size()) return new ArrayList<Artefatto>();
		return out.subList(startId,  Math.min(endId, out.size()));
	}
	
	public List<Artefatto> getDatasetsByKeyword(String pilot, String keyword){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		Iterator<Artefatto> dss = getDatasets(pilot).iterator();
		while(dss.hasNext()){
			Artefatto a = dss.next();
			if(a.getTitle().contains(keyword) || a.getAbstractDescription().contains(keyword) || a.getDescription().contains(keyword)){
				out.add(a);
			}
		}
		return out;
	}
	
	public List<Artefatto> getDatasetsByLangStatus(String lang, int status){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBycategoryStatusAndLanguage(status, Category.getMapByName().get("Dataset").getIdCategoria(), lang)); }
		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public List<Artefatto> getDatasetsByPilotLangStatus(String pilot, String lang, int status, int start, int end){
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		try{ out.addAll(getArtefattoPersistence().findBypilotCategoryStatusAndLanguage(pilot, status, Category.getMapByName().get("Dataset").getIdCategoria(), lang, start, end)); }
		catch(Exception e){ /* Do nothing */ }
		
		return out;
				
	}
	
	public Artefatto getArtefactByExternalId(String eid) throws Exception{
		return getArtefattoPersistence().findByeid(eid);
	}
	
	//for WeLive Controller
	public JSONArray getTopRatedArtefactsByPilot(String pilot, int maxNumber){
		
		List<Artefatto> artList = new ArrayList<Artefatto>(); 
				artList.addAll(getArtefactsByPilotStatus(pilot, WorkflowConstants.STATUS_APPROVED));
		
		List<Artefatto> out = new ArrayList<Artefatto>();
		ArtefactComparatorByRating ac = new ArtefactComparatorByRating();
		out = ListUtil.subList(ListUtil.sort(artList, ac), 0, maxNumber);
		
		return MyMarketplaceUtility.easyArtefactList2Json(out);
	}
	
	//for WeLive Controller
	public JSONArray getTopRatedArtefactsByLanguage(String language, int maxNumber){
		
		List<Artefatto> artList = new ArrayList<Artefatto>(); 
			artList.addAll(getArtefattiByLangStatus(language, WorkflowConstants.STATUS_APPROVED));
		
		List<Artefatto> out = new ArrayList<Artefatto>();
		ArtefactComparatorByRating ac = new ArtefactComparatorByRating();
		out = ListUtil.subList(ListUtil.sort(artList, ac), 0, maxNumber);
		
		return MyMarketplaceUtility.easyArtefactList2Json(out);
		
	}
	
	//for WeLive Controller
	public JSONArray getArtefactsByUserId(long liferayUserId, int maxNumber){
		
		List<Artefatto> artList = new ArrayList<Artefatto>(); 
		
		try {
			artList.addAll(artefattoLocalService.findArtefattoByUserId_Status(liferayUserId, WorkflowConstants.STATUS_APPROVED));
		}
		catch(Exception e){ e.printStackTrace(); }
		
		if(artList.size() < maxNumber){
			try { 
				User u = UserLocalServiceUtil.getUser(liferayUserId); 
				String pilotId = "";
				String[] pilots = ((String[])u.getExpandoBridge().getAttribute("pilot"));
				if(pilots!=null && pilots.length > 0) pilotId = pilots[0];
			
				artList.addAll(getArtefactsByPilotStatus(pilotId, WorkflowConstants.STATUS_APPROVED));
			
			} 
			catch (PortalException | SystemException e) {
				e.printStackTrace();
			}
		}
		
		List<Artefatto> out = new ArrayList<Artefatto>();
		out = ListUtil.subList(out, 0, maxNumber);
		
		return MyMarketplaceUtility.easyArtefactList2Json(out);
	}
	
	public List<Artefatto> getArtefattiByOwner(String owner){
		try { 
			return getArtefattoPersistence().findByOwner(owner); 
		} 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
	}
	
	public List<Artefatto> getArtefattiByOwnerAndStatus(String owner, int status){
		try { 
			return getArtefattoPersistence().findBystatusAndOwner(status, owner);
		} 
		catch (SystemException e) { return new ArrayList<Artefatto>(); }
	}
	
	public JSONObject forget(long liferayUserId, boolean removecontent) throws Exception{
		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		out.put("errors", JSONFactoryUtil.createJSONArray());
		out.put("success", true);
		
		//Recupero tutti gli artefatti dell'utente
		List<Artefatto> arts = getArtefattoPersistence().findByUserId(liferayUserId);
		
		if(removecontent){
			//Rimuovo gli artefatti
			for(Artefatto a : arts){
				try{
					
					long artid = a.getArtefattoId();
					deleteArtefatto(a);
					
					if(a.getStatus() == WorkflowConstants.STATUS_APPROVED){
						
						List<Dependencies> deps = DependenciesLocalServiceUtil.getDependenciesByDependsFrom(artid);
						
						if(!deps.isEmpty()){
							//TODO 
							//send a notification to the authors of the artefacts that depend from the removed one
						}
						
					}
					
				}
				catch(Exception e){
					e.printStackTrace();
					
					out.put("success", false);
					
					JSONObject j = JSONFactoryUtil.createJSONObject();
						j.put("id", a.getArtefattoId());
						j.put("message", e.toString());
					out.getJSONArray("errors").put(j);
				}
			}
			
		}
		else{
			
			String currentPilot = EditorUtils.getPilotByUserId(liferayUserId);
			
			long deletedUserId = MyMarketplaceUtility.getDeletedUserId(currentPilot);
			if(deletedUserId == -1L)
				throw new Exception("No DeletedUser registered on the platform.");
			
			for(Artefatto a : arts){
				if(a.getStatus() != WorkflowConstants.STATUS_APPROVED){
					ArtefattoLocalServiceUtil.permanentDeleteArtefatto(a);
				}
				else{
					try{
						
						List<Dependencies> deps = DependenciesLocalServiceUtil.getDependenciesByDependsFrom(a.getArtefattoId());
						if(deps.isEmpty()){
							ArtefattoLocalServiceUtil.deleteArtefatto(a);
						}
						else {
							long assigneeId = deletedUserId;
							
							try{
								JSONObject lusdl = JSONFactoryUtil.createJSONObject(a.getLusdlmodel());
								JSONArray arr = lusdl.getJSONArray("hasBusinessRoles");
								if(arr!=null){
									User author = UserLocalServiceUtil.getUser(a.getUserId());
									String authorname = author.getFullName();
									
									for(int i = 0; i<arr.length(); i++){
										JSONObject br = arr.getJSONObject(i);
										String sRole = br.getString("businessRole");
										
										if("owner".equalsIgnoreCase(sRole)){
											String entity = br.getString("title");
											
											if(!entity.equalsIgnoreCase(authorname)){
												try{
													//If the owner is the organization, assign the artefact to the Organization Leader
													Organization org = OrganizationLocalServiceUtil.getOrganization(a.getCompanyId(), entity);
													List<User> members = UserLocalServiceUtil.getOrganizationUsers(org.getOrganizationId());
													Role rCompanyLeader = RoleLocalServiceUtil.getRole(a.getCompanyId(), PortletProps.get("role.companyleader"));
													for(User m : members){
														List<Role> userRoles = m.getRoles();
														if(userRoles.contains(rCompanyLeader)){
															if(!m.equals(author)){
																assigneeId = m.getUserId();
															}
															break;
														}
													}
												}
												catch(Exception e){
													System.out.println(e.getMessage());
												}
											}
											
											break;
										}
									}
								}
							}
							catch(Exception e){
								System.out.println(e.getMessage());
							}
							
							
							boolean isOwnerEqualToAuthor = MyMarketplaceUtility.isOwnerEqualToAuthor(a.getUserId(),a.getOwner() );
							
							if (isOwnerEqualToAuthor)
								a.setOwner(MyMarketplaceUtility.getUserFullnameByUserId(assigneeId));
							
							a.setUserId(assigneeId);
							
							getArtefattoPersistence().update(a);
							
						}
					}
					catch(Exception e){
						e.printStackTrace();

						out.put("success", false);
						
						JSONObject j = JSONFactoryUtil.createJSONObject();
						j.put("id", a.getArtefattoId());
						j.put("message", e.toString());
						out.getJSONArray("errors").put(j);
					}
				}
			}
		}
		
		return out;
	}
}