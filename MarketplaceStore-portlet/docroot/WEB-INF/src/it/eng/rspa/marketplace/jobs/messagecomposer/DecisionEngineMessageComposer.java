package it.eng.rspa.marketplace.jobs.messagecomposer;

import it.eng.metamodel.definitions.Artefact;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.sesame.Crud;
import it.eng.sesame.ReaderRDF;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class DecisionEngineMessageComposer 
					extends CommonMessageComposer{
	
	private static Log _log = LogFactoryUtil.getLog(DecisionEngineMessageComposer.class);

	public static JSONObject prepareDERemoveMessage(Artefatto a){
		return null;
	}
	
	public static JSONObject prepareDEPublicationMessage(Artefatto a){
		JSONObject body = JSONFactoryUtil.createJSONObject();
		JSONArray tags = JSONFactoryUtil.createJSONArray();
		try { 
			String[] tmp = AssetTagLocalServiceUtil.getTagNames(Artefatto.class.getName(), a.getArtefattoId());
			Set<String> tagsetTmp = new HashSet<String>();
			tagsetTmp.addAll(Arrays.asList(tmp));
			
			String lusdlmodel = a.getLusdlmodel();
			
			if(Validator.isNotNull(a.getResourceRDF()) && lusdlmodel.equalsIgnoreCase("")){
				try{
					Crud c = new Crud();
					ByteArrayOutputStream os = c.exportRDFOutputStream(a.getResourceRDF());
					ReaderRDF reader = new ReaderRDF(os);
					Set<String> tagset = reader.getTags();
					tagsetTmp.addAll(tagset);
					tmp = new String[tagsetTmp.size()];
					tmp = tagsetTmp.toArray(tmp);
					
					AssetEntryLocalServiceUtil.updateEntry(a.getUserId(), 
							a.getGroupId(), 
							Artefatto.class.getName(), 
							a.getArtefattoId(), 
							new long[]{}, 
							tmp);
					
				}
				catch(Exception e){
					_log.warn("Unable to double check the tags: "+ e);
				}
				
				for(String t:tmp){
					tags.put(t);
				}
			}
			else if(!lusdlmodel.equalsIgnoreCase("")){
				try{
					ParserResponse resp = MetamodelParser.parse(lusdlmodel, MetamodelParser.SourceType.METAMODEL);
					Artefact art = resp.getBuildingBlock();
					
					for(String t:art.getTags()){
						tags.put(t);
					}
				}
				catch(Exception e){
					_log.warn(e);
				}
			}
			_log.debug("Tags: [ "+StringUtils.join(tags, ",")+" ]");
			
			
		} 
		catch (Exception e) { _log.warn(e.getMessage()); }
		
		if(Validator.isNotNull(a.getLanguage())){
			body.put("lang", a.getLanguage());
		}
		else{
			//Put the Pilot default language
			String pilot = a.getPilotid().toLowerCase();
			body.put("lang", ConfUtil.getString("lang.default."+pilot));
		}
		
		body.put("tags", tags);
		
		String[] psaCats = PortletProps.get("Category.catsOfType.psa").split(",");
		boolean ispsa = false;
		for(String psacat : psaCats){
			long catid = CategoriaLocalServiceUtil.getCategoriaByName(psacat).getIdCategoria();
			if(a.getCategoriamkpId() == catid){
				ispsa = true;
			}
		}
		
		if(ispsa){
			body.put("minimum_age", 0);
			body.put("scope", a.getPilotid());
		}
		
		
		return body;
	}
	
	public static JSONObject prepareDEUpdateMessage(Artefatto a){
		return null;
	}
	
}
