/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ImmagineArt in entity cache.
 *
 * @author eng
 * @see ImmagineArt
 * @generated
 */
public class ImmagineArtCacheModel implements CacheModel<ImmagineArt>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{imageId=");
		sb.append(imageId);
		sb.append(", dlImageId=");
		sb.append(dlImageId);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", artefattoIdent=");
		sb.append(artefattoIdent);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ImmagineArt toEntityModel() {
		ImmagineArtImpl immagineArtImpl = new ImmagineArtImpl();

		immagineArtImpl.setImageId(imageId);
		immagineArtImpl.setDlImageId(dlImageId);

		if (descrizione == null) {
			immagineArtImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			immagineArtImpl.setDescrizione(descrizione);
		}

		immagineArtImpl.setArtefattoIdent(artefattoIdent);

		immagineArtImpl.resetOriginalValues();

		return immagineArtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		imageId = objectInput.readLong();
		dlImageId = objectInput.readLong();
		descrizione = objectInput.readUTF();
		artefattoIdent = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(imageId);
		objectOutput.writeLong(dlImageId);

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		objectOutput.writeLong(artefattoIdent);
	}

	public long imageId;
	public long dlImageId;
	public String descrizione;
	public long artefattoIdent;
}