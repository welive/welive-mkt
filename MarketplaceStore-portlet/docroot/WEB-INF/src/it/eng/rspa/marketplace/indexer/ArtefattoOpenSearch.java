package it.eng.rspa.marketplace.indexer;

import com.liferay.portal.kernel.search.HitsOpenSearchImpl;

public class ArtefattoOpenSearch extends HitsOpenSearchImpl {
	public static final String SEARCH_PATH = "/c/artefatto/open_search";
	public static final String TITLE = "Liferay Sample Search: ";	
	 
	 
	
	@Override
	public String getPortletId() {		 
		return ArtefattoIndexer.PORTLET_ID;
	}

	@Override
	public String getSearchPath() {
		return SEARCH_PATH;
	}

	@Override
	public String getTitle(String keywords) {
		System.out.println("getTitle: "+TITLE + keywords);
		return TITLE + keywords;
	}

}
