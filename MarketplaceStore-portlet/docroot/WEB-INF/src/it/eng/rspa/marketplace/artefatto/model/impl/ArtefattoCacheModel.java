/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Artefatto in entity cache.
 *
 * @author eng
 * @see Artefatto
 * @generated
 */
public class ArtefattoCacheModel implements CacheModel<Artefatto>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(53);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", artefattoId=");
		sb.append(artefattoId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", title=");
		sb.append(title);
		sb.append(", webpage=");
		sb.append(webpage);
		sb.append(", abstractDescription=");
		sb.append(abstractDescription);
		sb.append(", description=");
		sb.append(description);
		sb.append(", providerName=");
		sb.append(providerName);
		sb.append(", resourceRDF=");
		sb.append(resourceRDF);
		sb.append(", repositoryRDF=");
		sb.append(repositoryRDF);
		sb.append(", status=");
		sb.append(status);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", owner=");
		sb.append(owner);
		sb.append(", imgId=");
		sb.append(imgId);
		sb.append(", categoriamkpId=");
		sb.append(categoriamkpId);
		sb.append(", pilotid=");
		sb.append(pilotid);
		sb.append(", url=");
		sb.append(url);
		sb.append(", eId=");
		sb.append(eId);
		sb.append(", extRating=");
		sb.append(extRating);
		sb.append(", interactionPoint=");
		sb.append(interactionPoint);
		sb.append(", language=");
		sb.append(language);
		sb.append(", hasMapping=");
		sb.append(hasMapping);
		sb.append(", isPrivate=");
		sb.append(isPrivate);
		sb.append(", lusdlmodel=");
		sb.append(lusdlmodel);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Artefatto toEntityModel() {
		ArtefattoImpl artefattoImpl = new ArtefattoImpl();

		if (uuid == null) {
			artefattoImpl.setUuid(StringPool.BLANK);
		}
		else {
			artefattoImpl.setUuid(uuid);
		}

		artefattoImpl.setArtefattoId(artefattoId);

		if (date == Long.MIN_VALUE) {
			artefattoImpl.setDate(null);
		}
		else {
			artefattoImpl.setDate(new Date(date));
		}

		if (title == null) {
			artefattoImpl.setTitle(StringPool.BLANK);
		}
		else {
			artefattoImpl.setTitle(title);
		}

		if (webpage == null) {
			artefattoImpl.setWebpage(StringPool.BLANK);
		}
		else {
			artefattoImpl.setWebpage(webpage);
		}

		if (abstractDescription == null) {
			artefattoImpl.setAbstractDescription(StringPool.BLANK);
		}
		else {
			artefattoImpl.setAbstractDescription(abstractDescription);
		}

		if (description == null) {
			artefattoImpl.setDescription(StringPool.BLANK);
		}
		else {
			artefattoImpl.setDescription(description);
		}

		if (providerName == null) {
			artefattoImpl.setProviderName(StringPool.BLANK);
		}
		else {
			artefattoImpl.setProviderName(providerName);
		}

		if (resourceRDF == null) {
			artefattoImpl.setResourceRDF(StringPool.BLANK);
		}
		else {
			artefattoImpl.setResourceRDF(resourceRDF);
		}

		if (repositoryRDF == null) {
			artefattoImpl.setRepositoryRDF(StringPool.BLANK);
		}
		else {
			artefattoImpl.setRepositoryRDF(repositoryRDF);
		}

		artefattoImpl.setStatus(status);
		artefattoImpl.setCompanyId(companyId);
		artefattoImpl.setGroupId(groupId);
		artefattoImpl.setUserId(userId);

		if (owner == null) {
			artefattoImpl.setOwner(StringPool.BLANK);
		}
		else {
			artefattoImpl.setOwner(owner);
		}

		artefattoImpl.setImgId(imgId);
		artefattoImpl.setCategoriamkpId(categoriamkpId);

		if (pilotid == null) {
			artefattoImpl.setPilotid(StringPool.BLANK);
		}
		else {
			artefattoImpl.setPilotid(pilotid);
		}

		if (url == null) {
			artefattoImpl.setUrl(StringPool.BLANK);
		}
		else {
			artefattoImpl.setUrl(url);
		}

		if (eId == null) {
			artefattoImpl.setEId(StringPool.BLANK);
		}
		else {
			artefattoImpl.setEId(eId);
		}

		artefattoImpl.setExtRating(extRating);

		if (interactionPoint == null) {
			artefattoImpl.setInteractionPoint(StringPool.BLANK);
		}
		else {
			artefattoImpl.setInteractionPoint(interactionPoint);
		}

		if (language == null) {
			artefattoImpl.setLanguage(StringPool.BLANK);
		}
		else {
			artefattoImpl.setLanguage(language);
		}

		artefattoImpl.setHasMapping(hasMapping);
		artefattoImpl.setIsPrivate(isPrivate);

		if (lusdlmodel == null) {
			artefattoImpl.setLusdlmodel(StringPool.BLANK);
		}
		else {
			artefattoImpl.setLusdlmodel(lusdlmodel);
		}

		artefattoImpl.resetOriginalValues();

		return artefattoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		artefattoId = objectInput.readLong();
		date = objectInput.readLong();
		title = objectInput.readUTF();
		webpage = objectInput.readUTF();
		abstractDescription = objectInput.readUTF();
		description = objectInput.readUTF();
		providerName = objectInput.readUTF();
		resourceRDF = objectInput.readUTF();
		repositoryRDF = objectInput.readUTF();
		status = objectInput.readInt();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		owner = objectInput.readUTF();
		imgId = objectInput.readLong();
		categoriamkpId = objectInput.readLong();
		pilotid = objectInput.readUTF();
		url = objectInput.readUTF();
		eId = objectInput.readUTF();
		extRating = objectInput.readInt();
		interactionPoint = objectInput.readUTF();
		language = objectInput.readUTF();
		hasMapping = objectInput.readBoolean();
		isPrivate = objectInput.readBoolean();
		lusdlmodel = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(artefattoId);
		objectOutput.writeLong(date);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (webpage == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(webpage);
		}

		if (abstractDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(abstractDescription);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (providerName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(providerName);
		}

		if (resourceRDF == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(resourceRDF);
		}

		if (repositoryRDF == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(repositoryRDF);
		}

		objectOutput.writeInt(status);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);

		if (owner == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(owner);
		}

		objectOutput.writeLong(imgId);
		objectOutput.writeLong(categoriamkpId);

		if (pilotid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pilotid);
		}

		if (url == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(url);
		}

		if (eId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(eId);
		}

		objectOutput.writeInt(extRating);

		if (interactionPoint == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(interactionPoint);
		}

		if (language == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(language);
		}

		objectOutput.writeBoolean(hasMapping);
		objectOutput.writeBoolean(isPrivate);

		if (lusdlmodel == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lusdlmodel);
		}
	}

	public String uuid;
	public long artefattoId;
	public long date;
	public String title;
	public String webpage;
	public String abstractDescription;
	public String description;
	public String providerName;
	public String resourceRDF;
	public String repositoryRDF;
	public int status;
	public long companyId;
	public long groupId;
	public long userId;
	public String owner;
	public long imgId;
	public long categoriamkpId;
	public String pilotid;
	public String url;
	public String eId;
	public int extRating;
	public String interactionPoint;
	public String language;
	public boolean hasMapping;
	public boolean isPrivate;
	public String lusdlmodel;
}