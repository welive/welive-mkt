/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the artifact organizations service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactOrganizationsPersistence
 * @see ArtifactOrganizationsUtil
 * @generated
 */
public class ArtifactOrganizationsPersistenceImpl extends BasePersistenceImpl<ArtifactOrganizations>
	implements ArtifactOrganizationsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ArtifactOrganizationsUtil} to access the artifact organizations persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ArtifactOrganizationsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByOrganizationId_Status",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrganizationId_Status",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ArtifactOrganizationsModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			ArtifactOrganizationsModelImpl.STATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONID_STATUS = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrganizationId_Status",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the artifact organizationses where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @return the matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status) throws SystemException {
		return findByOrganizationId_Status(organizationId, status,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @return the range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end)
		throws SystemException {
		return findByOrganizationId_Status(organizationId, status, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS;
			finderArgs = new Object[] { organizationId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS;
			finderArgs = new Object[] {
					organizationId, status,
					
					start, end, orderByComparator
				};
		}

		List<ArtifactOrganizations> list = (List<ArtifactOrganizations>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ArtifactOrganizations artifactOrganizations : list) {
				if ((organizationId != artifactOrganizations.getOrganizationId()) ||
						(status != artifactOrganizations.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(status);

				if (!pagination) {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactOrganizations>(list);
				}
				else {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByOrganizationId_Status_First(
		long organizationId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByOrganizationId_Status_First(organizationId,
				status, orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByOrganizationId_Status_First(
		long organizationId, int status, OrderByComparator orderByComparator)
		throws SystemException {
		List<ArtifactOrganizations> list = findByOrganizationId_Status(organizationId,
				status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByOrganizationId_Status_Last(
		long organizationId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByOrganizationId_Status_Last(organizationId,
				status, orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByOrganizationId_Status_Last(
		long organizationId, int status, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByOrganizationId_Status(organizationId, status);

		if (count == 0) {
			return null;
		}

		List<ArtifactOrganizations> list = findByOrganizationId_Status(organizationId,
				status, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	 *
	 * @param artifactOrganizationsPK the primary key of the current artifact organizations
	 * @param organizationId the organization ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations[] findByOrganizationId_Status_PrevAndNext(
		ArtifactOrganizationsPK artifactOrganizationsPK, long organizationId,
		int status, OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = findByPrimaryKey(artifactOrganizationsPK);

		Session session = null;

		try {
			session = openSession();

			ArtifactOrganizations[] array = new ArtifactOrganizationsImpl[3];

			array[0] = getByOrganizationId_Status_PrevAndNext(session,
					artifactOrganizations, organizationId, status,
					orderByComparator, true);

			array[1] = artifactOrganizations;

			array[2] = getByOrganizationId_Status_PrevAndNext(session,
					artifactOrganizations, organizationId, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ArtifactOrganizations getByOrganizationId_Status_PrevAndNext(
		Session session, ArtifactOrganizations artifactOrganizations,
		long organizationId, int status, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artifactOrganizations);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ArtifactOrganizations> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artifact organizationses where organizationId = &#63; and status = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrganizationId_Status(long organizationId, int status)
		throws SystemException {
		for (ArtifactOrganizations artifactOrganizations : findByOrganizationId_Status(
				organizationId, status, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(artifactOrganizations);
		}
	}

	/**
	 * Returns the number of artifact organizationses where organizationId = &#63; and status = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param status the status
	 * @return the number of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrganizationId_Status(long organizationId, int status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORGANIZATIONID_STATUS;

		Object[] finderArgs = new Object[] { organizationId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORGANIZATIONID_STATUS_ORGANIZATIONID_2 =
		"artifactOrganizations.id.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_ORGANIZATIONID_STATUS_STATUS_2 = "artifactOrganizations.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTID =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByArtifactId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByArtifactId",
			new String[] { Long.class.getName() },
			ArtifactOrganizationsModelImpl.ARTIFACTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTIFACTID = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByArtifactId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artifact organizationses where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @return the matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByArtifactId(long artifactId)
		throws SystemException {
		return findByArtifactId(artifactId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact organizationses where artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @return the range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByArtifactId(long artifactId,
		int start, int end) throws SystemException {
		return findByArtifactId(artifactId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact organizationses where artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByArtifactId(long artifactId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID;
			finderArgs = new Object[] { artifactId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTID;
			finderArgs = new Object[] { artifactId, start, end, orderByComparator };
		}

		List<ArtifactOrganizations> list = (List<ArtifactOrganizations>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ArtifactOrganizations artifactOrganizations : list) {
				if ((artifactId != artifactOrganizations.getArtifactId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTID_ARTIFACTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				if (!pagination) {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactOrganizations>(list);
				}
				else {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByArtifactId_First(long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByArtifactId_First(artifactId,
				orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByArtifactId_First(long artifactId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ArtifactOrganizations> list = findByArtifactId(artifactId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByArtifactId_Last(long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByArtifactId_Last(artifactId,
				orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByArtifactId_Last(long artifactId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArtifactId(artifactId);

		if (count == 0) {
			return null;
		}

		List<ArtifactOrganizations> list = findByArtifactId(artifactId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artifact organizationses before and after the current artifact organizations in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactOrganizationsPK the primary key of the current artifact organizations
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations[] findByArtifactId_PrevAndNext(
		ArtifactOrganizationsPK artifactOrganizationsPK, long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = findByPrimaryKey(artifactOrganizationsPK);

		Session session = null;

		try {
			session = openSession();

			ArtifactOrganizations[] array = new ArtifactOrganizationsImpl[3];

			array[0] = getByArtifactId_PrevAndNext(session,
					artifactOrganizations, artifactId, orderByComparator, true);

			array[1] = artifactOrganizations;

			array[2] = getByArtifactId_PrevAndNext(session,
					artifactOrganizations, artifactId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ArtifactOrganizations getByArtifactId_PrevAndNext(
		Session session, ArtifactOrganizations artifactOrganizations,
		long artifactId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

		query.append(_FINDER_COLUMN_ARTIFACTID_ARTIFACTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(artifactId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artifactOrganizations);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ArtifactOrganizations> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artifact organizationses where artifactId = &#63; from the database.
	 *
	 * @param artifactId the artifact ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArtifactId(long artifactId) throws SystemException {
		for (ArtifactOrganizations artifactOrganizations : findByArtifactId(
				artifactId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artifactOrganizations);
		}
	}

	/**
	 * Returns the number of artifact organizationses where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @return the number of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArtifactId(long artifactId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTIFACTID;

		Object[] finderArgs = new Object[] { artifactId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTID_ARTIFACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTIFACTID_ARTIFACTID_2 = "artifactOrganizations.id.artifactId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganizationId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID =
		new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganizationId",
			new String[] { Long.class.getName() },
			ArtifactOrganizationsModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONID = new FinderPath(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganizationId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artifact organizationses where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId(long organizationId)
		throws SystemException {
		return findByOrganizationId(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact organizationses where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @return the range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end) throws SystemException {
		return findByOrganizationId(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact organizationses where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<ArtifactOrganizations> list = (List<ArtifactOrganizations>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ArtifactOrganizations artifactOrganizations : list) {
				if ((organizationId != artifactOrganizations.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				if (!pagination) {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactOrganizations>(list);
				}
				else {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByOrganizationId_First(
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByOrganizationId_First(organizationId,
				orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByOrganizationId_First(
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		List<ArtifactOrganizations> list = findByOrganizationId(organizationId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByOrganizationId_Last(
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByOrganizationId_Last(organizationId,
				orderByComparator);

		if (artifactOrganizations != null) {
			return artifactOrganizations;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactOrganizationsException(msg.toString());
	}

	/**
	 * Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByOrganizationId_Last(
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByOrganizationId(organizationId);

		if (count == 0) {
			return null;
		}

		List<ArtifactOrganizations> list = findByOrganizationId(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63;.
	 *
	 * @param artifactOrganizationsPK the primary key of the current artifact organizations
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations[] findByOrganizationId_PrevAndNext(
		ArtifactOrganizationsPK artifactOrganizationsPK, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = findByPrimaryKey(artifactOrganizationsPK);

		Session session = null;

		try {
			session = openSession();

			ArtifactOrganizations[] array = new ArtifactOrganizationsImpl[3];

			array[0] = getByOrganizationId_PrevAndNext(session,
					artifactOrganizations, organizationId, orderByComparator,
					true);

			array[1] = artifactOrganizations;

			array[2] = getByOrganizationId_PrevAndNext(session,
					artifactOrganizations, organizationId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ArtifactOrganizations getByOrganizationId_PrevAndNext(
		Session session, ArtifactOrganizations artifactOrganizations,
		long organizationId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artifactOrganizations);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ArtifactOrganizations> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artifact organizationses where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrganizationId(long organizationId)
		throws SystemException {
		for (ArtifactOrganizations artifactOrganizations : findByOrganizationId(
				organizationId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artifactOrganizations);
		}
	}

	/**
	 * Returns the number of artifact organizationses where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrganizationId(long organizationId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORGANIZATIONID;

		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTIFACTORGANIZATIONS_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2 = "artifactOrganizations.id.organizationId = ?";

	public ArtifactOrganizationsPersistenceImpl() {
		setModelClass(ArtifactOrganizations.class);
	}

	/**
	 * Caches the artifact organizations in the entity cache if it is enabled.
	 *
	 * @param artifactOrganizations the artifact organizations
	 */
	@Override
	public void cacheResult(ArtifactOrganizations artifactOrganizations) {
		EntityCacheUtil.putResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			artifactOrganizations.getPrimaryKey(), artifactOrganizations);

		artifactOrganizations.resetOriginalValues();
	}

	/**
	 * Caches the artifact organizationses in the entity cache if it is enabled.
	 *
	 * @param artifactOrganizationses the artifact organizationses
	 */
	@Override
	public void cacheResult(List<ArtifactOrganizations> artifactOrganizationses) {
		for (ArtifactOrganizations artifactOrganizations : artifactOrganizationses) {
			if (EntityCacheUtil.getResult(
						ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
						ArtifactOrganizationsImpl.class,
						artifactOrganizations.getPrimaryKey()) == null) {
				cacheResult(artifactOrganizations);
			}
			else {
				artifactOrganizations.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all artifact organizationses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ArtifactOrganizationsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ArtifactOrganizationsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the artifact organizations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ArtifactOrganizations artifactOrganizations) {
		EntityCacheUtil.removeResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			artifactOrganizations.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ArtifactOrganizations> artifactOrganizationses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ArtifactOrganizations artifactOrganizations : artifactOrganizationses) {
			EntityCacheUtil.removeResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
				ArtifactOrganizationsImpl.class,
				artifactOrganizations.getPrimaryKey());
		}
	}

	/**
	 * Creates a new artifact organizations with the primary key. Does not add the artifact organizations to the database.
	 *
	 * @param artifactOrganizationsPK the primary key for the new artifact organizations
	 * @return the new artifact organizations
	 */
	@Override
	public ArtifactOrganizations create(
		ArtifactOrganizationsPK artifactOrganizationsPK) {
		ArtifactOrganizations artifactOrganizations = new ArtifactOrganizationsImpl();

		artifactOrganizations.setNew(true);
		artifactOrganizations.setPrimaryKey(artifactOrganizationsPK);

		return artifactOrganizations;
	}

	/**
	 * Removes the artifact organizations with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param artifactOrganizationsPK the primary key of the artifact organizations
	 * @return the artifact organizations that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations remove(
		ArtifactOrganizationsPK artifactOrganizationsPK)
		throws NoSuchArtifactOrganizationsException, SystemException {
		return remove((Serializable)artifactOrganizationsPK);
	}

	/**
	 * Removes the artifact organizations with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the artifact organizations
	 * @return the artifact organizations that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations remove(Serializable primaryKey)
		throws NoSuchArtifactOrganizationsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ArtifactOrganizations artifactOrganizations = (ArtifactOrganizations)session.get(ArtifactOrganizationsImpl.class,
					primaryKey);

			if (artifactOrganizations == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchArtifactOrganizationsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(artifactOrganizations);
		}
		catch (NoSuchArtifactOrganizationsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ArtifactOrganizations removeImpl(
		ArtifactOrganizations artifactOrganizations) throws SystemException {
		artifactOrganizations = toUnwrappedModel(artifactOrganizations);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(artifactOrganizations)) {
				artifactOrganizations = (ArtifactOrganizations)session.get(ArtifactOrganizationsImpl.class,
						artifactOrganizations.getPrimaryKeyObj());
			}

			if (artifactOrganizations != null) {
				session.delete(artifactOrganizations);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (artifactOrganizations != null) {
			clearCache(artifactOrganizations);
		}

		return artifactOrganizations;
	}

	@Override
	public ArtifactOrganizations updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws SystemException {
		artifactOrganizations = toUnwrappedModel(artifactOrganizations);

		boolean isNew = artifactOrganizations.isNew();

		ArtifactOrganizationsModelImpl artifactOrganizationsModelImpl = (ArtifactOrganizationsModelImpl)artifactOrganizations;

		Session session = null;

		try {
			session = openSession();

			if (artifactOrganizations.isNew()) {
				session.save(artifactOrganizations);

				artifactOrganizations.setNew(false);
			}
			else {
				session.merge(artifactOrganizations);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ArtifactOrganizationsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((artifactOrganizationsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artifactOrganizationsModelImpl.getOriginalOrganizationId(),
						artifactOrganizationsModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS,
					args);

				args = new Object[] {
						artifactOrganizationsModelImpl.getOrganizationId(),
						artifactOrganizationsModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID_STATUS,
					args);
			}

			if ((artifactOrganizationsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artifactOrganizationsModelImpl.getOriginalArtifactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID,
					args);

				args = new Object[] {
						artifactOrganizationsModelImpl.getArtifactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID,
					args);
			}

			if ((artifactOrganizationsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artifactOrganizationsModelImpl.getOriginalOrganizationId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID,
					args);

				args = new Object[] {
						artifactOrganizationsModelImpl.getOrganizationId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID,
					args);
			}
		}

		EntityCacheUtil.putResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactOrganizationsImpl.class,
			artifactOrganizations.getPrimaryKey(), artifactOrganizations);

		return artifactOrganizations;
	}

	protected ArtifactOrganizations toUnwrappedModel(
		ArtifactOrganizations artifactOrganizations) {
		if (artifactOrganizations instanceof ArtifactOrganizationsImpl) {
			return artifactOrganizations;
		}

		ArtifactOrganizationsImpl artifactOrganizationsImpl = new ArtifactOrganizationsImpl();

		artifactOrganizationsImpl.setNew(artifactOrganizations.isNew());
		artifactOrganizationsImpl.setPrimaryKey(artifactOrganizations.getPrimaryKey());

		artifactOrganizationsImpl.setOrganizationId(artifactOrganizations.getOrganizationId());
		artifactOrganizationsImpl.setArtifactId(artifactOrganizations.getArtifactId());
		artifactOrganizationsImpl.setStatus(artifactOrganizations.getStatus());

		return artifactOrganizationsImpl;
	}

	/**
	 * Returns the artifact organizations with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the artifact organizations
	 * @return the artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByPrimaryKey(Serializable primaryKey)
		throws NoSuchArtifactOrganizationsException, SystemException {
		ArtifactOrganizations artifactOrganizations = fetchByPrimaryKey(primaryKey);

		if (artifactOrganizations == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchArtifactOrganizationsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return artifactOrganizations;
	}

	/**
	 * Returns the artifact organizations with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException} if it could not be found.
	 *
	 * @param artifactOrganizationsPK the primary key of the artifact organizations
	 * @return the artifact organizations
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations findByPrimaryKey(
		ArtifactOrganizationsPK artifactOrganizationsPK)
		throws NoSuchArtifactOrganizationsException, SystemException {
		return findByPrimaryKey((Serializable)artifactOrganizationsPK);
	}

	/**
	 * Returns the artifact organizations with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the artifact organizations
	 * @return the artifact organizations, or <code>null</code> if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ArtifactOrganizations artifactOrganizations = (ArtifactOrganizations)EntityCacheUtil.getResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
				ArtifactOrganizationsImpl.class, primaryKey);

		if (artifactOrganizations == _nullArtifactOrganizations) {
			return null;
		}

		if (artifactOrganizations == null) {
			Session session = null;

			try {
				session = openSession();

				artifactOrganizations = (ArtifactOrganizations)session.get(ArtifactOrganizationsImpl.class,
						primaryKey);

				if (artifactOrganizations != null) {
					cacheResult(artifactOrganizations);
				}
				else {
					EntityCacheUtil.putResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
						ArtifactOrganizationsImpl.class, primaryKey,
						_nullArtifactOrganizations);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ArtifactOrganizationsModelImpl.ENTITY_CACHE_ENABLED,
					ArtifactOrganizationsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return artifactOrganizations;
	}

	/**
	 * Returns the artifact organizations with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param artifactOrganizationsPK the primary key of the artifact organizations
	 * @return the artifact organizations, or <code>null</code> if a artifact organizations with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactOrganizations fetchByPrimaryKey(
		ArtifactOrganizationsPK artifactOrganizationsPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)artifactOrganizationsPK);
	}

	/**
	 * Returns all the artifact organizationses.
	 *
	 * @return the artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact organizationses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @return the range of artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact organizationses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artifact organizationses
	 * @param end the upper bound of the range of artifact organizationses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactOrganizations> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ArtifactOrganizations> list = (List<ArtifactOrganizations>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ARTIFACTORGANIZATIONS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ARTIFACTORGANIZATIONS;

				if (pagination) {
					sql = sql.concat(ArtifactOrganizationsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactOrganizations>(list);
				}
				else {
					list = (List<ArtifactOrganizations>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the artifact organizationses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ArtifactOrganizations artifactOrganizations : findAll()) {
			remove(artifactOrganizations);
		}
	}

	/**
	 * Returns the number of artifact organizationses.
	 *
	 * @return the number of artifact organizationses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ARTIFACTORGANIZATIONS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the artifact organizations persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ArtifactOrganizations>> listenersList = new ArrayList<ModelListener<ArtifactOrganizations>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ArtifactOrganizations>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ArtifactOrganizationsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ARTIFACTORGANIZATIONS = "SELECT artifactOrganizations FROM ArtifactOrganizations artifactOrganizations";
	private static final String _SQL_SELECT_ARTIFACTORGANIZATIONS_WHERE = "SELECT artifactOrganizations FROM ArtifactOrganizations artifactOrganizations WHERE ";
	private static final String _SQL_COUNT_ARTIFACTORGANIZATIONS = "SELECT COUNT(artifactOrganizations) FROM ArtifactOrganizations artifactOrganizations";
	private static final String _SQL_COUNT_ARTIFACTORGANIZATIONS_WHERE = "SELECT COUNT(artifactOrganizations) FROM ArtifactOrganizations artifactOrganizations WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "artifactOrganizations.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ArtifactOrganizations exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ArtifactOrganizations exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ArtifactOrganizationsPersistenceImpl.class);
	private static ArtifactOrganizations _nullArtifactOrganizations = new ArtifactOrganizationsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ArtifactOrganizations> toCacheModel() {
				return _nullArtifactOrganizationsCacheModel;
			}
		};

	private static CacheModel<ArtifactOrganizations> _nullArtifactOrganizationsCacheModel =
		new CacheModel<ArtifactOrganizations>() {
			@Override
			public ArtifactOrganizations toEntityModel() {
				return _nullArtifactOrganizations;
			}
		};
}