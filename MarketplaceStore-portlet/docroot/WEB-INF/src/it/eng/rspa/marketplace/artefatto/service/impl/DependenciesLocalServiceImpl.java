/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.util.ArrayList;
import java.util.List;

import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.service.base.DependenciesLocalServiceBaseImpl;

/**
 * The implementation of the dependencies local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.DependenciesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.DependenciesLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil
 */
public class DependenciesLocalServiceImpl
	extends DependenciesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil} to access the dependencies local service.
	 */
	
	public List<Dependencies> getDependenciesByArtefactId(long artefactId){
		List<Dependencies> out = new ArrayList<Dependencies>();
		try{ out = dependenciesPersistence.findByArtefactId(artefactId); }
		catch(Exception e){ System.out.println(e); }
		
		return out;
	}
	
	public List<Dependencies> getDependenciesByDependsFrom(long dependesFromId){
		List<Dependencies> out = new ArrayList<Dependencies>();
		try{ out = dependenciesPersistence.findByDependsFrom(dependesFromId); }
		catch(Exception e){ System.out.println(e); }
		
		return out;
	}
	
	public Dependencies getDependencyByIDsCouple(long artefactId, long dependsFromId){
		Dependencies out = null;
		try{ out = dependenciesPersistence.findByidsCouple(artefactId, dependsFromId); }
		catch(Exception e){ System.out.println(e); }
		
		return out;
	}
	
}