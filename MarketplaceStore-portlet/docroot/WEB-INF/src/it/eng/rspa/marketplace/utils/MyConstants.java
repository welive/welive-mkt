package it.eng.rspa.marketplace.utils;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;

public final class MyConstants {

	
	
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_UUSIMAA = "Uusimaa";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_TRENTO = "Trento";
	
	public final static String UI_PILOT_CITY_BILBAO = "Bilbao";
	public final static String UI_PILOT_CITY_UUSIMAA = "Helsinki Region";
	public final static String UI_PILOT_CITY_NOVISAD = "Novi Sad";
	public final static String UI_PILOT_CITY_TRENTO = "Trento";
	
	public final static String LANGUAGE_ENGLISH = "English";
	public final static String LANGUAGE_SPANISH = "Spanish";
	public final static String LANGUAGE_ITALIAN = "Italian";
	public final static String LANGUAGE_FINNISH = "Finnish";
	public final static String LANGUAGE_SERBIAN = "Serbian";
	public final static String LANGUAGE_SERBIAN_LATIN = "SerbianLatin";
	
	public final static String ACRONIM_LANGUAGE_SPANISH = "es";
	public final static String ACRONIM_LANGUAGE_ITALIAN = "it";
	public final static String ACRONIM_LANGUAGE_FINNISH = "fi";
	public final static String ACRONIM_LANGUAGE_SERBIAN = "sr";
	
	public final static String ACRONIM_COUNTRY_SPAIN = "ES";
	public final static String ACRONIM_COUNTRY_ITALY= "IT";
	public final static String ACRONIM_COUNTRY_FINLAND = "FI";
	public final static String ACRONIM_COUNTRY_SERBIA = "RS";
	
	public static final long API_DL_ID = MyMarketplaceUtility.getDefaultGroupId();
	
	
}
