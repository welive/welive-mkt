package it.eng.rspa.marketplace;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;
import it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil;

public abstract class ConfUtil {
	
	private static Log _log = LogFactoryUtil.getLog(ConfUtil.class);
	
	public static Long getLong(String key){
		
		long retval = -1L;
		MarketplaceConf conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(key);
		if(conf!=null && conf.getType().equalsIgnoreCase("Long")){
			try{ retval = Long.parseLong(conf.getValue()); }
			catch(Exception e){ _log.error(e.getMessage()); }
		}
		return retval;
	}
	
	public static String getString(String key){
		String retval = "";
		MarketplaceConf conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(key);
		if(conf!=null && conf.getType().equalsIgnoreCase("String")){
			retval = conf.getValue();
		}
		return retval;
	}
	
	public static Boolean getBoolean(String key){
		Boolean retval = null;
		MarketplaceConf conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(key);
		if(conf!=null && conf.getType().equalsIgnoreCase("Boolean")){
			try{ retval = Boolean.parseBoolean(conf.getValue()); }
			catch(Exception e){ _log.error(e.getMessage()); }
		}
		return retval;
	}
	
	public static Integer getInteger(String key){
		Integer retval = null;
		MarketplaceConf conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(key);
		if(conf!=null && conf.getType().equalsIgnoreCase("Integer")){
			try{ retval = Integer.parseInt(conf.getValue()); }
			catch(Exception e){ _log.error(e.getMessage()); }
		}
		return retval;
	}
}
