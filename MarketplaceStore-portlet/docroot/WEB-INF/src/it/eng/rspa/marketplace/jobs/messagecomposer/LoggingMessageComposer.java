package it.eng.rspa.marketplace.jobs.messagecomposer;

import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dataset;
import it.eng.metamodel.definitions.Dependency;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.listeners.model.ArtefactType;
import it.eng.rspa.usdl.model.Artefact;
import it.eng.rspa.usdl.model.BusinessRole;
import it.eng.rspa.usdl.utils.EditorUtils;
import it.eng.rspa.usdl.utils.RDFUtils;
import it.eng.sesame.ReaderRDF;

import java.util.List;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

public abstract class LoggingMessageComposer
					extends CommonMessageComposer{
	
	private static Log _log = LogFactoryUtil.getLog(LoggingMessageComposer.class);

	public static JSONObject prepareLoggingPublicationMessage(Artefatto a){
		
		JSONObject message = JSONFactoryUtil.createJSONObject();
		message.put("artefactid", String.valueOf(a.getArtefattoId()));
		
		message.put("artefactname", a.getTitle());
		
		ArtefactType at = getArtefactType(a);
		message.put("artefacttype", at.toString());

		String resourceRDF = a.getResourceRDF();
		String lusdlmodel = a.getLusdlmodel();
		
		Long ccuid = getArtefactOwnerCcUid(a);
		if(ccuid!=null){ message.put("ownerid", ccuid.toString()); }
		else{ message.put("ownerid", EditorUtils.getPilotByUserId(a.getUserId())); }
		
		JSONArray organizations = JSONFactoryUtil.createJSONArray();
		try{
			List<Organization> orgs = OrganizationLocalServiceUtil.getUserOrganizations(a.getUserId());
			for(Organization o:orgs){
				try{ organizations.put(o.getName()); }
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		catch(Exception e){ e.printStackTrace(); }
		message.put("companies", organizations);
		
		String pilotID = getPilotId(a);
		message.put("pilot", pilotID);
		
		if(!resourceRDF.equals("") && lusdlmodel.equalsIgnoreCase("")){
		 	
	 		ReaderRDF readerRDF = getReaderRDF(resourceRDF);
	 		try{
	 			Artefact art = RDFUtils.getArtefact(readerRDF);
	 			String desc = art.getDescription().getValue();
		 		String abs = art.getAbstractDescription().getValue();
		 		if(a.getAbstractDescription()==null || a.getAbstractDescription().equalsIgnoreCase("")){
		 			if(abs==null || abs.equalsIgnoreCase("")){ abs = desc; }
		 			a.setAbstractDescription(abs);
		 		}
		 		else{ abs = a.getAbstractDescription(); }
		 		if(a.getDescription()==null || a.getDescription().equalsIgnoreCase("")){
		 			if(desc==null || desc.equalsIgnoreCase("")){ desc = abs; }
		 			a.setDescription(desc);
		 		}
		 		else{ desc = a.getDescription(); }
		 		
		 		ArtefattoLocalServiceUtil.updateArtefatto(a);
	 		}
	 		catch(Exception e){
	 			_log.warn("Unable to retrive the artefact description");
	 		}
			
			JSONArray dependencies = getArtefactDependencies(readerRDF);
			message.put("usedartefacts", dependencies);
			
			String providerName = getArtefactProvider(a, readerRDF);
			message.put("providername", providerName);
			
			JSONArray license = getArtefactLicense(readerRDF);
			message.put("license", license);
			
	 	}
		else if(!lusdlmodel.equalsIgnoreCase("")){
			switch(at){
			case BUILDING_BLOCK:{
					try{
						ParserResponse response = MetamodelParser.parse(lusdlmodel, MetamodelParser.SourceType.METAMODEL);
						BuildingBlock bb = response.getBuildingBlock();
						
						//Dependencies
						JSONArray dependencies = JSONFactoryUtil.createJSONArray();
						List<Dependency> deps = bb.getUsedBBs();
						for(Dependency d : deps){
							try{
								Long artid = d.getId();
								Artefatto da = ArtefattoLocalServiceUtil.getArtefatto(artid);
								
								Long depownerccuid = getArtefactOwnerCcUid(da);
								ArtefactType deptype = getArtefactType(da);
								
								JSONArray currdep = JSONFactoryUtil.createJSONArray();
								currdep.put(artid.toString());
								currdep.put(depownerccuid.toString());
								currdep.put(deptype.toString());
								
								dependencies.put(currdep);
								
							}
							catch(Exception e){
								_log.warn(e);
							}
						}
						message.put("usedartefacts", dependencies);
						
						
						//Provider
						message.put("providername", "");
						List<Entity> entities = bb.getBusinessRoles();
						for(Entity e : entities){
							if(e.getBusinessRole().equalsIgnoreCase(BusinessRole.PROVIDER.toString())){
								String providerName = e.getTitle();
								message.put("providername", providerName);
								break;
							}
						}
						
						//Licenses
						JSONArray license = JSONFactoryUtil.createJSONArray();
						List<License> lics = bb.getLegalConditions();
						for(License lic : lics){
							String sLic = lic.getTitle();
							license.put(sLic);
						}
						message.put("license", license);
					
					}
					catch(Exception e){
						_log.error(e);
						message = null;
					}
				}
				break;
			case DATASET:{
					try{
						Dataset ds = MetamodelParser.parseDataset(lusdlmodel);
						
						//Dependencies (always empty)
						JSONArray dependencies = JSONFactoryUtil.createJSONArray();
						message.put("usedartefacts", dependencies);
						
						//Provider
						message.put("providername", "");
						List<Entity> entities = ds.getBusinessRoles();
						for(Entity e : entities){
							if(e.getBusinessRole().equalsIgnoreCase(BusinessRole.PROVIDER.toString())){
								String providerName = e.getTitle();
								message.put("providername", providerName);
								break;
							}
						}
						
						//Licenses
						JSONArray license = JSONFactoryUtil.createJSONArray();
						List<License> lics = ds.getLegalConditions();
						for(License lic : lics){
							String sLic = lic.getTitle();
							license.put(sLic);
						}
						message.put("license", license);
						
					}
					catch(Exception e){
						_log.error(e);
						message = null;
					}
				}
				break;
			case PUBLIC_SERVICE_APPLICATION:{
					try{
						PublicServiceApplication psa = MetamodelParser.parsePSA(lusdlmodel);
						
						//Dependencies
						JSONArray dependencies = JSONFactoryUtil.createJSONArray();
						List<Dependency> deps = psa.getUsedBBs();
						for(Dependency d : deps){
							try{
								long artid = d.getId();
								Artefatto da = ArtefattoLocalServiceUtil.getArtefatto(artid);
								
								long depownerccuid = getArtefactOwnerCcUid(da);
								ArtefactType deptype = getArtefactType(da);
								
								JSONArray currdep = JSONFactoryUtil.createJSONArray();
								currdep.put(artid);
								currdep.put(depownerccuid);
								currdep.put(deptype.toString());
								
								dependencies.put(currdep);
								
							}
							catch(Exception e){
								_log.warn(e);
							}
						}
						
						//Provider
						message.put("providername", "");
						List<Entity> entities = psa.getBusinessRoles();
						for(Entity e : entities){
							if(e.getBusinessRole().equalsIgnoreCase(BusinessRole.PROVIDER.toString())){
								String providerName = e.getTitle();
								message.put("providername", providerName);
								break;
							}
						}
						
						//Licenses
						JSONArray license = JSONFactoryUtil.createJSONArray();
						List<License> lics = psa.getLegalConditions();
						for(License lic : lics){
							String sLic = lic.getTitle();
							license.put(sLic);
						}
						message.put("license", license);
						
					}
					catch(Exception e){
						_log.warn(e);
						message = null;
					}
				}
				break;
			
			default:
				message = null;
				break;
			
			}
			
		}
		else return null;
		
		return message;
	}
	
	public static JSONObject prepareLoggingUpdateMessage(Artefatto a){
		JSONObject message = JSONFactoryUtil.createJSONObject();

		message.put("artefactid", a.getArtefattoId());
		
		ArtefactType at = getArtefactType(a);
		message.put("artefacttype", at.toString());

		String resourceRDF = a.getResourceRDF();
		String lusdlmodel = a.getLusdlmodel();
		
	 	if(!resourceRDF.equals("") && lusdlmodel.equalsIgnoreCase("") ){
		 	
	 		ReaderRDF readerRDF = getReaderRDF(resourceRDF);
			
			Long ccuid = getArtefactOwnerCcUid(a);
			if(ccuid!=null){ message.put("ownerid", ccuid); }
			else{ return null; }
			
			JSONArray dependencies = getArtefactDependencies(readerRDF);
			message.put("usedartefacts", dependencies);
			
			JSONArray license = getArtefactLicense(readerRDF);
			message.put("license", license);
			
			return message;
	 	}
	 	else return null;
	}
	
	public static JSONObject prepareLoggingRemoveMessage(Artefatto a, String opts){
		
		JSONObject message = JSONFactoryUtil.createJSONObject();

		message.put("artefactid", String.valueOf(a.getArtefattoId()));
		
		ArtefactType at = getArtefactType(a);
		message.put("artefacttype", at.toString());
		
		String pilotID = getPilotId(a);
		message.put("pilot", pilotID);
		
		//Long ccuid = getArtefactOwnerCcUid(a);
		Long ccuid = getArtefactOwnerCcUid(a,opts);
		if(ccuid!=null){ message.put("ownerid", ccuid.toString()); }
		else{ return null; }
		
		message.put("license", JSONFactoryUtil.createJSONArray());
		
//		String resourceRDF = a.getResourceRDF();
//	 	if(!resourceRDF.equals("")){
//	 		try{
//		 		ReaderRDF readerRDF = getReaderRDF(resourceRDF);
//				
//				JSONArray license = getArtefactLicense(readerRDF);
//				message.put("license", license);
//	 		}
//	 		catch(Exception e){
//	 			_log.error(e);
//	 		}
//	 	}
	 	
	 	return message;
	}

	public static JSONObject prepareLoggingRatingMessage(Artefatto a, String opts) {

		try { return JSONFactoryUtil.createJSONObject(opts); } 
		catch (JSONException e) { e.printStackTrace(); }
		
		return null;
	}

	public static JSONObject prepareLoggingRatingRemovedMessage(Artefatto a, String opts) {
		return null;
	}
	
}
