package it.eng.rspa.marketplace.jobs;

import java.lang.reflect.Type;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class OdsNotificationService {
	
	private static Log _log = LogFactoryUtil.getLog(OdsNotificationService.class);
	
	
	private static String buildRatingUrl(String datasetid, long ccuid){
		return ConfUtil.getString("ods.url")+datasetid+"/rating/user/"+ccuid;
	}
	
	private static String buildDeleteUrl(String datasetid, long ccuid){
		return ConfUtil.getString("ods.url")+datasetid+"/user/"+ccuid;
	}
	
	public static boolean sendNotification(String method, Artefatto a, JSONObject body, String opts){
		
		if("delete".equalsIgnoreCase(method)){
			String datasetid = a.getEId();
			
			Gson gson = new Gson();
			Type settype = new TypeToken<Map<String, String>>(){}.getType();
			Map<String, String> options = gson.fromJson(opts, settype);
			
			long ccuid = Long.parseLong(options.get("ccuid"));
			
			String path = buildDeleteUrl(datasetid, ccuid);
			
			try{
				Client client = Client.create();
				
				client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
				
				WebResource webResource = client.resource(path);
				
				_log.info("Invoking [Delete]: "+path);
				ClientResponse resp = webResource.delete(ClientResponse.class);
				if(resp.getStatus() != 200){
					String respString = resp.getEntity(String.class);
					_log.warn("Service "+path+" responded with status "+resp.getStatus()+" and body "+respString);
					return false;
				}
				_log.info("ODS notified correctly");
				return true;
			}
			catch(Exception e){
				_log.error("Unable to send notification to " + path+": "+e);
				return false;
			}
			
		}
		else if("put".equalsIgnoreCase(method)){
			Gson gson = new Gson();
			Type settype = new TypeToken<Map<String, String>>(){}.getType();
			Map<String, String> options = gson.fromJson(opts, settype);
			
			String datasetid = a.getEId();
			long ccuid = Long.parseLong(options.get("ccuserid"));
			
			String path = buildRatingUrl(datasetid, ccuid);
			
			try{
				Client client = Client.create();
				
				client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
				WebResource webResource = client.resource(path);
				
				_log.debug("Invoking [PUT]: "+path + " with body "+body);
				ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class, body.toString());
				if(resp.getStatus() != 200){
					String respString = resp.getEntity(String.class);
					_log.warn("Service "+path+" responded with status "+resp.getStatus()+" and body "+respString);
					return false;
				}
				_log.debug("ODS notified correctly");
				return true;
			}
			catch(Exception e){
				_log.error("Unable to send notification to " + path+": "+e);
				return false;
			}
		}

		return true;
		
		
	}

}
