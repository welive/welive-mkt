package it.eng.rspa.marketplace.indexer;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletURL;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;

public class ArtefattoIndexer extends BaseIndexer {

	public static final String[] CLASS_NAMES = {Artefatto.class.getName()};
	public static final String PORTLET_ID = "marketplacestore_WAR_MarketplaceStoreportlet";

	public String[] getClassNames() { 
		return ArtefattoIndexer.CLASS_NAMES;
	}	

	@Override
	public String getPortletId() {
		return ArtefattoIndexer.PORTLET_ID;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletURL portletURL) throws Exception {
		
		String title = document.get(Field.TITLE);
		String content = snippet;
		if (Validator.isNull(snippet)) {
			content = document.get(Field.DESCRIPTION);
			if (Validator.isNull(content)) {
				content = StringUtil.shorten(HtmlUtil.escape(document.get(Field.CONTENT)), 200);
			}
			content = StringEscapeUtils.unescapeHtml4(content);
		}
		String resourcePrimKey = document.get(Field.ENTRY_CLASS_PK);
		portletURL.setParameter("jspPage", "html/marketplacestore/view_artefatto.jsp");
		portletURL.setParameter("resourcePrimKey", resourcePrimKey);
		return new Summary(title, content, portletURL);
	}

	@Override
	protected Document doGetDocument(Object obj) throws Exception {
		
		Artefatto entry = (Artefatto)obj;
		
		long companyId = entry.getCompanyId();
		long groupId = getSiteGroupId(entry.getGroupId());
		long scopeGroupId = entry.getGroupId();
		long userId = entry.getUserId();
		long resourcePrimKey = entry.getArtefattoId();
		String[] assetTagNames = AssetTagLocalServiceUtil.getTagNames(Artefatto.class.getName(), resourcePrimKey);
		String name = entry.getTitle();
		String description=entry.getDescription();

		Date modifiedDate = entry.getDate();
		String content = entry.getAbstractDescription();
		String regex ="\\<[^\\>]*\\>";
		String replacement = "";
		content = content.replaceAll(regex, replacement);

		Document document = new DocumentImpl();
		document.addUID(PORTLET_ID, resourcePrimKey);
		document.addDate(Field.MODIFIED_DATE,modifiedDate);

		document.addKeyword(Field.COMPANY_ID, companyId);
		document.addKeyword(Field.PORTLET_ID, PORTLET_ID);
		document.addKeyword(Field.GROUP_ID, groupId);
		document.addKeyword(Field.SCOPE_GROUP_ID, scopeGroupId);
		document.addKeyword(Field.USER_ID, userId);
		document.addText(Field.TITLE, name);
		document.addText(Field.DESCRIPTION, description);
		document.addKeyword(Field.ASSET_TAG_NAMES, assetTagNames);
		document.addKeyword(Field.ENTRY_CLASS_NAME, Artefatto.class.getName());
		document.addKeyword(Field.ENTRY_CLASS_PK, resourcePrimKey);
		document.addText(Field.CONTENT, content);
		
		return document;
	}

	protected void doDelete(Object obj) throws Exception {
		Artefatto entry = (Artefatto)obj;
		Document document = new DocumentImpl();
		document.addUID(getPortletId(), Long.toString(entry.getPrimaryKey()));
		SearchEngineUtil.deleteDocument(getSearchEngineId(), entry.getCompanyId(), document.get(Field.UID), true);
	}

	public void postProcessSearchQuery(
			BooleanQuery searchQuery, SearchContext searchContext)
					throws Exception {		
		addSearchTerm(searchQuery, searchContext, Field.NAME, true);
		addSearchTerm(searchQuery, searchContext, Field.DESCRIPTION, true);
		addSearchTerm(searchQuery, searchContext, Field.TITLE, true);
//		addSearchTerm(searchQuery, searchContext, Field.URL, true);
//		addSearchTerm(searchQuery, searchContext, "abstractDescription", true);	
//		addSearchTerm(searchQuery, searchContext, "providerName", true);	
		
	}	

	protected void doReindex(Object obj) throws Exception {		 
		Artefatto entry = (Artefatto)obj;		
		Document document = getDocument(entry);  
		SearchEngineUtil.updateDocument(getSearchEngineId(), entry.getCompanyId(), document, true);
		
		return;
	}

	protected void doReindex(String[] ids) throws Exception {		
		long companyId = GetterUtil.getLong(ids[0]);
		reindexEntries(companyId);
		
		return;
	}

	protected void doReindex(String className, long classPK) throws Exception {		
		Artefatto entry = ArtefattoLocalServiceUtil.getArtefatto((int) classPK);
		doReindex(entry);	
		
		return;
	}	
	
	protected String getPortletId(SearchContext searchContext) {		
		return ArtefattoIndexer.PORTLET_ID;
	}

	protected void reindexEntries(long companyId) throws Exception {
		int count = ArtefattoLocalServiceUtil.getArtefattosCount();
		int pages = count / Indexer.DEFAULT_INTERVAL;
		for (int i = 0; i <= pages; i++) {
			int start = (i * Indexer.DEFAULT_INTERVAL);
			int end = start + Indexer.DEFAULT_INTERVAL;

			reindexEntries(companyId, start, end);
		}
		
		return;
	}

	protected void reindexEntries(long companyId, int start, int end)
			throws Exception {
		List<Artefatto> entries = ArtefattoLocalServiceUtil.getArtefattos(0, ArtefattoLocalServiceUtil.getArtefattosCount());
		if (entries.isEmpty()) {
			return;
		}
		Collection<Document> documents = new ArrayList<Document>();
		for (Artefatto entry : entries) {
			Document document = getDocument(entry);
			documents.add(document);
		}
		SearchEngineUtil.updateDocuments(getSearchEngineId(), companyId, documents, true);
		
		return;
	}

}
