package it.eng.rspa.marketplace.listeners;

import java.util.ArrayList;
import java.util.List;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;

public class OIANotifier implements iArtefactListener {

	@Override
	public void onArtefactPublish(Artefatto a) {
		//DO nothing
		return;
	}

	@Override
	public void onArtefactDelete(Artefatto a) {
		long artid = a.getArtefattoId();
		List<CLSArtifacts> idearts = new ArrayList<CLSArtifacts>();
		
		try{ idearts = CLSArtifactsLocalServiceUtil.getArtifactsByArtifactId(artid); }
		catch(Exception e){ e.printStackTrace(); }
		
		if(idearts!=null && !idearts.isEmpty()){
			
			for(CLSArtifacts ideart : idearts){
				try{ CLSArtifactsLocalServiceUtil.deleteCLSArtifacts(ideart); }
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		
		System.out.println("Idea associations deleted for artefact " + artid);
		
		return;
		
	}

	@Override
	public void onArtefactUpdate(Artefatto a) {
		//DO nothing
		return;
	}

}
