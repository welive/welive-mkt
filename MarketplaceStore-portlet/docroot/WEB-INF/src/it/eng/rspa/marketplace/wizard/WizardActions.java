package it.eng.rspa.marketplace.wizard;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.eng.rspa.RDFException;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.usdl.model.Artefact;
import it.eng.rspa.usdl.model.BuildingBlock;
import it.eng.rspa.usdl.model.Entity;
import it.eng.rspa.usdl.model.Namespace;
import it.eng.rspa.usdl.model.PublicServiceApplication;
import it.eng.rspa.usdl.utils.EditorUtils;
import it.eng.rspa.usdl.utils.RDFUtils;
import it.eng.sesame.Crud;
import it.eng.sesame.ReaderRDF;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public class WizardActions extends MyMarketplaceUtility{
	
	public static enum DLFOLDER_TYPE { MAINMARKET, MAINARTIFACT, MAINIMAGES, SUBIMAGES };	
	public static final String USER_NOT_LOGGED_IN_KEY = "USER_NOT_LOGGED_KEY";
	public static final String ARTIFACT_NOT_EXISTS_KEY = "ARTIFACT_NOT_EXISTS_KEY";
	public static final String RDF_REPOSITORY_ERROR = "RDF_REPOSITORY_ERROR";
	public static final String INVALID_RDF_ERROR = "The provided RDF is not valid";
	public static final String DLFOLDER_ARTIFACT_IMAGES = "images";
	
	private static final Set<Long> psaIds;
	private static final Set<Long> bbIds;
	
	
	private static Log _log = LogFactoryUtil.getLog(Artefatto.class);
	
//	/**
//	 * Salva il tag-set associato all'artefatto.
//	 * @param actionRequest
//	 * @param actionResponse
//	 * @throws Exception
//	 */	
//	
//	public static void salvaTag(ActionRequest actionRequest, ActionResponse aResp) throws Exception{
//		aResp.setRenderParameter("jspPage", actionRequest.getParameter( "jspPage"));
//		User user =PortalUtil.getUser(actionRequest);
//		if (user==null){
//			_log.error(USER_NOT_LOGGED_IN_KEY);
//			throw new Exception(USER_NOT_LOGGED_IN_KEY);
//		}
//		PortletSession session = actionRequest.getPortletSession();
//		WizardUtil wub = ((WizardUtil)session.getAttribute("wizardUtilBean"));
//		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
//		AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(wub.getArtefatto().getUserId(), 
//												wub.getArtefatto().getGroupId(), 
//												Artefatto.class.getName(), 
//												wub.getIdArtefatto(), 
//												serviceContext.getAssetCategoryIds(), 
//												serviceContext.getAssetTagNames());
//		
//		ae.setVisible(false);
//		AssetEntryLocalServiceUtil.updateAssetEntry(ae);
//	}
	
	
	static{
		psaIds = new HashSet<Long>();
		String[] psaCats = PortletProps.get("Category.catsOfType.psa").split(",");
		for(String psaCat:psaCats){
			try{
				Categoria cat = CategoriaLocalServiceUtil.getCategoriaByName(psaCat);
				if(cat!=null) psaIds.add(cat.getIdCategoria());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		
		bbIds = new HashSet<Long>();
		String[] bbCats = PortletProps.get("Category.catsOfType.bblocks").split(",");
		for(String bbCat:bbCats){
			try{
				Categoria cat = CategoriaLocalServiceUtil.getCategoriaByName(bbCat);
				if(cat!=null) bbIds.add(cat.getIdCategoria());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
	}
	
	/**
	 * Salva l'artefatto come "Pubblicato". L'artefatto sar� visibile nella homepage del Marketplace
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public static Artefatto approvaArtefatto(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		actionResponse.setRenderParameter("jspPage", uploadPortletRequest.getParameter( "jspPage"));
		
		User user =PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		long idArtefatto = Long.parseLong(uploadPortletRequest.getParameter("idArtefatto"));
		
		Artefatto a =ArtefattoLocalServiceUtil.publishArtefatto(idArtefatto);
		
		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
		AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(a.getUserId(), 
																a.getGroupId(), 
																Artefatto.class.getName(), 
																idArtefatto, 
																serviceContext.getAssetCategoryIds(), 
																serviceContext.getAssetTagNames());

		ae.setVisible(true);
		AssetEntryLocalServiceUtil.updateAssetEntry(ae);
		
		Iterator<ArtifactOrganizations> it = ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByArtifactId(a.getArtefattoId()).iterator();

		while(it.hasNext()){
			ArtifactOrganizations ao = it.next();
			ao.setStatus(WorkflowConstants.STATUS_APPROVED);
			ArtifactOrganizationsLocalServiceUtil.updateArtifactOrganizations(ao);
		}
		
		
		
		uploadPortletRequest.setAttribute("idArtefatto", idArtefatto);
		uploadPortletRequest.setAttribute("resourcePrimKey", idArtefatto);
		uploadPortletRequest.setAttribute("approved", true);
		return a;
	}
	
	/**
	 * Salva l'artefatto come DRAFT. L'artefatto sar� visibile solo ai membri della company
	 * attraverso la pagina "I miei artefatti".
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public static void salvaBozza(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String redirectJspPage=uploadPortletRequest.getParameter( "jspPage");
		actionResponse.setRenderParameter("jspPage", redirectJspPage);
		
		User user =PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		//Di default gli artefatti vengono inizializzati come BOZZE. Nessuna operazione particolare richiesta
		//Qui mi occupo solamente di eliminare dalla sessione i dati dell'artefatto corrente
		
		//PortletSession session = actionRequest.getPortletSession();
		
		
//		uploadPortletRequest.setAttribute("idArtefatto", null);
//		uploadPortletRequest.setAttribute("idArtefatto", ((WizardUtil)session.getAttribute("wizardUtilBean")).getIdArtefatto()+"");
//		uploadPortletRequest.setAttribute("resourcePrimKey", ((WizardUtil)session.getAttribute("wizardUtilBean")).getIdArtefatto()+"");
//		actionResponse.setRenderParameter("idArtefatto", ((WizardUtil)session.getAttribute("wizardUtilBean")).getIdArtefatto()+"");
//		MyMarketplaceUtility.cleanSession(session);
		
		return;
	}

	/**
	 * Elimina l'artefatto dall'elenco degli asset e i file ad esso associati, che sono: 
	 * l'icona,  le immagini di carosello e il file applicazione.
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public static Artefatto deleteArtifact(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws Exception {
		
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String redirectJspPage=uploadPortletRequest.getParameter("jspPage");
		actionResponse.setRenderParameter("jspPage", redirectJspPage);
		uploadPortletRequest.setAttribute("idArtefatto", null);
		User user =PortalUtil.getUser(actionRequest);

		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		Long idArtefatto = -1L;
		
		if(actionRequest.getParameter("idArtefatto")!=null){
			idArtefatto = Long.parseLong(actionRequest.getParameter("idArtefatto"));
		}
		
		_log.debug("idArtefatto: "+idArtefatto);
		if(idArtefatto == -1L){
			_log.error(ARTIFACT_NOT_EXISTS_KEY);
			throw new Exception(ARTIFACT_NOT_EXISTS_KEY); 
		}
		
		Artefatto a=ArtefattoLocalServiceUtil.getArtefatto(idArtefatto);
		System.out.println("Permanent delete: "+ConfUtil.getBoolean("conf.delete.ispermanent"));
		MyMarketplaceUtility._deleteArtifact(actionRequest, a, ConfUtil.getBoolean("conf.delete.ispermanent"));
		
		AssetEntryLocalServiceUtil.deleteEntry(Artefatto.class.getName(), idArtefatto);
		
		_log.debug("Deleted artifactId "+idArtefatto);
		return a;
	}
	
//	/**
//	 * Elimina uno dei seguenti file associati all'artefatto:  l'icona, il war/apk, e l'OWL.
//	 * @param actionRequest
//	 * @param actionResponse
//	 * @throws Exception
//	 */
//	public static void deleteAllegato(ActionRequest actionRequest, ActionResponse actionResponse) 
//			throws Exception {
//
//		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
//		String redirectJspPage=uploadPortletRequest.getParameter( "jspPage");
//		actionResponse.setRenderParameter("jspPage", redirectJspPage);
//		
//		User user =PortalUtil.getUser(actionRequest);
//		if (user==null){
//			_log.error(USER_NOT_LOGGED_IN_KEY);
//			throw new Exception(USER_NOT_LOGGED_IN_KEY);
//		}
//
//		PortletSession session = actionRequest.getPortletSession();
//		WizardUtil wub = (WizardUtil)session.getAttribute("wizardUtilBean");
//		Long idArtefatto = wub.getIdArtefatto();
//		
//		String tipoElemento= uploadPortletRequest.getParameter("tipoElemento");
//		if(tipoElemento==null){
//			throw new NullPointerException("Tipo elemento da eliminare non specificato");
//		}
//
//		Artefatto a=ArtefattoLocalServiceUtil.getArtefatto(idArtefatto);
//		long idElemento=-1;
//
//		if (tipoElemento.equalsIgnoreCase("fileWar")){
//			Artefatto newArt = MyMarketplaceUtility.deleteApplication(a);
//			wub.setArtefatto(newArt);
//			wub.setCompleteApp(false);
//
//		}else if (tipoElemento.equalsIgnoreCase("icona") ){			
//			Artefatto newArt = MyMarketplaceUtility.deleteIcon(a);
//			wub.setArtefatto(newArt);
//			wub.setIconUrl("");
//		}
//		else if (tipoElemento.equalsIgnoreCase("immagineArt") ){
//
//			idElemento=Long.parseLong(uploadPortletRequest.getParameter("idElemento"));
//			Artefatto artefatto = wub.getArtefatto();
//			
//			if(artefatto.getImgId()==idElemento){
//				MyMarketplaceUtility.deleteIcon(artefatto);
//				wub.setArtefatto(artefatto);
//				wub.setIconUrl("");
//				_log.info("Eliminata anche l'icona dell'artefatto");
//			}
//			artefatto = MyMarketplaceUtility.deleteImmagineArt(artefatto, idElemento);
//			
//			if(ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).size()==0){
//				wub.setCompleteGallery(false);
//			}
//
//			idElemento=-1;
//		}
//		
//		actionRequest.getPortletSession().setAttribute("wizardUtilBean", wub);
//
//		//Verifica 
//		if (idElemento>0){
//			DLFileEntryLocalServiceUtil.deleteDLFileEntry(idElemento);
//		}
//		
//		ArtefattoLocalServiceUtil.updateArtefatto(a);
//		
//	}

	public static void deleteAllegato_new(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws Exception {
		

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String redirectJspPage=uploadPortletRequest.getParameter( "jspPage");
		actionResponse.setRenderParameter("jspPage", redirectJspPage);
		actionResponse.setRenderParameter("catsOfType", actionRequest.getParameter("catsOfType"));
		
		
		User user =PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		
		String tipoElemento= uploadPortletRequest.getParameter("tipoElemento");
		if(tipoElemento==null){
			_log.error("Element type not specified");
			throw new NullPointerException("Element type not specified");
		}

		Artefatto a = null;
		String artIdString = uploadPortletRequest.getParameter("idArtefatto");
		actionResponse.setRenderParameter("resourcePrimKey", artIdString);
		if(artIdString!=null && !artIdString.equals("")){
			try{
				a = ArtefattoLocalServiceUtil.fetchArtefatto(Long.parseLong(artIdString));
			}
			catch(Exception e){
				_log.warn(e.getMessage());
				throw new Exception(ARTIFACT_NOT_EXISTS_KEY);
			}
		}
		else{
			_log.warn("No artefactId specified");
			throw new Exception(ARTIFACT_NOT_EXISTS_KEY);
		}

		if (tipoElemento.equalsIgnoreCase("fileWar")){
			a = MyMarketplaceUtility.deleteApplication(a);
			actionResponse.setRenderParameter("wizard-start-index", "1");
		}
		else if (tipoElemento.equalsIgnoreCase("icona") ){
			a = MyMarketplaceUtility.deleteIcon(a);
			actionResponse.setRenderParameter("wizard-start-index", "1");
		}
		else if (tipoElemento.equalsIgnoreCase("immagineArt") ){
			actionResponse.setRenderParameter("wizard-start-index", "1");
			String elementIdString = uploadPortletRequest.getParameter("idElemento");
			if(elementIdString == null || elementIdString.equals("")){
				throw new Exception("Element id not specified");
			}
			long idElemento = Long.parseLong(elementIdString);
			if(a.getImgId()==idElemento){
				_log.info("Eliminando l'icona");
				a = MyMarketplaceUtility.deleteIcon(a);
				_log.info("Eliminata anche l'icona dell'artefatto");
			}
			a = MyMarketplaceUtility.deleteImmagineArt(a, idElemento);
		}
		
	}

	public static void setAsIcon(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws Exception {
		User user =PortalUtil.getUser(actionRequest);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String redirectJspPage=uploadPortletRequest.getParameter( "jspPage");
		String artefactIdString = uploadPortletRequest.getParameter("artefactId");
		actionResponse.setRenderParameter("jspPage", redirectJspPage);
		actionResponse.setRenderParameter("resourcePrimKey", artefactIdString);
		actionResponse.setRenderParameter("wizard-start-index", "1");
		actionResponse.setRenderParameter("catsOfType", actionRequest.getParameter("catsOfType"));
		
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		if(artefactIdString==null || artefactIdString.equals("")){
			_log.error(ARTIFACT_NOT_EXISTS_KEY);
			throw new Exception(ARTIFACT_NOT_EXISTS_KEY);
		}

		Long idArtefatto = Long.parseLong(artefactIdString);
		String idElemento=uploadPortletRequest.getParameter("idElemento");

		_log.info("idArtefatto "+idArtefatto+" - idElemento "+idElemento);
 
		Artefatto a=ArtefattoLocalServiceUtil.getArtefatto(idArtefatto);
		a.setImgId( Long.parseLong(idElemento) );
		ArtefattoLocalServiceUtil.updateArtefatto(a);
		
	}

	/**
	 * Elimina tutti gli artefatti dall'elenco degli assets
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public static void clearAll(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		User user =PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}


		int end=ArtefattoLocalServiceUtil.getArtefattosCount();
		List<Artefatto> listaArtefatti = ArtefattoLocalServiceUtil.getArtefattos(0, end);
		for (int i=0;i<end;i++){ 
			MyMarketplaceUtility._deleteArtifact(actionRequest,listaArtefatti.get(i));
		}
	}

	public static void uploadRDF(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		if (!PortalUtil.isMultipartRequest(httpRequest)) {
			System.out.println("Request is not multipart, please 'multipart/form-data' enctype for your form.");
			return;
		}
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		actionResponse.setRenderParameter("jspPage", uploadPortletRequest.getParameter("jspPage"));
		String sArtefattoId = uploadPortletRequest.getParameter("artefattoId");
		boolean isUpdate = sArtefattoId !=null;
		if(isUpdate){
			actionResponse.setRenderParameter("resourcePrimKey", sArtefattoId);
			System.out.println("for artefact "+uploadPortletRequest.getParameter("artefattoId"));
		}
		
		User user =PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}

		_log.debug("Aggiorno il file RDF");
		File[] submittedFiles = uploadPortletRequest.getFiles("fileRDF"); //uploaded cercalo in /tomcat/temp
		
		File submissionFile = submittedFiles[0];
		
		Set<Long> validIds = new HashSet<Long>();
		if(actionRequest.getParameter("catsOfType").equals("psa"))
			validIds = psaIds;
		else if(actionRequest.getParameter("catsOfType").equals("bblocks"))
			validIds = bbIds;
			
		if(submissionFile!= null){
			try{
				ReaderRDF readerRDF = new ReaderRDF(submissionFile.getAbsolutePath());
				
				//General
				Artefact artefact = RDFUtils.getArtefact(readerRDF);
				if(artefact==null)
					throw new RDFException("The RDF is not well formed");
				//Business Entities
				Entity prov = RDFUtils.getProvider(readerRDF);
				
				//Artefact type
				String type = artefact.getType().getValue();
				type = removeNamespace(type);
				System.out.println(type);
				if(!type.contains(" ")){
					type = StringUtils.join( StringUtils.splitByCharacterTypeCamelCase(type), " ");
				}
				long idCategoria = MyMarketplaceUtility.getCategoryByString(type);
				
				if(!validIds.contains(idCategoria)){
					throw new RDFException("Invalid type "+type);
				}
				
				//General metadata
				String abstractDescription = artefact.getAbstractDescription().getValue();
				String title = artefact.getTitle().getValue();
				String description = artefact.getDescription().getValue();
				String[] tags = artefact.getTag().getValue();
				
				String url = "";
				String interactionPoint = "";
				String webpage = "";
				if(actionRequest.getParameter("catsOfType").equals("bblocks")){
					webpage = artefact.getPage().getValue();
					interactionPoint = ((BuildingBlock)artefact).getInteractionpoints().getValue().get(0).getUrl().getValue();
				}
				if(actionRequest.getParameter("catsOfType").equals("psa")){
					url = ((PublicServiceApplication)artefact).getUrl().getValue();
				}
				
				String providerName = "";
				if(prov!=null)
				 providerName = prov.getTitle().getValue();
	
				String nameRDF="file://"+title.replace(" ", "_")+"_"+GregorianCalendar.getInstance().getTimeInMillis(); 
	
				Crud c=new Crud();  
				try{ c.addRDFfile(submissionFile.getAbsolutePath(), nameRDF); }
				catch(Exception e){	
					_log.error("RDF_REPOSITORY_ERROR.\n"+e.getMessage());
					throw new RDFException("The RDF cannot be stored");
				}
				
				long companyId=user.getCompanyId();
				long groupId=user.getGroupId();
				long userId=user.getUserId();
				long[] orgIds = user.getOrganizationIds();
	
				Artefatto artefatto;
				String oldResRDF;
				
				if(isUpdate){
					try{ 
						artefatto = ArtefattoLocalServiceUtil.getArtefatto(Long.parseLong(sArtefattoId));
						oldResRDF = artefatto.getResourceRDF();
					}
					catch(Exception e){ 
						//Se viene erroneamente inviato un id non pi� esistente, viene creato un artefatto nuovo
						e .printStackTrace();
						artefatto = MyMarketplaceUtility.newArtifact(companyId, groupId, userId, orgIds);
						oldResRDF = null;
						isUpdate = false;
					}
				}
				else{
					artefatto = MyMarketplaceUtility.newArtifact(companyId, groupId, userId, orgIds);
					oldResRDF = null;
				}
				
				String pilot = EditorUtils.getPilotByUserId(artefatto.getUserId());
				String[] languages = new String[]{"English"};
				try{
					ExpandoColumn langColumn = ExpandoColumnLocalServiceUtil.getColumn(MyMarketplaceUtility.getDefaultCompanyId(), User.class.getName(), "CUSTOM_FIELDS", "languages");
					languages = langColumn.getDefaultData().split(",");
				}
				catch(Exception e){ e.printStackTrace(); }
				String language = languages[0];
				
				//Presi dall'RDF
				artefatto.setTitle(title);
				artefatto.setCategoriamkpId(idCategoria);
				artefatto.setWebpage(webpage);
				artefatto.setPilotid(pilot);
				artefatto.setProviderName(providerName);
				if(oldResRDF!=null && !oldResRDF.equals("")){ 
					c.clearContext(artefatto.getResourceRDF()); //Rimuove il vecchio RDF dal repository Sesame
				}
				artefatto.setLanguage(language);
				artefatto.setResourceRDF(nameRDF);
				artefatto.setAbstractDescription(abstractDescription); 
				artefatto.setDescription(description);
				artefatto.setInteractionPoint(interactionPoint);
				artefatto.setUrl(url);
				
				ArtefattoLocalServiceUtil.updateArtefatto(artefatto);
				ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
				AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(userId,
														groupId,
														Artefatto.class.getName(), 
														artefatto.getArtefattoId(), 
														serviceContext.getAssetCategoryIds(),
														tags);

				ae.setVisible(false);
				AssetEntryLocalServiceUtil.updateAssetEntry(ae);
	
				actionResponse.setRenderParameter("resourcePrimKey", String.valueOf(artefatto.getArtefattoId()));
				actionResponse.setRenderParameter("catsOfType", uploadPortletRequest.getParameter("catsOfType"));
				
				_log.info("RDF Completato");
			}
			catch(RDFException e){
				_log.error(e.getClass()+": "+e.getMessage());
				actionResponse.setRenderParameter("catsOfType", uploadPortletRequest.getParameter("catsOfType"));
				throw new Exception(INVALID_RDF_ERROR+". "+e.getMessage());
			}
		}
	} 
	
	public static void updateFile_new(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String redirectJspPage= uploadPortletRequest.getParameter( "jspPage");
		actionResponse.setRenderParameter("jspPage", redirectJspPage);
		actionResponse.setRenderParameter("catsOfType", actionRequest.getParameter("catsOfType"));
		
		
		User user =  PortalUtil.getUser(actionRequest);
		if (user==null){
			_log.error(USER_NOT_LOGGED_IN_KEY);
			throw new Exception(USER_NOT_LOGGED_IN_KEY);
		}
		
		Artefatto a = null;
		String artIdString = uploadPortletRequest.getParameter("idArtefatto");
		
		Long idArtefatto = -1L;
		if(artIdString!=null && !artIdString.equals("")){
			try{ 
				a = ArtefattoLocalServiceUtil.fetchArtefatto(Long.parseLong(artIdString));
				idArtefatto = Long.parseLong(artIdString);
			}
			catch(Exception e){
				_log.warn(e.getMessage());
				a = null;
				idArtefatto = -1L;
				throw new Exception(ARTIFACT_NOT_EXISTS_KEY);
			}
		}
		
		if(a==null){
			long companyId=user.getCompanyId();
			long groupId=user.getGroupId();
			long userId=user.getUserId();
			long[] orgIds = user.getOrganizationIds();
			a = MyMarketplaceUtility.newArtifact(companyId, groupId, userId, orgIds);
			idArtefatto = a.getArtefattoId();
			
		}
		actionResponse.setRenderParameter("resourcePrimKey", String.valueOf(idArtefatto));
		String nomeForm=uploadPortletRequest.getParameter("formName");
		if(nomeForm==null){ return; }
		
		String  nomeCampoFile="";		//il nome dei campi dei form � diverso perch� sono diversi i validatori
		if(nomeForm.equalsIgnoreCase("myForm3")){
			//war
			nomeCampoFile="fileWAR";
			actionResponse.setRenderParameter("wizard-start-index", "1");
		}else if(nomeForm.equalsIgnoreCase("myForm4")){
			//img
			nomeCampoFile="fileIMG";
			actionResponse.setRenderParameter("wizard-start-index", "1");
		}else if(nomeForm.equalsIgnoreCase("myFormIcona")){
			//icona
			nomeCampoFile="fileIco";
			actionResponse.setRenderParameter("wizard-start-index", "1");
		}

		String filename = uploadPortletRequest.getFileName(nomeCampoFile);
		System.out.println(filename);
		File file = uploadPortletRequest.getFile(nomeCampoFile); 
		String nomeDLFileEntry= uploadPortletRequest.getParameter("nomeDLFileEntry");

		_log.info("nomeForm: "+nomeForm +" nomeDLFileEntry:"+nomeDLFileEntry);
		
		InputStream inputStream = new FileInputStream(file);
		try{

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			DLFolder dlFolder;
			if(nomeCampoFile=="fileIMG"){
				//Se si sta caricando un'immagine si deve caricarla nella cartella adibita alle immagini
				//DLMarketplaceFolder->DLFOLDER_ARTIFACT_IMAGES->idArtefatto
				dlFolder = (DLFolder) MyMarketplaceUtility._getDLMarketplacepSubFolder(themeDisplay,actionRequest,DLFOLDER_ARTIFACT_IMAGES);
				dlFolder= MyMarketplaceUtility.trovaCreaDLFolder(actionRequest,
											themeDisplay.getScopeGroupId(),
											dlFolder.getFolderId(),
											idArtefatto+"", 
											"Cartella contenente le immagini dell'artefatto "+idArtefatto,
											DLFOLDER_TYPE.SUBIMAGES);
			}
			else{
				dlFolder = (DLFolder) MyMarketplaceUtility._getDLMarketplacepSubFolder(themeDisplay,actionRequest,idArtefatto+"");
			}
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),actionRequest);

			//Elimina la versione precedente. 
			//Togliendo la seguente il marketplace gestirebbe le versioni dei file applicazione
			MyMarketplaceUtility.deleteDLFileEntry(themeDisplay.getScopeGroupId(),	dlFolder.getFolderId(),	nomeDLFileEntry);
			//http://www.liferay.com/it/community/forums/-/message_boards/message/30114148
			
			if(nomeDLFileEntry != null && nomeDLFileEntry.contains("\\")){
				String[] nomeParts = nomeDLFileEntry.split(Pattern.quote("\\"));
				nomeDLFileEntry = nomeParts[nomeParts.length - 1];
			}
			
			DLFileEntry dlFileEntry=DLFileEntryLocalServiceUtil.addFileEntry(
					user.getUserId(),
					themeDisplay.getScopeGroupId(),//user.getGroupId(), 
					dlFolder.getRepositoryId(), 
					dlFolder.getFolderId(),
					filename, 
					MimeTypesUtil.getContentType(file), 
					nomeDLFileEntry,//nomeFile, 
					"descrizione del file", 
					"",
					0, 
					new HashMap<String, Fields>(),//fieldsMap
					file,
					inputStream,//fileInfo.getFile()					  
					file.length(),
					serviceContext);


			DLFileEntryLocalServiceUtil.updateFileEntry(
					user.getUserId(),
					dlFileEntry.getFileEntryId(), 
					filename, 
					MimeTypesUtil.getContentType(file),
					nomeDLFileEntry,//nomeFile, 
					"descrizione del file",  
					"",
					true, 
					dlFileEntry.getFileEntryTypeId(), 
					new HashMap<String, Fields>(),
					file,
					inputStream,
					file.length(),
					serviceContext);

			_log.info("File: "+dlFileEntry.getFileEntryId() + " updated");
			String[] publicActions_file =  {ActionKeys.VIEW};
			MyMarketplaceUtility.setPermission(actionRequest, Long.toString(dlFileEntry.getFileEntryId()), DLFileEntry.class.getName(), publicActions_file);
			
			long entryId = dlFileEntry.getFileEntryId();
			if(nomeForm.equalsIgnoreCase("myForm3")){
				//war
//				a.setWarApkDLEntryId(entryId);
				_log.info("Setting App File Entry: "+entryId);
				_log.info("App Complete");
			}
			else if (nomeForm.equalsIgnoreCase("myForm4")){

				ImmagineArt img=new ImmagineArtImpl();
				img.setImageId(	CounterLocalServiceUtil.increment(ImmagineArt.class.getName()));
				img.setDlImageId(dlFileEntry.getFileEntryId());
				img.setDescrizione("descrizione");
				img.setArtefattoIdent(idArtefatto);

				ImmagineArtLocalServiceUtil.updateImmagineArt(img);
				if(a.getImgId() == -1){
					a.setImgId(img.getImageId());
				}
				
				_log.debug("Gallery complete");

			}else if(nomeForm.equalsIgnoreCase("myFormIcona")){
				//icona
				MyMarketplaceUtility.deleteIcon(a);
				a.setImgId(entryId);
				_log.debug("Setting Icon entry id: "+entryId);
				_log.debug("Gallery complete");
			}

			ArtefattoLocalServiceUtil.updateArtefatto(a);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static String removeNamespace(String string){
		
		String regex = "^(?:";
		Field[] namespaces = Namespace.class.getDeclaredFields();
		for(Field f:namespaces){
			try{ regex += f.get(null).toString().replaceAll("\\/", "\\\\/").replaceAll("\\-", "\\\\-") + "|"; }
			catch(Exception e){ _log.warn("Error while appending a field of the class Namespace to the Regexp");  }
		}
		regex += "#)?(.*)$";
			System.out.println(regex);
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(string);
		if(m.matches()){
			return m.group(1);
		}
		
		else return string;
		
	}
}
