/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.base;

import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;

import java.util.Arrays;

/**
 * @author eng
 * @generated
 */
public class ArtifactOrganizationsLocalServiceClpInvoker {
	public ArtifactOrganizationsLocalServiceClpInvoker() {
		_methodName0 = "addArtifactOrganizations";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations"
			};

		_methodName1 = "createArtifactOrganizations";

		_methodParameterTypes1 = new String[] {
				"it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK"
			};

		_methodName2 = "deleteArtifactOrganizations";

		_methodParameterTypes2 = new String[] {
				"it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK"
			};

		_methodName3 = "deleteArtifactOrganizations";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchArtifactOrganizations";

		_methodParameterTypes10 = new String[] {
				"it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK"
			};

		_methodName11 = "getArtifactOrganizations";

		_methodParameterTypes11 = new String[] {
				"it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK"
			};

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getArtifactOrganizationses";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getArtifactOrganizationsesCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateArtifactOrganizations";

		_methodParameterTypes15 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations"
			};

		_methodName64 = "getBeanIdentifier";

		_methodParameterTypes64 = new String[] {  };

		_methodName65 = "setBeanIdentifier";

		_methodParameterTypes65 = new String[] { "java.lang.String" };

		_methodName70 = "findArtefattoByOrganizationId_Status";

		_methodParameterTypes70 = new String[] { "long", "int" };

		_methodName71 = "findArtifactOrganizationsByArtifactId";

		_methodParameterTypes71 = new String[] { "long" };

		_methodName72 = "findArtifactOrganizationsByOrganizationId";

		_methodParameterTypes72 = new String[] { "long" };

		_methodName73 = "addArtifactOrganizations";

		_methodParameterTypes73 = new String[] { "long", "long", "int" };

		_methodName74 = "deleteArtifactOrganizations";

		_methodParameterTypes74 = new String[] { "long" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations((it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.createArtifactOrganizations((it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK)arguments[0]);
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.deleteArtifactOrganizations((it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK)arguments[0]);
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.deleteArtifactOrganizations((it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.fetchArtifactOrganizations((it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK)arguments[0]);
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.getArtifactOrganizations((it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.getArtifactOrganizationses(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.getArtifactOrganizationsesCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.updateArtifactOrganizations((it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations)arguments[0]);
		}

		if (_methodName64.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes64, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName65.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes65, parameterTypes)) {
			ArtifactOrganizationsLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName70.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes70, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.findArtefattoByOrganizationId_Status(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName71.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes71, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByArtifactId(((Long)arguments[0]).longValue());
		}

		if (_methodName72.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes72, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByOrganizationId(((Long)arguments[0]).longValue());
		}

		if (_methodName73.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes73, parameterTypes)) {
			return ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName74.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes74, parameterTypes)) {
			ArtifactOrganizationsLocalServiceUtil.deleteArtifactOrganizations(((Long)arguments[0]).longValue());

			return null;
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName64;
	private String[] _methodParameterTypes64;
	private String _methodName65;
	private String[] _methodParameterTypes65;
	private String _methodName70;
	private String[] _methodParameterTypes70;
	private String _methodName71;
	private String[] _methodParameterTypes71;
	private String _methodName72;
	private String[] _methodParameterTypes72;
	private String _methodName73;
	private String[] _methodParameterTypes73;
	private String _methodName74;
	private String[] _methodParameterTypes74;
}