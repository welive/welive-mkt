/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.CategoriaModel;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the Categoria service. Represents a row in the &quot;Artefatto_Categoria&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.eng.rspa.marketplace.artefatto.model.CategoriaModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CategoriaImpl}.
 * </p>
 *
 * @author eng
 * @see CategoriaImpl
 * @see it.eng.rspa.marketplace.artefatto.model.Categoria
 * @see it.eng.rspa.marketplace.artefatto.model.CategoriaModel
 * @generated
 */
public class CategoriaModelImpl extends BaseModelImpl<Categoria>
	implements CategoriaModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a categoria model instance should use the {@link it.eng.rspa.marketplace.artefatto.model.Categoria} interface instead.
	 */
	public static final String TABLE_NAME = "Artefatto_Categoria";
	public static final Object[][] TABLE_COLUMNS = {
			{ "idCategoria", Types.BIGINT },
			{ "nomeCategoria", Types.VARCHAR },
			{ "supports", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table Artefatto_Categoria (idCategoria LONG not null primary key,nomeCategoria VARCHAR(75) null,supports VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table Artefatto_Categoria";
	public static final String ORDER_BY_JPQL = " ORDER BY categoria.nomeCategoria ASC";
	public static final String ORDER_BY_SQL = " ORDER BY Artefatto_Categoria.nomeCategoria ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.eng.rspa.marketplace.artefatto.model.Categoria"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.eng.rspa.marketplace.artefatto.model.Categoria"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.eng.rspa.marketplace.artefatto.model.Categoria"),
			true);
	public static long IDCATEGORIA_COLUMN_BITMASK = 1L;
	public static long NOMECATEGORIA_COLUMN_BITMASK = 2L;
	public static long SUPPORTS_COLUMN_BITMASK = 4L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.eng.rspa.marketplace.artefatto.model.Categoria"));

	public CategoriaModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _idCategoria;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdCategoria(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idCategoria;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return Categoria.class;
	}

	@Override
	public String getModelClassName() {
		return Categoria.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCategoria", getIdCategoria());
		attributes.put("nomeCategoria", getNomeCategoria());
		attributes.put("supports", getSupports());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idCategoria = (Long)attributes.get("idCategoria");

		if (idCategoria != null) {
			setIdCategoria(idCategoria);
		}

		String nomeCategoria = (String)attributes.get("nomeCategoria");

		if (nomeCategoria != null) {
			setNomeCategoria(nomeCategoria);
		}

		String supports = (String)attributes.get("supports");

		if (supports != null) {
			setSupports(supports);
		}
	}

	@Override
	public long getIdCategoria() {
		return _idCategoria;
	}

	@Override
	public void setIdCategoria(long idCategoria) {
		_columnBitmask |= IDCATEGORIA_COLUMN_BITMASK;

		if (!_setOriginalIdCategoria) {
			_setOriginalIdCategoria = true;

			_originalIdCategoria = _idCategoria;
		}

		_idCategoria = idCategoria;
	}

	public long getOriginalIdCategoria() {
		return _originalIdCategoria;
	}

	@Override
	public String getNomeCategoria() {
		if (_nomeCategoria == null) {
			return StringPool.BLANK;
		}
		else {
			return _nomeCategoria;
		}
	}

	@Override
	public void setNomeCategoria(String nomeCategoria) {
		_columnBitmask = -1L;

		if (_originalNomeCategoria == null) {
			_originalNomeCategoria = _nomeCategoria;
		}

		_nomeCategoria = nomeCategoria;
	}

	public String getOriginalNomeCategoria() {
		return GetterUtil.getString(_originalNomeCategoria);
	}

	@Override
	public String getSupports() {
		if (_supports == null) {
			return StringPool.BLANK;
		}
		else {
			return _supports;
		}
	}

	@Override
	public void setSupports(String supports) {
		_columnBitmask |= SUPPORTS_COLUMN_BITMASK;

		if (_originalSupports == null) {
			_originalSupports = _supports;
		}

		_supports = supports;
	}

	public String getOriginalSupports() {
		return GetterUtil.getString(_originalSupports);
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			Categoria.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public Categoria toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (Categoria)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		CategoriaImpl categoriaImpl = new CategoriaImpl();

		categoriaImpl.setIdCategoria(getIdCategoria());
		categoriaImpl.setNomeCategoria(getNomeCategoria());
		categoriaImpl.setSupports(getSupports());

		categoriaImpl.resetOriginalValues();

		return categoriaImpl;
	}

	@Override
	public int compareTo(Categoria categoria) {
		int value = 0;

		value = getNomeCategoria().compareTo(categoria.getNomeCategoria());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Categoria)) {
			return false;
		}

		Categoria categoria = (Categoria)obj;

		long primaryKey = categoria.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		CategoriaModelImpl categoriaModelImpl = this;

		categoriaModelImpl._originalIdCategoria = categoriaModelImpl._idCategoria;

		categoriaModelImpl._setOriginalIdCategoria = false;

		categoriaModelImpl._originalNomeCategoria = categoriaModelImpl._nomeCategoria;

		categoriaModelImpl._originalSupports = categoriaModelImpl._supports;

		categoriaModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<Categoria> toCacheModel() {
		CategoriaCacheModel categoriaCacheModel = new CategoriaCacheModel();

		categoriaCacheModel.idCategoria = getIdCategoria();

		categoriaCacheModel.nomeCategoria = getNomeCategoria();

		String nomeCategoria = categoriaCacheModel.nomeCategoria;

		if ((nomeCategoria != null) && (nomeCategoria.length() == 0)) {
			categoriaCacheModel.nomeCategoria = null;
		}

		categoriaCacheModel.supports = getSupports();

		String supports = categoriaCacheModel.supports;

		if ((supports != null) && (supports.length() == 0)) {
			categoriaCacheModel.supports = null;
		}

		return categoriaCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idCategoria=");
		sb.append(getIdCategoria());
		sb.append(", nomeCategoria=");
		sb.append(getNomeCategoria());
		sb.append(", supports=");
		sb.append(getSupports());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.Categoria");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idCategoria</column-name><column-value><![CDATA[");
		sb.append(getIdCategoria());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomeCategoria</column-name><column-value><![CDATA[");
		sb.append(getNomeCategoria());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>supports</column-name><column-value><![CDATA[");
		sb.append(getSupports());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Categoria.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			Categoria.class
		};
	private long _idCategoria;
	private long _originalIdCategoria;
	private boolean _setOriginalIdCategoria;
	private String _nomeCategoria;
	private String _originalNomeCategoria;
	private String _supports;
	private String _originalSupports;
	private long _columnBitmask;
	private Categoria _escapedModel;
}