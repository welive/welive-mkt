package it.eng.rspa.marketplace.category;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.impl.CategoriaImpl;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;

public class Category {
	
	public static String var; 
	/** STORAGE DI NOME, ID e FORMATO DELLE APPLICAZIONI SUPPORTATE DI UNA CATEGORIA**/
	
	private static HashMap<Long, Categoria> categoryById;
	private static HashMap<String, Categoria> categoryByName;
	static
    {
		categoryById = new HashMap<Long, Categoria>();
		categoryByName = new HashMap<String, Categoria>();
		
		Categoria tempCat = new CategoriaImpl();
				tempCat.setNomeCategoria("other");
				tempCat.setIdCategoria(0);
				tempCat.setSupports("war,apk,xml");
		categoryById.put(0L, tempCat);
		categoryByName.put(tempCat.getNomeCategoria(), tempCat);
		
		List<Categoria> catList = CategoriaLocalServiceUtil.getCategorie();
		Iterator<Categoria> it = catList.iterator();
		
		while(it.hasNext()){
			tempCat = it.next();
			
			categoryById.put(tempCat.getIdCategoria(), tempCat);
			categoryByName.put(tempCat.getNomeCategoria(), tempCat);
		}
    }
	
	private Category(){
		//Do Nothing
	}
	
	public static HashMap<Long, Categoria> getMapById(){
		return categoryById;
	}
	
	public static HashMap<String, Categoria> getMapByName(){
		return categoryByName;
	}
	
	public static List<Categoria> getCategoryList(){
		return new ArrayList<Categoria>(getMapById().values());
	}
	
	public static boolean isOther(Artefatto a){
		System.out.println("Categoria Artefatto: "+a.getCategoriamkpId());
		System.out.println("isOther: "+categoryById.containsKey(a.getCategoriamkpId()));
		return !categoryById.containsKey(a.getCategoriamkpId());
	}
	
	public static void removeHashEntry(long id){
		String name = categoryById.get(id).getNomeCategoria();
		categoryById.remove(id);
		categoryByName.remove(name);
		
		return;
	}
	
	public static void addHashEntry(Categoria c){
		categoryById.put(c.getIdCategoria(), c);
		categoryByName.put(c.getNomeCategoria(), c);
		return;
	}
	
}
