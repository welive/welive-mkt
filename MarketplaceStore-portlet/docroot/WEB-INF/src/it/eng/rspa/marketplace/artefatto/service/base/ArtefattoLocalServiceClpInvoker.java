/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.base;

import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;

import java.util.Arrays;

/**
 * @author eng
 * @generated
 */
public class ArtefattoLocalServiceClpInvoker {
	public ArtefattoLocalServiceClpInvoker() {
		_methodName0 = "addArtefatto";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName1 = "createArtefatto";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteArtefatto";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteArtefatto";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchArtefatto";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "fetchArtefattoByUuidAndCompanyId";

		_methodParameterTypes11 = new String[] { "java.lang.String", "long" };

		_methodName12 = "fetchArtefattoByUuidAndGroupId";

		_methodParameterTypes12 = new String[] { "java.lang.String", "long" };

		_methodName13 = "getArtefatto";

		_methodParameterTypes13 = new String[] { "long" };

		_methodName14 = "getPersistedModel";

		_methodParameterTypes14 = new String[] { "java.io.Serializable" };

		_methodName15 = "getArtefattoByUuidAndCompanyId";

		_methodParameterTypes15 = new String[] { "java.lang.String", "long" };

		_methodName16 = "getArtefattoByUuidAndGroupId";

		_methodParameterTypes16 = new String[] { "java.lang.String", "long" };

		_methodName17 = "getArtefattos";

		_methodParameterTypes17 = new String[] { "int", "int" };

		_methodName18 = "getArtefattosCount";

		_methodParameterTypes18 = new String[] {  };

		_methodName19 = "updateArtefatto";

		_methodParameterTypes19 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName78 = "getBeanIdentifier";

		_methodParameterTypes78 = new String[] {  };

		_methodName79 = "setBeanIdentifier";

		_methodParameterTypes79 = new String[] { "java.lang.String" };

		_methodName84 = "publishArtefatto";

		_methodParameterTypes84 = new String[] { "long" };

		_methodName85 = "addArtefatto";

		_methodParameterTypes85 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto", "long",
				"java.lang.String[][]"
			};

		_methodName86 = "addArtefatto";

		_methodParameterTypes86 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto", "long",
				"com.liferay.portal.service.ServiceContext"
			};

		_methodName87 = "permanentDeleteArtefatto";

		_methodParameterTypes87 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName88 = "deleteArtefatto";

		_methodParameterTypes88 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName89 = "deleteArtefatto";

		_methodParameterTypes89 = new String[] { "long" };

		_methodName90 = "getArtefattiByPilotAndStatus";

		_methodParameterTypes90 = new String[] { "java.lang.String", "int" };

		_methodName91 = "getArtefattiByPilotAndStatus";

		_methodParameterTypes91 = new String[] {
				"java.lang.String", "int", "java.lang.String",
				"java.lang.String"
			};

		_methodName92 = "getArtefattoByDescriptionRDF";

		_methodParameterTypes92 = new String[] { "java.lang.String" };

		_methodName93 = "getArtefattoByResourceRDF";

		_methodParameterTypes93 = new String[] { "java.lang.String" };

		_methodName94 = "getArtefattiByTitle";

		_methodParameterTypes94 = new String[] { "java.lang.String" };

		_methodName95 = "findArtefattoByStatus";

		_methodParameterTypes95 = new String[] { "int" };

		_methodName96 = "getArtefactsByPilotStatus";

		_methodParameterTypes96 = new String[] {
				"java.lang.String", "int", "int", "int"
			};

		_methodName97 = "getArtefactsByPilotStatus";

		_methodParameterTypes97 = new String[] { "java.lang.String", "int" };

		_methodName98 = "findArtefattoByStatus";

		_methodParameterTypes98 = new String[] {
				"int", "java.lang.String", "java.lang.String"
			};

		_methodName99 = "findArtefattoByUserId_Status";

		_methodParameterTypes99 = new String[] { "long", "int" };

		_methodName100 = "findArtefattoByCategory_Status";

		_methodParameterTypes100 = new String[] { "long", "int" };

		_methodName101 = "findArtefattoByPilot_Category_Status";

		_methodParameterTypes101 = new String[] {
				"java.lang.String", "long", "int"
			};

		_methodName102 = "findArtefattoByCompanyId";

		_methodParameterTypes102 = new String[] { "long" };

		_methodName103 = "findArtefattoByCompanyId";

		_methodParameterTypes103 = new String[] {
				"long", "java.lang.String", "boolean"
			};

		_methodName104 = "findArtefattoByCompanyId";

		_methodParameterTypes104 = new String[] {
				"long", "java.lang.String", "java.lang.String"
			};

		_methodName105 = "findArtefattoByCategory";

		_methodParameterTypes105 = new String[] { "long" };

		_methodName106 = "findArtefattoByCategory";

		_methodParameterTypes106 = new String[] { "long", "int", "int" };

		_methodName107 = "findArtefattiByIds";

		_methodParameterTypes107 = new String[] { "java.util.List" };

		_methodName108 = "getArtefattiByLangStatus";

		_methodParameterTypes108 = new String[] { "java.lang.String", "int" };

		_methodName109 = "getArtefattiByPilotLangStatus";

		_methodParameterTypes109 = new String[] {
				"java.lang.String", "java.lang.String", "int"
			};

		_methodName110 = "getArtefattiByLangStatus";

		_methodParameterTypes110 = new String[] {
				"java.lang.String", "int", "int", "int"
			};

		_methodName111 = "getArtefattiByPilotLangStatus";

		_methodParameterTypes111 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName112 = "getArtefattiByKeyword";

		_methodParameterTypes112 = new String[] { "java.lang.String" };

		_methodName114 = "getBuildingBlocks";

		_methodParameterTypes114 = new String[] { "java.lang.String" };

		_methodName115 = "getBuildingBlocks";

		_methodParameterTypes115 = new String[] { "java.lang.String", "int", "int" };

		_methodName116 = "getBuildingBlocksByKeyword";

		_methodParameterTypes116 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName117 = "getBuildingBlocksByLangStatus";

		_methodParameterTypes117 = new String[] { "java.lang.String", "int" };

		_methodName118 = "getBuildingBlocksByPilotLangStatus";

		_methodParameterTypes118 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName119 = "getPublicServices";

		_methodParameterTypes119 = new String[] { "java.lang.String" };

		_methodName120 = "getPublicServices";

		_methodParameterTypes120 = new String[] { "java.lang.String", "int", "int" };

		_methodName121 = "getPublicServicesByKeyword";

		_methodParameterTypes121 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName122 = "getPublicServicesByLangStatus";

		_methodParameterTypes122 = new String[] { "java.lang.String", "int" };

		_methodName123 = "getPublicServicesByPilotLangStatus";

		_methodParameterTypes123 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName124 = "getDatasets";

		_methodParameterTypes124 = new String[] { "java.lang.String" };

		_methodName125 = "getDatasets";

		_methodParameterTypes125 = new String[] { "java.lang.String", "int", "int" };

		_methodName126 = "getDatasetsByKeyword";

		_methodParameterTypes126 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName127 = "getDatasetsByLangStatus";

		_methodParameterTypes127 = new String[] { "java.lang.String", "int" };

		_methodName128 = "getDatasetsByPilotLangStatus";

		_methodParameterTypes128 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName129 = "getArtefactByExternalId";

		_methodParameterTypes129 = new String[] { "java.lang.String" };

		_methodName130 = "getTopRatedArtefactsByPilot";

		_methodParameterTypes130 = new String[] { "java.lang.String", "int" };

		_methodName131 = "getTopRatedArtefactsByLanguage";

		_methodParameterTypes131 = new String[] { "java.lang.String", "int" };

		_methodName132 = "getArtefactsByUserId";

		_methodParameterTypes132 = new String[] { "long", "int" };

		_methodName133 = "getArtefattiByOwner";

		_methodParameterTypes133 = new String[] { "java.lang.String" };

		_methodName134 = "getArtefattiByOwnerAndStatus";

		_methodParameterTypes134 = new String[] { "java.lang.String", "int" };

		_methodName135 = "forget";

		_methodParameterTypes135 = new String[] { "long", "boolean" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return ArtefattoLocalServiceUtil.addArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return ArtefattoLocalServiceUtil.createArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return ArtefattoLocalServiceUtil.deleteArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return ArtefattoLocalServiceUtil.deleteArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return ArtefattoLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return ArtefattoLocalServiceUtil.fetchArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return ArtefattoLocalServiceUtil.fetchArtefattoByUuidAndCompanyId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return ArtefattoLocalServiceUtil.fetchArtefattoByUuidAndGroupId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattoByUuidAndCompanyId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName16.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes16, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattoByUuidAndGroupId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName17.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes17, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattos(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName18.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes18, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattosCount();
		}

		if (_methodName19.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes19, parameterTypes)) {
			return ArtefattoLocalServiceUtil.updateArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0]);
		}

		if (_methodName78.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes78, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName79.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes79, parameterTypes)) {
			ArtefattoLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName84.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes84, parameterTypes)) {
			return ArtefattoLocalServiceUtil.publishArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName85.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes85, parameterTypes)) {
			return ArtefattoLocalServiceUtil.addArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0],
				((Long)arguments[1]).longValue(),
				(java.lang.String[])arguments[2]);
		}

		if (_methodName86.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes86, parameterTypes)) {
			return ArtefattoLocalServiceUtil.addArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0],
				((Long)arguments[1]).longValue(),
				(com.liferay.portal.service.ServiceContext)arguments[2]);
		}

		if (_methodName87.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes87, parameterTypes)) {
			return ArtefattoLocalServiceUtil.permanentDeleteArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0]);
		}

		if (_methodName88.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes88, parameterTypes)) {
			return ArtefattoLocalServiceUtil.deleteArtefatto((it.eng.rspa.marketplace.artefatto.model.Artefatto)arguments[0]);
		}

		if (_methodName89.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes89, parameterTypes)) {
			return ArtefattoLocalServiceUtil.deleteArtefatto(((Long)arguments[0]).longValue());
		}

		if (_methodName90.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes90, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByPilotAndStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName91.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes91, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByPilotAndStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				(java.lang.String)arguments[2], (java.lang.String)arguments[3]);
		}

		if (_methodName92.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes92, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattoByDescriptionRDF((java.lang.String)arguments[0]);
		}

		if (_methodName93.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes93, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattoByResourceRDF((java.lang.String)arguments[0]);
		}

		if (_methodName94.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes94, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByTitle((java.lang.String)arguments[0]);
		}

		if (_methodName95.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes95, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByStatus(((Integer)arguments[0]).intValue());
		}

		if (_methodName96.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes96, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefactsByPilotStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue());
		}

		if (_methodName97.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes97, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefactsByPilotStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName98.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes98, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByStatus(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1], (java.lang.String)arguments[2]);
		}

		if (_methodName99.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes99, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByUserId_Status(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName100.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes100, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCategory_Status(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName101.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes101, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByPilot_Category_Status((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName102.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes102, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCompanyId(((Long)arguments[0]).longValue());
		}

		if (_methodName103.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes103, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCompanyId(((Long)arguments[0]).longValue(),
				(java.lang.String)arguments[1],
				((Boolean)arguments[2]).booleanValue());
		}

		if (_methodName104.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCompanyId(((Long)arguments[0]).longValue(),
				(java.lang.String)arguments[1], (java.lang.String)arguments[2]);
		}

		if (_methodName105.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCategory(((Long)arguments[0]).longValue());
		}

		if (_methodName106.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes106, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattoByCategory(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName107.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes107, parameterTypes)) {
			return ArtefattoLocalServiceUtil.findArtefattiByIds((java.util.List<java.lang.Long>)arguments[0]);
		}

		if (_methodName108.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes108, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByLangStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName109.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes109, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByPilotLangStatus((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Integer)arguments[2]).intValue());
		}

		if (_methodName110.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByLangStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue());
		}

		if (_methodName111.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByPilotLangStatus((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue(),
				((Integer)arguments[4]).intValue());
		}

		if (_methodName112.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByKeyword((java.lang.String)arguments[0]);
		}

		if (_methodName114.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBuildingBlocks((java.lang.String)arguments[0]);
		}

		if (_methodName115.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes115, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBuildingBlocks((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName116.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBuildingBlocksByKeyword((java.lang.String)arguments[0],
				(java.lang.String)arguments[1]);
		}

		if (_methodName117.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBuildingBlocksByLangStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName118.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getBuildingBlocksByPilotLangStatus((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue(),
				((Integer)arguments[4]).intValue());
		}

		if (_methodName119.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes119, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPublicServices((java.lang.String)arguments[0]);
		}

		if (_methodName120.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPublicServices((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName121.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPublicServicesByKeyword((java.lang.String)arguments[0],
				(java.lang.String)arguments[1]);
		}

		if (_methodName122.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes122, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPublicServicesByLangStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName123.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes123, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getPublicServicesByPilotLangStatus((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue(),
				((Integer)arguments[4]).intValue());
		}

		if (_methodName124.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes124, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getDatasets((java.lang.String)arguments[0]);
		}

		if (_methodName125.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes125, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getDatasets((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName126.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes126, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getDatasetsByKeyword((java.lang.String)arguments[0],
				(java.lang.String)arguments[1]);
		}

		if (_methodName127.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes127, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getDatasetsByLangStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName128.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes128, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getDatasetsByPilotLangStatus((java.lang.String)arguments[0],
				(java.lang.String)arguments[1],
				((Integer)arguments[2]).intValue(),
				((Integer)arguments[3]).intValue(),
				((Integer)arguments[4]).intValue());
		}

		if (_methodName129.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes129, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefactByExternalId((java.lang.String)arguments[0]);
		}

		if (_methodName130.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes130, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getTopRatedArtefactsByPilot((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName131.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes131, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getTopRatedArtefactsByLanguage((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName132.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes132, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefactsByUserId(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName133.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes133, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByOwner((java.lang.String)arguments[0]);
		}

		if (_methodName134.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes134, parameterTypes)) {
			return ArtefattoLocalServiceUtil.getArtefattiByOwnerAndStatus((java.lang.String)arguments[0],
				((Integer)arguments[1]).intValue());
		}

		if (_methodName135.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes135, parameterTypes)) {
			return ArtefattoLocalServiceUtil.forget(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue());
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName78;
	private String[] _methodParameterTypes78;
	private String _methodName79;
	private String[] _methodParameterTypes79;
	private String _methodName84;
	private String[] _methodParameterTypes84;
	private String _methodName85;
	private String[] _methodParameterTypes85;
	private String _methodName86;
	private String[] _methodParameterTypes86;
	private String _methodName87;
	private String[] _methodParameterTypes87;
	private String _methodName88;
	private String[] _methodParameterTypes88;
	private String _methodName89;
	private String[] _methodParameterTypes89;
	private String _methodName90;
	private String[] _methodParameterTypes90;
	private String _methodName91;
	private String[] _methodParameterTypes91;
	private String _methodName92;
	private String[] _methodParameterTypes92;
	private String _methodName93;
	private String[] _methodParameterTypes93;
	private String _methodName94;
	private String[] _methodParameterTypes94;
	private String _methodName95;
	private String[] _methodParameterTypes95;
	private String _methodName96;
	private String[] _methodParameterTypes96;
	private String _methodName97;
	private String[] _methodParameterTypes97;
	private String _methodName98;
	private String[] _methodParameterTypes98;
	private String _methodName99;
	private String[] _methodParameterTypes99;
	private String _methodName100;
	private String[] _methodParameterTypes100;
	private String _methodName101;
	private String[] _methodParameterTypes101;
	private String _methodName102;
	private String[] _methodParameterTypes102;
	private String _methodName103;
	private String[] _methodParameterTypes103;
	private String _methodName104;
	private String[] _methodParameterTypes104;
	private String _methodName105;
	private String[] _methodParameterTypes105;
	private String _methodName106;
	private String[] _methodParameterTypes106;
	private String _methodName107;
	private String[] _methodParameterTypes107;
	private String _methodName108;
	private String[] _methodParameterTypes108;
	private String _methodName109;
	private String[] _methodParameterTypes109;
	private String _methodName110;
	private String[] _methodParameterTypes110;
	private String _methodName111;
	private String[] _methodParameterTypes111;
	private String _methodName112;
	private String[] _methodParameterTypes112;
	private String _methodName114;
	private String[] _methodParameterTypes114;
	private String _methodName115;
	private String[] _methodParameterTypes115;
	private String _methodName116;
	private String[] _methodParameterTypes116;
	private String _methodName117;
	private String[] _methodParameterTypes117;
	private String _methodName118;
	private String[] _methodParameterTypes118;
	private String _methodName119;
	private String[] _methodParameterTypes119;
	private String _methodName120;
	private String[] _methodParameterTypes120;
	private String _methodName121;
	private String[] _methodParameterTypes121;
	private String _methodName122;
	private String[] _methodParameterTypes122;
	private String _methodName123;
	private String[] _methodParameterTypes123;
	private String _methodName124;
	private String[] _methodParameterTypes124;
	private String _methodName125;
	private String[] _methodParameterTypes125;
	private String _methodName126;
	private String[] _methodParameterTypes126;
	private String _methodName127;
	private String[] _methodParameterTypes127;
	private String _methodName128;
	private String[] _methodParameterTypes128;
	private String _methodName129;
	private String[] _methodParameterTypes129;
	private String _methodName130;
	private String[] _methodParameterTypes130;
	private String _methodName131;
	private String[] _methodParameterTypes131;
	private String _methodName132;
	private String[] _methodParameterTypes132;
	private String _methodName133;
	private String[] _methodParameterTypes133;
	private String _methodName134;
	private String[] _methodParameterTypes134;
	private String _methodName135;
	private String[] _methodParameterTypes135;
}