package it.eng.rspa.marketplace.listeners;

import java.util.GregorianCalendar;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;
import it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryImpl;
import it.eng.rspa.marketplace.artefatto.service.PendingNotificationEntryLocalServiceUtil;
import it.eng.rspa.marketplace.listeners.model.WeLiveComponent;
import it.eng.rspa.marketplace.listeners.model.WeLiveNotificationAction;

public class DecisionEngineNotifier implements iArtefactListener {

	private static Log _log = LogFactoryUtil.getLog(DecisionEngineNotifier.class);
	
	@Override
	public void onArtefactPublish(Artefatto a) {
		try{
			PendingNotificationEntry pne = new PendingNotificationEntryImpl();
			pne.setNotificationId(CounterLocalServiceUtil.increment(PendingNotificationEntry.class.getName()));
			pne.setArtefactId(a.getArtefattoId());
			pne.setAction(WeLiveNotificationAction.ArtefactPublished.toString());
			pne.setTargetComponent(WeLiveComponent.DECISION_ENGINE.toString());
			pne.setTimestamp(GregorianCalendar.getInstance().getTimeInMillis());
			
			PendingNotificationEntryLocalServiceUtil.addPendingNotificationEntry(pne);
			
			_log.info("Storing: "+pne);
		}
		catch(Exception e){
			_log.error("Unable to add the artefact to the pending notification registry.\n"+e.getClass()+" "+e.getMessage());
		}
		
		return;
	}

	@Override
	public void onArtefactDelete(Artefatto a) {
		try{
			PendingNotificationEntry pne = new PendingNotificationEntryImpl();
			pne.setNotificationId(CounterLocalServiceUtil.increment(PendingNotificationEntry.class.getName()));
			pne.setArtefactId(a.getArtefattoId());
			pne.setAction(WeLiveNotificationAction.ArtefactRemoved.toString());
			pne.setTargetComponent(WeLiveComponent.DECISION_ENGINE.toString());
			pne.setTimestamp(GregorianCalendar.getInstance().getTimeInMillis());
			
			PendingNotificationEntryLocalServiceUtil.addPendingNotificationEntry(pne);
			
			_log.info("Storing: "+pne);
		}
		catch(Exception e){
			_log.error("Unable to add the artefact to the pending notification registry.\n"+e.getClass()+" "+e.getMessage());
		}
		
		return;
	}

	@Override
	public void onArtefactUpdate(Artefatto a) {
		try{
			PendingNotificationEntry pne = new PendingNotificationEntryImpl();
			pne.setNotificationId(CounterLocalServiceUtil.increment(PendingNotificationEntry.class.getName()));
			pne.setArtefactId(a.getArtefattoId());
			pne.setAction(WeLiveNotificationAction.ArtefactModifed.toString()); // = ArtefactPublished
			pne.setTargetComponent(WeLiveComponent.DECISION_ENGINE.toString());
			pne.setTimestamp(GregorianCalendar.getInstance().getTimeInMillis());
			
			PendingNotificationEntryLocalServiceUtil.addPendingNotificationEntry(pne);
			
			_log.info("Storing: "+pne);
		}
		catch(Exception e){
			_log.error("Unable to add the artefact to the pending notification registry.\n"+e.getClass()+" "+e.getMessage());
		}
		
		return;
	}

}
