package it.eng.rspa.marketplace.jobs.messagecomposer;

import java.lang.reflect.Type;
import java.util.Map;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public abstract class ODSMessageComposer 
					extends CommonMessageComposer{
	
	private static Log _log = LogFactoryUtil.getLog(ODSMessageComposer.class);
	
	public static JSONObject prepareODSRatingMessage(Artefatto a, String opts) {
		
		_log.debug("Preparing ODS Rating message");
		
		Gson gson = new Gson();
		Type settype = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> options = gson.fromJson(opts, settype);
		
		JSONObject body = JSONFactoryUtil.createJSONObject();
		body.put("rating", Integer.parseInt(options.get("rate")));
		
		return body;
	}
	
	public static JSONObject prepareODSRatingRemovedMessage(Artefatto a, String opts) {
		return null;
	}
	
	public static JSONObject prepareODSArtefactRemovedMessage(Artefatto a, String opts) {
		return null;
	}

}
