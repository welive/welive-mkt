package it.eng.rspa.marketplace.listeners.model;

public enum WeLiveComponent {
	LOGGING_BB("LOGGING_BB"),
	DECISION_ENGINE("DECISION_ENGINE"),
	ODS("ODS");

    private final String text;

    /**
     * @param text
     */
    private WeLiveComponent(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
