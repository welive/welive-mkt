/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.Dependencies;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Dependencies in entity cache.
 *
 * @author eng
 * @see Dependencies
 * @generated
 */
public class DependenciesCacheModel implements CacheModel<Dependencies>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{dependencyId=");
		sb.append(dependencyId);
		sb.append(", artefactId=");
		sb.append(artefactId);
		sb.append(", dependsFrom=");
		sb.append(dependsFrom);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Dependencies toEntityModel() {
		DependenciesImpl dependenciesImpl = new DependenciesImpl();

		dependenciesImpl.setDependencyId(dependencyId);
		dependenciesImpl.setArtefactId(artefactId);
		dependenciesImpl.setDependsFrom(dependsFrom);

		dependenciesImpl.resetOriginalValues();

		return dependenciesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		dependencyId = objectInput.readLong();
		artefactId = objectInput.readLong();
		dependsFrom = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(dependencyId);
		objectOutput.writeLong(artefactId);
		objectOutput.writeLong(dependsFrom);
	}

	public long dependencyId;
	public long artefactId;
	public long dependsFrom;
}