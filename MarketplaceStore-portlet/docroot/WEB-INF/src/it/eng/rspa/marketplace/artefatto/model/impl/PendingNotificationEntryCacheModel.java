/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PendingNotificationEntry in entity cache.
 *
 * @author eng
 * @see PendingNotificationEntry
 * @generated
 */
public class PendingNotificationEntryCacheModel implements CacheModel<PendingNotificationEntry>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{notificationId=");
		sb.append(notificationId);
		sb.append(", timestamp=");
		sb.append(timestamp);
		sb.append(", artefactId=");
		sb.append(artefactId);
		sb.append(", action=");
		sb.append(action);
		sb.append(", targetComponent=");
		sb.append(targetComponent);
		sb.append(", options=");
		sb.append(options);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PendingNotificationEntry toEntityModel() {
		PendingNotificationEntryImpl pendingNotificationEntryImpl = new PendingNotificationEntryImpl();

		pendingNotificationEntryImpl.setNotificationId(notificationId);
		pendingNotificationEntryImpl.setTimestamp(timestamp);
		pendingNotificationEntryImpl.setArtefactId(artefactId);

		if (action == null) {
			pendingNotificationEntryImpl.setAction(StringPool.BLANK);
		}
		else {
			pendingNotificationEntryImpl.setAction(action);
		}

		if (targetComponent == null) {
			pendingNotificationEntryImpl.setTargetComponent(StringPool.BLANK);
		}
		else {
			pendingNotificationEntryImpl.setTargetComponent(targetComponent);
		}

		if (options == null) {
			pendingNotificationEntryImpl.setOptions(StringPool.BLANK);
		}
		else {
			pendingNotificationEntryImpl.setOptions(options);
		}

		pendingNotificationEntryImpl.resetOriginalValues();

		return pendingNotificationEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		notificationId = objectInput.readLong();
		timestamp = objectInput.readLong();
		artefactId = objectInput.readLong();
		action = objectInput.readUTF();
		targetComponent = objectInput.readUTF();
		options = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(notificationId);
		objectOutput.writeLong(timestamp);
		objectOutput.writeLong(artefactId);

		if (action == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(action);
		}

		if (targetComponent == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(targetComponent);
		}

		if (options == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(options);
		}
	}

	public long notificationId;
	public long timestamp;
	public long artefactId;
	public String action;
	public String targetComponent;
	public String options;
}