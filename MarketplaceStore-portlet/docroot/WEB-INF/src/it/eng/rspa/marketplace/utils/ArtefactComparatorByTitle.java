package it.eng.rspa.marketplace.utils;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.util.Comparator;

public class ArtefactComparatorByTitle implements Comparator<Artefatto> {
		
	@Override
	public int compare(Artefatto art1, Artefatto art2) {
		
		int ret=art1.getTitle().compareTo(art2.getTitle());
		return ret;
		

	}
}
