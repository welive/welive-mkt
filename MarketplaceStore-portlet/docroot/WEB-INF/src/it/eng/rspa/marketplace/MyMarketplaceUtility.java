package it.eng.rspa.marketplace;

import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dataset;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.ServiceOffering;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.cdv.datamodel.model.Language;
import it.eng.rspa.cdv.datamodel.model.UserLanguage;
import it.eng.rspa.cdv.datamodel.service.LanguageLocalServiceUtil;
import it.eng.rspa.cdv.datamodel.service.UserLanguageLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.utils.ArtefactComparatorByDate;
import it.eng.rspa.marketplace.utils.ArtefactComparatorByLevel;
import it.eng.rspa.marketplace.utils.ArtefactComparatorByRating;
import it.eng.rspa.marketplace.utils.ArtefactComparatorByTitle;
import it.eng.rspa.marketplace.utils.MyConstants;
import it.eng.rspa.marketplace.utils.RestUtils;
import it.eng.rspa.marketplace.wizard.WizardActions.DLFOLDER_TYPE;
import it.eng.rspa.usdl.model.BusinessRole;
import it.eng.rspa.usdl.utils.EditorUtils;
import it.eng.rspa.usdl.utils.RDFUtils;
import it.eng.rspa.wizard.WizardUtils;
import it.eng.rspa.wizard.utils.ArtifactValutationUtils;
import it.eng.sesame.Crud;
import it.eng.sesame.ReaderRDF;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;
import com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public abstract class MyMarketplaceUtility {
	
	public static final String DLFOLDER_NAME_MARKETPLACE = "MarketplaceDLFolder";
	public static final String DLFOLDER_MARKETPLACE_DESCRIPTION="DLFolder Marketplace e Artefatto";
	public static final String DLFOLDER_ARTIFACT_IMAGES = "images";
	public static final String NEW_ARTIFACT_TITLE = "new";
	
	private static Log _log = LogFactoryUtil.getLog(Artefatto.class);
	
	public static HTTPBasicAuthFilter getBasicAuthenticationUser(){
		String username = ConfUtil.getString("api.user.email");
		String password = ConfUtil.getString("api.user.password");
		
		return new HTTPBasicAuthFilter(username, password);
	}
	
	public static long getDefaultCompanyId(){
		long companyId = 0;
		try{ companyId = getDefaultCompany().getCompanyId(); }
		catch(Exception e){ System.out.println(e.getClass() + " " + e.getMessage()); }
		
		return companyId;
	}
	
	public static Company getDefaultCompany(){
		Company c = null;
		try{ c = CompanyLocalServiceUtil.getCompanyByWebId("liferay.com"); }
		catch(Exception e){ System.out.println(e.getClass() + " " + e.getMessage()); }
		
		return c;
	}
	
	protected static long getCategoryByString(String string){
		Categoria cat = Category.getMapByName().get(string);
		if(cat==null){
			String c = PortletProps.get("Category."+string.toLowerCase());
			cat = Category.getMapByName().get(c);
		}
		
		if(cat!=null) return cat.getIdCategoria();
		else return -1L;
	}
	
	protected static void deleteDLFileEntry(long scopeGroupId, long folderId, String nomeDLFileEntry) throws Exception {
		try{
			DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.fetchFileEntry(scopeGroupId, folderId, nomeDLFileEntry);
			if (dlFileEntry!=null){
				//Cancella la versione attuale del file
				DLFileEntryLocalServiceUtil.deleteDLFileEntry(dlFileEntry);
				_log.debug("Cancellato dlFileEntry: "+nomeDLFileEntry);
			}else{
				_log.debug("dlFileEntry non presente "+nomeDLFileEntry);
			}
		}catch (Exception e){
			_log.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	public static Artefatto deleteIcon(Artefatto a) 
			throws Exception {

		if( a.getImgId()>0 ){
			a.setImgId(-1);
			ArtefattoLocalServiceUtil.updateArtefatto(a);
		}
		
		return a;
	}
	
	protected static DLFolder _getMarketplaceFolder(ThemeDisplay themeDisplay, PortletRequest actionRequest) 
			throws Exception{
		
		return trovaCreaDLFolder(actionRequest, 
								themeDisplay.getScopeGroupId(), 
								0L, 
								DLFOLDER_NAME_MARKETPLACE, 
								DLFOLDER_MARKETPLACE_DESCRIPTION, 
								DLFOLDER_TYPE.MAINMARKET);

	}

	public static DLFolder  _getDLMarketplacepSubFolder(ThemeDisplay themeDisplay, PortletRequest actionRequest, String nomefolder ) 
			throws Exception{

		if(nomefolder==null){ throw new NullPointerException("Invalid parameter: nomefolder"); }
		if(themeDisplay==null){ throw new NullPointerException("Invalid parameter: themeDisplay"); }
		if(actionRequest==null){ throw new NullPointerException("Invalid parameter: actionRequest"); }
		
		DLFolder  folder=_getMarketplaceFolder(themeDisplay, actionRequest);

		if(nomefolder==DLFOLDER_ARTIFACT_IMAGES){
			folder= trovaCreaDLFolder(actionRequest,themeDisplay.getScopeGroupId(), folder.getFolderId(), nomefolder, nomefolder, DLFOLDER_TYPE.MAINIMAGES);
		}else{
			folder= trovaCreaDLFolder(actionRequest,themeDisplay.getScopeGroupId(), folder.getFolderId(), nomefolder, nomefolder, DLFOLDER_TYPE.MAINARTIFACT);
		}
		return folder;
	}

	/**
	 * Recupera la DLfolder, se non esiste la crea.
	 * @param actionRequest
	 * @param scopeGroupId
	 * @param parentFolder
	 * @param nomeFolder
	 * @param descrFolder
	 * @return la folder trovata o creata
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static DLFolder trovaCreaDLFolder(PortletRequest actionRequest,
									long scopeGroupId, 
									long parentFolder, 
									String nomeFolder, 
									String descrFolder, 
									DLFOLDER_TYPE subtype ) 
			throws Exception{
		
		
		DLFolder  folder= DLFolderLocalServiceUtil.fetchFolder(scopeGroupId,parentFolder, nomeFolder);
		if (folder==null){
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(),actionRequest);

			Folder folderA=   DLAppServiceUtil.addFolder(scopeGroupId, parentFolder,  nomeFolder, descrFolder, serviceContext);
			if(subtype == DLFOLDER_TYPE.MAINIMAGES || subtype == DLFOLDER_TYPE.SUBIMAGES){				
				String[] publicPermission =  {ActionKeys.VIEW, ActionKeys.ACCESS};
				setPermission(actionRequest, Long.toString(folderA.getFolderId()), DLFolder.class.getName(), publicPermission);
			}
			folder=DLFolderLocalServiceUtil.getDLFolder(folderA.getFolderId());
			
		}

		return folder;
	}
	
	public static void setPermission(PortletRequest actionRequest, 
								String resourceId, 
								String resourceType,
								String[] actionIdWiew) 
					throws Exception{
				
				User user = (User) actionRequest.getAttribute(WebKeys.USER);
				
				String[] actionIdAllFile =  {ActionKeys.VIEW,
												ActionKeys.ADD_DISCUSSION,
												ActionKeys.DELETE,
												ActionKeys.DELETE_DISCUSSION,
												ActionKeys.PERMISSIONS,
												ActionKeys.UPDATE,
												ActionKeys.UPDATE_DISCUSSION};
				
				String[] actionIdAllFolder =  {ActionKeys.VIEW,
												ActionKeys.ACCESS,
												ActionKeys.ADD_DOCUMENT,
												ActionKeys.ADD_SHORTCUT,
												ActionKeys.ADD_SUBFOLDER,
												ActionKeys.DELETE,
												ActionKeys.PERMISSIONS,
												ActionKeys.UPDATE};
				
				String[] publicActions_file = {ActionKeys.VIEW};
				String[] publicActions_folder = {ActionKeys.VIEW, ActionKeys.ACCESS};
				
				
				List<Role> roles = RoleLocalServiceUtil.getRoles(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
				Iterator<Role> rolesIt = roles.iterator();
				
				while(rolesIt.hasNext()){
					
					Role role = rolesIt.next();	
					String[] perm;
					if(!role.getName().equals("Owner")){ //Se l'utente non � il proprietario
						if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
							perm = Arrays.copyOf(publicActions_file, publicActions_file.length);
						}
						else if(resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){
							perm = Arrays.copyOf(publicActions_folder, publicActions_folder.length);
						}
						else{
							throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Tipo di risorsa non valida");
						}
						
					}
					else{ //Se l'utente � il proprietario
						if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
							perm = Arrays.copyOf(actionIdAllFile, actionIdAllFile.length);
						}else if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){	
							perm = Arrays.copyOf(actionIdAllFolder, actionIdAllFolder.length);
						}
						else{
							throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Tipo di risorsa non valida");
						}
					}
					try{
						ResourcePermissionServiceUtil.setIndividualResourcePermissions(
								ParamUtil.getLong(actionRequest, "groupid"), //groupId
								user.getCompanyId(), //companyId
								resourceType, //name
								resourceId, //primKey
								role.getPrimaryKey(), //roleId
								perm);
					}
					catch(Exception e){ throw new Exception("Impossibile assegnare i permessi alla risorsa "+resourceId+". Permesso negato."); }
				}
	}
	
	public static DLFileEntry storeDLFile(UploadPortletRequest request, File file, DLFolder dlFolder) 
			throws Exception{
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),request);
		
		User user = themeDisplay.getUser();
		InputStream inputStream = new FileInputStream(file);
		
		DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(
				user.getUserId(),
				themeDisplay.getScopeGroupId(),//user.getGroupId(), 
				dlFolder.getRepositoryId(), 
				dlFolder.getFolderId(),
				file.getName(), 
				MimeTypesUtil.getContentType(file), 
				file.getName(), 
				"descrizione del file", 
				"",
				0, 
				new HashMap<String, Fields>(),//fieldsMap
				file,
				inputStream,//fileInfo.getFile()					  
				file.length(),
				serviceContext);

		DLFileEntryLocalServiceUtil.updateFileEntry(
				user.getUserId(),
				dlFileEntry.getFileEntryId(), 
				file.getName(), 
				MimeTypesUtil.getContentType(file),
				file.getName(), 
				"descrizione del file",  
				"",
				true, 
				dlFileEntry.getFileEntryTypeId(), 
				new HashMap<String, Fields>(),
				file,
				inputStream,
				file.length(),
				serviceContext);
		
		String[] publicActions_file =  {ActionKeys.VIEW};
		WizardUtils.setPermission(user, Long.toString(dlFileEntry.getFileEntryId()), DLFileEntry.class.getName(), publicActions_file);
		
		return dlFileEntry;
	}
	
	public static Artefatto newArtifact(long companyId, long groupId, long userId, long[] orgIds ) 
			throws SystemException {
			
		Artefatto artefatto= new ArtefattoImpl();
			
		long artifactId = CounterLocalServiceUtil.increment();
			 artefatto.setArtefattoId(artifactId);
		
		artefatto.setTitle(NEW_ARTIFACT_TITLE);
		artefatto.setRepositoryRDF(Crud.getRepositoryURL());
		artefatto.setCompanyId(companyId);
		artefatto.setGroupId(groupId); 
		artefatto.setUserId(userId);
		artefatto.setDate(GregorianCalendar.getInstance().getTime());
		
		int idImgIconDefault=-1;
		artefatto.setImgId(idImgIconDefault);
		
//		artefatto.setWarApkDLEntryId(-1);
		artefatto.setStatus(WorkflowConstants.STATUS_DRAFT);
		
		ArtefattoLocalServiceUtil.addArtefatto(artefatto);
		
		ArtifactOrganizations ao = new ArtifactOrganizationsImpl();
		for(int i=0; i<orgIds.length; i++){
			ao.setArtifactId(artefatto.getArtefattoId());
			ao.setOrganizationId(orgIds[i]);
			ao.setStatus(WorkflowConstants.STATUS_DRAFT);
			ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations(ao);
		}
		
		return artefatto;
	}
	
	protected static void _deleteArtifact(ActionRequest actionRequest, Artefatto a) 
			throws Exception{
		_deleteArtifact(actionRequest, a, ConfUtil.getBoolean("conf.delete.ispermanent"));
	}
	
	protected static void _deleteArtifact(ActionRequest actionRequest, Artefatto a, boolean permanent) 
			throws Exception  {

		//Elimina lista ImmagineArt
		List <ImmagineArt>listaCoppie=new ArrayList<ImmagineArt>();
		try{
			listaCoppie=ImmagineArtLocalServiceUtil.findByArtefattoId(a.getArtefattoId());

			for (int i=0;i<listaCoppie.size();i++){
				ImmagineArtLocalServiceUtil.deleteImmagineArt(listaCoppie.get(i).getImageId());
			}

		}catch(Exception e){	
			_log.error("Impossibile eliminare le immagini dell'artefatto "+a.getArtefattoId());
			throw new Exception("Impossibile eliminare le immagini dell'artefatto "+a.getArtefattoId());  
		}

		//Elimina cartella DL
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		DLFolder dlFolder;
		try {
			//il nome della dlfolder � l'idArtifact
			dlFolder = (DLFolder) MyMarketplaceUtility._getDLMarketplacepSubFolder(themeDisplay,actionRequest,a.getArtefattoId()+"");
			DLAppServiceUtil.deleteFolder(dlFolder.getFolderId());
			
			//Elimina anche la cartella delle immagini
			dlFolder = (DLFolder) MyMarketplaceUtility._getDLMarketplacepSubFolder(themeDisplay,actionRequest,DLFOLDER_ARTIFACT_IMAGES);
			dlFolder = (DLFolder) MyMarketplaceUtility.trovaCreaDLFolder(actionRequest,
													themeDisplay.getScopeGroupId(), 
													dlFolder.getFolderId(), 
													a.getArtefattoId()+"", 
													"",
													DLFOLDER_TYPE.SUBIMAGES);
			DLAppServiceUtil.deleteFolder(dlFolder.getFolderId());
			
		} catch (Exception e) {
			_log.error("Impossibile eliminare le DLFolder dell'artefatto "+a.getArtefattoId());
			throw new Exception("Impossibile eliminare le DLFolder dell'artefatto "+a.getArtefattoId());
		}

		//Delete dependencies		
		List<Dependencies> depen = DependenciesLocalServiceUtil.getDependenciesByDependsFrom(a.getArtefattoId());
		for (Dependencies dep:depen){
			DependenciesLocalServiceUtil.deleteDependencies(dep);
		}
		
		//Delete idea association by OIANotifier.java
		
		ArtefattoLocalServiceUtil.deleteArtefatto(a);
		
		if(permanent || a.getResourceRDF().equals("")){
			AssetEntryLocalServiceUtil.deleteEntry(Artefatto.class.getName(), a.getArtefattoId());
			ArtifactOrganizationsLocalServiceUtil.deleteArtifactOrganizations(a.getArtefattoId());
		}
		else{
			try{ 
				Iterator<ArtifactOrganizations> it = ArtifactOrganizationsLocalServiceUtil.findArtifactOrganizationsByArtifactId(a.getArtefattoId()).iterator();
				while(it.hasNext()){
					try{
						ArtifactOrganizations ao = it.next();
						ao.setStatus(WorkflowConstants.STATUS_IN_TRASH);
						ArtifactOrganizationsLocalServiceUtil.updateArtifactOrganizations(ao);
					}
					catch(Exception e){
						_log.error(e.getMessage());
					}
				}
			}
			catch(Exception e){
				_log.error("Unable to set any ArtifactOrganizations IN_TRASH");
			}
			
		}

	}

	protected static Artefatto deleteApplication(Artefatto a) throws Exception{
		_log.info("Deletinf app");
//		DLFileEntryLocalServiceUtil.deleteDLFileEntry(a.getWarApkDLEntryId());
//		a.setWarApkDLEntryId(-1);
		
		ArtefattoLocalServiceUtil.updateArtefatto(a);
		_log.info("Deletion completed");
		return a;
		
	}

	protected static Artefatto deleteImmagineArt(Artefatto artefatto, long idElemento) 
			throws Exception{
		
		ImmagineArt imm=ImmagineArtLocalServiceUtil.getImmagineArt(idElemento);
		DLFileEntryLocalServiceUtil.deleteDLFileEntry(imm.getDlImageId());
		ImmagineArtLocalServiceUtil.deleteImmagineArt(idElemento);
		return artefatto;
		
	}
	
//	protected static String addGadgetContainer(ResourceRequest rReq) throws PortalException, SystemException {
//		long plid = PortalUtil.getPlidFromFriendlyURL(PortalUtil.getCompanyId(rReq), "/user/"+PortalUtil.getUser(rReq).getScreenName()+"/"+PortletProps.get("OpenSocialInstall.myPageName"));
//		Layout layout = LayoutLocalServiceUtil.getLayout(plid);
//		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
//		
//		String newPortletId = layoutTypePortlet.addPortletId(PortalUtil.getUserId(rReq), "3_WAR_opensocialportlet");
//		LayoutLocalServiceUtil.updateLayout(layout);
//		
//		return newPortletId;
//		
//	}
	
	private static ClientResponse invoke(String url){
		
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		
		return resp;
	}
	
	private static List<Artefatto> invokeByArtCategory(String url){

		List<Artefatto> out = new ArrayList<Artefatto>();
		
		ClientResponse resp = invoke(url);
		JSONObject ids = JSONFactoryUtil.createJSONObject();
				
		try {
			String s = resp.getEntity(String.class);
			if(s==null || s.equals("") || !s.startsWith("{")){
				_log.warn("Invalid Json Response: "+s);
				return out;
			}
			ids = JSONFactoryUtil.createJSONObject(s);

			if(ids.length()==0) return out;
			
			Iterator<String> keys = ids.keys();
			while(keys.hasNext()){
				try{
					String key = keys.next();
					JSONArray arts = ids.getJSONArray(key);
					
					for(int i=0; i<arts.length(); i++){
						try{
							long artId = arts.getLong(i);
							Artefatto x = ArtefattoLocalServiceUtil.getArtefatto(artId);
							out.add(x);
						}
						catch(Exception e){ _log.warn(e.getClass()+": "+e.getMessage()); }
					}
				}
				catch(Exception e){ _log.warn(e.getClass()+": "+e.getMessage()); }
			}
			
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return out;
	}
	
	private static List<Artefatto> invokeByCategory(String url){
		System.out.println("Invoking: "+url);
		List<Artefatto> out = new ArrayList<Artefatto>();
		JSONArray ids = JSONFactoryUtil.createJSONArray();
		
		try {
			try{ 
				ClientResponse resp = invoke(url);
				String s = resp.getEntity(String.class);
				if(s==null || s.equals("") || !s.startsWith("[")){
					_log.warn("Invalid Json Response: "+s);
					return out;
				}
				ids = JSONFactoryUtil.createJSONArray(s);
			}
			catch(Exception e){/* */}
			
			if(ids.length()==0) return out;
			
			for(int i=0; i<ids.length(); i++){
				long artId = ids.getLong(i);
				
				try{
					Artefatto x = ArtefattoLocalServiceUtil.getArtefatto(artId);
					out.add(x);
				}
				catch(Exception e){ /* Do nothing, just skip this artefact */ }
			}
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return out;
	}
	
	public static List<Artefatto> getSuggestedArtefacts(long userId, Set<String> typ, double lat, double lng, double radius) throws Exception{
		List<Artefatto> out = new ArrayList<Artefatto>();
		Set<String> categories = new HashSet<String>();
		for(String t:typ){
			categories.add(PortletProps.get("Category.DEMapping."+t.replace(" ", ""))+"s");
		}
		
		String queryString = "";
		if(lat!=-1 && lng!=-1 && radius>0){
			queryString = "?lat="+lat+"&lon="+lng+"&radius="+radius;
		}
		
		for(String cat : categories){
			try{ out.addAll(invokeByCategory(ConfUtil.getString("de.url")+"user/"+userId+"/"+cat+queryString)); }
			catch(Exception e){ e.printStackTrace(); }
		}

		return out;
	}
	
	public static List<Artefatto> getSuggestedArtefacts(long currArtId) throws Exception{
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		if(ConfUtil.getBoolean("de.enabled")){
			Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(currArtId);
			long catId = a.getCategoriamkpId();
			String t = "AndroidApplication";
			try { t = CategoriaLocalServiceUtil.getCategoria(catId).getNomeCategoria().replaceAll(" ", "");} 
			catch (Exception e) { t = "BuildingBlock"; }
			String type = PortletProps.get("Category.DEMapping."+t);
			
			String baseUrl = ConfUtil.getString("de.url")+type+"/"+currArtId+"/recommend/artifacts";
			
			out.addAll(invokeByArtCategory(baseUrl));
		}
		
		if(out!=null && !out.isEmpty()){
			int last = Math.min(5, out.size());
			return out.subList(0, last);
		}
		else
			return new ArrayList<Artefatto>();
	}
	
	/**
	 * @param themeDisplay
	 * @return
	 */
	public static String getDocumentLibraryBaseUrl(ThemeDisplay themeDisplay){
		return themeDisplay.getPortalURL() + themeDisplay.getPathContext() 
					+ StringPool.SLASH + "documents" + StringPool.SLASH 
					+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
	}
	
	
	/**
	 * @param artifactId
	 * @return
	 */
	public static JSONArray getArtefactImagesDetails(long artifactId, ThemeDisplay themeDisplay){
		
		List<ImmagineArt> imgs = new ArrayList<ImmagineArt>();
		try {
			imgs = ImmagineArtLocalServiceUtil.findByArtefattoId(artifactId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		
		JSONArray arResp = JSONFactoryUtil.createJSONArray();
		
		for (ImmagineArt img : imgs){

			DLFileEntry dlFileEntry = null;
			try {
				dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(img.getDlImageId());
			} catch (PortalException | SystemException e) {
				e.printStackTrace();
			} 
			String imgPath = getDocumentLibraryBaseUrl(themeDisplay) + dlFileEntry.getUuid();
			
			JSONObject resp = JSONFactoryUtil.createJSONObject();
			resp.put("artefactid", artifactId);
			resp.put("itemid", dlFileEntry.getFileEntryId());
			resp.put("itemurl", imgPath);
			
			arResp.put(resp);
			
		}	
		
		return arResp;
		
	}
	
	
	
	
	public static String getArtefactImage(Artefatto a, String pathDocumentLibrary, String contextPath, String catname){
		
		ImmagineArt imgart = null;
		String imgUrl = "";
		try {
			if(a.getImgId() == -1){
				
				List<ImmagineArt> imgsart = ImmagineArtLocalServiceUtil.getService().findByArtefattoId(a.getArtefattoId());
				if(!imgsart.isEmpty()){
					imgart = imgsart.get(0);
				}
				
			}
			else{ imgart = ImmagineArtLocalServiceUtil.getImmagineArt(a.getImgId()); }
			
			
			if(Validator.isNotNull(imgart)){
				long dlid = imgart.getDlImageId();
				imgUrl = pathDocumentLibrary + DLFileEntryLocalServiceUtil.getDLFileEntry(dlid).getUuid();
			}
			else{
				throw new Exception("No image specified for the artefact");
			}
			
		}
		catch(Exception e){
			
			imgUrl = contextPath+"/icons/";
			
			if(catname.equalsIgnoreCase(PortletProps.get("Category.rest")))
				imgUrl += "icone_servizi_rest-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.soap")))
				imgUrl += "icone_servizi_soap-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.dataset")))
				imgUrl += "icone_dataset-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appmobile")))
				imgUrl += "icone_app_android-01.png";
			else if(catname.equalsIgnoreCase(PortletProps.get("Category.appweb")))
				imgUrl += "icone_app_web-01.png";
			else
				imgUrl += "defaultIcon.png";
			
		}	
		
		return imgUrl;
	}

	/**
	 * @param a
	 * @return
	 */
	public static String getTypeByArtifact (Artefatto a){
		
		String catname = "Other";
		try{ catname = CategoriaLocalServiceUtil.getCategoria(a.getCategoriamkpId()).getNomeCategoria(); }
		catch(Exception e){}
		
		return catname;
		
	}
	
	/**
	 * @param a
	 * @return
	 */
	public static int getLevelByArtifact (Artefatto a){
		
		//Recupero il livello dell'artefatto, se non esiste posto il livello a zero
		ArtifactLevels aLevel=null;
		try {
			aLevel = ArtifactLevelsLocalServiceUtil.fetchArtifactLevels(a.getArtefattoId());
		} catch (SystemException e2) {
			e2.printStackTrace();
		}
		int level = 0;	
				
		if (Validator.isNotNull(aLevel))
			level = aLevel.getLevel();
		
		return level;
		
	}
	
	/**
	 * @param a
	 * @return
	 */
	public static String getFriendLyUrlByArtifact (Artefatto a){
		
		String frUrl = "/web/guest/marketplace/-/marketplace/view/"+a.getArtefattoId();
		return frUrl;
	}
	
	
	
	/**
	 * @param pathDocumentLibrary
	 * @param contextPath
	 * @param a
	 * @param rowURL
	 * @param cachedRating
	 * @return
	 */
	public static JSONObject artefactToJSON(String pathDocumentLibrary, String contextPath, Artefatto a, PortletURL rowURL, boolean cachedRating){
		JSONObject out = JSONFactoryUtil.createJSONObject();

		//Recupero la categoria dell'artefatto
		String catname = "Other";
		try{ catname = CategoriaLocalServiceUtil.getCategoria(a.getCategoriamkpId()).getNomeCategoria(); }
		catch(Exception e){}
		finally{
			try{ out.put("type", catname); }
			catch(Exception e){}
		}
		
		//Recupero l'immagine dell'artefatto
		String imgUrl = getArtefactImage(a, pathDocumentLibrary, contextPath, catname);
		
		try{ out.put("imgUrl", imgUrl); }
		catch(Exception e){ e.printStackTrace(); }
		
		//Recupero il titolo dell'artefatto
		try{ out.put("title", a.getTitle()); }
		catch(Exception e){
			try { out.put("title", ""); } 
			catch (Exception e1) { e1.printStackTrace(); } 
		}
		
		//Recupero il provider dell'artefatto
		try{ out.put("id", a.getArtefattoId()); }
		catch(Exception e){
			try { out.put("id", ""); } 
			catch (Exception e1) { e1.printStackTrace(); } 
		}
		
		//Recupero l'url della pagina dettaglio artefatto
		try{
			rowURL.setParameter("resourcePrimKey", String.valueOf(a.getArtefattoId()));
			out.put("url", rowURL.toString());
		}
		catch(Exception e){
			try { out.put("url", ""); } 
			catch (Exception e1) { e1.printStackTrace(); } 
		}
		
		//Recupero il livello dell'artefatto, se non esiste posto il livello a zero
		int level = getLevelByArtifact(a);
		out.put("level", level);			
		
		
		//Recupero la votazione dell'artefatto
		try{
			
			out.put("cached-rating", cachedRating);
			int avScore = 0;
			if(catname.equalsIgnoreCase("dataset")){
				try{
					if(!cachedRating){
						String sResp = RestUtils.consumeGet(ConfUtil.getString("ods.url")+a.getEId());
						JSONObject jResp = JSONFactoryUtil.createJSONObject(sResp);
						if(jResp.has("result")){
							JSONObject result = jResp.getJSONObject("result");
							if(result.has("ratings")){
								Object ratings = result.getDouble("ratings");
								
								if(ratings!=null) avScore = (int) (Math.round((Double)ratings));
								else avScore = 0;
								
							}
						}
						
						a.setExtRating(avScore);
						ArtefattoLocalServiceUtil.updateArtefatto(a);
					}
					else throw new ClientHandlerException("Cache requested");
				}
				catch(ClientHandlerException che){
					System.out.println("Open Data Stack not reachable. Showing cached rating for dataset "+a.getArtefattoId());
					avScore = a.getExtRating();
					out.put("cached-rating", true);
				}
				catch(Exception e){ System.out.println(e.getClass()+"["+e.getMessage()+"]"); }
			}
			else{
				avScore = (int)Math.round(RatingsStatsLocalServiceUtil.getStats(Artefatto.class.getName(), a.getArtefattoId()).getAverageScore());
			}
			out.put("score", avScore);
		}
		catch(Exception e){
			try { out.put("score", 0); } 
			catch (Exception e1) { e1.printStackTrace(); }
		}
		
		return out;
	}
	
	private static List<Artefatto> getArtefactList(JSONObject types, String pilot){
		
		List<Artefatto> l = new ArrayList<Artefatto>();
		try {
			if(types.getBoolean("bblocks")){ 
				l.addAll(ArtefattoLocalServiceUtil.getBuildingBlocks(pilot)); 
			}
			if(types.getBoolean("dataset")){ 
				l.addAll(ArtefattoLocalServiceUtil.getDatasets(pilot)); 
			}
			if(types.getBoolean("psa")){ 
				l.addAll(ArtefattoLocalServiceUtil.getPublicServices(pilot)); 
			}
			
//			if(types.getBoolean("all")){ l = ArtefattoLocalServiceUtil.getArtefactsByPilotStatus(pilot, WorkflowConstants.STATUS_APPROVED);  }
		}
		catch (Exception e) {
			/* Do Nothing */
		}
		
		//Sort Artefacts by Date
		/*
		Collections.sort(l, new Comparator<Artefatto>() {
			@Override
			public int compare(Artefatto o1, Artefatto o2) {
				return o2.getDate().compareTo(o1.getDate());
			}
	    });
		*/
		return l;

	}
	
	@SuppressWarnings("unchecked")
	public static List<Artefatto> getArtefactListByIdList (List<Long> idList, Criterion criteria) {
		Criterion c = null;
		List<Artefatto> artefacts = new ArrayList<Artefatto>();
		if(idList.size()==0) return artefacts; //if the idList is empty

		//c = RestrictionsFactoryUtil.in(
				c = PropertyFactoryUtil.forName("artefattoId").in(idList);
		
		//If a previous criteria exists
		if(criteria!=null) {
			c = RestrictionsFactoryUtil.and(c, criteria); //Chain
		}	
		try {	
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(ArtefattoImpl.class).add(c);
			artefacts = ArtefattoLocalServiceUtil.dynamicQuery(query);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return artefacts;
	}
	
	@SuppressWarnings("unchecked")
	private static List<Artefatto> getArtefactListByUserLangs(JSONObject types, String[] langs){
		
		Criterion c_type = null;
		if(types!=null){
			
			Set<String> catset = new HashSet<String>();
			
			try{
				if(types.getBoolean("bblocks")){ 
					String [] bbcats = PortletProps.get("Category.catsOfType.bblocks").split(",");
					for(String bbcat : bbcats){
						catset.add(bbcat);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				if(types.getBoolean("dataset")){ 
					String [] dscats = PortletProps.get("Category.catsOfType.ds").split(",");
					for(String dscat : dscats){
						catset.add(dscat);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				if(types.getBoolean("psa")){ 
					String [] psacats = PortletProps.get("Category.catsOfType.psa").split(",");
					for(String psacat : psacats){
						catset.add(psacat);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			
			Set<Long> bblockCatsIds = new HashSet<Long>();
			for(String c:catset){
				bblockCatsIds.add(Category.getMapByName().get(c).getIdCategoria());
			}
			c_type = RestrictionsFactoryUtil.in("categoriamkpId", bblockCatsIds);
		}
		
		//Imposto lo status la lingua
		Criterion c_tot_lang = RestrictionsFactoryUtil.ilike("language", "%english%");
		for(String l : langs){
			Criterion c_lang = RestrictionsFactoryUtil.ilike("language", "%"+l+"%");
			c_tot_lang = RestrictionsFactoryUtil.or(c_tot_lang, c_lang);
		}
		Criterion c_status = RestrictionsFactoryUtil.eq("status", 0);
		Criterion c_common = RestrictionsFactoryUtil.and(c_tot_lang, c_status);
		
		Criterion c_total = c_common;
		if(c_type != null){ c_total = RestrictionsFactoryUtil.and(c_type, c_common); }
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(Artefatto.class).add(c_total);
		
		List<Artefatto> l = new ArrayList<Artefatto>();
		try { l = ArtefattoLocalServiceUtil.dynamicQuery(query); } 
		catch (SystemException e) { e.printStackTrace(); }
		
		return l;

	}
	
	private static JSONObject generateResponse(ResourceRequest rReq, ResourceResponse rResp, List<Artefatto> l, int endId){
		
		ThemeDisplay themeDisplay = (ThemeDisplay)rReq.getAttribute(WebKeys.THEME_DISPLAY);
		String pathDocumentLibrary = themeDisplay.getPortalURL()
				+ themeDisplay.getPathContext() + "/documents/"
				+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
		
		PortletURL rowURL = rResp.createRenderURL();
		rowURL.setParameter("jspPage", "/html/marketplacestore/view_artefatto.jsp");
		
		JSONObject container = JSONFactoryUtil.createJSONObject();
		JSONObject outer = JSONFactoryUtil.createJSONObject();
		JSONArray arr = JSONFactoryUtil.createJSONArray();
		
		if(l.isEmpty()){
			try{
				container.put("finished", true);
				container.put("artefacts", outer);
			
				return container;
			}
			catch(Exception e){ return  JSONFactoryUtil.createJSONObject(); }
		}
		
		boolean cachedrating = !ConfUtil.getBoolean("ods.enabled");
		for(Artefatto a:l){
			JSONObject out = artefactToJSON(pathDocumentLibrary, rReq.getContextPath(), a, rowURL, cachedrating);
			
			//accodo l'artefatto corrente
			arr.put(out);
			try { cachedrating = out.getBoolean("cached-rating"); } 
			catch (Exception e) { /*Do nothing. Leave cachedrating as is*/ }
		}
		
		//Posiziono l'insieme di artefatti nella risposta
		try { 
			container.put("empty", false);
			container.put("artefacts", arr);
			container.put("finished", false);
			container.put("next_index", endId);
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return container;
	}
	
	private static Criterion getLanguagesCriteria(ResourceRequest rReq) {
		Criterion c_language = null;
		User currentUser = null;
		try { 
			currentUser = PortalUtil.getUser(rReq);
			if(currentUser!=null && !currentUser.isDefaultUser()) {
				String[] langs = new String[]{"English"};
				Set<String> sLangs = new HashSet<String>();
				try{
					List<UserLanguage> ulangs = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(currentUser.getUserId());
					for(UserLanguage ul : ulangs){
						try{ 
							Language lang = LanguageLocalServiceUtil.getLanguage(ul.getLanguageId());
							sLangs.add(lang.getName());
							System.out.println("\t"+lang.getName());
						}
						catch(Exception e){
							//skip this lang
							System.out.println(e.getMessage());
						}
					}
					
					langs = sLangs.toArray(new String[sLangs.size()]);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
					langs = (String[]) currentUser.getExpandoBridge().getAttribute("languages");
				}
				if(langs==null || langs.length==0){
					langs = new String[]{"English"};
				}
				
				Criterion c_tot_lang = RestrictionsFactoryUtil.ilike("language", "%english%");
				c_tot_lang = RestrictionsFactoryUtil.or(c_tot_lang, RestrictionsFactoryUtil.eq("language", ""));
				for(String l : langs){
					Criterion c_lang = RestrictionsFactoryUtil.ilike("language", "%"+l+"%");
					c_tot_lang = RestrictionsFactoryUtil.or(c_tot_lang, c_lang);
				}
				Criterion c_status = RestrictionsFactoryUtil.eq("status", 0);
				Criterion c_common = RestrictionsFactoryUtil.and(c_tot_lang, c_status);
				
				Criterion c_total = c_common;
				//c_language = PropertyFactoryUtil.forName("language").in(langs);
				
				return c_total;
			}
			
		} 
		catch (Exception e) {
			System.out.println("Error while getting artefacts by languages: "+e.getClass()+": "+e.getMessage());
			e.printStackTrace();
		}
		
		System.out.println("LINGUE: "+ c_language);
		return c_language;
	}
	
	private static Criterion getCategoryCriteria(JSONObject types) {
		Criterion c_ds = null;
		Criterion c_psa = null;
		Criterion c_types = null;
		try {
			if(types.getBoolean("bblocks")){
				c_types = RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("SOAP Web Service").getIdCategoria());
				c_types = RestrictionsFactoryUtil.or(c_types, RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("RESTful Web Service").getIdCategoria()));
				//c_types = RestrictionsFactoryUtil.or(c_types, RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("Other").getIdCategoria()));
				c_types = RestrictionsFactoryUtil.or(c_types, RestrictionsFactoryUtil.eq("categoriamkpId", Long.parseLong("-1") ));
			}
			if(types.getBoolean("dataset")){
				c_ds = RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("Dataset").getIdCategoria());
				c_ds = RestrictionsFactoryUtil.and(c_ds,RestrictionsFactoryUtil.eq("isPrivate", false));
				if(c_types == null) c_types = c_ds;
				else c_types = RestrictionsFactoryUtil.or(c_types, c_ds);
			}
			if(types.getBoolean("psa")){ 				
				c_psa = RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("Web Application").getIdCategoria());
				c_psa = RestrictionsFactoryUtil.or(c_psa, RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get("Android Application").getIdCategoria()));
				if(c_types == null) c_types = c_psa;
				else c_types = RestrictionsFactoryUtil.or(c_types, c_psa);
			}	
			
			if(c_types!=null) c_types = RestrictionsFactoryUtil.and(c_types, RestrictionsFactoryUtil.eq("status", WorkflowConstants.STATUS_APPROVED));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return c_types;
	}
	
	private static Criterion getPilotsCriteria(JSONObject pilots) {
		Criterion c_pilots = null;
		Criterion c_Uusimaa = null;
		Criterion c_Novisad = null;
		Criterion c_Trento = null;
		Criterion c_ALL = null;
		try {
			boolean isBilbao = pilots.getBoolean("Bilbao");
			boolean isUusimaa = pilots.getBoolean("Uusimaa");
			boolean isNovisad = pilots.getBoolean("Novisad");
			boolean isTrento = pilots.getBoolean("Trento");
			
			if (isBilbao){
				c_pilots = RestrictionsFactoryUtil.eq("pilotid", "Bilbao");
			}
			
			if (isUusimaa){
				c_Uusimaa = RestrictionsFactoryUtil.eq("pilotid", "Uusimaa");
				
				if(c_pilots == null) c_pilots = c_Uusimaa;
				else c_pilots = RestrictionsFactoryUtil.or(c_pilots, c_Uusimaa);
				
			}
			if (isNovisad){
				c_Novisad = RestrictionsFactoryUtil.eq("pilotid", "Novisad");
				
				if(c_pilots == null) c_pilots = c_Novisad;
				else c_pilots = RestrictionsFactoryUtil.or(c_pilots, c_Novisad);
			}
			if (isTrento){
				c_Trento = RestrictionsFactoryUtil.eq("pilotid", "Trento");
				
				if(c_pilots == null) c_pilots = c_Trento;
				else c_pilots = RestrictionsFactoryUtil.or(c_pilots, c_Trento);
			}
			
			//Cross pilot
			c_ALL = RestrictionsFactoryUtil.eq("pilotid", "ALL");
			
			if(c_pilots == null) c_pilots = c_ALL;
			else c_pilots = RestrictionsFactoryUtil.or(c_pilots, c_ALL);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return c_pilots;
	}
	
	
	
	private static Criterion chainCriteria(List<Criterion> criteria) {
		Criterion all = RestrictionsFactoryUtil.isNotNull("artefattoId"); //Always true criteria
	
		for (Criterion e : criteria) {
			all = RestrictionsFactoryUtil.and(all, e);
		}
		
		return all;
	}	
	
	
	@SuppressWarnings("unchecked")
	public static JSONObject getMoreArtefacts_Sorted(int startId,JSONObject types, ResourceRequest rReq, ResourceResponse rResp,JSONObject pilots, String orderby, String keywords) {

		
		boolean isSearch = true;
		if (Validator.isNull(keywords) || keywords.trim().equals(""))
			isSearch=false;
		
		
		List<Criterion> criteria = new ArrayList<Criterion>();
		Criterion tmp = null;
		Criterion queryCriteria = null; //Chained Criteria
		
		tmp = getCategoryCriteria(types); //Category Restriction
		if(tmp!=null) criteria.add(tmp);
		
		
		Criterion c_pilots = null;
		c_pilots = getPilotsCriteria(pilots);
		if(c_pilots!=null) criteria.add(c_pilots);
		

		queryCriteria = chainCriteria(criteria);
		
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(ArtefattoImpl.class).add(queryCriteria).addOrder(OrderFactoryUtil.desc("artefattoId"));
		
		List<Artefatto> tmpList= null;
		try {
			tmpList = ArtefattoLocalServiceUtil.dynamicQuery(query);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		tmpList = ordersArtefactsList(tmpList, orderby);
		
		
		List<Artefatto> listArts = new LinkedList<Artefatto>(tmpList);		
		JSONObject container = JSONFactoryUtil.createJSONObject();		
		
		//SEARCH
		if (isSearch){
			String[] keys = keywords.split(",");
			Artefatto a = null;
			List<Artefatto> listArtsSearched = new LinkedList<Artefatto>();
			Iterator<Artefatto> it = listArts.iterator();
			
			while(it.hasNext()){
				a = it.next();
	
				for(String k:keys){
					if(MarketplaceApiUtils.matchKeyword(a,k))
						listArtsSearched.add(a);
				}
			}
			listArts = new ArrayList<Artefatto>(listArtsSearched);
		}
		
		if(listArts==null || listArts.size()==0 || tmp==null || c_pilots==null){
			try{
				container.put("empty", true);
				container.put("artefacts", JSONFactoryUtil.createJSONArray());
				container.put("finished", true);
			}
			catch(Exception e){ /* Do nothing */ }
			
			return container;
		}
		
		
		
		int delta = ConfUtil.getInteger("conf.nr.artefact.asyncLoad"); 
		int endId = Math.min(startId + delta, listArts.size());
		
		if(startId<listArts.size()){
			listArts = listArts.subList(startId, endId);
			container = generateResponse(rReq, rResp, listArts, endId);
			return container;
		}
		
		try{
			container.put("empty", false);
			container.put("artefacts", JSONFactoryUtil.createJSONArray());
			container.put("finished", true);
		}
		catch(Exception e){
			/*Do Nothing*/
		}
		
		return container;
	}
	



	public static JSONObject getMoreArtefacts(int startId, JSONObject types, ResourceRequest rReq, ResourceResponse rResp, String pilot, boolean onlyPilot) {
		
		/**
		* Il json di ritorno 
		* {
		* 	empty: boolean,
		* 	finished: boolean,
		* 	artefacts: [{ id: {long},
		* 					imgUrl: --String--,
		* 					title: --String--,
		* 					url: --String--,
		* 					score: --int[0-5]--,
		* 					type: --String-- }],
		*  next_index: int
		* }
		* 
		*/
		
		List<Artefatto> l = getArtefactList(types, pilot);
		List<Artefatto> listArts_tmp = new LinkedList<Artefatto>(l);
		List<Artefatto> listArts = new LinkedList<Artefatto>();
		
		for(Artefatto a : listArts_tmp){
			if(!a.isIsPrivate()){
				listArts.add(a);
			}
		}
		
		if(!onlyPilot){	
			User currentUser = null;
			try { 
				currentUser = PortalUtil.getUser(rReq);
				if(currentUser!=null && !currentUser.isDefaultUser()){
					String[] langs = new String[]{"English"};
					Set<String> sLangs = new HashSet<String>();
					try{
						List<UserLanguage> ulangs = UserLanguageLocalServiceUtil.getUserLanguagesByUserId(currentUser.getUserId());
						for(UserLanguage ul : ulangs){
							try{ 
								Language lang = LanguageLocalServiceUtil.getLanguage(ul.getLanguageId());
								sLangs.add(lang.getName());
								System.out.println("\t"+lang.getName());
							}
							catch(Exception e){
								//skip this lang
								System.out.println(e.getMessage());
							}
						}
						
						langs = sLangs.toArray(new String[sLangs.size()]);
					}
					catch(Exception e){
						System.out.println(e.getMessage());
						langs = (String[]) currentUser.getExpandoBridge().getAttribute("languages");
					}
					if(langs==null || langs.length==0){
						langs = new String[]{"English"};
					}
					
					List<Artefatto> tmp = getArtefactListByUserLangs(types, langs);
					List<Artefatto> list = new LinkedList<Artefatto>(tmp);
					
					list.removeAll(l);
					listArts.addAll(list);
				}
				
			} 
			catch (Exception e) {
				System.out.println("Error while getting artefacts by languages: "+e.getClass()+": "+e.getMessage());
				e.printStackTrace();
			}
		}
		
		JSONObject container = JSONFactoryUtil.createJSONObject();
		
		if(listArts==null || listArts.size()==0){
			try{
				container.put("empty", true);
				container.put("artefacts", JSONFactoryUtil.createJSONArray());
				container.put("finished", true);
			}
			catch(Exception e){ /* Do nothing */ }
			
			return container;
		}
		
		int delta = ConfUtil.getInteger("conf.nr.artefact.asyncLoad");
		int endId = Math.min(startId + delta, listArts.size());
		
		if(startId<listArts.size()){
			listArts = listArts.subList(startId, endId);
			container = generateResponse(rReq, rResp, listArts, endId);
			return container;
		}
		
		try{
			container.put("empty", false);
			container.put("artefacts", JSONFactoryUtil.createJSONArray());
			container.put("finished", true);
		}
		catch(Exception e){
			/*Do Nothing*/
		}
		
		return container;
		
	}

	public static void notifyCDV(long userId, long artId){
		CitizenDataVaultNotificationService.notify(userId, artId);
	}
	
	public static boolean isDeveloper(User user){
		//return Boolean.parseBoolean(user.getExpandoBridge().getAttribute("isDeveloper").toString());
		if(user!=null)
			return isDeveloper(user.getUserId());
		else
			return false;
	}
	
	public static boolean isAuthority(User user){
		if(user!=null)
			return isAuthority(user.getUserId());
		else
			return false;
	}
	
	
	public static boolean isDeletedUser(long userid){
		
		long companyId = MyMarketplaceUtility.getDefaultCompanyId();
		String deletedRoleName = PortletProps.get("role.deleteduser");
		
		try{ 
			Role rDeveloper = RoleLocalServiceUtil.getRole(companyId, deletedRoleName);
			List<Role> userRoles = RoleLocalServiceUtil.getUserRoles(userid);
			
			return userRoles.contains(rDeveloper);
		}
		catch(Exception e){
			return false;
		}
	}
	
	
	public static boolean isDeveloper(long userid){
		
		long companyId = MyMarketplaceUtility.getDefaultCompanyId();
		String developerRoleName = PortletProps.get("role.developer");
		
		try{ 
			Role rDeveloper = RoleLocalServiceUtil.getRole(companyId, developerRoleName);
			List<Role> userRoles = RoleLocalServiceUtil.getUserRoles(userid);
			
			return userRoles.contains(rDeveloper);
		}
		catch(Exception e){
			return false;
		}
	}
	
	public static boolean isAuthority(long userid){
		
		long companyId = MyMarketplaceUtility.getDefaultCompanyId();
		String authorityRoleName = PortletProps.get("role.authority");
		
		try{ 
			Role rDeveloper = RoleLocalServiceUtil.getRole(companyId, authorityRoleName);
			List<Role> userRoles = RoleLocalServiceUtil.getUserRoles(userid);
			
			return userRoles.contains(rDeveloper);
		}
		catch(Exception e){
			return false;
		}
	}
	
	public static String getPilot(User user, HttpServletRequest request){
		String pilotId = null;
		if(user==null || user.isDefaultUser()){
			Object oPilot = request.getSession().getAttribute("LIFERAY_SHARED_pilot");
			if(oPilot!=null) pilotId = oPilot.toString();
		}
		else{
			String[] pilots = ((String[])user.getExpandoBridge().getAttribute("pilot"));
			if(pilots!=null && pilots.length > 0) pilotId = pilots[0];
		}
		
		return pilotId;
	}
	
	public static User getDeletedUser(String pilot){
		long companyId = MyMarketplaceUtility.getDefaultCompanyId();
		Role rDeletedUser=null;
		try { rDeletedUser = RoleLocalServiceUtil.getRole(companyId, PortletProps.get("role.deleteduser")); } 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//a user foreach pilot
		List<User> users = new ArrayList<User>();
		try { users = UserLocalServiceUtil.getRoleUsers(rDeletedUser.getRoleId()); } 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//search the user for the current Pilot
		for (User u:users){
			String pilotUserToDelete = EditorUtils.getPilotByUser(u);
			if (pilot.equalsIgnoreCase(pilotUserToDelete) ){
				return u;
			}
		}
		
		return null;
	}
	
	public static long getDeletedUserId(String pilot){
		User deletedUser = getDeletedUser(pilot);
		if(deletedUser != null)
			return deletedUser.getUserId();
		
		return -1L;
	}
	
	public static ParserResponse validateOperationWadl(String url, String type){
		
		ParserResponse ret = null ;
		
		String descriptorType = type.toUpperCase();
//		String descriptorType = "WADL";
//		if(type.equalsIgnoreCase(PortletProps.get("Category.soap")))
//			descriptorType = "WSDL";
		
//		System.out.println(descriptorType);
		
		if(url==null || url.equalsIgnoreCase("") || type==null || type.equalsIgnoreCase("")){
			ret = null;
		}
		else{
			//ret = EditorUtils.validateServiceDescriptor(url, apiType);
			try{
				
				String descriptor = WizardUtils.getRemoteFileString(url);
				
				//Invoke Parser for WADL Validator
				//SOAP WSDL - REST WADL 
				ParserResponse response = MetamodelParser.parse(descriptor, MetamodelParser.SourceType.valueOf(descriptorType));
				ret= response;
//				//If an error occur 
//				if(response.getErrors().size() > 0) return null;
//				
//				//Map response into a Java Object
//				BuildingBlock bb = response.getBuildingBlock();
//				
//				if(Validator.isNotNull(bb)){			
//					ret = bb;				
//				}
//				else{ 
//					ret = null;
//				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return ret;
	}
	
	
	public static com.liferay.portal.kernel.json.JSONObject _validationWadl(String url, String type){
		
		com.liferay.portal.kernel.json.JSONObject ret = JSONFactoryUtil.createJSONObject();	
		String descriptorType = "WADL";
		if(type.equalsIgnoreCase(PortletProps.get("Category.soap")))
			descriptorType = "WSDL";
		
		System.out.println(descriptorType);
		
		if(url==null || url.equalsIgnoreCase("") || type==null || type.equalsIgnoreCase("")){
			ret.put("error", true);
			ret.put("message", "Invalid parameters");
		}
		else{
			//ret = EditorUtils.validateServiceDescriptor(url, apiType);
			try{
				
				String descriptor = WizardUtils.getRemoteFileString(url);
				
				//Invoke Parser for WADL Validator
				//SOAP WSDL - REST WADL 
				ParserResponse response = MetamodelParser.parse(descriptor, MetamodelParser.SourceType.valueOf(descriptorType));
								
				for(String err : response.getErrors()){					
					ret.put("message", err);
				}
		
				for(String warn : response.getWarnings()){
					ret.put("warning", warn);
				}
				
				//If an error occur 
				if(response.getErrors().size() > 0) ret.put("error", true);
				
				//Map response into a Java Object
				BuildingBlock bb = response.getBuildingBlock();
				
				if(Validator.isNotNull(bb)){			
					//If the BB is valid , map BB into a JSONString
					String jBBmetamodel = bb.writeAsJsonString();
					//Invoke the ArtifactValid Validator
					System.out.println(jBBmetamodel);
					ret.put("valid", ArtifactValutationUtils.isArtifactValid(jBBmetamodel));					
				}
				else{ 
					ret.put("error", true);
					String msg = response.getErrors().get(0);
					throw new Exception(msg);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				String msg = e.getMessage();
				ret.put("message", msg);
			}
		}
		
		return ret;
	}
	
	@Deprecated
	public static com.liferay.portal.kernel.json.JSONObject validationWadl(String url, String type){
		
		com.liferay.portal.kernel.json.JSONObject ret = JSONFactoryUtil.createJSONObject();
		System.out.println("OLD");
		String apiType = "rest";
		if(type.equalsIgnoreCase(PortletProps.get("Category.soap")))
			apiType = "soap";
		
		if(url==null || url.equalsIgnoreCase("") || type==null || type.equalsIgnoreCase("")){
			ret.put("error", true);
			ret.put("message", "Invalid parameters");
		}
		else{
			ret = EditorUtils.validateServiceDescriptor(url, apiType);
		}
		
		return ret;
	}
	
	public static double getArtefactRating(Artefatto a){
		
		double rating1 = 0;
		
		try { rating1 = RatingsStatsLocalServiceUtil.getStats(Artefatto.class.getName(), a.getArtefattoId()).getAverageScore(); } 
		catch (Exception e) { e.printStackTrace(); }
		
		return rating1;
	}
	
	
	
	/**Rating considering also external tool (ODS) 
	 * @param a
	 * @return
	 */
	public static double getArtefactRatingAExtAligned(Artefatto a){
		
		int extRating = a.getExtRating();
		
		if (extRating >0) //only for dataset
			return (double) extRating;
		
		return getArtefactRating(a);
	}
	
	public static com.liferay.portal.kernel.json.JSONObject easyArtefact2Json(Artefatto a) 
			throws Exception{
		
		com.liferay.portal.kernel.json.JSONObject out = JSONFactoryUtil.createJSONObject();
		
		out.put("tit", a.getTitle());
		out.put("text",  a.getAbstractDescription());
		
		out.put("url", "/marketplace/-/marketplace/view/"+a.getArtefattoId());
		String catname = "Other";
		try{catname = CategoriaLocalServiceUtil.getCategoria(a.getCategoriamkpId()).getNomeCategoria();}
		catch(Exception e) {}
		out.put("type", catname);
		out.put("image", getArtefactImage(a, "/documents/"+MyConstants.API_DL_ID+"/", "/MarketplaceStore-portlet", catname));
		out.put("rating", getArtefactRating(a));
		
		return out;
	}
	
	public static com.liferay.portal.kernel.json.JSONArray easyArtefactList2Json(List<Artefatto> aa){
		JSONArray out = JSONFactoryUtil.createJSONArray();
		
		for(Artefatto a : aa){
			try{ out.put(easyArtefact2Json(a)); }
			catch(Exception e){ e.printStackTrace(); }
		}
		
		return out;
	}

	public static Dataset setupDatasetLusdl(String title, String description, String webpage, String pilot, User author, String providerName, String language, String rdf) {
		
		List<Entity> broles = new ArrayList<Entity>();
		Entity eAuthor = new Entity(author.getFullName().trim(), "", author.getEmailAddress(), BusinessRole.AUTHOR.toString());
		broles.add(eAuthor);
		
		Entity eOwner = new Entity(author.getFullName().trim(), "", author.getEmailAddress(), BusinessRole.OWNER.toString());
		broles.add(eOwner);
		
		Entity eProvider = new Entity(providerName, "", "", BusinessRole.PROVIDER.toString());
		broles.add(eProvider);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String created = sdf.format(GregorianCalendar.getInstance().getTime());
		
		InputStream is = new ByteArrayInputStream(rdf.getBytes(StandardCharsets.UTF_8));
		Dataset out = null;
		
		try{
			ReaderRDF reader = new ReaderRDF(is);
			
			out = new Dataset(title, description, description, pilot, created, webpage, language, RDFUtils.getTags(reader) , broles, RDFUtils.getLicenses_v3(reader), new ArrayList<ServiceOffering>() );
			
			System.out.println(out.getTitle()+"\'s metamodel created.");
		}
		catch(Exception e){
			System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());
		}
		
		return out;
	}
	

	public static boolean isBilbaoPilot(String pilotId){
		if (pilotId.equals(MyConstants.PILOT_CITY_BILBAO))
			return true;
		return false;
	}
	
	public static boolean  isUusimaaPilot(String pilotId){
		if (pilotId.equals(MyConstants.PILOT_CITY_UUSIMAA))
			return true;
		return false;
	}
	
	public static boolean  isNovisadPilot(String pilotId){
		if (pilotId.equals(MyConstants.PILOT_CITY_NOVISAD))
			return true;
		return false;
	}
	
	public static boolean  isTrentoPilot(String pilotId){
		if (pilotId.equals(MyConstants.PILOT_CITY_TRENTO))
			return true;
		return false;
	}
	
	/**
	 * @param liferayPilot
	 * @return
	 */
	public static String getUIPilotByPilotId(String pilotId){
		
		String val="";
		
		Map<String, String> pilots = new HashMap<String, String>();
		pilots.put(MyConstants.PILOT_CITY_BILBAO, MyConstants.UI_PILOT_CITY_BILBAO);
		pilots.put(MyConstants.PILOT_CITY_NOVISAD, MyConstants.UI_PILOT_CITY_NOVISAD);
		pilots.put(MyConstants.PILOT_CITY_UUSIMAA, MyConstants.UI_PILOT_CITY_UUSIMAA);
		pilots.put(MyConstants.PILOT_CITY_TRENTO, MyConstants.UI_PILOT_CITY_TRENTO);
	
		
		val = pilots.get(pilotId);		
		
		return val;
		
	}
	
	/**
	 * @param artefacts
	 * @param orderBy
	 * @return
	 */
	public static List<Artefatto> ordersArtefactsList (List<Artefatto> artefacts, String orderBy){
	
		if (orderBy.equalsIgnoreCase("title")){
			ArtefactComparatorByTitle ac = new ArtefactComparatorByTitle();
			artefacts=ListUtil.sort(artefacts, ac);
		}else if(orderBy.equalsIgnoreCase("date")){
			ArtefactComparatorByDate ac = new ArtefactComparatorByDate();
			artefacts=ListUtil.sort(artefacts, ac);
		}else if(orderBy.equalsIgnoreCase("level")){
			ArtefactComparatorByLevel ac = new ArtefactComparatorByLevel();
			artefacts=ListUtil.sort(artefacts, ac);
		}else if(orderBy.equalsIgnoreCase("ratings")){
			ArtefactComparatorByRating ac = new ArtefactComparatorByRating();
			artefacts=ListUtil.sort(artefacts, ac);
		}
			return artefacts;
		
	}
	
	
	/**
	 * @param assigneeId
	 * @param owner
	 * @return
	 */
	public static boolean isOwnerEqualToAuthor (long userId, String owner){
		
		User user = null;
		
		try {
			 user = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		String fullName = user.getFullName();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		
		if (owner.equalsIgnoreCase(fullName))
			return true;
		
		if (owner.contains(lastName))
			return true;
		
		if (owner.contains(firstName))
			return true;
		
		return false;
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static String getUserFullnameByUserId (long userId){
		
		User user = null;
		
		try {
			 user = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		String fullName = user.getFullName();
		
		return fullName;
		
	}
	

	
	public static long getDefaultGroupId (){
		
		long companyId = getDefaultCompanyId();
		long globalGroupId = 0L;
		
		Group group = null;
		try {
			group = GroupLocalServiceUtil.getGroup(companyId, "Guest");
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return globalGroupId;
		}
		 globalGroupId = group.getGroupId();
		
	
		return globalGroupId;
	}
	
}
