package it.eng.rspa.marketplace.jobs.messagecomposer;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.listeners.model.ArtefactType;
import it.eng.rspa.usdl.model.Artefact;
import it.eng.rspa.usdl.model.BusinessRole;
import it.eng.rspa.usdl.model.Entity;
import it.eng.rspa.usdl.model.TermsAndCondition;
import it.eng.rspa.usdl.utils.RDFUtils;
import it.eng.sesame.Crud;
import it.eng.sesame.ReaderRDF;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Set;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class CommonMessageComposer {
	
	private static Log _log = LogFactoryUtil.getLog(CommonMessageComposer.class);
	
	protected static ReaderRDF getReaderRDF(String resourceRDF){
		if(!resourceRDF.equals("")){
			try{
			 	Crud c = new Crud();
				ByteArrayOutputStream os = c.exportRDFOutputStream(resourceRDF);
				ReaderRDF readerRDF = new ReaderRDF(os);
				return readerRDF;
			}
			catch(Exception e){ _log.warn(e); }
		}
		return null;
	}
	
	protected static Long getArtefactOwnerCcUid(Artefatto a,String opts){
		try{
			User u = UserLocalServiceUtil.getUser(a.getUserId());
			Long ccuid = Long.parseLong(u.getExpandoBridge().getAttribute("CCUserID").toString());
			return ccuid;
		}
		catch(Exception e){
			_log.error("Retriving CCUserId from the Options field");
			try {
				JSONObject obj = JSONFactoryUtil.createJSONObject(opts);
				Long ccuid = obj.getLong("CCUserID");
				return ccuid;			
			} catch (JSONException e1) {
				_log.error("the Options field for Artefact "+a.getArtefattoId()+" contains an invalid JSON");
				return null;
			}
		}
	}
	
	protected static Long getArtefactOwnerCcUid(Artefatto a){
		try{
			User u = UserLocalServiceUtil.getUser(a.getUserId());
			Long ccuid = Long.parseLong(u.getExpandoBridge().getAttribute("CCUserID").toString());
			return ccuid;
		}
		catch(Exception e){
			_log.error("Unable to arrange notification for Logging BB. No user for artefact.");
			return null;
		}
	}
	
	protected static ArtefactType getArtefactType(Artefatto a){
		Categoria cat = Category.getMapById().get(a.getCategoriamkpId());
		if(cat!=null){
			String catName = cat.getNomeCategoria();
			String bbcats = PortletProps.get("Category.catsOfType.bblocks");
			String dscats = PortletProps.get("Category.catsOfType.ds");
			String psacats = PortletProps.get("Category.catsOfType.psa");
			if(bbcats.indexOf(catName) > -1)
				return ArtefactType.BUILDING_BLOCK; 
			else if(dscats.indexOf(catName) > -1)
				return ArtefactType.DATASET;
			else if(psacats.indexOf(catName) > -1)
				return ArtefactType.PUBLIC_SERVICE_APPLICATION;
		}
		
		return ArtefactType.BUILDING_BLOCK;
	}
	
	protected static String getArtefactProvider(Artefatto a, ReaderRDF readerRDF){
		Entity entity = RDFUtils.getProvider(readerRDF);
		if(entity==null)
			entity = RDFUtils.getAuthor(readerRDF);
		if(entity==null){
			try { 
				User auth = UserLocalServiceUtil.getUser(a.getUserId());
				entity = new Entity();
				entity.setBusinessRole(BusinessRole.AUTHOR);
				entity.setMbox(auth.getEmailAddress());
				entity.setTitle(auth.getFullName());
			} 
			catch (Exception e) { entity = null; }
			
		}
		if(entity==null){
			_log.error("Unable to send notification to Logging BB. Missing business roles information");
			return null;
		}
		
		if(entity.getTitle()!=null)
			return entity.getTitle().getValue();
		
		return "WeLive_OSF";
	}
	
	protected static JSONArray getArtefactLicense(ReaderRDF readerRDF){
		Set<TermsAndCondition> licenses = RDFUtils.getTerms(readerRDF);
		JSONArray arr = JSONFactoryUtil.createJSONArray();
		for(TermsAndCondition tac : licenses){
			String license = tac.getTitle().getValue();
			arr.put(license);
		}
		
		return arr;
	}
	
	protected static JSONArray getArtefactDependencies(ReaderRDF readerRDF){
		
		Set<Artefact> arts = RDFUtils.getDependencies(readerRDF);
		JSONArray arr = JSONFactoryUtil.createJSONArray();
		for(Artefact a : arts){
			List<Artefatto> deps = ArtefattoLocalServiceUtil.getArtefattiByTitle(a.getTitle().getValue());
			if(!deps.isEmpty()){
				Artefatto d = deps.get(0);
				Long id = d.getArtefattoId();
				String dType = getArtefactType(d).toString();
				
				String resourceRDF = d.getResourceRDF();
				if(resourceRDF==null || resourceRDF.equals("")) continue;
				ReaderRDF dReader = getReaderRDF(resourceRDF);
				if(dReader==null) continue;
				
				Long owner = getArtefactOwnerCcUid(d);
				if(owner==null) continue;
				
				JSONArray jsonarr = JSONFactoryUtil.createJSONArray();
				jsonarr.put(id.toString());
				jsonarr.put(owner.toString());
				jsonarr.put(dType);
				
				arr.put(jsonarr);					
			}
		}
		
		return arr;
	}
	
	
	protected static String getPilotId(Artefatto a){
		User utente = null;
		try { utente = UserLocalServiceUtil.getUserById(a.getUserId()); } 
		catch (Exception e) {
			_log.warn(e);
		}
		if(utente==null)
			return null;
		
		String[] pilotUser = (String[]) utente.getExpandoBridge().getAttribute("pilot");
		String pilotId = pilotUser[0];
		
		return pilotId;
	}

}
