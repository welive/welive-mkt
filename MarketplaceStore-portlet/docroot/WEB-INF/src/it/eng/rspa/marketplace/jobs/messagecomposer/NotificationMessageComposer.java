package it.eng.rspa.marketplace.jobs.messagecomposer;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.listeners.model.WeLiveNotificationAction;
import com.liferay.portal.kernel.json.JSONObject;


public abstract class NotificationMessageComposer{
	
	public static JSONObject prepareLoggingMessage(WeLiveNotificationAction action, Artefatto a, String opts){
		
		switch(action){
			case ArtefactPublished: return LoggingMessageComposer.prepareLoggingPublicationMessage(a);
			case ArtefactModifed: return LoggingMessageComposer.prepareLoggingUpdateMessage(a);
			case ArtefactRemoved: return LoggingMessageComposer.prepareLoggingRemoveMessage(a, opts);
			case ArtefactRated: return LoggingMessageComposer.prepareLoggingRatingMessage(a, opts);
			case ArtefactRateModifed: return LoggingMessageComposer.prepareLoggingRatingMessage(a, opts);
			case ArtefactRateRemoved: return LoggingMessageComposer.prepareLoggingRatingRemovedMessage(a, opts);
			default: return null;
		}
				
	}
	
	public static JSONObject prepareDecisionEngineMessage(WeLiveNotificationAction action, Artefatto a, String opts){
		
		switch(action){
			case ArtefactPublished: return DecisionEngineMessageComposer.prepareDEPublicationMessage(a);
			case ArtefactModifed: return DecisionEngineMessageComposer.prepareDEUpdateMessage(a);
			case ArtefactRemoved: return DecisionEngineMessageComposer.prepareDERemoveMessage(a);
			default: return null;
		}
	}

	public static JSONObject prepareODSMessage(WeLiveNotificationAction action, Artefatto a, String opts) {
		switch(action){
			case ArtefactRated: return ODSMessageComposer.prepareODSRatingMessage(a, opts);
			case ArtefactRateModifed: return ODSMessageComposer.prepareODSRatingMessage(a, opts);
			case ArtefactRateRemoved: return ODSMessageComposer.prepareODSRatingRemovedMessage(a, opts);
			case ArtefactRemoved: return ODSMessageComposer.prepareODSRatingRemovedMessage(a, opts);
			default: return null;
		}
	}

}
