/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ArtifactLevels in entity cache.
 *
 * @author eng
 * @see ArtifactLevels
 * @generated
 */
public class ArtifactLevelsCacheModel implements CacheModel<ArtifactLevels>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{artifactId=");
		sb.append(artifactId);
		sb.append(", level=");
		sb.append(level);
		sb.append(", notes=");
		sb.append(notes);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ArtifactLevels toEntityModel() {
		ArtifactLevelsImpl artifactLevelsImpl = new ArtifactLevelsImpl();

		artifactLevelsImpl.setArtifactId(artifactId);
		artifactLevelsImpl.setLevel(level);

		if (notes == null) {
			artifactLevelsImpl.setNotes(StringPool.BLANK);
		}
		else {
			artifactLevelsImpl.setNotes(notes);
		}

		artifactLevelsImpl.resetOriginalValues();

		return artifactLevelsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		artifactId = objectInput.readLong();
		level = objectInput.readInt();
		notes = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(artifactId);
		objectOutput.writeInt(level);

		if (notes == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(notes);
		}
	}

	public long artifactId;
	public int level;
	public String notes;
}