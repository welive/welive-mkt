package it.eng.rspa.marketplace.controller.model;

import java.util.Set;

public class ViewArtefattoAjaxModel {

	private Rating rating;
	private Set<Resource> resources;
	
	public Rating getRating() {
		return rating;
	}
	public void setRating(Rating rating) {
		this.rating = rating;
	}
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	
}
