package it.eng.rspa.marketplace.controller.model;

public class CategoryListItem {

	private String name;
	private String url;
	private Boolean current;
	private String tip;
	
	
	public CategoryListItem(String name, String url, Boolean current, String tip) {
		this.name = name;
		this.url = url;
		this.current = current;
		this.tip = tip;
	}
	
	public CategoryListItem(String name, String url, Boolean current) {
		this(name, url, current, null);
	}	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getCurrent() {
		return current;
	}
	public void setCurrent(Boolean current) {
		this.current = current;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	
	
}
