package it.eng.rspa.marketplace;

import it.eng.metamodel.parser.MetamodelParser;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.controller.ViewArtefattoController;
import it.eng.rspa.marketplace.controller.model.Rating;
import it.eng.rspa.marketplace.controller.model.Resource;
import it.eng.rspa.marketplace.controller.model.ViewArtefattoAjaxModel;
import it.eng.rspa.marketplace.utils.MyConstants;
import it.eng.rspa.marketplace.wizard.WizardActions;
import it.eng.rspa.marketplace.wizard.WizardActions.DLFOLDER_TYPE;
import it.eng.rspa.wizard.WizardUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.ratings.service.RatingsEntryLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class MarketplaceStore
 */
public class MarketplaceStore extends MVCPortlet {
	
	public void deleteArtifact(ActionRequest actionRequest, ActionResponse actionResponse){
		try { WizardActions.deleteArtifact(actionRequest, actionResponse); } 
		catch (Exception e) {
			SessionErrors.add(actionRequest, e.getMessage());
		}
	}
	
	public void serveResource(ResourceRequest rReq, ResourceResponse rResp){
		
		String action= rReq.getParameter("action");
		
		if(action.equals("updatelkl")){
			ThemeDisplay themeD = (ThemeDisplay) rReq.getAttribute(WebKeys.THEME_DISPLAY);
			CitizenDataVaultNotificationService.notifylkl(themeD.getUser(), Double.parseDouble(rReq.getParameter("lat")), Double.parseDouble(rReq.getParameter("lng")));
			
		}
		else if(action.equals("moreArtefacts")){
			
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(rReq);
			try{
				
				int startId = Integer.parseInt(rReq.getParameter("start_id"));
				
				JSONObject types = JSONFactoryUtil.createJSONObject();
						types.put("dataset", ParamUtil.getBoolean(uploadPortletRequest, "types[dataset]"));
						types.put("psa", ParamUtil.getBoolean(uploadPortletRequest, "types[psa]"));
						types.put("bblocks", ParamUtil.getBoolean(uploadPortletRequest, "types[bblocks]"));
				
				JSONObject pilots = JSONFactoryUtil.createJSONObject();
						pilots.put(MyConstants.PILOT_CITY_BILBAO, ParamUtil.getBoolean(uploadPortletRequest, "pilots[Bilbao]"));
						pilots.put(MyConstants.PILOT_CITY_UUSIMAA, ParamUtil.getBoolean(uploadPortletRequest, "pilots[Uusimaa]"));
						pilots.put(MyConstants.PILOT_CITY_NOVISAD, ParamUtil.getBoolean(uploadPortletRequest, "pilots[Novisad]"));
						pilots.put(MyConstants.PILOT_CITY_TRENTO, ParamUtil.getBoolean(uploadPortletRequest, "pilots[Trento]"));
						
				String orderby = ParamUtil.getString(uploadPortletRequest, "orderby");
				String searchby = ParamUtil.getString(uploadPortletRequest, "searchby");
				
				JSONObject json	=JSONFactoryUtil.createJSONObject();	
				json = MyMarketplaceUtility.getMoreArtefacts_Sorted(startId, types, rReq, rResp, pilots,orderby, searchby );

				rResp.getWriter().write(json.toString());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		else if(action.equals("cdvNotification")){
			try{
				long userId = Long.parseLong(rReq.getParameter("userId"));
				long artId = Long.parseLong(rReq.getParameter("artId"));

				MyMarketplaceUtility.notifyCDV(userId, artId);
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		else if(action.equals("recommend")){
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(rReq);
			
			List<Artefatto> sugg = new ArrayList<Artefatto>();
			JSONObject container = JSONFactoryUtil.createJSONObject();
			JSONArray arr = JSONFactoryUtil.createJSONArray();
			
			if(ConfUtil.getBoolean("de.enabled")){
				try{ 
					Long userid = Long.parseLong(rReq.getParameter("userId"));
					Set<String> catset = new HashSet<String>(); 
					
					if(ParamUtil.getBoolean(uploadPortletRequest, "types[dataset]")){
						String[] cats = PortletProps.get("Category.catsOfType.ds").split(",");
						for(String cat : cats){
							catset.add(cat);
						}
						
					}
					if(ParamUtil.getBoolean(uploadPortletRequest, "types[bblocks]")){
						String[] cats = PortletProps.get("Category.catsOfType.bblocks").split(",");
						for(String cat : cats){
							catset.add(cat);
						}
					}
					if(ParamUtil.getBoolean(uploadPortletRequest, "types[psa]")){
						String[] cats = PortletProps.get("Category.catsOfType.psa").split(",");
						for(String cat : cats){
							catset.add(cat);
						}
					}
					
					String sLatitude = rReq.getParameter("lat");
					String sLongitude =  rReq.getParameter("lng");
					
					double lat = -1L;
					double lng = -1L;
					if(sLatitude!=null && sLongitude!=null && !sLatitude.equals("") && !sLongitude.equals("")){
						try{
							lat = Double.parseDouble(sLatitude);
							lng = Double.parseDouble(sLongitude);
						}
						catch(Exception e){
							System.out.println(e.getClass()+": "+e.getMessage());
							lat=-1L;
							lng=-1L;
						}
					}
					
					sugg = MyMarketplaceUtility.getSuggestedArtefacts(userid, catset, lat, lng, 20.0);
					PortletURL rowURL = rResp.createRenderURL();
					rowURL.setParameter("jspPage", "/html/marketplacestore/view_artefatto.jsp");
					ThemeDisplay themeDisplay = (ThemeDisplay)rReq.getAttribute(WebKeys.THEME_DISPLAY);
					
					String pathDocumentLibrary = themeDisplay.getPortalURL()
							+ themeDisplay.getPathContext() + "/documents/"
							+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
					
					boolean cachedrating = !ConfUtil.getBoolean("ods.enabled");
					for(Artefatto a:sugg){
						JSONObject out = MyMarketplaceUtility.artefactToJSON(pathDocumentLibrary, rReq.getContextPath(), a, rowURL, cachedrating);
						
						//accodo l'artefatto corrente
						arr.put(out);
						try { cachedrating = out.getBoolean("cached-rating"); } 
						catch (Exception e) { /*Do nothing. Leave cachedrating as is*/ }
					}
					
				}
				catch(Exception e){ System.out.println("Unable to find Reccomended artefacts. "+e.getMessage()); }
			}
				
			try{
				container.put("artefacts", arr);
				container.put("finished", true);
				rResp.getWriter().write(container.toString());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		else if(action.equals("validate")){
			
			try{
				com.liferay.portal.kernel.json.JSONObject ret = JSONFactoryUtil.createJSONObject();
				
				if(!ConfUtil.getBoolean("vc.validation.enabled")){
					ret.put("valid", true);
				}
				else{
					String url = rReq.getParameter("url");
					String type = rReq.getParameter("type");
					
					ret = MyMarketplaceUtility.validationWadl(url, type);
					if(!ret.getBoolean("valid")){
						//Internazionalizzo il messaggio di errore
						ThemeDisplay themeD = (ThemeDisplay) rReq.getAttribute(WebKeys.THEME_DISPLAY);
						ResourceBundle res = ResourceBundle.getBundle("content/Language", themeD.getLocale());
						String message = res.getString(ret.getString("code"));
						
						ret.put("code", message);
					}
				}
				
				rResp.getWriter().write(ret.toString());
			}
			catch(Exception e){ e.printStackTrace(); }
		}
		else if(action.equals("alignDatasetMetadata")){
			
			try{
				User currentUser = null;
				currentUser = PortalUtil.getUser(rReq);
				long aId = Long.parseLong(rReq.getParameter("artefactid"));
				Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(aId);
				Rating score = ViewArtefattoController.alignDatasetRating(a, currentUser);
				Set<Resource> res = ViewArtefattoController.getResources(a);
				
				ViewArtefattoAjaxModel out = new ViewArtefattoAjaxModel();
				out.setRating(score);
				out.setResources(res);
				
				Gson gson = new Gson();
				rResp.getWriter().write(gson.toJson(out));
			}
			catch(Exception e){ 
				e.printStackTrace(); 
			}
		}
		else if(action.equals("storeRating")){
			try{
				long aId = Long.parseLong(rReq.getParameter("artefactid"));
				String classname = Artefatto.class.getName();
				long uId = Long.parseLong(rReq.getParameter("userid"));
				int score = Integer.parseInt(rReq.getParameter("rating"));
				ServiceContext serviceContext = ServiceContextFactory.getInstance(Artefatto.class.getName(),rReq);
				RatingsEntryLocalServiceUtil.updateEntry(uId, classname, aId, score,serviceContext);
				
			}
			catch(Exception e){
				e.printStackTrace(); 
			}
		}
		else if(action.equals("preparedescriptor")){
			try{
				long aId = Long.parseLong(rReq.getParameter("idArtefatto"));
				Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(aId);
				
				String lusdlmodel = a.getLusdlmodel().replaceAll("&quot;", "\\\\\"");
				
				List<String> s_descriptor = MetamodelParser.convertDescriptor(lusdlmodel, MetamodelParser.SourceType.METAMODEL, MetamodelParser.SourceType.SWAGGER);
				
				String filenameTmp = "";
				
				if (s_descriptor.size() >1){
					filenameTmp ="Multihost"+GregorianCalendar.getInstance().getTimeInMillis()+".zip";
				}else{
					
					filenameTmp =a.getTitle().trim().replaceAll(" ", "_")+"_"+GregorianCalendar.getInstance().getTimeInMillis()+".json";
				}
				
				File tmpfile = new File(filenameTmp);
				
				
				if (s_descriptor.size() >1){
					
					FileOutputStream fos = new FileOutputStream(filenameTmp);
		            ZipOutputStream zos = new ZipOutputStream(fos);
					
					for (int i=0; i<s_descriptor.size(); i++){
							
							String filename = a.getTitle().trim().replaceAll(" ", "_")+"_"+i+"_"+GregorianCalendar.getInstance().getTimeInMillis()+".json";
							File tmpDescrfile = new File(filename);
							
							try(PrintWriter pw = new PrintWriter(tmpDescrfile)){
								pw.write(s_descriptor.get(i));
							}
							
							 zos.putNextEntry(new ZipEntry(new File(filename).getName()));
							 
				             byte[] bytes = Files.readAllBytes(Paths.get(filename));
				             zos.write(bytes, 0, bytes.length);
				             zos.closeEntry();
							
					}
					 zos.close();
				
				}else{
								
					if (s_descriptor.size() ==1){
						try(PrintWriter pw = new PrintWriter(tmpfile)){
								pw.write(s_descriptor.get(0));
						}
					
					}
				}
				
				
				DLFolder tmpfolder = DLFolderLocalServiceUtil.getDLFolder(ConfUtil.getLong("download.tmp.dlfolder.id"));
				UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(rReq);
				
				ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
				DLFolder dlFolder = (DLFolder) WizardUtils._getMarketplaceFolder(themeDisplay, uploadPortletRequest);
				
				dlFolder = WizardUtils.trovaCreaDLFolder(uploadPortletRequest,
														themeDisplay.getScopeGroupId(),
														dlFolder.getFolderId(),
														"TMP",
														"Temporary folder",
														DLFOLDER_TYPE.SUBIMAGES);
				
				DLFileEntry dlf = MyMarketplaceUtility.storeDLFile(uploadPortletRequest, tmpfile, tmpfolder);
				String pathDocumentLibrary = themeDisplay.getPortalURL()
											+ themeDisplay.getPathContext() + "/documents/"
											+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
				String path = pathDocumentLibrary+dlf.getUuid();
				
				com.liferay.portal.kernel.json.JSONObject error = JSONFactoryUtil.createJSONObject();
				error.put("error", false);
				error.put("redirect", path);
				
				rResp.getWriter().write(error.toString());
				
			}
			catch(Exception e){
				e.printStackTrace();
				com.liferay.portal.kernel.json.JSONObject error = JSONFactoryUtil.createJSONObject();
				error.put("error", true);
				error.put("msg", e.getMessage());
				
				try { rResp.getWriter().write(error.toString());} 
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		return;
		
	}
}