package it.eng.rspa.marketplace.listeners.model;

public enum ArtefactType {

	BUILDING_BLOCK("Building Block"),
	DATASET ("Dataset"),
	PUBLIC_SERVICE_APPLICATION("Public Service");

    private final String text;

    /**
     * @param text
     */
    private ArtefactType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
	
}
