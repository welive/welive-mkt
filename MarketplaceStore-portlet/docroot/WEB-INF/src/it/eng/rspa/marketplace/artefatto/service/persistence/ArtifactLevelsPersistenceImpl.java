/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the artifact levels service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactLevelsPersistence
 * @see ArtifactLevelsUtil
 * @generated
 */
public class ArtifactLevelsPersistenceImpl extends BasePersistenceImpl<ArtifactLevels>
	implements ArtifactLevelsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ArtifactLevelsUtil} to access the artifact levels persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ArtifactLevelsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTID_LEVEL =
		new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByArtifactId_Level",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID_LEVEL =
		new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByArtifactId_Level",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ArtifactLevelsModelImpl.ARTIFACTID_COLUMN_BITMASK |
			ArtifactLevelsModelImpl.LEVEL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTIFACTID_LEVEL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByArtifactId_Level",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the artifact levelses where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @return the matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByArtifactId_Level(long artifactId,
		int level) throws SystemException {
		return findByArtifactId_Level(artifactId, level, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @return the range of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByArtifactId_Level(long artifactId,
		int level, int start, int end) throws SystemException {
		return findByArtifactId_Level(artifactId, level, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByArtifactId_Level(long artifactId,
		int level, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID_LEVEL;
			finderArgs = new Object[] { artifactId, level };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTID_LEVEL;
			finderArgs = new Object[] {
					artifactId, level,
					
					start, end, orderByComparator
				};
		}

		List<ArtifactLevels> list = (List<ArtifactLevels>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ArtifactLevels artifactLevels : list) {
				if ((artifactId != artifactLevels.getArtifactId()) ||
						(level != artifactLevels.getLevel())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTIFACTLEVELS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTID_LEVEL_ARTIFACTID_2);

			query.append(_FINDER_COLUMN_ARTIFACTID_LEVEL_LEVEL_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtifactLevelsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				qPos.add(level);

				if (!pagination) {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactLevels>(list);
				}
				else {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByArtifactId_Level_First(long artifactId,
		int level, OrderByComparator orderByComparator)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = fetchByArtifactId_Level_First(artifactId,
				level, orderByComparator);

		if (artifactLevels != null) {
			return artifactLevels;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(", level=");
		msg.append(level);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactLevelsException(msg.toString());
	}

	/**
	 * Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByArtifactId_Level_First(long artifactId,
		int level, OrderByComparator orderByComparator)
		throws SystemException {
		List<ArtifactLevels> list = findByArtifactId_Level(artifactId, level,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByArtifactId_Level_Last(long artifactId,
		int level, OrderByComparator orderByComparator)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = fetchByArtifactId_Level_Last(artifactId,
				level, orderByComparator);

		if (artifactLevels != null) {
			return artifactLevels;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(", level=");
		msg.append(level);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactLevelsException(msg.toString());
	}

	/**
	 * Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByArtifactId_Level_Last(long artifactId,
		int level, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByArtifactId_Level(artifactId, level);

		if (count == 0) {
			return null;
		}

		List<ArtifactLevels> list = findByArtifactId_Level(artifactId, level,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the artifact levelses where artifactId = &#63; and level = &#63; from the database.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArtifactId_Level(long artifactId, int level)
		throws SystemException {
		for (ArtifactLevels artifactLevels : findByArtifactId_Level(
				artifactId, level, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artifactLevels);
		}
	}

	/**
	 * Returns the number of artifact levelses where artifactId = &#63; and level = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param level the level
	 * @return the number of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArtifactId_Level(long artifactId, int level)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTIFACTID_LEVEL;

		Object[] finderArgs = new Object[] { artifactId, level };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTIFACTLEVELS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTID_LEVEL_ARTIFACTID_2);

			query.append(_FINDER_COLUMN_ARTIFACTID_LEVEL_LEVEL_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				qPos.add(level);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTIFACTID_LEVEL_ARTIFACTID_2 = "artifactLevels.artifactId = ? AND ";
	private static final String _FINDER_COLUMN_ARTIFACTID_LEVEL_LEVEL_2 = "artifactLevels.level = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LEVEL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLevel",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVEL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED,
			ArtifactLevelsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLevel",
			new String[] { Integer.class.getName() },
			ArtifactLevelsModelImpl.LEVEL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LEVEL = new FinderPath(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLevel",
			new String[] { Integer.class.getName() });

	/**
	 * Returns all the artifact levelses where level = &#63;.
	 *
	 * @param level the level
	 * @return the matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByLevel(int level)
		throws SystemException {
		return findByLevel(level, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact levelses where level = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param level the level
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @return the range of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByLevel(int level, int start, int end)
		throws SystemException {
		return findByLevel(level, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact levelses where level = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param level the level
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findByLevel(int level, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVEL;
			finderArgs = new Object[] { level };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LEVEL;
			finderArgs = new Object[] { level, start, end, orderByComparator };
		}

		List<ArtifactLevels> list = (List<ArtifactLevels>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ArtifactLevels artifactLevels : list) {
				if ((level != artifactLevels.getLevel())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTIFACTLEVELS_WHERE);

			query.append(_FINDER_COLUMN_LEVEL_LEVEL_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtifactLevelsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(level);

				if (!pagination) {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactLevels>(list);
				}
				else {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artifact levels in the ordered set where level = &#63;.
	 *
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByLevel_First(int level,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = fetchByLevel_First(level,
				orderByComparator);

		if (artifactLevels != null) {
			return artifactLevels;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("level=");
		msg.append(level);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactLevelsException(msg.toString());
	}

	/**
	 * Returns the first artifact levels in the ordered set where level = &#63;.
	 *
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByLevel_First(int level,
		OrderByComparator orderByComparator) throws SystemException {
		List<ArtifactLevels> list = findByLevel(level, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artifact levels in the ordered set where level = &#63;.
	 *
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByLevel_Last(int level,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = fetchByLevel_Last(level,
				orderByComparator);

		if (artifactLevels != null) {
			return artifactLevels;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("level=");
		msg.append(level);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtifactLevelsException(msg.toString());
	}

	/**
	 * Returns the last artifact levels in the ordered set where level = &#63;.
	 *
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByLevel_Last(int level,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLevel(level);

		if (count == 0) {
			return null;
		}

		List<ArtifactLevels> list = findByLevel(level, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artifact levelses before and after the current artifact levels in the ordered set where level = &#63;.
	 *
	 * @param artifactId the primary key of the current artifact levels
	 * @param level the level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels[] findByLevel_PrevAndNext(long artifactId, int level,
		OrderByComparator orderByComparator)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = findByPrimaryKey(artifactId);

		Session session = null;

		try {
			session = openSession();

			ArtifactLevels[] array = new ArtifactLevelsImpl[3];

			array[0] = getByLevel_PrevAndNext(session, artifactLevels, level,
					orderByComparator, true);

			array[1] = artifactLevels;

			array[2] = getByLevel_PrevAndNext(session, artifactLevels, level,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ArtifactLevels getByLevel_PrevAndNext(Session session,
		ArtifactLevels artifactLevels, int level,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTIFACTLEVELS_WHERE);

		query.append(_FINDER_COLUMN_LEVEL_LEVEL_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtifactLevelsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(level);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artifactLevels);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ArtifactLevels> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artifact levelses where level = &#63; from the database.
	 *
	 * @param level the level
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByLevel(int level) throws SystemException {
		for (ArtifactLevels artifactLevels : findByLevel(level,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artifactLevels);
		}
	}

	/**
	 * Returns the number of artifact levelses where level = &#63;.
	 *
	 * @param level the level
	 * @return the number of matching artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByLevel(int level) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LEVEL;

		Object[] finderArgs = new Object[] { level };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTIFACTLEVELS_WHERE);

			query.append(_FINDER_COLUMN_LEVEL_LEVEL_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(level);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LEVEL_LEVEL_2 = "artifactLevels.level = ?";

	public ArtifactLevelsPersistenceImpl() {
		setModelClass(ArtifactLevels.class);
	}

	/**
	 * Caches the artifact levels in the entity cache if it is enabled.
	 *
	 * @param artifactLevels the artifact levels
	 */
	@Override
	public void cacheResult(ArtifactLevels artifactLevels) {
		EntityCacheUtil.putResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsImpl.class, artifactLevels.getPrimaryKey(),
			artifactLevels);

		artifactLevels.resetOriginalValues();
	}

	/**
	 * Caches the artifact levelses in the entity cache if it is enabled.
	 *
	 * @param artifactLevelses the artifact levelses
	 */
	@Override
	public void cacheResult(List<ArtifactLevels> artifactLevelses) {
		for (ArtifactLevels artifactLevels : artifactLevelses) {
			if (EntityCacheUtil.getResult(
						ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
						ArtifactLevelsImpl.class, artifactLevels.getPrimaryKey()) == null) {
				cacheResult(artifactLevels);
			}
			else {
				artifactLevels.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all artifact levelses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ArtifactLevelsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ArtifactLevelsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the artifact levels.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ArtifactLevels artifactLevels) {
		EntityCacheUtil.removeResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsImpl.class, artifactLevels.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ArtifactLevels> artifactLevelses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ArtifactLevels artifactLevels : artifactLevelses) {
			EntityCacheUtil.removeResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
				ArtifactLevelsImpl.class, artifactLevels.getPrimaryKey());
		}
	}

	/**
	 * Creates a new artifact levels with the primary key. Does not add the artifact levels to the database.
	 *
	 * @param artifactId the primary key for the new artifact levels
	 * @return the new artifact levels
	 */
	@Override
	public ArtifactLevels create(long artifactId) {
		ArtifactLevels artifactLevels = new ArtifactLevelsImpl();

		artifactLevels.setNew(true);
		artifactLevels.setPrimaryKey(artifactId);

		return artifactLevels;
	}

	/**
	 * Removes the artifact levels with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param artifactId the primary key of the artifact levels
	 * @return the artifact levels that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels remove(long artifactId)
		throws NoSuchArtifactLevelsException, SystemException {
		return remove((Serializable)artifactId);
	}

	/**
	 * Removes the artifact levels with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the artifact levels
	 * @return the artifact levels that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels remove(Serializable primaryKey)
		throws NoSuchArtifactLevelsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ArtifactLevels artifactLevels = (ArtifactLevels)session.get(ArtifactLevelsImpl.class,
					primaryKey);

			if (artifactLevels == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchArtifactLevelsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(artifactLevels);
		}
		catch (NoSuchArtifactLevelsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ArtifactLevels removeImpl(ArtifactLevels artifactLevels)
		throws SystemException {
		artifactLevels = toUnwrappedModel(artifactLevels);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(artifactLevels)) {
				artifactLevels = (ArtifactLevels)session.get(ArtifactLevelsImpl.class,
						artifactLevels.getPrimaryKeyObj());
			}

			if (artifactLevels != null) {
				session.delete(artifactLevels);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (artifactLevels != null) {
			clearCache(artifactLevels);
		}

		return artifactLevels;
	}

	@Override
	public ArtifactLevels updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws SystemException {
		artifactLevels = toUnwrappedModel(artifactLevels);

		boolean isNew = artifactLevels.isNew();

		ArtifactLevelsModelImpl artifactLevelsModelImpl = (ArtifactLevelsModelImpl)artifactLevels;

		Session session = null;

		try {
			session = openSession();

			if (artifactLevels.isNew()) {
				session.save(artifactLevels);

				artifactLevels.setNew(false);
			}
			else {
				session.merge(artifactLevels);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ArtifactLevelsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((artifactLevelsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID_LEVEL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artifactLevelsModelImpl.getOriginalArtifactId(),
						artifactLevelsModelImpl.getOriginalLevel()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTID_LEVEL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID_LEVEL,
					args);

				args = new Object[] {
						artifactLevelsModelImpl.getArtifactId(),
						artifactLevelsModelImpl.getLevel()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTID_LEVEL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTID_LEVEL,
					args);
			}

			if ((artifactLevelsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVEL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artifactLevelsModelImpl.getOriginalLevel()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LEVEL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVEL,
					args);

				args = new Object[] { artifactLevelsModelImpl.getLevel() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LEVEL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVEL,
					args);
			}
		}

		EntityCacheUtil.putResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
			ArtifactLevelsImpl.class, artifactLevels.getPrimaryKey(),
			artifactLevels);

		return artifactLevels;
	}

	protected ArtifactLevels toUnwrappedModel(ArtifactLevels artifactLevels) {
		if (artifactLevels instanceof ArtifactLevelsImpl) {
			return artifactLevels;
		}

		ArtifactLevelsImpl artifactLevelsImpl = new ArtifactLevelsImpl();

		artifactLevelsImpl.setNew(artifactLevels.isNew());
		artifactLevelsImpl.setPrimaryKey(artifactLevels.getPrimaryKey());

		artifactLevelsImpl.setArtifactId(artifactLevels.getArtifactId());
		artifactLevelsImpl.setLevel(artifactLevels.getLevel());
		artifactLevelsImpl.setNotes(artifactLevels.getNotes());

		return artifactLevelsImpl;
	}

	/**
	 * Returns the artifact levels with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the artifact levels
	 * @return the artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByPrimaryKey(Serializable primaryKey)
		throws NoSuchArtifactLevelsException, SystemException {
		ArtifactLevels artifactLevels = fetchByPrimaryKey(primaryKey);

		if (artifactLevels == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchArtifactLevelsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return artifactLevels;
	}

	/**
	 * Returns the artifact levels with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException} if it could not be found.
	 *
	 * @param artifactId the primary key of the artifact levels
	 * @return the artifact levels
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels findByPrimaryKey(long artifactId)
		throws NoSuchArtifactLevelsException, SystemException {
		return findByPrimaryKey((Serializable)artifactId);
	}

	/**
	 * Returns the artifact levels with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the artifact levels
	 * @return the artifact levels, or <code>null</code> if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ArtifactLevels artifactLevels = (ArtifactLevels)EntityCacheUtil.getResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
				ArtifactLevelsImpl.class, primaryKey);

		if (artifactLevels == _nullArtifactLevels) {
			return null;
		}

		if (artifactLevels == null) {
			Session session = null;

			try {
				session = openSession();

				artifactLevels = (ArtifactLevels)session.get(ArtifactLevelsImpl.class,
						primaryKey);

				if (artifactLevels != null) {
					cacheResult(artifactLevels);
				}
				else {
					EntityCacheUtil.putResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
						ArtifactLevelsImpl.class, primaryKey,
						_nullArtifactLevels);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ArtifactLevelsModelImpl.ENTITY_CACHE_ENABLED,
					ArtifactLevelsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return artifactLevels;
	}

	/**
	 * Returns the artifact levels with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param artifactId the primary key of the artifact levels
	 * @return the artifact levels, or <code>null</code> if a artifact levels with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ArtifactLevels fetchByPrimaryKey(long artifactId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)artifactId);
	}

	/**
	 * Returns all the artifact levelses.
	 *
	 * @return the artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artifact levelses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @return the range of artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the artifact levelses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artifact levelses
	 * @param end the upper bound of the range of artifact levelses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ArtifactLevels> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ArtifactLevels> list = (List<ArtifactLevels>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ARTIFACTLEVELS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ARTIFACTLEVELS;

				if (pagination) {
					sql = sql.concat(ArtifactLevelsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ArtifactLevels>(list);
				}
				else {
					list = (List<ArtifactLevels>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the artifact levelses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ArtifactLevels artifactLevels : findAll()) {
			remove(artifactLevels);
		}
	}

	/**
	 * Returns the number of artifact levelses.
	 *
	 * @return the number of artifact levelses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ARTIFACTLEVELS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the artifact levels persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.ArtifactLevels")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ArtifactLevels>> listenersList = new ArrayList<ModelListener<ArtifactLevels>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ArtifactLevels>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ArtifactLevelsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ARTIFACTLEVELS = "SELECT artifactLevels FROM ArtifactLevels artifactLevels";
	private static final String _SQL_SELECT_ARTIFACTLEVELS_WHERE = "SELECT artifactLevels FROM ArtifactLevels artifactLevels WHERE ";
	private static final String _SQL_COUNT_ARTIFACTLEVELS = "SELECT COUNT(artifactLevels) FROM ArtifactLevels artifactLevels";
	private static final String _SQL_COUNT_ARTIFACTLEVELS_WHERE = "SELECT COUNT(artifactLevels) FROM ArtifactLevels artifactLevels WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "artifactLevels.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ArtifactLevels exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ArtifactLevels exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ArtifactLevelsPersistenceImpl.class);
	private static ArtifactLevels _nullArtifactLevels = new ArtifactLevelsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ArtifactLevels> toCacheModel() {
				return _nullArtifactLevelsCacheModel;
			}
		};

	private static CacheModel<ArtifactLevels> _nullArtifactLevelsCacheModel = new CacheModel<ArtifactLevels>() {
			@Override
			public ArtifactLevels toEntityModel() {
				return _nullArtifactLevels;
			}
		};
}