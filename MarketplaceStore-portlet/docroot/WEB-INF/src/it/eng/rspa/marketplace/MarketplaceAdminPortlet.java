package it.eng.rspa.marketplace;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;
import it.eng.rspa.marketplace.artefatto.model.impl.CategoriaImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

/**
 * Portlet implementation class MarketplaceAdminPortlet
 */
public class MarketplaceAdminPortlet extends MVCPortlet {
	
	private static Log _log = LogFactoryUtil.getLog(MarketplaceAdminPortlet.class);
	
	//Init Conf Key Pattern
	final String confPropKeyRegex = "^init\\.(.+)$";
	final Pattern confPropKeyPattern = Pattern.compile(confPropKeyRegex);
	
	//Init Conf Value Pattern
	final String confPropValueRegex = "^(?:\\[(\\w*)\\])?(.+)$";
	final Pattern confPropValuePattern = Pattern.compile(confPropValueRegex);
	
	public MarketplaceAdminPortlet(){
		super();
		
		_log.info("Configuring Marketplace Portlet");
		
		Properties props = PortletProps.getProperties();
		for(Entry<Object, Object> p : props.entrySet()){
			
			try{
				
				String k = ((String)p.getKey()).trim();
				
				Matcher keyMatcher = confPropKeyPattern.matcher(k);
				if(!keyMatcher.matches())
					continue;
					
				String confKey = keyMatcher.group(1);
				String confType = "String";
					
				String v = ((String)p.getValue()).trim();
				final Matcher valueMatcher = confPropValuePattern.matcher(v);
	
				if(!valueMatcher.matches())
				   continue;
			
				if(valueMatcher.group(1)!= null && !valueMatcher.group(1).equalsIgnoreCase(""))
					confType = valueMatcher.group(1);
	
				String confValue = valueMatcher.group(2);
				Method method = ConfUtil.class.getMethod("get"+confType.substring(0, 1).toUpperCase()+confType.substring(1).toLowerCase(), String.class);
				Object dbprop = method.invoke(null, confKey);
				
				if(dbprop==null || dbprop.toString().equalsIgnoreCase("")){
					
					if(confKey.equals("download.tmp.dlfolder.id")){
						Date now = GregorianCalendar.getInstance().getTime();
						
						DLFolder folder = DLFolderLocalServiceUtil.createDLFolder(CounterLocalServiceUtil.increment(DLFolder.class.getName()));
						folder.setName("TMP");
						folder.setParentFolderId(0);
						folder.setDescription("download.tmp.dlfolder");
						
						Company defaultCompany = MyMarketplaceUtility.getDefaultCompany();
						folder.setCompanyId(defaultCompany.getCompanyId());
						
						long userid = -1;
						long groupid = -1;
						Role admin = RoleLocalServiceUtil.getRole(defaultCompany.getCompanyId(), PortletProps.get("role.admin"));
						List<User> users = UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
						for(User u : users){
							try{
								if(u.getRoles().contains(admin)){
									userid = u.getUserId();
									groupid = u.getGroupId();
								}
							}
							catch(Exception e){
								System.out.println(e.getClass().getSimpleName()+": "+e.getMessage());
							}
						}
						
						if(userid == -1 || groupid == -1)
							throw new Exception("No administrator defined. Plese create the download.tmp.dlfolder folder manually.");
						
						folder.setGroupId(groupid);
						folder.setUserId(userid);
						
						folder.setCreateDate(now);
						folder.setModifiedDate(now);
						folder.setLastPostDate(now);
						
						DLFolderLocalServiceUtil.addDLFolder(folder);
						
						confValue = String.valueOf(folder.getFolderId());
					}
					
					
					System.out.println("Setting up "+confType + " " + confKey+" = "+confValue);
					
					MarketplaceConf cfg = MarketplaceConfLocalServiceUtil.createMarketplaceConf(CounterLocalServiceUtil.increment(MarketplaceConf.class.getName()));
					cfg.setKey(confKey);
					cfg.setType(confType);
					cfg.setValue(confValue);
					
					MarketplaceConfLocalServiceUtil.updateMarketplaceConf(cfg);
				}
			
			}
			catch(Exception e){ e.printStackTrace(); }
		}
	}
	
	

	public void addCategory(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Categoria newCat = new CategoriaImpl();
    	long catId = CounterLocalServiceUtil.increment(newCat.getClass().getName());
    	newCat.setIdCategoria(catId);
    	newCat.setNomeCategoria(actionRequest.getParameter("newCategoryName"));
    	newCat.setSupports(actionRequest.getParameter("newCategorySupports"));
    	_log.info("##ADD##\nCategoria ["+catId+" - "+newCat.getNomeCategoria()+" - "+newCat.getSupports()+"]");
    	
    	CategoriaLocalServiceUtil.addCategoria(newCat);
    	Category.addHashEntry(newCat);
	}
	
	public void updateCategory(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		long catId = Long.parseLong(actionRequest.getParameter("catId"));
		
		Categoria newCat = CategoriaLocalServiceUtil.getCategoria(catId);
    	newCat.setNomeCategoria(actionRequest.getParameter("newCategoryName"));
    	newCat.setSupports(actionRequest.getParameter("newCategorySupports"));
    	_log.info("##UPDATE##\nCategory ["+catId+" - "+newCat.getNomeCategoria()+" - "+newCat.getSupports()+"]");
    	
    	CategoriaLocalServiceUtil.updateCategoria(newCat);
    	Category.removeHashEntry(catId);
    	Category.addHashEntry(newCat);
    	
	}
	
	public void deleteCategory(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		Category.removeHashEntry(Long.parseLong(actionRequest.getParameter("catId")));
    	
    	_log.info("##DELETE##\nCategory ["+actionRequest.getParameter("catId")+"]");
    	CategoriaLocalServiceUtil.deleteCategoria(Long.parseLong(actionRequest.getParameter("catId")));
	}

	public void setProperty(ActionRequest request, ActionResponse response) throws Exception{
		
		MarketplaceConf conf = null;
		long confId = -1L;
		
		String sConfId = ParamUtil.getString(request, "confId", null);
		String key = ParamUtil.getString(request, "key", null);
		String value = ParamUtil.getString(request, "value", null);
		String type = ParamUtil.getString(request, "type", null);
		String options = ParamUtil.getString(request, "options", null);
		
		if(Validator.isNotNull(sConfId)){
			confId = Long.parseLong(sConfId);
			_log.info("Updating property [#"+confId+"] " + key + " on Database.");
			conf = MarketplaceConfLocalServiceUtil.getMarketplaceConf(confId);
		}
		else{
			confId = CounterLocalServiceUtil.increment(MarketplaceConf.class.getName());
			_log.info("Creating property [#"+confId+"] " + key + " on Database.");
			conf = MarketplaceConfLocalServiceUtil.createMarketplaceConf(confId);
		}
		
		conf.setKey(key);
		conf.setValue(value);
		conf.setOptions(options);
		conf.setType(type);
		MarketplaceConfLocalServiceUtil.updateMarketplaceConf(conf);
		
	}
	
	public void deleteProperty(ActionRequest request, ActionResponse response) throws Exception{
		String sConfId = ParamUtil.getString(request, "confId", null);
		
		if(Validator.isNotNull(sConfId)){
			MarketplaceConf conf = MarketplaceConfLocalServiceUtil.deleteMarketplaceConf(Long.parseLong(sConfId));
			_log.info("Deleted Property [#" + conf.getConfId() + "] "+ conf.getKey() + ": " + conf.getValue() + " ");
		}
	}
	
	public void cleanup(ActionRequest request, ActionResponse response) throws Exception{
		
		Criterion c_total = null;
		
		String id = request.getParameter("categoryid");
		if(id!=null && !id.trim().equals("")){
			long catid = Long.parseLong(id);
			if(catid!=-1){
				c_total = RestrictionsFactoryUtil.eq("categoriamkpId", catid);
			}
		}
		
		String status = request.getParameter("status");
		if(status!=null && !status.trim().equals("")){
			int nStatus = Integer.parseInt(status);
			if(nStatus!=-1){
				if(c_total == null){
					c_total = RestrictionsFactoryUtil.eq("status", nStatus);
				}
				else{
					Criterion c_tmp =  RestrictionsFactoryUtil.eq("status", nStatus);
					c_total = RestrictionsFactoryUtil.and(c_tmp, c_total);
				}
			}
		}
		
		if(c_total!=null){
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(Artefatto.class).add(c_total);
			@SuppressWarnings("unchecked")
			List<Artefatto> arts = ArtefattoLocalServiceUtil.dynamicQuery(query);
			
			for(Artefatto a:arts){
				System.out.println("Deleting ["+a.getArtefattoId()+"]"+a.getTitle());
				ArtefattoLocalServiceUtil.deleteArtefatto(a);
			}
		}
	}
	
	public void cleandatasetsbyid(ActionRequest request, ActionResponse response) throws Exception{
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		
		File listfile = uploadPortletRequest.getFile("titlelistfile");
		String thisLine;
		
		 //Open the file for reading
	     try {
	    	 BufferedReader br = new BufferedReader(new FileReader(listfile));
	    	 while ((thisLine = br.readLine()) != null) {
	    		 try{
		    		System.out.println("Deleting dataset: "+thisLine);
		    		Artefatto thisart = ArtefattoLocalServiceUtil.getArtefactByExternalId(thisLine.trim().toLowerCase());
		    		ArtefattoLocalServiceUtil.deleteArtefatto(thisart);
	    		 }
	    		 catch(Exception e){
	    			 System.out.println(e); 
	    		 }
	    	 }
	    	 br.close();
	     }
	     catch (Exception e) {
	       System.err.println("Error: " + e);
	     }
	}

	public void publishDataset(ActionRequest request, ActionResponse response){
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		
		File listfile = uploadPortletRequest.getFile("models");
		String thisLine;
		
		 //Open the file for reading
	     try {
	    	 BufferedReader br = new BufferedReader(new FileReader(listfile));
	    	 while ((thisLine = br.readLine()) != null) {
	    		 try{
		    		System.out.println("Publishing dataset: "+thisLine);
		    		Artefatto thisart = ArtefattoLocalServiceUtil.getArtefactByExternalId(thisLine.trim().toLowerCase());
		    		ArtefattoLocalServiceUtil.deleteArtefatto(thisart);
	    		 }
	    		 catch(Exception e){
	    			 System.out.println(e); 
	    		 }
	    	 }
	    	 br.close();
	     }
	     catch (Exception e) {
	       System.err.println("Error: " + e);
	     }
	}
}
