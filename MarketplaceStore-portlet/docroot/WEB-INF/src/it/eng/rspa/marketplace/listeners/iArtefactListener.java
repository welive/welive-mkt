package it.eng.rspa.marketplace.listeners;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

public interface iArtefactListener {

	/** Invocato alla pubblicazione di un artefatto **/
	public void onArtefactPublish(Artefatto a);
	
	/** Invocato alla cancellazione di un artefatto **/
	public void onArtefactDelete(Artefatto a);
	
	/** Invocato alla modifica di un artefatto **/
	public void onArtefactUpdate(Artefatto a);
	
}
