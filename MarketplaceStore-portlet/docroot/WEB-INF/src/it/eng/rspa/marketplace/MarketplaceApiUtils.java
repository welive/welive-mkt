package it.eng.rspa.marketplace;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.marketplace.utils.MyConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;
import com.liferay.portlet.ratings.model.RatingsStats;
import com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public class MarketplaceApiUtils {

	private static Log _log = LogFactoryUtil.getLog(MarketplaceApiUtils.class);
	
	private static Set<String> psaCategories = new HashSet<String>();
	private static Set<String> bbCategories = new HashSet<String>();
	
	static{
		String[] psaCats = PortletProps.get("Category.catsOfType.psa").split(",");
		String[] bbCats = PortletProps.get("Category.catsOfType.bblocks").split(",");
		psaCategories.addAll(Arrays.asList(psaCats));
		bbCategories.addAll(Arrays.asList(bbCats));
	}
	
	public static List<Artefatto> getArtefatti(long cat){
		try {
			return ArtefattoLocalServiceUtil.findArtefattoByCategory_Status(cat, WorkflowConstants.STATUS_APPROVED);
		} catch (Exception e) {
			_log.warn(e.getMessage());
			return new ArrayList<Artefatto>();
		}
	}
	
	public static List<Artefatto> getArtefatti(String pilotId, long cat){
		
		List<Artefatto> out = new ArrayList<Artefatto>();
		
		if(pilotId==null || pilotId.equalsIgnoreCase("") || pilotId.equalsIgnoreCase("all")){
			try {
				return ArtefattoLocalServiceUtil.findArtefattoByCategory_Status(cat, WorkflowConstants.STATUS_APPROVED);
			} catch (Exception e) {
				_log.warn(e.getMessage());
				return new ArrayList<Artefatto>();
			}
		}
		else{
			try{
				List<Artefatto> list = ArtefattoLocalServiceUtil.getArtefattiByPilotAndStatus(pilotId, WorkflowConstants.STATUS_APPROVED);
				for(Artefatto a:list){
					try{ if(a.getCategoriamkpId() == cat) out.add(a); }
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			return out;
		}
	}
	
	public static List<Artefatto> getArtefatti(String pilotId, long cat, int artefactLevel) {
		List<Artefatto> out = new ArrayList<Artefatto>();
		boolean found;
		if(pilotId==null || pilotId.equalsIgnoreCase("") || pilotId.equalsIgnoreCase("all")){
			try {
				List<ArtifactLevels> matchingArts = ArtifactLevelsLocalServiceUtil.getArtifactLevelses(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
				out = ArtefattoLocalServiceUtil.findArtefattoByCategory_Status(cat, WorkflowConstants.STATUS_APPROVED);
				for(Artefatto a : out) {
					found = false;
					for(ArtifactLevels l : matchingArts) {
						if(l.getArtifactId() == a.getArtefattoId()) found = true;
					}
					if(!found) out.remove(a);
				}
			} catch (Exception e) {
				_log.warn(e.getMessage());
				return new ArrayList<Artefatto>();
			}
		}
		else{
			try{
				List<Artefatto> list = ArtefattoLocalServiceUtil.getArtefattiByPilotAndStatus(pilotId, WorkflowConstants.STATUS_APPROVED);
				for(Artefatto a:list){
					try{ if(a.getCategoriamkpId() == cat) out.add(a); }
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
						
		}
		
		return out;
	}
	
	public static JSONObject constructResponse(Artefatto a){
		
		if(a==null){
			_log.warn("No Artefact is specified");
			return null;
		}
		
		try {
				double score = 0;
				try{
					RatingsStats rs = RatingsStatsLocalServiceUtil.getStats(Artefatto.class.getName(), a.getArtefattoId());
					score = rs.getAverageScore();
				}
				catch(Exception e){
					_log.warn(e.getMessage());
					score = 0;
				}
				
				List<MBMessage> comments = MBMessageLocalServiceUtil.getMessages(Artefatto.class.getName(), a.getArtefattoId(), 0);
				JSONArray messages = JSONFactoryUtil.createJSONArray();
				for(MBMessage message:comments){
					if(message.getParentMessageId()!=0){
						JSONObject obj = JSONFactoryUtil.createJSONObject();
						obj.put("text", message.getBody());
						User user = UserLocalServiceUtil.getUser(message.getUserId());
						String username = "Unknown";
						if(user!=null){ 
							username = user.getFullName(); 
						}
						
						obj.put("author", username);
						obj.put("creation_date", message.getCreateDate());
						
						messages.put(obj);
					}
				}
				
				String catName = Category.getMapById().get(a.getCategoriamkpId()).getNomeCategoria();
				
				String[] tags = AssetTagLocalServiceUtil.getTagNames(Artefatto.class.getName(), a.getArtefattoId());
				JSONArray tagsArr = JSONFactoryUtil.createJSONArray();
				for(String t:tags){
					tagsArr.put(t);
				}
				
				JSONObject jlusdl = null;
				if(Validator.isNotNull(a.getLusdlmodel())){
					try{
						String slusdl = a.getLusdlmodel();
						jlusdl = JSONFactoryUtil.createJSONObject(slusdl);
					}
					catch(Exception e){
						System.out.println(e);
					}
				}
				
				JSONObject obj = JSONFactoryUtil.createJSONObject();
							obj.put("name", a.getTitle());
							obj.put("description", a.getAbstractDescription());
							obj.put("interfaceOperation", a.getInteractionPoint());
							obj.put("type", catName);
							obj.put("typeId", a.getCategoriamkpId());
							obj.put("rating", score);
							obj.put("artefactId", a.getArtefattoId());
							obj.put("eId", a.getEId());
							obj.put("tags", tagsArr);
							obj.put("comments", messages);
							obj.put("hasmapping", a.getHasMapping());
							
							if(Validator.isNotNull(jlusdl)){
								jlusdl = fixArtefatto(jlusdl); //TODO Rimuovere a regime WORKAROUND
								obj.put("metamodel", jlusdl);
							}

				
				if(psaCategories.contains(catName)){
					obj.put("url", a.getUrl());
				}
				
				if(bbCategories.contains(catName)){
//					Crud c = new Crud();
//					
//					ByteArrayOutputStream os = c.exportRDFOutputStream(a.getResourceRDF());	
//					ReaderRDF r = new ReaderRDF(os);
//					Set<SecurityMeasure> secMeas = RDFUtils.getSecurities(r);
					
					JSONObject authentication = JSONFactoryUtil.createJSONObject();
					authentication.put("protected", false);
					authentication.put("securities", JSONFactoryUtil.createJSONArray());
					
//					for(SecurityMeasure sm : secMeas){
//						if(sm instanceof AuthenticationMeasure){
//							authentication.put("protected", true);
//							
//							JSONObject tmp = JSONFactoryUtil.createJSONObject();
//							AuthenticationMeasure am = (AuthenticationMeasure)sm;
//							tmp.put("title", am.getTitle().getValue());
//							tmp.put("description", am.getDescription().getValue());
//							
//							Field<Set<Permission>> scopes = am.getRequires();
//							
//							JSONArray scopesArray = JSONFactoryUtil.createJSONArray();
//							Iterator<Permission> perms = scopes.getValue().iterator();
//							while(perms.hasNext()){
//								scopesArray.put(perms.next().getIdentifier().getValue());
//							}
//							
//							tmp.put("scopes",scopesArray);
//							
//							authentication.getJSONArray("securities").put(tmp);
//						}
//					}
					obj.put("authentication", authentication);
				}
							
				ImmagineArt coverimg = null;
				DLFileEntry dlf = null;
		
				String linkimg = "";
				try { 
					if(a.getImgId()>0){
						coverimg=ImmagineArtLocalServiceUtil.getImmagineArt(a.getImgId());
					}
					else if (ImmagineArtLocalServiceUtil.findByArtefattoId(a.getArtefattoId()).size() > 0) {
						coverimg = ImmagineArtLocalServiceUtil.findByArtefattoId(a.getArtefattoId()).get(0);
					}
					
					if(coverimg != null){
						dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(coverimg.getDlImageId()); 
						linkimg = "/documents/"+MyConstants.API_DL_ID+"/"+dlf.getUuid();
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				finally{
					obj.put("linkImage",linkimg);
				}
				
				return obj;
					
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return null;
	}
	
	private static JSONObject fixArtefatto(JSONObject jlusdl) {
		
		JSONObject parsedDescriptor = JSONFactoryUtil.createJSONObject();
		try {
			parsedDescriptor = JSONFactoryUtil.createJSONObject(jlusdl.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			return parsedDescriptor;			
		}
		
		JSONArray fixedOperation =JSONFactoryUtil.createJSONArray();
		JSONArray opArray = parsedDescriptor.getJSONArray("hasOperation");
		if(!Validator.isNull(opArray)) {
			int size = opArray.length();
			for(int i=0;i<size;i++) {
				//Prendo l'operation
				JSONObject op = opArray.getJSONObject(i);
				//Prendo il produces dell'operation
				JSONArray produces = op.getJSONArray("produces");
				
				//Se non è un array
				if(Validator.isNull(produces)) {
					String p = op.getString("produces");
					JSONArray producesArray = JSONFactoryUtil.createJSONArray();
					
					//Creo un array a partire dalla stringa
					if(p.trim().length() == 0) p = "text";
					producesArray.put(p);
					
					//Aggiorno l'operation
					op.put("produces", producesArray);
				}
				
				//Salvo l'operation modificata
				fixedOperation.put(op);
			}
			
			parsedDescriptor.put("hasOperation",fixedOperation);
		} 
		
		return parsedDescriptor;

	}

	public static boolean matchKeyword(Artefatto a, String keyword) {
		try {
			String title = a.getTitle().toLowerCase();
			String abstractDescription = a.getAbstractDescription().toLowerCase();
			
			return title.contains(keyword.toLowerCase()) || abstractDescription.contains(keyword.toLowerCase()); 
		}
		catch(Exception e) {
			return false;
		}
	}
	
	public static Long getUserIdByCcUserId(String ccUserId){
		Long userId = null;
		try{
			long columnId = ExpandoColumnLocalServiceUtil.getColumn(MyMarketplaceUtility.getDefaultCompanyId(), User.class.getName(), "CUSTOM_FIELDS", "CCUserID").getColumnId();
			List<ExpandoValue> values = ExpandoValueLocalServiceUtil.getColumnValues(columnId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for(ExpandoValue v:values){
				if(v.getData().equals(ccUserId)){
					userId = v.getClassPK();
					break;
				}
			}
		}
		catch(Exception e){ System.out.println(e.getMessage()); }
		
		return userId;
	}
	
	public static void logError(Exception e){
		_log.error(e.getMessage());
	}
	
	public static void logWarning(Exception e){
		_log.warn(e.getMessage());
	}
	
	public static void logInfo(Exception e){
		_log.info(e.getMessage());
	}

	public static JSONObject constructResponse_v2(Artefatto a) {
		if(a==null){
			_log.warn("No Artefact is specified");
			return null;
		}
		
		try {
				double score = 0;
				try{
					RatingsStats rs = RatingsStatsLocalServiceUtil.getStats(Artefatto.class.getName(), a.getArtefattoId());
					score = rs.getAverageScore();
				}
				catch(Exception e){
					_log.warn(e.getMessage());
					score = 0;
				}
				
				List<MBMessage> comments = MBMessageLocalServiceUtil.getMessages(Artefatto.class.getName(), a.getArtefattoId(), 0);
				JSONArray messages = JSONFactoryUtil.createJSONArray();
				for(MBMessage message:comments){
					if(message.getParentMessageId()!=0){
						JSONObject obj = JSONFactoryUtil.createJSONObject();
						obj.put("text", message.getBody());
						User user = UserLocalServiceUtil.getUser(message.getUserId());
						String username = "Unknown";
						if(user!=null){ 
							username = user.getFullName(); 
						}
						
						obj.put("author", username);
						obj.put("creation_date", message.getCreateDate());
						
						messages.put(obj);
					}
				}
				
				JSONObject obj = JSONFactoryUtil.createJSONObject();
							obj.put("typeId", a.getCategoriamkpId());
							obj.put("rating", score);
							obj.put("artefactId", a.getArtefattoId());
							obj.put("eId", a.getEId());
							obj.put("comments", messages);

				
				ImmagineArt coverimg = null;
				DLFileEntry dlf = null;
		
				try { 
					if(a.getImgId()>0){		
						coverimg=ImmagineArtLocalServiceUtil.getImmagineArt(a.getImgId());
					}else if (ImmagineArtLocalServiceUtil.findByArtefattoId(a.getArtefattoId()).size() > 0) {
						coverimg = ImmagineArtLocalServiceUtil.findByArtefattoId(a.getArtefattoId()).get(0);
					}
					dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(coverimg.getDlImageId()); 
					obj.put("linkImage","/documents/"+MyConstants.API_DL_ID+"/"+dlf.getUuid());
				} 
				catch (Exception e) { 
					obj.put("linkImage","");
					_log.warn("Setting null image");
				}
  
				try{
					//Add the metamodel
					JSONObject meta = JSONFactoryUtil.createJSONObject(a.getLusdlmodel());
					obj.put("metamodel", meta);
				}
				catch(Exception e){
					System.out.println("Error creating json \n\t"+a.getLusdlmodel());
					throw e;
				}
				
				return obj;
					
		} 
		catch (Exception e) { 
			e.printStackTrace();
		}
		
		return null;
	}

	
	
}
