package it.eng.rspa.marketplace.listeners;

import it.eng.rspa.cdv.datamodel.model.UsedApplication;
import it.eng.rspa.cdv.datamodel.service.UsedApplicationLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.util.List;

public class CDVNotifier implements iArtefactListener {

	@Override
	public void onArtefactPublish(Artefatto a) {
		//DO nothing
		return;
	}

	@Override
	public void onArtefactDelete(Artefatto a) {
		long artid = a.getArtefattoId();
		

		List<UsedApplication> apps = UsedApplicationLocalServiceUtil.getUsedApplicationByAppId(artid);

		for(UsedApplication app : apps){
			try{ 
				UsedApplicationLocalServiceUtil.deleteUsedApplication(app);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
		return;
		
	}

	@Override
	public void onArtefactUpdate(Artefatto a) {
		//DO nothing
		return;
	}

}
