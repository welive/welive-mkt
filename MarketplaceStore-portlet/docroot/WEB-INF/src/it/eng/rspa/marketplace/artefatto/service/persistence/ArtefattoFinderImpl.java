package it.eng.rspa.marketplace.artefatto.service.persistence;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.sesame.Crud;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.RepositoryException;

import com.liferay.portal.kernel.exception.SystemException;

public class ArtefattoFinderImpl extends ArtefattoPersistenceImpl implements ArtefattoFinder  {

	/**
	 * Ricerca il filtro nel repository RDF e restituisce l'elenco dei nomi dei modelli RDF che contengono la descrizione 
	 * tra gli elementi di tipo dcterms:title
	 * Nota: il nome della risorsa � univoco nel repository RDF, per questo motivo la lista non pu� contenere duplicati.
	 * String filtro="*"+description+"*";
	 * SELECT DISTINCT c FROM CONTEXT c {something} dcterms:title {t} WHERE  t Like \""+filtro+"\" IGNORE CASE ";
	 *   @param description termini da ricercare
	 *   @return lista dei nomi dei modelli RDF che soddisfano la ricerca 
	 */
	public List<Artefatto> findByDescriptionRDF(String description) throws  SystemException{
		List<Artefatto> out=new ArrayList<Artefatto>();

		if (description.equals("")){
			return out;
		}
		
		try {
			Crud c=new Crud();
//			String filtro="*"+description+"*";
			
			String [] filtroTokenized =description.split(" ");			
			//			String queryString  = "SELECT DISTINCT c,something, t FROM CONTEXT c {something} dcterms:title {t} WHERE  t Like \""+filtro+"\" IGNORE CASE ";
			
			String queryString  = "SELECT DISTINCT c FROM CONTEXT c {something} dcterms:description{t} WHERE ( ";
			queryString=queryString+ " ( t Like \"*"+filtroTokenized[0]+"*\" IGNORE CASE) ";
			for (int i=1;i<filtroTokenized.length;i++){
				queryString=queryString+ " or ( t Like \"*"+filtroTokenized[i]+"*\" IGNORE CASE) ";
			}
			queryString=queryString+ "  ) ";
			
			List<String> risultati = c.evaluateQuery(QueryLanguage.SERQL,queryString );
			
			System.out.println(queryString+ " produced "+risultati.size()+" results");
			out= getArtefatti(risultati);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		// System.out.println("\nArtefattoFinderImpl.getArtefattoByDescriptionRDF: "+description+" count:"+out.size());

		return out;
	} 

	
	/**
	 * Crea la lista degli artefatti corrispondenti alla lista delle resourceRDF.
	 * @param elenco dei nomi delle resourceRDF
	 * @return lista di modelli di tipo {@link Artefatto}
	 */
	public List<Artefatto> getArtefatti(List<String> listaResourceRDF) {
		List<Artefatto> out=new ArrayList<Artefatto>();
		List<Artefatto>lista=new ArrayList<Artefatto>();
		Artefatto a=null;
		for (Iterator<String> iterator = listaResourceRDF.iterator(); iterator.hasNext();) {
			String stringaID = (String) iterator.next(); 
			try {
				//System.out.println("findByResourceRDF("+stringaID);
				lista= findByResourceRDF(stringaID); 
				if (lista.size()>0){
					a=lista.get(0);
					out.add(a);
				}				
			} catch ( Exception   e) { 
				e.printStackTrace();
			}
		}

		return out;
	}


}
