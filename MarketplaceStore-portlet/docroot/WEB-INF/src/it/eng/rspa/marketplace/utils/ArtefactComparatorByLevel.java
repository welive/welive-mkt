package it.eng.rspa.marketplace.utils;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;

import java.util.Comparator;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class ArtefactComparatorByLevel implements Comparator<Artefatto> {
		
	@Override
	public int compare(Artefatto art1, Artefatto art2) {
		
		int level1=0;
		
		try {
			ArtifactLevels aLevel1 = ArtifactLevelsLocalServiceUtil.getArtifactLevels(art1.getArtefattoId());
			level1 = aLevel1.getLevel();
		} catch (PortalException | SystemException e) {
			
			//System.out.println("No Level for ArtifactId "+art1.getArtefattoId());
		}
		
		int level2=0;
		
		try {
			ArtifactLevels aLevel2 = ArtifactLevelsLocalServiceUtil.getArtifactLevels(art1.getArtefattoId());
			level2 = aLevel2.getLevel();
		} catch (PortalException | SystemException e) {
			
			//System.out.println("No Level for ArtifactId "+art2.getArtefattoId());
		}
		
		if (level1 == 0 &&  level2 == 0)
			return 0;
		
		if (level1 == 0 || (level2 > level1))
			return 1;
		
		if (level1 > level2)
			return -1;
		
		return 0;
		

	}
}
