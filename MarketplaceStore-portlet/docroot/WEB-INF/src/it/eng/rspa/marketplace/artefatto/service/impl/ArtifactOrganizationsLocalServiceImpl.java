/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsImpl;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.base.ArtifactOrganizationsLocalServiceBaseImpl;

/**
 * The implementation of the artifact organizations local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.ArtifactOrganizationsLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil
 */
public class ArtifactOrganizationsLocalServiceImpl
	extends ArtifactOrganizationsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil} to access the artifact organizations local service.
	 */
	
	private static Log _log = LogFactoryUtil.getLog(ArtifactOrganizations.class);
	
	public List<Artefatto>findArtefattoByOrganizationId_Status(long organizationId, int status)
			throws PortalException, SystemException, RemoteException {
		List<Artefatto> out=new ArrayList<Artefatto>();
		 
		Iterator<ArtifactOrganizations> it = getArtifactOrganizationsPersistence().findByOrganizationId_Status(organizationId, status).iterator();

		while(it.hasNext()){
			out.add(ArtefattoLocalServiceUtil.getArtefatto(it.next().getArtifactId()));
		}
		//System.out.println("\findArtefattoByCompanyId_Status companyId:"+companyId+" status:"+status+" size:"+out.size());
		return out;
	}
	
	public List<ArtifactOrganizations> findArtifactOrganizationsByArtifactId(long artifactId){
		try{ return getArtifactOrganizationsPersistence().findByArtifactId(artifactId); }
		catch(Exception e){
			return new ArrayList<ArtifactOrganizations>();
		}
	}
	
	public List<ArtifactOrganizations> findArtifactOrganizationsByOrganizationId(long orgId) 
			throws SystemException{
		return getArtifactOrganizationsPersistence().findByOrganizationId(orgId);
	}
	
	public ArtifactOrganizations addArtifactOrganizations(long artifactId, long orgId, int status) 
			throws SystemException{
		ArtifactOrganizations ao = new ArtifactOrganizationsImpl();
		ao.setArtifactId(artifactId);
		ao.setOrganizationId(orgId);
		ao.setStatus(status);
		
		ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations(ao);
		
		return ao;
		
	}
	
	public void deleteArtifactOrganizations(long artifactId){
		Iterator<ArtifactOrganizations> it;
		try {
			it = getArtifactOrganizationsPersistence().findByArtifactId(artifactId).iterator();
		} catch (SystemException e) {
			return;
		}
		while(it.hasNext()){
			try {
				artifactOrganizationsPersistence.remove(it.next());
			} catch (SystemException e) {
				_log.warn("Unable to remove ArtifactOrganizations' item");
			}
		}
	}
	
	
}