/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;
import com.sun.xml.internal.messaging.saaj.util.Base64;

import it.eng.metamodel.definitions.Artefact;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dataset;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MarketplaceApiUtils;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.base.SearchServiceBaseImpl;
import it.eng.rspa.marketplace.category.Category;
import it.eng.rspa.usdl.model.BusinessRole;
import it.eng.rspa.usdl.utils.EditorUtils;
import it.eng.rspa.wizard.WizardUtils;
import it.eng.rspa.wizard.utils.ArtifactEvaluationUtils;
import it.eng.rspa.wizard.utils.ArtifactValutationUtils;

/**
 * The implementation of the search remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.SearchService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.SearchServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil
 */
public class SearchServiceImpl extends SearchServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil} to access the search remote service.
	 */
	
	private static Log _log = LogFactoryUtil.getLog(SearchServiceImpl.class);
	
	private boolean isValidUser(User requirer){
		String authorized = ConfUtil.getString("api.user.email");
		return requirer.getEmailAddress().equals(authorized);
	}
	
	private String getRdfAsString(String resourceRdf) throws IOException {
		_log.debug("Getting RDF from "+resourceRdf);
		URL url = new URL(resourceRdf);
		URLConnection urlConnection = url.openConnection();
		InputStream in = new BufferedInputStream(urlConnection.getInputStream());
		
		StringWriter writer = new StringWriter();
		IOUtils.copy(in, writer, "UTF-8");
		return writer.toString();
	}
	
	private String myURLDecode(String s, boolean base64Encoded) throws UnsupportedEncodingException{
		
		if(base64Encoded){
			String step1 = s.replaceAll("--fs--", "/");
			
			return new String(Base64.base64Decode(step1)); 
		}
		else{ 
			String step1 = URLDecoder.decode(s, "UTF-8");
			return step1.replaceAll("--fs--", "/");
		}
	}
	
	@JSONWebService(value="/mkp/get-all-artefacts")
	public JSONObject getAllArtefatti(String pilotID, String artefactTypes){
		return getAllArtefatti(pilotID,artefactTypes,"1");
	}

	
	@JSONWebService(value="/mkp/get-all-artefacts")
	public JSONObject getAllArtefatti(String pilotID, String artefactTypes, String level){
		
		JSONObject outwrapper = JSONFactoryUtil.createJSONObject();
		JSONArray out = JSONFactoryUtil.createJSONArray();
		String tmp = artefactTypes;
		int artLevel;
		
		try {
			artLevel = Integer.parseInt(level);
		} catch(NumberFormatException e) {
			_log.warn(e.getMessage());
			artLevel = 1;
		}
		
		
		if(artLevel < 1 || artLevel > 5) {
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "Invalid level");
			return out1;
		}
		
		if(artefactTypes==null || artefactTypes.trim().equals("")){
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "ArtefactType is mandatory");
			return out1;
		}
		if(tmp.trim().equalsIgnoreCase("All")){
			tmp = "BuildingBlock,Dataset,PSA";
		}
		else if(tmp.trim().equalsIgnoreCase("vc-all")){
			tmp = "BuildingBlock,Dataset";
//			isFromVC = true;
		}
		
		String[] types = tmp.split(",");

		
		for(String type : types){
			String label = "bblocks";
			if(type.equalsIgnoreCase("dataset")){
				label="ds";
			}
			else if(type.equalsIgnoreCase("buildingblock")){
				label="bblocks";
			}
			else if(type.equalsIgnoreCase("psa")){
				label="psa";
			}
			
			String[] subtypes = PortletProps.get("Category.catsOfType."+label).split(",");
			
			for(String st:subtypes){
				
				List<Artefatto>  list = new ArrayList<Artefatto>();
				//TODO Level filter
				pilotID = ignoreCase(pilotID);
				if("bblocks".compareTo(label)==0) {
					Criterion criteria;
										
					Criterion c_category = RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get(st).getIdCategoria());
					Criterion c_status = RestrictionsFactoryUtil.eq("status", WorkflowConstants.STATUS_APPROVED);
					criteria = RestrictionsFactoryUtil.and(c_category,c_status);
					if(!"ALL".equalsIgnoreCase(pilotID)) {
						Criterion c_pilot = RestrictionsFactoryUtil.eq("pilotid", pilotID);
						if(!"none".equalsIgnoreCase(pilotID)) {
							Criterion c_tmp = RestrictionsFactoryUtil.eq("pilotid", "ALL");
							c_pilot = RestrictionsFactoryUtil.or(c_pilot, c_tmp);
						}
						criteria = RestrictionsFactoryUtil.and(c_pilot, criteria);
					}
					
					
					List<Long> idList = ArtifactLevelsLocalServiceUtil.getArtifactLevelsByLevel(artLevel);
					list = MyMarketplaceUtility.getArtefactListByIdList(idList, criteria);
				} 
				else {
					list = MarketplaceApiUtils.getArtefatti(pilotID, Category.getMapByName().get(st).getIdCategoria());
				}
				//______________________+

				Iterator<Artefatto> it = list.iterator();
				
				while(it.hasNext()){
					out.put(MarketplaceApiUtils.constructResponse(it.next()));					
				}
			}
		}
		outwrapper.put("artefacts", out);
		//outwrapper.put("count", out.length());
		return outwrapper;
	}
	
	private String ignoreCase(String pilotID) {
		if(pilotID.equalsIgnoreCase("Trento")) pilotID = "Trento";
		else if(pilotID.equalsIgnoreCase("Bilbao")) pilotID = "Bilbao";
		else if(pilotID.equalsIgnoreCase("Novisad")) pilotID = "Novisad";
		else if(pilotID.equalsIgnoreCase("Uusimaa")) pilotID = "Uusimaa";
		else if(pilotID.equalsIgnoreCase("All")) pilotID = "ALL";
		else pilotID = "none";
		return pilotID;
	}

	@JSONWebService(value="/v2/get-all-artefacts")
	public JSONObject getAllArtefattiV2(String pilotID, String artefactTypes){
		_log.debug("Get all artefacts v2 request ");
		
		//It provides all the artefacts having a metamodel associated
		JSONObject out = JSONFactoryUtil.createJSONObject();
		out.put("artefacts", JSONFactoryUtil.createJSONArray());
		
		try{ 
			JSONArray arr = JSONFactoryUtil.createJSONArray();
			List<Artefatto> arts = artefattoPersistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			for(Artefatto art : arts){
				if(Validator.isNotNull(art.getLusdlmodel()) 
						&& !art.getLusdlmodel().trim().equalsIgnoreCase("") 
						&& art.getStatus()==WorkflowConstants.STATUS_APPROVED){
					
					JSONObject tmp = MarketplaceApiUtils.constructResponse_v2(art);
					
					arr.put(tmp);
				
				}
			}
			
			out.put("artefacts", arr);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return out;
	}
	
	@JSONWebService(value="/mkp/get-artefacts-by-keywords")
	public JSONObject getArtefattiByKeywords(String keywords, String artefactTypes, String pilot) throws Exception{
		return getArtefattiByKeywords(keywords, artefactTypes, pilot, "1");
	}
	
	@JSONWebService(value="/mkp/get-artefacts-by-keywords")
	public JSONObject getArtefattiByKeywords(String keywords, String artefactTypes, String pilot, String level)
			 throws Exception {
		
		JSONObject outwrapper = JSONFactoryUtil.createJSONObject();
		JSONArray out = JSONFactoryUtil.createJSONArray();
		
		int artLevel;
		
		try {
			artLevel = Integer.parseInt(level);
		} catch(NumberFormatException e) {
			_log.warn(e.getMessage());
			artLevel = 1;
		}
		
		
		if(artLevel < 1 || artLevel > 5) {
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "Invalid level");
			return out1;
		}
		
		
		HashSet<JSONObject> tmp = new HashSet<JSONObject>();
		if(keywords==null || keywords.trim().equals("")){
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "Field 'keywords' is mandatory");
			return out1;
		}
		
		if(artefactTypes==null || artefactTypes.trim().equals("")){
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "Field 'artefactTypes' is mandatory");
			return out1;
		}
		
		String tmp1 = artefactTypes;
		if(artefactTypes.equalsIgnoreCase("All")){
			tmp1 = "BuildingBlock,Dataset,PSA";
		}	
		else if(tmp1.trim().equalsIgnoreCase("vc-all")){
			tmp1 = "BuildingBlock,Dataset";
		}
		
		String[] types = tmp1.split(",");
		
		for(String type : types){
			String label = "bblocks";
			if(type.equalsIgnoreCase("dataset")){
				label="ds";
			}
			else if(type.equalsIgnoreCase("buildingblock")){
				label="bblocks";
			}
			else if(type.equalsIgnoreCase("psa")){
				label="psa";
			}
			String[] subtypes = PortletProps.get("Category.catsOfType."+label).split(",");
			String[] keys = keywords.split(",");
			
			for(String st:subtypes){
				Artefatto a = null;
				List<Artefatto> list = new ArrayList<Artefatto>();
				pilot = ignoreCase(pilot);
				if("bblocks".compareTo(label)==0) {		
					Criterion criteria;				
					Criterion c_category = RestrictionsFactoryUtil.eq("categoriamkpId", Category.getMapByName().get(st).getIdCategoria());
					Criterion c_status = RestrictionsFactoryUtil.eq("status", WorkflowConstants.STATUS_APPROVED);
					criteria = RestrictionsFactoryUtil.and(c_category,c_status);
					if(!"ALL".equalsIgnoreCase(pilot)) {
						Criterion c_pilot = RestrictionsFactoryUtil.eq("pilotid", pilot);								
						if(!"none".equalsIgnoreCase(pilot)) {
							Criterion c_tmp = RestrictionsFactoryUtil.eq("pilotid", "ALL");
							c_pilot = RestrictionsFactoryUtil.or(c_pilot, c_tmp);
						}
						criteria = RestrictionsFactoryUtil.and(c_pilot, criteria);
					}
					
					
					List<Long> idList = ArtifactLevelsLocalServiceUtil.getArtifactLevelsByLevel(artLevel);
					
					list = MyMarketplaceUtility.getArtefactListByIdList(idList, criteria);
				} else {
					list = MarketplaceApiUtils.getArtefatti(pilot, Category.getMapByName().get(st).getIdCategoria());
				}
				Iterator<Artefatto> it = list.iterator();
				
				while(it.hasNext()){
					a = it.next();
		
					for(String k:keys){
						if(MarketplaceApiUtils.matchKeyword(a,k)){
							tmp.add(MarketplaceApiUtils.constructResponse(a));
						}
					}
				}
				
				for(JSONObject json : tmp){
					out.put(json);
				}
				tmp = new HashSet<JSONObject>();
			}
		}
		outwrapper.put("artefacts", out);
		return outwrapper;
	}
	
	@JSONWebService(value="/v2/get-artefacts-by-keywords")
	public JSONObject getArtefattiByKeywordsV2(String keywords, String artefactTypes, String pilot)
			 throws PortalException, SystemException, RemoteException, Exception {
		return null;
	}
	
	@JSONWebService(value="/mkp/get-artefacts-by-ids")
	public JSONObject getModelsById(String[] ids)
			 throws Exception {
		
		JSONObject outwrapper = JSONFactoryUtil.createJSONObject();
		JSONArray out = JSONFactoryUtil.createJSONArray();
		
		if(ids==null){
			JSONObject out1 = JSONFactoryUtil.createJSONObject();
			out1.put("error", true);
			out1.put("message", "Field 'ids' is mandatory");
			return out1;
		}
		
		for(String s:ids){
			try{
				out.put(MarketplaceApiUtils.constructResponse_v2(ArtefattoLocalServiceUtil.getArtefatto(Long.parseLong(s))));
			}
			catch(Exception e){ MarketplaceApiUtils.logWarning(e); }
		}
		
		outwrapper.put("artefacts", out);
		return outwrapper;
	}
	
	@JSONWebService(value="/mkp/get-artefact")
	public JSONObject getArtefact(long artefactid) {
		
		Artefatto a = null;
		try{ 
			a = ArtefattoLocalServiceUtil.getArtefatto(artefactid);
			if(a.getStatus()!=WorkflowConstants.STATUS_APPROVED){ 
				return JSONFactoryUtil.createJSONObject();
			}
			else{ return MarketplaceApiUtils.constructResponse(a); }
		}
		catch(Exception e){ return JSONFactoryUtil.createJSONObject(); }
	}
	
	@JSONWebService(value="/v2/get-artefact")
	public JSONObject getArtefactV2(long artefactid) {
		try{
			Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(artefactid);
			return MarketplaceApiUtils.constructResponse_v2(a);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	@JSONWebService(value="/mkp/create-dataset")
	public JSONObject createDataset(String datasetId,
								String title, 
								String description, 
								String resourceRdf,
								String webpage,
								String ccUserId,
								String providerName,
								String language)
					throws Exception{
		
		try {
			User requirer = this.getUser();
			if(!isValidUser(requirer)){
				throw new SecurityException("User unauthorized");
			}
		} catch (Exception e) {
			JSONObject error = JSONFactoryUtil.createJSONObject();
			error.put("error", true);
			error.put("message", e.getClass()+"["+e.getMessage()+"]");
			_log.error("This user in unauthorized to publish dataset");
			return error;
		}
		JSONObject out = JSONFactoryUtil.createJSONObject();
		if(datasetId==null || datasetId.equals("")){
			out.put("error", true);
			out.put("message", "DatasetId is mandatory");
			_log.error("DatasetId is mandatory to publish dataset "+title);
			return out;
		}
		if(ccUserId==null || ccUserId.equals("")){
			out.put("error", true);
			out.put("message", "ccUserId is mandatory");
			_log.error("ccUserId is mandatory to publish dataset "+title);
			return out;
		}
		Long userId = MarketplaceApiUtils.getUserIdByCcUserId(ccUserId); 
		if(userId==null){
			out.put("error", true);
			out.put("message", "No user exists with the provided ccUserId");
			_log.error("No user exists with the provided ccUserId "+ccUserId);
			return out;
		}
		boolean isDeveloper = MyMarketplaceUtility.isDeveloper(userId);
		if(!isDeveloper){
			out.put("error", true);
			out.put("message", "User with ccuid "+ccUserId+" is unauthorized");
			_log.error("User with ccuid "+ccUserId+" is unauthorized");
			return out;
		}
		if(title==null || title.trim().equals("")){
			out.put("error", true);
			out.put("message", "Title is mandatory");
			_log.error("Title is mandatory to publish dataset");
			return out;
		}
		
		User user = UserLocalServiceUtil.getUser(userId);
		Artefatto a = artefattoLocalService.createArtefatto(counterLocalService.increment(Artefatto.class.getName()));
		String tit = myURLDecode(title, false);
		a.setTitle(tit);
		String descr = "";
		String abs = "";
		if(description!=null){
			descr = myURLDecode(description, false);
			//abs = descr.subSequence(0, Math.min(200, descr.length())).toString();
		}
		a.setDescription(descr);
		a.setAbstractDescription(descr);
		
		a.setEId(datasetId);
		
		//Imposto la categoria DATASET, se tale categoria non � censita sul portale imposto 
		//il codice-categoria a 0, che � inteso come ALTRO
		Categoria datasetCat = categoriaLocalService.getCategoriaByName(PortletProps.get("Category.dataset"));
		
		long datasetCatId = 0;
		if(datasetCat!=null) datasetCatId = datasetCat.getIdCategoria();
		a.setCategoriamkpId(datasetCatId);
		
		long groupId = user.getGroupId();
			a.setCompanyId(user.getCompanyId());
			a.setUserId(user.getUserId());
			a.setGroupId(groupId);

		String provider = "-";
		if(providerName!=null)
			provider = myURLDecode(providerName, false);

		a.setProviderName(provider);
		
		a.setOwner(user.getFullName().trim());
		
		a.setDate(GregorianCalendar.getInstance().getTime());
		a.setStatus(WorkflowConstants.STATUS_DRAFT);
		a.setInteractionPoint("");
		String wbpg = "";
		if(webpage!=null && !webpage.equals("")){
			wbpg = myURLDecode(webpage,true);
		}
		a.setWebpage(wbpg);
		
		
		a.setImgId(-1L);
		
		String pilotId = "";
		String[] pilots = ((String[])user.getExpandoBridge().getAttribute("pilot"));
		if(pilots!=null && pilots.length > 0) pilotId = pilots[0];
		a.setPilotid(pilotId);
		
		String artLanguage = "English";
		if(language!=null && !language.equals("")){
			artLanguage = myURLDecode(language,false);
		}
		else{
			String[] languages = (String[]) user.getExpandoBridge().getAttribute("languages");
			if(languages!=null && languages.length > 0){
				artLanguage = languages[0];
			}
		}
		a.setLanguage(artLanguage);
		
		Dataset metamodel = MyMarketplaceUtility.setupDatasetLusdl(tit, descr, wbpg, pilotId, user, providerName, artLanguage, getRdfAsString(myURLDecode(resourceRdf,true)));
		a.setLusdlmodel(metamodel.writeAsJsonString());
		
		ArtefattoLocalServiceUtil.addArtefatto(a);
		ArtefattoLocalServiceUtil.publishArtefatto(a.getArtefattoId());
		out.put("error", false);
		out.put("message", "Artefact created succesfully with id "+a.getArtefattoId());
		out.put("artefactid", a.getArtefattoId());
		
		_log.debug("Artefatto creato correttamente: "+out.toString());
		
		return out;
		
	}
	
	
	@JSONWebService(value="/mkp/create-service")
	public JSONObject createService(String title, 
								String description,
								String category,
								String resourceRdf,
								String endpoint,
								String webpage,
								String ccUserId,
								String providerName,
								String language) throws Exception{
		
		try {
			User requirer = this.getUser();
			if(!isValidUser(requirer)){
				throw new SecurityException("User unauthorized");
			}
		}
		catch (Exception e) {
			JSONObject error = JSONFactoryUtil.createJSONObject();
			error.put("error", true);
			error.put("message", e.getMessage());
			return error;
		}
		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		if(resourceRdf==null || resourceRdf.equals("")){
			out.put("error", true);
			out.put("message", "RDF information are mandatory");
			_log.error("RDF information are mandatory");
			return out;
		}
		
		if(ccUserId==null || ccUserId.equals("")){
			out.put("error", true);
			out.put("message", "ccUserId is mandatory");
			_log.error("ccUserId is mandatory");
			return out;
		}
		
		Long userId = MarketplaceApiUtils.getUserIdByCcUserId(ccUserId); 
		if(userId==null){
			out.put("error", true);
			out.put("message", "No user exists with the provided ccUserId");
			_log.error("No user exists with the provided ccUserId");
			return out;
		}
		boolean isDeveloper = MyMarketplaceUtility.isDeveloper(userId);
		if(!isDeveloper){
			out.put("error", true);
			out.put("message", "User with ccuid "+ccUserId+" is unauthorized");
			_log.error("User with ccuid "+ccUserId+" is unauthorized");
			return out;
		}		

		String cat = null;
		if(category!=null && category.trim().length() > 0 ) {
			String catL = category.toLowerCase();
			if( "rest".compareTo(catL)== 0) cat = "wadl";
			else if("soap".compareTo(catL)== 0 ) cat = "wsdl";
			else if ("wadl".equals(catL) || "wsdl".equals(catL) || "swagger".equals(catL)) cat = catL;
			else {
				out.put("error", true);
				out.put("message", "Category is not valid!");
				_log.error("Category is not valid!");
				return out;
			}
		}
		
		String wadl = null;
		if(endpoint!=null && endpoint.trim().length()>0)
			wadl = myURLDecode(endpoint,true);
		
		String urlDoc = null;
		if(webpage!=null && webpage.trim().length()>0)
			urlDoc = myURLDecode(webpage,true);
		//out = MyMarketplaceUtility.validationWadl(wadl, cat); //VALIDATION
		
		//----Building Block Object fill-----
		BuildingBlock bb = new BuildingBlock();
		//If was inserted a descriptor, Validate it!
		if(wadl!=null) {
			ParserResponse response = MyMarketplaceUtility.validateOperationWadl(wadl, cat);
			//If an error occur 
			if(response.getErrors().size() > 0) bb = null;
			
			//Map response into a Java Object
			BuildingBlock parsedBb = response.getBuildingBlock();
			
			if(Validator.isNotNull(parsedBb)){			
				bb = parsedBb;				
			}
			else{ 
				bb = null;
			}
			
			if(bb==null) {
				//If it's invalid return error
				out.put("error", true);
				out.put("message", "Descriptor Error: "+response.getErrors().get(0));
				return out;
			}  else {
				//Set default output
				bb = ArtifactValutationUtils.setDefaultOutput(bb);
			}
		}
		
		if(title!=null && title.trim().length() > 0)             bb.setTitle(title);
		if(description!=null && description.trim().length() > 0) {
			bb.setDescription(description);
			bb.setAbstractDescription(description);
		}
		if(cat!=null){
			String catL = category.toLowerCase();
			if( "rest".compareTo(catL)== 0 || "soap".compareTo(catL)== 0 ) cat = PortletProps.get("Category."+category.toLowerCase());
			else if( "wadl".compareTo(catL)== 0) cat = PortletProps.get("Category."+"rest".toLowerCase());
			else if( "wsdl".compareTo(catL)== 0) cat = PortletProps.get("Category."+"soap".toLowerCase());
			else if( "swagger".compareTo(catL)== 0) cat = PortletProps.get("Category."+"rest".toLowerCase());
			bb.setType(cat);
		}                                            
		if(language!=null && language.trim().length() > 0)       bb.setLang(language);
		if(urlDoc!=null)                                           bb.setPage(urlDoc);
		
		List<Entity> businessRoles = new ArrayList<Entity>();
		User uAuthor = UserLocalServiceUtil.getUser(userId);
		//SET Author
		Entity author= new Entity();		
		author.setBusinessRole(BusinessRole.AUTHOR.toString());
		author.setTitle(uAuthor.getFullName());
		author.setMbox(uAuthor.getEmailAddress());
		businessRoles.add(author);
		//SET Owner
		Entity owner= new Entity();
		owner.setBusinessRole(BusinessRole.OWNER.toString());
		owner.setTitle(uAuthor.getFullName());
		owner.setMbox(uAuthor.getEmailAddress());
		businessRoles.add(owner);
		
		//SET Provider
		if(providerName!=null && providerName.trim().length() > 0) {
			Entity provider= new Entity();
			provider.setBusinessRole(BusinessRole.PROVIDER.toString());
			provider.setTitle(providerName);
			businessRoles.add(provider);
		}
		
		
		bb.setBusinessRoles(businessRoles);
		out.put("valid",true);
		//--------------------------+
		
		if(!out.getBoolean("valid")){ return out; }
		else{ out = JSONFactoryUtil.createJSONObject(); }
		
		Artefatto a = artefattoLocalService.createArtefatto(counterLocalService.increment(Artefatto.class.getName()));
		
		a.setTitle(StringEscapeUtils.unescapeHtml4(myURLDecode(title, false).replace("&apos;", "'")));
		
		String descr = "";
		String abs = "";
		if(description!=null){
			descr = myURLDecode(description, false);
			//abs = descr.subSequence(0, Math.min(200, descr.length())).toString();
		}
		a.setDescription(descr);
		a.setAbstractDescription(descr);
		
		a.setRepositoryRDF(ConfUtil.getString("Crud.SesamServerURI")+ConfUtil.getString("Crud.SesameRepositoryName"));
		if(resourceRdf!=null && !resourceRdf.equals("")){
			String rdf = getRdfAsString(myURLDecode(resourceRdf,true));
			String name = a.getTitle().replace(" ", "_") + "_" + GregorianCalendar.getInstance().getTimeInMillis();
			EditorUtils.uploadOnRepositoryRDF(rdf, name, false);
			a.setResourceRDF("file://"+name);
		}
		
		a.setEId(null);

		//Imposto la categoria REST, se tale categoria non � censita sul portale imposto 
		//il codice-categoria a 0, che � inteso come ALTRO
		Categoria serviceCat = CategoriaLocalServiceUtil.getCategoriaByName(cat);
		long datasetCatId = 0;
		if(serviceCat!=null) 
			datasetCatId = serviceCat.getIdCategoria();
		a.setCategoriamkpId(datasetCatId);
		
		User user = UserLocalServiceUtil.getUser(userId);
		long groupId = user.getGroupId();
			a.setCompanyId(user.getCompanyId());
			a.setUserId(user.getUserId());
			a.setGroupId(groupId);

		if(providerName!=null){ a.setProviderName(StringEscapeUtils.unescapeHtml4(myURLDecode(providerName, false)).replace("&apos;", "'")); }
		else{ a.setProviderName(""); }
		a.setDate(GregorianCalendar.getInstance().getTime());
		
		a.setInteractionPoint(wadl);
		if(webpage!=null && !webpage.equals("")) a.setWebpage(myURLDecode(webpage,true));
		else a.setWebpage("");
		a.setImgId(-1L);
//		a.setWarApkDLEntryId(-1L);
		String pilotId = "";
		String[] pilots = ((String[])user.getExpandoBridge().getAttribute("pilot"));
		if(pilots!=null && pilots.length > 0) {
			pilotId = pilots[0]; 
			//BB fill
			bb.setPilot(pilotId);			
		}
		a.setPilotid(StringEscapeUtils.unescapeHtml4(pilotId).replace("&apos;", "'"));
		
		String artLanguage = "English";
		if(language!=null && !language.equals("")){ artLanguage = myURLDecode(language,false); }
		else{
			String[] languages = (String[]) user.getExpandoBridge().getAttribute("languages");
			if(languages!=null && languages.length > 0){
				artLanguage = languages[0];
			}
		}
		a.setLanguage(artLanguage);
		//Serve per triggerrare i listener sulla pubblicazione
		a.setStatus(WorkflowConstants.STATUS_DRAFT);
		
		a.setOwner(uAuthor.getFullName());
		
		//FIX
		bb = ArtifactValutationUtils.fixBody(bb);
		bb = ArtifactValutationUtils.fixSecurity(bb);
		
		//Add lusdl Serialized
		String lusdlmodel = bb.writeAsJsonString();
		a.setLusdlmodel(lusdlmodel);
		
		artefattoLocalService.addArtefatto(a);
		//ARTIFACT VALIDATION
		_log.debug(bb.writeAsJsonString());
		if(!ArtifactValutationUtils.isArtifactValid(lusdlmodel)) {
			_log.error("ARTEFATTO BLOCCATO NON TRATTABILE");
			//SE L'ARTEFATTO NON E' VALIDO NON FACCIO FARE L'INSERT NEL DB
			out.put("error", true);
			out.put("message", "The Artefact did not pass the validation process");
		} else {
			//--------------------------
			Artefatto art_tmp = ArtefattoLocalServiceUtil.publishArtefatto(a.getArtefattoId());
			//Get ArtefattoId
			long artefattoid = art_tmp.getArtefattoId();
			
			//ARTEFACT VALUTATION
			_log.debug("[API]TODO START HERE----Valutazione Artefatto ID: "+artefattoid);
			int artifactLevel =(int) ArtifactEvaluationUtils.getArtifactLevelById(artefattoid, true).get("artefactLevel");
			String missingForLevel3 = (String) ArtifactEvaluationUtils.getArtifactLevelById(artefattoid, true).get("problems");
			_log.debug("[API]LIVELLO ARTEFATTO: "+artifactLevel);
			ArtifactLevels level = ArtifactLevelsLocalServiceUtil.addArtifactLevels(artefattoid,artifactLevel,"");
			_log.debug("[API]LIVELLO ARTEFATTO PUBBLICATO: "+level);
			//_------------------------------
			
			out.put("error", false);
			out.put("message", "Artefact created succesfully with id "+a.getArtefattoId());
			out.put("artefactid", a.getArtefattoId());
			//----
			out.put("assignedLevel", artifactLevel);
			out.put("Note", missingForLevel3);
			//_____
		}
		return out;	
	}
	
	@JSONWebService(value="/v2/setupartefact")
	public JSONObject setupArtefact(String body) throws Exception{
		
		JSONObject jin = null;
		JSONObject jout = JSONFactoryUtil.createJSONObject();
		
		JSONArray warn_arr = JSONFactoryUtil.createJSONArray();
		JSONArray errors_arr = JSONFactoryUtil.createJSONArray();
		
		if(!isValidUser(this.getUser())){
			errors_arr.put("User "+this.getUser().getScreenName()+" unauthorized");
			_log.error("User "+this.getUser().getScreenName()+" unauthorized");
			
			jout.put("success", false);
			jout.put("warnings", warn_arr);
			jout.put("errors", errors_arr);
			return jout;
		}
		
		long ideaid = -1L;
		long ccuserid = -1L;
		String type = null;
		JSONObject lusdlmodel = null;
		
		try{ 
			jin = JSONFactoryUtil.createJSONObject(body);
			
			boolean valid = true;
			
			if((!jin.has("mashupid") || jin.getLong("mashupid") ==-1L) && (!jin.has("mockupid") || jin.getLong("mockupid") ==-1L)){
				errors_arr.put("Invalid mashup/mockup id");
				valid = false;
			}
			
			if(jin.has("ideaid")){
				try{ ideaid = jin.getLong("ideaid"); }
				catch(Exception e){ 
					errors_arr.put("Invalid ideaid");
					valid = false;
				}
			}
			
			ccuserid = jin.getLong("ccuserid");
			if(ccuserid==-1L){
				errors_arr.put("Invalid ccuserid");
				valid = false;
			}
			
			lusdlmodel = jin.getJSONObject("lusdl");
			if(lusdlmodel==null){
				errors_arr.put("Invalid lusdlmodel");
				valid = false;
			}
			
			boolean isbb = false;
			boolean ispsa = false;
			
			type = jin.getString("type");
			if(type==null){
				errors_arr.put("Invalid type");
				valid = false;
			}
			else{ 
				type = type.trim(); 
				if("bblocks".equalsIgnoreCase(type)){
					isbb = true;
				}
				else if("psa".equalsIgnoreCase(type)){
					ispsa = true;
				}
			}
			
			if(!valid){
				throw new Exception();
			}
			
			String s_lusdl = lusdlmodel.toString();
			ParserResponse response = null;
			try{
				response = MetamodelParser.parse(s_lusdl, MetamodelParser.SourceType.METAMODEL);
				if(response==null){
					throw new Exception("Invalid Linked USDL supplied");
				}
			}
			catch(Exception e){
				errors_arr.put(e.getMessage());
				throw new Exception();
			}
			
			Artefact lusdlArt = null;
			Class<?> lusdlclass = null;
			if(isbb){
				long mashupid = jin.getLong("mashupid");
				jout.put("mashupid", mashupid);
				
				lusdlclass = BuildingBlock.class;
				lusdlArt = response.getBuildingBlock();
				
				_log.debug("Warnings:");
				for(String warn : response.getWarnings()){
					_log.debug("\t"+warn);
					warn_arr.put(warn);
				}
			}
			else if(ispsa){
				long mockupid = jin.getLong("mockupid");
				jout.put("mockupid", mockupid);
				
				lusdlclass = PublicServiceApplication.class;
				try{ lusdlArt = MetamodelParser.parsePSA(s_lusdl); }
				catch(Exception e){
					throw new Exception("Invalid lusdl. "+e);
				}
			}
			else{
				throw new Exception("Invalid type "+type);
			}
			
			
			if(lusdlArt!=null){
				try{
					User u = null;
					Long userid = MarketplaceApiUtils.getUserIdByCcUserId(String.valueOf(ccuserid));
					try{  
						u = UserLocalServiceUtil.getUser(userid);
					}
					catch(Exception e){
						errors_arr.put("No user exists with ccuserid "+ccuserid);
						throw e;
					}
					
					Long artefattoid = -1L;
					Artefatto art_tmp = WizardUtils.setupArtefact(artefattoid, lusdlArt, lusdlclass, u, ideaid);
					artefattoid = art_tmp.getArtefattoId();
					
					jout.put("success", true);
					jout.put("warnings", warn_arr);
					jout.put("errors", errors_arr);
					jout.put("artefactid", artefattoid);
				}
				catch(Exception e){
					errors_arr.put(e.getMessage());
					throw e;
				}
			}
			else{
				_log.debug("Errors:");
				for(String err : response.getErrors()){
					_log.debug("\t"+err);
					errors_arr.put(err);
				}
				throw new Exception();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			_log.error(e.getMessage());
			jout.put("success", false);
			jout.put("warnings", warn_arr);
			jout.put("errors", errors_arr);
		}		
		
		return jout;
	}
	
	@JSONWebService(value="/v2/update-dataset")
	public JSONObject updateDataset(String body) throws Exception {
		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		
		try{
			User requirer = this.getUser();
			if(!isValidUser(requirer)){
				throw new SecurityException("User unauthorized");
			}
			
			JSONObject jbody = JSONFactoryUtil.createJSONObject(body);
			Boolean isMapped = jbody.getBoolean("hasmapping");
			String datasetid = jbody.getString("datasetid");
			Boolean isPrivate = jbody.getBoolean("isPrivate");
			
			Artefatto a = null;
			if(Validator.isNull(datasetid) || datasetid.length()<=0){
				throw new IllegalArgumentException(datasetid);
			}
			else{
				try{
					a = ArtefattoLocalServiceUtil.getArtefactByExternalId(datasetid);
				}
				catch(Exception e){
					throw new IllegalArgumentException("No dataset "+datasetid+" exists");
				}
				
				if(Validator.isNull(a)){
					throw new IllegalArgumentException("No dataset "+datasetid+" exists");
				}
			}
			
			a.setHasMapping(isMapped);
			a.setIsPrivate(isPrivate);
			
			if(jbody.has("model")){
				
				JSONObject model = jbody.getJSONObject("model");
				Dataset dataset = MetamodelParser.parseDataset(model.toString());
				_log.debug(dataset.writeAsJsonString());
				
				User author = null;
				List<Entity> broles = dataset.getBusinessRoles();
				for(Entity brole : broles){
					String s_brole = brole.getBusinessRole();
					if(Validator.isNotNull(s_brole)){
						s_brole = s_brole.substring(0, 1).toUpperCase() + s_brole.substring(1);
					}
					
					BusinessRole br = BusinessRole.valueOf(s_brole);
					switch(br){
						case AUTHOR:
							long companyId = a.getCompanyId();
							author = UserLocalServiceUtil.getUserByEmailAddress(companyId, brole.getMbox());
							a.setUserId(author.getUserId());
							break;
						case PROVIDER:
							a.setProviderName(brole.getTitle());
							break;
						default:
							break;
					}
				}
				
				if(Validator.isNull(author)){
					throw new IllegalArgumentException("A Dataset must have an author specified among business roles");
				}
				
				a.setTitle(dataset.getTitle());
				a.setDescription(dataset.getDescription());
				a.setAbstractDescription(dataset.getAbstractDescription());
				a.setPilotid(dataset.getPilot());
				
				Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataset.getCreated());
				a.setDate(date);
				
				a.setWebpage(dataset.getPage());
				a.setLanguage(dataset.getLang());
				
					
				a.setLusdlmodel(body);
				
				//Update tags
				String[] tags = dataset.getTags().toArray(new String[0]);
				AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(author.getUserId(),
																		author.getGroupId(),
																		Artefatto.class.getName(), 
																		a.getArtefattoId(), 
																		new long[]{},
																		tags);

				ae.setVisible(false);
				AssetEntryLocalServiceUtil.updateAssetEntry(ae);
				
			}
			
			ArtefattoLocalServiceUtil.updateArtefatto(a);
			
			out = JSONFactoryUtil.createJSONObject();
			out.put("error", false);
			out.put("message", "Dataset "+datasetid+" updated correctly");
			_log.info("Dataset "+datasetid+" updated correctly");
			
		}
		catch(Exception e){
			out = JSONFactoryUtil.createJSONObject();
			out.put("error", true);
			out.put("message", e.getClass().getSimpleName()+"[ "+e.getMessage()+" ]");
			_log.error("This user in unauthorized to update dataset");
		}
		
		return out;
	}
	
	@JSONWebService(value="/v2/publish-dataset")
	public JSONObject newDataset_v2(String body) throws Exception {
		
		JSONObject out = JSONFactoryUtil.createJSONObject();
		
		try{
			User requirer = this.getUser();
			if(!isValidUser(requirer)){
				throw new SecurityException("User unauthorized");
			}
			
			JSONObject jbody = JSONFactoryUtil.createJSONObject(body);
			String datasetid = jbody.getString("datasetid");
			
			Artefatto a = null;
			if(Validator.isNull(datasetid) || datasetid.length()<=0){
				throw new IllegalArgumentException(datasetid);
			}
			else{
				try{ a = ArtefattoLocalServiceUtil.getArtefactByExternalId(datasetid); }
				catch(Exception e){ a = null; }
				if(Validator.isNotNull(a)){
					throw new IllegalArgumentException("Dataset "+datasetid+" already exists");
				}
				
				Long ccuid = jbody.getLong("ccuserid");
				Long userId = MarketplaceApiUtils.getUserIdByCcUserId(String.valueOf(ccuid));
				User author = UserLocalServiceUtil.getUser(userId);
				
				long[] orgIds = author.getOrganizationIds();
				a = MyMarketplaceUtility.newArtifact(requirer.getCompanyId(), 
															requirer.getGroupId(), 
															userId, orgIds);
				
				a.setHasMapping(false);
				a.setStatus(WorkflowConstants.STATUS_DRAFT);
				
				Categoria datasetCat = categoriaLocalService.getCategoriaByName(PortletProps.get("Category.dataset"));
				long datasetCatId = 0;
				if(datasetCat!=null) 
					datasetCatId = datasetCat.getIdCategoria();
				
				a.setCategoriamkpId(datasetCatId);
			}
			
			JSONObject model = jbody.getJSONObject("model");
			Dataset dataset = MetamodelParser.parseDataset(model.toString());
			
			User author = null;
			List<Entity> broles = dataset.getBusinessRoles();
			for(Entity brole : broles){
				BusinessRole br = BusinessRole.valueOf(brole.getBusinessRole().toUpperCase());
				switch(br){
					case AUTHOR:
						long companyId = MyMarketplaceUtility.getDefaultCompanyId();
						author = UserLocalServiceUtil.getUserByEmailAddress(companyId, brole.getMbox());
						a.setUserId(author.getUserId());
						break;
					case PROVIDER:
						a.setProviderName(brole.getTitle());
						break;
					default:
						break;
				}
			}
			
			if(Validator.isNull(author)){
				throw new IllegalArgumentException("A Dataset must have an author specified among business roles");
			}
			
			a.setTitle(dataset.getTitle());
			a.setDescription(dataset.getDescription());
			a.setAbstractDescription(dataset.getAbstractDescription());
			a.setPilotid(dataset.getPilot());
			
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataset.getCreated());
			a.setDate(date);
			
			a.setWebpage(dataset.getPage());
			a.setLanguage(dataset.getLang());
				
			a.setLusdlmodel(body);
			
			//Update tags
			String[] tags = dataset.getTags().toArray(new String[0]);
			AssetEntry ae = AssetEntryLocalServiceUtil.updateEntry(author.getUserId(),
																	author.getGroupId(),
																	Artefatto.class.getName(), 
																	a.getArtefattoId(), 
																	new long[]{},
																	tags);

			ae.setVisible(false);
			AssetEntryLocalServiceUtil.updateAssetEntry(ae);
				
			ArtefattoLocalServiceUtil.addArtefatto(a);
			ArtefattoLocalServiceUtil.publishArtefatto(a.getArtefattoId());
			
			out = JSONFactoryUtil.createJSONObject();
			out.put("error", false);
			out.put("message", "Dataset "+datasetid+" updated correctly");
			_log.info("Dataset "+datasetid+" updated correctly");
			
		}
		catch(Exception e){
			e.printStackTrace();
			out = JSONFactoryUtil.createJSONObject();
			out.put("error", true);
			out.put("message", e.getClass().getSimpleName()+"[ "+e.getMessage()+" ]");
			_log.error("This user in unauthorized to update dataset");
		}
		
		return out;
	}

	@JSONWebService(method="DELETE", value="/v2/delete-artefact")
	public JSONObject deleteartefact(long artefactid) throws Exception {
		JSONObject out = JSONFactoryUtil.createJSONObject();
		out.put("success", true);
		
		try{ 
			ArtefattoLocalServiceUtil.deleteArtefatto(artefactid); 
			ArtifactLevelsLocalServiceUtil.deleteArtifactLevels(artefactid);
		}
		catch(Exception e){
			out.put("success", false);
			out.put("message", e.getMessage());
		}
		
		return out;
	}
	
	@JSONWebService(method="POST", value="/v2/updateartefact")
	public JSONObject updateArtefact(long artefactid, String body) throws Exception {

		JSONObject out = JSONFactoryUtil.createJSONObject();
		try{
			//To check that it's a valid json
			JSONObject lusdlmodel = JSONFactoryUtil.createJSONObject(body);
			Artefatto a = ArtefattoLocalServiceUtil.getArtefatto(artefactid);
			a.setLusdlmodel(lusdlmodel.toString());
			
			ArtefattoLocalServiceUtil.updateArtefatto(a);
			out.put("success", true);
		}
		catch(Exception e){
			out.put("success", false);
			out.put("message", e.getMessage());
		}
		
		return out;
	}


}