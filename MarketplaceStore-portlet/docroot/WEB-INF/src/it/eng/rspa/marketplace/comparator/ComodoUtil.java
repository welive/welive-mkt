package it.eng.rspa.marketplace.comparator;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;

public class ComodoUtil {
	public static String getStringRoW(Artefatto artefatto, String pathDocumentLibrary, RenderRequest req, PortletURL rowURL){
		String out="";
	    String imgURl = "";
		ImmagineArt img = null;
		try{
			if (ImmagineArtLocalServiceUtil.findByArtefattoId(
					artefatto.getArtefattoId()).size() > 0) {
				img = ImmagineArtLocalServiceUtil
						.findByArtefattoId(
								artefatto.getArtefattoId()).get(0);
			
				DLFileEntry dlf = null;
				try {
					dlf = DLFileEntryLocalServiceUtil
							.getDLFileEntry(img.getDlImageId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					imgURl = pathDocumentLibrary + dlf.getUuid();
				}//if
				if(imgURl.equals("")){
					imgURl=req.getContextPath()+"/icons/defaultIcon.png";
				}
		 		 out="<img src="+imgURl+" width='50' height='50' />"+
		 		" <a href='"+rowURL.toString()+"'>"+artefatto.getTitle()+"</a>"+
		 		//" (id: "+artefatto.getArtefattoId()+")"+
				"<br>Fornito da: <a href='"+artefatto.getWebpage()+"'>"+artefatto.getProviderName()+"</a>"+
				//"<br><liferay-ui:ratings className=\""+Artefatto.class.getName()+"\" classPK=\""+artefatto.getArtefattoId()+"\" type=\"stars\" />"+
				"<br>Pubblicato il: "+artefatto.getDate() +
				"<br>Categoria: "+artefatto.getCategoriamkpId()+
				"<br>"+artefatto.getAbstractDescription()+
				"<a href='"+rowURL.toString()+"'> (vedi...)</a></p>";
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(out);
		return out;
	}
}
