/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;
import it.eng.rspa.marketplace.artefatto.model.Dependencies;
import it.eng.rspa.marketplace.artefatto.model.impl.DependenciesImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the dependencies service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see DependenciesPersistence
 * @see DependenciesUtil
 * @generated
 */
public class DependenciesPersistenceImpl extends BasePersistenceImpl<Dependencies>
	implements DependenciesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DependenciesUtil} to access the dependencies persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DependenciesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTEFACTID =
		new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByArtefactId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFACTID =
		new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByArtefactId",
			new String[] { Long.class.getName() },
			DependenciesModelImpl.ARTEFACTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTEFACTID = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByArtefactId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the dependencieses where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @return the matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByArtefactId(long artefactId)
		throws SystemException {
		return findByArtefactId(artefactId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dependencieses where artefactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artefactId the artefact ID
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @return the range of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByArtefactId(long artefactId, int start,
		int end) throws SystemException {
		return findByArtefactId(artefactId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dependencieses where artefactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artefactId the artefact ID
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByArtefactId(long artefactId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFACTID;
			finderArgs = new Object[] { artefactId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTEFACTID;
			finderArgs = new Object[] { artefactId, start, end, orderByComparator };
		}

		List<Dependencies> list = (List<Dependencies>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Dependencies dependencies : list) {
				if ((artefactId != dependencies.getArtefactId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_ARTEFACTID_ARTEFACTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DependenciesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefactId);

				if (!pagination) {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Dependencies>(list);
				}
				else {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dependencies in the ordered set where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByArtefactId_First(long artefactId,
		OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByArtefactId_First(artefactId,
				orderByComparator);

		if (dependencies != null) {
			return dependencies;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artefactId=");
		msg.append(artefactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDependenciesException(msg.toString());
	}

	/**
	 * Returns the first dependencies in the ordered set where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByArtefactId_First(long artefactId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Dependencies> list = findByArtefactId(artefactId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dependencies in the ordered set where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByArtefactId_Last(long artefactId,
		OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByArtefactId_Last(artefactId,
				orderByComparator);

		if (dependencies != null) {
			return dependencies;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artefactId=");
		msg.append(artefactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDependenciesException(msg.toString());
	}

	/**
	 * Returns the last dependencies in the ordered set where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByArtefactId_Last(long artefactId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByArtefactId(artefactId);

		if (count == 0) {
			return null;
		}

		List<Dependencies> list = findByArtefactId(artefactId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dependencieses before and after the current dependencies in the ordered set where artefactId = &#63;.
	 *
	 * @param dependencyId the primary key of the current dependencies
	 * @param artefactId the artefact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies[] findByArtefactId_PrevAndNext(long dependencyId,
		long artefactId, OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = findByPrimaryKey(dependencyId);

		Session session = null;

		try {
			session = openSession();

			Dependencies[] array = new DependenciesImpl[3];

			array[0] = getByArtefactId_PrevAndNext(session, dependencies,
					artefactId, orderByComparator, true);

			array[1] = dependencies;

			array[2] = getByArtefactId_PrevAndNext(session, dependencies,
					artefactId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Dependencies getByArtefactId_PrevAndNext(Session session,
		Dependencies dependencies, long artefactId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DEPENDENCIES_WHERE);

		query.append(_FINDER_COLUMN_ARTEFACTID_ARTEFACTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DependenciesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(artefactId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dependencies);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Dependencies> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dependencieses where artefactId = &#63; from the database.
	 *
	 * @param artefactId the artefact ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByArtefactId(long artefactId) throws SystemException {
		for (Dependencies dependencies : findByArtefactId(artefactId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(dependencies);
		}
	}

	/**
	 * Returns the number of dependencieses where artefactId = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @return the number of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByArtefactId(long artefactId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTEFACTID;

		Object[] finderArgs = new Object[] { artefactId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_ARTEFACTID_ARTEFACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefactId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTEFACTID_ARTEFACTID_2 = "dependencies.artefactId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DEPENDSFROM =
		new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDependsFrom",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DEPENDSFROM =
		new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDependsFrom",
			new String[] { Long.class.getName() },
			DependenciesModelImpl.DEPENDSFROM_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DEPENDSFROM = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDependsFrom",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the dependencieses where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @return the matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByDependsFrom(long dependsFrom)
		throws SystemException {
		return findByDependsFrom(dependsFrom, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dependencieses where dependsFrom = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dependsFrom the depends from
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @return the range of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByDependsFrom(long dependsFrom, int start,
		int end) throws SystemException {
		return findByDependsFrom(dependsFrom, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dependencieses where dependsFrom = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dependsFrom the depends from
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findByDependsFrom(long dependsFrom, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DEPENDSFROM;
			finderArgs = new Object[] { dependsFrom };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DEPENDSFROM;
			finderArgs = new Object[] { dependsFrom, start, end, orderByComparator };
		}

		List<Dependencies> list = (List<Dependencies>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Dependencies dependencies : list) {
				if ((dependsFrom != dependencies.getDependsFrom())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_DEPENDSFROM_DEPENDSFROM_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DependenciesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dependsFrom);

				if (!pagination) {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Dependencies>(list);
				}
				else {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByDependsFrom_First(long dependsFrom,
		OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByDependsFrom_First(dependsFrom,
				orderByComparator);

		if (dependencies != null) {
			return dependencies;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dependsFrom=");
		msg.append(dependsFrom);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDependenciesException(msg.toString());
	}

	/**
	 * Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByDependsFrom_First(long dependsFrom,
		OrderByComparator orderByComparator) throws SystemException {
		List<Dependencies> list = findByDependsFrom(dependsFrom, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByDependsFrom_Last(long dependsFrom,
		OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByDependsFrom_Last(dependsFrom,
				orderByComparator);

		if (dependencies != null) {
			return dependencies;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dependsFrom=");
		msg.append(dependsFrom);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDependenciesException(msg.toString());
	}

	/**
	 * Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByDependsFrom_Last(long dependsFrom,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByDependsFrom(dependsFrom);

		if (count == 0) {
			return null;
		}

		List<Dependencies> list = findByDependsFrom(dependsFrom, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dependencieses before and after the current dependencies in the ordered set where dependsFrom = &#63;.
	 *
	 * @param dependencyId the primary key of the current dependencies
	 * @param dependsFrom the depends from
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies[] findByDependsFrom_PrevAndNext(long dependencyId,
		long dependsFrom, OrderByComparator orderByComparator)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = findByPrimaryKey(dependencyId);

		Session session = null;

		try {
			session = openSession();

			Dependencies[] array = new DependenciesImpl[3];

			array[0] = getByDependsFrom_PrevAndNext(session, dependencies,
					dependsFrom, orderByComparator, true);

			array[1] = dependencies;

			array[2] = getByDependsFrom_PrevAndNext(session, dependencies,
					dependsFrom, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Dependencies getByDependsFrom_PrevAndNext(Session session,
		Dependencies dependencies, long dependsFrom,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DEPENDENCIES_WHERE);

		query.append(_FINDER_COLUMN_DEPENDSFROM_DEPENDSFROM_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DependenciesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dependsFrom);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dependencies);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Dependencies> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dependencieses where dependsFrom = &#63; from the database.
	 *
	 * @param dependsFrom the depends from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByDependsFrom(long dependsFrom) throws SystemException {
		for (Dependencies dependencies : findByDependsFrom(dependsFrom,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(dependencies);
		}
	}

	/**
	 * Returns the number of dependencieses where dependsFrom = &#63;.
	 *
	 * @param dependsFrom the depends from
	 * @return the number of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDependsFrom(long dependsFrom) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DEPENDSFROM;

		Object[] finderArgs = new Object[] { dependsFrom };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_DEPENDSFROM_DEPENDSFROM_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dependsFrom);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DEPENDSFROM_DEPENDSFROM_2 = "dependencies.dependsFrom = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_IDSCOUPLE = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, DependenciesImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByidsCouple",
			new String[] { Long.class.getName(), Long.class.getName() },
			DependenciesModelImpl.ARTEFACTID_COLUMN_BITMASK |
			DependenciesModelImpl.DEPENDSFROM_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDSCOUPLE = new FinderPath(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByidsCouple",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	 *
	 * @param artefactId the artefact ID
	 * @param dependsFrom the depends from
	 * @return the matching dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByidsCouple(long artefactId, long dependsFrom)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByidsCouple(artefactId, dependsFrom);

		if (dependencies == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("artefactId=");
			msg.append(artefactId);

			msg.append(", dependsFrom=");
			msg.append(dependsFrom);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDependenciesException(msg.toString());
		}

		return dependencies;
	}

	/**
	 * Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param artefactId the artefact ID
	 * @param dependsFrom the depends from
	 * @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByidsCouple(long artefactId, long dependsFrom)
		throws SystemException {
		return fetchByidsCouple(artefactId, dependsFrom, true);
	}

	/**
	 * Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param artefactId the artefact ID
	 * @param dependsFrom the depends from
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByidsCouple(long artefactId, long dependsFrom,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { artefactId, dependsFrom };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDSCOUPLE,
					finderArgs, this);
		}

		if (result instanceof Dependencies) {
			Dependencies dependencies = (Dependencies)result;

			if ((artefactId != dependencies.getArtefactId()) ||
					(dependsFrom != dependencies.getDependsFrom())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_IDSCOUPLE_ARTEFACTID_2);

			query.append(_FINDER_COLUMN_IDSCOUPLE_DEPENDSFROM_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefactId);

				qPos.add(dependsFrom);

				List<Dependencies> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDSCOUPLE,
						finderArgs, list);
				}
				else {
					Dependencies dependencies = list.get(0);

					result = dependencies;

					cacheResult(dependencies);

					if ((dependencies.getArtefactId() != artefactId) ||
							(dependencies.getDependsFrom() != dependsFrom)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDSCOUPLE,
							finderArgs, dependencies);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDSCOUPLE,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Dependencies)result;
		}
	}

	/**
	 * Removes the dependencies where artefactId = &#63; and dependsFrom = &#63; from the database.
	 *
	 * @param artefactId the artefact ID
	 * @param dependsFrom the depends from
	 * @return the dependencies that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies removeByidsCouple(long artefactId, long dependsFrom)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = findByidsCouple(artefactId, dependsFrom);

		return remove(dependencies);
	}

	/**
	 * Returns the number of dependencieses where artefactId = &#63; and dependsFrom = &#63;.
	 *
	 * @param artefactId the artefact ID
	 * @param dependsFrom the depends from
	 * @return the number of matching dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByidsCouple(long artefactId, long dependsFrom)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDSCOUPLE;

		Object[] finderArgs = new Object[] { artefactId, dependsFrom };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DEPENDENCIES_WHERE);

			query.append(_FINDER_COLUMN_IDSCOUPLE_ARTEFACTID_2);

			query.append(_FINDER_COLUMN_IDSCOUPLE_DEPENDSFROM_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artefactId);

				qPos.add(dependsFrom);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDSCOUPLE_ARTEFACTID_2 = "dependencies.artefactId = ? AND ";
	private static final String _FINDER_COLUMN_IDSCOUPLE_DEPENDSFROM_2 = "dependencies.dependsFrom = ?";

	public DependenciesPersistenceImpl() {
		setModelClass(Dependencies.class);
	}

	/**
	 * Caches the dependencies in the entity cache if it is enabled.
	 *
	 * @param dependencies the dependencies
	 */
	@Override
	public void cacheResult(Dependencies dependencies) {
		EntityCacheUtil.putResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesImpl.class, dependencies.getPrimaryKey(), dependencies);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDSCOUPLE,
			new Object[] {
				dependencies.getArtefactId(), dependencies.getDependsFrom()
			}, dependencies);

		dependencies.resetOriginalValues();
	}

	/**
	 * Caches the dependencieses in the entity cache if it is enabled.
	 *
	 * @param dependencieses the dependencieses
	 */
	@Override
	public void cacheResult(List<Dependencies> dependencieses) {
		for (Dependencies dependencies : dependencieses) {
			if (EntityCacheUtil.getResult(
						DependenciesModelImpl.ENTITY_CACHE_ENABLED,
						DependenciesImpl.class, dependencies.getPrimaryKey()) == null) {
				cacheResult(dependencies);
			}
			else {
				dependencies.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all dependencieses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DependenciesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DependenciesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the dependencies.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Dependencies dependencies) {
		EntityCacheUtil.removeResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesImpl.class, dependencies.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(dependencies);
	}

	@Override
	public void clearCache(List<Dependencies> dependencieses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Dependencies dependencies : dependencieses) {
			EntityCacheUtil.removeResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
				DependenciesImpl.class, dependencies.getPrimaryKey());

			clearUniqueFindersCache(dependencies);
		}
	}

	protected void cacheUniqueFindersCache(Dependencies dependencies) {
		if (dependencies.isNew()) {
			Object[] args = new Object[] {
					dependencies.getArtefactId(), dependencies.getDependsFrom()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDSCOUPLE, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDSCOUPLE, args,
				dependencies);
		}
		else {
			DependenciesModelImpl dependenciesModelImpl = (DependenciesModelImpl)dependencies;

			if ((dependenciesModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_IDSCOUPLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dependencies.getArtefactId(),
						dependencies.getDependsFrom()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDSCOUPLE, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDSCOUPLE, args,
					dependencies);
			}
		}
	}

	protected void clearUniqueFindersCache(Dependencies dependencies) {
		DependenciesModelImpl dependenciesModelImpl = (DependenciesModelImpl)dependencies;

		Object[] args = new Object[] {
				dependencies.getArtefactId(), dependencies.getDependsFrom()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDSCOUPLE, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDSCOUPLE, args);

		if ((dependenciesModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_IDSCOUPLE.getColumnBitmask()) != 0) {
			args = new Object[] {
					dependenciesModelImpl.getOriginalArtefactId(),
					dependenciesModelImpl.getOriginalDependsFrom()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDSCOUPLE, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDSCOUPLE, args);
		}
	}

	/**
	 * Creates a new dependencies with the primary key. Does not add the dependencies to the database.
	 *
	 * @param dependencyId the primary key for the new dependencies
	 * @return the new dependencies
	 */
	@Override
	public Dependencies create(long dependencyId) {
		Dependencies dependencies = new DependenciesImpl();

		dependencies.setNew(true);
		dependencies.setPrimaryKey(dependencyId);

		return dependencies;
	}

	/**
	 * Removes the dependencies with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dependencyId the primary key of the dependencies
	 * @return the dependencies that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies remove(long dependencyId)
		throws NoSuchDependenciesException, SystemException {
		return remove((Serializable)dependencyId);
	}

	/**
	 * Removes the dependencies with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the dependencies
	 * @return the dependencies that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies remove(Serializable primaryKey)
		throws NoSuchDependenciesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Dependencies dependencies = (Dependencies)session.get(DependenciesImpl.class,
					primaryKey);

			if (dependencies == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDependenciesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(dependencies);
		}
		catch (NoSuchDependenciesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Dependencies removeImpl(Dependencies dependencies)
		throws SystemException {
		dependencies = toUnwrappedModel(dependencies);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(dependencies)) {
				dependencies = (Dependencies)session.get(DependenciesImpl.class,
						dependencies.getPrimaryKeyObj());
			}

			if (dependencies != null) {
				session.delete(dependencies);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (dependencies != null) {
			clearCache(dependencies);
		}

		return dependencies;
	}

	@Override
	public Dependencies updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws SystemException {
		dependencies = toUnwrappedModel(dependencies);

		boolean isNew = dependencies.isNew();

		DependenciesModelImpl dependenciesModelImpl = (DependenciesModelImpl)dependencies;

		Session session = null;

		try {
			session = openSession();

			if (dependencies.isNew()) {
				session.save(dependencies);

				dependencies.setNew(false);
			}
			else {
				session.merge(dependencies);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DependenciesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((dependenciesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFACTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dependenciesModelImpl.getOriginalArtefactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTEFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFACTID,
					args);

				args = new Object[] { dependenciesModelImpl.getArtefactId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTEFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTEFACTID,
					args);
			}

			if ((dependenciesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DEPENDSFROM.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dependenciesModelImpl.getOriginalDependsFrom()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DEPENDSFROM,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DEPENDSFROM,
					args);

				args = new Object[] { dependenciesModelImpl.getDependsFrom() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DEPENDSFROM,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DEPENDSFROM,
					args);
			}
		}

		EntityCacheUtil.putResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
			DependenciesImpl.class, dependencies.getPrimaryKey(), dependencies);

		clearUniqueFindersCache(dependencies);
		cacheUniqueFindersCache(dependencies);

		return dependencies;
	}

	protected Dependencies toUnwrappedModel(Dependencies dependencies) {
		if (dependencies instanceof DependenciesImpl) {
			return dependencies;
		}

		DependenciesImpl dependenciesImpl = new DependenciesImpl();

		dependenciesImpl.setNew(dependencies.isNew());
		dependenciesImpl.setPrimaryKey(dependencies.getPrimaryKey());

		dependenciesImpl.setDependencyId(dependencies.getDependencyId());
		dependenciesImpl.setArtefactId(dependencies.getArtefactId());
		dependenciesImpl.setDependsFrom(dependencies.getDependsFrom());

		return dependenciesImpl;
	}

	/**
	 * Returns the dependencies with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the dependencies
	 * @return the dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDependenciesException, SystemException {
		Dependencies dependencies = fetchByPrimaryKey(primaryKey);

		if (dependencies == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDependenciesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return dependencies;
	}

	/**
	 * Returns the dependencies with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	 *
	 * @param dependencyId the primary key of the dependencies
	 * @return the dependencies
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies findByPrimaryKey(long dependencyId)
		throws NoSuchDependenciesException, SystemException {
		return findByPrimaryKey((Serializable)dependencyId);
	}

	/**
	 * Returns the dependencies with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the dependencies
	 * @return the dependencies, or <code>null</code> if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Dependencies dependencies = (Dependencies)EntityCacheUtil.getResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
				DependenciesImpl.class, primaryKey);

		if (dependencies == _nullDependencies) {
			return null;
		}

		if (dependencies == null) {
			Session session = null;

			try {
				session = openSession();

				dependencies = (Dependencies)session.get(DependenciesImpl.class,
						primaryKey);

				if (dependencies != null) {
					cacheResult(dependencies);
				}
				else {
					EntityCacheUtil.putResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
						DependenciesImpl.class, primaryKey, _nullDependencies);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DependenciesModelImpl.ENTITY_CACHE_ENABLED,
					DependenciesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return dependencies;
	}

	/**
	 * Returns the dependencies with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dependencyId the primary key of the dependencies
	 * @return the dependencies, or <code>null</code> if a dependencies with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Dependencies fetchByPrimaryKey(long dependencyId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)dependencyId);
	}

	/**
	 * Returns all the dependencieses.
	 *
	 * @return the dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dependencieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @return the range of dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the dependencieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dependencieses
	 * @param end the upper bound of the range of dependencieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Dependencies> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Dependencies> list = (List<Dependencies>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DEPENDENCIES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DEPENDENCIES;

				if (pagination) {
					sql = sql.concat(DependenciesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Dependencies>(list);
				}
				else {
					list = (List<Dependencies>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the dependencieses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Dependencies dependencies : findAll()) {
			remove(dependencies);
		}
	}

	/**
	 * Returns the number of dependencieses.
	 *
	 * @return the number of dependencieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DEPENDENCIES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the dependencies persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.Dependencies")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Dependencies>> listenersList = new ArrayList<ModelListener<Dependencies>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Dependencies>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DependenciesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DEPENDENCIES = "SELECT dependencies FROM Dependencies dependencies";
	private static final String _SQL_SELECT_DEPENDENCIES_WHERE = "SELECT dependencies FROM Dependencies dependencies WHERE ";
	private static final String _SQL_COUNT_DEPENDENCIES = "SELECT COUNT(dependencies) FROM Dependencies dependencies";
	private static final String _SQL_COUNT_DEPENDENCIES_WHERE = "SELECT COUNT(dependencies) FROM Dependencies dependencies WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "dependencies.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Dependencies exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Dependencies exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DependenciesPersistenceImpl.class);
	private static Dependencies _nullDependencies = new DependenciesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Dependencies> toCacheModel() {
				return _nullDependenciesCacheModel;
			}
		};

	private static CacheModel<Dependencies> _nullDependenciesCacheModel = new CacheModel<Dependencies>() {
			@Override
			public Dependencies toEntityModel() {
				return _nullDependencies;
			}
		};
}