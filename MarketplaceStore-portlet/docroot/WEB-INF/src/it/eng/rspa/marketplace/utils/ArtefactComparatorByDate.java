package it.eng.rspa.marketplace.utils;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.util.Comparator;

public class ArtefactComparatorByDate implements Comparator<Artefatto> {
		
	@Override
	public int compare(Artefatto art1, Artefatto art2) {
		
		int ret=art2.getDate().compareTo(art1.getDate());
		return ret;
		

	}
}
