/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ArtifactOrganizations in entity cache.
 *
 * @author eng
 * @see ArtifactOrganizations
 * @generated
 */
public class ArtifactOrganizationsCacheModel implements CacheModel<ArtifactOrganizations>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{organizationId=");
		sb.append(organizationId);
		sb.append(", artifactId=");
		sb.append(artifactId);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ArtifactOrganizations toEntityModel() {
		ArtifactOrganizationsImpl artifactOrganizationsImpl = new ArtifactOrganizationsImpl();

		artifactOrganizationsImpl.setOrganizationId(organizationId);
		artifactOrganizationsImpl.setArtifactId(artifactId);
		artifactOrganizationsImpl.setStatus(status);

		artifactOrganizationsImpl.resetOriginalValues();

		return artifactOrganizationsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		organizationId = objectInput.readLong();
		artifactId = objectInput.readLong();
		status = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(organizationId);
		objectOutput.writeLong(artifactId);
		objectOutput.writeInt(status);
	}

	public long organizationId;
	public long artifactId;
	public int status;
}