/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.base.ImmagineArtLocalServiceBaseImpl;

/**
 * The implementation of the immagine art local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author eng
 * @see it.eng.rspa.marketplace.artefatto.service.base.ImmagineArtLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil
 */
public class ImmagineArtLocalServiceImpl extends ImmagineArtLocalServiceBaseImpl {
	/** 
	 *  
	 * @param standard 
	 * @return List<ImmagineArt> 
	 * @throws SystemException 
	 */  
	//  http://www.opensource-techblog.com/2013/03/create-finder-method-for-service.html
	// http://liferayzone.wordpress.com/2011/06/09/liferay-service-builder-part-1-finder-methods-in-liferay-portal/
	public List<ImmagineArt> findByArtefattoId(long artefattoId) throws SystemException  {  
		 
		return this.immagineArtPersistence.findByArtefattoIdentifier(artefattoId);
//		return new ImmagineArtPersistenceImpl().findByArtefattoIdentifier(artefattoId);  
	}  
	
	public void removeByArtefattoId(long artefattoId) throws SystemException{
		
		this.immagineArtPersistence.removeByArtefattoIdentifier(artefattoId);
		
		return;		
	}

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil} to access the immagine art local service.
	 */
}