package it.eng.rspa.marketplace;

import javax.ws.rs.core.MediaType;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class CitizenDataVaultNotificationService {

	private static JSONObject wrappertemplate;
	private static JSONObject  template;
	
	static{
		wrappertemplate = JSONFactoryUtil.createJSONObject();
		wrappertemplate.put("eventName", "ArtefactUsage");
		wrappertemplate.put("entries", JSONFactoryUtil.createJSONArray());
		
		template = JSONFactoryUtil.createJSONObject();
		template.put("key", "newUsage");
	}
	
	private static JSONArray createUsageMessage(Long userId, Long artId){
		
		JSONArray ext = JSONFactoryUtil.createJSONArray();
		try {
			JSONObject messageWrapper = JSONFactoryUtil.createJSONObject(wrappertemplate.toString());
			User user = UserLocalServiceUtil.getUser(userId);
			messageWrapper.put("ccUserID", Integer.parseInt(user.getExpandoBridge().getAttribute("CCUserID").toString()));
			JSONArray entries = messageWrapper.getJSONArray("entries");
		
			JSONObject message = JSONFactoryUtil.createJSONObject(template.toString());
			message.put("value", artId.toString());
			entries.put(message);
			
			ext.put(messageWrapper);
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return ext;
	}
	
	public static boolean notify(Long userId, Long artId){
		
		JSONArray message = createUsageMessage(userId, artId);
		
		Client client = Client.create();
		
		client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
		WebResource webResource = client.resource(ConfUtil.getString("cdv.url")+"/push");
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, message.toString());
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        	return false;
        }
        //String resp = clientResponse.getEntity(String.class);
		return true;
	}
	
	public static boolean notifylkl(User u, double lat, double lng){
		JSONArray message = createNewLKLMessage(u, lat, lng);

		Client client = Client.create();
		
		client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
		WebResource webResource = client.resource(ConfUtil.getString("cdv.url")+"/push");
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, message.toString());
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        	return false;
        }
        //String resp = clientResponse.getEntity(String.class);
		return true;
	}

	private static JSONArray createNewLKLMessage(User u, double lat, double lng) {
		JSONArray ext = JSONFactoryUtil.createJSONArray();
		try {
			JSONObject messageWrapper = JSONFactoryUtil.createJSONObject();
			messageWrapper.put("ccUserID", Integer.parseInt(u.getExpandoBridge().getAttribute("CCUserID").toString()));
			messageWrapper.put("eventName", "NewKnownLocation");
			messageWrapper.put("entries", JSONFactoryUtil.createJSONArray());
			JSONArray entries = messageWrapper.getJSONArray("entries");
		
			JSONObject message = JSONFactoryUtil.createJSONObject();
			message.put("key", "lat");
			message.put("value", String.valueOf(lat));
			entries.put(message);
			
			JSONObject message2 = JSONFactoryUtil.createJSONObject();
			message2.put("key", "lng");
			message2.put("value",  String.valueOf(lng));
			entries.put(message2);
			
			ext.put(messageWrapper);
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		return ext;
	}
	
}
