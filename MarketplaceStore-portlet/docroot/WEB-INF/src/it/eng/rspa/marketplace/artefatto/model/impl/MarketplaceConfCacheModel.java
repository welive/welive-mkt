/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing MarketplaceConf in entity cache.
 *
 * @author eng
 * @see MarketplaceConf
 * @generated
 */
public class MarketplaceConfCacheModel implements CacheModel<MarketplaceConf>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{confId=");
		sb.append(confId);
		sb.append(", key=");
		sb.append(key);
		sb.append(", value=");
		sb.append(value);
		sb.append(", type=");
		sb.append(type);
		sb.append(", options=");
		sb.append(options);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MarketplaceConf toEntityModel() {
		MarketplaceConfImpl marketplaceConfImpl = new MarketplaceConfImpl();

		marketplaceConfImpl.setConfId(confId);

		if (key == null) {
			marketplaceConfImpl.setKey(StringPool.BLANK);
		}
		else {
			marketplaceConfImpl.setKey(key);
		}

		if (value == null) {
			marketplaceConfImpl.setValue(StringPool.BLANK);
		}
		else {
			marketplaceConfImpl.setValue(value);
		}

		if (type == null) {
			marketplaceConfImpl.setType(StringPool.BLANK);
		}
		else {
			marketplaceConfImpl.setType(type);
		}

		if (options == null) {
			marketplaceConfImpl.setOptions(StringPool.BLANK);
		}
		else {
			marketplaceConfImpl.setOptions(options);
		}

		marketplaceConfImpl.resetOriginalValues();

		return marketplaceConfImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		confId = objectInput.readLong();
		key = objectInput.readUTF();
		value = objectInput.readUTF();
		type = objectInput.readUTF();
		options = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(confId);

		if (key == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(key);
		}

		if (value == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(value);
		}

		if (type == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (options == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(options);
		}
	}

	public long confId;
	public String key;
	public String value;
	public String type;
	public String options;
}