/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the artefatto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtefattoPersistence
 * @see ArtefattoUtil
 * @generated
 */
public class ArtefattoPersistenceImpl extends BasePersistenceImpl<Artefatto>
	implements ArtefattoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ArtefattoUtil} to access the artefatto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ArtefattoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.UUID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(uuid, artefatto.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUuid_First(uuid, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUuid_Last(uuid, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where uuid = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByUuid_PrevAndNext(long artefattoId, String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, artefatto, uuid,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByUuid_PrevAndNext(session, artefatto, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByUuid_PrevAndNext(Session session,
		Artefatto artefatto, String uuid, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (Artefatto artefatto : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "artefatto.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "artefatto.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(artefatto.uuid IS NULL OR artefatto.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			ArtefattoModelImpl.UUID_COLUMN_BITMASK |
			ArtefattoModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the artefatto where uuid = &#63; and groupId = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUUID_G(String uuid, long groupId)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUUID_G(uuid, groupId);

		if (artefatto == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchArtefattoException(msg.toString());
		}

		return artefatto;
	}

	/**
	 * Returns the artefatto where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the artefatto where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof Artefatto) {
			Artefatto artefatto = (Artefatto)result;

			if (!Validator.equals(uuid, artefatto.getUuid()) ||
					(groupId != artefatto.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<Artefatto> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					Artefatto artefatto = list.get(0);

					result = artefatto;

					cacheResult(artefatto);

					if ((artefatto.getUuid() == null) ||
							!artefatto.getUuid().equals(uuid) ||
							(artefatto.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, artefatto);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Artefatto)result;
		}
	}

	/**
	 * Removes the artefatto where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the artefatto that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto removeByUUID_G(String uuid, long groupId)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByUUID_G(uuid, groupId);

		return remove(artefatto);
	}

	/**
	 * Returns the number of artefattos where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "artefatto.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "artefatto.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(artefatto.uuid IS NULL OR artefatto.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "artefatto.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ArtefattoModelImpl.UUID_COLUMN_BITMASK |
			ArtefattoModelImpl.COMPANYID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the artefattos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid_C(String uuid, long companyId, int start,
		int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(uuid, artefatto.getUuid()) ||
						(companyId != artefatto.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByUuid_C_PrevAndNext(long artefattoId, String uuid,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, artefatto, uuid,
					companyId, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByUuid_C_PrevAndNext(session, artefatto, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByUuid_C_PrevAndNext(Session session,
		Artefatto artefatto, String uuid, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (Artefatto artefatto : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "artefatto.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "artefatto.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(artefatto.uuid IS NULL OR artefatto.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "artefatto.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			ArtefattoModelImpl.GROUPID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artefattos where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByGroupId(long groupId)
		throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((groupId != artefatto.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByGroupId_First(groupId, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByGroupId_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByGroupId_Last(groupId, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByGroupId_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where groupId = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByGroupId_PrevAndNext(long artefattoId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, artefatto, groupId,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByGroupId_PrevAndNext(session, artefatto, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByGroupId_PrevAndNext(Session session,
		Artefatto artefatto, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroupId(long groupId) throws SystemException {
		for (Artefatto artefatto : findByGroupId(groupId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "artefatto.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			ArtefattoModelImpl.COMPANYID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artefattos where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the artefattos where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCompanyId(long companyId, int start, int end)
		throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCompanyId(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((companyId != artefatto.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCompanyId_Last(companyId, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where companyId = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByCompanyId_PrevAndNext(long artefattoId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, artefatto,
					companyId, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByCompanyId_PrevAndNext(session, artefatto,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByCompanyId_PrevAndNext(Session session,
		Artefatto artefatto, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (Artefatto artefatto : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "artefatto.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId_Status",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId_Status",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ArtefattoModelImpl.USERID_COLUMN_BITMASK |
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID_STATUS = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId_Status",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the artefattos where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId_Status(long userId, int status)
		throws SystemException {
		return findByUserId_Status(userId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId_Status(long userId, int status,
		int start, int end) throws SystemException {
		return findByUserId_Status(userId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId_Status(long userId, int status,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID_STATUS;
			finderArgs = new Object[] { userId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID_STATUS;
			finderArgs = new Object[] {
					userId, status,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((userId != artefatto.getUserId()) ||
						(status != artefatto.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_USERID_STATUS_USERID_2);

			query.append(_FINDER_COLUMN_USERID_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(status);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUserId_Status_First(long userId, int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUserId_Status_First(userId, status,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUserId_Status_First(long userId, int status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByUserId_Status(userId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUserId_Status_Last(long userId, int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUserId_Status_Last(userId, status,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUserId_Status_Last(long userId, int status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserId_Status(userId, status);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByUserId_Status(userId, status, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where userId = &#63; and status = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByUserId_Status_PrevAndNext(long artefattoId,
		long userId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByUserId_Status_PrevAndNext(session, artefatto,
					userId, status, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByUserId_Status_PrevAndNext(session, artefatto,
					userId, status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByUserId_Status_PrevAndNext(Session session,
		Artefatto artefatto, long userId, int status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_USERID_STATUS_USERID_2);

		query.append(_FINDER_COLUMN_USERID_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where userId = &#63; and status = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserId_Status(long userId, int status)
		throws SystemException {
		for (Artefatto artefatto : findByUserId_Status(userId, status,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where userId = &#63; and status = &#63;.
	 *
	 * @param userId the user ID
	 * @param status the status
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId_Status(long userId, int status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID_STATUS;

		Object[] finderArgs = new Object[] { userId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_USERID_STATUS_USERID_2);

			query.append(_FINDER_COLUMN_USERID_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_STATUS_USERID_2 = "artefatto.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERID_STATUS_STATUS_2 = "artefatto.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			ArtefattoModelImpl.USERID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artefattos where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((userId != artefatto.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUserId_First(userId, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByUserId_Last(userId, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByUserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where userId = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByUserId_PrevAndNext(long artefattoId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByUserId_PrevAndNext(session, artefatto, userId,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByUserId_PrevAndNext(session, artefatto, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByUserId_PrevAndNext(Session session,
		Artefatto artefatto, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserId(long userId) throws SystemException {
		for (Artefatto artefatto : findByUserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "artefatto.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitle",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTitle",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.TITLE_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TITLE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTitle",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where title = &#63;.
	 *
	 * @param title the title
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByTitle(String title) throws SystemException {
		return findByTitle(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where title = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByTitle(String title, int start, int end)
		throws SystemException {
		return findByTitle(title, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where title = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByTitle(String title, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE;
			finderArgs = new Object[] { title };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE;
			finderArgs = new Object[] { title, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(title, artefatto.getTitle())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitle) {
					qPos.add(title);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByTitle_First(String title,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByTitle_First(title, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("title=");
		msg.append(title);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByTitle_First(String title,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByTitle(title, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByTitle_Last(String title,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByTitle_Last(title, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("title=");
		msg.append(title);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByTitle_Last(String title,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTitle(title);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByTitle(title, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where title = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByTitle_PrevAndNext(long artefattoId, String title,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByTitle_PrevAndNext(session, artefatto, title,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByTitle_PrevAndNext(session, artefatto, title,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByTitle_PrevAndNext(Session session,
		Artefatto artefatto, String title, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindTitle = false;

		if (title == null) {
			query.append(_FINDER_COLUMN_TITLE_TITLE_1);
		}
		else if (title.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TITLE_TITLE_3);
		}
		else {
			bindTitle = true;

			query.append(_FINDER_COLUMN_TITLE_TITLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTitle) {
			qPos.add(title);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where title = &#63; from the database.
	 *
	 * @param title the title
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByTitle(String title) throws SystemException {
		for (Artefatto artefatto : findByTitle(title, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where title = &#63;.
	 *
	 * @param title the title
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTitle(String title) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TITLE;

		Object[] finderArgs = new Object[] { title };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitle) {
					qPos.add(title);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TITLE_TITLE_1 = "artefatto.title IS NULL";
	private static final String _FINDER_COLUMN_TITLE_TITLE_2 = "artefatto.title = ?";
	private static final String _FINDER_COLUMN_TITLE_TITLE_3 = "(artefatto.title IS NULL OR artefatto.title = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNER = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOwner",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNER = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOwner",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.OWNER_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_OWNER = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOwner",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where owner = &#63;.
	 *
	 * @param owner the owner
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByOwner(String owner) throws SystemException {
		return findByOwner(owner, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where owner = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param owner the owner
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByOwner(String owner, int start, int end)
		throws SystemException {
		return findByOwner(owner, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where owner = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param owner the owner
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByOwner(String owner, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNER;
			finderArgs = new Object[] { owner };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNER;
			finderArgs = new Object[] { owner, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(owner, artefatto.getOwner())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindOwner = false;

			if (owner == null) {
				query.append(_FINDER_COLUMN_OWNER_OWNER_1);
			}
			else if (owner.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_OWNER_OWNER_3);
			}
			else {
				bindOwner = true;

				query.append(_FINDER_COLUMN_OWNER_OWNER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOwner) {
					qPos.add(owner);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where owner = &#63;.
	 *
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByOwner_First(String owner,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByOwner_First(owner, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("owner=");
		msg.append(owner);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where owner = &#63;.
	 *
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByOwner_First(String owner,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByOwner(owner, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where owner = &#63;.
	 *
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByOwner_Last(String owner,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByOwner_Last(owner, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("owner=");
		msg.append(owner);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where owner = &#63;.
	 *
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByOwner_Last(String owner,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOwner(owner);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByOwner(owner, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where owner = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByOwner_PrevAndNext(long artefattoId, String owner,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByOwner_PrevAndNext(session, artefatto, owner,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByOwner_PrevAndNext(session, artefatto, owner,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByOwner_PrevAndNext(Session session,
		Artefatto artefatto, String owner, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindOwner = false;

		if (owner == null) {
			query.append(_FINDER_COLUMN_OWNER_OWNER_1);
		}
		else if (owner.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_OWNER_OWNER_3);
		}
		else {
			bindOwner = true;

			query.append(_FINDER_COLUMN_OWNER_OWNER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindOwner) {
			qPos.add(owner);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where owner = &#63; from the database.
	 *
	 * @param owner the owner
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOwner(String owner) throws SystemException {
		for (Artefatto artefatto : findByOwner(owner, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where owner = &#63;.
	 *
	 * @param owner the owner
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOwner(String owner) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_OWNER;

		Object[] finderArgs = new Object[] { owner };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindOwner = false;

			if (owner == null) {
				query.append(_FINDER_COLUMN_OWNER_OWNER_1);
			}
			else if (owner.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_OWNER_OWNER_3);
			}
			else {
				bindOwner = true;

				query.append(_FINDER_COLUMN_OWNER_OWNER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOwner) {
					qPos.add(owner);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_OWNER_OWNER_1 = "artefatto.owner IS NULL";
	private static final String _FINDER_COLUMN_OWNER_OWNER_2 = "artefatto.owner = ?";
	private static final String _FINDER_COLUMN_OWNER_OWNER_3 = "(artefatto.owner IS NULL OR artefatto.owner = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDOWNER =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBystatusAndOwner",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDOWNER =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBystatusAndOwner",
			new String[] { Integer.class.getName(), String.class.getName() },
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.OWNER_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUSANDOWNER = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBystatusAndOwner",
			new String[] { Integer.class.getName(), String.class.getName() });

	/**
	 * Returns all the artefattos where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndOwner(int status, String owner)
		throws SystemException {
		return findBystatusAndOwner(status, owner, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where status = &#63; and owner = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndOwner(int status, String owner,
		int start, int end) throws SystemException {
		return findBystatusAndOwner(status, owner, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where status = &#63; and owner = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndOwner(int status, String owner,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDOWNER;
			finderArgs = new Object[] { status, owner };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDOWNER;
			finderArgs = new Object[] {
					status, owner,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((status != artefatto.getStatus()) ||
						!Validator.equals(owner, artefatto.getOwner())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDOWNER_STATUS_2);

			boolean bindOwner = false;

			if (owner == null) {
				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_1);
			}
			else if (owner.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_3);
			}
			else {
				bindOwner = true;

				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindOwner) {
					qPos.add(owner);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndOwner_First(int status, String owner,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndOwner_First(status, owner,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", owner=");
		msg.append(owner);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndOwner_First(int status, String owner,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBystatusAndOwner(status, owner, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndOwner_Last(int status, String owner,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndOwner_Last(status, owner,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", owner=");
		msg.append(owner);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndOwner_Last(int status, String owner,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBystatusAndOwner(status, owner);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBystatusAndOwner(status, owner, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and owner = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param status the status
	 * @param owner the owner
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBystatusAndOwner_PrevAndNext(long artefattoId,
		int status, String owner, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBystatusAndOwner_PrevAndNext(session, artefatto,
					status, owner, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBystatusAndOwner_PrevAndNext(session, artefatto,
					status, owner, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBystatusAndOwner_PrevAndNext(Session session,
		Artefatto artefatto, int status, String owner,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_STATUSANDOWNER_STATUS_2);

		boolean bindOwner = false;

		if (owner == null) {
			query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_1);
		}
		else if (owner.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_3);
		}
		else {
			bindOwner = true;

			query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (bindOwner) {
			qPos.add(owner);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where status = &#63; and owner = &#63; from the database.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBystatusAndOwner(int status, String owner)
		throws SystemException {
		for (Artefatto artefatto : findBystatusAndOwner(status, owner,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where status = &#63; and owner = &#63;.
	 *
	 * @param status the status
	 * @param owner the owner
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBystatusAndOwner(int status, String owner)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUSANDOWNER;

		Object[] finderArgs = new Object[] { status, owner };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDOWNER_STATUS_2);

			boolean bindOwner = false;

			if (owner == null) {
				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_1);
			}
			else if (owner.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_3);
			}
			else {
				bindOwner = true;

				query.append(_FINDER_COLUMN_STATUSANDOWNER_OWNER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindOwner) {
					qPos.add(owner);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUSANDOWNER_STATUS_2 = "artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_STATUSANDOWNER_OWNER_1 = "artefatto.owner IS NULL";
	private static final String _FINDER_COLUMN_STATUSANDOWNER_OWNER_2 = "artefatto.owner = ?";
	private static final String _FINDER_COLUMN_STATUSANDOWNER_OWNER_3 = "(artefatto.owner IS NULL OR artefatto.owner = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RESOURCERDF =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByResourceRDF",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESOURCERDF =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByResourceRDF",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.RESOURCERDF_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RESOURCERDF = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByResourceRDF",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByResourceRDF(String resourceRDF)
		throws SystemException {
		return findByResourceRDF(resourceRDF, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where resourceRDF = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param resourceRDF the resource r d f
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByResourceRDF(String resourceRDF, int start,
		int end) throws SystemException {
		return findByResourceRDF(resourceRDF, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where resourceRDF = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param resourceRDF the resource r d f
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByResourceRDF(String resourceRDF, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESOURCERDF;
			finderArgs = new Object[] { resourceRDF };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RESOURCERDF;
			finderArgs = new Object[] { resourceRDF, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(resourceRDF, artefatto.getResourceRDF())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindResourceRDF = false;

			if (resourceRDF == null) {
				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_1);
			}
			else if (resourceRDF.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_3);
			}
			else {
				bindResourceRDF = true;

				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindResourceRDF) {
					qPos.add(resourceRDF);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByResourceRDF_First(String resourceRDF,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByResourceRDF_First(resourceRDF,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("resourceRDF=");
		msg.append(resourceRDF);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByResourceRDF_First(String resourceRDF,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByResourceRDF(resourceRDF, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByResourceRDF_Last(String resourceRDF,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByResourceRDF_Last(resourceRDF,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("resourceRDF=");
		msg.append(resourceRDF);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByResourceRDF_Last(String resourceRDF,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByResourceRDF(resourceRDF);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByResourceRDF(resourceRDF, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where resourceRDF = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param resourceRDF the resource r d f
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByResourceRDF_PrevAndNext(long artefattoId,
		String resourceRDF, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByResourceRDF_PrevAndNext(session, artefatto,
					resourceRDF, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByResourceRDF_PrevAndNext(session, artefatto,
					resourceRDF, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByResourceRDF_PrevAndNext(Session session,
		Artefatto artefatto, String resourceRDF,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindResourceRDF = false;

		if (resourceRDF == null) {
			query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_1);
		}
		else if (resourceRDF.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_3);
		}
		else {
			bindResourceRDF = true;

			query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindResourceRDF) {
			qPos.add(resourceRDF);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where resourceRDF = &#63; from the database.
	 *
	 * @param resourceRDF the resource r d f
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByResourceRDF(String resourceRDF)
		throws SystemException {
		for (Artefatto artefatto : findByResourceRDF(resourceRDF,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where resourceRDF = &#63;.
	 *
	 * @param resourceRDF the resource r d f
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByResourceRDF(String resourceRDF) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RESOURCERDF;

		Object[] finderArgs = new Object[] { resourceRDF };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindResourceRDF = false;

			if (resourceRDF == null) {
				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_1);
			}
			else if (resourceRDF.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_3);
			}
			else {
				bindResourceRDF = true;

				query.append(_FINDER_COLUMN_RESOURCERDF_RESOURCERDF_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindResourceRDF) {
					qPos.add(resourceRDF);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RESOURCERDF_RESOURCERDF_1 = "artefatto.resourceRDF IS NULL";
	private static final String _FINDER_COLUMN_RESOURCERDF_RESOURCERDF_2 = "artefatto.resourceRDF = ?";
	private static final String _FINDER_COLUMN_RESOURCERDF_RESOURCERDF_3 = "(artefatto.resourceRDF IS NULL OR artefatto.resourceRDF = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROVIDERNAME =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProviderName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROVIDERNAME =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProviderName",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.PROVIDERNAME_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROVIDERNAME = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProviderName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByProviderName(String providerName)
		throws SystemException {
		return findByProviderName(providerName, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where providerName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param providerName the provider name
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByProviderName(String providerName, int start,
		int end) throws SystemException {
		return findByProviderName(providerName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where providerName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param providerName the provider name
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByProviderName(String providerName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROVIDERNAME;
			finderArgs = new Object[] { providerName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROVIDERNAME;
			finderArgs = new Object[] {
					providerName,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(providerName, artefatto.getProviderName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindProviderName = false;

			if (providerName == null) {
				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_1);
			}
			else if (providerName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_3);
			}
			else {
				bindProviderName = true;

				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindProviderName) {
					qPos.add(providerName);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByProviderName_First(String providerName,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByProviderName_First(providerName,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("providerName=");
		msg.append(providerName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByProviderName_First(String providerName,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByProviderName(providerName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByProviderName_Last(String providerName,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByProviderName_Last(providerName,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("providerName=");
		msg.append(providerName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByProviderName_Last(String providerName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByProviderName(providerName);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByProviderName(providerName, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where providerName = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param providerName the provider name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByProviderName_PrevAndNext(long artefattoId,
		String providerName, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByProviderName_PrevAndNext(session, artefatto,
					providerName, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByProviderName_PrevAndNext(session, artefatto,
					providerName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByProviderName_PrevAndNext(Session session,
		Artefatto artefatto, String providerName,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindProviderName = false;

		if (providerName == null) {
			query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_1);
		}
		else if (providerName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_3);
		}
		else {
			bindProviderName = true;

			query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindProviderName) {
			qPos.add(providerName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where providerName = &#63; from the database.
	 *
	 * @param providerName the provider name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByProviderName(String providerName)
		throws SystemException {
		for (Artefatto artefatto : findByProviderName(providerName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where providerName = &#63;.
	 *
	 * @param providerName the provider name
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByProviderName(String providerName)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROVIDERNAME;

		Object[] finderArgs = new Object[] { providerName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindProviderName = false;

			if (providerName == null) {
				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_1);
			}
			else if (providerName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_3);
			}
			else {
				bindProviderName = true;

				query.append(_FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindProviderName) {
					qPos.add(providerName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_1 = "artefatto.providerName IS NULL";
	private static final String _FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_2 = "artefatto.providerName = ?";
	private static final String _FINDER_COLUMN_PROVIDERNAME_PROVIDERNAME_3 = "(artefatto.providerName IS NULL OR artefatto.providerName = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoria",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCategoria",
			new String[] { Long.class.getName() },
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORIA = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCategoria",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the artefattos where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria(long categoriamkpId)
		throws SystemException {
		return findByCategoria(categoriamkpId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where categoriamkpId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria(long categoriamkpId, int start,
		int end) throws SystemException {
		return findByCategoria(categoriamkpId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where categoriamkpId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria(long categoriamkpId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA;
			finderArgs = new Object[] { categoriamkpId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA;
			finderArgs = new Object[] {
					categoriamkpId,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((categoriamkpId != artefatto.getCategoriamkpId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORIA_CATEGORIAMKPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriamkpId);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCategoria_First(long categoriamkpId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCategoria_First(categoriamkpId,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCategoria_First(long categoriamkpId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByCategoria(categoriamkpId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCategoria_Last(long categoriamkpId,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCategoria_Last(categoriamkpId,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCategoria_Last(long categoriamkpId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCategoria(categoriamkpId);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByCategoria(categoriamkpId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where categoriamkpId = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param categoriamkpId the categoriamkp ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByCategoria_PrevAndNext(long artefattoId,
		long categoriamkpId, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByCategoria_PrevAndNext(session, artefatto,
					categoriamkpId, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByCategoria_PrevAndNext(session, artefatto,
					categoriamkpId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByCategoria_PrevAndNext(Session session,
		Artefatto artefatto, long categoriamkpId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_CATEGORIA_CATEGORIAMKPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(categoriamkpId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where categoriamkpId = &#63; from the database.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCategoria(long categoriamkpId)
		throws SystemException {
		for (Artefatto artefatto : findByCategoria(categoriamkpId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where categoriamkpId = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCategoria(long categoriamkpId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CATEGORIA;

		Object[] finderArgs = new Object[] { categoriamkpId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORIA_CATEGORIAMKPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriamkpId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORIA_CATEGORIAMKPID_2 = "artefatto.categoriamkpId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoria_Status",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCategoria_Status",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORIA_STATUS = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCategoria_Status",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the artefattos where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria_Status(long categoriamkpId,
		int status) throws SystemException {
		return findByCategoria_Status(categoriamkpId, status,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where categoriamkpId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria_Status(long categoriamkpId,
		int status, int start, int end) throws SystemException {
		return findByCategoria_Status(categoriamkpId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where categoriamkpId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByCategoria_Status(long categoriamkpId,
		int status, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA_STATUS;
			finderArgs = new Object[] { categoriamkpId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORIA_STATUS;
			finderArgs = new Object[] {
					categoriamkpId, status,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((categoriamkpId != artefatto.getCategoriamkpId()) ||
						(status != artefatto.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORIA_STATUS_CATEGORIAMKPID_2);

			query.append(_FINDER_COLUMN_CATEGORIA_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriamkpId);

				qPos.add(status);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCategoria_Status_First(long categoriamkpId,
		int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCategoria_Status_First(categoriamkpId,
				status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCategoria_Status_First(long categoriamkpId,
		int status, OrderByComparator orderByComparator)
		throws SystemException {
		List<Artefatto> list = findByCategoria_Status(categoriamkpId, status,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByCategoria_Status_Last(long categoriamkpId,
		int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByCategoria_Status_Last(categoriamkpId,
				status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByCategoria_Status_Last(long categoriamkpId,
		int status, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCategoria_Status(categoriamkpId, status);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByCategoria_Status(categoriamkpId, status,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByCategoria_Status_PrevAndNext(long artefattoId,
		long categoriamkpId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByCategoria_Status_PrevAndNext(session, artefatto,
					categoriamkpId, status, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByCategoria_Status_PrevAndNext(session, artefatto,
					categoriamkpId, status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByCategoria_Status_PrevAndNext(Session session,
		Artefatto artefatto, long categoriamkpId, int status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_CATEGORIA_STATUS_CATEGORIAMKPID_2);

		query.append(_FINDER_COLUMN_CATEGORIA_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(categoriamkpId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where categoriamkpId = &#63; and status = &#63; from the database.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCategoria_Status(long categoriamkpId, int status)
		throws SystemException {
		for (Artefatto artefatto : findByCategoria_Status(categoriamkpId,
				status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCategoria_Status(long categoriamkpId, int status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CATEGORIA_STATUS;

		Object[] finderArgs = new Object[] { categoriamkpId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORIA_STATUS_CATEGORIAMKPID_2);

			query.append(_FINDER_COLUMN_CATEGORIA_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriamkpId);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORIA_STATUS_CATEGORIAMKPID_2 =
		"artefatto.categoriamkpId = ? AND ";
	private static final String _FINDER_COLUMN_CATEGORIA_STATUS_STATUS_2 = "artefatto.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByPilot_Categoria_Status",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByPilot_Categoria_Status",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName()
			},
			ArtefattoModelImpl.PILOTID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PILOT_CATEGORIA_STATUS = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByPilot_Categoria_Status",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName()
			});

	/**
	 * Returns all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByPilot_Categoria_Status(String pilotid,
		long categoriamkpId, int status) throws SystemException {
		return findByPilot_Categoria_Status(pilotid, categoriamkpId, status,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByPilot_Categoria_Status(String pilotid,
		long categoriamkpId, int status, int start, int end)
		throws SystemException {
		return findByPilot_Categoria_Status(pilotid, categoriamkpId, status,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByPilot_Categoria_Status(String pilotid,
		long categoriamkpId, int status, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS;
			finderArgs = new Object[] { pilotid, categoriamkpId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS;
			finderArgs = new Object[] {
					pilotid, categoriamkpId, status,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(pilotid, artefatto.getPilotid()) ||
						(categoriamkpId != artefatto.getCategoriamkpId()) ||
						(status != artefatto.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_CATEGORIAMKPID_2);

			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(categoriamkpId);

				qPos.add(status);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByPilot_Categoria_Status_First(String pilotid,
		long categoriamkpId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByPilot_Categoria_Status_First(pilotid,
				categoriamkpId, status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByPilot_Categoria_Status_First(String pilotid,
		long categoriamkpId, int status, OrderByComparator orderByComparator)
		throws SystemException {
		List<Artefatto> list = findByPilot_Categoria_Status(pilotid,
				categoriamkpId, status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByPilot_Categoria_Status_Last(String pilotid,
		long categoriamkpId, int status, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByPilot_Categoria_Status_Last(pilotid,
				categoriamkpId, status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByPilot_Categoria_Status_Last(String pilotid,
		long categoriamkpId, int status, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByPilot_Categoria_Status(pilotid, categoriamkpId,
				status);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByPilot_Categoria_Status(pilotid,
				categoriamkpId, status, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByPilot_Categoria_Status_PrevAndNext(
		long artefattoId, String pilotid, long categoriamkpId, int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByPilot_Categoria_Status_PrevAndNext(session,
					artefatto, pilotid, categoriamkpId, status,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByPilot_Categoria_Status_PrevAndNext(session,
					artefatto, pilotid, categoriamkpId, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByPilot_Categoria_Status_PrevAndNext(
		Session session, Artefatto artefatto, String pilotid,
		long categoriamkpId, int status, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindPilotid = false;

		if (pilotid == null) {
			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_1);
		}
		else if (pilotid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_3);
		}
		else {
			bindPilotid = true;

			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_2);
		}

		query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_CATEGORIAMKPID_2);

		query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPilotid) {
			qPos.add(pilotid);
		}

		qPos.add(categoriamkpId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63; from the database.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByPilot_Categoria_Status(String pilotid,
		long categoriamkpId, int status) throws SystemException {
		for (Artefatto artefatto : findByPilot_Categoria_Status(pilotid,
				categoriamkpId, status, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param categoriamkpId the categoriamkp ID
	 * @param status the status
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPilot_Categoria_Status(String pilotid,
		long categoriamkpId, int status) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PILOT_CATEGORIA_STATUS;

		Object[] finderArgs = new Object[] { pilotid, categoriamkpId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_CATEGORIAMKPID_2);

			query.append(_FINDER_COLUMN_PILOT_CATEGORIA_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(categoriamkpId);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_1 = "artefatto.pilotid IS NULL AND ";
	private static final String _FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_2 = "artefatto.pilotid = ? AND ";
	private static final String _FINDER_COLUMN_PILOT_CATEGORIA_STATUS_PILOTID_3 = "(artefatto.pilotid IS NULL OR artefatto.pilotid = '') AND ";
	private static final String _FINDER_COLUMN_PILOT_CATEGORIA_STATUS_CATEGORIAMKPID_2 =
		"artefatto.categoriamkpId = ? AND ";
	private static final String _FINDER_COLUMN_PILOT_CATEGORIA_STATUS_STATUS_2 = "artefatto.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypilotId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypilotId",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.PILOTID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PILOTID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypilotId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotId(String pilotid)
		throws SystemException {
		return findBypilotId(pilotid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where pilotid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotId(String pilotid, int start, int end)
		throws SystemException {
		return findBypilotId(pilotid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where pilotid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotId(String pilotid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTID;
			finderArgs = new Object[] { pilotid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTID;
			finderArgs = new Object[] { pilotid, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(pilotid, artefatto.getPilotid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTID_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTID_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTID_PILOTID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotId_First(String pilotid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotId_First(pilotid, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotId_First(String pilotid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBypilotId(pilotid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotId_Last(String pilotid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotId_Last(pilotid, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotId_Last(String pilotid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBypilotId(pilotid);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBypilotId(pilotid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBypilotId_PrevAndNext(long artefattoId,
		String pilotid, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBypilotId_PrevAndNext(session, artefatto, pilotid,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBypilotId_PrevAndNext(session, artefatto, pilotid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBypilotId_PrevAndNext(Session session,
		Artefatto artefatto, String pilotid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindPilotid = false;

		if (pilotid == null) {
			query.append(_FINDER_COLUMN_PILOTID_PILOTID_1);
		}
		else if (pilotid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOTID_PILOTID_3);
		}
		else {
			bindPilotid = true;

			query.append(_FINDER_COLUMN_PILOTID_PILOTID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPilotid) {
			qPos.add(pilotid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where pilotid = &#63; from the database.
	 *
	 * @param pilotid the pilotid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypilotId(String pilotid) throws SystemException {
		for (Artefatto artefatto : findBypilotId(pilotid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where pilotid = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypilotId(String pilotid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PILOTID;

		Object[] finderArgs = new Object[] { pilotid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTID_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTID_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTID_PILOTID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PILOTID_PILOTID_1 = "artefatto.pilotid IS NULL";
	private static final String _FINDER_COLUMN_PILOTID_PILOTID_2 = "artefatto.pilotid = ?";
	private static final String _FINDER_COLUMN_PILOTID_PILOTID_3 = "(artefatto.pilotid IS NULL OR artefatto.pilotid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStatus",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatus",
			new String[] { Integer.class.getName() },
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUS = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStatus",
			new String[] { Integer.class.getName() });

	/**
	 * Returns all the artefattos where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByStatus(int status) throws SystemException {
		return findByStatus(status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByStatus(int status, int start, int end)
		throws SystemException {
		return findByStatus(status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findByStatus(int status, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] { status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] { status, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((status != artefatto.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByStatus_First(int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByStatus_First(status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByStatus_First(int status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findByStatus(status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByStatus_Last(int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByStatus_Last(status, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByStatus_Last(int status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStatus(status);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findByStatus(status, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where status = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findByStatus_PrevAndNext(long artefattoId, int status,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getByStatus_PrevAndNext(session, artefatto, status,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getByStatus_PrevAndNext(session, artefatto, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getByStatus_PrevAndNext(Session session,
		Artefatto artefatto, int status, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where status = &#63; from the database.
	 *
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByStatus(int status) throws SystemException {
		for (Artefatto artefatto : findByStatus(status, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByStatus(int status) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUS;

		Object[] finderArgs = new Object[] { status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUS_STATUS_2 = "artefatto.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDPILOTID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBystatusAndpilotId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPILOTID =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBystatusAndpilotId",
			new String[] { Integer.class.getName(), String.class.getName() },
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.PILOTID_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUSANDPILOTID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBystatusAndpilotId",
			new String[] { Integer.class.getName(), String.class.getName() });

	/**
	 * Returns all the artefattos where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndpilotId(int status, String pilotid)
		throws SystemException {
		return findBystatusAndpilotId(status, pilotid, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where status = &#63; and pilotid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndpilotId(int status, String pilotid,
		int start, int end) throws SystemException {
		return findBystatusAndpilotId(status, pilotid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where status = &#63; and pilotid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndpilotId(int status, String pilotid,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPILOTID;
			finderArgs = new Object[] { status, pilotid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDPILOTID;
			finderArgs = new Object[] {
					status, pilotid,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((status != artefatto.getStatus()) ||
						!Validator.equals(pilotid, artefatto.getPilotid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDPILOTID_STATUS_2);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndpilotId_First(int status, String pilotid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndpilotId_First(status, pilotid,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", pilotid=");
		msg.append(pilotid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndpilotId_First(int status, String pilotid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBystatusAndpilotId(status, pilotid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndpilotId_Last(int status, String pilotid,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndpilotId_Last(status, pilotid,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", pilotid=");
		msg.append(pilotid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndpilotId_Last(int status, String pilotid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBystatusAndpilotId(status, pilotid);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBystatusAndpilotId(status, pilotid,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param status the status
	 * @param pilotid the pilotid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBystatusAndpilotId_PrevAndNext(long artefattoId,
		int status, String pilotid, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBystatusAndpilotId_PrevAndNext(session, artefatto,
					status, pilotid, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBystatusAndpilotId_PrevAndNext(session, artefatto,
					status, pilotid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBystatusAndpilotId_PrevAndNext(Session session,
		Artefatto artefatto, int status, String pilotid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_STATUSANDPILOTID_STATUS_2);

		boolean bindPilotid = false;

		if (pilotid == null) {
			query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_1);
		}
		else if (pilotid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_3);
		}
		else {
			bindPilotid = true;

			query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (bindPilotid) {
			qPos.add(pilotid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where status = &#63; and pilotid = &#63; from the database.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBystatusAndpilotId(int status, String pilotid)
		throws SystemException {
		for (Artefatto artefatto : findBystatusAndpilotId(status, pilotid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where status = &#63; and pilotid = &#63;.
	 *
	 * @param status the status
	 * @param pilotid the pilotid
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBystatusAndpilotId(int status, String pilotid)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUSANDPILOTID;

		Object[] finderArgs = new Object[] { status, pilotid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDPILOTID_STATUS_2);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_STATUSANDPILOTID_PILOTID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUSANDPILOTID_STATUS_2 = "artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_STATUSANDPILOTID_PILOTID_1 = "artefatto.pilotid IS NULL";
	private static final String _FINDER_COLUMN_STATUSANDPILOTID_PILOTID_2 = "artefatto.pilotid = ?";
	private static final String _FINDER_COLUMN_STATUSANDPILOTID_PILOTID_3 = "(artefatto.pilotid IS NULL OR artefatto.pilotid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LANGUAGE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBylanguage",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBylanguage",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.LANGUAGE_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LANGUAGE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBylanguage",
			new String[] { String.class.getName() });

	/**
	 * Returns all the artefattos where language = &#63;.
	 *
	 * @param language the language
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBylanguage(String language)
		throws SystemException {
		return findBylanguage(language, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the artefattos where language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBylanguage(String language, int start, int end)
		throws SystemException {
		return findBylanguage(language, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBylanguage(String language, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LANGUAGE;
			finderArgs = new Object[] { language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LANGUAGE;
			finderArgs = new Object[] { language, start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(language, artefatto.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where language = &#63;.
	 *
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBylanguage_First(String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBylanguage_First(language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where language = &#63;.
	 *
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBylanguage_First(String language,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBylanguage(language, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where language = &#63;.
	 *
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBylanguage_Last(String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBylanguage_Last(language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where language = &#63;.
	 *
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBylanguage_Last(String language,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBylanguage(language);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBylanguage(language, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where language = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBylanguage_PrevAndNext(long artefattoId,
		String language, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBylanguage_PrevAndNext(session, artefatto, language,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBylanguage_PrevAndNext(session, artefatto, language,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBylanguage_PrevAndNext(Session session,
		Artefatto artefatto, String language,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where language = &#63; from the database.
	 *
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBylanguage(String language) throws SystemException {
		for (Artefatto artefatto : findBylanguage(language, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where language = &#63;.
	 *
	 * @param language the language
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBylanguage(String language) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LANGUAGE;

		Object[] finderArgs = new Object[] { language };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_LANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LANGUAGE_LANGUAGE_1 = "artefatto.language IS NULL";
	private static final String _FINDER_COLUMN_LANGUAGE_LANGUAGE_2 = "artefatto.language = ?";
	private static final String _FINDER_COLUMN_LANGUAGE_LANGUAGE_3 = "(artefatto.language IS NULL OR artefatto.language = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBystatusAndLanguage",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBystatusAndLanguage",
			new String[] { Integer.class.getName(), String.class.getName() },
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.LANGUAGE_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUSANDLANGUAGE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBystatusAndLanguage",
			new String[] { Integer.class.getName(), String.class.getName() });

	/**
	 * Returns all the artefattos where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndLanguage(int status, String language)
		throws SystemException {
		return findBystatusAndLanguage(status, language, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where status = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndLanguage(int status, String language,
		int start, int end) throws SystemException {
		return findBystatusAndLanguage(status, language, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where status = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBystatusAndLanguage(int status, String language,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDLANGUAGE;
			finderArgs = new Object[] { status, language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUSANDLANGUAGE;
			finderArgs = new Object[] {
					status, language,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((status != artefatto.getStatus()) ||
						!Validator.equals(language, artefatto.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_STATUS_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndLanguage_First(int status, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndLanguage_First(status, language,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndLanguage_First(int status,
		String language, OrderByComparator orderByComparator)
		throws SystemException {
		List<Artefatto> list = findBystatusAndLanguage(status, language, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBystatusAndLanguage_Last(int status, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBystatusAndLanguage_Last(status, language,
				orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBystatusAndLanguage_Last(int status, String language,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBystatusAndLanguage(status, language);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBystatusAndLanguage(status, language,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and language = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBystatusAndLanguage_PrevAndNext(long artefattoId,
		int status, String language, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBystatusAndLanguage_PrevAndNext(session, artefatto,
					status, language, orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBystatusAndLanguage_PrevAndNext(session, artefatto,
					status, language, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBystatusAndLanguage_PrevAndNext(Session session,
		Artefatto artefatto, int status, String language,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_STATUS_2);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where status = &#63; and language = &#63; from the database.
	 *
	 * @param status the status
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBystatusAndLanguage(int status, String language)
		throws SystemException {
		for (Artefatto artefatto : findBystatusAndLanguage(status, language,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where status = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param language the language
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBystatusAndLanguage(int status, String language)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUSANDLANGUAGE;

		Object[] finderArgs = new Object[] { status, language };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_STATUS_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUSANDLANGUAGE_STATUS_2 = "artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_1 = "artefatto.language IS NULL";
	private static final String _FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_2 = "artefatto.language = ?";
	private static final String _FINDER_COLUMN_STATUSANDLANGUAGE_LANGUAGE_3 = "(artefatto.language IS NULL OR artefatto.language = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycategoryStatusAndLanguage",
			new String[] {
				Integer.class.getName(), Long.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycategoryStatusAndLanguage",
			new String[] {
				Integer.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.LANGUAGE_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBycategoryStatusAndLanguage",
			new String[] {
				Integer.class.getName(), Long.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBycategoryStatusAndLanguage(int status,
		long categoriamkpId, String language) throws SystemException {
		return findBycategoryStatusAndLanguage(status, categoriamkpId,
			language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBycategoryStatusAndLanguage(int status,
		long categoriamkpId, String language, int start, int end)
		throws SystemException {
		return findBycategoryStatusAndLanguage(status, categoriamkpId,
			language, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBycategoryStatusAndLanguage(int status,
		long categoriamkpId, String language, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE;
			finderArgs = new Object[] { status, categoriamkpId, language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE;
			finderArgs = new Object[] {
					status, categoriamkpId, language,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if ((status != artefatto.getStatus()) ||
						(categoriamkpId != artefatto.getCategoriamkpId()) ||
						!Validator.equals(language, artefatto.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_STATUS_2);

			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				qPos.add(categoriamkpId);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBycategoryStatusAndLanguage_First(int status,
		long categoriamkpId, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBycategoryStatusAndLanguage_First(status,
				categoriamkpId, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBycategoryStatusAndLanguage_First(int status,
		long categoriamkpId, String language,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBycategoryStatusAndLanguage(status,
				categoriamkpId, language, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBycategoryStatusAndLanguage_Last(int status,
		long categoriamkpId, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBycategoryStatusAndLanguage_Last(status,
				categoriamkpId, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBycategoryStatusAndLanguage_Last(int status,
		long categoriamkpId, String language,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycategoryStatusAndLanguage(status, categoriamkpId,
				language);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBycategoryStatusAndLanguage(status,
				categoriamkpId, language, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBycategoryStatusAndLanguage_PrevAndNext(
		long artefattoId, int status, long categoriamkpId, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBycategoryStatusAndLanguage_PrevAndNext(session,
					artefatto, status, categoriamkpId, language,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBycategoryStatusAndLanguage_PrevAndNext(session,
					artefatto, status, categoriamkpId, language,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBycategoryStatusAndLanguage_PrevAndNext(
		Session session, Artefatto artefatto, int status, long categoriamkpId,
		String language, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_STATUS_2);

		query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		qPos.add(categoriamkpId);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63; from the database.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBycategoryStatusAndLanguage(int status,
		long categoriamkpId, String language) throws SystemException {
		for (Artefatto artefatto : findBycategoryStatusAndLanguage(status,
				categoriamkpId, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycategoryStatusAndLanguage(int status,
		long categoriamkpId, String language) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CATEGORYSTATUSANDLANGUAGE;

		Object[] finderArgs = new Object[] { status, categoriamkpId, language };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_STATUS_2);

			query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				qPos.add(categoriamkpId);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_STATUS_2 =
		"artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2 =
		"artefatto.categoriamkpId = ? AND ";
	private static final String _FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_1 =
		"artefatto.language IS NULL";
	private static final String _FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_2 =
		"artefatto.language = ?";
	private static final String _FINDER_COLUMN_CATEGORYSTATUSANDLANGUAGE_LANGUAGE_3 =
		"(artefatto.language IS NULL OR artefatto.language = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBypilotStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBypilotStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				String.class.getName()
			},
			ArtefattoModelImpl.PILOTID_COLUMN_BITMASK |
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.LANGUAGE_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PILOTSTATUSANDLANGUAGE = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBypilotStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotStatusAndLanguage(String pilotid,
		int status, String language) throws SystemException {
		return findBypilotStatusAndLanguage(pilotid, status, language,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotStatusAndLanguage(String pilotid,
		int status, String language, int start, int end)
		throws SystemException {
		return findBypilotStatusAndLanguage(pilotid, status, language, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotStatusAndLanguage(String pilotid,
		int status, String language, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE;
			finderArgs = new Object[] { pilotid, status, language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE;
			finderArgs = new Object[] {
					pilotid, status, language,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(pilotid, artefatto.getPilotid()) ||
						(status != artefatto.getStatus()) ||
						!Validator.equals(language, artefatto.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_STATUS_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(status);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotStatusAndLanguage_First(String pilotid,
		int status, String language, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotStatusAndLanguage_First(pilotid,
				status, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", status=");
		msg.append(status);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotStatusAndLanguage_First(String pilotid,
		int status, String language, OrderByComparator orderByComparator)
		throws SystemException {
		List<Artefatto> list = findBypilotStatusAndLanguage(pilotid, status,
				language, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotStatusAndLanguage_Last(String pilotid,
		int status, String language, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotStatusAndLanguage_Last(pilotid,
				status, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", status=");
		msg.append(status);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotStatusAndLanguage_Last(String pilotid,
		int status, String language, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBypilotStatusAndLanguage(pilotid, status, language);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBypilotStatusAndLanguage(pilotid, status,
				language, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBypilotStatusAndLanguage_PrevAndNext(
		long artefattoId, String pilotid, int status, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBypilotStatusAndLanguage_PrevAndNext(session,
					artefatto, pilotid, status, language, orderByComparator,
					true);

			array[1] = artefatto;

			array[2] = getBypilotStatusAndLanguage_PrevAndNext(session,
					artefatto, pilotid, status, language, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBypilotStatusAndLanguage_PrevAndNext(
		Session session, Artefatto artefatto, String pilotid, int status,
		String language, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindPilotid = false;

		if (pilotid == null) {
			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_1);
		}
		else if (pilotid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_3);
		}
		else {
			bindPilotid = true;

			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_2);
		}

		query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_STATUS_2);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPilotid) {
			qPos.add(pilotid);
		}

		qPos.add(status);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where pilotid = &#63; and status = &#63; and language = &#63; from the database.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypilotStatusAndLanguage(String pilotid, int status,
		String language) throws SystemException {
		for (Artefatto artefatto : findBypilotStatusAndLanguage(pilotid,
				status, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param language the language
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypilotStatusAndLanguage(String pilotid, int status,
		String language) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PILOTSTATUSANDLANGUAGE;

		Object[] finderArgs = new Object[] { pilotid, status, language };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_STATUS_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(status);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_1 = "artefatto.pilotid IS NULL AND ";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_2 = "artefatto.pilotid = ? AND ";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_PILOTID_3 = "(artefatto.pilotid IS NULL OR artefatto.pilotid = '') AND ";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_STATUS_2 = "artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_1 =
		"artefatto.language IS NULL";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_2 =
		"artefatto.language = ?";
	private static final String _FINDER_COLUMN_PILOTSTATUSANDLANGUAGE_LANGUAGE_3 =
		"(artefatto.language IS NULL OR artefatto.language = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBypilotCategoryStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBypilotCategoryStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Long.class.getName(), String.class.getName()
			},
			ArtefattoModelImpl.PILOTID_COLUMN_BITMASK |
			ArtefattoModelImpl.STATUS_COLUMN_BITMASK |
			ArtefattoModelImpl.CATEGORIAMKPID_COLUMN_BITMASK |
			ArtefattoModelImpl.LANGUAGE_COLUMN_BITMASK |
			ArtefattoModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PILOTCATEGORYSTATUSANDLANGUAGE =
		new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBypilotCategoryStatusAndLanguage",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Long.class.getName(), String.class.getName()
			});

	/**
	 * Returns all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @return the matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotCategoryStatusAndLanguage(
		String pilotid, int status, long categoriamkpId, String language)
		throws SystemException {
		return findBypilotCategoryStatusAndLanguage(pilotid, status,
			categoriamkpId, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotCategoryStatusAndLanguage(
		String pilotid, int status, long categoriamkpId, String language,
		int start, int end) throws SystemException {
		return findBypilotCategoryStatusAndLanguage(pilotid, status,
			categoriamkpId, language, start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findBypilotCategoryStatusAndLanguage(
		String pilotid, int status, long categoriamkpId, String language,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE;
			finderArgs = new Object[] { pilotid, status, categoriamkpId, language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE;
			finderArgs = new Object[] {
					pilotid, status, categoriamkpId, language,
					
					start, end, orderByComparator
				};
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Artefatto artefatto : list) {
				if (!Validator.equals(pilotid, artefatto.getPilotid()) ||
						(status != artefatto.getStatus()) ||
						(categoriamkpId != artefatto.getCategoriamkpId()) ||
						!Validator.equals(language, artefatto.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_STATUS_2);

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(status);

				qPos.add(categoriamkpId);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotCategoryStatusAndLanguage_First(
		String pilotid, int status, long categoriamkpId, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotCategoryStatusAndLanguage_First(pilotid,
				status, categoriamkpId, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", status=");
		msg.append(status);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotCategoryStatusAndLanguage_First(
		String pilotid, int status, long categoriamkpId, String language,
		OrderByComparator orderByComparator) throws SystemException {
		List<Artefatto> list = findBypilotCategoryStatusAndLanguage(pilotid,
				status, categoriamkpId, language, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findBypilotCategoryStatusAndLanguage_Last(String pilotid,
		int status, long categoriamkpId, String language,
		OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchBypilotCategoryStatusAndLanguage_Last(pilotid,
				status, categoriamkpId, language, orderByComparator);

		if (artefatto != null) {
			return artefatto;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("pilotid=");
		msg.append(pilotid);

		msg.append(", status=");
		msg.append(status);

		msg.append(", categoriamkpId=");
		msg.append(categoriamkpId);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchArtefattoException(msg.toString());
	}

	/**
	 * Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchBypilotCategoryStatusAndLanguage_Last(
		String pilotid, int status, long categoriamkpId, String language,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBypilotCategoryStatusAndLanguage(pilotid, status,
				categoriamkpId, language);

		if (count == 0) {
			return null;
		}

		List<Artefatto> list = findBypilotCategoryStatusAndLanguage(pilotid,
				status, categoriamkpId, language, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param artefattoId the primary key of the current artefatto
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto[] findBypilotCategoryStatusAndLanguage_PrevAndNext(
		long artefattoId, String pilotid, int status, long categoriamkpId,
		String language, OrderByComparator orderByComparator)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByPrimaryKey(artefattoId);

		Session session = null;

		try {
			session = openSession();

			Artefatto[] array = new ArtefattoImpl[3];

			array[0] = getBypilotCategoryStatusAndLanguage_PrevAndNext(session,
					artefatto, pilotid, status, categoriamkpId, language,
					orderByComparator, true);

			array[1] = artefatto;

			array[2] = getBypilotCategoryStatusAndLanguage_PrevAndNext(session,
					artefatto, pilotid, status, categoriamkpId, language,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Artefatto getBypilotCategoryStatusAndLanguage_PrevAndNext(
		Session session, Artefatto artefatto, String pilotid, int status,
		long categoriamkpId, String language,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ARTEFATTO_WHERE);

		boolean bindPilotid = false;

		if (pilotid == null) {
			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_1);
		}
		else if (pilotid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_3);
		}
		else {
			bindPilotid = true;

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_2);
		}

		query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_STATUS_2);

		query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ArtefattoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPilotid) {
			qPos.add(pilotid);
		}

		qPos.add(status);

		qPos.add(categoriamkpId);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(artefatto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Artefatto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63; from the database.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypilotCategoryStatusAndLanguage(String pilotid,
		int status, long categoriamkpId, String language)
		throws SystemException {
		for (Artefatto artefatto : findBypilotCategoryStatusAndLanguage(
				pilotid, status, categoriamkpId, language, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	 *
	 * @param pilotid the pilotid
	 * @param status the status
	 * @param categoriamkpId the categoriamkp ID
	 * @param language the language
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypilotCategoryStatusAndLanguage(String pilotid,
		int status, long categoriamkpId, String language)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PILOTCATEGORYSTATUSANDLANGUAGE;

		Object[] finderArgs = new Object[] {
				pilotid, status, categoriamkpId, language
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindPilotid = false;

			if (pilotid == null) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_1);
			}
			else if (pilotid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_3);
			}
			else {
				bindPilotid = true;

				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_2);
			}

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_STATUS_2);

			query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPilotid) {
					qPos.add(pilotid);
				}

				qPos.add(status);

				qPos.add(categoriamkpId);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_1 =
		"artefatto.pilotid IS NULL AND ";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_2 =
		"artefatto.pilotid = ? AND ";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_PILOTID_3 =
		"(artefatto.pilotid IS NULL OR artefatto.pilotid = '') AND ";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_STATUS_2 =
		"artefatto.status = ? AND ";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_CATEGORIAMKPID_2 =
		"artefatto.categoriamkpId = ? AND ";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_1 =
		"artefatto.language IS NULL";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_2 =
		"artefatto.language = ?";
	private static final String _FINDER_COLUMN_PILOTCATEGORYSTATUSANDLANGUAGE_LANGUAGE_3 =
		"(artefatto.language IS NULL OR artefatto.language = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_EID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, ArtefattoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByeid",
			new String[] { String.class.getName() },
			ArtefattoModelImpl.EID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EID = new FinderPath(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByeid",
			new String[] { String.class.getName() });

	/**
	 * Returns the artefatto where eId = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	 *
	 * @param eId the e ID
	 * @return the matching artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByeid(String eId)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByeid(eId);

		if (artefatto == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("eId=");
			msg.append(eId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchArtefattoException(msg.toString());
		}

		return artefatto;
	}

	/**
	 * Returns the artefatto where eId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param eId the e ID
	 * @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByeid(String eId) throws SystemException {
		return fetchByeid(eId, true);
	}

	/**
	 * Returns the artefatto where eId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param eId the e ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByeid(String eId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { eId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_EID,
					finderArgs, this);
		}

		if (result instanceof Artefatto) {
			Artefatto artefatto = (Artefatto)result;

			if (!Validator.equals(eId, artefatto.getEId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ARTEFATTO_WHERE);

			boolean bindEId = false;

			if (eId == null) {
				query.append(_FINDER_COLUMN_EID_EID_1);
			}
			else if (eId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EID_EID_3);
			}
			else {
				bindEId = true;

				query.append(_FINDER_COLUMN_EID_EID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEId) {
					qPos.add(eId.toLowerCase());
				}

				List<Artefatto> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ArtefattoPersistenceImpl.fetchByeid(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Artefatto artefatto = list.get(0);

					result = artefatto;

					cacheResult(artefatto);

					if ((artefatto.getEId() == null) ||
							!artefatto.getEId().equals(eId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EID,
							finderArgs, artefatto);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Artefatto)result;
		}
	}

	/**
	 * Removes the artefatto where eId = &#63; from the database.
	 *
	 * @param eId the e ID
	 * @return the artefatto that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto removeByeid(String eId)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = findByeid(eId);

		return remove(artefatto);
	}

	/**
	 * Returns the number of artefattos where eId = &#63;.
	 *
	 * @param eId the e ID
	 * @return the number of matching artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByeid(String eId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EID;

		Object[] finderArgs = new Object[] { eId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ARTEFATTO_WHERE);

			boolean bindEId = false;

			if (eId == null) {
				query.append(_FINDER_COLUMN_EID_EID_1);
			}
			else if (eId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EID_EID_3);
			}
			else {
				bindEId = true;

				query.append(_FINDER_COLUMN_EID_EID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEId) {
					qPos.add(eId.toLowerCase());
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EID_EID_1 = "artefatto.eId IS NULL";
	private static final String _FINDER_COLUMN_EID_EID_2 = "lower(artefatto.eId) = ?";
	private static final String _FINDER_COLUMN_EID_EID_3 = "(artefatto.eId IS NULL OR artefatto.eId = '')";

	public ArtefattoPersistenceImpl() {
		setModelClass(Artefatto.class);
	}

	/**
	 * Caches the artefatto in the entity cache if it is enabled.
	 *
	 * @param artefatto the artefatto
	 */
	@Override
	public void cacheResult(Artefatto artefatto) {
		EntityCacheUtil.putResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoImpl.class, artefatto.getPrimaryKey(), artefatto);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { artefatto.getUuid(), artefatto.getGroupId() },
			artefatto);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EID,
			new Object[] { artefatto.getEId() }, artefatto);

		artefatto.resetOriginalValues();
	}

	/**
	 * Caches the artefattos in the entity cache if it is enabled.
	 *
	 * @param artefattos the artefattos
	 */
	@Override
	public void cacheResult(List<Artefatto> artefattos) {
		for (Artefatto artefatto : artefattos) {
			if (EntityCacheUtil.getResult(
						ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
						ArtefattoImpl.class, artefatto.getPrimaryKey()) == null) {
				cacheResult(artefatto);
			}
			else {
				artefatto.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all artefattos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ArtefattoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ArtefattoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the artefatto.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Artefatto artefatto) {
		EntityCacheUtil.removeResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoImpl.class, artefatto.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(artefatto);
	}

	@Override
	public void clearCache(List<Artefatto> artefattos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Artefatto artefatto : artefattos) {
			EntityCacheUtil.removeResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
				ArtefattoImpl.class, artefatto.getPrimaryKey());

			clearUniqueFindersCache(artefatto);
		}
	}

	protected void cacheUniqueFindersCache(Artefatto artefatto) {
		if (artefatto.isNew()) {
			Object[] args = new Object[] {
					artefatto.getUuid(), artefatto.getGroupId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				artefatto);

			args = new Object[] { artefatto.getEId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EID, args, artefatto);
		}
		else {
			ArtefattoModelImpl artefattoModelImpl = (ArtefattoModelImpl)artefatto;

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefatto.getUuid(), artefatto.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					artefatto);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_EID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { artefatto.getEId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EID, args,
					artefatto);
			}
		}
	}

	protected void clearUniqueFindersCache(Artefatto artefatto) {
		ArtefattoModelImpl artefattoModelImpl = (ArtefattoModelImpl)artefatto;

		Object[] args = new Object[] { artefatto.getUuid(), artefatto.getGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((artefattoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					artefattoModelImpl.getOriginalUuid(),
					artefattoModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { artefatto.getEId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EID, args);

		if ((artefattoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_EID.getColumnBitmask()) != 0) {
			args = new Object[] { artefattoModelImpl.getOriginalEId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EID, args);
		}
	}

	/**
	 * Creates a new artefatto with the primary key. Does not add the artefatto to the database.
	 *
	 * @param artefattoId the primary key for the new artefatto
	 * @return the new artefatto
	 */
	@Override
	public Artefatto create(long artefattoId) {
		Artefatto artefatto = new ArtefattoImpl();

		artefatto.setNew(true);
		artefatto.setPrimaryKey(artefattoId);

		String uuid = PortalUUIDUtil.generate();

		artefatto.setUuid(uuid);

		return artefatto;
	}

	/**
	 * Removes the artefatto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param artefattoId the primary key of the artefatto
	 * @return the artefatto that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto remove(long artefattoId)
		throws NoSuchArtefattoException, SystemException {
		return remove((Serializable)artefattoId);
	}

	/**
	 * Removes the artefatto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the artefatto
	 * @return the artefatto that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto remove(Serializable primaryKey)
		throws NoSuchArtefattoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Artefatto artefatto = (Artefatto)session.get(ArtefattoImpl.class,
					primaryKey);

			if (artefatto == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchArtefattoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(artefatto);
		}
		catch (NoSuchArtefattoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Artefatto removeImpl(Artefatto artefatto)
		throws SystemException {
		artefatto = toUnwrappedModel(artefatto);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(artefatto)) {
				artefatto = (Artefatto)session.get(ArtefattoImpl.class,
						artefatto.getPrimaryKeyObj());
			}

			if (artefatto != null) {
				session.delete(artefatto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (artefatto != null) {
			clearCache(artefatto);
		}

		return artefatto;
	}

	@Override
	public Artefatto updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws SystemException {
		artefatto = toUnwrappedModel(artefatto);

		boolean isNew = artefatto.isNew();

		ArtefattoModelImpl artefattoModelImpl = (ArtefattoModelImpl)artefatto;

		if (Validator.isNull(artefatto.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			artefatto.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (artefatto.isNew()) {
				session.save(artefatto);

				artefatto.setNew(false);
			}
			else {
				session.merge(artefatto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ArtefattoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalUuid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { artefattoModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalUuid(),
						artefattoModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						artefattoModelImpl.getUuid(),
						artefattoModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { artefattoModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { artefattoModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalUserId(),
						artefattoModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID_STATUS,
					args);

				args = new Object[] {
						artefattoModelImpl.getUserId(),
						artefattoModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID_STATUS,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { artefattoModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalTitle()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
					args);

				args = new Object[] { artefattoModelImpl.getTitle() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalOwner()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNER,
					args);

				args = new Object[] { artefattoModelImpl.getOwner() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNER,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDOWNER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalOwner()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDOWNER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDOWNER,
					args);

				args = new Object[] {
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getOwner()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDOWNER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDOWNER,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESOURCERDF.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalResourceRDF()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RESOURCERDF,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESOURCERDF,
					args);

				args = new Object[] { artefattoModelImpl.getResourceRDF() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RESOURCERDF,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESOURCERDF,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROVIDERNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalProviderName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROVIDERNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROVIDERNAME,
					args);

				args = new Object[] { artefattoModelImpl.getProviderName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROVIDERNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROVIDERNAME,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalCategoriamkpId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORIA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA,
					args);

				args = new Object[] { artefattoModelImpl.getCategoriamkpId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORIA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalCategoriamkpId(),
						artefattoModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORIA_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA_STATUS,
					args);

				args = new Object[] {
						artefattoModelImpl.getCategoriamkpId(),
						artefattoModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORIA_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORIA_STATUS,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalPilotid(),
						artefattoModelImpl.getOriginalCategoriamkpId(),
						artefattoModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOT_CATEGORIA_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS,
					args);

				args = new Object[] {
						artefattoModelImpl.getPilotid(),
						artefattoModelImpl.getCategoriamkpId(),
						artefattoModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOT_CATEGORIA_STATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOT_CATEGORIA_STATUS,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalPilotid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTID,
					args);

				args = new Object[] { artefattoModelImpl.getPilotid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);

				args = new Object[] { artefattoModelImpl.getStatus() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPILOTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalPilotid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDPILOTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPILOTID,
					args);

				args = new Object[] {
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getPilotid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDPILOTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDPILOTID,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LANGUAGE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LANGUAGE,
					args);

				args = new Object[] { artefattoModelImpl.getLanguage() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LANGUAGE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LANGUAGE,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDLANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDLANGUAGE,
					args);

				args = new Object[] {
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUSANDLANGUAGE,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalCategoriamkpId(),
						artefattoModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORYSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE,
					args);

				args = new Object[] {
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getCategoriamkpId(),
						artefattoModelImpl.getLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORYSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSTATUSANDLANGUAGE,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalPilotid(),
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE,
					args);

				args = new Object[] {
						artefattoModelImpl.getPilotid(),
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTSTATUSANDLANGUAGE,
					args);
			}

			if ((artefattoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						artefattoModelImpl.getOriginalPilotid(),
						artefattoModelImpl.getOriginalStatus(),
						artefattoModelImpl.getOriginalCategoriamkpId(),
						artefattoModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTCATEGORYSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE,
					args);

				args = new Object[] {
						artefattoModelImpl.getPilotid(),
						artefattoModelImpl.getStatus(),
						artefattoModelImpl.getCategoriamkpId(),
						artefattoModelImpl.getLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PILOTCATEGORYSTATUSANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PILOTCATEGORYSTATUSANDLANGUAGE,
					args);
			}
		}

		EntityCacheUtil.putResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
			ArtefattoImpl.class, artefatto.getPrimaryKey(), artefatto);

		clearUniqueFindersCache(artefatto);
		cacheUniqueFindersCache(artefatto);

		return artefatto;
	}

	protected Artefatto toUnwrappedModel(Artefatto artefatto) {
		if (artefatto instanceof ArtefattoImpl) {
			return artefatto;
		}

		ArtefattoImpl artefattoImpl = new ArtefattoImpl();

		artefattoImpl.setNew(artefatto.isNew());
		artefattoImpl.setPrimaryKey(artefatto.getPrimaryKey());

		artefattoImpl.setUuid(artefatto.getUuid());
		artefattoImpl.setArtefattoId(artefatto.getArtefattoId());
		artefattoImpl.setDate(artefatto.getDate());
		artefattoImpl.setTitle(artefatto.getTitle());
		artefattoImpl.setWebpage(artefatto.getWebpage());
		artefattoImpl.setAbstractDescription(artefatto.getAbstractDescription());
		artefattoImpl.setDescription(artefatto.getDescription());
		artefattoImpl.setProviderName(artefatto.getProviderName());
		artefattoImpl.setResourceRDF(artefatto.getResourceRDF());
		artefattoImpl.setRepositoryRDF(artefatto.getRepositoryRDF());
		artefattoImpl.setStatus(artefatto.getStatus());
		artefattoImpl.setCompanyId(artefatto.getCompanyId());
		artefattoImpl.setGroupId(artefatto.getGroupId());
		artefattoImpl.setUserId(artefatto.getUserId());
		artefattoImpl.setOwner(artefatto.getOwner());
		artefattoImpl.setImgId(artefatto.getImgId());
		artefattoImpl.setCategoriamkpId(artefatto.getCategoriamkpId());
		artefattoImpl.setPilotid(artefatto.getPilotid());
		artefattoImpl.setUrl(artefatto.getUrl());
		artefattoImpl.setEId(artefatto.getEId());
		artefattoImpl.setExtRating(artefatto.getExtRating());
		artefattoImpl.setInteractionPoint(artefatto.getInteractionPoint());
		artefattoImpl.setLanguage(artefatto.getLanguage());
		artefattoImpl.setHasMapping(artefatto.isHasMapping());
		artefattoImpl.setIsPrivate(artefatto.isIsPrivate());
		artefattoImpl.setLusdlmodel(artefatto.getLusdlmodel());

		return artefattoImpl;
	}

	/**
	 * Returns the artefatto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the artefatto
	 * @return the artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByPrimaryKey(Serializable primaryKey)
		throws NoSuchArtefattoException, SystemException {
		Artefatto artefatto = fetchByPrimaryKey(primaryKey);

		if (artefatto == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchArtefattoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return artefatto;
	}

	/**
	 * Returns the artefatto with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	 *
	 * @param artefattoId the primary key of the artefatto
	 * @return the artefatto
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto findByPrimaryKey(long artefattoId)
		throws NoSuchArtefattoException, SystemException {
		return findByPrimaryKey((Serializable)artefattoId);
	}

	/**
	 * Returns the artefatto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the artefatto
	 * @return the artefatto, or <code>null</code> if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Artefatto artefatto = (Artefatto)EntityCacheUtil.getResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
				ArtefattoImpl.class, primaryKey);

		if (artefatto == _nullArtefatto) {
			return null;
		}

		if (artefatto == null) {
			Session session = null;

			try {
				session = openSession();

				artefatto = (Artefatto)session.get(ArtefattoImpl.class,
						primaryKey);

				if (artefatto != null) {
					cacheResult(artefatto);
				}
				else {
					EntityCacheUtil.putResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
						ArtefattoImpl.class, primaryKey, _nullArtefatto);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ArtefattoModelImpl.ENTITY_CACHE_ENABLED,
					ArtefattoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return artefatto;
	}

	/**
	 * Returns the artefatto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param artefattoId the primary key of the artefatto
	 * @return the artefatto, or <code>null</code> if a artefatto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Artefatto fetchByPrimaryKey(long artefattoId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)artefattoId);
	}

	/**
	 * Returns all the artefattos.
	 *
	 * @return the artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the artefattos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @return the range of artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the artefattos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of artefattos
	 * @param end the upper bound of the range of artefattos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Artefatto> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Artefatto> list = (List<Artefatto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ARTEFATTO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ARTEFATTO;

				if (pagination) {
					sql = sql.concat(ArtefattoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Artefatto>(list);
				}
				else {
					list = (List<Artefatto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the artefattos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Artefatto artefatto : findAll()) {
			remove(artefatto);
		}
	}

	/**
	 * Returns the number of artefattos.
	 *
	 * @return the number of artefattos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ARTEFATTO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the artefatto persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.Artefatto")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Artefatto>> listenersList = new ArrayList<ModelListener<Artefatto>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Artefatto>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ArtefattoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ARTEFATTO = "SELECT artefatto FROM Artefatto artefatto";
	private static final String _SQL_SELECT_ARTEFATTO_WHERE = "SELECT artefatto FROM Artefatto artefatto WHERE ";
	private static final String _SQL_COUNT_ARTEFATTO = "SELECT COUNT(artefatto) FROM Artefatto artefatto";
	private static final String _SQL_COUNT_ARTEFATTO_WHERE = "SELECT COUNT(artefatto) FROM Artefatto artefatto WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "artefatto.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Artefatto exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Artefatto exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ArtefattoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid", "date"
			});
	private static Artefatto _nullArtefatto = new ArtefattoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Artefatto> toCacheModel() {
				return _nullArtefattoCacheModel;
			}
		};

	private static CacheModel<Artefatto> _nullArtefattoCacheModel = new CacheModel<Artefatto>() {
			@Override
			public Artefatto toEntityModel() {
				return _nullArtefatto;
			}
		};
}