package it.eng.rspa.marketplace.controller;

import java.util.LinkedList;
import java.util.List;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.controller.model.CategoryListItem;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;

public abstract class HomeController extends CommonController{
	
	public static void action(HttpServletRequest request, HttpServletResponse response){

		User user = null;
		try { user = PortalUtil.getUser(request); } 
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("redirectNow", "/");
			return;
		}
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		String pilotId = MyMarketplaceUtility.getPilot(user, request);
		if(pilotId==null){ 
			request.setAttribute("redirectNow", "/");
			return;
		}
		
		boolean canPublish = false;
		if(themeDisplay.isSignedIn() && user!=null && !user.isDefaultUser()){
			canPublish = MyMarketplaceUtility.isDeveloper(user);
		}
		request.setAttribute("canPublish", canPublish);
		
		boolean onlyPilot = request.getParameter("onlyPilot")!=null && Boolean.parseBoolean(request.getParameter("onlyPilot"));
		request.setAttribute("onlyPilot", onlyPilot);
		
		//Imposto il titolo della pagina
		Long cat = request.getParameter("categoryid")!=null ? Long.parseLong(request.getParameter("categoryid")) : 0;
		if(cat == 0)
			request.setAttribute("cat", "all");
		else if(cat == 1)
			request.setAttribute("cat", "bblocks");
		else if(cat == 2)
			request.setAttribute("cat", "psa");
		else if(cat == 3)
			request.setAttribute("cat", "dataset");
		/*
		 * 0: All
		 * 1: Building Blocks
		 * 2: Applications
		 * 3: Datasets
		*/
		
		//Imposto la lista delle macro categorie
		List<CategoryListItem> menuItems = new LinkedList<CategoryListItem>();
		
			//0: All
				PortletURL url = PortletURLFactoryUtil.create(request, themeDisplay.getPortletDisplay().getId(), themeDisplay.getPlid(), PortletRequest.ACTION_PHASE);
				url.setParameter("jspPage", "/html/marketplacestore/view.jsp"); 
				url.setParameter("javax.portlet.action", "homeController");
				url.setParameter("p_p_state", WindowState.NORMAL.toString());
				url.setParameter("categoryid", "0");
				
				CategoryListItem allItem = new CategoryListItem("all_categories", url.toString(), cat==0);
			menuItems.add(allItem);
			
			//1: BuildingBlocks
				PortletURL bburl = PortletURLFactoryUtil.create(request, themeDisplay.getPortletDisplay().getId(), themeDisplay.getPlid(), PortletRequest.ACTION_PHASE);
				bburl.setParameter("jspPage", "/html/marketplacestore/view.jsp"); 
				bburl.setParameter("javax.portlet.action", "homeController");
				url.setParameter("p_p_state", WindowState.NORMAL.toString());
				bburl.setParameter("categoryid", "1");
				
				CategoryListItem bbItem = new CategoryListItem("b-blocks", bburl.toString(), cat==1, "bbDefinition");
			menuItems.add(bbItem);
			
			//2: Applications
				PortletURL psaurl = PortletURLFactoryUtil.create(request, themeDisplay.getPortletDisplay().getId(), themeDisplay.getPlid(), PortletRequest.ACTION_PHASE);
				psaurl.setParameter("jspPage", "/html/marketplacestore/view.jsp"); 
				psaurl.setParameter("javax.portlet.action", "homeController");
				url.setParameter("p_p_state", WindowState.NORMAL.toString());
				psaurl.setParameter("categoryid", "1");
				
				CategoryListItem psaItem = new CategoryListItem("psa", psaurl.toString(), cat==2, "psaDefinition");
			menuItems.add(psaItem);
			
			//3: Datasets
				PortletURL dsurl = PortletURLFactoryUtil.create(request, themeDisplay.getPortletDisplay().getId(), themeDisplay.getPlid(), PortletRequest.ACTION_PHASE);
				dsurl.setParameter("jspPage", "/html/marketplacestore/view.jsp"); 
				dsurl.setParameter("javax.portlet.action", "homeController");
				url.setParameter("p_p_state", WindowState.NORMAL.toString());
				dsurl.setParameter("categoryid", "1");
				
				CategoryListItem dsItem = new CategoryListItem("datasets", dsurl.toString(), cat==3, "dsDefinition");
			menuItems.add(dsItem);
		
		request.setAttribute("catList", menuItems);
		
		String title = "last-arrived";
		if(cat != 0){
			if(cat == 1){ title = "Building Blocks"; }
			else if(cat == 2){ title = "Public Service Applications"; }
			else if(cat == 3){ title = "Datasets"; }
		}
		request.setAttribute("title", title);
		
		//Abilito o disabilito i suggerimenti
		boolean getRecommendations = user!=null && !user.isDefaultUser() && ConfUtil.getBoolean("de.enabled");
		request.setAttribute("recommenderEnabled",getRecommendations);
		
	}

}
