package it.eng.rspa.marketplace.utils;

import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;

import java.util.Comparator;

public class ArtefactComparatorByRating implements Comparator<Artefatto> {
		
	@Override
	public int compare(Artefatto art1, Artefatto art2) {
		
		double rating1 = MyMarketplaceUtility.getArtefactRatingAExtAligned(art1);
		double rating2 = MyMarketplaceUtility.getArtefactRatingAExtAligned(art2);
		
		if (rating2 > rating1)
			return 1;
		
		if (rating1 > rating2)
			return -1;
		
		return 0;
		
	}
}
