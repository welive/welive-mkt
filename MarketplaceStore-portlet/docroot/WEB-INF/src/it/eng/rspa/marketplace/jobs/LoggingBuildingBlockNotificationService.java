package it.eng.rspa.marketplace.jobs;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;

import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class LoggingBuildingBlockNotificationService {

	private static Log _log = LogFactoryUtil.getLog(LoggingBuildingBlockNotificationService.class);
	
	public static boolean sendNotification(String actionType, JSONObject message, long timestamp){
		
		long time = timestamp;
		if(timestamp==-1) time = GregorianCalendar.getInstance().getTimeInMillis();
		
		String url = ConfUtil.getString("lbb.url")+ConfUtil.getString("lbb.message.appid");
		
		JSONObject messageWrapper = JSONFactoryUtil.createJSONObject();
		messageWrapper.put("appid", ConfUtil.getString("lbb.message.appid"));
		messageWrapper.put("timestamp", time);
		messageWrapper.put("type", actionType);
		messageWrapper.put("msg", actionType);
		messageWrapper.put("custom_attr", message);
		try{
			Client client = Client.create();
			client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
			WebResource webResource = client.resource(url);
			
			_log.debug("Sending: "+messageWrapper.toString()+" to "+url);
			
			ClientResponse resp = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, messageWrapper.toString());
			String respString = resp.getEntity(String.class);
			
			if(resp.getStatus() != 200){
				_log.error("Service "+url+" responded with status "+resp.getStatus() +" and body " + respString);
				return false;
			}
			return true;
		}
		catch(Exception e){
			_log.error("Unable to send LoggingBB notification: "+e);
			return false;
		}
	}
	
}
