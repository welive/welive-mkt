/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException;
import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;
import it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfImpl;
import it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the marketplace conf service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see MarketplaceConfPersistence
 * @see MarketplaceConfUtil
 * @generated
 */
public class MarketplaceConfPersistenceImpl extends BasePersistenceImpl<MarketplaceConf>
	implements MarketplaceConfPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MarketplaceConfUtil} to access the marketplace conf persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MarketplaceConfImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfModelImpl.FINDER_CACHE_ENABLED,
			MarketplaceConfImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfModelImpl.FINDER_CACHE_ENABLED,
			MarketplaceConfImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_KEY = new FinderPath(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfModelImpl.FINDER_CACHE_ENABLED,
			MarketplaceConfImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByKey",
			new String[] { String.class.getName() },
			MarketplaceConfModelImpl.KEY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_KEY = new FinderPath(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByKey",
			new String[] { String.class.getName() });

	/**
	 * Returns the marketplace conf where key = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	 *
	 * @param key the key
	 * @return the matching marketplace conf
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a matching marketplace conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf findByKey(String key)
		throws NoSuchMarketplaceConfException, SystemException {
		MarketplaceConf marketplaceConf = fetchByKey(key);

		if (marketplaceConf == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("key=");
			msg.append(key);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchMarketplaceConfException(msg.toString());
		}

		return marketplaceConf;
	}

	/**
	 * Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param key the key
	 * @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf fetchByKey(String key) throws SystemException {
		return fetchByKey(key, true);
	}

	/**
	 * Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param key the key
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf fetchByKey(String key, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { key };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_KEY,
					finderArgs, this);
		}

		if (result instanceof MarketplaceConf) {
			MarketplaceConf marketplaceConf = (MarketplaceConf)result;

			if (!Validator.equals(key, marketplaceConf.getKey())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_MARKETPLACECONF_WHERE);

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_KEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_KEY_KEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKey) {
					qPos.add(key);
				}

				List<MarketplaceConf> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_KEY,
						finderArgs, list);
				}
				else {
					MarketplaceConf marketplaceConf = list.get(0);

					result = marketplaceConf;

					cacheResult(marketplaceConf);

					if ((marketplaceConf.getKey() == null) ||
							!marketplaceConf.getKey().equals(key)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_KEY,
							finderArgs, marketplaceConf);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_KEY,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (MarketplaceConf)result;
		}
	}

	/**
	 * Removes the marketplace conf where key = &#63; from the database.
	 *
	 * @param key the key
	 * @return the marketplace conf that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf removeByKey(String key)
		throws NoSuchMarketplaceConfException, SystemException {
		MarketplaceConf marketplaceConf = findByKey(key);

		return remove(marketplaceConf);
	}

	/**
	 * Returns the number of marketplace confs where key = &#63;.
	 *
	 * @param key the key
	 * @return the number of matching marketplace confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByKey(String key) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_KEY;

		Object[] finderArgs = new Object[] { key };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MARKETPLACECONF_WHERE);

			boolean bindKey = false;

			if (key == null) {
				query.append(_FINDER_COLUMN_KEY_KEY_1);
			}
			else if (key.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEY_KEY_3);
			}
			else {
				bindKey = true;

				query.append(_FINDER_COLUMN_KEY_KEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKey) {
					qPos.add(key);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_KEY_KEY_1 = "marketplaceConf.key IS NULL";
	private static final String _FINDER_COLUMN_KEY_KEY_2 = "marketplaceConf.key = ?";
	private static final String _FINDER_COLUMN_KEY_KEY_3 = "(marketplaceConf.key IS NULL OR marketplaceConf.key = '')";

	public MarketplaceConfPersistenceImpl() {
		setModelClass(MarketplaceConf.class);
	}

	/**
	 * Caches the marketplace conf in the entity cache if it is enabled.
	 *
	 * @param marketplaceConf the marketplace conf
	 */
	@Override
	public void cacheResult(MarketplaceConf marketplaceConf) {
		EntityCacheUtil.putResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfImpl.class, marketplaceConf.getPrimaryKey(),
			marketplaceConf);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_KEY,
			new Object[] { marketplaceConf.getKey() }, marketplaceConf);

		marketplaceConf.resetOriginalValues();
	}

	/**
	 * Caches the marketplace confs in the entity cache if it is enabled.
	 *
	 * @param marketplaceConfs the marketplace confs
	 */
	@Override
	public void cacheResult(List<MarketplaceConf> marketplaceConfs) {
		for (MarketplaceConf marketplaceConf : marketplaceConfs) {
			if (EntityCacheUtil.getResult(
						MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
						MarketplaceConfImpl.class,
						marketplaceConf.getPrimaryKey()) == null) {
				cacheResult(marketplaceConf);
			}
			else {
				marketplaceConf.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all marketplace confs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MarketplaceConfImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MarketplaceConfImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the marketplace conf.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MarketplaceConf marketplaceConf) {
		EntityCacheUtil.removeResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfImpl.class, marketplaceConf.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(marketplaceConf);
	}

	@Override
	public void clearCache(List<MarketplaceConf> marketplaceConfs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (MarketplaceConf marketplaceConf : marketplaceConfs) {
			EntityCacheUtil.removeResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
				MarketplaceConfImpl.class, marketplaceConf.getPrimaryKey());

			clearUniqueFindersCache(marketplaceConf);
		}
	}

	protected void cacheUniqueFindersCache(MarketplaceConf marketplaceConf) {
		if (marketplaceConf.isNew()) {
			Object[] args = new Object[] { marketplaceConf.getKey() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_KEY, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_KEY, args,
				marketplaceConf);
		}
		else {
			MarketplaceConfModelImpl marketplaceConfModelImpl = (MarketplaceConfModelImpl)marketplaceConf;

			if ((marketplaceConfModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_KEY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { marketplaceConf.getKey() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_KEY, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_KEY, args,
					marketplaceConf);
			}
		}
	}

	protected void clearUniqueFindersCache(MarketplaceConf marketplaceConf) {
		MarketplaceConfModelImpl marketplaceConfModelImpl = (MarketplaceConfModelImpl)marketplaceConf;

		Object[] args = new Object[] { marketplaceConf.getKey() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEY, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_KEY, args);

		if ((marketplaceConfModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_KEY.getColumnBitmask()) != 0) {
			args = new Object[] { marketplaceConfModelImpl.getOriginalKey() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEY, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_KEY, args);
		}
	}

	/**
	 * Creates a new marketplace conf with the primary key. Does not add the marketplace conf to the database.
	 *
	 * @param confId the primary key for the new marketplace conf
	 * @return the new marketplace conf
	 */
	@Override
	public MarketplaceConf create(long confId) {
		MarketplaceConf marketplaceConf = new MarketplaceConfImpl();

		marketplaceConf.setNew(true);
		marketplaceConf.setPrimaryKey(confId);

		return marketplaceConf;
	}

	/**
	 * Removes the marketplace conf with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param confId the primary key of the marketplace conf
	 * @return the marketplace conf that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf remove(long confId)
		throws NoSuchMarketplaceConfException, SystemException {
		return remove((Serializable)confId);
	}

	/**
	 * Removes the marketplace conf with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the marketplace conf
	 * @return the marketplace conf that was removed
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf remove(Serializable primaryKey)
		throws NoSuchMarketplaceConfException, SystemException {
		Session session = null;

		try {
			session = openSession();

			MarketplaceConf marketplaceConf = (MarketplaceConf)session.get(MarketplaceConfImpl.class,
					primaryKey);

			if (marketplaceConf == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMarketplaceConfException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(marketplaceConf);
		}
		catch (NoSuchMarketplaceConfException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MarketplaceConf removeImpl(MarketplaceConf marketplaceConf)
		throws SystemException {
		marketplaceConf = toUnwrappedModel(marketplaceConf);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(marketplaceConf)) {
				marketplaceConf = (MarketplaceConf)session.get(MarketplaceConfImpl.class,
						marketplaceConf.getPrimaryKeyObj());
			}

			if (marketplaceConf != null) {
				session.delete(marketplaceConf);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (marketplaceConf != null) {
			clearCache(marketplaceConf);
		}

		return marketplaceConf;
	}

	@Override
	public MarketplaceConf updateImpl(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf)
		throws SystemException {
		marketplaceConf = toUnwrappedModel(marketplaceConf);

		boolean isNew = marketplaceConf.isNew();

		Session session = null;

		try {
			session = openSession();

			if (marketplaceConf.isNew()) {
				session.save(marketplaceConf);

				marketplaceConf.setNew(false);
			}
			else {
				session.merge(marketplaceConf);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MarketplaceConfModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
			MarketplaceConfImpl.class, marketplaceConf.getPrimaryKey(),
			marketplaceConf);

		clearUniqueFindersCache(marketplaceConf);
		cacheUniqueFindersCache(marketplaceConf);

		return marketplaceConf;
	}

	protected MarketplaceConf toUnwrappedModel(MarketplaceConf marketplaceConf) {
		if (marketplaceConf instanceof MarketplaceConfImpl) {
			return marketplaceConf;
		}

		MarketplaceConfImpl marketplaceConfImpl = new MarketplaceConfImpl();

		marketplaceConfImpl.setNew(marketplaceConf.isNew());
		marketplaceConfImpl.setPrimaryKey(marketplaceConf.getPrimaryKey());

		marketplaceConfImpl.setConfId(marketplaceConf.getConfId());
		marketplaceConfImpl.setKey(marketplaceConf.getKey());
		marketplaceConfImpl.setValue(marketplaceConf.getValue());
		marketplaceConfImpl.setType(marketplaceConf.getType());
		marketplaceConfImpl.setOptions(marketplaceConf.getOptions());

		return marketplaceConfImpl;
	}

	/**
	 * Returns the marketplace conf with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the marketplace conf
	 * @return the marketplace conf
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMarketplaceConfException, SystemException {
		MarketplaceConf marketplaceConf = fetchByPrimaryKey(primaryKey);

		if (marketplaceConf == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMarketplaceConfException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return marketplaceConf;
	}

	/**
	 * Returns the marketplace conf with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	 *
	 * @param confId the primary key of the marketplace conf
	 * @return the marketplace conf
	 * @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf findByPrimaryKey(long confId)
		throws NoSuchMarketplaceConfException, SystemException {
		return findByPrimaryKey((Serializable)confId);
	}

	/**
	 * Returns the marketplace conf with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the marketplace conf
	 * @return the marketplace conf, or <code>null</code> if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		MarketplaceConf marketplaceConf = (MarketplaceConf)EntityCacheUtil.getResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
				MarketplaceConfImpl.class, primaryKey);

		if (marketplaceConf == _nullMarketplaceConf) {
			return null;
		}

		if (marketplaceConf == null) {
			Session session = null;

			try {
				session = openSession();

				marketplaceConf = (MarketplaceConf)session.get(MarketplaceConfImpl.class,
						primaryKey);

				if (marketplaceConf != null) {
					cacheResult(marketplaceConf);
				}
				else {
					EntityCacheUtil.putResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
						MarketplaceConfImpl.class, primaryKey,
						_nullMarketplaceConf);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MarketplaceConfModelImpl.ENTITY_CACHE_ENABLED,
					MarketplaceConfImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return marketplaceConf;
	}

	/**
	 * Returns the marketplace conf with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param confId the primary key of the marketplace conf
	 * @return the marketplace conf, or <code>null</code> if a marketplace conf with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MarketplaceConf fetchByPrimaryKey(long confId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)confId);
	}

	/**
	 * Returns all the marketplace confs.
	 *
	 * @return the marketplace confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MarketplaceConf> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the marketplace confs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of marketplace confs
	 * @param end the upper bound of the range of marketplace confs (not inclusive)
	 * @return the range of marketplace confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MarketplaceConf> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the marketplace confs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of marketplace confs
	 * @param end the upper bound of the range of marketplace confs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of marketplace confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MarketplaceConf> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<MarketplaceConf> list = (List<MarketplaceConf>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MARKETPLACECONF);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MARKETPLACECONF;

				if (pagination) {
					sql = sql.concat(MarketplaceConfModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<MarketplaceConf>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MarketplaceConf>(list);
				}
				else {
					list = (List<MarketplaceConf>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the marketplace confs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (MarketplaceConf marketplaceConf : findAll()) {
			remove(marketplaceConf);
		}
	}

	/**
	 * Returns the number of marketplace confs.
	 *
	 * @return the number of marketplace confs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MARKETPLACECONF);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the marketplace conf persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.marketplace.artefatto.model.MarketplaceConf")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<MarketplaceConf>> listenersList = new ArrayList<ModelListener<MarketplaceConf>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<MarketplaceConf>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(MarketplaceConfImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_MARKETPLACECONF = "SELECT marketplaceConf FROM MarketplaceConf marketplaceConf";
	private static final String _SQL_SELECT_MARKETPLACECONF_WHERE = "SELECT marketplaceConf FROM MarketplaceConf marketplaceConf WHERE ";
	private static final String _SQL_COUNT_MARKETPLACECONF = "SELECT COUNT(marketplaceConf) FROM MarketplaceConf marketplaceConf";
	private static final String _SQL_COUNT_MARKETPLACECONF_WHERE = "SELECT COUNT(marketplaceConf) FROM MarketplaceConf marketplaceConf WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "marketplaceConf.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MarketplaceConf exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No MarketplaceConf exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(MarketplaceConfPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"key", "type"
			});
	private static MarketplaceConf _nullMarketplaceConf = new MarketplaceConfImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<MarketplaceConf> toCacheModel() {
				return _nullMarketplaceConfCacheModel;
			}
		};

	private static CacheModel<MarketplaceConf> _nullMarketplaceConfCacheModel = new CacheModel<MarketplaceConf>() {
			@Override
			public MarketplaceConf toEntityModel() {
				return _nullMarketplaceConf;
			}
		};
}