package it.eng.rspa;

public class RDFException extends Exception {

	private static final long serialVersionUID = -79403147754319349L;

	public RDFException(String message){
		super(message);
	}
	
}
