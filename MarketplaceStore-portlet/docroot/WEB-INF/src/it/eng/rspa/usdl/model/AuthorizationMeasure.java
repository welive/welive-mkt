package it.eng.rspa.usdl.model;

public class AuthorizationMeasure extends SecurityMeasure {

	public AuthorizationMeasure(String title, String description) {
		super(title, description);
	}

}
