package it.eng.rspa.usdl.model;

public class Namespace {
	public final static String RDFS_TYPE ="http://www.w3.org/2001/XMLSchema#";	
	public final static String RDF_DATATYPE ="http://www.w3.org/2001/XMLSchema#";	
	public final static String BLUEPRINT = "http://bizweb.sap.com/TR/blueprint#"; 	
	public final static String USDL = "http://www.linked-usdl.org/ns/usdl-core#";
	public final static String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String OWL = "http://www.w3.org/2002/07/owl#";
	public final static String DCTERMS = "http://purl.org/dc/terms/";
	public final static String SCHEMA = "http://schema.org/";
	public final static String XSD = "http://www.w3.org/2001/XMLSchema#";
	public final static String VANN = "http://purl.org/vocab/vann/";
	public final static String FOAF = "http://xmlns.com/foaf/0.1/";
	public final static String USDK = "http://www.linked-usdl.org/ns/usdl#";
	public final static String TEST = "http://www.w3.org/2006/03/test-description#";
	public final static String SWRL = "http://www.w3.org/2003/11/swrl#";
	public final static String USDL_SLA = "http://www.linked-usdl.org/ns/usdl-sla#";
	public final static String USDL_SECURITY = "http://www.linked-usdl.org/ns/usdl-security#";
	public final static String WELIVE_SECURITY = "http://welive.eu/ns/security#";
	public final static String USDL_LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String USDL_PRICE = "http://www.linked-usdl.org/ns/usdl-price#";	
	public final static String RDFS = "http://www.w3.org/2000/01/rdf-schema#";
	public final static String GR = "http://purl.org/goodrelations/v1#";
	public final static String SKOS = "http://www.w3.org/2004/02/skos/core#";
	public final static String ORG = "http://www.w3.org/ns/org#";
	public final static String PRICE = "http://www.linked-usdl.org/ns/usdl-price#";
	public final static String LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String DEI = "http://dei.uc.pt/rdf/dei#";	
	public final static String RDF_Syntax ="http://www.w3.org/1999/02/22-rdf-syntax-ns#";		
	public final static String USDL_Core_Schema_File = "./ttl/usdl-core.ttl";
	public final static String USDL_Price_Schema_File = "./ttl/usdl-price.ttl";
	public final static String USDL_Instance_File = "./ttl/Service_01.ttl";
	public final static String USDL_Instance_FileRDF = "./ttl/ServiceRDF_01.rdf";
//	public final static String WELIVE = "http://welive.eu/ns#";
	public final static String WELIVE_CORE = "http://www.welive.eu/ns/welive-core#";
	public final static String TAGS = "http://www.holygoat.co.uk/owl/redwood/0.1/tags#";
}
