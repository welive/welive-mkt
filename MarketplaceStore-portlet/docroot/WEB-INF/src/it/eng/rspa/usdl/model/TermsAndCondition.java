package it.eng.rspa.usdl.model;

public abstract class TermsAndCondition extends LUSDLResource{

	public Field<String> title;
	public Field<String> description;
	
	public TermsAndCondition(String title, String description) {
		this.setTitle(title);
		this.setDescription(description);
	}
	
	public Field<String> getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}
	
	public Field<String> getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = new Field<String>(Namespace.DCTERMS, "description", description);
	}
	
}
