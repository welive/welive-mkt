package it.eng.rspa.usdl.model;

public class Entity extends LUSDLResource{

	public Field<String> title;
	public Field<String> page;
	public Field<String> mbox;
	
	public Field<BusinessRole> businessRole;
	
	public Entity(){
		this.title = null;
		this.page = null;
		this.mbox = null;
		this.businessRole = null;
	}

	public Field<String> getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}

	public Field<String> getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = new Field<String>(Namespace.FOAF, "page", page);
	}

	public Field<String> getMbox() {
		return mbox;
	}

	public void setMbox(String mbox) {
		this.mbox = new Field<String>(Namespace.FOAF, "mbox", mbox);
	}

	public Field<BusinessRole> getBusinessRole() {
		return businessRole;
	}

	public void setBusinessRole(BusinessRole businessRole) {
		this.businessRole = new Field<BusinessRole>(Namespace.FOAF, "businessRole", businessRole);
	}
	
}
