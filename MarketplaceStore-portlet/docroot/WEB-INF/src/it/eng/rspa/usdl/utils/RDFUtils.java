package it.eng.rspa.usdl.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import it.eng.metamodel.definitions.License;
import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.usdl.model.AccessType;
import it.eng.rspa.usdl.model.Artefact;
import it.eng.rspa.usdl.model.AuthenticationMeasure;
import it.eng.rspa.usdl.model.BuildingBlock;
import it.eng.rspa.usdl.model.BusinessRole;
import it.eng.rspa.usdl.model.CommunicationMeasure;
import it.eng.rspa.usdl.model.CustomLicense;
import it.eng.rspa.usdl.model.Entity;
import it.eng.rspa.usdl.model.IdentityProvider;
import it.eng.rspa.usdl.model.InteractionPoint;
import it.eng.rspa.usdl.model.Namespace;
import it.eng.rspa.usdl.model.OriginConstraint;
import it.eng.rspa.usdl.model.Permission;
import it.eng.rspa.usdl.model.ProtocolConstraint;
import it.eng.rspa.usdl.model.ProtocolType;
import it.eng.rspa.usdl.model.PublicServiceApplication;
import it.eng.rspa.usdl.model.SecurityMeasure;
import it.eng.rspa.usdl.model.ServiceLevelProfile;
import it.eng.rspa.usdl.model.ServiceOffering;
import it.eng.rspa.usdl.model.StandardLicense;
import it.eng.rspa.usdl.model.TermsAndCondition;
import it.eng.sesame.InteractionPointBean;
import it.eng.sesame.ReaderRDF;

public abstract class RDFUtils {

	public static Entity getAuthor(ReaderRDF reader){
		
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Author");
		
		if(it.hasNext()){
			Resource author = it.next().getResource();
			
			Property propertyName = reader.getModel().createProperty(Namespace.DCTERMS + "title");
			Property propertyEmail = reader.getModel().createProperty(Namespace.FOAF + "mbox");
			
			String name = reader.getPropertyValue(author, propertyName);
			String email = reader.getPropertyValue(author, propertyEmail);
			
			Entity autore = new Entity();
			autore.setBusinessRole(BusinessRole.AUTHOR);
			autore.setMbox(email);
			autore.setTitle(name);
			
			return autore; 
			
		}
		else {
			ReaderRDF._log.warn("Author not found");
			return null;
		}
	}

	public static Entity getProvider(ReaderRDF reader){
		
		try{
			StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Provider");
			
			if(it.hasNext()){
				
				Resource serviceProvider = it.next().getResource();
				
				Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
				Property propertyPage = reader.getModel().createProperty(Namespace.FOAF + "page");

				String name = reader.getPropertyValue(serviceProvider, propertyTitle);
				String page = reader.getPropertyValue(serviceProvider, propertyPage);
				
				Entity provider = new Entity();
				provider.setBusinessRole(BusinessRole.PROVIDER);
				provider.setTitle(name);
				provider.setPage(page);
				
				return provider;
			}
			else{
				ReaderRDF._log.warn("Provider not found");
				return null;
			}
			
		}
		catch(Exception e){
			ReaderRDF._log.error("Provider not found: "+e.getMessage());
			return null;
		}
	}
	
	public static Entity getOwner(ReaderRDF reader){
		
		try{
			
			StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Owner");
			
			if(it.hasNext()){
				
				Resource owner = it.next().getResource();
				
				Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
				Property propertyPage = reader.getModel().createProperty(Namespace.FOAF + "page");

				String name = reader.getPropertyValue(owner, propertyTitle);
				String page = reader.getPropertyValue(owner, propertyPage);
				
				Entity jOwner = new Entity();
				jOwner.setBusinessRole(BusinessRole.OWNER);
				jOwner.setTitle(name);
				jOwner.setPage(page);
				
				return jOwner;
			}
			else{
				ReaderRDF._log.warn("Owner not found");
				return null;
			}
			
		}
		catch(Exception e){
			ReaderRDF._log.warn("Owner not found: "+e.getMessage());
			return null;
		}
		
	}
	
	public static List<it.eng.metamodel.definitions.License> getLicenses_v3(ReaderRDF reader){
		
		List<it.eng.metamodel.definitions.License> out = new ArrayList<it.eng.metamodel.definitions.License>();
		
		try{
			StmtIterator it = reader.getResourcesOfType(Namespace.USDL + "LegalCondition");
			
			if(!it.hasNext()){
				ReaderRDF._log.warn("Offering not found!");
				return out;
			}
			
			//licenses
			Property propertyTitleLicense = reader.getModel().createProperty(Namespace.DCTERMS + "title");
			Property propertyDescriptionLicense = reader.getModel().createProperty(Namespace.DCTERMS + "description");
			Property propertyUrlLicense = reader.getModel().createProperty(Namespace.SCHEMA + "description");
			
			while(it.hasNext()){
				Statement stmt = it.next();
				License jLicense;
				try{
					
					Resource term = reader.getResourceOfName(stmt.getSubject().toString());
					
					if(term!=null){
						jLicense = new License(reader.getPropertyValue(term, propertyTitleLicense),
											   reader.getPropertyValue(term, propertyDescriptionLicense),
											   reader.getPropertyValue(term, propertyUrlLicense));
						
						out.add(jLicense);
					}
					else{
						ReaderRDF._log.warn("License not found!");
						jLicense = null;
					}
					
				}
				catch(Exception e){ ReaderRDF._log.warn("Error while getting Licenses "+stmt.getSubject().toString()); }
			}
		}
		catch(Exception e){
			ReaderRDF._log.warn("Error while getting Licenses "+e.getClass().getSimpleName() + ": "+e.getMessage());
		}
		
		return out;
		
	}
	
	public static Set<ServiceOffering> getServiceOfferings(ReaderRDF reader){
		
		Set<ServiceOffering> out = new HashSet<ServiceOffering>();
		
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Offering");
		
		if(!it.hasNext()){
			ReaderRDF._log.warn("Offering not found!");
			return out;
		}
		
		//offering
		Property propertyTitleOffering = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyDescriptionOffering = reader.getModel().createProperty(Namespace.DCTERMS + "description");
		
		Property propertySLPtitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertySLPdescription = reader.getModel().createProperty(Namespace.DCTERMS + "description");
		Property propertySLPformat = reader.getModel().createProperty(Namespace.DCTERMS + "format");
		
		while(it.hasNext()){
			Statement stmt = it.next();
			ServiceOffering jSo;
			try{
				
				Resource serviceOffering = reader.getResourceOfName(stmt.getSubject().toString());
				
				if(serviceOffering!=null){
					String id = serviceOffering.toString().split("#Offering")[1];
					Resource rSlp = reader.getResourceOfName(Namespace.WELIVE_CORE + "ServiceLevelProfile"+id);
					
					ServiceLevelProfile jSlp = new ServiceLevelProfile(reader.getPropertyValue(rSlp, propertySLPtitle), 
																	   reader.getPropertyValue(rSlp, propertySLPdescription), 
																	   reader.getPropertyValue(rSlp, propertySLPformat));
					
					jSo = new ServiceOffering(reader.getPropertyValue(serviceOffering, propertyTitleOffering),
															  reader.getPropertyValue(serviceOffering, propertyDescriptionOffering),
															  jSlp);
				}
				else{
					ReaderRDF._log.warn("ServiceOffering not found!");
					jSo = null;
				}
				
				out.add(jSo);
				
			}
			catch(Exception e){ ReaderRDF._log.warn("Error while getting ServiceOffering "+stmt.getSubject().toString()); }
		}
		
		return out;
	}

	public static Set<Artefact> getDependencies(ReaderRDF reader){
		
		Set<Artefact> out = new HashSet<Artefact>();
		
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Dependency");
		
		if(!it.hasNext()){
			ReaderRDF._log.warn("Artifact not found");
			return out;
		}
		
		Resource dependency;
		
		Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyDescription = reader.getModel().createProperty(Namespace.DCTERMS + "abstract");
		Property propertyLocation = reader.getModel().createProperty(Namespace.FOAF + "page");
		
		while(it.hasNext()){
			dependency = it.next().getSubject();
			String title = reader.getPropertyValue(dependency, propertyTitle);
			String description = reader.getPropertyValue(dependency, propertyDescription);
			String location = reader.getPropertyValue(dependency, propertyLocation);
			Artefact a = new PublicServiceApplication(title, description, description, "", location, new String[]{}, "", "");
			
			out.add(a);	
		}
		
		return out;
	}
	
	public static Set<TermsAndCondition> getTerms(ReaderRDF reader){
		
		Set<TermsAndCondition> out = new HashSet<TermsAndCondition>();
		
		if(reader==null)
			return out;
		
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "LegalCondition");
		
		if(!it.hasNext()){
			ReaderRDF._log.warn("Terms not found!");
			return out;
		}
		
		Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyDescription = reader.getModel().createProperty(Namespace.DCTERMS + "description");
		Property propertyStandardURL = reader.getModel().createProperty(Namespace.WELIVE_CORE + "url");
		Property propertyVersion = reader.getModel().createProperty(Namespace.DCTERMS + "hasVersion");
		
		while(it.hasNext()){
			
			try{
				Resource terms = it.next().getSubject();

				String title = reader.getPropertyValue(terms, propertyTitle);
				String description = reader.getPropertyValue(terms, propertyDescription);
				String url = reader.getPropertyValue(terms,propertyStandardURL);
				String version = reader.getPropertyValue(terms,propertyVersion);
				
				TermsAndCondition term;
				if(version != null && !version.equals("")) term = new StandardLicense(title, description, url, version);
				else term = new CustomLicense(title, description, url);
				
				out.add(term);
			}
			catch(Exception e){
				ReaderRDF._log.warn("Error while getting term: "+e.getMessage());
			}
		}
		
		return out;
	}
	
	public static List<QuerySolution> runQuery(String query, String formatType) {

		Query queryS = QueryFactory.create(query);
		QueryExecution exec = QueryExecutionFactory.createServiceRequest(ConfUtil.getString("Crud.SesamServerURI")+ConfUtil.getString("Crud.SesameRepositoryName"), queryS);
		ResultSet resultSet = exec.execSelect();
		List<QuerySolution> resultsAsString = new ArrayList<QuerySolution>();
		if(formatType.equalsIgnoreCase("XML") || formatType.equalsIgnoreCase("RDF")){
			resultsAsString = ResultSetFormatter.toList(resultSet);
		}

		return resultsAsString;
	}  
	
	public static Set<SecurityMeasure> getSecurities(ReaderRDF reader) {
		
		Set<SecurityMeasure> out = new HashSet<SecurityMeasure>();
		
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Security");
		
		if(!it.hasNext()){
			ReaderRDF._log.warn("Security not found!");
			return out;
		}
		
		Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyDescription = reader.getModel().createProperty(Namespace.DCTERMS + "description");
		Property propertyProtocolType = reader.getModel().createProperty(Namespace.WELIVE_CORE, "protocolType");
		Property propertyAccessType = reader.getModel().createProperty(Namespace.WELIVE_CORE, "accessType");
		Property propertyIdentityProvider = reader.getModel().createProperty(Namespace.WELIVE_CORE, "withIdentityProvider");
		Property propertyRequires = reader.getModel().createProperty(Namespace.WELIVE_SECURITY, "requires");
		Property propertyWithOrigin = reader.getModel().createProperty(Namespace.WELIVE_SECURITY, "withOrigin");
		Property propertyWithProtocol = reader.getModel().createProperty(Namespace.WELIVE_SECURITY, "withProtocol");
		
		while(it.hasNext()){
			
			try{
				Resource rSec = it.next().getSubject();
				SecurityMeasure jSec = null;
				
				String title = reader.getPropertyValue(rSec, propertyTitle);
				String description = reader.getPropertyValue(rSec, propertyDescription);
				String protocolType = reader.getPropertyValue(rSec, propertyProtocolType);
				StmtIterator requiresIt = rSec.listProperties(propertyRequires);
				StmtIterator originsIt = rSec.listProperties(propertyWithOrigin);
				StmtIterator protosIt = rSec.listProperties(propertyWithProtocol);
				
				if(protocolType!=null && !protocolType.equals("") && !protocolType.equals("null")){
					String accessType = reader.getPropertyValue(rSec, propertyAccessType);
					jSec = new AuthenticationMeasure(title, description, ProtocolType.valueOf(protocolType.toUpperCase()), AccessType.valueOf(accessType.toUpperCase()));
					StmtIterator idProviderIt = rSec.listProperties(propertyIdentityProvider);
					while (idProviderIt.hasNext()){
						RDFNode obj = idProviderIt.next().getObject();
						Property identifier = reader.getModel().createProperty(Namespace.DCTERMS, "identifier");
						String id = reader.getPropertyValue(obj.asResource(), identifier);
						((AuthenticationMeasure)jSec).addIdentityProvider(new IdentityProvider(id));
					}
					while(requiresIt.hasNext()){
						RDFNode obj = requiresIt.next().getObject();
						Property identifier = reader.getModel().createProperty(Namespace.DCTERMS, "identifier");
						String id = reader.getPropertyValue(obj.asResource(), identifier);
						String permissionTitle = reader.getPropertyValue(obj.asResource(), propertyTitle);
						String permissionDescription = reader.getPropertyValue(obj.asResource(), propertyDescription);
						((AuthenticationMeasure)jSec).addRequires(new Permission(id, permissionTitle, permissionDescription));
					}
					
				}
//				else if(requiresIt.hasNext()){
//					jSec = new PermissionAuthorization(title, description);
//					while(requiresIt.hasNext()){
//						RDFNode obj = requiresIt.next().getObject();
//						Property identifier = reader.getModel().createProperty(Namespace.DCTERMS, "identifier");
//						String id = reader.getPropertyValue(obj.asResource(), identifier);
//						String permissionTitle = reader.getPropertyValue(obj.asResource(), propertyTitle);
//						String permissionDescription = reader.getPropertyValue(obj.asResource(), propertyDescription);
//						((PermissionAuthorization)jSec).addRequires(new Permission(id, permissionTitle, permissionDescription));
//					}
//				}
				else if(originsIt.hasNext() || protosIt.hasNext()){
					jSec = new CommunicationMeasure(title, description);
					//Aggiungo le origins
					while(originsIt.hasNext()){
						RDFNode obj = originsIt.next().getObject();
						Property origin = reader.getModel().createProperty(Namespace.WELIVE_CORE, "origin");
						String originName = reader.getPropertyValue(obj.asResource(), origin);
						((CommunicationMeasure)jSec).addOriginConstraint(new OriginConstraint(originName));
					}
					
					//Aggiungo i protocolli
					while(protosIt.hasNext()){
						RDFNode obj = protosIt.next().getObject();
						Property proto = reader.getModel().createProperty(Namespace.WELIVE_CORE, "protocol");
						String protoName = reader.getPropertyValue(obj.asResource(), proto);
						((CommunicationMeasure)jSec).addProtocolConstraint(new ProtocolConstraint(protoName));
					}
				}
				
				out.add(jSec);
			}
			catch(Exception e){
				ReaderRDF._log.warn("Error while getting security: "+e.getMessage());
			}
		}
		
		return out;
	}
	
	public static Set<InteractionPoint> getInteractions(ReaderRDF reader){
		Set<InteractionPoint> out = new HashSet<InteractionPoint>();
		
		Set<InteractionPointBean> ipbs = reader.getInteractions();
		for(InteractionPointBean ipb : ipbs){
			InteractionPoint point = new InteractionPoint(ipb.getTitle(), ipb.getWadl());
			out.add(point);
		}
		
		return out;
	}
	
	public static List<String> getTags(ReaderRDF reader){
		
		List<String> out = new ArrayList<String>();
		try{
			Resource service = reader.getServiceResource();
			
			if(service!=null){
				Property propertyTag = reader.getModel().createProperty(Namespace.TAGS + "tag");
				List<String> tags = reader.getMultiplePropertyValuesList(service, propertyTag);
				out.addAll(tags);
			}
		}
		catch(Exception e){
			ReaderRDF._log.error(e.getClass().getSimpleName()+": "+e.getMessage());
		}
		
		return out;
		
		
	}
	
	public static Artefact getArtefact(ReaderRDF reader){
		
		Resource service = reader.getServiceResource();
			
		if(service!=null){
		
			Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
			Property propertyAbstract = reader.getModel().createProperty(Namespace.DCTERMS + "abstract");
			Property propertyDescription = reader.getModel().createProperty(Namespace.DCTERMS + "description"); 
			Property propertyCreated = reader.getModel().createProperty(Namespace.DCTERMS + "created");
			Property propertyWebpage = reader.getModel().createProperty(Namespace.FOAF + "page");
			Property propertyTag = reader.getModel().createProperty(Namespace.TAGS + "tag");
			Property propertyUrl = reader.getModel().createProperty(Namespace.SCHEMA + "url");
			Property propertyType = reader.getModel().createProperty(Namespace.DCTERMS + "type");
			
			String title = reader.getPropertyValue(service, propertyTitle);
			String created = reader.getPropertyValue(service, propertyCreated);
			String abs = reader.getPropertyValue(service, propertyAbstract);
			String description = reader.getPropertyValue(service, propertyDescription);
			String webpage = reader.getPropertyValue(service, propertyWebpage);
			HashSet<String> tags = reader.getMultiplePropertyValues(service, propertyTag);
			String[] tag;
			if(tags!=null){
				tag = tags.toArray(new String[tags.size()]); 
			}
			else{ tag = new String[]{}; }
			String type = reader.getPropertyValue(service, propertyType);
			
			Set<InteractionPoint> intpoints = getInteractions(reader);
			Artefact art = null;
			if(intpoints==null || intpoints.isEmpty()){
				String url = reader.getPropertyValue(service, propertyUrl);
				art = new PublicServiceApplication(title, description, abs, created, webpage, tag, type, url);
			}
			else{
				art = new BuildingBlock(title, description, abs, created, webpage, tag, type);
				for(InteractionPoint i : intpoints){
					((BuildingBlock)art).addInteractionPoint(i);
				}
			}
			
			Entity e = getAuthor(reader);
			if(e!=null)
				art.addBusinessRole(e);
			
			e = getProvider(reader); 
			if(e!=null)
				art.addBusinessRole(e);
			
			e = getOwner(reader);
			if(e!=null)
				art.addBusinessRole(e);
			
			
			Set<Artefact> dependencies = getDependencies(reader);
			if(dependencies!=null && !dependencies.isEmpty()){
				for(Artefact a: dependencies){
					art.addUses(a);
				}
			}
			
			Set<SecurityMeasure> securities = getSecurities(reader);
			for(SecurityMeasure measure: securities){
				art.addSecurityMeasure(measure);
			}
			
			Set<TermsAndCondition> terms = getTerms(reader);
			for(TermsAndCondition t : terms)
				art.addLegalCondition(t);
			
			Set<ServiceOffering> serviceOfferings = getServiceOfferings(reader);
			for(ServiceOffering so : serviceOfferings)
				art.addServiceOffering(so);
			
			return art;
		}
		else{
			ReaderRDF._log.error("Artefact not found");
			return null;
		}
	}
}
