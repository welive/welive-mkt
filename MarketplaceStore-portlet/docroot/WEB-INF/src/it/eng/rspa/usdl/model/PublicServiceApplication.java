package it.eng.rspa.usdl.model;

public class PublicServiceApplication extends Artefact {

	public Field<String> url;
	
	public PublicServiceApplication(String title, 
									String description, 
									String abstractDescription, 
									String created, 
									String page, 
									String[] tag,
									String type,
									String url) {
		
		super(title, description, abstractDescription, created, page, tag, type);
		this.setUrl(url);
	}
	
	public Field<String> getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = new Field<String>(Namespace.SCHEMA, "url", url);
	}
	
}
