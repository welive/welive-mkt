package it.eng.rspa.usdl.model;

public class Permission extends LUSDLResource{

	public Field<String> identifier;
	public Field<String> title;
	public Field<String> description;
	
	public Permission(String identifier, String title, String description){
		this.setIdentifier(identifier);
		this.setTitle(title);
		this.setDescription(description);
	}
	
	public Field<String> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = new Field<String>(Namespace.DCTERMS, "identifier", identifier);
	}
	
	public Field<String> getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}

	public Field<String> getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = new Field<String>(Namespace.DCTERMS, "description", description);
	}
	
}
