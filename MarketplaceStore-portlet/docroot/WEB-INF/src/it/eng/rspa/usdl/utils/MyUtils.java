package it.eng.rspa.usdl.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

public class MyUtils {
	
	/**
	 * @return
	 */
	public static String getRootUrl() {
		
		String rootUrl = ""; 

		//Ottengo utente default Liferay
		 String webId = new String("liferay.com");
		 Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			 rootUrl = PortalUtil.getPortalURL(company.getVirtualHostname(), PortalUtil.getPortalPort(), false);
			
		} catch (PortalException | SystemException e) {
            e.printStackTrace();
			 return rootUrl;
		}
		
		
		return rootUrl;
	}
	
	
	/**
	 * @param themeDisplay
	 * @param eid
	 * @return
	 */
	public static String getODSUrl(ThemeDisplay themeDisplay, String eid){
	
		//https://test.welive.eu/ods/en/dataset/avvisi-del-comune-di-trento
		
		String url =getRootUrl();
		
		
		String lang = themeDisplay.getLanguageId();
		
		if (lang.equals("it_IT"))
			url += "/ods/it/";
		else if (lang.equalsIgnoreCase("fi_FI")) 
			url += "/ods/fi/";
		else if (lang.equalsIgnoreCase("sr_RS") || lang.equalsIgnoreCase("sr_RS_latin")) 
			url += "/ods/sr_Latn/";
		else if (lang.equalsIgnoreCase("es_ES")) 
			url += "/ods/es/";
		else
			url += "/ods/en/";
		
		url +="dataset/"+eid;
		
		
		return url;
		

		}
	

}
