package it.eng.rspa.usdl.model;

public abstract class LegalCondition extends TermsAndCondition {

	public LegalCondition(String title, String description) {
		super(title, description);
	}
	
}
