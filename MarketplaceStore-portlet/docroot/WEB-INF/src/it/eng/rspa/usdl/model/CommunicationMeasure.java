package it.eng.rspa.usdl.model;

import java.util.HashSet;
import java.util.Set;

public class CommunicationMeasure extends SecurityMeasure {

	Field<Set<OriginConstraint>> withOrigin = new Field<Set<OriginConstraint>>(Namespace.WELIVE_SECURITY, "withOrigin", new HashSet<OriginConstraint>());
	Field<Set<ProtocolConstraint>> withProtocol = new Field<Set<ProtocolConstraint>>(Namespace.WELIVE_SECURITY, "withProtocol", new HashSet<ProtocolConstraint>());
	
	public CommunicationMeasure(String title, String description) {
		super(title, description);
		
	}

	public Field<Set<OriginConstraint>> getOriginConstraint() {
		return withOrigin;
	}

	public void addOriginConstraint(OriginConstraint origin) {
		this.withOrigin.getValue().add(origin);
	}
	
	public Field<Set<ProtocolConstraint>> getProtocolConstraint() {
		return withProtocol;
	}

	public void addProtocolConstraint(ProtocolConstraint protocol) {
		this.withProtocol.getValue().add(protocol);
	}

}
