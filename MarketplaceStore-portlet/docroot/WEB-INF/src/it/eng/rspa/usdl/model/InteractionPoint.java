package it.eng.rspa.usdl.model;

public class InteractionPoint extends LUSDLResource{

	public Field<String> title;
	public Field<String> url;
	
	public InteractionPoint(String title, String url) {
		this.setTitle(title);
		this.setUrl(url);
	}
	
	public Field<String> getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}

	public Field<String> getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = new Field<String>(Namespace.SCHEMA, "url", url);
	}
		
}
