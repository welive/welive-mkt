package it.eng.rspa.usdl.model;

public class CustomLicense extends License {

	public CustomLicense(String title, String description, String url) {
		super(title, description, url);
	}

}
