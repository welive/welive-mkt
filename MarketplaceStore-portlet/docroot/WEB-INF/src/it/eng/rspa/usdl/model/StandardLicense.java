package it.eng.rspa.usdl.model;

public class StandardLicense extends License {
	
	public Field<String> version;

	public StandardLicense(String title, String description, String url, String version) {
		super(title, description, url);
		this.setVersion(version);
	}

	public Field<String> getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = new Field<String>(Namespace.DCTERMS, "hasVersion", version);
	}
	
}
