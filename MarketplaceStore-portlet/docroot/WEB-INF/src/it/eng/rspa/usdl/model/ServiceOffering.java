package it.eng.rspa.usdl.model;

public class ServiceOffering extends LUSDLResource{
	
	public Field<String> title;
	public Field<String> description;
	
	public Field<ServiceLevelProfile> hasServiceLevelProfile;
	
	public ServiceOffering(String title, String description, ServiceLevelProfile sla) {
		this.setTitle(title);
		this.setDescription(description);
		this.setHasServiceLevelProfile(sla);
	}
	
	public Field<String> getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}
	public Field<String> getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = new Field<String>(Namespace.DCTERMS, "description", description);
	}

	public Field<ServiceLevelProfile> getHasServiceLevelProfile() {
		return hasServiceLevelProfile;
	}
	public void setHasServiceLevelProfile(ServiceLevelProfile serviceLevelProfile) {
		this.hasServiceLevelProfile = new Field<ServiceLevelProfile>(Namespace.WELIVE_CORE, "hasServiceLevelProfile", serviceLevelProfile);
	}
	
}
