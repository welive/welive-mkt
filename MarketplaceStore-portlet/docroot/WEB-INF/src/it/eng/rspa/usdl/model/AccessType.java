package it.eng.rspa.usdl.model;

public enum AccessType {
	
	USER("User"),
	CLIENT("Client"),
	USER_CLIENT("User and Client");
	
	private final String text;
	private AccessType(final String access) {
        this.text = access;
    }
	
	@Override
    public String toString() {
        return text;
    }
	
	public String getShortcut(){
		if(text.equals("User")) return "U";
		if(text.equals("Client")) return "C";
		if(text.equals("User and Client")) return "UC";
		
		return "";
	}
	
}
