package it.eng.rspa.usdl.model;

public enum ProtocolType {
	
	OAUTH2("OAuth2"), 
	OPENID("OpenID"), 
	CAS("CAS"), 
	BASIC("Basic Authentication"), 
	SHIBBOLETH("Shibboleth");
	
	
	private final String text;
	private ProtocolType(final String protocol) {
        this.text = protocol;
    }
	
	@Override
    public String toString() {
        return text;
    }
}
