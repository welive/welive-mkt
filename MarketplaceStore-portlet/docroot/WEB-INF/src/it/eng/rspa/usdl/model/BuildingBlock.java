package it.eng.rspa.usdl.model;

import java.util.ArrayList;
import java.util.List;

public class BuildingBlock extends Artefact {

	public Field<List<InteractionPoint>> interactionpoints;
	
	public BuildingBlock(String title, String description, String abstractDescription, String created, String page, String[] tag, String type) {
		super(title, description, abstractDescription, created, page, tag, type);
		interactionpoints = new Field<List<InteractionPoint>>(Namespace.WELIVE_CORE, "hasInteractionPoint", new ArrayList<InteractionPoint>());
	}

	public Field<List<InteractionPoint>> getInteractionpoints() {
		return interactionpoints;
	}

	public void setInteractionpoints(Field<List<InteractionPoint>> interactionpoints) {
		this.interactionpoints = interactionpoints;
	}

	public void addInteractionPoint(InteractionPoint ip){
		this.interactionpoints.getValue().add(ip);
	}

}
