package it.eng.rspa.usdl.model;

public class ProtocolConstraint extends LUSDLResource{

	public Field<String> protocol;

	public ProtocolConstraint(String protocol) {
		this.setProtocol(protocol);
	}

	public Field<String> getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = new Field<String>(Namespace.WELIVE_CORE, "protocol", protocol);
	}
}
