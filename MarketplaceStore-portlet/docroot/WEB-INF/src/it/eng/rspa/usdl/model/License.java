package it.eng.rspa.usdl.model;

public abstract class License extends LegalCondition {

	public Field<String> url;

	public License(String title, String description, String url) {
		super(title, description);
		this.setUrl(url);
	}

	public Field<String> getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = new Field<String>(Namespace.WELIVE_CORE, "url", url);
	}
}
