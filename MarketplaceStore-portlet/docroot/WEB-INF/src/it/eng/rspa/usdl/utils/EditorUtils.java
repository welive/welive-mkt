package it.eng.rspa.usdl.utils;

import it.eng.rspa.marketplace.ConfUtil;
import it.eng.rspa.marketplace.MyMarketplaceUtility;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.utils.MyConstants;
import it.eng.sesame.Crud;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;

import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;
import javax.ws.rs.core.MediaType;

import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class EditorUtils {
	
	private static Log _log = LogFactoryUtil.getLog(EditorUtils.class);
	
	public static final String DLFOLDER_NAME_MARKETPLACE = "MarketplaceDLFolder";
	public static final String DLFOLDER_MARKETPLACE_DESCRIPTION="DLFolder Marketplace e Artefatto";
	public static final String DLFOLDER_ARTIFACT_IMAGES = "images";
	public static final String NEW_ARTIFACT_TITLE = "new";
	
	private static String validationAPIurl = ConfUtil.getString("vc.validation.enabled");
	private static URL wadlXsdUrl;
	private static URL wsdlXsdUrl;

	static{
		try { wadlXsdUrl = new URL("https://www.w3.org/Submission/wadl/wadl.xsd"); } 
		catch (MalformedURLException e) { e.printStackTrace(); }
		
		try { wsdlXsdUrl = new URL("http://schemas.xmlsoap.org/wsdl/"); } 
		catch (MalformedURLException e) { e.printStackTrace(); }
	}
	
	public static String getPilotByUser(User u){
		String[] pilotEnte = (String[]) u.getExpandoBridge().getAttribute("pilot");
		
		return pilotEnte[0];
	}

	public static String getPilotByUserId (long userId){
		
		User ente = null;
		try { ente = UserLocalServiceUtil.getUserById(userId); } 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return getPilotByUser(ente);
	}
	
	private static Artefatto newArtifact(long companyId, long groupId, long userId ) 
			throws SystemException {
		
		long artifactId = CounterLocalServiceUtil.increment();
		
		Artefatto artefatto= ArtefattoLocalServiceUtil.createArtefatto(artifactId);
			
		artefatto.setTitle(NEW_ARTIFACT_TITLE);
		artefatto.setRepositoryRDF(Crud.getRepositoryURL());
		artefatto.setCompanyId(companyId);
		artefatto.setGroupId(groupId); 
		artefatto.setUserId(userId);
		artefatto.setDate(GregorianCalendar.getInstance().getTime());
		//artefatto.setStatusDate(GregorianCalendar.getInstance().getTime());
		
		int idImgIconDefault=-1;
		artefatto.setImgId(idImgIconDefault);
		
		//artefatto.setOwlsDLEntryId(-1);
//		artefatto.setWarApkDLEntryId(-1);
		artefatto.setStatus(WorkflowConstants.STATUS_DRAFT);
		
		return ArtefattoLocalServiceUtil.addArtefatto(artefatto);
	}
	
	public static PortletURL getPublishUrl(User user, String gadgetEndpointUrl, String title, String description, RenderResponse renderResponse){
		try{	
			URL url = new URL(gadgetEndpointUrl);
			InputStream is = url.openStream();
			byte[] fileBytes = org.apache.commons.io.IOUtils.toByteArray(is);
			String xmlMsg = new String(fileBytes);
			
			Artefatto a = newArtifact(user.getCompanyId(), user.getGroupId(), user.getUserId());
			long artId = a.getArtefattoId();
			is = new ByteArrayInputStream(xmlMsg.getBytes());
			
			
			DLFolder mainMarketplaceFolder = DLFolderLocalServiceUtil.fetchFolder(MyConstants.API_DL_ID, 0L, DLFOLDER_NAME_MARKETPLACE);
			DLFolder artifactFolder = DLFolderLocalServiceUtil.fetchFolder(MyConstants.API_DL_ID, mainMarketplaceFolder.getFolderId(), artId+"");
			ServiceContext serviceContext = new ServiceContext();
			if (artifactFolder==null){
	
				Folder folderA = DLAppServiceUtil.addFolder(MyConstants.API_DL_ID, mainMarketplaceFolder.getFolderId(),  artId+"", "DescrizioneFolder", serviceContext);
				artifactFolder = DLFolderLocalServiceUtil.getDLFolder(folderA.getFolderId());
			}
			
			FileEntry dlFileEntry = DLAppLocalServiceUtil.addFileEntry( user.getUserId(),
																		artifactFolder.getRepositoryId(), 
																		artifactFolder.getFolderId(),
																		title+".xml", 
																		MimeTypesUtil.getContentType(is, title), 
																		title+".xml",
																		description,
																		"",
																		fileBytes,
																		serviceContext);
			
//			long entryId = dlFileEntry.getFileEntryId();
//			a.setWarApkDLEntryId(entryId);
			
			ArtefattoLocalServiceUtil.updateArtefatto(a);
			
			PortletURL retUrl = renderResponse.createRenderURL();
			retUrl.setParameter("jspPage", "/html/editorusdl/view.jsp");
			retUrl.setParameter("artefattoId", String.valueOf(artId));
			retUrl.setParameter("redirectTo", "/html/marketplacestore/carica_artefatto_new.jsp");
			
			return retUrl;
		}
		catch(Exception e){
			_log.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public static void uploadOnRepositoryRDF(String modelString, String modelName, boolean update)
			throws RepositoryException, ConnectException, RDFParseException, IOException{
		
		String nameRDF = "file://" + modelName; 
		Crud c = new Crud();
		if(update)
			c.clearContext(nameRDF);
			
		c.addRDFString(modelString, nameRDF);
	}
	
	public static boolean validateXml(InputStream xmlStream, InputStream xsdStream){
//		try{
//			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//	        Schema schema = factory.newSchema(new StreamSource(xsdStream));
//	        Validator validator = schema.newValidator();
//	        validator.validate(new StreamSource(xmlStream));
//	    }
//	    catch(Exception ex){
//	        return false;
//	    }
		
		return true;
	}
	
	public static JSONObject validateServiceDescriptor(String url, String type){
		
		JSONObject jResp = JSONFactoryUtil.createJSONObject();
		jResp.put("error", false);
		jResp.put("valid", true);
		
		Client client = Client.create();
		client.addFilter(MyMarketplaceUtility.getBasicAuthenticationUser());
		
		String paramType = type;
		if(type.equalsIgnoreCase(PortletProps.get("Category.rest"))){
			paramType = "rest";
		}
		else if(type.equalsIgnoreCase(PortletProps.get("Category.soap"))){
			paramType = "soap";
		}
		
		if(paramType!=null){
			try{
				JSONObject body = JSONFactoryUtil.createJSONObject();
				body.put("url", url);
				body.put("type", paramType);
				
				System.out.println(validationAPIurl+"\t-->\t"+body);
				WebResource.Builder webResource = client.resource(validationAPIurl).type(MediaType.APPLICATION_JSON);
				ClientResponse cResp = webResource.post(ClientResponse.class, body.toString());
				
				String sResp = cResp.getEntity(String.class);
				System.out.println(sResp);
				jResp = JSONFactoryUtil.createJSONObject(sResp);
			}
			catch(Exception e){
				e.printStackTrace();
				jResp.put("error", true);
				jResp.put("message", "unable-to-validate");
			}
		}
		
		return jResp;
	}
	
	@Deprecated
	public static boolean validateInteractionPoint(String url, String type){
		
		InputStream xsdStream = null;
		
		if(type.equalsIgnoreCase(PortletProps.get("Category.rest"))){
			try{ xsdStream = wadlXsdUrl.openStream(); }
			catch(Exception e){e.printStackTrace();}
		}
		else if(type.equalsIgnoreCase(PortletProps.get("Category.soap"))){
			try{ xsdStream = wsdlXsdUrl.openStream(); }
			catch(Exception e){e.printStackTrace();}
		}
		
		if(xsdStream!=null){
			try{
				InputStream xmlStream = new URL(url).openStream();
				validateXml(xmlStream, xsdStream);
			}
			catch(Exception e){
				e.printStackTrace();
				return false; 
			}
		}
		
		return true;
	}
}
