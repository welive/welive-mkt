package it.eng.rspa.usdl.model;

public enum BusinessRole {
	
	PROVIDER("Provider"),
	AUTHOR("Author"),
	OWNER("Owner");	
	
	private final String text;
	private BusinessRole(final String role) {
        this.text = role;
    }
	
	@Override
    public String toString() {
        return text;
    }
}
