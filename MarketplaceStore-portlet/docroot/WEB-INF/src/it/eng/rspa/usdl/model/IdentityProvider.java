package it.eng.rspa.usdl.model;

public class IdentityProvider extends LUSDLResource {

	Field<String> identifier;

	public IdentityProvider(String identifier) {
		this.setIdentifier(identifier);
	}

	public Field<String> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = new Field<String>(Namespace.DCTERMS, "identifier", identifier);
	}
}
