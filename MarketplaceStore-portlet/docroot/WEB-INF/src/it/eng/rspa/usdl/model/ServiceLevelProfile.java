package it.eng.rspa.usdl.model;

public class ServiceLevelProfile extends LUSDLResource{

	public Field<String> title;
	public Field<String> description;
	public Field<String> format;
	
	
	public ServiceLevelProfile(String title, String description, String format) {
		this.setTitle(title);
		this.setDescription(description);
		this.setFormat(format);
	}
	
	public Field<String> getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}
	public Field<String> getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = new Field<String>(Namespace.DCTERMS, "description", description);
	}
	public void setFormat(String format) {
		this.format = new Field<String>(Namespace.DCTERMS, "format", format);
	}
	public Field<String> getFormat(){
		return this.format;
	}
	
}
