package it.eng.rspa.usdl.model;

import java.util.HashSet;
import java.util.Set;

public abstract class Artefact extends LUSDLResource{
	
	public Field<String> title;
	public Field<String> description;
	public Field<String> abstractDescription;
	public Field<String> created;
	public Field<String> page;
	public Field<String[]> tag;
	public Field<String> type;
	
	public Field<Set<Entity>> hasBusinessRole;
	public Field<Set<TermsAndCondition>> hasLegalCondition;
	public Field<Set<ServiceOffering>> hasServiceOffering;
	public Field<Set<SecurityMeasure>> hasSecurityMeasure;
	public Field<Set<Artefact>> uses;
	
	public Artefact(String title, 
					String description, 
					String abstractDescription, 
					String created,
					String page, 
					String[] tag,
					String type) {
		
		this.setTitle(title);
		this.setDescription(description);
		this.setAbstractDescription(abstractDescription);
		this.setCreated(created);
		this.setPage(page);
		this.setTag(tag);
		this.setType(type);
		this.setUses(new HashSet<Artefact>());
		
		hasBusinessRole = new Field<Set<Entity>>(Namespace.WELIVE_CORE, "hasBusinessRole", new HashSet<Entity>());
		hasLegalCondition = new Field<Set<TermsAndCondition>>(Namespace.WELIVE_CORE, "hasLegalCondition", new HashSet<TermsAndCondition>());
		hasServiceOffering = new Field<Set<ServiceOffering>>(Namespace.WELIVE_CORE, "hasServiceOffering", new HashSet<ServiceOffering>());
		hasSecurityMeasure = new Field<Set<SecurityMeasure>>(Namespace.WELIVE_CORE, "hasSecurityMeasure", new HashSet<SecurityMeasure>());
	}

	public Field<String> getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = new Field<String>(Namespace.DCTERMS, "title", title);
	}

	public Field<String> getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = new Field<String>(Namespace.DCTERMS, "description", description);
	}

	public Field<String> getAbstractDescription() {
		return abstractDescription;
	}

	public void setAbstractDescription(String abstractDescription) {
		this.abstractDescription = new Field<String>(Namespace.DCTERMS, "abstract", abstractDescription);
	}

	public Field<String> getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = new Field<String>(Namespace.DCTERMS, "created", created);
	}

	public Field<String> getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = new Field<String>(Namespace.FOAF, "page", page);
	}

	public Field<String[]> getTag() {
		return tag;
	}

	public void setTag(String[] tag) {
		this.tag = new Field<String[]>(Namespace.TAGS, "tag", tag);
	}
	
	public Field<String> getType() {
		return type;
	}

	public void setType(String type) {
		this.type = new Field<String>(Namespace.DCTERMS, "type", type);
	}
	
	public Field<Set<Entity>> getHasBusinessRole() {
		return hasBusinessRole;
	}

	public void addBusinessRole(Entity entity) {
		this.hasBusinessRole.getValue().add(entity);
	}

	public Field<Set<TermsAndCondition>> getLegalConditions() {
		return hasLegalCondition;
	}

	public void addLegalCondition(TermsAndCondition legalCondition) {
		this.hasLegalCondition.getValue().add(legalCondition);
	}
	
	public Field<Set<ServiceOffering>> getServiceOffering() {
		return hasServiceOffering;
	}

	public void addServiceOffering(ServiceOffering serviceOffering) {
		this.hasServiceOffering.getValue().add(serviceOffering);
	}
	
	public Field<Set<SecurityMeasure>> getSecurityMeasure() {
		return hasSecurityMeasure;
	}

	public void addSecurityMeasure(SecurityMeasure measure) {
		this.hasSecurityMeasure.getValue().add(measure);
	}

	public Field<Set<Artefact>> getUses() {
		return uses;
	}
	
	public void addUses(Artefact uses) {
		this.uses.getValue().add(uses);
	}
	
	private void setUses(HashSet<Artefact> set) {
		this.uses = new Field<Set<Artefact>>(Namespace.WELIVE_CORE, "uses", set);
	}
	
}
