package it.eng.rspa.usdl.model;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

public class Field<T> {

	String namespace;
	String name;
	T value;
	
	public Field(String namespace, String name, T value) {
		this.setNamespace(namespace);
		this.setName(name);
		this.setValue(value);
	}
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {

		org.json.JSONObject json = new org.json.JSONObject();
		Calendar c = GregorianCalendar.getInstance();
		if(this.getValue() == null){ return ""; }
		else if(this.getValue() instanceof Collection<?>){
			Collection<?> collection = (Collection<?>)this.getValue();
			for(Object item : collection){
				try { json.put(Long.toString(c.getTimeInMillis()), item.toString()); } 
				catch (Exception e) { System.out.println(e.getMessage()); }
			}
		}
		else if(this.getValue() instanceof LUSDLResource){
			try { json.put(Long.toString(c.getTimeInMillis()), ((LUSDLResource)this.getValue()).toJSONObject()); } 
			catch (Exception e) { System.out.println(e.getMessage()); }
		}
		else return this.getValue().toString();
			
		return json.toString();
	}

	public org.json.JSONObject jsonValue() {

		org.json.JSONObject json = new org.json.JSONObject();
		
		if(this.getValue() == null){ return json; }
		else if(this.getValue() instanceof Collection<?>){
			Collection<?> collection = (Collection<?>)this.getValue();
			int i= 0;
			for(Object item : collection){
				try {
					LUSDLResource res = (LUSDLResource)item;
					json.put(String.valueOf(i), res.toJSONObject()); 
					i++;
				} 
				catch (Exception e) { System.out.println(e.getMessage()); }
			}
		}
		else if(this.getValue() instanceof LUSDLResource){
			LUSDLResource res = (LUSDLResource)this.getValue();
			json = res.toJSONObject();
		}
		return json;
	}
	
}
