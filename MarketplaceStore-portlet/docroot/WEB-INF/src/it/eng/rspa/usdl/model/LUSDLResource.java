package it.eng.rspa.usdl.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class LUSDLResource {
	
	public static List<java.lang.reflect.Field> getAllFields(List<java.lang.reflect.Field> fields, Class<?> type) {
	    fields.addAll(Arrays.asList(type.getDeclaredFields()));

	    if (type.getSuperclass() != null) {
	        fields = getAllFields(fields, type.getSuperclass());
	    }

	    return fields;
	}
	
	public org.json.JSONObject toJSONObject(){
		
		org.json.JSONObject json = new org.json.JSONObject();
		List<java.lang.reflect.Field> fields = getAllFields(new ArrayList<java.lang.reflect.Field>(), this.getClass());

		for(java.lang.reflect.Field f: fields){
			Field<?> currField = null;
			try { currField = (Field<?>)f.get(this); }
			catch (Exception e1) { e1.printStackTrace(); }
			if(currField != null){
				try { 
					if(currField.getValue() instanceof Collection<?> || currField.getValue() instanceof LUSDLResource){
						json.put(currField.getName(), currField.jsonValue());
					}
					else{
						json.put(currField.getName(), currField.getValue());
					}
				} 
				catch (Exception e) { System.out.println(e.getMessage()); }
			}
		}
		
		return json;
	}
	
	@Override
	public String toString(){	
		org.json.JSONObject json = this.toJSONObject();
		System.out.println(json);
		return json.toString();
	}

}
