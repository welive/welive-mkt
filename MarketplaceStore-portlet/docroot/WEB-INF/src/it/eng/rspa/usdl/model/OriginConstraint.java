package it.eng.rspa.usdl.model;

public class OriginConstraint extends LUSDLResource{

	public Field<String> origin;

	public OriginConstraint(String origin) {
		this.setOrigin(origin);
	}

	public Field<String> getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = new Field<String>(Namespace.WELIVE_CORE, "origin", origin);
	}
	
	
	
}
