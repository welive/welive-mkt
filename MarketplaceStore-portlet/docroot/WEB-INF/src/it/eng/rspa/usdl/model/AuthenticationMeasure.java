package it.eng.rspa.usdl.model;

import java.util.HashSet;
import java.util.Set;

public class AuthenticationMeasure extends SecurityMeasure {

	Field<Set<IdentityProvider>> withIdentityProvider;
	Field<ProtocolType> protocolType;
	Field<AccessType> accessType;
	Field<Set<Permission>> requires;
	
		
	public AuthenticationMeasure(String title, String description, ProtocolType protocol, AccessType access) {
		super(title, description);
		this.setAccessType(access);
		this.setProtocolType(protocol);
		this.requires = new Field<Set<Permission>>(Namespace.WELIVE_SECURITY, "requires", new HashSet<Permission>());
	}

	public Field<Set<IdentityProvider>> getWithIdentityProvider() {
		return withIdentityProvider;
	}

	public void addIdentityProvider( IdentityProvider withIdentityProvider) {
		if(this.withIdentityProvider == null){
			this.withIdentityProvider = new Field<Set<IdentityProvider>>(Namespace.WELIVE_CORE, "withIdentityProvider", new HashSet<IdentityProvider>());
		}
		this.withIdentityProvider.getValue().add(withIdentityProvider);
	}
	
	public void addRequire( Permission permission) {
		if(this.requires == null){
			this.requires = new Field<Set<Permission>>(Namespace.WELIVE_SECURITY, "requires", new HashSet<Permission>());
		}
		this.requires.getValue().add(permission);
	}

	public Field<ProtocolType> getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(ProtocolType protocolType) {
		this.protocolType = new Field<ProtocolType>(Namespace.WELIVE_CORE, "protocolType", protocolType);
	}

	public Field<AccessType> getAccessType() {
		return accessType;
	}

	public void setAccessType(AccessType accessType) {
		this.accessType = new Field<AccessType>(Namespace.WELIVE_CORE, "accessType", accessType);
	}

	public Field<Set<Permission>> getRequires() {
		return requires;
	}

	public void addRequires(Permission requires) {
		this.requires.getValue().add(requires);
	}
}
