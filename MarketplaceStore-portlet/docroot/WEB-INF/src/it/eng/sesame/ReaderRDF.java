package it.eng.sesame;

import it.eng.rspa.usdl.model.Namespace;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.LiteralRequiredException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.shared.PropertyNotFoundException;
import com.hp.hpl.jena.util.FileManager;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ReaderRDF {

	public final static Log _log = LogFactoryUtil.getLog(ReaderRDF.class);
	
	private Model  model;
	private Property propertyType; 
	
	public ReaderRDF(String inputFileName){
		model = this.getModel(inputFileName);
		if(model == null) return;
		
		propertyType = model.createProperty(Namespace.RDF + "type");
	}
	
	public ReaderRDF(File file){
		InputStream inputStream;
		try { inputStream = new FileInputStream(file); } 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		
		model = this.getModel(inputStream);
	}
	
	public ReaderRDF(ByteArrayOutputStream inputStream){
		model = this.getModel(inputStream);
		if(model == null) return;
	}
	
	public ReaderRDF(InputStream inputStream){
		model = this.getModel(inputStream);
		if(model == null) return;
	}
	
	public Model getModel(String inputFileName) {

		InputStream in = FileManager.get().open( inputFileName );
		if (in == null) { throw new IllegalArgumentException( "File: " + inputFileName + " not found"); }
		
		return this.getModel(in);
	}
	
	public Model getModel(ByteArrayOutputStream inputStream) {
		
		InputStream in = new ByteArrayInputStream(inputStream.toByteArray());
		return this.getModel(in);
		
	}
	
	public Model getModel(InputStream in) {
		
		//create an empty model
		Model model = ModelFactory.createDefaultModel();
		
		try{ 
			// read the RDF/XML file
			model.read(in, "");
			return model;
		}
		catch(Exception e){
			e.printStackTrace();
			return model; 
		}
	}
	
	public Model getModel(){
		return this.model;
	}
	
	public StmtIterator getResourcesOfType(String type){
		return model.listStatements(null, propertyType, ResourceFactory.createResource(type));
	}
	
	public Resource getResourceOfName(String name){
		return model.getResource(name);
	}
	
	public List<String>  getMultiplePropertyValuesList(Resource resource,Property property){
		List<String> outSet = new ArrayList<String>();
		StmtIterator stmtit = resource.listProperties(property);
		List<Statement> list = stmtit.toList();
		
		for(Statement stmt : list){
			String toBeAdded = null;
			try{
				RDFNode object = stmt.getLiteral();
				if (object instanceof Resource) { toBeAdded = object.asNode().toString().split("\\^\\^")[0]; }
				else{ toBeAdded = object.toString().split("\\^\\^")[0]; }
			}
			catch(LiteralRequiredException e){
				_log.warn("Exception:" + e.getMessage());
				toBeAdded = stmt.getLiteral().getValue().toString().split("\\^\\^")[0];
			}
			catch(PropertyNotFoundException e){
				_log.warn("Resource " + resource.toString() + " has not " + property.toString());
			}
			
			if(toBeAdded!=null)
				outSet.add(toBeAdded);
		}
		
		return outSet;
	}
	
	public HashSet<String>  getMultiplePropertyValues(Resource resource,Property property){
		HashSet<String> outSet = new HashSet<String>();
		StmtIterator stmtit = resource.listProperties(property);
		List<Statement> list = stmtit.toList();
		
		for(Statement stmt : list){
			String toBeAdded = null;
			try{
				RDFNode object = stmt.getLiteral();
				if (object instanceof Resource) { toBeAdded = object.asNode().toString().split("\\^\\^")[0]; }
				else{ toBeAdded = object.toString().split("\\^\\^")[0]; }
			}
			catch(LiteralRequiredException e){
				_log.warn("Exception:" + e.getMessage());
				toBeAdded = stmt.getLiteral().getValue().toString().split("\\^\\^")[0];
			}
			catch(PropertyNotFoundException e){
				_log.warn("Resource " + resource.toString() + " has not " + property.toString());
			}
			
			if(toBeAdded!=null)
				outSet.add(toBeAdded);
		}
		
		return outSet;
	}
	
	public String  getPropertyValue(Resource resource,Property property){
		String out=null;
		Statement stm =null;
		try{
			stm = resource.getRequiredProperty(property);
			RDFNode object = stm.getObject();    // get the object
			if (object instanceof Resource) { out=object.asNode().toString(); }
			else{ out=object.toString(); }
		}
		catch(LiteralRequiredException e){
//			_log.warn("Exception:" + out);
			out=resource.getRequiredProperty(property).getLiteral().getValue().toString();
		}
		catch(PropertyNotFoundException e){
//			_log.warn("Resource " + resource.toString() + " has not " + property.toString());
			out = null;
		}
		
		if(out!=null)
			out = out.split("\\^\\^")[0];
		
		return out;
	}
	
	private HashSet<InteractionPointBean> getEndpoints(StmtIterator it){
		
		InteractionPointBean ipb = null;
		
		HashSet<InteractionPointBean>  out = new  HashSet<InteractionPointBean>();
		
		Property propertyTitle = model.createProperty(Namespace.DCTERMS + "title");
		Property propertyDescription = model.createProperty(Namespace.DCTERMS + "description");
		Property propertyRestInterfaceOperation = model.createProperty(Namespace.WELIVE_CORE + "wadl");
		Property propertySoapInterfaceOperation = model.createProperty(Namespace.WELIVE_CORE + "wsdl");
		Property propertyUrl = model.createProperty(Namespace.SCHEMA + "url");
		
		if(!it.hasNext()){
			return out;
		}
		
		String title = "";
		String description = "";
		String wadl = null;
		String url = "";
		
		Resource interactionPoint;
		
		while(it.hasNext()){
			
			interactionPoint = it.next().getSubject();
			
			title = getPropertyValue(interactionPoint, propertyTitle);
			description = getPropertyValue(interactionPoint, propertyDescription);
			url = getPropertyValue(interactionPoint, propertyUrl);
			wadl = getPropertyValue(interactionPoint, propertyRestInterfaceOperation);
			if(wadl==null){
				wadl = getPropertyValue(interactionPoint, propertySoapInterfaceOperation);
				if(wadl==null){
					wadl=url;
				}
			}
			
			ipb = new InteractionPointBean(title, wadl, url, description);
			
			out.add(ipb);
		}
		
		return out;
	}
	
	public HashSet<InteractionPointBean> getInteractions(){
		
		HashSet<InteractionPointBean>  out = new  HashSet<InteractionPointBean>();
		
		/** REST endpoints **/
		StmtIterator itrest = getResourcesOfType(Namespace.WELIVE_CORE+"RESTWebServiceIP");
		out.addAll(getEndpoints(itrest));
		
		/** SOAP endpoints **/
		StmtIterator itsoap = getResourcesOfType(Namespace.WELIVE_CORE+"SOAPWebServiceIP");
		out.addAll(getEndpoints(itsoap));
		
		/** Generic endpoints **/
		StmtIterator it = getResourcesOfType(Namespace.WELIVE_CORE+"InteractionPoint");
		out.addAll(getEndpoints(it));
		
		return out;
	}
	
	public HashSet<String> getTags(){
		
		HashSet<String> out = new HashSet<String>();
		Property tagProp = model.createProperty(Namespace.TAGS+"tag");
		
		Resource service = getServiceResource();
		if(service == null)
			return out;
		
		HashSet<String> tag = getMultiplePropertyValues(service, tagProp);
		out.addAll(tag);
		
		return out;
		
	}
	
	public Resource getServiceResource(){
		StmtIterator stmtIt = getResourcesOfType( Namespace.WELIVE_CORE+"WeLiveArtefact");
		StmtIterator stmtItArt = getResourcesOfType( Namespace.WELIVE_CORE+"Artifact");
		StmtIterator stmtItBB = getResourcesOfType( Namespace.WELIVE_CORE+"BuildingBlock");
		StmtIterator stmtItPSA = getResourcesOfType( Namespace.WELIVE_CORE+"PublicServiceApplication");
		StmtIterator stmtItDS = getResourcesOfType( Namespace.WELIVE_CORE+"Dataset");
		
		Resource artefact = null;
		Set<Statement> services = new HashSet<Statement>();
		
		if(stmtIt!=null) services.addAll(stmtIt.toSet());
		if(stmtItArt!=null) services.addAll(stmtItArt.toSet());
		if(stmtItBB!=null) services.addAll(stmtItBB.toSet());
		if(stmtItPSA!=null) services.addAll(stmtItPSA.toSet());
		if(stmtItDS!=null) services.addAll(stmtItDS.toSet());
		if(!services.isEmpty()) artefact = services.iterator().next().getSubject();
		
		return artefact;
	}

}
