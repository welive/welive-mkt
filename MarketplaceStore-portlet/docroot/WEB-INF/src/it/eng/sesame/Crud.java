/**
 * http://openrdf.callimachus.net/sesame/2.7/docs/users.docbook?view#Parsing_and_Writing_RDF_with_Rio
 * 
 */
package it.eng.sesame;

import it.eng.rspa.marketplace.ConfUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

import org.openrdf.model.Resource;
import org.openrdf.model.Value;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.BooleanQuery;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.query.resultio.sparqlxml.SPARQLResultsXMLWriter;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.BasicParserSettings;
import org.openrdf.rio.rdfxml.RDFXMLWriter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class Crud {

	public static String sesameServerURI = ConfUtil.getString("Crud.SesamServerURI");
	public static String repositoryID = ConfUtil.getString("Crud.SesameRepositoryName");
	
	private static Log _log = LogFactoryUtil.getLog(Crud.class);
	
	//	public  final static String repositoryID = "RepoNJS";

	//public static final String baseDir="C:/SviluppoLiferay/modelli_RDF/"; //$NON-NLS-1$

	public static Repository repo;
	private boolean updatedRef = false;
	
	private void updateRef(){
		
		if(!sesameServerURI.equals(ConfUtil.getString("Crud.SesamServerURI"))){
			sesameServerURI = ConfUtil.getString("Crud.SesamServerURI");
			updatedRef = true;
		}
		if(!repositoryID.equals(ConfUtil.getString("Crud.SesameRepositoryName"))){
			repositoryID = ConfUtil.getString("Crud.SesameRepositoryName");
			updatedRef = true;
		}
		
		return;
	}

	public  String  addRDFfile(String filePath2,String nomeResourceRDF) throws RepositoryException, IOException, RDFParseException{

		updateRef();
		
		File file = new File(filePath2);
		if (!file.exists()){
			throw new FileNotFoundException("File inesistente "+filePath2); //$NON-NLS-1$
		}

		Resource resource=null;
		if (!nomeResourceRDF.contains("file://")){ //$NON-NLS-1$
			nomeResourceRDF= "file://"+nomeResourceRDF; //$NON-NLS-1$
		} 
		
		resource=new URIImpl(nomeResourceRDF );
		RepositoryConnection con = getRepository().getConnection();

		try{  
			con.add(file, null, RDFFormat.RDFXML, resource);
			_log.info("RDF File succesfully uploaded");
		}finally{ 
			con.close(); 
		}
		
		return resource.stringValue();
	}

	public  void clearContext(String context) throws RepositoryException, IOException{
		
		updateRef();
		
		if (!context.contains("file://")){ //$NON-NLS-1$
			context=("file://")+context; //$NON-NLS-1$
		}
		Resource resource=new URIImpl(context); 

		RepositoryConnection con = getRepository().getConnection();
		try{
			con.clear(resource);

		}
		finally{ 
			con.close(); 
		}
		_log.info("Context: "+context+" deleted"); //$NON-NLS-1$

	}


	/**
	 * 
	 * @param printContextName se true stampa la lista dei nomi dei contesti, altrimenti no 
	 * @return numero di contesti, -1 in caso di errore
	 * @throws RepositoryException
	 * @throws IOException
	 */
	public  int  countContext(boolean printContextName) throws RepositoryException {
		
		updateRef();
		
		RepositoryConnection con = getRepository().getConnection();
		int i=0;
		try {
			RepositoryResult<Resource> contexts=con.getContextIDs(); 
			while(contexts.hasNext()){
				i++;
				String d=contexts.next().stringValue();
				if (printContextName){
					_log.debug("--context_"+i+": "+d); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}//while
		}
		finally{
			con.close();
		} 
		return i;
	}


	/**
	 *  
	 * @throws RepositoryException 
	 * @throws IOException 
	 */
	public  Repository getRepository( ) throws RepositoryException  {
		updateRef();
		return getRepository(sesameServerURI+repositoryID);
	}

	/**
	 * Apre e chiude una connessione con il repository per verificare se il repository � raggiungibile.
	 * @return true se il repository � raggiungibile, altrimenti false. 
	 */ 
	public boolean isRepositoryOn(){
		
		updateRef();
		
		boolean opened=false;
		boolean closed=false;
		
		RepositoryConnection con = null;
		try { 
			 con = getRepository().getConnection();
			 opened=true;
		} 
		catch (Exception e) { e.printStackTrace(); }
		finally { 
			if(con!=null){
				try{ 
					con.close();
					closed = true;
				}
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		
		return opened && closed;
		
	}
	
	/**
	 * @param repo url del repository (es. http://localhost:8080/openrdf-sesame/repositories/Repo1
	 * 	 * @throws RepositoryException 
	 * @throws RepositoryException 
	 * @throws IOException 
	 */

	public Repository getRepository(String repoURL) throws RepositoryException{
		
		updateRef();
		if (repo==null || updatedRef){
			repo = new HTTPRepository(repoURL);
			repo.initialize();
			updatedRef = false;
		}
		return  repo;
	}


//	private void searchByTitle(String titleString) throws RepositoryException, QueryEvaluationException, MalformedQueryException {		
//		System.out.println("searchByTitle "+titleString); //$NON-NLS-1$
//		String queryString  = " SELECT DISTINCT c,something, t FROM CONTEXT c {something} dcterms:title {t} WHERE  t Like \""+titleString+"\" IGNORE CASE ";				 //$NON-NLS-1$ //$NON-NLS-2$
//		evaluateQuery(QueryLanguage.SERQL,queryString );
//	}


	public  File exportRDF(String resourceName, String pathDestination ) throws RepositoryException, IOException, RDFHandlerException   {
		File file01 = new File(pathDestination); 
		//System.out.println(file01.getAbsolutePath());
		
		if (!file01.exists()){
			file01.createNewFile(); 
		}
		else{
			return file01;
		}
		RepositoryConnection con = getRepository().getConnection();
//		ValueFactory f = con.getValueFactory();
//		org.openrdf.model.URI contextURI = f.createURI(resourceName);
		Resource resource=new URIImpl(resourceName);
		
		if (!resourceName.contains("file://")){ //$NON-NLS-1$
			resourceName=("file://")+resourceName; //$NON-NLS-1$
		}
		
		FileOutputStream fileOutput01 = new FileOutputStream( file01); 
		     
		try{   
			//il seguente setting disabilita il check della validit� dei campi.
			//soluzione al problema:Caused by: org.openrdf.rio.RDFParseException:  "was not recognised, and could not be verified, with datatype" 
			con.getParserConfig().set(BasicParserSettings.VERIFY_DATATYPE_VALUES, false);
			
			//UTF-8
			Charset	charset = Charset.forName("UTF-8"); //$NON-NLS-1$
			CharsetEncoder encoder = charset.newEncoder();
			RDFXMLWriter writerUTF=(RDFXMLWriter) Rio.createWriter(RDFFormat.RDFXML, new OutputStreamWriter(fileOutput01, encoder));
			
			con.export(writerUTF, resource);
//			System.out.println("Estratto UTF-8: "+resourceName+" da:"+getRepositoryURL()+" a "+file01.getAbsolutePath()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			
		}
		catch (Exception e){
			_log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			con.close();
			fileOutput01.write('\n');
			fileOutput01.close(); 
		} 

		return file01;
	}


	public boolean evaluateAskQuery(QueryLanguage sparql, String queryString2) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
		boolean out=false;		  

		RepositoryConnection con = getRepository().getConnection();
		try {             	
			BooleanQuery query = con.prepareBooleanQuery(org.openrdf.query.QueryLanguage.SPARQL, queryString2);
			out= query.evaluate();

		} finally {
			con.close();
		}

		return out;
	}



	private   String getPrefixes(QueryLanguage querylanguage) {
		//TODO: recuperare dal repository la lista dei prefissi 
		String out=""; //$NON-NLS-1$
		if (querylanguage.equals(QueryLanguage.SPARQL)){
			out="PREFIX legal:<http://www.linked-usdl.org/ns/usdl-legal#>"+ //$NON-NLS-1$
					" PREFIX foaf:<http://xmlns.com/foaf/0.1/>"+ //$NON-NLS-1$
					" PREFIX msm:<http://cms-wg.sti2.org/ns/minimal-service-model#>"+ //$NON-NLS-1$
					" PREFIX org:<http://www.w3.org/ns/org#>"+ //$NON-NLS-1$
					" PREFIX vcard:<http://www.w3.org/2006/vcard/ns#>"+ //$NON-NLS-1$
					" PREFIX dcterms:<http://purl.org/dc/terms/>"+ //$NON-NLS-1$
					" PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>"+ //$NON-NLS-1$
					" PREFIX time:<http://www.w3.org/2006/time#>"+ //$NON-NLS-1$
					" PREFIX price:<http://www.linked-usdl.org/ns/usdl-pricing#>"+ //$NON-NLS-1$
					" PREFIX psys:<http://proton.semanticweb.org/protonsys#>"+ //$NON-NLS-1$
					" PREFIX sec:<http://www.linked-usdl.org/ns/usdl-sec#>"+ //$NON-NLS-1$
					" PREFIX usdl:<http://www.linked-usdl.org/ns/usdl-core#>"+ //$NON-NLS-1$
					" PREFIX ctag:<http://commontag.org/ns#>"+ //$NON-NLS-1$
					" PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>"+ //$NON-NLS-1$
					" PREFIX owl:<http://www.w3.org/2002/07/owl#>"+ //$NON-NLS-1$
					" PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+ //$NON-NLS-1$
					" PREFIX sla:<http://www.linked-usdl.org/ns/usdl-sla#>"+ //$NON-NLS-1$
					" PREFIX blueprint:<http://bizweb.sap.com/TR/blueprint#>"+ //$NON-NLS-1$
					" PREFIX pext:<http://proton.semanticweb.org/protonext#>"+ //$NON-NLS-1$
					" PREFIX gr:<http://purl.org/goodrelations/v1#>"+ //$NON-NLS-1$
					" PREFIX doap:<http://usefulinc.com/ns/doap#>"+ //$NON-NLS-1$
					" PREFIX skos:<http://www.w3.org/2004/02/skos/core#>"+ //$NON-NLS-1$
					" PREFIX cd:<http://www.recshop.fake/cd#> "; //$NON-NLS-1$
		}
		else{
			out="USING NAMESPACE  "+ //$NON-NLS-1$
					" legal = <http://www.linked-usdl.org/ns/usdl-legal#>, "+ //$NON-NLS-1$
					" foaf = <http://xmlns.com/foaf/0.1/>, "+ //$NON-NLS-1$
					" a0 = <file:///D:/PROGRAMMAZIONE/Linked-USDL%20Editor/index.html#>, "+ //$NON-NLS-1$
					" msm = <http://cms-wg.sti2.org/ns/minimal-service-model#>, "+ //$NON-NLS-1$
					" org = <http://www.w3.org/ns/org#>, "+ //$NON-NLS-1$
					" vcard = <http://www.w3.org/2006/vcard/ns#>, "+ //$NON-NLS-1$
					" dcterms = <http://purl.org/dc/terms/>, "+ //$NON-NLS-1$
					" rdfs = <http://www.w3.org/2000/01/rdf-schema#>, "+ //$NON-NLS-1$
					" time = <http://www.w3.org/2006/time#>, "+ //$NON-NLS-1$
					" price = <http://www.linked-usdl.org/ns/usdl-pricing#>, "+ //$NON-NLS-1$
					" psys = <http://proton.semanticweb.org/protonsys#>, "+ //$NON-NLS-1$
					" sec = <http://www.linked-usdl.org/ns/usdl-sec#>, "+ //$NON-NLS-1$
					" usdl = <http://www.linked-usdl.org/ns/usdl-core#>, "+ //$NON-NLS-1$
					" ctag = <http://commontag.org/ns#>, "+ //$NON-NLS-1$
					" xsd = <http://www.w3.org/2001/XMLSchema#>, "+ //$NON-NLS-1$
					" owl = <http://www.w3.org/2002/07/owl#>, "+ //$NON-NLS-1$
					" rdf = <http://www.w3.org/1999/02/22-rdf-syntax-ns#>, "+ //$NON-NLS-1$
					" sla = <http://www.linked-usdl.org/ns/usdl-sla#>, "+ //$NON-NLS-1$
					" blueprint = <http://bizweb.sap.com/TR/blueprint#>, "+ //$NON-NLS-1$
					" pext = <http://proton.semanticweb.org/protonext#>, "+ //$NON-NLS-1$
					" gr = <http://purl.org/goodrelations/v1#>, "+ //$NON-NLS-1$
					" doap = <http://usefulinc.com/ns/doap#>, "+ //$NON-NLS-1$
					" skos = <http://www.w3.org/2004/02/skos/core#> "; //$NON-NLS-1$
		}
		return out;
	}

	/**
	 * @deprecated
	 * @param querylanguage
	 * @param queryString
	 * @param outputFilePath
	 * @throws RepositoryException
	 * @throws QueryEvaluationException
	 * @throws MalformedQueryException
	 * @throws TupleQueryResultHandlerException
	 * @throws IOException
	 */
	public  void evaluateQueryAndWriteFile(QueryLanguage querylanguage,String queryString, String outputFilePath) throws RepositoryException, QueryEvaluationException, MalformedQueryException, TupleQueryResultHandlerException, IOException    { 

		FileOutputStream out = new FileOutputStream(outputFilePath);
		try {
			SPARQLResultsXMLWriter sparqlWriter = new SPARQLResultsXMLWriter(out);
			RepositoryConnection con = getRepository().getConnection(); 
			try {
				//				String queryString = "SELECT * FROM {x} p {y}";
				TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SERQL, queryString);
				tupleQuery.evaluate(sparqlWriter);				
			}
			finally {
				con.close();
			}
		}
		finally {
			out.close();
		}
	}


	public List<String> evaluateQuery(QueryLanguage querylanguage,String queryString ) throws RepositoryException, QueryEvaluationException, MalformedQueryException {
		List<String> bindingNames=new ArrayList<String>();
		List<String> outList=new ArrayList<String>();
		queryString=addPrefixes(querylanguage,queryString);

		RepositoryConnection con = getRepository().getConnection();
		try { 
			//			  String queryString = "SELECT ?x ?y WHERE { ?x ?p ?y } ";
			TupleQuery tupleQuery = con.prepareTupleQuery(querylanguage, queryString);
			_log.debug(queryString); //$NON-NLS-1$

			TupleQueryResult result = tupleQuery.evaluate();
			try {
				bindingNames = result.getBindingNames();

				while (result.hasNext()) {
					BindingSet bindingSet = result.next();
					Value firstValue = bindingSet.getValue(bindingNames.get(0));
//					System.out.println("valueOfX "+firstValue.toString()); //$NON-NLS-1$
					//						Value secondValue = bindingSet.getValue(bindingNames.get(1));
					//						Value terzoValue = bindingSet.getValue(bindingNames.get(2)); 

					//						System.out.println("valueOfY "+secondValue.toString());
					//						System.out.println("valueOfz "+terzoValue.toString());

					outList.add(firstValue.toString());
				}

			}
			finally {
				result.close();
			}
		}
		finally {
			con.close();
		}
		return outList;
	}


	/**
	 * aggiunge i prefissi alla query
	 * @param querylanguage QueryLanguage.SPARQL, oppure QueryLanguage.SERQL
	 * @param queryString
	 * @return se SERQL queryString+prefissi, se SPARQL prefissi+queryString
	 */
	private String addPrefixes(QueryLanguage querylanguage, String queryString) {
		String out=""; //$NON-NLS-1$
		if (querylanguage.equals(QueryLanguage.SPARQL)){
			out=getPrefixes(querylanguage)+queryString;	
		}else{
			out=queryString+getPrefixes(querylanguage);
		}
		return out;
	}





//	/**
//	 * @deprecated
//	 * 
//	 * @param querylanguage
//	 * @param queryString
//	 */
//	public void evaluateQueryModel(QueryLanguage querylanguage,String queryString ) {
//
//		try { 
//			RepositoryConnection con = getRepository().getConnection();
//			try {			 
//				GraphQueryResult   graphResult = con.prepareGraphQuery(querylanguage,queryString).evaluate();
//				try{
//					while (graphResult.hasNext()) {
//						Statement st = graphResult.next();
//						//System.out.println("st "+st.toString()); //$NON-NLS-1$
//					}
//				}catch (Exception e){
//					e.printStackTrace();
//				}finally{
//					graphResult.close();
//				}
//
//				Model resultModel = QueryResults.asModel(con.prepareGraphQuery(querylanguage,queryString).evaluate());
//
//				Iterator<Statement> bindingNames = resultModel.iterator();
//				while (bindingNames.hasNext()) { 
//					Statement st = bindingNames.next();
//					//System.out.println("Model "+st.toString()); //$NON-NLS-1$
//				}   
//			}
//			finally {
//				con.close();
//			}
//		}
//		catch (OpenRDFException e) {
//			e.printStackTrace();
//		}
//	}


	public static String getRepositoryURL() {
		return sesameServerURI+repositoryID;
	} 
	
	public ByteArrayOutputStream exportRDFOutputStream(String resourceName) throws RepositoryException, IOException, RDFHandlerException{
		
		RepositoryConnection con = getRepository().getConnection();
		//ValueFactory f = con.getValueFactory();
		//org.openrdf.model.URI contextURI = f.createURI(resourceName);
		Resource resource=new URIImpl(resourceName);
		
		if (!resourceName.contains("file://")){ 
			resourceName=("file://")+resourceName;
		}
		
		ByteArrayOutputStream output = new ByteArrayOutputStream(); 
		     
		try{
			//il seguente setting disabilita il check della validit� dei campi.
			//soluzione al problema:Caused by: org.openrdf.rio.RDFParseException:  "was not recognised, and could not be verified, with datatype" 
			con.getParserConfig().set(BasicParserSettings.VERIFY_DATATYPE_VALUES, false);
			
			//UTF-8
			Charset	charset = Charset.forName("UTF-8"); //$NON-NLS-1$
			CharsetEncoder encoder = charset.newEncoder();
			RDFXMLWriter writerUTF = (RDFXMLWriter) Rio.createWriter(RDFFormat.RDFXML, new OutputStreamWriter(output, encoder));
			
			con.export(writerUTF, resource);
			
		}
		catch (Exception e){ e.printStackTrace(); }
		finally{
			output.close();
			con.close();
		} 
		
		return output;
	}

	public String addRDFString(String modelString, String nomeResourceRDF) throws RepositoryException, IOException, RDFParseException{
		
		InputStream is = new ByteArrayInputStream(modelString.getBytes());
	
		Resource resource=null;
		
		if (!nomeResourceRDF.contains("file://")){ //$NON-NLS-1$
			nomeResourceRDF= "file://"+nomeResourceRDF; //$NON-NLS-1$
		} 
		
		resource=new URIImpl(nomeResourceRDF);
		RepositoryConnection con = getRepository().getConnection();
	
		try{
			con.add(is, null, RDFFormat.RDFXML, resource);
			_log.debug("Caricamento file RDF completato con successo!");
		}
		finally
		{ 
			con.close(); 
		}
		
		return resource.stringValue();
	}
	
}

//String queryString = "SELECT ?x ?p ?y WHERE { ?x ?p ?y } ";			 
//			c.evaluateQuery(QueryLanguage.SPARQL,queryString);
//String filtro="*urban*";

//String queryString  = "SELECT DISTINCT c,something, t FROM CONTEXT c {something} dcterms:title {t} WHERE  t Like \""+filtro+"\" IGNORE CASE ";
//c.evaluateQuery(QueryLanguage.SERQL,queryString );


/*	
String queryString2= prefissi+
		" select  * where {?something rdf:type usdl:Service ."+
		" ?something dcterms:description ?servDescr ."+
		" FILTER (regex(?servDescr, 'vehicle number or phone MAC', 'i'))}";

c.evaluateQuery(QueryLanguage.SPARQL,queryString2 );

String askQueryString =prefissi+
		" ask {?something rdf:type usdl:Service ."+
		" ?something dcterms:description ?servDescr ."+
		" FILTER (regex(?servDescr, 'vehicle number or phone MAC', 'i'))}"; //IGNORE CASE
boolean askResult=c.evaluateAskQuery(QueryLanguage.SPARQL,askQueryString );
System.out.println("Ask query result: "+askResult);

 */

//						String   filePathDest="C:/SviluppoLiferay/modelli_RDF/provaEstratto.rdf";
//						c.extractRDF("prova.rdf",filePathDest);

//			c.addRDFfile(baseDir+"InvioRicetta.rdf");
//			c.addRDFfile(baseDir+"prova.rdf");
//			c.addRDFfile(baseDir+"birmingham.rdf");

//c.clearContext("file://prova.rdf");
//			c.clearContext("file://birmingham.rdf");
//		c.dumpRDF(System.out, SimpleGraph.NTRIPLES); 
//public static void main(String args[]) throws RepositoryException, RDFHandlerException, IOException{
//	
//	Crud c = new Crud();
//	System.out.println(new String(c.exportRDFOutputStream("file://Titolo del servizio_1456925867276").toByteArray()));
//	
//}