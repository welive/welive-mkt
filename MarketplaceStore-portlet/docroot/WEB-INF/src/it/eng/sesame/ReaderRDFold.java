package it.eng.sesame;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.jena.riot.RiotException;

import com.hp.hpl.jena.rdf.model.LiteralRequiredException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.shared.PropertyNotFoundException;
import com.hp.hpl.jena.util.FileManager;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ReaderRDFold {

	private Model  model;
	//private Property propertyAbstract ;
	//private Resource generalDescriptionService;
	private Property propertyType;
	private Property propertyDescription;
	private Property propertyTitle;
	
	/*
	private Property propertyOffering;
	private Property propertyServiceLevelProfile;
	private Property propertyPrice;
	
	private Property propertyProfile;
	private Property propertyServiceProvider;
	private Property propertyArtifact;
	private Property propertyTerms;
	private Property propertyClause;
	private Property propertyPrecondition;
	private Property propertySecurity;
	*/
	
	/*
	private Property propertyLevelName;
	private Property propertyLevelDescription;
	
	private Property propertyInteraction;
	private Property property_I_receive;
	private Property property_I_yields;
	private Property property_I_before;
	private Property property_I_after;
	*/
	
	// NAMESPACES
	
	public final static String RDFS_TYPE ="http://www.w3.org/2001/XMLSchema#";
	
	public final static String RDF_DATATYPE ="http://www.w3.org/2001/XMLSchema#";
	
	public final static String BLUEPRINT = "http://bizweb.sap.com/TR/blueprint#"; 
	
	public final static String USDL = "http://www.linked-usdl.org/ns/usdl-core#";
	public final static String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String OWL = "http://www.w3.org/2002/07/owl#";
	// public final static String DC = "http://purl.org/dc/elements/1.1/";
	public final static String DCTERMS = "http://purl.org/dc/terms/";	

	public final static String XSD = "http://www.w3.org/2001/XMLSchema#";
	public final static String VANN = "http://purl.org/vocab/vann/";
	public final static String FOAF = "http://xmlns.com/foaf/0.1/";
	public final static String USDK = "http://www.linked-usdl.org/ns/usdl#";
	
	//public final static String USDL_CORE = "http://www.linked-usdl.org/ns/usdl-core#";	
	public final static String USDL_SLA = "http://www.linked-usdl.org/ns/usdl-sla#";
	public final static String USDL_SECURITY = "http://www.linked-usdl.org/ns/usdl-security#";
	public final static String USDL_LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String USDL_PRICE = "http://www.linked-usdl.org/ns/usdl-price#";
	
	public final static String RDFS = "http://www.w3.org/2000/01/rdf-schema#";
	public final static String GR = "http://purl.org/goodrelations/v1#";
	public final static String SKOS = "http://www.w3.org/2004/02/skos/core#";
	public final static String ORG = "http://www.w3.org/ns/org#";
	public final static String PRICE = "http://www.linked-usdl.org/ns/usdl-price#";
	public final static String LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String DEI = "http://dei.uc.pt/rdf/dei#";
	public static final String TEST = "http://www.w3.org/2006/03/test-description#";
	public static final String SCHEMA = "http://schema.org/";
	public final static String  RDF_Syntax ="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	
	public final static Log _log = LogFactoryUtil.getLog(ReaderRDFold.class);
	


	public ReaderRDFold(String inputFileName)
	{
		model = this.getModel(inputFileName);
		
		if(model == null) return;
		
		propertyType = model.createProperty(RDF + "type");

		propertyDescription=model.createProperty(DCTERMS + "description"); 
		propertyTitle=model.createProperty(DCTERMS + "title"); 
		
	}
	
	public ReaderRDFold(ByteArrayOutputStream inputStream)
	{
		model = this.getModel(inputStream);
		
		if(model == null) return;
		
		propertyType = model.createProperty(RDF + "type");
		propertyDescription=model.createProperty(DCTERMS + "description"); 
		propertyTitle=model.createProperty(DCTERMS + "title"); 
	}
	
	public Model getReaderModel()
	{
		return model;
	}

	public String  getPropertyValue(Resource resource,Property property)
	{
		String out=null;
		Statement stm =null;
		try{
			stm = resource.getRequiredProperty(property);
			RDFNode object = stm.getObject();    // get the object

			if (object instanceof Resource) {
				out=object.asNode().toString();
			}
			else{
				out=object.toString();
			}
		}catch(LiteralRequiredException e){
			_log.debug("Exception:" + out);
			out=resource.getRequiredProperty(property).getLiteral().getValue().toString();
		}catch(PropertyNotFoundException e){
			_log.debug("Resource " + resource.toString() + " has not " + property.toString());
			out = "null";
		}
		
		return out;
	}

	public StmtIterator getResourcesOfType(String type){
		return model.listStatements(null, propertyType, ResourceFactory.createResource(type));
	}
	
	public Resource getResourceOfName(String name){
		return model.getResource(name);
	}

	public Model getModel(String inputFileName) {
		// create an empty model
		Model model = ModelFactory.createDefaultModel();

		InputStream in = FileManager.get().open( inputFileName );
		if (in == null) {
			throw new IllegalArgumentException( "File: " + inputFileName + " not found");
		}
		try
		{
		// read the RDF/XML file
		model.read(in, "");
		return model;
		}
		catch(RiotException e)
		{
			//e.printStackTrace();
			return null;
		}
	}
	
	public Model getModel(ByteArrayOutputStream inputStream) {
		// create an empty model
		Model model = ModelFactory.createDefaultModel();

		InputStream in = new ByteArrayInputStream(inputStream.toByteArray());
		
		try
		{
			// read the RDF/XML file
			model.read(in, "");
			return model;
		}
		catch(RiotException e)
		{
			//e.printStackTrace();
			return null;
		}
	}
	
	public String getServiceInfo(){
		
		StmtIterator it = getResourcesOfType(USDL + "WeLiveArtefact");
		
		if(it.hasNext())
		{
			Resource service = it.next().getSubject();
			
			Property propertyCreated = model.createProperty(DCTERMS + "created");
			Property propertyAbstract = model.createProperty(DCTERMS + "abstract");
			Property propertyWebpage = model.createProperty(FOAF + "page");
			
			String title = getPropertyValue(service, propertyTitle);
			String created = "|" + getPropertyValue(service, propertyCreated);
			String abs = "|" + getPropertyValue(service, propertyAbstract);
			String description = "|" + getPropertyValue(service, propertyDescription);
			String webpage = "|" + getPropertyValue(service, propertyWebpage);
						
			return title + created + abs + description + webpage;
			
		}
		else
		{
			_log.error("Service not found");
			return null;
		}
	}
	
	public String getAuthorInfo(){
		
		StmtIterator it = getResourcesOfType(USDL + "Author");
		
		if(it.hasNext())
		{
			Resource author = it.next().getSubject();
			
			Property propertyName = model.createProperty(DCTERMS + "title");
			Property propertyEmail = model.createProperty(FOAF + "mbox");
			
			String name = getPropertyValue(author, propertyName);
			String email = "|" + getPropertyValue(author, propertyEmail);
			
			return name + email;
			
		}
		else
		{
			_log.warn("Author not found");
			return null;
		}
	}
	
	public String[] getServiceOffering(String name){
		
		String out[] = new String[5] ;
		
		//offering
		Property propertyTitleOffering = model.createProperty(DCTERMS + "title");
		Property propertyDescriptionOffering = model.createProperty(DCTERMS + "description");
		
		Property propertySLPtitle = model.createProperty(DCTERMS + "title");
		Property propertySLPdescription = model.createProperty(DCTERMS + "description");
		Property propertySLPformat = model.createProperty(DCTERMS + "format");
		
		try{			
			Resource serviceOffering = this.getResourceOfName(name);
			
			if(serviceOffering!=null){
				
				String id = serviceOffering.toString().split("#Offering")[1];
				Resource slp = this.getResourceOfName(USDL + "ServiceLevelProfile"+id);
				
				out[0]=this.getPropertyValue(serviceOffering, propertyTitleOffering);
				out[1]=this.getPropertyValue(serviceOffering, propertyDescriptionOffering);
				out[2]= this.getPropertyValue(slp, propertySLPtitle);
				out[3]=this.getPropertyValue(slp, propertySLPdescription);
				out[4]=this.getPropertyValue(slp, propertySLPformat);
			}
			else{
				_log.warn("ServiceOffering not found!");
				out[0] = "";
				out[1] = "";
				out[2] = "";
				out[3] = "";
				out[4] = "";
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
				
		return out;
	}
	
	public String getTitleProfile()
	{
		
		String out="";
		try
		{
			StmtIterator it = getResourcesOfType(USDL_SECURITY + "SecurityProfile");
			
			if(it.hasNext())
			{				
				Resource securityProfile = it.next().getSubject();			
				Property propertyTitleProfile=model.createProperty("http://purl.org/dc/terms/title");

				out=getPropertyValue(securityProfile, propertyTitleProfile);
			}
			else
			{
				_log.warn("SecurityProfile not found!");
				out= "";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public String[] getServiceProvider()
	{
		String out[] = new String[3] ;
		try
		{
			StmtIterator it = getResourcesOfType(USDL + "Provider");
			
			if(it.hasNext())
			{
				Resource serviceProvider = it.next().getSubject();
				
				Property propertyTitle = model.createProperty(DCTERMS + "title");
				Property propertyDescription = model.createProperty(FOAF + "homepage");

				out[0] = getPropertyValue(serviceProvider, propertyTitle);
				out[1] = getPropertyValue(serviceProvider, propertyDescription);
			}
			else
			{
				_log.warn("Provider not found");
				out[0] = "";
				out[1] = "";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public String[] getOwnerInfo()
	{
		String out[] = new String[3] ;
		try
		{
			StmtIterator it = getResourcesOfType(USDL + "Owner");
			
			if(it.hasNext())
			{
				Resource serviceProvider = it.next().getSubject();
				
				Property propertyTitle = model.createProperty(DCTERMS + "title");
				Property propertyDescription = model.createProperty(FOAF + "homepage");

				out[0] = getPropertyValue(serviceProvider, propertyTitle);
				out[1] = getPropertyValue(serviceProvider, propertyDescription);
			}
			else
			{
				_log.warn("Owner not found");
				out[0] = "";
				out[1] = "";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public ArrayList<HashMap<Integer,Object>> getArtifacts()
	{
		//_log.debug("GET ARTIFACTS");
					
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		String location = "";
		
		StmtIterator it = getResourcesOfType(USDL + "Dependency");
		
		if(!it.hasNext())
		{
			_log.warn("Artifact not found");
			return null;
		}
		
		Resource artifact;
		
		Property propertyTitle = model.createProperty(DCTERMS + "title");
		Property propertyDescription = model.createProperty(DCTERMS + "description");
		Property propertyLocation = model.createProperty(DCTERMS + "location");
		String uri_id = "";
		int uri_length = (USDL + "WeLiveArtefact").length();
				
		int i = 0;
		
		while(it.hasNext())
		{
			artifact = it.next().getSubject();
			uri_id = "|" + artifact.getURI().substring(uri_length);
			
			title = getPropertyValue(artifact, propertyTitle);
			description = "|" + getPropertyValue(artifact, propertyDescription);
			location = "|" + getPropertyValue(artifact, propertyLocation);
			
			e.put(i, title + description + location + uri_id);
			out.add(e);
			
			i++;
		}
		
		return out;
	}
		
	public ArrayList<HashMap<Integer,Object>> getTerms(){
					
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		String url = "";
		String customOrStandard = "";
		
		StmtIterator it = getResourcesOfType(USDL + "LegalCondition");
		
		if(!it.hasNext()){
			_log.warn("Terms not found!");
			return null;
		}
		
		Resource terms;
		
		Property propertyTitle = model.createProperty(DCTERMS + "title");
		Property propertyDescription = model.createProperty(DCTERMS + "description");
		Property propertyStandardURL = model.createProperty(SCHEMA + "url");
		Property propertyCustomPage = model.createProperty(FOAF + "page");
		
		int i = 0;
				
		while(it.hasNext()){
			terms = it.next().getSubject();
			
			title = getPropertyValue(terms, propertyTitle);
			description = getPropertyValue(terms, propertyDescription);
			url = getPropertyValue(terms,propertyStandardURL);
			customOrStandard = "standard";
			
			if(url==null || url.equals("null") || url.equals("")){
				url = getPropertyValue(terms,propertyCustomPage);
				customOrStandard = "custom";
			}
			
			e.put(i, title + "|" + description + "|" + url + "|" + customOrStandard);
			out.add(e);
			
			i++;
		}
		
		return out;
	}
			
	public ArrayList<HashMap<Integer,Object>> getSecurityMeasure()
	{
		//_log.debug("GET SECURITYMEASURE");
		
		ArrayList<HashMap<Integer,Object>> out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		
		StmtIterator it = getResourcesOfType(USDL_SECURITY + "SecurityMeasure");
		
		if(!it.hasNext())
		{
			_log.debug("EDITOR L-USDL ### WARNING: SecurityMeasure not found!");
			//_log.debug("GET SECURITYMEASURE END");
			return null;
		}
		
		Resource security;
		
		int i = 0;
		
		while(it.hasNext())
		{
			security = it.next().getSubject();
			
			title = getPropertyValue(security, propertyTitle);
			description = "|" + getPropertyValue(security, propertyDescription);
			
			
			e.put(i, title + description);
			out.add(e); 
			
			i++;			
		}
		
		/*
		for(int i=0; i<20; i++)						
		{		   								
			myP = "http://www.w3.org/1999/02/22-rdf-syntax-ns#SecurityMeasure"+i;
			propertySecurity= model.createProperty(myP); 
			_log.debug("propertySecurity________"+ propertySecurity);
				try
				{
					Property propertyTitle=model.createProperty("http://purl.org/dc/terms/title");
					Property propertyDescription=model.createProperty("http://purl.org/dc/terms/description");								
					
					if(!getPropertyValue(propertySecurity, propertyTitle).isEmpty())
					{	
						title = getPropertyValue(propertySecurity, propertyTitle);
						description = "|" + getPropertyValue(propertySecurity, propertyDescription);
						
						
						s += title += description;
														
						e.put(i, s);
						out.add(e); 
					}
					else
					{ 
						break; 
					}	
					
				}catch(Exception ex)
				{
					_log.debug("securityMeasure finiti.");
				}
				
			s="";
			
		}*/	
		//_log.debug("GET SECURITYMEASURE END");
		return out;
	}
			
	public ArrayList<HashMap<Integer,Object[]>> getServiceLevel(){
							
		ArrayList<HashMap<Integer,Object[]>> out = new  ArrayList<HashMap<Integer,Object[]>>();
		HashMap<Integer,Object[]> e = new HashMap<Integer,Object[]>();
		
		StmtIterator it = getResourcesOfType(USDL + "Offering");
		
		if(!it.hasNext()){
			_log.warn("Offering not found!");
			return null;
		}
		
		int i = 0;
		
		while(it.hasNext()){		
			e.put(i, getServiceOffering(it.next().getSubject().toString()));
			out.add(e); 
			
			i++;
		}
		
		//_log.debug("GET SERVICELEVEL END");
		return out;
	}

	public ArrayList<HashMap<Integer,Object>> getInteractions(){
		
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		StmtIterator it = getResourcesOfType(USDL + "InteractionPoint");
		
		if(!it.hasNext())
		{
			_log.warn("InteractionPoint not found");
			return null;
		}
		
		String out_string = "";
		String title = "";
		String description = "";
		String interfaceOperation = "";
		String yields = "";
		String yields_isarray = "";
		
		String isCollection = "";
		String label = "";
		String type = "";
		
		Resource interactionPoint;
		Resource parameter;
		Resource yield;
		
		StmtIterator it_parameter;
		
		Property propertyInterfaceOperation = model.createProperty(SCHEMA + "url");
		Property propertyIsCollection = model.createProperty(USDL + "isCollection");
		Property propertyParameterType = model.createProperty(XSD + "type");	
		
		int i = 0;
		String id = "";
		String URI = "";
		String tmp = "";
		
		int uri_length = (USDL + "InteractionPoint").length();
		
		int par_count;
		
		while(it.hasNext())
		{
			par_count = 0;
			
			interactionPoint = it.next().getSubject();
			URI = interactionPoint.getURI();
			id = URI.substring(uri_length);
			
			title = getPropertyValue(interactionPoint, propertyTitle);
			description = "|" + getPropertyValue(interactionPoint, propertyDescription);
			interfaceOperation = "|" + getPropertyValue(interactionPoint, propertyInterfaceOperation);
			
			yield = model.getResource(USDL + "Output_" + id);
			
			try{
				yields = getPropertyValue(yield, propertyParameterType).split("\\^\\^")[0];
			}
			catch(Exception ex){
				_log.warn(ex.getMessage());
				yields = "string";
			}
			
			if(yields.compareTo("null") == 0){
				_log.warn("No output founded");
				yields = "string";
			}
			
			try{
				yields_isarray = getPropertyValue(yield, propertyIsCollection).split("\\^\\^")[0];
			}
			catch(Exception ex){
				_log.warn(ex.getMessage());
				yields_isarray = "false";
			}
			
			if(yields_isarray.compareTo("null") == 0){
				_log.warn("Unable to check if the output is a collection or not");
				yields_isarray = "false";
			}
			
			yields = "|" + yields + "__" + yields_isarray;
			
			out_string = title + description + interfaceOperation + yields;
			
			it_parameter = getResourcesOfType(USDL + "Parameter");
			
			while(it_parameter.hasNext())
			{
				parameter = it_parameter.next().getSubject();
				URI = parameter.getURI();
				tmp = URI.split("_")[1];
				
				if(tmp.compareTo(id) == 0)
				{
					if(par_count == 0)
						type = "|" + getPropertyValue(parameter, propertyParameterType).split("\\^\\^")[0];
					else
						type = "__" + getPropertyValue(parameter, propertyParameterType).split("\\^\\^")[0];
					
					isCollection = "__" + getPropertyValue(parameter, propertyIsCollection).split("\\^\\^")[0];
					label = "__" + getPropertyValue(parameter, propertyTitle);
					
					out_string += type += label += isCollection;
					
					par_count++;
				}
			}
			
			if(par_count == 0) out_string += "|null__null__null";
			
			//TODO: Inserire la parte relativa ai Vincoli tecnici (Technical Constraint)
			
			e.put(i, out_string);			
			
			i++;
		}
		
		out.add(e);
		//_log.debug("GET INTERACTIONPOINTS END");
		return out;
	}
	
	public String getClassificationType()
	{ 
		String out="";
		try{
			/*
			Property propertyClassification=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#hasClassification");

			out=getPropertyValue(generalDescriptionService, propertyClassification);
			
			*/
			Property propertyClassification=model.createProperty(RDFS + "label");
			Resource classification = model.getResource("http://www.w3.org/2004/02/skos/core#Classification");
			out = getPropertyValue(classification, propertyClassification);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
		
	public String getMarketplaceType()
	{ 
		String out = "";
		try
		{
			
			Property propertyMarketplace=model.createProperty(RDFS + "label");
			Resource classification = model.getResource("http://www.w3.org/2004/02/skos/core#Marketplace");
			out = getPropertyValue(classification, propertyMarketplace);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return out;
	}

//	public String getDescriptionService() {
//		String out="";
//		try{ 
//			out=getPropertyValue(generalDescriptionService, propertyDescription);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}

//	public String getAboutTitle() {
//		String out="";
//		try{
//			StmtIterator risultato = getSubjects(model,propertyType, "http://www.linked-usdl.org/ns/usdl-core#ServiceDescription");
//			Resource serviceDescription=risultato.next().getSubject(); 
//
//			out=getPropertyValue(serviceDescription, propertyTitle);
//
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
	
	
//	public ArrayList<HashMap<String,Object>> getTermsAndConditions(){
//		ArrayList<HashMap<String,Object>>  out=new  ArrayList<HashMap<String,Object>>();		 
//		try{ 			  
//			StmtIterator risultato = getSubjects(model, propertyType, "http://www.linked-usdl.org/ns/usdl-legal#TermsAndConditions");
//			while(risultato.hasNext()){
//				out.add(getTermsAndConditionsMap(risultato.next().getSubject()));
//			} 
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		return out;
//	}


//	private HashMap<String,Object> getTermsAndConditionsMap(Resource subject) {
//		HashMap<String,Object> out=new HashMap<String,Object> ();
//		String valore=getPropertyValue(subject, propertyTitle);
//		out.put("title", valore);
//		out.put("description",getPropertyValue(subject, propertyDescription));
//		StmtIterator iteratore = subject.listProperties(model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#hasClause"));
// 
//		ArrayList <String> listaClause=new ArrayList<String>();
//		
//		while (iteratore.hasNext()){
//			RDFNode obj = iteratore.next().getObject();
// 
//			//coppie(name,text)
//			listaClause.add(getPropertyValue(obj.asResource(), model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#name")));
//			listaClause.add(getPropertyValue(obj.asResource(), model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#text")));
//		}
//		out.put("clause", listaClause);
//		return out;
//	}
//
//
//
//	public ArrayList<HashMap> getOffering() {
//		ArrayList<HashMap> out=new ArrayList<HashMap>();
//		try{ 
//			StmtIterator risultato = getSubjects(model,propertyType, "http://www.linked-usdl.org/ns/usdl-core#ServiceOffering");
//			while(risultato.hasNext()){
//				out.add(getOfferingMap(risultato.next().getSubject()));
//			} 
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
//
//	private HashMap<String, Object> getOfferingMap(Resource offering){
//		HashMap<String, Object> out=new HashMap<String, Object>();
//		if (offering!=null){
//			Property property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#validFrom");
//
//			String valid="da: "+getPropertyValue(offering, property);
//			if (valid.contains("^^")){
//				valid=valid.substring(0,  valid.indexOf("^^"));
//			}
//			property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#validThrough");
//			valid=valid+" a: "+getPropertyValue(offering, property);
//			if (valid.contains("^^")){
//				valid=valid.substring(0,  valid.indexOf("^^"));
//			}
//			out.put("valid", valid);
//			out.put("description", getPropertyValue(offering, propertyDescription));
//			out.put("title",getPropertyValue(offering, propertyTitle).replace("@en", ""));
//
//			out.put("pricePlans",getPricePlans(offering));
//			out.put("slaProfiles",getSLAProfiles(offering));
//		}
//
//		return out;
//	}

//	private ArrayList <String>  getPricePlans(Resource offering) {
//		ArrayList <String>  out=new ArrayList <String> ();
//		Property	property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#hasPricePlan");
//		StmtIterator iterator = offering.listProperties(property);
//
//		return getListaTitoloDescrizione(iterator);
//	}
//
//	private ArrayList <String>  getListaTitoloDescrizione(StmtIterator iterator) {
//		ArrayList <String>  out=new ArrayList <String> ();
//		String titoloDescrizione="";
//		RDFNode nodoObj = null;
//		Property property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#serviceLevelExpression");
//
//		while (iterator.hasNext()){
//			nodoObj = iterator.next().getObject();
//			if (nodoObj instanceof Resource) {
//
//				titoloDescrizione=getPropertyValue((Resource)nodoObj, propertyTitle).replace("@en", "");	
//
//				if (((Resource)nodoObj).hasProperty(propertyDescription)){
//					titoloDescrizione=titoloDescrizione+": "+getPropertyValue((Resource)nodoObj, propertyDescription).replace("@en", "");
//				}
//				else if (((Resource)nodoObj).hasProperty(property)){
//
//					titoloDescrizione=titoloDescrizione+": "+	
//							getPropertyValue(
//									(Resource) (((Resource) nodoObj).listProperties(property).next().getObject()),propertyDescription).replace("@en", "");					
//				}else{
//					titoloDescrizione=titoloDescrizione+": no description";
//				}
//
//				out.add(titoloDescrizione);							  
//
//			} else {
//				// object is a literal
//				out.add( nodoObj.toString() );
//			}
//		}
//		return out;
//	}
//
//	private HashMap <String,List<String>> getSLAProfiles(Resource offering) {
//		HashMap<String,List<String>> out=new HashMap<String,List<String>>();
//		Property	property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#hasServiceLevelProfile");
//		StmtIterator iterator = offering.listProperties(property);
//
//		String profilo="";
//
//		RDFNode nodoObj = null;
//		while (iterator.hasNext()){
//			nodoObj = iterator.next().getObject();
//			if (nodoObj instanceof Resource) {
//
//				profilo=getPropertyValue((Resource)nodoObj, propertyTitle).replace("@en", "");
//
//				property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#hasServiceLevel");
//				iterator=((Resource)nodoObj).listProperties(property);
//
//				out.put(profilo,getListaTitoloDescrizione(iterator));
//
//			} else {
//				// object is a literal
//				System.out.print(" \"" + nodoObj.toString() + "\"");
//			}
//		}
//		return out;
//	}

//	public String getTitle() {
//		String out="";
//		try{  
//			out=getPropertyValue(generalDescriptionService, propertyTitle);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}

//	public String  getProviderName() {
//		String out="";
//		try{
//			Resource businessEntityResource=getSubjects(model,propertyType, "http://purl.org/goodrelations/v1#BusinessEntity").next().getSubject();
//			Property propertyProviderName=model.createProperty("http://purl.org/goodrelations/v1#legalName"); 
//
//			out=getPropertyValue(businessEntityResource, propertyProviderName);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
	
}

/** 
package it.eng;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.jena.riot.RiotException;

import com.hp.hpl.jena.rdf.model.LiteralRequiredException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.shared.PropertyNotFoundException;
import com.hp.hpl.jena.util.FileManager;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ReaderRDF {

	private Model  model;
	private Property propertyType;
	private Property propertyDescription;
	private Property propertyTitle;
	
	// NAMESPACES
	
	public final static String RDFS_TYPE ="http://www.w3.org/2001/XMLSchema#";
	
	public final static String RDF_DATATYPE ="http://www.w3.org/2001/XMLSchema#";
	
	public final static String BLUEPRINT = "http://bizweb.sap.com/TR/blueprint#"; 
	
	public final static String USDL = "http://www.linked-usdl.org/ns/usdl-core#";
	public final static String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String OWL = "http://www.w3.org/2002/07/owl#";
	public final static String DCTERMS = "http://purl.org/dc/terms/";	

	public final static String XSD = "http://www.w3.org/2001/XMLSchema#";
	public final static String VANN = "http://purl.org/vocab/vann/";
	public final static String FOAF = "http://xmlns.com/foaf/0.1/";
	public final static String USDK = "http://www.linked-usdl.org/ns/usdl#";
	
	public final static String USDL_SLA = "http://www.linked-usdl.org/ns/usdl-sla#";
	public final static String USDL_SECURITY = "http://www.linked-usdl.org/ns/usdl-security#";
	public final static String USDL_LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String USDL_PRICE = "http://www.linked-usdl.org/ns/usdl-price#";
	
	public final static String RDFS = "http://www.w3.org/2000/01/rdf-schema#";
	public final static String GR = "http://purl.org/goodrelations/v1#";
	public final static String SKOS = "http://www.w3.org/2004/02/skos/core#";
	public final static String ORG = "http://www.w3.org/ns/org#";
	public final static String PRICE = "http://www.linked-usdl.org/ns/usdl-price#";
	public final static String LEGAL = "http://www.linked-usdl.org/ns/usdl-legal#";
	public final static String DEI = "http://dei.uc.pt/rdf/dei#";
	public static final String TEST = "http://www.w3.org/2006/03/test-description#";
	public static final String SCHEMA = "http://schema.org/";
	public final static String RDF_Syntax ="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String SWRL = "http://www.w3.org/2003/11/swrl#";
	private static final String WELIVE_SECURITY = "http://welive.eu/ns/security#";
	
	public final static Log _log = LogFactoryUtil.getLog(ReaderRDF.class);
	
	public String getServiceInfo(){
		
		StmtIterator it = getResourcesOfType(USDL + "WeLiveArtefact");
		
		if(it.hasNext())
		{
			Resource service = it.next().getSubject();
			
			Property propertyCreated = model.createProperty(DCTERMS + "created");
			Property propertyAbstract = model.createProperty(DCTERMS + "abstract");
			Property propertyWebpage = model.createProperty(FOAF + "page");
			
			String title = getPropertyValue(service, propertyTitle);
			String created = "|" + getPropertyValue(service, propertyCreated);
			String abs = "|" + getPropertyValue(service, propertyAbstract);
			String description = "|" + getPropertyValue(service, propertyDescription);
			String webpage = "|" + getPropertyValue(service, propertyWebpage);
						
			return title + created + abs + description + webpage;
			
		}
		else
		{
			_log.error("Service not found");
			return null;
		}
	}
	
	public String getAuthorInfo(){
		
		StmtIterator it = getResourcesOfType(USDL + "Author");
		
		if(it.hasNext()){
			
			Resource author = it.next().getSubject();
			
			Property propertyName = model.createProperty(DCTERMS + "title");
			Property propertyEmail = model.createProperty(FOAF + "mbox");
			
			String name = getPropertyValue(author, propertyName);
			String email = "|" + getPropertyValue(author, propertyEmail);
			
			return name + email;
			
		}
		else
		{
			_log.warn("Author not found");
			return null;
		}
	}
	
	public String[] getServiceOffering(String name){
		
		String out[] = new String[5] ;
		
		//offering
		Property propertyTitleOffering = model.createProperty(DCTERMS + "title");
		Property propertyDescriptionOffering = model.createProperty(DCTERMS + "description");
		
		Property propertySLPtitle = model.createProperty(DCTERMS + "title");
		Property propertySLPdescription = model.createProperty(DCTERMS + "description");
		Property propertySLPformat = model.createProperty(DCTERMS + "format");
		
		try{			
			Resource serviceOffering = this.getResourceOfName(name);
			
			if(serviceOffering!=null){
				
				String id = serviceOffering.toString().split("#Offering")[1];
				Resource slp = this.getResourceOfName(USDL + "ServiceLevelProfile"+id);
				
				out[0]=this.getPropertyValue(serviceOffering, propertyTitleOffering);
				out[1]=this.getPropertyValue(serviceOffering, propertyDescriptionOffering);
				out[2]= this.getPropertyValue(slp, propertySLPtitle);
				out[3]=this.getPropertyValue(slp, propertySLPdescription);
				out[4]=this.getPropertyValue(slp, propertySLPformat);
			}
			else{
				_log.warn("ServiceOffering not found!");
				out[0] = "";
				out[1] = "";
				out[2] = "";
				out[3] = "";
				out[4] = "";
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
				
		return out;
	}
	
	public String getTitleProfile()
	{
		
		String out="";
		try
		{
			StmtIterator it = getResourcesOfType(USDL_SECURITY + "SecurityProfile");
			
			if(it.hasNext())
			{				
				Resource securityProfile = it.next().getSubject();			
				Property propertyTitleProfile=model.createProperty("http://purl.org/dc/terms/title");

				out=getPropertyValue(securityProfile, propertyTitleProfile);
			}
			else
			{
				_log.warn("SecurityProfile not found!");
				out= "";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public String[] getServiceProvider()
	{
		String out[] = new String[3] ;
		try
		{
			StmtIterator it = getResourcesOfType(USDL + "Provider");
			
			if(it.hasNext())
			{
				Resource serviceProvider = it.next().getSubject();
				
				Property propertyTitle = model.createProperty(DCTERMS + "title");
				Property propertyDescription = model.createProperty(FOAF + "homepage");

				out[0] = getPropertyValue(serviceProvider, propertyTitle);
				out[1] = getPropertyValue(serviceProvider, propertyDescription);
			}
			else
			{
				_log.warn("Provider not found");
				out[0] = "";
				out[1] = "";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public String[] getOwnerInfo()
	{
		String out[] = new String[3] ;
		try
		{
			StmtIterator it = getResourcesOfType(USDL + "Owner");
			
			if(it.hasNext())
			{
				Resource serviceProvider = it.next().getSubject();
				
				Property propertyTitle = model.createProperty(DCTERMS + "title");
				Property propertyDescription = model.createProperty(FOAF + "homepage");

				out[0] = getPropertyValue(serviceProvider, propertyTitle);
				out[1] = getPropertyValue(serviceProvider, propertyDescription);
			}
			else
			{
				_log.warn("Owner not found");
				out[0] = "";
				out[1] = "";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
	
	public ArrayList<HashMap<Integer,Object>> getArtifacts()
	{
		//_log.debug("GET ARTIFACTS");
					
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		String location = "";
		
		StmtIterator it = getResourcesOfType(USDL + "Dependency");
		
		if(!it.hasNext()){
			_log.warn("Artifact not found");
			return null;
		}
		
		Resource artifact;
		
		Property propertyTitle = model.createProperty(DCTERMS + "title");
		Property propertyDescription = model.createProperty(DCTERMS + "description");
		Property propertyLocation = model.createProperty(DCTERMS + "location");
		String uri_id = "";
		int uri_length = (USDL + "WeLiveArtefact").length();
				
		int i = 0;
		
		while(it.hasNext())
		{
			artifact = it.next().getSubject();
			uri_id = "|" + artifact.getURI().substring(uri_length);
			
			title = getPropertyValue(artifact, propertyTitle);
			description = "|" + getPropertyValue(artifact, propertyDescription);
			location = "|" + getPropertyValue(artifact, propertyLocation);
			
			e.put(i, title + description + location + uri_id);
			
			
			i++;
		}
		out.add(e);
		return out;
	}
		
	public ArrayList<HashMap<Integer,Object>> getTerms(){
					
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		String url = "";
		String customOrStandard = "";
		
		StmtIterator it = getResourcesOfType(USDL + "LegalCondition");
		
		if(!it.hasNext()){
			_log.warn("Terms not found!");
			return null;
		}
		
		Resource terms;
		
		Property propertyTitle = model.createProperty(DCTERMS + "title");
		Property propertyDescription = model.createProperty(DCTERMS + "description");
		Property propertyStandardURL = model.createProperty(SCHEMA + "url");
		Property propertyCustomPage = model.createProperty(FOAF + "page");
		
		int i = 0;
				
		while(it.hasNext()){
			terms = it.next().getSubject();
			
			title = getPropertyValue(terms, propertyTitle);
			description = getPropertyValue(terms, propertyDescription).replaceAll("\\\n", " ");
			url = getPropertyValue(terms,propertyStandardURL);
			customOrStandard = "standard";
			
			if(url==null || url.equals("null") || url.equals("")){
				url = getPropertyValue(terms,propertyCustomPage);
				customOrStandard = "custom";
			}
			
			e.put(i, title + "|" + description + "|" + url + "|" + customOrStandard);
			
			
			i++;
		}
		out.add(e);
		return out;
	}
			
	public HashMap<Integer,Object> getSecurityMeasures(){
		
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		String title = "";
		String description = "";
		String type ="";
		
		StmtIterator it = getResourcesOfType(WELIVE_SECURITY + "SecurityMeasure");
		
		if(!it.hasNext()){
			_log.warn("SecurityMeasure not found");
			return null;
		}
		
		Resource security;
		int i = 0;
		
		while(it.hasNext()){
			
			security = it.next().getSubject();

			String s = "";
			
			try { 
				title = URLEncoder.encode(getPropertyValue(security, propertyTitle), "UTF-8");
				s += title;
			} 
			catch (Exception e1) { title = "Default Value";  _log.error("Error getting Security Measures Data");}
			
			try{ 
				description = URLEncoder.encode(getPropertyValue(security, propertyDescription), "UTF-8");
				s += "_"+description;
			}
			catch (Exception e1) { description = "Default Value";  _log.error("Error getting Security Measures Data");}
			
			String uri = security.getURI();
			
			if(uri.contains("SecurityProtocol")){
				try {
					type = URLEncoder.encode("Security Protocol", "UTF-8");
					s += "_"+type;
				} 
				catch (Exception e1) { type = "Default Value";  _log.error("Error getting Security Measures Data");}
				
				String protocolResourceString = getPropertyValue(security, model.createProperty(WELIVE_SECURITY + "protocol"));
				Resource protocolResource = model.getResource(protocolResourceString);
				String protocol;
				try { protocol = URLEncoder.encode(getPropertyValue(protocolResource, model.createProperty(RDFS + "label")),"UTF-8");
				} 
				catch (Exception e1) { protocol = "Default Value";  _log.error("Error getting Security Measures Data");}
				s += "_"+protocol;
				
			}
			else if(uri.contains("BasicAuthentication")){
				try {
					type = URLEncoder.encode("Authentication Protocol", "UTF-8");
					s += "_"+type;
				} 
				catch (Exception e1) { type = "Default Value";  _log.error("Error getting Security Measures Data");}
				
				s += "_Basic Authentication";
				
				
			}
			else if(uri.contains("OAUTH2")){
				String scope ="";
				String url = "";
				String response = "";
				String grant ="";
				
				try {
					type = URLEncoder.encode("Authentication Protocol", "UTF-8");
					s += "_"+type;
				} 
				catch (Exception e1) { type = "Default Value";  _log.error("Error getting Security Measures Data");}
				
				s += "_OAUTH 2.0";
				
				try{ 
					scope = URLEncoder.encode(getPropertyValue(security, model.createProperty(WELIVE_SECURITY + "scope")), "UTF-8");
					s += "_"+scope;
				}
				catch (Exception e1) { scope = "Default Value";  _log.error("Error getting Security Measures Data");}
				
				try{ 
					url = URLEncoder.encode(getPropertyValue(security, model.createProperty(SCHEMA + "url")), "UTF-8");
					s += "_"+url;
				}
				catch (Exception e1) { url = "Default Value";  _log.error("Error getting Security Measures Data");}
				
				String responseResourceString = getPropertyValue(security, model.createProperty(WELIVE_SECURITY + "responseType"));
				Resource responseResource = model.getResource(responseResourceString);
				response = getPropertyValue(responseResource, model.createProperty(RDFS + "label"));
				s += "_"+response;
				
				String grantResourceString = getPropertyValue(security, model.createProperty(WELIVE_SECURITY + "grantType"));
				Resource grantResource = model.getResource(grantResourceString);
				grant = getPropertyValue(grantResource, model.createProperty(RDFS + "label"));
				s += "_"+grant;
				
			}
			
			e.put(i, s);
			
			i++;			
		}
		
		return e;
	}
			
	public ArrayList<HashMap<Integer,Object[]>> getServiceLevel(){

		ArrayList<HashMap<Integer,Object[]>> out = new  ArrayList<HashMap<Integer,Object[]>>();
		HashMap<Integer,Object[]> e = new HashMap<Integer,Object[]>();
		
		StmtIterator it = getResourcesOfType(USDL + "Offering");
		
		if(!it.hasNext()){
			_log.warn("Offering not found!");
			return null;
		}
		
		int i = 0;
		
		while(it.hasNext()){		
			e.put(i, getServiceOffering(it.next().getSubject().toString()));			
			i++;
		}
		out.add(e); 
		
		return out;
	}
	
	public ArrayList<HashMap<Integer, Object>> getTechnicalConstraints(long interactionPointID){
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		StmtIterator it = getResourcesOfType(USDL + "TechnicalConstraint");
		if(!it.hasNext()){
			_log.warn("Technical Constraints not found for Interaction Point "+interactionPointID);
			return null;
		}
		
		int tech_count = 0;
		
		while(it.hasNext()){
			Resource vincolo = it.next().getSubject();
			
			//ids[0] � l'id interactionPoint id[1] � l'id del vincolo
			String[] ids = vincolo.getURI().split("#")[1].replace("TechnicalConstraint", "").split("_");
			
			//Se il vincolo � riferito all'interaction point corrente
			if(ids[0].equals(String.valueOf(interactionPointID))){
				String title ="";
				try { title = URLEncoder.encode(getPropertyValue(vincolo, propertyTitle),"UTF-8"); } 
				catch (Exception e1) { title = "Default Value";  _log.error("Error getting Technical Constraint Data");}
				
				String description ="";
				try { description = URLEncoder.encode(getPropertyValue(vincolo, propertyDescription),"UTF-8"); } 
				catch (Exception e1) { title = "Default Value";  _log.error("Error getting Technical Constraint Data");}
				
				String language ="";
				try { language = URLEncoder.encode(getPropertyValue(vincolo, model.createProperty(DCTERMS + "language")),"UTF-8"); } 
				catch (Exception e1) { title = "Default Value";  _log.error("Error getting Technical Constraint Data");}
				
				String body ="";
				try { body = URLEncoder.encode(getPropertyValue(vincolo, model.createProperty(SWRL + "body")),"UTF-8"); } 
				catch (Exception e1) { title = "Default Value";  _log.error("Error getting Technical Constraint Data");}
				
				String resourceString = getPropertyValue(vincolo, model.createProperty(DCTERMS + "type"));
				Resource RuleTypeResource = model.getResource(resourceString);
				String type = getPropertyValue(RuleTypeResource, model.createProperty(RDFS + "label"));
				
				e.put(tech_count, title+"_"+description+"_"+body+"_"+language+"_"+type+"_"+ids[1]);
			}
			
			tech_count++;
		}
		
		out.add(e);
		
		return out;
	}

	public ArrayList<HashMap<Integer,Object>> getInteractions(){
		
		ArrayList<HashMap<Integer,Object>>  out = new  ArrayList<HashMap<Integer,Object>>();
		HashMap<Integer,Object> e = new HashMap<Integer,Object>();
		
		StmtIterator it = getResourcesOfType(USDL + "InteractionPoint");
		
		if(!it.hasNext()){
			_log.warn("InteractionPoint not found");
			return null;
		}
		
		String out_string = "";
		String title = "";
		String description = "";
		String interfaceOperation = "";
		String yields = "";
		String yields_isarray = "";
		
		String isCollection = "";
		String label = "";
		String type = "";
		
		Resource interactionPoint;
		Resource parameter;
		Resource yield;
		
		StmtIterator it_parameter;
		
		Property propertyInterfaceOperation = model.createProperty(SCHEMA + "url");
		Property propertyIsCollection = model.createProperty(USDL + "isCollection");
		Property propertyParameterType = model.createProperty(XSD + "type");	
		
		int i = 0;
		String id = "";
		String URI = "";
		String tmp = "";
		
		int uri_length = (USDL + "InteractionPoint").length();
		
		int par_count;
		
		while(it.hasNext()){
			
			par_count = 0;
			
			interactionPoint = it.next().getSubject();
			URI = interactionPoint.getURI();
			id = URI.substring(uri_length);
			
			title = getPropertyValue(interactionPoint, propertyTitle);
			description = "|" + getPropertyValue(interactionPoint, propertyDescription);
			interfaceOperation = "|" + getPropertyValue(interactionPoint, propertyInterfaceOperation);
			
			yield = model.getResource(USDL + "Output_" + id);
			
			try{
				yields = getPropertyValue(yield, propertyParameterType).split("\\^\\^")[0];
			}
			catch(Exception ex){
				_log.warn(ex.getMessage());
				yields = "string";
			}
			
			if(yields.compareTo("null") == 0){
				_log.warn("No output founded");
				yields = "string";
			}
			
			try{
				yields_isarray = getPropertyValue(yield, propertyIsCollection).split("\\^\\^")[0];
			}
			catch(Exception ex){
				_log.warn(ex.getMessage());
				yields_isarray = "false";
			}
			
			if(yields_isarray.compareTo("null") == 0){
				_log.warn("Unable to check if the output is a collection or not");
				yields_isarray = "false";
			}
			
			yields = "|" + yields + "__" + yields_isarray;
			
			out_string = title + description + interfaceOperation + yields;
			
			it_parameter = getResourcesOfType(USDL + "Parameter");
			
			while(it_parameter.hasNext()){
				
				parameter = it_parameter.next().getSubject();
				URI = parameter.getURI();
				tmp = URI.split("_")[1];
				
				if(tmp.compareTo(id) == 0)
				{
					if(par_count == 0)
						type = "|" + getPropertyValue(parameter, propertyParameterType).split("\\^\\^")[0];
					else
						type = "__" + getPropertyValue(parameter, propertyParameterType).split("\\^\\^")[0];
					
					isCollection = "__" + getPropertyValue(parameter, propertyIsCollection).split("\\^\\^")[0];
					label = "__" + getPropertyValue(parameter, propertyTitle);
					
					out_string += type += label += isCollection;
					
					par_count++;
				}
			}
			
			if(par_count == 0) out_string += "|null__null__null";
			
			e.put(i, out_string);
			
			i++;
		}
		out.add(e);
		//_log.debug("GET INTERACTIONPOINTS END");
		return out;
	}
	
	public String getClassificationType()
	{ 
		String out="";
		try{
			/*
			Property propertyClassification=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#hasClassification");

			out=getPropertyValue(generalDescriptionService, propertyClassification);
			
			*/
			/** Property propertyClassification=model.createProperty(RDFS + "label");
			Resource classification = model.getResource("http://www.w3.org/2004/02/skos/core#Classification");
			out = getPropertyValue(classification, propertyClassification);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return out;
	}
		
	public String getMarketplaceType()
	{ 
		String out = "";
		try
		{
			
			Property propertyMarketplace=model.createProperty(RDFS + "label");
			Resource classification = model.getResource("http://www.w3.org/2004/02/skos/core#Type");
			out = getPropertyValue(classification, propertyMarketplace);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return out;
	}

//	public String getDescriptionService() {
//		String out="";
//		try{ 
//			out=getPropertyValue(generalDescriptionService, propertyDescription);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}

//	public String getAboutTitle() {
//		String out="";
//		try{
//			StmtIterator risultato = getSubjects(model,propertyType, "http://www.linked-usdl.org/ns/usdl-core#ServiceDescription");
//			Resource serviceDescription=risultato.next().getSubject(); 
//
//			out=getPropertyValue(serviceDescription, propertyTitle);
//
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
	
	
//	public ArrayList<HashMap<String,Object>> getTermsAndConditions(){
//		ArrayList<HashMap<String,Object>>  out=new  ArrayList<HashMap<String,Object>>();		 
//		try{ 			  
//			StmtIterator risultato = getSubjects(model, propertyType, "http://www.linked-usdl.org/ns/usdl-legal#TermsAndConditions");
//			while(risultato.hasNext()){
//				out.add(getTermsAndConditionsMap(risultato.next().getSubject()));
//			} 
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		return out;
//	}


//	private HashMap<String,Object> getTermsAndConditionsMap(Resource subject) {
//		HashMap<String,Object> out=new HashMap<String,Object> ();
//		String valore=getPropertyValue(subject, propertyTitle);
//		out.put("title", valore);
//		out.put("description",getPropertyValue(subject, propertyDescription));
//		StmtIterator iteratore = subject.listProperties(model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#hasClause"));
// 
//		ArrayList <String> listaClause=new ArrayList<String>();
//		
//		while (iteratore.hasNext()){
//			RDFNode obj = iteratore.next().getObject();
// 
//			//coppie(name,text)
//			listaClause.add(getPropertyValue(obj.asResource(), model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#name")));
//			listaClause.add(getPropertyValue(obj.asResource(), model.createProperty("http://www.linked-usdl.org/ns/usdl-legal#text")));
//		}
//		out.put("clause", listaClause);
//		return out;
//	}
//
//
//
//	public ArrayList<HashMap> getOffering() {
//		ArrayList<HashMap> out=new ArrayList<HashMap>();
//		try{ 
//			StmtIterator risultato = getSubjects(model,propertyType, "http://www.linked-usdl.org/ns/usdl-core#ServiceOffering");
//			while(risultato.hasNext()){
//				out.add(getOfferingMap(risultato.next().getSubject()));
//			} 
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
//
//	private HashMap<String, Object> getOfferingMap(Resource offering){
//		HashMap<String, Object> out=new HashMap<String, Object>();
//		if (offering!=null){
//			Property property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#validFrom");
//
//			String valid="da: "+getPropertyValue(offering, property);
//			if (valid.contains("^^")){
//				valid=valid.substring(0,  valid.indexOf("^^"));
//			}
//			property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#validThrough");
//			valid=valid+" a: "+getPropertyValue(offering, property);
//			if (valid.contains("^^")){
//				valid=valid.substring(0,  valid.indexOf("^^"));
//			}
//			out.put("valid", valid);
//			out.put("description", getPropertyValue(offering, propertyDescription));
//			out.put("title",getPropertyValue(offering, propertyTitle).replace("@en", ""));
//
//			out.put("pricePlans",getPricePlans(offering));
//			out.put("slaProfiles",getSLAProfiles(offering));
//		}
//
//		return out;
//	}

//	private ArrayList <String>  getPricePlans(Resource offering) {
//		ArrayList <String>  out=new ArrayList <String> ();
//		Property	property=model.createProperty("http://www.linked-usdl.org/ns/usdl-core#hasPricePlan");
//		StmtIterator iterator = offering.listProperties(property);
//
//		return getListaTitoloDescrizione(iterator);
//	}
//
//	private ArrayList <String>  getListaTitoloDescrizione(StmtIterator iterator) {
//		ArrayList <String>  out=new ArrayList <String> ();
//		String titoloDescrizione="";
//		RDFNode nodoObj = null;
//		Property property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#serviceLevelExpression");
//
//		while (iterator.hasNext()){
//			nodoObj = iterator.next().getObject();
//			if (nodoObj instanceof Resource) {
//
//				titoloDescrizione=getPropertyValue((Resource)nodoObj, propertyTitle).replace("@en", "");	
//
//				if (((Resource)nodoObj).hasProperty(propertyDescription)){
//					titoloDescrizione=titoloDescrizione+": "+getPropertyValue((Resource)nodoObj, propertyDescription).replace("@en", "");
//				}
//				else if (((Resource)nodoObj).hasProperty(property)){
//
//					titoloDescrizione=titoloDescrizione+": "+	
//							getPropertyValue(
//									(Resource) (((Resource) nodoObj).listProperties(property).next().getObject()),propertyDescription).replace("@en", "");					
//				}else{
//					titoloDescrizione=titoloDescrizione+": no description";
//				}
//
//				out.add(titoloDescrizione);							  
//
//			} else {
//				// object is a literal
//				out.add( nodoObj.toString() );
//			}
//		}
//		return out;
//	}
//
//	private HashMap <String,List<String>> getSLAProfiles(Resource offering) {
//		HashMap<String,List<String>> out=new HashMap<String,List<String>>();
//		Property	property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#hasServiceLevelProfile");
//		StmtIterator iterator = offering.listProperties(property);
//
//		String profilo="";
//
//		RDFNode nodoObj = null;
//		while (iterator.hasNext()){
//			nodoObj = iterator.next().getObject();
//			if (nodoObj instanceof Resource) {
//
//				profilo=getPropertyValue((Resource)nodoObj, propertyTitle).replace("@en", "");
//
//				property=model.createProperty("http://www.linked-usdl.org/ns/usdl-sla#hasServiceLevel");
//				iterator=((Resource)nodoObj).listProperties(property);
//
//				out.put(profilo,getListaTitoloDescrizione(iterator));
//
//			} else {
//				// object is a literal
//				System.out.print(" \"" + nodoObj.toString() + "\"");
//			}
//		}
//		return out;
//	}

//	public String getTitle() {
//		String out="";
//		try{  
//			out=getPropertyValue(generalDescriptionService, propertyTitle);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}

//	public String  getProviderName() {
//		String out="";
//		try{
//			Resource businessEntityResource=getSubjects(model,propertyType, "http://purl.org/goodrelations/v1#BusinessEntity").next().getSubject();
//			Property propertyProviderName=model.createProperty("http://purl.org/goodrelations/v1#legalName"); 
//
//			out=getPropertyValue(businessEntityResource, propertyProviderName);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return out;
//	}
}*/