create index IX_1FA82D99 on Artefatto_Artefatto (categoriamkpId);
create index IX_244FC37F on Artefatto_Artefatto (categoriamkpId, status);
create index IX_35B9BC9D on Artefatto_Artefatto (companyId);
create index IX_850E3EC5 on Artefatto_Artefatto (eId);
create index IX_58D2C65F on Artefatto_Artefatto (groupId);
create index IX_6BCE1D37 on Artefatto_Artefatto (language);
create index IX_DA9FE398 on Artefatto_Artefatto (owner);
create index IX_3FA73DF8 on Artefatto_Artefatto (pilotid);
create index IX_46B22BCC on Artefatto_Artefatto (pilotid, categoriamkpId, status);
create index IX_9A63D718 on Artefatto_Artefatto (pilotid, status, categoriamkpId, language);
create index IX_6E333FAA on Artefatto_Artefatto (pilotid, status, language);
create index IX_FA79DE7B on Artefatto_Artefatto (providerName);
create index IX_BBF77B6B on Artefatto_Artefatto (resourceRDF);
create index IX_64D56291 on Artefatto_Artefatto (status);
create index IX_D919AB0B on Artefatto_Artefatto (status, categoriamkpId, language);
create index IX_58FD599D on Artefatto_Artefatto (status, language);
create index IX_47270F2 on Artefatto_Artefatto (status, owner);
create index IX_3F0BDCD2 on Artefatto_Artefatto (status, pilotid);
create index IX_CB92BDFD on Artefatto_Artefatto (title);
create index IX_6547BE5 on Artefatto_Artefatto (userId);
create index IX_7D1F05CB on Artefatto_Artefatto (userId, status);
create index IX_145464A9 on Artefatto_Artefatto (uuid_);
create index IX_5854BDF on Artefatto_Artefatto (uuid_, companyId);
create unique index IX_215D3E21 on Artefatto_Artefatto (uuid_, groupId);

create index IX_3E74E3BF on Artefatto_ArtifactLevels (artifactId, level);
create index IX_12E796A0 on Artefatto_ArtifactLevels (level);
create index IX_9955F20 on Artefatto_ArtifactLevels (uuid_);

create index IX_12AF6528 on Artefatto_ArtifactOrganizations (artifactId);
create index IX_EA353789 on Artefatto_ArtifactOrganizations (organizationId);
create index IX_79EBDD6F on Artefatto_ArtifactOrganizations (organizationId, status);

create index IX_F1CF6A9C on Artefatto_Categoria (idCategoria);
create index IX_5EDD415E on Artefatto_Categoria (nomeCategoria);
create index IX_3C69BEE4 on Artefatto_Categoria (supports);

create index IX_B2369C59 on Artefatto_Dependencies (artefactId);
create unique index IX_41B43408 on Artefatto_Dependencies (artefactId, dependsFrom);
create index IX_848158A5 on Artefatto_Dependencies (dependsFrom);

create index IX_124E735D on Artefatto_ImmagineArt (artefattoIdent);
create index IX_DC7310F3 on Artefatto_ImmagineArt (dlImageId);
create index IX_264F8D5B on Artefatto_ImmagineArt (imageId);

create unique index IX_29B922DC on Artefatto_MarketplaceConf (key_);