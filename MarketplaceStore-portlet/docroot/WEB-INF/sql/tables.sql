create table Artefatto_Artefatto (
	uuid_ VARCHAR(75) null,
	artefattoId LONG not null primary key,
	date_ DATE null,
	title VARCHAR(400) null,
	webpage VARCHAR(400) null,
	abstractDescription TEXT null,
	description TEXT null,
	providerName VARCHAR(400) null,
	resourceRDF TEXT null,
	repositoryRDF TEXT null,
	status INTEGER,
	companyId LONG,
	groupId LONG,
	userId LONG,
	owner VARCHAR(75) null,
	imgId LONG,
	categoriamkpId LONG,
	pilotid VARCHAR(400) null,
	url TEXT null,
	eId TEXT null,
	extRating INTEGER,
	interactionPoint TEXT null,
	language VARCHAR(75) null,
	hasMapping BOOLEAN,
	isPrivate BOOLEAN,
	lusdlmodel TEXT null
);

create table Artefatto_ArtifactLevels (
	artifactId LONG not null primary key,
	level INTEGER,
	notes TEXT null
);

create table Artefatto_ArtifactOrganizations (
	organizationId LONG not null,
	artifactId LONG not null,
	status INTEGER,
	primary key (organizationId, artifactId)
);

create table Artefatto_Categoria (
	idCategoria LONG not null primary key,
	nomeCategoria VARCHAR(75) null,
	supports VARCHAR(75) null
);

create table Artefatto_Dependencies (
	dependencyId LONG not null primary key,
	artefactId LONG,
	dependsFrom LONG
);

create table Artefatto_ImmagineArt (
	imageId LONG not null primary key,
	dlImageId LONG,
	descrizione VARCHAR(75) null,
	artefattoIdent LONG
);

create table Artefatto_MarketplaceConf (
	confId LONG not null primary key,
	key_ VARCHAR(75) null,
	value VARCHAR(75) null,
	type_ VARCHAR(75) null,
	options VARCHAR(75) null
);

create table Artefatto_PendingNotificationEntry (
	notificationId LONG not null primary key,
	timestamp LONG,
	artefactId LONG,
	action VARCHAR(75) null,
	targetComponent VARCHAR(75) null,
	options TEXT null
);