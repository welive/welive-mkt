/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class ArtefattoSoap implements Serializable {
	public static ArtefattoSoap toSoapModel(Artefatto model) {
		ArtefattoSoap soapModel = new ArtefattoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setArtefattoId(model.getArtefattoId());
		soapModel.setDate(model.getDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setWebpage(model.getWebpage());
		soapModel.setAbstractDescription(model.getAbstractDescription());
		soapModel.setDescription(model.getDescription());
		soapModel.setProviderName(model.getProviderName());
		soapModel.setResourceRDF(model.getResourceRDF());
		soapModel.setRepositoryRDF(model.getRepositoryRDF());
		soapModel.setStatus(model.getStatus());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setOwner(model.getOwner());
		soapModel.setImgId(model.getImgId());
		soapModel.setCategoriamkpId(model.getCategoriamkpId());
		soapModel.setPilotid(model.getPilotid());
		soapModel.setUrl(model.getUrl());
		soapModel.setEId(model.getEId());
		soapModel.setExtRating(model.getExtRating());
		soapModel.setInteractionPoint(model.getInteractionPoint());
		soapModel.setLanguage(model.getLanguage());
		soapModel.setHasMapping(model.getHasMapping());
		soapModel.setIsPrivate(model.getIsPrivate());
		soapModel.setLusdlmodel(model.getLusdlmodel());

		return soapModel;
	}

	public static ArtefattoSoap[] toSoapModels(Artefatto[] models) {
		ArtefattoSoap[] soapModels = new ArtefattoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ArtefattoSoap[][] toSoapModels(Artefatto[][] models) {
		ArtefattoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ArtefattoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ArtefattoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ArtefattoSoap[] toSoapModels(List<Artefatto> models) {
		List<ArtefattoSoap> soapModels = new ArrayList<ArtefattoSoap>(models.size());

		for (Artefatto model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ArtefattoSoap[soapModels.size()]);
	}

	public ArtefattoSoap() {
	}

	public long getPrimaryKey() {
		return _artefattoId;
	}

	public void setPrimaryKey(long pk) {
		setArtefattoId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getArtefattoId() {
		return _artefattoId;
	}

	public void setArtefattoId(long artefattoId) {
		_artefattoId = artefattoId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getWebpage() {
		return _webpage;
	}

	public void setWebpage(String webpage) {
		_webpage = webpage;
	}

	public String getAbstractDescription() {
		return _abstractDescription;
	}

	public void setAbstractDescription(String abstractDescription) {
		_abstractDescription = abstractDescription;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getProviderName() {
		return _providerName;
	}

	public void setProviderName(String providerName) {
		_providerName = providerName;
	}

	public String getResourceRDF() {
		return _resourceRDF;
	}

	public void setResourceRDF(String resourceRDF) {
		_resourceRDF = resourceRDF;
	}

	public String getRepositoryRDF() {
		return _repositoryRDF;
	}

	public void setRepositoryRDF(String repositoryRDF) {
		_repositoryRDF = repositoryRDF;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getOwner() {
		return _owner;
	}

	public void setOwner(String owner) {
		_owner = owner;
	}

	public long getImgId() {
		return _imgId;
	}

	public void setImgId(long imgId) {
		_imgId = imgId;
	}

	public long getCategoriamkpId() {
		return _categoriamkpId;
	}

	public void setCategoriamkpId(long categoriamkpId) {
		_categoriamkpId = categoriamkpId;
	}

	public String getPilotid() {
		return _pilotid;
	}

	public void setPilotid(String pilotid) {
		_pilotid = pilotid;
	}

	public String getUrl() {
		return _url;
	}

	public void setUrl(String url) {
		_url = url;
	}

	public String getEId() {
		return _eId;
	}

	public void setEId(String eId) {
		_eId = eId;
	}

	public int getExtRating() {
		return _extRating;
	}

	public void setExtRating(int extRating) {
		_extRating = extRating;
	}

	public String getInteractionPoint() {
		return _interactionPoint;
	}

	public void setInteractionPoint(String interactionPoint) {
		_interactionPoint = interactionPoint;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	public boolean getHasMapping() {
		return _hasMapping;
	}

	public boolean isHasMapping() {
		return _hasMapping;
	}

	public void setHasMapping(boolean hasMapping) {
		_hasMapping = hasMapping;
	}

	public boolean getIsPrivate() {
		return _isPrivate;
	}

	public boolean isIsPrivate() {
		return _isPrivate;
	}

	public void setIsPrivate(boolean isPrivate) {
		_isPrivate = isPrivate;
	}

	public String getLusdlmodel() {
		return _lusdlmodel;
	}

	public void setLusdlmodel(String lusdlmodel) {
		_lusdlmodel = lusdlmodel;
	}

	private String _uuid;
	private long _artefattoId;
	private Date _date;
	private String _title;
	private String _webpage;
	private String _abstractDescription;
	private String _description;
	private String _providerName;
	private String _resourceRDF;
	private String _repositoryRDF;
	private int _status;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _owner;
	private long _imgId;
	private long _categoriamkpId;
	private String _pilotid;
	private String _url;
	private String _eId;
	private int _extRating;
	private String _interactionPoint;
	private String _language;
	private boolean _hasMapping;
	private boolean _isPrivate;
	private String _lusdlmodel;
}