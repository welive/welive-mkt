/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PendingNotificationEntry}.
 * </p>
 *
 * @author eng
 * @see PendingNotificationEntry
 * @generated
 */
public class PendingNotificationEntryWrapper implements PendingNotificationEntry,
	ModelWrapper<PendingNotificationEntry> {
	public PendingNotificationEntryWrapper(
		PendingNotificationEntry pendingNotificationEntry) {
		_pendingNotificationEntry = pendingNotificationEntry;
	}

	@Override
	public Class<?> getModelClass() {
		return PendingNotificationEntry.class;
	}

	@Override
	public String getModelClassName() {
		return PendingNotificationEntry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("notificationId", getNotificationId());
		attributes.put("timestamp", getTimestamp());
		attributes.put("artefactId", getArtefactId());
		attributes.put("action", getAction());
		attributes.put("targetComponent", getTargetComponent());
		attributes.put("options", getOptions());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long notificationId = (Long)attributes.get("notificationId");

		if (notificationId != null) {
			setNotificationId(notificationId);
		}

		Long timestamp = (Long)attributes.get("timestamp");

		if (timestamp != null) {
			setTimestamp(timestamp);
		}

		Long artefactId = (Long)attributes.get("artefactId");

		if (artefactId != null) {
			setArtefactId(artefactId);
		}

		String action = (String)attributes.get("action");

		if (action != null) {
			setAction(action);
		}

		String targetComponent = (String)attributes.get("targetComponent");

		if (targetComponent != null) {
			setTargetComponent(targetComponent);
		}

		String options = (String)attributes.get("options");

		if (options != null) {
			setOptions(options);
		}
	}

	/**
	* Returns the primary key of this pending notification entry.
	*
	* @return the primary key of this pending notification entry
	*/
	@Override
	public long getPrimaryKey() {
		return _pendingNotificationEntry.getPrimaryKey();
	}

	/**
	* Sets the primary key of this pending notification entry.
	*
	* @param primaryKey the primary key of this pending notification entry
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_pendingNotificationEntry.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the notification ID of this pending notification entry.
	*
	* @return the notification ID of this pending notification entry
	*/
	@Override
	public long getNotificationId() {
		return _pendingNotificationEntry.getNotificationId();
	}

	/**
	* Sets the notification ID of this pending notification entry.
	*
	* @param notificationId the notification ID of this pending notification entry
	*/
	@Override
	public void setNotificationId(long notificationId) {
		_pendingNotificationEntry.setNotificationId(notificationId);
	}

	/**
	* Returns the timestamp of this pending notification entry.
	*
	* @return the timestamp of this pending notification entry
	*/
	@Override
	public long getTimestamp() {
		return _pendingNotificationEntry.getTimestamp();
	}

	/**
	* Sets the timestamp of this pending notification entry.
	*
	* @param timestamp the timestamp of this pending notification entry
	*/
	@Override
	public void setTimestamp(long timestamp) {
		_pendingNotificationEntry.setTimestamp(timestamp);
	}

	/**
	* Returns the artefact ID of this pending notification entry.
	*
	* @return the artefact ID of this pending notification entry
	*/
	@Override
	public long getArtefactId() {
		return _pendingNotificationEntry.getArtefactId();
	}

	/**
	* Sets the artefact ID of this pending notification entry.
	*
	* @param artefactId the artefact ID of this pending notification entry
	*/
	@Override
	public void setArtefactId(long artefactId) {
		_pendingNotificationEntry.setArtefactId(artefactId);
	}

	/**
	* Returns the action of this pending notification entry.
	*
	* @return the action of this pending notification entry
	*/
	@Override
	public java.lang.String getAction() {
		return _pendingNotificationEntry.getAction();
	}

	/**
	* Sets the action of this pending notification entry.
	*
	* @param action the action of this pending notification entry
	*/
	@Override
	public void setAction(java.lang.String action) {
		_pendingNotificationEntry.setAction(action);
	}

	/**
	* Returns the target component of this pending notification entry.
	*
	* @return the target component of this pending notification entry
	*/
	@Override
	public java.lang.String getTargetComponent() {
		return _pendingNotificationEntry.getTargetComponent();
	}

	/**
	* Sets the target component of this pending notification entry.
	*
	* @param targetComponent the target component of this pending notification entry
	*/
	@Override
	public void setTargetComponent(java.lang.String targetComponent) {
		_pendingNotificationEntry.setTargetComponent(targetComponent);
	}

	/**
	* Returns the options of this pending notification entry.
	*
	* @return the options of this pending notification entry
	*/
	@Override
	public java.lang.String getOptions() {
		return _pendingNotificationEntry.getOptions();
	}

	/**
	* Sets the options of this pending notification entry.
	*
	* @param options the options of this pending notification entry
	*/
	@Override
	public void setOptions(java.lang.String options) {
		_pendingNotificationEntry.setOptions(options);
	}

	@Override
	public boolean isNew() {
		return _pendingNotificationEntry.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_pendingNotificationEntry.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _pendingNotificationEntry.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pendingNotificationEntry.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _pendingNotificationEntry.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _pendingNotificationEntry.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_pendingNotificationEntry.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _pendingNotificationEntry.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_pendingNotificationEntry.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_pendingNotificationEntry.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_pendingNotificationEntry.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PendingNotificationEntryWrapper((PendingNotificationEntry)_pendingNotificationEntry.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry) {
		return _pendingNotificationEntry.compareTo(pendingNotificationEntry);
	}

	@Override
	public int hashCode() {
		return _pendingNotificationEntry.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> toCacheModel() {
		return _pendingNotificationEntry.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry toEscapedModel() {
		return new PendingNotificationEntryWrapper(_pendingNotificationEntry.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry toUnescapedModel() {
		return new PendingNotificationEntryWrapper(_pendingNotificationEntry.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _pendingNotificationEntry.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _pendingNotificationEntry.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_pendingNotificationEntry.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PendingNotificationEntryWrapper)) {
			return false;
		}

		PendingNotificationEntryWrapper pendingNotificationEntryWrapper = (PendingNotificationEntryWrapper)obj;

		if (Validator.equals(_pendingNotificationEntry,
					pendingNotificationEntryWrapper._pendingNotificationEntry)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PendingNotificationEntry getWrappedPendingNotificationEntry() {
		return _pendingNotificationEntry;
	}

	@Override
	public PendingNotificationEntry getWrappedModel() {
		return _pendingNotificationEntry;
	}

	@Override
	public void resetOriginalValues() {
		_pendingNotificationEntry.resetOriginalValues();
	}

	private PendingNotificationEntry _pendingNotificationEntry;
}