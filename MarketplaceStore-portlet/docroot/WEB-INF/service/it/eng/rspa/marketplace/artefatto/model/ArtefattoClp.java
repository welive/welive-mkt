/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class ArtefattoClp extends BaseModelImpl<Artefatto> implements Artefatto {
	public ArtefattoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Artefatto.class;
	}

	@Override
	public String getModelClassName() {
		return Artefatto.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _artefattoId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setArtefattoId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _artefattoId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("artefattoId", getArtefattoId());
		attributes.put("date", getDate());
		attributes.put("title", getTitle());
		attributes.put("webpage", getWebpage());
		attributes.put("abstractDescription", getAbstractDescription());
		attributes.put("description", getDescription());
		attributes.put("providerName", getProviderName());
		attributes.put("resourceRDF", getResourceRDF());
		attributes.put("repositoryRDF", getRepositoryRDF());
		attributes.put("status", getStatus());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("owner", getOwner());
		attributes.put("imgId", getImgId());
		attributes.put("categoriamkpId", getCategoriamkpId());
		attributes.put("pilotid", getPilotid());
		attributes.put("url", getUrl());
		attributes.put("eId", getEId());
		attributes.put("extRating", getExtRating());
		attributes.put("interactionPoint", getInteractionPoint());
		attributes.put("language", getLanguage());
		attributes.put("hasMapping", getHasMapping());
		attributes.put("isPrivate", getIsPrivate());
		attributes.put("lusdlmodel", getLusdlmodel());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long artefattoId = (Long)attributes.get("artefattoId");

		if (artefattoId != null) {
			setArtefattoId(artefattoId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String webpage = (String)attributes.get("webpage");

		if (webpage != null) {
			setWebpage(webpage);
		}

		String abstractDescription = (String)attributes.get(
				"abstractDescription");

		if (abstractDescription != null) {
			setAbstractDescription(abstractDescription);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String providerName = (String)attributes.get("providerName");

		if (providerName != null) {
			setProviderName(providerName);
		}

		String resourceRDF = (String)attributes.get("resourceRDF");

		if (resourceRDF != null) {
			setResourceRDF(resourceRDF);
		}

		String repositoryRDF = (String)attributes.get("repositoryRDF");

		if (repositoryRDF != null) {
			setRepositoryRDF(repositoryRDF);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String owner = (String)attributes.get("owner");

		if (owner != null) {
			setOwner(owner);
		}

		Long imgId = (Long)attributes.get("imgId");

		if (imgId != null) {
			setImgId(imgId);
		}

		Long categoriamkpId = (Long)attributes.get("categoriamkpId");

		if (categoriamkpId != null) {
			setCategoriamkpId(categoriamkpId);
		}

		String pilotid = (String)attributes.get("pilotid");

		if (pilotid != null) {
			setPilotid(pilotid);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String eId = (String)attributes.get("eId");

		if (eId != null) {
			setEId(eId);
		}

		Integer extRating = (Integer)attributes.get("extRating");

		if (extRating != null) {
			setExtRating(extRating);
		}

		String interactionPoint = (String)attributes.get("interactionPoint");

		if (interactionPoint != null) {
			setInteractionPoint(interactionPoint);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		Boolean hasMapping = (Boolean)attributes.get("hasMapping");

		if (hasMapping != null) {
			setHasMapping(hasMapping);
		}

		Boolean isPrivate = (Boolean)attributes.get("isPrivate");

		if (isPrivate != null) {
			setIsPrivate(isPrivate);
		}

		String lusdlmodel = (String)attributes.get("lusdlmodel");

		if (lusdlmodel != null) {
			setLusdlmodel(lusdlmodel);
		}
	}

	@Override
	public String getUuid() {
		return _uuid;
	}

	@Override
	public void setUuid(String uuid) {
		_uuid = uuid;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setUuid", String.class);

				method.invoke(_artefattoRemoteModel, uuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtefattoId() {
		return _artefattoId;
	}

	@Override
	public void setArtefattoId(long artefattoId) {
		_artefattoId = artefattoId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setArtefattoId", long.class);

				method.invoke(_artefattoRemoteModel, artefattoId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_artefattoRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTitle() {
		return _title;
	}

	@Override
	public void setTitle(String title) {
		_title = title;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setTitle", String.class);

				method.invoke(_artefattoRemoteModel, title);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getWebpage() {
		return _webpage;
	}

	@Override
	public void setWebpage(String webpage) {
		_webpage = webpage;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setWebpage", String.class);

				method.invoke(_artefattoRemoteModel, webpage);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAbstractDescription() {
		return _abstractDescription;
	}

	@Override
	public void setAbstractDescription(String abstractDescription) {
		_abstractDescription = abstractDescription;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setAbstractDescription",
						String.class);

				method.invoke(_artefattoRemoteModel, abstractDescription);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public void setDescription(String description) {
		_description = description;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setDescription", String.class);

				method.invoke(_artefattoRemoteModel, description);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getProviderName() {
		return _providerName;
	}

	@Override
	public void setProviderName(String providerName) {
		_providerName = providerName;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setProviderName", String.class);

				method.invoke(_artefattoRemoteModel, providerName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getResourceRDF() {
		return _resourceRDF;
	}

	@Override
	public void setResourceRDF(String resourceRDF) {
		_resourceRDF = resourceRDF;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setResourceRDF", String.class);

				method.invoke(_artefattoRemoteModel, resourceRDF);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRepositoryRDF() {
		return _repositoryRDF;
	}

	@Override
	public void setRepositoryRDF(String repositoryRDF) {
		_repositoryRDF = repositoryRDF;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setRepositoryRDF", String.class);

				method.invoke(_artefattoRemoteModel, repositoryRDF);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		_status = status;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setStatus", int.class);

				method.invoke(_artefattoRemoteModel, status);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_artefattoRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_artefattoRemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_artefattoRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getOwner() {
		return _owner;
	}

	@Override
	public void setOwner(String owner) {
		_owner = owner;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setOwner", String.class);

				method.invoke(_artefattoRemoteModel, owner);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getImgId() {
		return _imgId;
	}

	@Override
	public void setImgId(long imgId) {
		_imgId = imgId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setImgId", long.class);

				method.invoke(_artefattoRemoteModel, imgId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCategoriamkpId() {
		return _categoriamkpId;
	}

	@Override
	public void setCategoriamkpId(long categoriamkpId) {
		_categoriamkpId = categoriamkpId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriamkpId", long.class);

				method.invoke(_artefattoRemoteModel, categoriamkpId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPilotid() {
		return _pilotid;
	}

	@Override
	public void setPilotid(String pilotid) {
		_pilotid = pilotid;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setPilotid", String.class);

				method.invoke(_artefattoRemoteModel, pilotid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrl() {
		return _url;
	}

	@Override
	public void setUrl(String url) {
		_url = url;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setUrl", String.class);

				method.invoke(_artefattoRemoteModel, url);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEId() {
		return _eId;
	}

	@Override
	public void setEId(String eId) {
		_eId = eId;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setEId", String.class);

				method.invoke(_artefattoRemoteModel, eId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getExtRating() {
		return _extRating;
	}

	@Override
	public void setExtRating(int extRating) {
		_extRating = extRating;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setExtRating", int.class);

				method.invoke(_artefattoRemoteModel, extRating);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getInteractionPoint() {
		return _interactionPoint;
	}

	@Override
	public void setInteractionPoint(String interactionPoint) {
		_interactionPoint = interactionPoint;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setInteractionPoint",
						String.class);

				method.invoke(_artefattoRemoteModel, interactionPoint);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLanguage() {
		return _language;
	}

	@Override
	public void setLanguage(String language) {
		_language = language;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setLanguage", String.class);

				method.invoke(_artefattoRemoteModel, language);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getHasMapping() {
		return _hasMapping;
	}

	@Override
	public boolean isHasMapping() {
		return _hasMapping;
	}

	@Override
	public void setHasMapping(boolean hasMapping) {
		_hasMapping = hasMapping;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setHasMapping", boolean.class);

				method.invoke(_artefattoRemoteModel, hasMapping);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsPrivate() {
		return _isPrivate;
	}

	@Override
	public boolean isIsPrivate() {
		return _isPrivate;
	}

	@Override
	public void setIsPrivate(boolean isPrivate) {
		_isPrivate = isPrivate;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setIsPrivate", boolean.class);

				method.invoke(_artefattoRemoteModel, isPrivate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLusdlmodel() {
		return _lusdlmodel;
	}

	@Override
	public void setLusdlmodel(String lusdlmodel) {
		_lusdlmodel = lusdlmodel;

		if (_artefattoRemoteModel != null) {
			try {
				Class<?> clazz = _artefattoRemoteModel.getClass();

				Method method = clazz.getMethod("setLusdlmodel", String.class);

				method.invoke(_artefattoRemoteModel, lusdlmodel);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getArtefattoRemoteModel() {
		return _artefattoRemoteModel;
	}

	public void setArtefattoRemoteModel(BaseModel<?> artefattoRemoteModel) {
		_artefattoRemoteModel = artefattoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _artefattoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_artefattoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ArtefattoLocalServiceUtil.addArtefatto(this);
		}
		else {
			ArtefattoLocalServiceUtil.updateArtefatto(this);
		}
	}

	@Override
	public Artefatto toEscapedModel() {
		return (Artefatto)ProxyUtil.newProxyInstance(Artefatto.class.getClassLoader(),
			new Class[] { Artefatto.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ArtefattoClp clone = new ArtefattoClp();

		clone.setUuid(getUuid());
		clone.setArtefattoId(getArtefattoId());
		clone.setDate(getDate());
		clone.setTitle(getTitle());
		clone.setWebpage(getWebpage());
		clone.setAbstractDescription(getAbstractDescription());
		clone.setDescription(getDescription());
		clone.setProviderName(getProviderName());
		clone.setResourceRDF(getResourceRDF());
		clone.setRepositoryRDF(getRepositoryRDF());
		clone.setStatus(getStatus());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setOwner(getOwner());
		clone.setImgId(getImgId());
		clone.setCategoriamkpId(getCategoriamkpId());
		clone.setPilotid(getPilotid());
		clone.setUrl(getUrl());
		clone.setEId(getEId());
		clone.setExtRating(getExtRating());
		clone.setInteractionPoint(getInteractionPoint());
		clone.setLanguage(getLanguage());
		clone.setHasMapping(getHasMapping());
		clone.setIsPrivate(getIsPrivate());
		clone.setLusdlmodel(getLusdlmodel());

		return clone;
	}

	@Override
	public int compareTo(Artefatto artefatto) {
		int value = 0;

		if (getArtefattoId() < artefatto.getArtefattoId()) {
			value = -1;
		}
		else if (getArtefattoId() > artefatto.getArtefattoId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getCategoriamkpId() < artefatto.getCategoriamkpId()) {
			value = -1;
		}
		else if (getCategoriamkpId() > artefatto.getCategoriamkpId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getDate(), artefatto.getDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtefattoClp)) {
			return false;
		}

		ArtefattoClp artefatto = (ArtefattoClp)obj;

		long primaryKey = artefatto.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(53);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", artefattoId=");
		sb.append(getArtefattoId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append(", webpage=");
		sb.append(getWebpage());
		sb.append(", abstractDescription=");
		sb.append(getAbstractDescription());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", providerName=");
		sb.append(getProviderName());
		sb.append(", resourceRDF=");
		sb.append(getResourceRDF());
		sb.append(", repositoryRDF=");
		sb.append(getRepositoryRDF());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", owner=");
		sb.append(getOwner());
		sb.append(", imgId=");
		sb.append(getImgId());
		sb.append(", categoriamkpId=");
		sb.append(getCategoriamkpId());
		sb.append(", pilotid=");
		sb.append(getPilotid());
		sb.append(", url=");
		sb.append(getUrl());
		sb.append(", eId=");
		sb.append(getEId());
		sb.append(", extRating=");
		sb.append(getExtRating());
		sb.append(", interactionPoint=");
		sb.append(getInteractionPoint());
		sb.append(", language=");
		sb.append(getLanguage());
		sb.append(", hasMapping=");
		sb.append(getHasMapping());
		sb.append(", isPrivate=");
		sb.append(getIsPrivate());
		sb.append(", lusdlmodel=");
		sb.append(getLusdlmodel());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(82);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.Artefatto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artefattoId</column-name><column-value><![CDATA[");
		sb.append(getArtefattoId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>webpage</column-name><column-value><![CDATA[");
		sb.append(getWebpage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>abstractDescription</column-name><column-value><![CDATA[");
		sb.append(getAbstractDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>providerName</column-name><column-value><![CDATA[");
		sb.append(getProviderName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>resourceRDF</column-name><column-value><![CDATA[");
		sb.append(getResourceRDF());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>repositoryRDF</column-name><column-value><![CDATA[");
		sb.append(getRepositoryRDF());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>owner</column-name><column-value><![CDATA[");
		sb.append(getOwner());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>imgId</column-name><column-value><![CDATA[");
		sb.append(getImgId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoriamkpId</column-name><column-value><![CDATA[");
		sb.append(getCategoriamkpId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pilotid</column-name><column-value><![CDATA[");
		sb.append(getPilotid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>url</column-name><column-value><![CDATA[");
		sb.append(getUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>eId</column-name><column-value><![CDATA[");
		sb.append(getEId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>extRating</column-name><column-value><![CDATA[");
		sb.append(getExtRating());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>interactionPoint</column-name><column-value><![CDATA[");
		sb.append(getInteractionPoint());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>language</column-name><column-value><![CDATA[");
		sb.append(getLanguage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>hasMapping</column-name><column-value><![CDATA[");
		sb.append(getHasMapping());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isPrivate</column-name><column-value><![CDATA[");
		sb.append(getIsPrivate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lusdlmodel</column-name><column-value><![CDATA[");
		sb.append(getLusdlmodel());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _uuid;
	private long _artefattoId;
	private Date _date;
	private String _title;
	private String _webpage;
	private String _abstractDescription;
	private String _description;
	private String _providerName;
	private String _resourceRDF;
	private String _repositoryRDF;
	private int _status;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private String _owner;
	private long _imgId;
	private long _categoriamkpId;
	private String _pilotid;
	private String _url;
	private String _eId;
	private int _extRating;
	private String _interactionPoint;
	private String _language;
	private boolean _hasMapping;
	private boolean _isPrivate;
	private String _lusdlmodel;
	private BaseModel<?> _artefattoRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}