/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;

import java.util.List;

/**
 * The persistence utility for the immagine art service. This utility wraps {@link ImmagineArtPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ImmagineArtPersistence
 * @see ImmagineArtPersistenceImpl
 * @generated
 */
public class ImmagineArtUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ImmagineArt immagineArt) {
		getPersistence().clearCache(immagineArt);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ImmagineArt> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ImmagineArt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ImmagineArt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ImmagineArt update(ImmagineArt immagineArt)
		throws SystemException {
		return getPersistence().update(immagineArt);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ImmagineArt update(ImmagineArt immagineArt,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(immagineArt, serviceContext);
	}

	/**
	* Returns all the immagine arts where imageId = &#63;.
	*
	* @param imageId the image ID
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByImageIdentifier(imageId);
	}

	/**
	* Returns a range of all the immagine arts where imageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param imageId the image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByImageIdentifier(imageId, start, end);
	}

	/**
	* Returns an ordered range of all the immagine arts where imageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param imageId the image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByImageIdentifier(imageId, start, end, orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByImageIdentifier_First(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByImageIdentifier_First(imageId, orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByImageIdentifier_First(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByImageIdentifier_First(imageId, orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByImageIdentifier_Last(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByImageIdentifier_Last(imageId, orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByImageIdentifier_Last(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByImageIdentifier_Last(imageId, orderByComparator);
	}

	/**
	* Removes all the immagine arts where imageId = &#63; from the database.
	*
	* @param imageId the image ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByImageIdentifier(long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByImageIdentifier(imageId);
	}

	/**
	* Returns the number of immagine arts where imageId = &#63;.
	*
	* @param imageId the image ID
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static int countByImageIdentifier(long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByImageIdentifier(imageId);
	}

	/**
	* Returns all the immagine arts where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDlImageIdentifier(dlImageId);
	}

	/**
	* Returns a range of all the immagine arts where dlImageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dlImageId the dl image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDlImageIdentifier(dlImageId, start, end);
	}

	/**
	* Returns an ordered range of all the immagine arts where dlImageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dlImageId the dl image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDlImageIdentifier(dlImageId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByDlImageIdentifier_First(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByDlImageIdentifier_First(dlImageId, orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByDlImageIdentifier_First(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDlImageIdentifier_First(dlImageId, orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByDlImageIdentifier_Last(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByDlImageIdentifier_Last(dlImageId, orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByDlImageIdentifier_Last(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDlImageIdentifier_Last(dlImageId, orderByComparator);
	}

	/**
	* Returns the immagine arts before and after the current immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param imageId the primary key of the current immagine art
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt[] findByDlImageIdentifier_PrevAndNext(
		long imageId, long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByDlImageIdentifier_PrevAndNext(imageId, dlImageId,
			orderByComparator);
	}

	/**
	* Removes all the immagine arts where dlImageId = &#63; from the database.
	*
	* @param dlImageId the dl image ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByDlImageIdentifier(long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByDlImageIdentifier(dlImageId);
	}

	/**
	* Returns the number of immagine arts where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDlImageIdentifier(long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDlImageIdentifier(dlImageId);
	}

	/**
	* Returns all the immagine arts where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtefattoIdentifier(artefattoIdent);
	}

	/**
	* Returns a range of all the immagine arts where artefattoIdent = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefattoIdent the artefatto ident
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtefattoIdentifier(artefattoIdent, start, end);
	}

	/**
	* Returns an ordered range of all the immagine arts where artefattoIdent = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefattoIdent the artefatto ident
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtefattoIdentifier(artefattoIdent, start, end,
			orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByArtefattoIdentifier_First(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByArtefattoIdentifier_First(artefattoIdent,
			orderByComparator);
	}

	/**
	* Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByArtefattoIdentifier_First(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtefattoIdentifier_First(artefattoIdent,
			orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByArtefattoIdentifier_Last(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByArtefattoIdentifier_Last(artefattoIdent,
			orderByComparator);
	}

	/**
	* Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByArtefattoIdentifier_Last(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtefattoIdentifier_Last(artefattoIdent,
			orderByComparator);
	}

	/**
	* Returns the immagine arts before and after the current immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param imageId the primary key of the current immagine art
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt[] findByArtefattoIdentifier_PrevAndNext(
		long imageId, long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence()
				   .findByArtefattoIdentifier_PrevAndNext(imageId,
			artefattoIdent, orderByComparator);
	}

	/**
	* Removes all the immagine arts where artefattoIdent = &#63; from the database.
	*
	* @param artefattoIdent the artefatto ident
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByArtefattoIdentifier(long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByArtefattoIdentifier(artefattoIdent);
	}

	/**
	* Returns the number of immagine arts where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static int countByArtefattoIdentifier(long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByArtefattoIdentifier(artefattoIdent);
	}

	/**
	* Caches the immagine art in the entity cache if it is enabled.
	*
	* @param immagineArt the immagine art
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt) {
		getPersistence().cacheResult(immagineArt);
	}

	/**
	* Caches the immagine arts in the entity cache if it is enabled.
	*
	* @param immagineArts the immagine arts
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> immagineArts) {
		getPersistence().cacheResult(immagineArts);
	}

	/**
	* Creates a new immagine art with the primary key. Does not add the immagine art to the database.
	*
	* @param imageId the primary key for the new immagine art
	* @return the new immagine art
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt create(
		long imageId) {
		return getPersistence().create(imageId);
	}

	/**
	* Removes the immagine art with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt remove(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence().remove(imageId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(immagineArt);
	}

	/**
	* Returns the immagine art with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException} if it could not be found.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByPrimaryKey(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException {
		return getPersistence().findByPrimaryKey(imageId);
	}

	/**
	* Returns the immagine art with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art, or <code>null</code> if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByPrimaryKey(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(imageId);
	}

	/**
	* Returns all the immagine arts.
	*
	* @return the immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the immagine arts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the immagine arts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the immagine arts from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of immagine arts.
	*
	* @return the number of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ImmagineArtPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ImmagineArtPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					ImmagineArtPersistence.class.getName());

			ReferenceRegistry.registerReference(ImmagineArtUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ImmagineArtPersistence persistence) {
	}

	private static ImmagineArtPersistence _persistence;
}