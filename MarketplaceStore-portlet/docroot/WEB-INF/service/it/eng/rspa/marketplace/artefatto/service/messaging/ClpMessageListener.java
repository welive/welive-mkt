/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.PendingNotificationEntryLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.SearchServiceUtil;

/**
 * @author eng
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			ArtefattoLocalServiceUtil.clearService();

			ArtifactLevelsLocalServiceUtil.clearService();

			ArtifactOrganizationsLocalServiceUtil.clearService();

			CategoriaLocalServiceUtil.clearService();

			DependenciesLocalServiceUtil.clearService();

			ImmagineArtLocalServiceUtil.clearService();

			MarketplaceConfLocalServiceUtil.clearService();

			PendingNotificationEntryLocalServiceUtil.clearService();

			SearchServiceUtil.clearService();
		}
	}
}