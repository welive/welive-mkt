/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ImmagineArtLocalService}.
 *
 * @author eng
 * @see ImmagineArtLocalService
 * @generated
 */
public class ImmagineArtLocalServiceWrapper implements ImmagineArtLocalService,
	ServiceWrapper<ImmagineArtLocalService> {
	public ImmagineArtLocalServiceWrapper(
		ImmagineArtLocalService immagineArtLocalService) {
		_immagineArtLocalService = immagineArtLocalService;
	}

	/**
	* Adds the immagine art to the database. Also notifies the appropriate model listeners.
	*
	* @param immagineArt the immagine art
	* @return the immagine art that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt addImmagineArt(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.addImmagineArt(immagineArt);
	}

	/**
	* Creates a new immagine art with the primary key. Does not add the immagine art to the database.
	*
	* @param imageId the primary key for the new immagine art
	* @return the new immagine art
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt createImmagineArt(
		long imageId) {
		return _immagineArtLocalService.createImmagineArt(imageId);
	}

	/**
	* Deletes the immagine art with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art that was removed
	* @throws PortalException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt deleteImmagineArt(
		long imageId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.deleteImmagineArt(imageId);
	}

	/**
	* Deletes the immagine art from the database. Also notifies the appropriate model listeners.
	*
	* @param immagineArt the immagine art
	* @return the immagine art that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt deleteImmagineArt(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.deleteImmagineArt(immagineArt);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _immagineArtLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchImmagineArt(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.fetchImmagineArt(imageId);
	}

	/**
	* Returns the immagine art with the primary key.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art
	* @throws PortalException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt getImmagineArt(
		long imageId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.getImmagineArt(imageId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the immagine arts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> getImmagineArts(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.getImmagineArts(start, end);
	}

	/**
	* Returns the number of immagine arts.
	*
	* @return the number of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getImmagineArtsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.getImmagineArtsCount();
	}

	/**
	* Updates the immagine art in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param immagineArt the immagine art
	* @return the immagine art that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt updateImmagineArt(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.updateImmagineArt(immagineArt);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _immagineArtLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_immagineArtLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _immagineArtLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* @param standard
	* @return List<ImmagineArt>
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoId(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _immagineArtLocalService.findByArtefattoId(artefattoId);
	}

	@Override
	public void removeByArtefattoId(long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		_immagineArtLocalService.removeByArtefattoId(artefattoId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ImmagineArtLocalService getWrappedImmagineArtLocalService() {
		return _immagineArtLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedImmagineArtLocalService(
		ImmagineArtLocalService immagineArtLocalService) {
		_immagineArtLocalService = immagineArtLocalService;
	}

	@Override
	public ImmagineArtLocalService getWrappedService() {
		return _immagineArtLocalService;
	}

	@Override
	public void setWrappedService(
		ImmagineArtLocalService immagineArtLocalService) {
		_immagineArtLocalService = immagineArtLocalService;
	}

	private ImmagineArtLocalService _immagineArtLocalService;
}