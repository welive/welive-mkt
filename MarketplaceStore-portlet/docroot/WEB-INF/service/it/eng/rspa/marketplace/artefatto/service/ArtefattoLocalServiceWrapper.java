/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ArtefattoLocalService}.
 *
 * @author eng
 * @see ArtefattoLocalService
 * @generated
 */
public class ArtefattoLocalServiceWrapper implements ArtefattoLocalService,
	ServiceWrapper<ArtefattoLocalService> {
	public ArtefattoLocalServiceWrapper(
		ArtefattoLocalService artefattoLocalService) {
		_artefattoLocalService = artefattoLocalService;
	}

	/**
	* Adds the artefatto to the database. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.addArtefatto(artefatto);
	}

	/**
	* Creates a new artefatto with the primary key. Does not add the artefatto to the database.
	*
	* @param artefattoId the primary key for the new artefatto
	* @return the new artefatto
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto createArtefatto(
		long artefattoId) {
		return _artefattoLocalService.createArtefatto(artefattoId);
	}

	/**
	* Deletes the artefatto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto that was removed
	* @throws PortalException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.deleteArtefatto(artefattoId);
	}

	/**
	* Deletes the artefatto from the database. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.deleteArtefatto(artefatto);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _artefattoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.fetchArtefatto(artefattoId);
	}

	/**
	* Returns the artefatto with the matching UUID and company.
	*
	* @param uuid the artefatto's UUID
	* @param companyId the primary key of the company
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.fetchArtefattoByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the artefatto matching the UUID and group.
	*
	* @param uuid the artefatto's UUID
	* @param groupId the primary key of the group
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.fetchArtefattoByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the artefatto with the primary key.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto
	* @throws PortalException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getArtefatto(artefattoId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the artefatto with the matching UUID and company.
	*
	* @param uuid the artefatto's UUID
	* @param companyId the primary key of the company
	* @return the matching artefatto
	* @throws PortalException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getArtefattoByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the artefatto matching the UUID and group.
	*
	* @param uuid the artefatto's UUID
	* @param groupId the primary key of the group
	* @return the matching artefatto
	* @throws PortalException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getArtefattoByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the artefattos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of artefattos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getArtefattos(start, end);
	}

	/**
	* Returns the number of artefattos.
	*
	* @return the number of artefattos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getArtefattosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.getArtefattosCount();
	}

	/**
	* Updates the artefatto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto updateArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.updateArtefatto(artefatto);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _artefattoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_artefattoLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _artefattoLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto publishArtefatto(
		long idArtefatto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.publishArtefatto(idArtefatto);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, java.lang.String[] tags)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.addArtefatto(newArtefatto, userId, tags);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.addArtefatto(newArtefatto, userId,
			serviceContext);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto permanentDeleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefattoLocalService.permanentDeleteArtefatto(artefatto);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status) {
		return _artefattoLocalService.getArtefattiByPilotAndStatus(pilotId,
			status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status, java.lang.String orderByCampo,
		java.lang.String orderType) {
		return _artefattoLocalService.getArtefattiByPilotAndStatus(pilotId,
			status, orderByCampo, orderType);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByDescriptionRDF(
		java.lang.String description) {
		return _artefattoLocalService.getArtefattoByDescriptionRDF(description);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByResourceRDF(
		java.lang.String resourceRDF) {
		return _artefattoLocalService.getArtefattoByResourceRDF(resourceRDF);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByTitle(
		java.lang.String title) {
		return _artefattoLocalService.getArtefattiByTitle(title);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status) {
		return _artefattoLocalService.findArtefattoByStatus(status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status, int start, int end) {
		return _artefattoLocalService.getArtefactsByPilotStatus(pilot, status,
			start, end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status) {
		return _artefattoLocalService.getArtefactsByPilotStatus(pilot, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status, java.lang.String orderByCampo, java.lang.String orderType) {
		return _artefattoLocalService.findArtefattoByStatus(status,
			orderByCampo, orderType);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByUserId_Status(
		long userId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		return _artefattoLocalService.findArtefattoByUserId_Status(userId,
			status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory_Status(
		long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		return _artefattoLocalService.findArtefattoByCategory_Status(categoryId,
			status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByPilot_Category_Status(
		java.lang.String pilot, long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		return _artefattoLocalService.findArtefattoByPilot_Category_Status(pilot,
			categoryId, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId) {
		return _artefattoLocalService.findArtefattoByCompanyId(companyId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo, boolean isAsc) {
		return _artefattoLocalService.findArtefattoByCompanyId(companyId,
			orderByCampo, isAsc);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo,
		java.lang.String orderType) {
		return _artefattoLocalService.findArtefattoByCompanyId(companyId,
			orderByCampo, orderType);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category) {
		return _artefattoLocalService.findArtefattoByCategory(category);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category, int start, int end) {
		return _artefattoLocalService.findArtefattoByCategory(category, start,
			end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattiByIds(
		java.util.List<java.lang.Long> ids) {
		return _artefattoLocalService.findArtefattiByIds(ids);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status) {
		return _artefattoLocalService.getArtefattiByLangStatus(lang, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status) {
		return _artefattoLocalService.getArtefattiByPilotLangStatus(pilot,
			lang, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status, int start, int end) {
		return _artefattoLocalService.getArtefattiByLangStatus(lang, status,
			start, end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		return _artefattoLocalService.getArtefattiByPilotLangStatus(pilot,
			lang, status, start, end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByKeyword(
		java.lang.String keyword) {
		return _artefattoLocalService.getArtefattiByKeyword(keyword);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId) {
		return _artefattoLocalService.getBuildingBlocks(pilotId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId, int startId, int endId) {
		return _artefattoLocalService.getBuildingBlocks(pilotId, startId, endId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByKeyword(
		java.lang.String pilotId, java.lang.String keyword) {
		return _artefattoLocalService.getBuildingBlocksByKeyword(pilotId,
			keyword);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByLangStatus(
		java.lang.String lang, int status) {
		return _artefattoLocalService.getBuildingBlocksByLangStatus(lang, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		return _artefattoLocalService.getBuildingBlocksByPilotLangStatus(pilot,
			lang, status, start, end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot) {
		return _artefattoLocalService.getPublicServices(pilot);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot, int starId, int endId) {
		return _artefattoLocalService.getPublicServices(pilot, starId, endId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByKeyword(
		java.lang.String pilot, java.lang.String keyword) {
		return _artefattoLocalService.getPublicServicesByKeyword(pilot, keyword);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByLangStatus(
		java.lang.String lang, int status) {
		return _artefattoLocalService.getPublicServicesByLangStatus(lang, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		return _artefattoLocalService.getPublicServicesByPilotLangStatus(pilot,
			lang, status, start, end);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot) {
		return _artefattoLocalService.getDatasets(pilot);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot, int startId, int endId) {
		return _artefattoLocalService.getDatasets(pilot, startId, endId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByKeyword(
		java.lang.String pilot, java.lang.String keyword) {
		return _artefattoLocalService.getDatasetsByKeyword(pilot, keyword);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByLangStatus(
		java.lang.String lang, int status) {
		return _artefattoLocalService.getDatasetsByLangStatus(lang, status);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		return _artefattoLocalService.getDatasetsByPilotLangStatus(pilot, lang,
			status, start, end);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefactByExternalId(
		java.lang.String eid) throws java.lang.Exception {
		return _artefattoLocalService.getArtefactByExternalId(eid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByPilot(
		java.lang.String pilot, int maxNumber) {
		return _artefattoLocalService.getTopRatedArtefactsByPilot(pilot,
			maxNumber);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByLanguage(
		java.lang.String language, int maxNumber) {
		return _artefattoLocalService.getTopRatedArtefactsByLanguage(language,
			maxNumber);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getArtefactsByUserId(
		long liferayUserId, int maxNumber) {
		return _artefattoLocalService.getArtefactsByUserId(liferayUserId,
			maxNumber);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwner(
		java.lang.String owner) {
		return _artefattoLocalService.getArtefattiByOwner(owner);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwnerAndStatus(
		java.lang.String owner, int status) {
		return _artefattoLocalService.getArtefattiByOwnerAndStatus(owner, status);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject forget(
		long liferayUserId, boolean removecontent) throws java.lang.Exception {
		return _artefattoLocalService.forget(liferayUserId, removecontent);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ArtefattoLocalService getWrappedArtefattoLocalService() {
		return _artefattoLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedArtefattoLocalService(
		ArtefattoLocalService artefattoLocalService) {
		_artefattoLocalService = artefattoLocalService;
	}

	@Override
	public ArtefattoLocalService getWrappedService() {
		return _artefattoLocalService;
	}

	@Override
	public void setWrappedService(ArtefattoLocalService artefattoLocalService) {
		_artefattoLocalService = artefattoLocalService;
	}

	private ArtefattoLocalService _artefattoLocalService;
}