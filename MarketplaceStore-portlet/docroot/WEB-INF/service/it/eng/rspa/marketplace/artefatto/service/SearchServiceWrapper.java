/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SearchService}.
 *
 * @author eng
 * @see SearchService
 * @generated
 */
public class SearchServiceWrapper implements SearchService,
	ServiceWrapper<SearchService> {
	public SearchServiceWrapper(SearchService searchService) {
		_searchService = searchService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _searchService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_searchService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _searchService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getAllArtefatti(
		java.lang.String pilotID, java.lang.String artefactTypes) {
		return _searchService.getAllArtefatti(pilotID, artefactTypes);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getAllArtefatti(
		java.lang.String pilotID, java.lang.String artefactTypes,
		java.lang.String level) {
		return _searchService.getAllArtefatti(pilotID, artefactTypes, level);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getAllArtefattiV2(
		java.lang.String pilotID, java.lang.String artefactTypes) {
		return _searchService.getAllArtefattiV2(pilotID, artefactTypes);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot) throws java.lang.Exception {
		return _searchService.getArtefattiByKeywords(keywords, artefactTypes,
			pilot);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot, java.lang.String level)
		throws java.lang.Exception {
		return _searchService.getArtefattiByKeywords(keywords, artefactTypes,
			pilot, level);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywordsV2(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.lang.Exception, java.rmi.RemoteException {
		return _searchService.getArtefattiByKeywordsV2(keywords, artefactTypes,
			pilot);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getModelsById(
		java.lang.String[] ids) throws java.lang.Exception {
		return _searchService.getModelsById(ids);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getArtefact(
		long artefactid) {
		return _searchService.getArtefact(artefactid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getArtefactV2(
		long artefactid) {
		return _searchService.getArtefactV2(artefactid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject createDataset(
		java.lang.String datasetId, java.lang.String title,
		java.lang.String description, java.lang.String resourceRdf,
		java.lang.String webpage, java.lang.String ccUserId,
		java.lang.String providerName, java.lang.String language)
		throws java.lang.Exception {
		return _searchService.createDataset(datasetId, title, description,
			resourceRdf, webpage, ccUserId, providerName, language);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject createService(
		java.lang.String title, java.lang.String description,
		java.lang.String category, java.lang.String resourceRdf,
		java.lang.String endpoint, java.lang.String webpage,
		java.lang.String ccUserId, java.lang.String providerName,
		java.lang.String language) throws java.lang.Exception {
		return _searchService.createService(title, description, category,
			resourceRdf, endpoint, webpage, ccUserId, providerName, language);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject setupArtefact(
		java.lang.String body) throws java.lang.Exception {
		return _searchService.setupArtefact(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateDataset(
		java.lang.String body) throws java.lang.Exception {
		return _searchService.updateDataset(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject newDataset_v2(
		java.lang.String body) throws java.lang.Exception {
		return _searchService.newDataset_v2(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deleteartefact(
		long artefactid) throws java.lang.Exception {
		return _searchService.deleteartefact(artefactid);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateArtefact(
		long artefactid, java.lang.String body) throws java.lang.Exception {
		return _searchService.updateArtefact(artefactid, body);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public SearchService getWrappedSearchService() {
		return _searchService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedSearchService(SearchService searchService) {
		_searchService = searchService;
	}

	@Override
	public SearchService getWrappedService() {
		return _searchService;
	}

	@Override
	public void setWrappedService(SearchService searchService) {
		_searchService = searchService;
	}

	private SearchService _searchService;
}