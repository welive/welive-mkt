/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;

/**
 * The persistence interface for the artefatto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtefattoPersistenceImpl
 * @see ArtefattoUtil
 * @generated
 */
public interface ArtefattoPersistence extends BasePersistence<Artefatto> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ArtefattoUtil} to access the artefatto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the artefattos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where uuid = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByUuid_PrevAndNext(
		long artefattoId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto where uuid = &#63; and groupId = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the artefatto where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the artefatto where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the artefatto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the number of artefattos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByUuid_C_PrevAndNext(
		long artefattoId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where groupId = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByGroupId_PrevAndNext(
		long artefattoId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCompanyId(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where companyId = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByCompanyId_PrevAndNext(
		long artefattoId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId_Status(
		long userId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where userId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId_Status(
		long userId, int status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where userId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId_Status(
		long userId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUserId_Status_First(
		long userId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUserId_Status_First(
		long userId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUserId_Status_Last(
		long userId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUserId_Status_Last(
		long userId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where userId = &#63; and status = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param userId the user ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByUserId_Status_PrevAndNext(
		long artefattoId, long userId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where userId = &#63; and status = &#63; from the database.
	*
	* @param userId the user ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId_Status(long userId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where userId = &#63; and status = &#63;.
	*
	* @param userId the user ID
	* @param status the status
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId_Status(long userId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where userId = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByUserId_PrevAndNext(
		long artefattoId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where title = &#63;.
	*
	* @param title the title
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByTitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByTitle(
		java.lang.String title, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByTitle(
		java.lang.String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByTitle_First(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByTitle_First(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByTitle_Last(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByTitle_Last(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where title = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByTitle_PrevAndNext(
		long artefattoId, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where title = &#63; from the database.
	*
	* @param title the title
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where title = &#63;.
	*
	* @param title the title
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByTitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where owner = &#63;.
	*
	* @param owner the owner
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByOwner(
		java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where owner = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param owner the owner
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByOwner(
		java.lang.String owner, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where owner = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param owner the owner
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByOwner(
		java.lang.String owner, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where owner = &#63;.
	*
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByOwner_First(
		java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where owner = &#63;.
	*
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByOwner_First(
		java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where owner = &#63;.
	*
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByOwner_Last(
		java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where owner = &#63;.
	*
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByOwner_Last(
		java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where owner = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByOwner_PrevAndNext(
		long artefattoId, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where owner = &#63; from the database.
	*
	* @param owner the owner
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOwner(java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where owner = &#63;.
	*
	* @param owner the owner
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByOwner(java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndOwner(
		int status, java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where status = &#63; and owner = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param owner the owner
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndOwner(
		int status, java.lang.String owner, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where status = &#63; and owner = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param owner the owner
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndOwner(
		int status, java.lang.String owner, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndOwner_First(
		int status, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndOwner_First(
		int status, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndOwner_Last(
		int status, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndOwner_Last(
		int status, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and owner = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param status the status
	* @param owner the owner
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBystatusAndOwner_PrevAndNext(
		long artefattoId, int status, java.lang.String owner,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where status = &#63; and owner = &#63; from the database.
	*
	* @param status the status
	* @param owner the owner
	* @throws SystemException if a system exception occurred
	*/
	public void removeBystatusAndOwner(int status, java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where status = &#63; and owner = &#63;.
	*
	* @param status the status
	* @param owner the owner
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBystatusAndOwner(int status, java.lang.String owner)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByResourceRDF(
		java.lang.String resourceRDF)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where resourceRDF = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param resourceRDF the resource r d f
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByResourceRDF(
		java.lang.String resourceRDF, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where resourceRDF = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param resourceRDF the resource r d f
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByResourceRDF(
		java.lang.String resourceRDF, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByResourceRDF_First(
		java.lang.String resourceRDF,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByResourceRDF_First(
		java.lang.String resourceRDF,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByResourceRDF_Last(
		java.lang.String resourceRDF,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByResourceRDF_Last(
		java.lang.String resourceRDF,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where resourceRDF = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param resourceRDF the resource r d f
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByResourceRDF_PrevAndNext(
		long artefattoId, java.lang.String resourceRDF,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where resourceRDF = &#63; from the database.
	*
	* @param resourceRDF the resource r d f
	* @throws SystemException if a system exception occurred
	*/
	public void removeByResourceRDF(java.lang.String resourceRDF)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where resourceRDF = &#63;.
	*
	* @param resourceRDF the resource r d f
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByResourceRDF(java.lang.String resourceRDF)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where providerName = &#63;.
	*
	* @param providerName the provider name
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByProviderName(
		java.lang.String providerName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where providerName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param providerName the provider name
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByProviderName(
		java.lang.String providerName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where providerName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param providerName the provider name
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByProviderName(
		java.lang.String providerName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where providerName = &#63;.
	*
	* @param providerName the provider name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByProviderName_First(
		java.lang.String providerName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where providerName = &#63;.
	*
	* @param providerName the provider name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByProviderName_First(
		java.lang.String providerName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where providerName = &#63;.
	*
	* @param providerName the provider name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByProviderName_Last(
		java.lang.String providerName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where providerName = &#63;.
	*
	* @param providerName the provider name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByProviderName_Last(
		java.lang.String providerName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where providerName = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param providerName the provider name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByProviderName_PrevAndNext(
		long artefattoId, java.lang.String providerName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where providerName = &#63; from the database.
	*
	* @param providerName the provider name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByProviderName(java.lang.String providerName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where providerName = &#63;.
	*
	* @param providerName the provider name
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByProviderName(java.lang.String providerName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria(
		long categoriamkpId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where categoriamkpId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriamkpId the categoriamkp ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria(
		long categoriamkpId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where categoriamkpId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriamkpId the categoriamkp ID
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria(
		long categoriamkpId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCategoria_First(
		long categoriamkpId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCategoria_First(
		long categoriamkpId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCategoria_Last(
		long categoriamkpId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCategoria_Last(
		long categoriamkpId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where categoriamkpId = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param categoriamkpId the categoriamkp ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByCategoria_PrevAndNext(
		long artefattoId, long categoriamkpId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where categoriamkpId = &#63; from the database.
	*
	* @param categoriamkpId the categoriamkp ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCategoria(long categoriamkpId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where categoriamkpId = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByCategoria(long categoriamkpId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria_Status(
		long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where categoriamkpId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria_Status(
		long categoriamkpId, int status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where categoriamkpId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByCategoria_Status(
		long categoriamkpId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCategoria_Status_First(
		long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCategoria_Status_First(
		long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByCategoria_Status_Last(
		long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByCategoria_Status_Last(
		long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where categoriamkpId = &#63; and status = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByCategoria_Status_PrevAndNext(
		long artefattoId, long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where categoriamkpId = &#63; and status = &#63; from the database.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCategoria_Status(long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where categoriamkpId = &#63; and status = &#63;.
	*
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByCategoria_Status(long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByPilot_Categoria_Status(
		java.lang.String pilotid, long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByPilot_Categoria_Status(
		java.lang.String pilotid, long categoriamkpId, int status, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByPilot_Categoria_Status(
		java.lang.String pilotid, long categoriamkpId, int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByPilot_Categoria_Status_First(
		java.lang.String pilotid, long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByPilot_Categoria_Status_First(
		java.lang.String pilotid, long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByPilot_Categoria_Status_Last(
		java.lang.String pilotid, long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByPilot_Categoria_Status_Last(
		java.lang.String pilotid, long categoriamkpId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByPilot_Categoria_Status_PrevAndNext(
		long artefattoId, java.lang.String pilotid, long categoriamkpId,
		int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63; from the database.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPilot_Categoria_Status(java.lang.String pilotid,
		long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where pilotid = &#63; and categoriamkpId = &#63; and status = &#63;.
	*
	* @param pilotid the pilotid
	* @param categoriamkpId the categoriamkp ID
	* @param status the status
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByPilot_Categoria_Status(java.lang.String pilotid,
		long categoriamkpId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotId(
		java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where pilotid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotId(
		java.lang.String pilotid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where pilotid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotId(
		java.lang.String pilotid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotId_First(
		java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotId_First(
		java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotId_Last(
		java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotId_Last(
		java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBypilotId_PrevAndNext(
		long artefattoId, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where pilotid = &#63; from the database.
	*
	* @param pilotid the pilotid
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypilotId(java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where pilotid = &#63;.
	*
	* @param pilotid the pilotid
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBypilotId(java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where status = &#63;.
	*
	* @param status the status
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByStatus(
		int status) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByStatus(
		int status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findByStatus(
		int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByStatus_First(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByStatus_First(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByStatus_Last(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByStatus_Last(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where status = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findByStatus_PrevAndNext(
		long artefattoId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where status = &#63; from the database.
	*
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStatus(int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where status = &#63;.
	*
	* @param status the status
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByStatus(int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndpilotId(
		int status, java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where status = &#63; and pilotid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndpilotId(
		int status, java.lang.String pilotid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where status = &#63; and pilotid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndpilotId(
		int status, java.lang.String pilotid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndpilotId_First(
		int status, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndpilotId_First(
		int status, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndpilotId_Last(
		int status, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndpilotId_Last(
		int status, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and pilotid = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param status the status
	* @param pilotid the pilotid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBystatusAndpilotId_PrevAndNext(
		long artefattoId, int status, java.lang.String pilotid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where status = &#63; and pilotid = &#63; from the database.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @throws SystemException if a system exception occurred
	*/
	public void removeBystatusAndpilotId(int status, java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where status = &#63; and pilotid = &#63;.
	*
	* @param status the status
	* @param pilotid the pilotid
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBystatusAndpilotId(int status, java.lang.String pilotid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where language = &#63;.
	*
	* @param language the language
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBylanguage(
		java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBylanguage(
		java.lang.String language, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBylanguage(
		java.lang.String language, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where language = &#63;.
	*
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBylanguage_First(
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where language = &#63;.
	*
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBylanguage_First(
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where language = &#63;.
	*
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBylanguage_Last(
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where language = &#63;.
	*
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBylanguage_Last(
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where language = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBylanguage_PrevAndNext(
		long artefattoId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where language = &#63; from the database.
	*
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeBylanguage(java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where language = &#63;.
	*
	* @param language the language
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBylanguage(java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndLanguage(
		int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where status = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndLanguage(
		int status, java.lang.String language, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where status = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBystatusAndLanguage(
		int status, java.lang.String language, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndLanguage_First(
		int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndLanguage_First(
		int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBystatusAndLanguage_Last(
		int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBystatusAndLanguage_Last(
		int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and language = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBystatusAndLanguage_PrevAndNext(
		long artefattoId, int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where status = &#63; and language = &#63; from the database.
	*
	* @param status the status
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeBystatusAndLanguage(int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where status = &#63; and language = &#63;.
	*
	* @param status the status
	* @param language the language
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBystatusAndLanguage(int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBycategoryStatusAndLanguage(
		int status, long categoriamkpId, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBycategoryStatusAndLanguage(
		int status, long categoriamkpId, java.lang.String language, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBycategoryStatusAndLanguage(
		int status, long categoriamkpId, java.lang.String language, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBycategoryStatusAndLanguage_First(
		int status, long categoriamkpId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBycategoryStatusAndLanguage_First(
		int status, long categoriamkpId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBycategoryStatusAndLanguage_Last(
		int status, long categoriamkpId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBycategoryStatusAndLanguage_Last(
		int status, long categoriamkpId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBycategoryStatusAndLanguage_PrevAndNext(
		long artefattoId, int status, long categoriamkpId,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63; from the database.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycategoryStatusAndLanguage(int status,
		long categoriamkpId, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBycategoryStatusAndLanguage(int status,
		long categoriamkpId, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotStatusAndLanguage(
		java.lang.String pilotid, int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotStatusAndLanguage(
		java.lang.String pilotid, int status, java.lang.String language,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotStatusAndLanguage(
		java.lang.String pilotid, int status, java.lang.String language,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotStatusAndLanguage_First(
		java.lang.String pilotid, int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotStatusAndLanguage_First(
		java.lang.String pilotid, int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotStatusAndLanguage_Last(
		java.lang.String pilotid, int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotStatusAndLanguage_Last(
		java.lang.String pilotid, int status, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBypilotStatusAndLanguage_PrevAndNext(
		long artefattoId, java.lang.String pilotid, int status,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where pilotid = &#63; and status = &#63; and language = &#63; from the database.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypilotStatusAndLanguage(java.lang.String pilotid,
		int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where pilotid = &#63; and status = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param language the language
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBypilotStatusAndLanguage(java.lang.String pilotid,
		int status, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @return the matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotCategoryStatusAndLanguage(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotCategoryStatusAndLanguage(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findBypilotCategoryStatusAndLanguage(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotCategoryStatusAndLanguage_First(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the first artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotCategoryStatusAndLanguage_First(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findBypilotCategoryStatusAndLanguage_Last(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the last artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchBypilotCategoryStatusAndLanguage_Last(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefattos before and after the current artefatto in the ordered set where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param artefattoId the primary key of the current artefatto
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto[] findBypilotCategoryStatusAndLanguage_PrevAndNext(
		long artefattoId, java.lang.String pilotid, int status,
		long categoriamkpId, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Removes all the artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63; from the database.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypilotCategoryStatusAndLanguage(
		java.lang.String pilotid, int status, long categoriamkpId,
		java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos where pilotid = &#63; and status = &#63; and categoriamkpId = &#63; and language = &#63;.
	*
	* @param pilotid the pilotid
	* @param status the status
	* @param categoriamkpId the categoriamkp ID
	* @param language the language
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countBypilotCategoryStatusAndLanguage(java.lang.String pilotid,
		int status, long categoriamkpId, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto where eId = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	*
	* @param eId the e ID
	* @return the matching artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByeid(
		java.lang.String eId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the artefatto where eId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param eId the e ID
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByeid(
		java.lang.String eId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto where eId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param eId the e ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByeid(
		java.lang.String eId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the artefatto where eId = &#63; from the database.
	*
	* @param eId the e ID
	* @return the artefatto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto removeByeid(
		java.lang.String eId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the number of artefattos where eId = &#63;.
	*
	* @param eId the e ID
	* @return the number of matching artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countByeid(java.lang.String eId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the artefatto in the entity cache if it is enabled.
	*
	* @param artefatto the artefatto
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto);

	/**
	* Caches the artefattos in the entity cache if it is enabled.
	*
	* @param artefattos the artefattos
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> artefattos);

	/**
	* Creates a new artefatto with the primary key. Does not add the artefatto to the database.
	*
	* @param artefattoId the primary key for the new artefatto
	* @return the new artefatto
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto create(
		long artefattoId);

	/**
	* Removes the artefatto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto remove(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	public it.eng.rspa.marketplace.artefatto.model.Artefatto updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException} if it could not be found.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto findByPrimaryKey(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException;

	/**
	* Returns the artefatto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto, or <code>null</code> if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchByPrimaryKey(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artefattos.
	*
	* @return the artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artefattos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of artefattos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the artefattos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos.
	*
	* @return the number of artefattos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}