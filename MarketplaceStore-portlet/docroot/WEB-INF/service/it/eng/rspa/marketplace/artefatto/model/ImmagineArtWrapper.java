/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ImmagineArt}.
 * </p>
 *
 * @author eng
 * @see ImmagineArt
 * @generated
 */
public class ImmagineArtWrapper implements ImmagineArt,
	ModelWrapper<ImmagineArt> {
	public ImmagineArtWrapper(ImmagineArt immagineArt) {
		_immagineArt = immagineArt;
	}

	@Override
	public Class<?> getModelClass() {
		return ImmagineArt.class;
	}

	@Override
	public String getModelClassName() {
		return ImmagineArt.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("imageId", getImageId());
		attributes.put("dlImageId", getDlImageId());
		attributes.put("descrizione", getDescrizione());
		attributes.put("artefattoIdent", getArtefattoIdent());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long imageId = (Long)attributes.get("imageId");

		if (imageId != null) {
			setImageId(imageId);
		}

		Long dlImageId = (Long)attributes.get("dlImageId");

		if (dlImageId != null) {
			setDlImageId(dlImageId);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Long artefattoIdent = (Long)attributes.get("artefattoIdent");

		if (artefattoIdent != null) {
			setArtefattoIdent(artefattoIdent);
		}
	}

	/**
	* Returns the primary key of this immagine art.
	*
	* @return the primary key of this immagine art
	*/
	@Override
	public long getPrimaryKey() {
		return _immagineArt.getPrimaryKey();
	}

	/**
	* Sets the primary key of this immagine art.
	*
	* @param primaryKey the primary key of this immagine art
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_immagineArt.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the image ID of this immagine art.
	*
	* @return the image ID of this immagine art
	*/
	@Override
	public long getImageId() {
		return _immagineArt.getImageId();
	}

	/**
	* Sets the image ID of this immagine art.
	*
	* @param imageId the image ID of this immagine art
	*/
	@Override
	public void setImageId(long imageId) {
		_immagineArt.setImageId(imageId);
	}

	/**
	* Returns the dl image ID of this immagine art.
	*
	* @return the dl image ID of this immagine art
	*/
	@Override
	public long getDlImageId() {
		return _immagineArt.getDlImageId();
	}

	/**
	* Sets the dl image ID of this immagine art.
	*
	* @param dlImageId the dl image ID of this immagine art
	*/
	@Override
	public void setDlImageId(long dlImageId) {
		_immagineArt.setDlImageId(dlImageId);
	}

	/**
	* Returns the descrizione of this immagine art.
	*
	* @return the descrizione of this immagine art
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _immagineArt.getDescrizione();
	}

	/**
	* Sets the descrizione of this immagine art.
	*
	* @param descrizione the descrizione of this immagine art
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_immagineArt.setDescrizione(descrizione);
	}

	/**
	* Returns the artefatto ident of this immagine art.
	*
	* @return the artefatto ident of this immagine art
	*/
	@Override
	public long getArtefattoIdent() {
		return _immagineArt.getArtefattoIdent();
	}

	/**
	* Sets the artefatto ident of this immagine art.
	*
	* @param artefattoIdent the artefatto ident of this immagine art
	*/
	@Override
	public void setArtefattoIdent(long artefattoIdent) {
		_immagineArt.setArtefattoIdent(artefattoIdent);
	}

	@Override
	public boolean isNew() {
		return _immagineArt.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_immagineArt.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _immagineArt.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_immagineArt.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _immagineArt.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _immagineArt.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_immagineArt.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _immagineArt.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_immagineArt.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_immagineArt.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_immagineArt.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ImmagineArtWrapper((ImmagineArt)_immagineArt.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt) {
		return _immagineArt.compareTo(immagineArt);
	}

	@Override
	public int hashCode() {
		return _immagineArt.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> toCacheModel() {
		return _immagineArt.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt toEscapedModel() {
		return new ImmagineArtWrapper(_immagineArt.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt toUnescapedModel() {
		return new ImmagineArtWrapper(_immagineArt.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _immagineArt.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _immagineArt.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_immagineArt.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ImmagineArtWrapper)) {
			return false;
		}

		ImmagineArtWrapper immagineArtWrapper = (ImmagineArtWrapper)obj;

		if (Validator.equals(_immagineArt, immagineArtWrapper._immagineArt)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ImmagineArt getWrappedImmagineArt() {
		return _immagineArt;
	}

	@Override
	public ImmagineArt getWrappedModel() {
		return _immagineArt;
	}

	@Override
	public void resetOriginalValues() {
		_immagineArt.resetOriginalValues();
	}

	private ImmagineArt _immagineArt;
}