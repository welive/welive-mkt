/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;

/**
 * The persistence interface for the artifact levels service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactLevelsPersistenceImpl
 * @see ArtifactLevelsUtil
 * @generated
 */
public interface ArtifactLevelsPersistence extends BasePersistence<ArtifactLevels> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ArtifactLevelsUtil} to access the artifact levels persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @return the matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByArtifactId_Level_First(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByArtifactId_Level_First(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByArtifactId_Level_Last(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByArtifactId_Level_Last(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the artifact levelses where artifactId = &#63; and level = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArtifactId_Level(long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @return the number of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public int countByArtifactId_Level(long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artifact levelses where level = &#63;.
	*
	* @param level the level
	* @return the matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact levelses where level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact levelses where level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByLevel_First(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Returns the first artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByLevel_First(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByLevel_Last(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Returns the last artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByLevel_Last(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact levelses before and after the current artifact levels in the ordered set where level = &#63;.
	*
	* @param artifactId the primary key of the current artifact levels
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels[] findByLevel_PrevAndNext(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Removes all the artifact levelses where level = &#63; from the database.
	*
	* @param level the level
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLevel(int level)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact levelses where level = &#63;.
	*
	* @param level the level
	* @return the number of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public int countByLevel(int level)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the artifact levels in the entity cache if it is enabled.
	*
	* @param artifactLevels the artifact levels
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels);

	/**
	* Caches the artifact levelses in the entity cache if it is enabled.
	*
	* @param artifactLevelses the artifact levelses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> artifactLevelses);

	/**
	* Creates a new artifact levels with the primary key. Does not add the artifact levels to the database.
	*
	* @param artifactId the primary key for the new artifact levels
	* @return the new artifact levels
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels create(
		long artifactId);

	/**
	* Removes the artifact levels with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels remove(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact levels with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException} if it could not be found.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByPrimaryKey(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException;

	/**
	* Returns the artifact levels with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels, or <code>null</code> if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByPrimaryKey(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artifact levelses.
	*
	* @return the artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact levelses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact levelses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the artifact levelses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact levelses.
	*
	* @return the number of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}