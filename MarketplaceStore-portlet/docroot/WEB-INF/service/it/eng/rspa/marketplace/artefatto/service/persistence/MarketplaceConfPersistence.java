/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;

/**
 * The persistence interface for the marketplace conf service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see MarketplaceConfPersistenceImpl
 * @see MarketplaceConfUtil
 * @generated
 */
public interface MarketplaceConfPersistence extends BasePersistence<MarketplaceConf> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MarketplaceConfUtil} to access the marketplace conf persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the marketplace conf where key = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	*
	* @param key the key
	* @return the matching marketplace conf
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf findByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException;

	/**
	* Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param key the key
	* @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param key the key
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByKey(
		java.lang.String key, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the marketplace conf where key = &#63; from the database.
	*
	* @param key the key
	* @return the marketplace conf that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf removeByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException;

	/**
	* Returns the number of marketplace confs where key = &#63;.
	*
	* @param key the key
	* @return the number of matching marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public int countByKey(java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the marketplace conf in the entity cache if it is enabled.
	*
	* @param marketplaceConf the marketplace conf
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf);

	/**
	* Caches the marketplace confs in the entity cache if it is enabled.
	*
	* @param marketplaceConfs the marketplace confs
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> marketplaceConfs);

	/**
	* Creates a new marketplace conf with the primary key. Does not add the marketplace conf to the database.
	*
	* @param confId the primary key for the new marketplace conf
	* @return the new marketplace conf
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf create(
		long confId);

	/**
	* Removes the marketplace conf with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf remove(
		long confId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException;

	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf updateImpl(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the marketplace conf with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf findByPrimaryKey(
		long confId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException;

	/**
	* Returns the marketplace conf with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf, or <code>null</code> if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByPrimaryKey(
		long confId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the marketplace confs.
	*
	* @return the marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the marketplace confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of marketplace confs
	* @param end the upper bound of the range of marketplace confs (not inclusive)
	* @return the range of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the marketplace confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of marketplace confs
	* @param end the upper bound of the range of marketplace confs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the marketplace confs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of marketplace confs.
	*
	* @return the number of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}