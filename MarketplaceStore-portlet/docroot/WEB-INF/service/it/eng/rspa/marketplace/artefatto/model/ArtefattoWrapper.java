/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Artefatto}.
 * </p>
 *
 * @author eng
 * @see Artefatto
 * @generated
 */
public class ArtefattoWrapper implements Artefatto, ModelWrapper<Artefatto> {
	public ArtefattoWrapper(Artefatto artefatto) {
		_artefatto = artefatto;
	}

	@Override
	public Class<?> getModelClass() {
		return Artefatto.class;
	}

	@Override
	public String getModelClassName() {
		return Artefatto.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("artefattoId", getArtefattoId());
		attributes.put("date", getDate());
		attributes.put("title", getTitle());
		attributes.put("webpage", getWebpage());
		attributes.put("abstractDescription", getAbstractDescription());
		attributes.put("description", getDescription());
		attributes.put("providerName", getProviderName());
		attributes.put("resourceRDF", getResourceRDF());
		attributes.put("repositoryRDF", getRepositoryRDF());
		attributes.put("status", getStatus());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("owner", getOwner());
		attributes.put("imgId", getImgId());
		attributes.put("categoriamkpId", getCategoriamkpId());
		attributes.put("pilotid", getPilotid());
		attributes.put("url", getUrl());
		attributes.put("eId", getEId());
		attributes.put("extRating", getExtRating());
		attributes.put("interactionPoint", getInteractionPoint());
		attributes.put("language", getLanguage());
		attributes.put("hasMapping", getHasMapping());
		attributes.put("isPrivate", getIsPrivate());
		attributes.put("lusdlmodel", getLusdlmodel());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long artefattoId = (Long)attributes.get("artefattoId");

		if (artefattoId != null) {
			setArtefattoId(artefattoId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String webpage = (String)attributes.get("webpage");

		if (webpage != null) {
			setWebpage(webpage);
		}

		String abstractDescription = (String)attributes.get(
				"abstractDescription");

		if (abstractDescription != null) {
			setAbstractDescription(abstractDescription);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String providerName = (String)attributes.get("providerName");

		if (providerName != null) {
			setProviderName(providerName);
		}

		String resourceRDF = (String)attributes.get("resourceRDF");

		if (resourceRDF != null) {
			setResourceRDF(resourceRDF);
		}

		String repositoryRDF = (String)attributes.get("repositoryRDF");

		if (repositoryRDF != null) {
			setRepositoryRDF(repositoryRDF);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String owner = (String)attributes.get("owner");

		if (owner != null) {
			setOwner(owner);
		}

		Long imgId = (Long)attributes.get("imgId");

		if (imgId != null) {
			setImgId(imgId);
		}

		Long categoriamkpId = (Long)attributes.get("categoriamkpId");

		if (categoriamkpId != null) {
			setCategoriamkpId(categoriamkpId);
		}

		String pilotid = (String)attributes.get("pilotid");

		if (pilotid != null) {
			setPilotid(pilotid);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String eId = (String)attributes.get("eId");

		if (eId != null) {
			setEId(eId);
		}

		Integer extRating = (Integer)attributes.get("extRating");

		if (extRating != null) {
			setExtRating(extRating);
		}

		String interactionPoint = (String)attributes.get("interactionPoint");

		if (interactionPoint != null) {
			setInteractionPoint(interactionPoint);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		Boolean hasMapping = (Boolean)attributes.get("hasMapping");

		if (hasMapping != null) {
			setHasMapping(hasMapping);
		}

		Boolean isPrivate = (Boolean)attributes.get("isPrivate");

		if (isPrivate != null) {
			setIsPrivate(isPrivate);
		}

		String lusdlmodel = (String)attributes.get("lusdlmodel");

		if (lusdlmodel != null) {
			setLusdlmodel(lusdlmodel);
		}
	}

	/**
	* Returns the primary key of this artefatto.
	*
	* @return the primary key of this artefatto
	*/
	@Override
	public long getPrimaryKey() {
		return _artefatto.getPrimaryKey();
	}

	/**
	* Sets the primary key of this artefatto.
	*
	* @param primaryKey the primary key of this artefatto
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_artefatto.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this artefatto.
	*
	* @return the uuid of this artefatto
	*/
	@Override
	public java.lang.String getUuid() {
		return _artefatto.getUuid();
	}

	/**
	* Sets the uuid of this artefatto.
	*
	* @param uuid the uuid of this artefatto
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_artefatto.setUuid(uuid);
	}

	/**
	* Returns the artefatto ID of this artefatto.
	*
	* @return the artefatto ID of this artefatto
	*/
	@Override
	public long getArtefattoId() {
		return _artefatto.getArtefattoId();
	}

	/**
	* Sets the artefatto ID of this artefatto.
	*
	* @param artefattoId the artefatto ID of this artefatto
	*/
	@Override
	public void setArtefattoId(long artefattoId) {
		_artefatto.setArtefattoId(artefattoId);
	}

	/**
	* Returns the date of this artefatto.
	*
	* @return the date of this artefatto
	*/
	@Override
	public java.util.Date getDate() {
		return _artefatto.getDate();
	}

	/**
	* Sets the date of this artefatto.
	*
	* @param date the date of this artefatto
	*/
	@Override
	public void setDate(java.util.Date date) {
		_artefatto.setDate(date);
	}

	/**
	* Returns the title of this artefatto.
	*
	* @return the title of this artefatto
	*/
	@Override
	public java.lang.String getTitle() {
		return _artefatto.getTitle();
	}

	/**
	* Sets the title of this artefatto.
	*
	* @param title the title of this artefatto
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_artefatto.setTitle(title);
	}

	/**
	* Returns the webpage of this artefatto.
	*
	* @return the webpage of this artefatto
	*/
	@Override
	public java.lang.String getWebpage() {
		return _artefatto.getWebpage();
	}

	/**
	* Sets the webpage of this artefatto.
	*
	* @param webpage the webpage of this artefatto
	*/
	@Override
	public void setWebpage(java.lang.String webpage) {
		_artefatto.setWebpage(webpage);
	}

	/**
	* Returns the abstract description of this artefatto.
	*
	* @return the abstract description of this artefatto
	*/
	@Override
	public java.lang.String getAbstractDescription() {
		return _artefatto.getAbstractDescription();
	}

	/**
	* Sets the abstract description of this artefatto.
	*
	* @param abstractDescription the abstract description of this artefatto
	*/
	@Override
	public void setAbstractDescription(java.lang.String abstractDescription) {
		_artefatto.setAbstractDescription(abstractDescription);
	}

	/**
	* Returns the description of this artefatto.
	*
	* @return the description of this artefatto
	*/
	@Override
	public java.lang.String getDescription() {
		return _artefatto.getDescription();
	}

	/**
	* Sets the description of this artefatto.
	*
	* @param description the description of this artefatto
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_artefatto.setDescription(description);
	}

	/**
	* Returns the provider name of this artefatto.
	*
	* @return the provider name of this artefatto
	*/
	@Override
	public java.lang.String getProviderName() {
		return _artefatto.getProviderName();
	}

	/**
	* Sets the provider name of this artefatto.
	*
	* @param providerName the provider name of this artefatto
	*/
	@Override
	public void setProviderName(java.lang.String providerName) {
		_artefatto.setProviderName(providerName);
	}

	/**
	* Returns the resource r d f of this artefatto.
	*
	* @return the resource r d f of this artefatto
	*/
	@Override
	public java.lang.String getResourceRDF() {
		return _artefatto.getResourceRDF();
	}

	/**
	* Sets the resource r d f of this artefatto.
	*
	* @param resourceRDF the resource r d f of this artefatto
	*/
	@Override
	public void setResourceRDF(java.lang.String resourceRDF) {
		_artefatto.setResourceRDF(resourceRDF);
	}

	/**
	* Returns the repository r d f of this artefatto.
	*
	* @return the repository r d f of this artefatto
	*/
	@Override
	public java.lang.String getRepositoryRDF() {
		return _artefatto.getRepositoryRDF();
	}

	/**
	* Sets the repository r d f of this artefatto.
	*
	* @param repositoryRDF the repository r d f of this artefatto
	*/
	@Override
	public void setRepositoryRDF(java.lang.String repositoryRDF) {
		_artefatto.setRepositoryRDF(repositoryRDF);
	}

	/**
	* Returns the status of this artefatto.
	*
	* @return the status of this artefatto
	*/
	@Override
	public int getStatus() {
		return _artefatto.getStatus();
	}

	/**
	* Sets the status of this artefatto.
	*
	* @param status the status of this artefatto
	*/
	@Override
	public void setStatus(int status) {
		_artefatto.setStatus(status);
	}

	/**
	* Returns the company ID of this artefatto.
	*
	* @return the company ID of this artefatto
	*/
	@Override
	public long getCompanyId() {
		return _artefatto.getCompanyId();
	}

	/**
	* Sets the company ID of this artefatto.
	*
	* @param companyId the company ID of this artefatto
	*/
	@Override
	public void setCompanyId(long companyId) {
		_artefatto.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this artefatto.
	*
	* @return the group ID of this artefatto
	*/
	@Override
	public long getGroupId() {
		return _artefatto.getGroupId();
	}

	/**
	* Sets the group ID of this artefatto.
	*
	* @param groupId the group ID of this artefatto
	*/
	@Override
	public void setGroupId(long groupId) {
		_artefatto.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this artefatto.
	*
	* @return the user ID of this artefatto
	*/
	@Override
	public long getUserId() {
		return _artefatto.getUserId();
	}

	/**
	* Sets the user ID of this artefatto.
	*
	* @param userId the user ID of this artefatto
	*/
	@Override
	public void setUserId(long userId) {
		_artefatto.setUserId(userId);
	}

	/**
	* Returns the user uuid of this artefatto.
	*
	* @return the user uuid of this artefatto
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _artefatto.getUserUuid();
	}

	/**
	* Sets the user uuid of this artefatto.
	*
	* @param userUuid the user uuid of this artefatto
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_artefatto.setUserUuid(userUuid);
	}

	/**
	* Returns the owner of this artefatto.
	*
	* @return the owner of this artefatto
	*/
	@Override
	public java.lang.String getOwner() {
		return _artefatto.getOwner();
	}

	/**
	* Sets the owner of this artefatto.
	*
	* @param owner the owner of this artefatto
	*/
	@Override
	public void setOwner(java.lang.String owner) {
		_artefatto.setOwner(owner);
	}

	/**
	* Returns the img ID of this artefatto.
	*
	* @return the img ID of this artefatto
	*/
	@Override
	public long getImgId() {
		return _artefatto.getImgId();
	}

	/**
	* Sets the img ID of this artefatto.
	*
	* @param imgId the img ID of this artefatto
	*/
	@Override
	public void setImgId(long imgId) {
		_artefatto.setImgId(imgId);
	}

	/**
	* Returns the categoriamkp ID of this artefatto.
	*
	* @return the categoriamkp ID of this artefatto
	*/
	@Override
	public long getCategoriamkpId() {
		return _artefatto.getCategoriamkpId();
	}

	/**
	* Sets the categoriamkp ID of this artefatto.
	*
	* @param categoriamkpId the categoriamkp ID of this artefatto
	*/
	@Override
	public void setCategoriamkpId(long categoriamkpId) {
		_artefatto.setCategoriamkpId(categoriamkpId);
	}

	/**
	* Returns the pilotid of this artefatto.
	*
	* @return the pilotid of this artefatto
	*/
	@Override
	public java.lang.String getPilotid() {
		return _artefatto.getPilotid();
	}

	/**
	* Sets the pilotid of this artefatto.
	*
	* @param pilotid the pilotid of this artefatto
	*/
	@Override
	public void setPilotid(java.lang.String pilotid) {
		_artefatto.setPilotid(pilotid);
	}

	/**
	* Returns the url of this artefatto.
	*
	* @return the url of this artefatto
	*/
	@Override
	public java.lang.String getUrl() {
		return _artefatto.getUrl();
	}

	/**
	* Sets the url of this artefatto.
	*
	* @param url the url of this artefatto
	*/
	@Override
	public void setUrl(java.lang.String url) {
		_artefatto.setUrl(url);
	}

	/**
	* Returns the e ID of this artefatto.
	*
	* @return the e ID of this artefatto
	*/
	@Override
	public java.lang.String getEId() {
		return _artefatto.getEId();
	}

	/**
	* Sets the e ID of this artefatto.
	*
	* @param eId the e ID of this artefatto
	*/
	@Override
	public void setEId(java.lang.String eId) {
		_artefatto.setEId(eId);
	}

	/**
	* Returns the ext rating of this artefatto.
	*
	* @return the ext rating of this artefatto
	*/
	@Override
	public int getExtRating() {
		return _artefatto.getExtRating();
	}

	/**
	* Sets the ext rating of this artefatto.
	*
	* @param extRating the ext rating of this artefatto
	*/
	@Override
	public void setExtRating(int extRating) {
		_artefatto.setExtRating(extRating);
	}

	/**
	* Returns the interaction point of this artefatto.
	*
	* @return the interaction point of this artefatto
	*/
	@Override
	public java.lang.String getInteractionPoint() {
		return _artefatto.getInteractionPoint();
	}

	/**
	* Sets the interaction point of this artefatto.
	*
	* @param interactionPoint the interaction point of this artefatto
	*/
	@Override
	public void setInteractionPoint(java.lang.String interactionPoint) {
		_artefatto.setInteractionPoint(interactionPoint);
	}

	/**
	* Returns the language of this artefatto.
	*
	* @return the language of this artefatto
	*/
	@Override
	public java.lang.String getLanguage() {
		return _artefatto.getLanguage();
	}

	/**
	* Sets the language of this artefatto.
	*
	* @param language the language of this artefatto
	*/
	@Override
	public void setLanguage(java.lang.String language) {
		_artefatto.setLanguage(language);
	}

	/**
	* Returns the has mapping of this artefatto.
	*
	* @return the has mapping of this artefatto
	*/
	@Override
	public boolean getHasMapping() {
		return _artefatto.getHasMapping();
	}

	/**
	* Returns <code>true</code> if this artefatto is has mapping.
	*
	* @return <code>true</code> if this artefatto is has mapping; <code>false</code> otherwise
	*/
	@Override
	public boolean isHasMapping() {
		return _artefatto.isHasMapping();
	}

	/**
	* Sets whether this artefatto is has mapping.
	*
	* @param hasMapping the has mapping of this artefatto
	*/
	@Override
	public void setHasMapping(boolean hasMapping) {
		_artefatto.setHasMapping(hasMapping);
	}

	/**
	* Returns the is private of this artefatto.
	*
	* @return the is private of this artefatto
	*/
	@Override
	public boolean getIsPrivate() {
		return _artefatto.getIsPrivate();
	}

	/**
	* Returns <code>true</code> if this artefatto is is private.
	*
	* @return <code>true</code> if this artefatto is is private; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsPrivate() {
		return _artefatto.isIsPrivate();
	}

	/**
	* Sets whether this artefatto is is private.
	*
	* @param isPrivate the is private of this artefatto
	*/
	@Override
	public void setIsPrivate(boolean isPrivate) {
		_artefatto.setIsPrivate(isPrivate);
	}

	/**
	* Returns the lusdlmodel of this artefatto.
	*
	* @return the lusdlmodel of this artefatto
	*/
	@Override
	public java.lang.String getLusdlmodel() {
		return _artefatto.getLusdlmodel();
	}

	/**
	* Sets the lusdlmodel of this artefatto.
	*
	* @param lusdlmodel the lusdlmodel of this artefatto
	*/
	@Override
	public void setLusdlmodel(java.lang.String lusdlmodel) {
		_artefatto.setLusdlmodel(lusdlmodel);
	}

	@Override
	public boolean isNew() {
		return _artefatto.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_artefatto.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _artefatto.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_artefatto.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _artefatto.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _artefatto.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_artefatto.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _artefatto.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_artefatto.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_artefatto.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_artefatto.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ArtefattoWrapper((Artefatto)_artefatto.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto) {
		return _artefatto.compareTo(artefatto);
	}

	@Override
	public int hashCode() {
		return _artefatto.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.Artefatto> toCacheModel() {
		return _artefatto.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto toEscapedModel() {
		return new ArtefattoWrapper(_artefatto.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto toUnescapedModel() {
		return new ArtefattoWrapper(_artefatto.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _artefatto.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _artefatto.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_artefatto.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtefattoWrapper)) {
			return false;
		}

		ArtefattoWrapper artefattoWrapper = (ArtefattoWrapper)obj;

		if (Validator.equals(_artefatto, artefattoWrapper._artefatto)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Artefatto getWrappedArtefatto() {
		return _artefatto;
	}

	@Override
	public Artefatto getWrappedModel() {
		return _artefatto;
	}

	@Override
	public void resetOriginalValues() {
		_artefatto.resetOriginalValues();
	}

	private Artefatto _artefatto;
}