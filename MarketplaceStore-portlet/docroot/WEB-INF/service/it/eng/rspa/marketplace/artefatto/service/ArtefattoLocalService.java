/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * Provides the local service interface for Artefatto. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author eng
 * @see ArtefattoLocalServiceUtil
 * @see it.eng.rspa.marketplace.artefatto.service.base.ArtefattoLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.impl.ArtefattoLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ArtefattoLocalService extends BaseLocalService,
	InvokableLocalService, PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ArtefattoLocalServiceUtil} to access the artefatto local service. Add custom service methods to {@link it.eng.rspa.marketplace.artefatto.service.impl.ArtefattoLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the artefatto to the database. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was added
	* @throws SystemException if a system exception occurred
	*/
	@com.liferay.portal.kernel.search.Indexable(type = IndexableType.REINDEX)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Creates a new artefatto with the primary key. Does not add the artefatto to the database.
	*
	* @param artefattoId the primary key for the new artefatto
	* @return the new artefatto
	*/
	public it.eng.rspa.marketplace.artefatto.model.Artefatto createArtefatto(
		long artefattoId);

	/**
	* Deletes the artefatto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto that was removed
	* @throws PortalException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@com.liferay.portal.kernel.search.Indexable(type = IndexableType.DELETE)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Deletes the artefatto from the database. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was removed
	* @throws SystemException if a system exception occurred
	*/
	@com.liferay.portal.kernel.search.Indexable(type = IndexableType.DELETE)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException;

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto with the matching UUID and company.
	*
	* @param uuid the artefatto's UUID
	* @param companyId the primary key of the company
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto matching the UUID and group.
	*
	* @param uuid the artefatto's UUID
	* @param groupId the primary key of the group
	* @return the matching artefatto, or <code>null</code> if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto with the primary key.
	*
	* @param artefattoId the primary key of the artefatto
	* @return the artefatto
	* @throws PortalException if a artefatto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto with the matching UUID and company.
	*
	* @param uuid the artefatto's UUID
	* @param companyId the primary key of the company
	* @return the matching artefatto
	* @throws PortalException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artefatto matching the UUID and group.
	*
	* @param uuid the artefatto's UUID
	* @param groupId the primary key of the group
	* @return the matching artefatto
	* @throws PortalException if a matching artefatto could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artefattos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artefattos
	* @param end the upper bound of the range of artefattos (not inclusive)
	* @return the range of artefattos
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artefattos.
	*
	* @return the number of artefattos
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getArtefattosCount()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Updates the artefatto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param artefatto the artefatto
	* @return the artefatto that was updated
	* @throws SystemException if a system exception occurred
	*/
	@com.liferay.portal.kernel.search.Indexable(type = IndexableType.REINDEX)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto updateArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	public it.eng.rspa.marketplace.artefatto.model.Artefatto publishArtefatto(
		long idArtefatto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, java.lang.String[] tags)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	public it.eng.rspa.marketplace.artefatto.model.Artefatto permanentDeleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status, java.lang.String orderByCampo,
		java.lang.String orderType);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByDescriptionRDF(
		java.lang.String description);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByResourceRDF(
		java.lang.String resourceRDF);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByTitle(
		java.lang.String title);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status, java.lang.String orderByCampo, java.lang.String orderType);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByUserId_Status(
		long userId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException;

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory_Status(
		long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException;

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByPilot_Category_Status(
		java.lang.String pilot, long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException;

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo, boolean isAsc);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo,
		java.lang.String orderType);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category, int start, int end);

	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattiByIds(
		java.util.List<java.lang.Long> ids);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByKeyword(
		java.lang.String keyword);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId, int startId, int endId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByKeyword(
		java.lang.String pilotId, java.lang.String keyword);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByLangStatus(
		java.lang.String lang, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot, int starId, int endId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByKeyword(
		java.lang.String pilot, java.lang.String keyword);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByLangStatus(
		java.lang.String lang, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot, int startId, int endId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByKeyword(
		java.lang.String pilot, java.lang.String keyword);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByLangStatus(
		java.lang.String lang, int status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefactByExternalId(
		java.lang.String eid) throws java.lang.Exception;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByPilot(
		java.lang.String pilot, int maxNumber);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByLanguage(
		java.lang.String language, int maxNumber);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONArray getArtefactsByUserId(
		long liferayUserId, int maxNumber);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwner(
		java.lang.String owner);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwnerAndStatus(
		java.lang.String owner, int status);

	public com.liferay.portal.kernel.json.JSONObject forget(
		long liferayUserId, boolean removecontent) throws java.lang.Exception;
}