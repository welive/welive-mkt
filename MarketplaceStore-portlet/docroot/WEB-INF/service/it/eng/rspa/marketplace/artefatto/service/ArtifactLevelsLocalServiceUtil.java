/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ArtifactLevels. This utility wraps
 * {@link it.eng.rspa.marketplace.artefatto.service.impl.ArtifactLevelsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author eng
 * @see ArtifactLevelsLocalService
 * @see it.eng.rspa.marketplace.artefatto.service.base.ArtifactLevelsLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.impl.ArtifactLevelsLocalServiceImpl
 * @generated
 */
public class ArtifactLevelsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.marketplace.artefatto.service.impl.ArtifactLevelsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the artifact levels to the database. Also notifies the appropriate model listeners.
	*
	* @param artifactLevels the artifact levels
	* @return the artifact levels that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels addArtifactLevels(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addArtifactLevels(artifactLevels);
	}

	/**
	* Creates a new artifact levels with the primary key. Does not add the artifact levels to the database.
	*
	* @param artifactId the primary key for the new artifact levels
	* @return the new artifact levels
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels createArtifactLevels(
		long artifactId) {
		return getService().createArtifactLevels(artifactId);
	}

	/**
	* Deletes the artifact levels with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels that was removed
	* @throws PortalException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels deleteArtifactLevels(
		long artifactId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteArtifactLevels(artifactId);
	}

	/**
	* Deletes the artifact levels from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactLevels the artifact levels
	* @return the artifact levels that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels deleteArtifactLevels(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteArtifactLevels(artifactLevels);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchArtifactLevels(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchArtifactLevels(artifactId);
	}

	/**
	* Returns the artifact levels with the primary key.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels
	* @throws PortalException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels getArtifactLevels(
		long artifactId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactLevels(artifactId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the artifact levelses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> getArtifactLevelses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactLevelses(start, end);
	}

	/**
	* Returns the number of artifact levelses.
	*
	* @return the number of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static int getArtifactLevelsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactLevelsesCount();
	}

	/**
	* Updates the artifact levels in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param artifactLevels the artifact levels
	* @return the artifact levels that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels updateArtifactLevels(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateArtifactLevels(artifactLevels);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels addArtifactLevels(
		long artifactId, int level, java.lang.String notes)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().addArtifactLevels(artifactId, level, notes);
	}

	public static java.util.List<java.lang.Long> getArtifactLevelsByLevel(
		int level) {
		return getService().getArtifactLevelsByLevel(level);
	}

	public static void clearService() {
		_service = null;
	}

	public static ArtifactLevelsLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ArtifactLevelsLocalService.class.getName());

			if (invokableLocalService instanceof ArtifactLevelsLocalService) {
				_service = (ArtifactLevelsLocalService)invokableLocalService;
			}
			else {
				_service = new ArtifactLevelsLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ArtifactLevelsLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ArtifactLevelsLocalService service) {
	}

	private static ArtifactLevelsLocalService _service;
}