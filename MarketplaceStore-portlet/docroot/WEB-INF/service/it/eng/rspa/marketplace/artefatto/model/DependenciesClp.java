/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.DependenciesLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class DependenciesClp extends BaseModelImpl<Dependencies>
	implements Dependencies {
	public DependenciesClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Dependencies.class;
	}

	@Override
	public String getModelClassName() {
		return Dependencies.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _dependencyId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setDependencyId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _dependencyId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dependencyId", getDependencyId());
		attributes.put("artefactId", getArtefactId());
		attributes.put("dependsFrom", getDependsFrom());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long dependencyId = (Long)attributes.get("dependencyId");

		if (dependencyId != null) {
			setDependencyId(dependencyId);
		}

		Long artefactId = (Long)attributes.get("artefactId");

		if (artefactId != null) {
			setArtefactId(artefactId);
		}

		Long dependsFrom = (Long)attributes.get("dependsFrom");

		if (dependsFrom != null) {
			setDependsFrom(dependsFrom);
		}
	}

	@Override
	public long getDependencyId() {
		return _dependencyId;
	}

	@Override
	public void setDependencyId(long dependencyId) {
		_dependencyId = dependencyId;

		if (_dependenciesRemoteModel != null) {
			try {
				Class<?> clazz = _dependenciesRemoteModel.getClass();

				Method method = clazz.getMethod("setDependencyId", long.class);

				method.invoke(_dependenciesRemoteModel, dependencyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtefactId() {
		return _artefactId;
	}

	@Override
	public void setArtefactId(long artefactId) {
		_artefactId = artefactId;

		if (_dependenciesRemoteModel != null) {
			try {
				Class<?> clazz = _dependenciesRemoteModel.getClass();

				Method method = clazz.getMethod("setArtefactId", long.class);

				method.invoke(_dependenciesRemoteModel, artefactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getDependsFrom() {
		return _dependsFrom;
	}

	@Override
	public void setDependsFrom(long dependsFrom) {
		_dependsFrom = dependsFrom;

		if (_dependenciesRemoteModel != null) {
			try {
				Class<?> clazz = _dependenciesRemoteModel.getClass();

				Method method = clazz.getMethod("setDependsFrom", long.class);

				method.invoke(_dependenciesRemoteModel, dependsFrom);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDependenciesRemoteModel() {
		return _dependenciesRemoteModel;
	}

	public void setDependenciesRemoteModel(BaseModel<?> dependenciesRemoteModel) {
		_dependenciesRemoteModel = dependenciesRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _dependenciesRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_dependenciesRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DependenciesLocalServiceUtil.addDependencies(this);
		}
		else {
			DependenciesLocalServiceUtil.updateDependencies(this);
		}
	}

	@Override
	public Dependencies toEscapedModel() {
		return (Dependencies)ProxyUtil.newProxyInstance(Dependencies.class.getClassLoader(),
			new Class[] { Dependencies.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DependenciesClp clone = new DependenciesClp();

		clone.setDependencyId(getDependencyId());
		clone.setArtefactId(getArtefactId());
		clone.setDependsFrom(getDependsFrom());

		return clone;
	}

	@Override
	public int compareTo(Dependencies dependencies) {
		long primaryKey = dependencies.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DependenciesClp)) {
			return false;
		}

		DependenciesClp dependencies = (DependenciesClp)obj;

		long primaryKey = dependencies.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{dependencyId=");
		sb.append(getDependencyId());
		sb.append(", artefactId=");
		sb.append(getArtefactId());
		sb.append(", dependsFrom=");
		sb.append(getDependsFrom());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.Dependencies");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>dependencyId</column-name><column-value><![CDATA[");
		sb.append(getDependencyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artefactId</column-name><column-value><![CDATA[");
		sb.append(getArtefactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dependsFrom</column-name><column-value><![CDATA[");
		sb.append(getDependsFrom());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _dependencyId;
	private long _artefactId;
	private long _dependsFrom;
	private BaseModel<?> _dependenciesRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}