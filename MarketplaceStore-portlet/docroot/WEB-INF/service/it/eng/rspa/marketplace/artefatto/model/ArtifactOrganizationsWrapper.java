/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ArtifactOrganizations}.
 * </p>
 *
 * @author eng
 * @see ArtifactOrganizations
 * @generated
 */
public class ArtifactOrganizationsWrapper implements ArtifactOrganizations,
	ModelWrapper<ArtifactOrganizations> {
	public ArtifactOrganizationsWrapper(
		ArtifactOrganizations artifactOrganizations) {
		_artifactOrganizations = artifactOrganizations;
	}

	@Override
	public Class<?> getModelClass() {
		return ArtifactOrganizations.class;
	}

	@Override
	public String getModelClassName() {
		return ArtifactOrganizations.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("organizationId", getOrganizationId());
		attributes.put("artifactId", getArtifactId());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this artifact organizations.
	*
	* @return the primary key of this artifact organizations
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK getPrimaryKey() {
		return _artifactOrganizations.getPrimaryKey();
	}

	/**
	* Sets the primary key of this artifact organizations.
	*
	* @param primaryKey the primary key of this artifact organizations
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK primaryKey) {
		_artifactOrganizations.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the organization ID of this artifact organizations.
	*
	* @return the organization ID of this artifact organizations
	*/
	@Override
	public long getOrganizationId() {
		return _artifactOrganizations.getOrganizationId();
	}

	/**
	* Sets the organization ID of this artifact organizations.
	*
	* @param organizationId the organization ID of this artifact organizations
	*/
	@Override
	public void setOrganizationId(long organizationId) {
		_artifactOrganizations.setOrganizationId(organizationId);
	}

	/**
	* Returns the artifact ID of this artifact organizations.
	*
	* @return the artifact ID of this artifact organizations
	*/
	@Override
	public long getArtifactId() {
		return _artifactOrganizations.getArtifactId();
	}

	/**
	* Sets the artifact ID of this artifact organizations.
	*
	* @param artifactId the artifact ID of this artifact organizations
	*/
	@Override
	public void setArtifactId(long artifactId) {
		_artifactOrganizations.setArtifactId(artifactId);
	}

	/**
	* Returns the status of this artifact organizations.
	*
	* @return the status of this artifact organizations
	*/
	@Override
	public int getStatus() {
		return _artifactOrganizations.getStatus();
	}

	/**
	* Sets the status of this artifact organizations.
	*
	* @param status the status of this artifact organizations
	*/
	@Override
	public void setStatus(int status) {
		_artifactOrganizations.setStatus(status);
	}

	@Override
	public boolean isNew() {
		return _artifactOrganizations.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_artifactOrganizations.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _artifactOrganizations.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_artifactOrganizations.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _artifactOrganizations.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _artifactOrganizations.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_artifactOrganizations.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _artifactOrganizations.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_artifactOrganizations.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_artifactOrganizations.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_artifactOrganizations.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ArtifactOrganizationsWrapper((ArtifactOrganizations)_artifactOrganizations.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations) {
		return _artifactOrganizations.compareTo(artifactOrganizations);
	}

	@Override
	public int hashCode() {
		return _artifactOrganizations.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> toCacheModel() {
		return _artifactOrganizations.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations toEscapedModel() {
		return new ArtifactOrganizationsWrapper(_artifactOrganizations.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations toUnescapedModel() {
		return new ArtifactOrganizationsWrapper(_artifactOrganizations.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _artifactOrganizations.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _artifactOrganizations.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_artifactOrganizations.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtifactOrganizationsWrapper)) {
			return false;
		}

		ArtifactOrganizationsWrapper artifactOrganizationsWrapper = (ArtifactOrganizationsWrapper)obj;

		if (Validator.equals(_artifactOrganizations,
					artifactOrganizationsWrapper._artifactOrganizations)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ArtifactOrganizations getWrappedArtifactOrganizations() {
		return _artifactOrganizations;
	}

	@Override
	public ArtifactOrganizations getWrappedModel() {
		return _artifactOrganizations;
	}

	@Override
	public void resetOriginalValues() {
		_artifactOrganizations.resetOriginalValues();
	}

	private ArtifactOrganizations _artifactOrganizations;
}