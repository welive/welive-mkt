/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;

import java.util.List;

/**
 * The persistence utility for the pending notification entry service. This utility wraps {@link PendingNotificationEntryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see PendingNotificationEntryPersistence
 * @see PendingNotificationEntryPersistenceImpl
 * @generated
 */
public class PendingNotificationEntryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		PendingNotificationEntry pendingNotificationEntry) {
		getPersistence().clearCache(pendingNotificationEntry);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PendingNotificationEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PendingNotificationEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PendingNotificationEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PendingNotificationEntry update(
		PendingNotificationEntry pendingNotificationEntry)
		throws SystemException {
		return getPersistence().update(pendingNotificationEntry);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PendingNotificationEntry update(
		PendingNotificationEntry pendingNotificationEntry,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(pendingNotificationEntry, serviceContext);
	}

	/**
	* Caches the pending notification entry in the entity cache if it is enabled.
	*
	* @param pendingNotificationEntry the pending notification entry
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry) {
		getPersistence().cacheResult(pendingNotificationEntry);
	}

	/**
	* Caches the pending notification entries in the entity cache if it is enabled.
	*
	* @param pendingNotificationEntries the pending notification entries
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> pendingNotificationEntries) {
		getPersistence().cacheResult(pendingNotificationEntries);
	}

	/**
	* Creates a new pending notification entry with the primary key. Does not add the pending notification entry to the database.
	*
	* @param notificationId the primary key for the new pending notification entry
	* @return the new pending notification entry
	*/
	public static it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry create(
		long notificationId) {
		return getPersistence().create(notificationId);
	}

	/**
	* Removes the pending notification entry with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry remove(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException {
		return getPersistence().remove(notificationId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry updateImpl(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(pendingNotificationEntry);
	}

	/**
	* Returns the pending notification entry with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException} if it could not be found.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry findByPrimaryKey(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException {
		return getPersistence().findByPrimaryKey(notificationId);
	}

	/**
	* Returns the pending notification entry with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry, or <code>null</code> if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry fetchByPrimaryKey(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(notificationId);
	}

	/**
	* Returns all the pending notification entries.
	*
	* @return the pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the pending notification entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pending notification entries
	* @param end the upper bound of the range of pending notification entries (not inclusive)
	* @return the range of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the pending notification entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pending notification entries
	* @param end the upper bound of the range of pending notification entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the pending notification entries from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of pending notification entries.
	*
	* @return the number of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PendingNotificationEntryPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PendingNotificationEntryPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					PendingNotificationEntryPersistence.class.getName());

			ReferenceRegistry.registerReference(PendingNotificationEntryUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PendingNotificationEntryPersistence persistence) {
	}

	private static PendingNotificationEntryPersistence _persistence;
}