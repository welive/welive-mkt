/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.MarketplaceConf;

import java.util.List;

/**
 * The persistence utility for the marketplace conf service. This utility wraps {@link MarketplaceConfPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see MarketplaceConfPersistence
 * @see MarketplaceConfPersistenceImpl
 * @generated
 */
public class MarketplaceConfUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(MarketplaceConf marketplaceConf) {
		getPersistence().clearCache(marketplaceConf);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MarketplaceConf> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MarketplaceConf> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MarketplaceConf> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static MarketplaceConf update(MarketplaceConf marketplaceConf)
		throws SystemException {
		return getPersistence().update(marketplaceConf);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static MarketplaceConf update(MarketplaceConf marketplaceConf,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(marketplaceConf, serviceContext);
	}

	/**
	* Returns the marketplace conf where key = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	*
	* @param key the key
	* @return the matching marketplace conf
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf findByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException {
		return getPersistence().findByKey(key);
	}

	/**
	* Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param key the key
	* @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByKey(key);
	}

	/**
	* Returns the marketplace conf where key = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param key the key
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching marketplace conf, or <code>null</code> if a matching marketplace conf could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByKey(
		java.lang.String key, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByKey(key, retrieveFromCache);
	}

	/**
	* Removes the marketplace conf where key = &#63; from the database.
	*
	* @param key the key
	* @return the marketplace conf that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf removeByKey(
		java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException {
		return getPersistence().removeByKey(key);
	}

	/**
	* Returns the number of marketplace confs where key = &#63;.
	*
	* @param key the key
	* @return the number of matching marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByKey(java.lang.String key)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByKey(key);
	}

	/**
	* Caches the marketplace conf in the entity cache if it is enabled.
	*
	* @param marketplaceConf the marketplace conf
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf) {
		getPersistence().cacheResult(marketplaceConf);
	}

	/**
	* Caches the marketplace confs in the entity cache if it is enabled.
	*
	* @param marketplaceConfs the marketplace confs
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> marketplaceConfs) {
		getPersistence().cacheResult(marketplaceConfs);
	}

	/**
	* Creates a new marketplace conf with the primary key. Does not add the marketplace conf to the database.
	*
	* @param confId the primary key for the new marketplace conf
	* @return the new marketplace conf
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf create(
		long confId) {
		return getPersistence().create(confId);
	}

	/**
	* Removes the marketplace conf with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf remove(
		long confId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException {
		return getPersistence().remove(confId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf updateImpl(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(marketplaceConf);
	}

	/**
	* Returns the marketplace conf with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException} if it could not be found.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf findByPrimaryKey(
		long confId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException {
		return getPersistence().findByPrimaryKey(confId);
	}

	/**
	* Returns the marketplace conf with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param confId the primary key of the marketplace conf
	* @return the marketplace conf, or <code>null</code> if a marketplace conf with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.MarketplaceConf fetchByPrimaryKey(
		long confId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(confId);
	}

	/**
	* Returns all the marketplace confs.
	*
	* @return the marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the marketplace confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of marketplace confs
	* @param end the upper bound of the range of marketplace confs (not inclusive)
	* @return the range of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the marketplace confs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of marketplace confs
	* @param end the upper bound of the range of marketplace confs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the marketplace confs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of marketplace confs.
	*
	* @return the number of marketplace confs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static MarketplaceConfPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (MarketplaceConfPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					MarketplaceConfPersistence.class.getName());

			ReferenceRegistry.registerReference(MarketplaceConfUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(MarketplaceConfPersistence persistence) {
	}

	private static MarketplaceConfPersistence _persistence;
}