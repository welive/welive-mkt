/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.PendingNotificationEntryLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class PendingNotificationEntryClp extends BaseModelImpl<PendingNotificationEntry>
	implements PendingNotificationEntry {
	public PendingNotificationEntryClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PendingNotificationEntry.class;
	}

	@Override
	public String getModelClassName() {
		return PendingNotificationEntry.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _notificationId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setNotificationId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _notificationId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("notificationId", getNotificationId());
		attributes.put("timestamp", getTimestamp());
		attributes.put("artefactId", getArtefactId());
		attributes.put("action", getAction());
		attributes.put("targetComponent", getTargetComponent());
		attributes.put("options", getOptions());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long notificationId = (Long)attributes.get("notificationId");

		if (notificationId != null) {
			setNotificationId(notificationId);
		}

		Long timestamp = (Long)attributes.get("timestamp");

		if (timestamp != null) {
			setTimestamp(timestamp);
		}

		Long artefactId = (Long)attributes.get("artefactId");

		if (artefactId != null) {
			setArtefactId(artefactId);
		}

		String action = (String)attributes.get("action");

		if (action != null) {
			setAction(action);
		}

		String targetComponent = (String)attributes.get("targetComponent");

		if (targetComponent != null) {
			setTargetComponent(targetComponent);
		}

		String options = (String)attributes.get("options");

		if (options != null) {
			setOptions(options);
		}
	}

	@Override
	public long getNotificationId() {
		return _notificationId;
	}

	@Override
	public void setNotificationId(long notificationId) {
		_notificationId = notificationId;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setNotificationId", long.class);

				method.invoke(_pendingNotificationEntryRemoteModel,
					notificationId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTimestamp() {
		return _timestamp;
	}

	@Override
	public void setTimestamp(long timestamp) {
		_timestamp = timestamp;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setTimestamp", long.class);

				method.invoke(_pendingNotificationEntryRemoteModel, timestamp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtefactId() {
		return _artefactId;
	}

	@Override
	public void setArtefactId(long artefactId) {
		_artefactId = artefactId;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setArtefactId", long.class);

				method.invoke(_pendingNotificationEntryRemoteModel, artefactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAction() {
		return _action;
	}

	@Override
	public void setAction(String action) {
		_action = action;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setAction", String.class);

				method.invoke(_pendingNotificationEntryRemoteModel, action);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTargetComponent() {
		return _targetComponent;
	}

	@Override
	public void setTargetComponent(String targetComponent) {
		_targetComponent = targetComponent;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setTargetComponent",
						String.class);

				method.invoke(_pendingNotificationEntryRemoteModel,
					targetComponent);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOptions() {
		return _options;
	}

	@Override
	public void setOptions(String options) {
		_options = options;

		if (_pendingNotificationEntryRemoteModel != null) {
			try {
				Class<?> clazz = _pendingNotificationEntryRemoteModel.getClass();

				Method method = clazz.getMethod("setOptions", String.class);

				method.invoke(_pendingNotificationEntryRemoteModel, options);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPendingNotificationEntryRemoteModel() {
		return _pendingNotificationEntryRemoteModel;
	}

	public void setPendingNotificationEntryRemoteModel(
		BaseModel<?> pendingNotificationEntryRemoteModel) {
		_pendingNotificationEntryRemoteModel = pendingNotificationEntryRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _pendingNotificationEntryRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_pendingNotificationEntryRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PendingNotificationEntryLocalServiceUtil.addPendingNotificationEntry(this);
		}
		else {
			PendingNotificationEntryLocalServiceUtil.updatePendingNotificationEntry(this);
		}
	}

	@Override
	public PendingNotificationEntry toEscapedModel() {
		return (PendingNotificationEntry)ProxyUtil.newProxyInstance(PendingNotificationEntry.class.getClassLoader(),
			new Class[] { PendingNotificationEntry.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PendingNotificationEntryClp clone = new PendingNotificationEntryClp();

		clone.setNotificationId(getNotificationId());
		clone.setTimestamp(getTimestamp());
		clone.setArtefactId(getArtefactId());
		clone.setAction(getAction());
		clone.setTargetComponent(getTargetComponent());
		clone.setOptions(getOptions());

		return clone;
	}

	@Override
	public int compareTo(PendingNotificationEntry pendingNotificationEntry) {
		long primaryKey = pendingNotificationEntry.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PendingNotificationEntryClp)) {
			return false;
		}

		PendingNotificationEntryClp pendingNotificationEntry = (PendingNotificationEntryClp)obj;

		long primaryKey = pendingNotificationEntry.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{notificationId=");
		sb.append(getNotificationId());
		sb.append(", timestamp=");
		sb.append(getTimestamp());
		sb.append(", artefactId=");
		sb.append(getArtefactId());
		sb.append(", action=");
		sb.append(getAction());
		sb.append(", targetComponent=");
		sb.append(getTargetComponent());
		sb.append(", options=");
		sb.append(getOptions());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>notificationId</column-name><column-value><![CDATA[");
		sb.append(getNotificationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>timestamp</column-name><column-value><![CDATA[");
		sb.append(getTimestamp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artefactId</column-name><column-value><![CDATA[");
		sb.append(getArtefactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>action</column-name><column-value><![CDATA[");
		sb.append(getAction());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>targetComponent</column-name><column-value><![CDATA[");
		sb.append(getTargetComponent());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>options</column-name><column-value><![CDATA[");
		sb.append(getOptions());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _notificationId;
	private long _timestamp;
	private long _artefactId;
	private String _action;
	private String _targetComponent;
	private String _options;
	private BaseModel<?> _pendingNotificationEntryRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}