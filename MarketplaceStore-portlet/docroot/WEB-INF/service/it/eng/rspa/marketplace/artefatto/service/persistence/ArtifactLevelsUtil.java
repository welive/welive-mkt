/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.ArtifactLevels;

import java.util.List;

/**
 * The persistence utility for the artifact levels service. This utility wraps {@link ArtifactLevelsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactLevelsPersistence
 * @see ArtifactLevelsPersistenceImpl
 * @generated
 */
public class ArtifactLevelsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ArtifactLevels artifactLevels) {
		getPersistence().clearCache(artifactLevels);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ArtifactLevels> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ArtifactLevels> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ArtifactLevels> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ArtifactLevels update(ArtifactLevels artifactLevels)
		throws SystemException {
		return getPersistence().update(artifactLevels);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ArtifactLevels update(ArtifactLevels artifactLevels,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(artifactLevels, serviceContext);
	}

	/**
	* Returns all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @return the matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtifactId_Level(artifactId, level);
	}

	/**
	* Returns a range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtifactId_Level(artifactId, level, start, end);
	}

	/**
	* Returns an ordered range of all the artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByArtifactId_Level(
		long artifactId, int level, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtifactId_Level(artifactId, level, start, end,
			orderByComparator);
	}

	/**
	* Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByArtifactId_Level_First(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence()
				   .findByArtifactId_Level_First(artifactId, level,
			orderByComparator);
	}

	/**
	* Returns the first artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByArtifactId_Level_First(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtifactId_Level_First(artifactId, level,
			orderByComparator);
	}

	/**
	* Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByArtifactId_Level_Last(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence()
				   .findByArtifactId_Level_Last(artifactId, level,
			orderByComparator);
	}

	/**
	* Returns the last artifact levels in the ordered set where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByArtifactId_Level_Last(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtifactId_Level_Last(artifactId, level,
			orderByComparator);
	}

	/**
	* Removes all the artifact levelses where artifactId = &#63; and level = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByArtifactId_Level(long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByArtifactId_Level(artifactId, level);
	}

	/**
	* Returns the number of artifact levelses where artifactId = &#63; and level = &#63;.
	*
	* @param artifactId the artifact ID
	* @param level the level
	* @return the number of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByArtifactId_Level(long artifactId, int level)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByArtifactId_Level(artifactId, level);
	}

	/**
	* Returns all the artifact levelses where level = &#63;.
	*
	* @param level the level
	* @return the matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLevel(level);
	}

	/**
	* Returns a range of all the artifact levelses where level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLevel(level, start, end);
	}

	/**
	* Returns an ordered range of all the artifact levelses where level = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param level the level
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findByLevel(
		int level, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLevel(level, start, end, orderByComparator);
	}

	/**
	* Returns the first artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByLevel_First(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence().findByLevel_First(level, orderByComparator);
	}

	/**
	* Returns the first artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByLevel_First(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLevel_First(level, orderByComparator);
	}

	/**
	* Returns the last artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByLevel_Last(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence().findByLevel_Last(level, orderByComparator);
	}

	/**
	* Returns the last artifact levels in the ordered set where level = &#63;.
	*
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact levels, or <code>null</code> if a matching artifact levels could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByLevel_Last(
		int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLevel_Last(level, orderByComparator);
	}

	/**
	* Returns the artifact levelses before and after the current artifact levels in the ordered set where level = &#63;.
	*
	* @param artifactId the primary key of the current artifact levels
	* @param level the level
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels[] findByLevel_PrevAndNext(
		long artifactId, int level,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence()
				   .findByLevel_PrevAndNext(artifactId, level, orderByComparator);
	}

	/**
	* Removes all the artifact levelses where level = &#63; from the database.
	*
	* @param level the level
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLevel(int level)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLevel(level);
	}

	/**
	* Returns the number of artifact levelses where level = &#63;.
	*
	* @param level the level
	* @return the number of matching artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLevel(int level)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLevel(level);
	}

	/**
	* Caches the artifact levels in the entity cache if it is enabled.
	*
	* @param artifactLevels the artifact levels
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels) {
		getPersistence().cacheResult(artifactLevels);
	}

	/**
	* Caches the artifact levelses in the entity cache if it is enabled.
	*
	* @param artifactLevelses the artifact levelses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> artifactLevelses) {
		getPersistence().cacheResult(artifactLevelses);
	}

	/**
	* Creates a new artifact levels with the primary key. Does not add the artifact levels to the database.
	*
	* @param artifactId the primary key for the new artifact levels
	* @return the new artifact levels
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels create(
		long artifactId) {
		return getPersistence().create(artifactId);
	}

	/**
	* Removes the artifact levels with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels remove(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence().remove(artifactId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(artifactLevels);
	}

	/**
	* Returns the artifact levels with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException} if it could not be found.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels findByPrimaryKey(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException {
		return getPersistence().findByPrimaryKey(artifactId);
	}

	/**
	* Returns the artifact levels with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param artifactId the primary key of the artifact levels
	* @return the artifact levels, or <code>null</code> if a artifact levels with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactLevels fetchByPrimaryKey(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(artifactId);
	}

	/**
	* Returns all the artifact levelses.
	*
	* @return the artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the artifact levelses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @return the range of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the artifact levelses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact levelses
	* @param end the upper bound of the range of artifact levelses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the artifact levelses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of artifact levelses.
	*
	* @return the number of artifact levelses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ArtifactLevelsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ArtifactLevelsPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					ArtifactLevelsPersistence.class.getName());

			ReferenceRegistry.registerReference(ArtifactLevelsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ArtifactLevelsPersistence persistence) {
	}

	private static ArtifactLevelsPersistence _persistence;
}