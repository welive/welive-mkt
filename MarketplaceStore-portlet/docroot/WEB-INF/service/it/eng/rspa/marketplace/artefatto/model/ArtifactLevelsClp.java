/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ArtifactLevelsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class ArtifactLevelsClp extends BaseModelImpl<ArtifactLevels>
	implements ArtifactLevels {
	public ArtifactLevelsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ArtifactLevels.class;
	}

	@Override
	public String getModelClassName() {
		return ArtifactLevels.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _artifactId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setArtifactId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _artifactId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("artifactId", getArtifactId());
		attributes.put("level", getLevel());
		attributes.put("notes", getNotes());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}

		Integer level = (Integer)attributes.get("level");

		if (level != null) {
			setLevel(level);
		}

		String notes = (String)attributes.get("notes");

		if (notes != null) {
			setNotes(notes);
		}
	}

	@Override
	public long getArtifactId() {
		return _artifactId;
	}

	@Override
	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;

		if (_artifactLevelsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactLevelsRemoteModel.getClass();

				Method method = clazz.getMethod("setArtifactId", long.class);

				method.invoke(_artifactLevelsRemoteModel, artifactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getLevel() {
		return _level;
	}

	@Override
	public void setLevel(int level) {
		_level = level;

		if (_artifactLevelsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactLevelsRemoteModel.getClass();

				Method method = clazz.getMethod("setLevel", int.class);

				method.invoke(_artifactLevelsRemoteModel, level);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNotes() {
		return _notes;
	}

	@Override
	public void setNotes(String notes) {
		_notes = notes;

		if (_artifactLevelsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactLevelsRemoteModel.getClass();

				Method method = clazz.getMethod("setNotes", String.class);

				method.invoke(_artifactLevelsRemoteModel, notes);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getArtifactLevelsRemoteModel() {
		return _artifactLevelsRemoteModel;
	}

	public void setArtifactLevelsRemoteModel(
		BaseModel<?> artifactLevelsRemoteModel) {
		_artifactLevelsRemoteModel = artifactLevelsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _artifactLevelsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_artifactLevelsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ArtifactLevelsLocalServiceUtil.addArtifactLevels(this);
		}
		else {
			ArtifactLevelsLocalServiceUtil.updateArtifactLevels(this);
		}
	}

	@Override
	public ArtifactLevels toEscapedModel() {
		return (ArtifactLevels)ProxyUtil.newProxyInstance(ArtifactLevels.class.getClassLoader(),
			new Class[] { ArtifactLevels.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ArtifactLevelsClp clone = new ArtifactLevelsClp();

		clone.setArtifactId(getArtifactId());
		clone.setLevel(getLevel());
		clone.setNotes(getNotes());

		return clone;
	}

	@Override
	public int compareTo(ArtifactLevels artifactLevels) {
		long primaryKey = artifactLevels.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtifactLevelsClp)) {
			return false;
		}

		ArtifactLevelsClp artifactLevels = (ArtifactLevelsClp)obj;

		long primaryKey = artifactLevels.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{artifactId=");
		sb.append(getArtifactId());
		sb.append(", level=");
		sb.append(getLevel());
		sb.append(", notes=");
		sb.append(getNotes());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.ArtifactLevels");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>artifactId</column-name><column-value><![CDATA[");
		sb.append(getArtifactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>level</column-name><column-value><![CDATA[");
		sb.append(getLevel());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>notes</column-name><column-value><![CDATA[");
		sb.append(getNotes());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _artifactId;
	private int _level;
	private String _notes;
	private BaseModel<?> _artifactLevelsRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}