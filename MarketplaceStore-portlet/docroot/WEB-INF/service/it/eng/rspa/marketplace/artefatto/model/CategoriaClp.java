/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class CategoriaClp extends BaseModelImpl<Categoria> implements Categoria {
	public CategoriaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Categoria.class;
	}

	@Override
	public String getModelClassName() {
		return Categoria.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idCategoria;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdCategoria(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idCategoria;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCategoria", getIdCategoria());
		attributes.put("nomeCategoria", getNomeCategoria());
		attributes.put("supports", getSupports());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idCategoria = (Long)attributes.get("idCategoria");

		if (idCategoria != null) {
			setIdCategoria(idCategoria);
		}

		String nomeCategoria = (String)attributes.get("nomeCategoria");

		if (nomeCategoria != null) {
			setNomeCategoria(nomeCategoria);
		}

		String supports = (String)attributes.get("supports");

		if (supports != null) {
			setSupports(supports);
		}
	}

	@Override
	public long getIdCategoria() {
		return _idCategoria;
	}

	@Override
	public void setIdCategoria(long idCategoria) {
		_idCategoria = idCategoria;

		if (_categoriaRemoteModel != null) {
			try {
				Class<?> clazz = _categoriaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdCategoria", long.class);

				method.invoke(_categoriaRemoteModel, idCategoria);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNomeCategoria() {
		return _nomeCategoria;
	}

	@Override
	public void setNomeCategoria(String nomeCategoria) {
		_nomeCategoria = nomeCategoria;

		if (_categoriaRemoteModel != null) {
			try {
				Class<?> clazz = _categoriaRemoteModel.getClass();

				Method method = clazz.getMethod("setNomeCategoria", String.class);

				method.invoke(_categoriaRemoteModel, nomeCategoria);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSupports() {
		return _supports;
	}

	@Override
	public void setSupports(String supports) {
		_supports = supports;

		if (_categoriaRemoteModel != null) {
			try {
				Class<?> clazz = _categoriaRemoteModel.getClass();

				Method method = clazz.getMethod("setSupports", String.class);

				method.invoke(_categoriaRemoteModel, supports);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCategoriaRemoteModel() {
		return _categoriaRemoteModel;
	}

	public void setCategoriaRemoteModel(BaseModel<?> categoriaRemoteModel) {
		_categoriaRemoteModel = categoriaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _categoriaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_categoriaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CategoriaLocalServiceUtil.addCategoria(this);
		}
		else {
			CategoriaLocalServiceUtil.updateCategoria(this);
		}
	}

	@Override
	public Categoria toEscapedModel() {
		return (Categoria)ProxyUtil.newProxyInstance(Categoria.class.getClassLoader(),
			new Class[] { Categoria.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CategoriaClp clone = new CategoriaClp();

		clone.setIdCategoria(getIdCategoria());
		clone.setNomeCategoria(getNomeCategoria());
		clone.setSupports(getSupports());

		return clone;
	}

	@Override
	public int compareTo(Categoria categoria) {
		int value = 0;

		value = getNomeCategoria().compareTo(categoria.getNomeCategoria());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CategoriaClp)) {
			return false;
		}

		CategoriaClp categoria = (CategoriaClp)obj;

		long primaryKey = categoria.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idCategoria=");
		sb.append(getIdCategoria());
		sb.append(", nomeCategoria=");
		sb.append(getNomeCategoria());
		sb.append(", supports=");
		sb.append(getSupports());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.Categoria");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idCategoria</column-name><column-value><![CDATA[");
		sb.append(getIdCategoria());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nomeCategoria</column-name><column-value><![CDATA[");
		sb.append(getNomeCategoria());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>supports</column-name><column-value><![CDATA[");
		sb.append(getSupports());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idCategoria;
	private String _nomeCategoria;
	private String _supports;
	private BaseModel<?> _categoriaRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}