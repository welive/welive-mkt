/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry;

/**
 * The persistence interface for the pending notification entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see PendingNotificationEntryPersistenceImpl
 * @see PendingNotificationEntryUtil
 * @generated
 */
public interface PendingNotificationEntryPersistence extends BasePersistence<PendingNotificationEntry> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PendingNotificationEntryUtil} to access the pending notification entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the pending notification entry in the entity cache if it is enabled.
	*
	* @param pendingNotificationEntry the pending notification entry
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry);

	/**
	* Caches the pending notification entries in the entity cache if it is enabled.
	*
	* @param pendingNotificationEntries the pending notification entries
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> pendingNotificationEntries);

	/**
	* Creates a new pending notification entry with the primary key. Does not add the pending notification entry to the database.
	*
	* @param notificationId the primary key for the new pending notification entry
	* @return the new pending notification entry
	*/
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry create(
		long notificationId);

	/**
	* Removes the pending notification entry with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry remove(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException;

	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry updateImpl(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the pending notification entry with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException} if it could not be found.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry findByPrimaryKey(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException;

	/**
	* Returns the pending notification entry with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry, or <code>null</code> if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry fetchByPrimaryKey(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the pending notification entries.
	*
	* @return the pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the pending notification entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pending notification entries
	* @param end the upper bound of the range of pending notification entries (not inclusive)
	* @return the range of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the pending notification entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pending notification entries
	* @param end the upper bound of the range of pending notification entries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the pending notification entries from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of pending notification entries.
	*
	* @return the number of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}