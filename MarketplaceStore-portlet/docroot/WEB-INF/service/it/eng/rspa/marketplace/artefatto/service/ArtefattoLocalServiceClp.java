/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.InvokableLocalService;

/**
 * @author eng
 * @generated
 */
public class ArtefattoLocalServiceClp implements ArtefattoLocalService {
	public ArtefattoLocalServiceClp(InvokableLocalService invokableLocalService) {
		_invokableLocalService = invokableLocalService;

		_methodName0 = "addArtefatto";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName1 = "createArtefatto";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteArtefatto";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteArtefatto";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchArtefatto";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "fetchArtefattoByUuidAndCompanyId";

		_methodParameterTypes11 = new String[] { "java.lang.String", "long" };

		_methodName12 = "fetchArtefattoByUuidAndGroupId";

		_methodParameterTypes12 = new String[] { "java.lang.String", "long" };

		_methodName13 = "getArtefatto";

		_methodParameterTypes13 = new String[] { "long" };

		_methodName14 = "getPersistedModel";

		_methodParameterTypes14 = new String[] { "java.io.Serializable" };

		_methodName15 = "getArtefattoByUuidAndCompanyId";

		_methodParameterTypes15 = new String[] { "java.lang.String", "long" };

		_methodName16 = "getArtefattoByUuidAndGroupId";

		_methodParameterTypes16 = new String[] { "java.lang.String", "long" };

		_methodName17 = "getArtefattos";

		_methodParameterTypes17 = new String[] { "int", "int" };

		_methodName18 = "getArtefattosCount";

		_methodParameterTypes18 = new String[] {  };

		_methodName19 = "updateArtefatto";

		_methodParameterTypes19 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName20 = "getBeanIdentifier";

		_methodParameterTypes20 = new String[] {  };

		_methodName21 = "setBeanIdentifier";

		_methodParameterTypes21 = new String[] { "java.lang.String" };

		_methodName23 = "publishArtefatto";

		_methodParameterTypes23 = new String[] { "long" };

		_methodName24 = "addArtefatto";

		_methodParameterTypes24 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto", "long",
				"java.lang.String[][]"
			};

		_methodName25 = "addArtefatto";

		_methodParameterTypes25 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto", "long",
				"com.liferay.portal.service.ServiceContext"
			};

		_methodName26 = "permanentDeleteArtefatto";

		_methodParameterTypes26 = new String[] {
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			};

		_methodName27 = "getArtefattiByPilotAndStatus";

		_methodParameterTypes27 = new String[] { "java.lang.String", "int" };

		_methodName28 = "getArtefattiByPilotAndStatus";

		_methodParameterTypes28 = new String[] {
				"java.lang.String", "int", "java.lang.String",
				"java.lang.String"
			};

		_methodName29 = "getArtefattoByDescriptionRDF";

		_methodParameterTypes29 = new String[] { "java.lang.String" };

		_methodName30 = "getArtefattoByResourceRDF";

		_methodParameterTypes30 = new String[] { "java.lang.String" };

		_methodName31 = "getArtefattiByTitle";

		_methodParameterTypes31 = new String[] { "java.lang.String" };

		_methodName32 = "findArtefattoByStatus";

		_methodParameterTypes32 = new String[] { "int" };

		_methodName33 = "getArtefactsByPilotStatus";

		_methodParameterTypes33 = new String[] {
				"java.lang.String", "int", "int", "int"
			};

		_methodName34 = "getArtefactsByPilotStatus";

		_methodParameterTypes34 = new String[] { "java.lang.String", "int" };

		_methodName35 = "findArtefattoByStatus";

		_methodParameterTypes35 = new String[] {
				"int", "java.lang.String", "java.lang.String"
			};

		_methodName36 = "findArtefattoByUserId_Status";

		_methodParameterTypes36 = new String[] { "long", "int" };

		_methodName37 = "findArtefattoByCategory_Status";

		_methodParameterTypes37 = new String[] { "long", "int" };

		_methodName38 = "findArtefattoByPilot_Category_Status";

		_methodParameterTypes38 = new String[] { "java.lang.String", "long", "int" };

		_methodName39 = "findArtefattoByCompanyId";

		_methodParameterTypes39 = new String[] { "long" };

		_methodName40 = "findArtefattoByCompanyId";

		_methodParameterTypes40 = new String[] {
				"long", "java.lang.String", "boolean"
			};

		_methodName41 = "findArtefattoByCompanyId";

		_methodParameterTypes41 = new String[] {
				"long", "java.lang.String", "java.lang.String"
			};

		_methodName42 = "findArtefattoByCategory";

		_methodParameterTypes42 = new String[] { "long" };

		_methodName43 = "findArtefattoByCategory";

		_methodParameterTypes43 = new String[] { "long", "int", "int" };

		_methodName44 = "findArtefattiByIds";

		_methodParameterTypes44 = new String[] { "java.util.List" };

		_methodName45 = "getArtefattiByLangStatus";

		_methodParameterTypes45 = new String[] { "java.lang.String", "int" };

		_methodName46 = "getArtefattiByPilotLangStatus";

		_methodParameterTypes46 = new String[] {
				"java.lang.String", "java.lang.String", "int"
			};

		_methodName47 = "getArtefattiByLangStatus";

		_methodParameterTypes47 = new String[] {
				"java.lang.String", "int", "int", "int"
			};

		_methodName48 = "getArtefattiByPilotLangStatus";

		_methodParameterTypes48 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName49 = "getArtefattiByKeyword";

		_methodParameterTypes49 = new String[] { "java.lang.String" };

		_methodName50 = "getBuildingBlocks";

		_methodParameterTypes50 = new String[] { "java.lang.String" };

		_methodName51 = "getBuildingBlocks";

		_methodParameterTypes51 = new String[] { "java.lang.String", "int", "int" };

		_methodName52 = "getBuildingBlocksByKeyword";

		_methodParameterTypes52 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName53 = "getBuildingBlocksByLangStatus";

		_methodParameterTypes53 = new String[] { "java.lang.String", "int" };

		_methodName54 = "getBuildingBlocksByPilotLangStatus";

		_methodParameterTypes54 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName55 = "getPublicServices";

		_methodParameterTypes55 = new String[] { "java.lang.String" };

		_methodName56 = "getPublicServices";

		_methodParameterTypes56 = new String[] { "java.lang.String", "int", "int" };

		_methodName57 = "getPublicServicesByKeyword";

		_methodParameterTypes57 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName58 = "getPublicServicesByLangStatus";

		_methodParameterTypes58 = new String[] { "java.lang.String", "int" };

		_methodName59 = "getPublicServicesByPilotLangStatus";

		_methodParameterTypes59 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName60 = "getDatasets";

		_methodParameterTypes60 = new String[] { "java.lang.String" };

		_methodName61 = "getDatasets";

		_methodParameterTypes61 = new String[] { "java.lang.String", "int", "int" };

		_methodName62 = "getDatasetsByKeyword";

		_methodParameterTypes62 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName63 = "getDatasetsByLangStatus";

		_methodParameterTypes63 = new String[] { "java.lang.String", "int" };

		_methodName64 = "getDatasetsByPilotLangStatus";

		_methodParameterTypes64 = new String[] {
				"java.lang.String", "java.lang.String", "int", "int", "int"
			};

		_methodName65 = "getArtefactByExternalId";

		_methodParameterTypes65 = new String[] { "java.lang.String" };

		_methodName66 = "getTopRatedArtefactsByPilot";

		_methodParameterTypes66 = new String[] { "java.lang.String", "int" };

		_methodName67 = "getTopRatedArtefactsByLanguage";

		_methodParameterTypes67 = new String[] { "java.lang.String", "int" };

		_methodName68 = "getArtefactsByUserId";

		_methodParameterTypes68 = new String[] { "long", "int" };

		_methodName69 = "getArtefattiByOwner";

		_methodParameterTypes69 = new String[] { "java.lang.String" };

		_methodName70 = "getArtefattiByOwnerAndStatus";

		_methodParameterTypes70 = new String[] { "java.lang.String", "int" };

		_methodName71 = "forget";

		_methodParameterTypes71 = new String[] { "long", "boolean" };
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName0,
					_methodParameterTypes0,
					new Object[] { ClpSerializer.translateInput(artefatto) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto createArtefatto(
		long artefattoId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName1,
					_methodParameterTypes1, new Object[] { artefattoId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName2,
					_methodParameterTypes2, new Object[] { artefattoId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto deleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName3,
					_methodParameterTypes3,
					new Object[] { ClpSerializer.translateInput(artefatto) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName4,
					_methodParameterTypes4, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.dao.orm.DynamicQuery)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName5,
					_methodParameterTypes5,
					new Object[] { ClpSerializer.translateInput(dynamicQuery) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName6,
					_methodParameterTypes6,
					new Object[] {
						ClpSerializer.translateInput(dynamicQuery),
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName7,
					_methodParameterTypes7,
					new Object[] {
						ClpSerializer.translateInput(dynamicQuery),
						
					start,
						
					end,
						
					ClpSerializer.translateInput(orderByComparator)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName8,
					_methodParameterTypes8,
					new Object[] { ClpSerializer.translateInput(dynamicQuery) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Long)returnObj).longValue();
	}

	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName9,
					_methodParameterTypes9,
					new Object[] {
						ClpSerializer.translateInput(dynamicQuery),
						
					ClpSerializer.translateInput(projection)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Long)returnObj).longValue();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName10,
					_methodParameterTypes10, new Object[] { artefattoId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName11,
					_methodParameterTypes11,
					new Object[] { ClpSerializer.translateInput(uuid), companyId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto fetchArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName12,
					_methodParameterTypes12,
					new Object[] { ClpSerializer.translateInput(uuid), groupId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefatto(
		long artefattoId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName13,
					_methodParameterTypes13, new Object[] { artefattoId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName14,
					_methodParameterTypes14,
					new Object[] { ClpSerializer.translateInput(primaryKeyObj) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.model.PersistedModel)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName15,
					_methodParameterTypes15,
					new Object[] { ClpSerializer.translateInput(uuid), companyId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefattoByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName16,
					_methodParameterTypes16,
					new Object[] { ClpSerializer.translateInput(uuid), groupId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName17,
					_methodParameterTypes17, new Object[] { start, end });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public int getArtefattosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName18,
					_methodParameterTypes18, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Integer)returnObj).intValue();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto updateArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName19,
					_methodParameterTypes19,
					new Object[] { ClpSerializer.translateInput(artefatto) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.lang.String getBeanIdentifier() {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName20,
					_methodParameterTypes20, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		try {
			_invokableLocalService.invokeMethod(_methodName21,
				_methodParameterTypes21,
				new Object[] { ClpSerializer.translateInput(beanIdentifier) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		throw new UnsupportedOperationException();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto publishArtefatto(
		long idArtefatto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName23,
					_methodParameterTypes23, new Object[] { idArtefatto });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, java.lang.String[] tags)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName24,
					_methodParameterTypes24,
					new Object[] {
						ClpSerializer.translateInput(newArtefatto),
						
					userId,
						
					ClpSerializer.translateInput(tags)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto addArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto newArtefatto,
		long userId, com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName25,
					_methodParameterTypes25,
					new Object[] {
						ClpSerializer.translateInput(newArtefatto),
						
					userId,
						
					ClpSerializer.translateInput(serviceContext)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto permanentDeleteArtefatto(
		it.eng.rspa.marketplace.artefatto.model.Artefatto artefatto)
		throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName26,
					_methodParameterTypes26,
					new Object[] { ClpSerializer.translateInput(artefatto) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName27,
					_methodParameterTypes27,
					new Object[] { ClpSerializer.translateInput(pilotId), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotAndStatus(
		java.lang.String pilotId, int status, java.lang.String orderByCampo,
		java.lang.String orderType) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName28,
					_methodParameterTypes28,
					new Object[] {
						ClpSerializer.translateInput(pilotId),
						
					status,
						
					ClpSerializer.translateInput(orderByCampo),
						
					ClpSerializer.translateInput(orderType)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByDescriptionRDF(
		java.lang.String description) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName29,
					_methodParameterTypes29,
					new Object[] { ClpSerializer.translateInput(description) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattoByResourceRDF(
		java.lang.String resourceRDF) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName30,
					_methodParameterTypes30,
					new Object[] { ClpSerializer.translateInput(resourceRDF) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByTitle(
		java.lang.String title) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName31,
					_methodParameterTypes31,
					new Object[] { ClpSerializer.translateInput(title) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName32,
					_methodParameterTypes32, new Object[] { status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status, int start, int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName33,
					_methodParameterTypes33,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefactsByPilotStatus(
		java.lang.String pilot, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName34,
					_methodParameterTypes34,
					new Object[] { ClpSerializer.translateInput(pilot), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByStatus(
		int status, java.lang.String orderByCampo, java.lang.String orderType) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName35,
					_methodParameterTypes35,
					new Object[] {
						status,
						
					ClpSerializer.translateInput(orderByCampo),
						
					ClpSerializer.translateInput(orderType)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByUserId_Status(
		long userId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName36,
					_methodParameterTypes36, new Object[] { userId, status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory_Status(
		long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName37,
					_methodParameterTypes37, new Object[] { categoryId, status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByPilot_Category_Status(
		java.lang.String pilot, long categoryId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName38,
					_methodParameterTypes38,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					categoryId,
						
					status
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException)t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException)t;
			}

			if (t instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName39,
					_methodParameterTypes39, new Object[] { companyId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo, boolean isAsc) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName40,
					_methodParameterTypes40,
					new Object[] {
						companyId,
						
					ClpSerializer.translateInput(orderByCampo),
						
					isAsc
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCompanyId(
		long companyId, java.lang.String orderByCampo,
		java.lang.String orderType) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName41,
					_methodParameterTypes41,
					new Object[] {
						companyId,
						
					ClpSerializer.translateInput(orderByCampo),
						
					ClpSerializer.translateInput(orderType)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName42,
					_methodParameterTypes42, new Object[] { category });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByCategory(
		long category, int start, int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName43,
					_methodParameterTypes43,
					new Object[] { category, start, end });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattiByIds(
		java.util.List<java.lang.Long> ids) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName44,
					_methodParameterTypes44,
					new Object[] { ClpSerializer.translateInput(ids) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName45,
					_methodParameterTypes45,
					new Object[] { ClpSerializer.translateInput(lang), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName46,
					_methodParameterTypes46,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(lang),
						
					status
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByLangStatus(
		java.lang.String lang, int status, int start, int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName47,
					_methodParameterTypes47,
					new Object[] {
						ClpSerializer.translateInput(lang),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName48,
					_methodParameterTypes48,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(lang),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByKeyword(
		java.lang.String keyword) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName49,
					_methodParameterTypes49,
					new Object[] { ClpSerializer.translateInput(keyword) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName50,
					_methodParameterTypes50,
					new Object[] { ClpSerializer.translateInput(pilotId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocks(
		java.lang.String pilotId, int startId, int endId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName51,
					_methodParameterTypes51,
					new Object[] {
						ClpSerializer.translateInput(pilotId),
						
					startId,
						
					endId
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByKeyword(
		java.lang.String pilotId, java.lang.String keyword) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName52,
					_methodParameterTypes52,
					new Object[] {
						ClpSerializer.translateInput(pilotId),
						
					ClpSerializer.translateInput(keyword)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByLangStatus(
		java.lang.String lang, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName53,
					_methodParameterTypes53,
					new Object[] { ClpSerializer.translateInput(lang), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getBuildingBlocksByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName54,
					_methodParameterTypes54,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(lang),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName55,
					_methodParameterTypes55,
					new Object[] { ClpSerializer.translateInput(pilot) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServices(
		java.lang.String pilot, int starId, int endId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName56,
					_methodParameterTypes56,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					starId,
						
					endId
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByKeyword(
		java.lang.String pilot, java.lang.String keyword) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName57,
					_methodParameterTypes57,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(keyword)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByLangStatus(
		java.lang.String lang, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName58,
					_methodParameterTypes58,
					new Object[] { ClpSerializer.translateInput(lang), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getPublicServicesByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName59,
					_methodParameterTypes59,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(lang),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName60,
					_methodParameterTypes60,
					new Object[] { ClpSerializer.translateInput(pilot) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasets(
		java.lang.String pilot, int startId, int endId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName61,
					_methodParameterTypes61,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					startId,
						
					endId
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByKeyword(
		java.lang.String pilot, java.lang.String keyword) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName62,
					_methodParameterTypes62,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(keyword)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByLangStatus(
		java.lang.String lang, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName63,
					_methodParameterTypes63,
					new Object[] { ClpSerializer.translateInput(lang), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getDatasetsByPilotLangStatus(
		java.lang.String pilot, java.lang.String lang, int status, int start,
		int end) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName64,
					_methodParameterTypes64,
					new Object[] {
						ClpSerializer.translateInput(pilot),
						
					ClpSerializer.translateInput(lang),
						
					status,
						
					start,
						
					end
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Artefatto getArtefactByExternalId(
		java.lang.String eid) throws java.lang.Exception {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName65,
					_methodParameterTypes65,
					new Object[] { ClpSerializer.translateInput(eid) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof java.lang.Exception) {
				throw (java.lang.Exception)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (it.eng.rspa.marketplace.artefatto.model.Artefatto)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByPilot(
		java.lang.String pilot, int maxNumber) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName66,
					_methodParameterTypes66,
					new Object[] { ClpSerializer.translateInput(pilot), maxNumber });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONArray)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedArtefactsByLanguage(
		java.lang.String language, int maxNumber) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName67,
					_methodParameterTypes67,
					new Object[] {
						ClpSerializer.translateInput(language),
						
					maxNumber
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONArray)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getArtefactsByUserId(
		long liferayUserId, int maxNumber) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName68,
					_methodParameterTypes68,
					new Object[] { liferayUserId, maxNumber });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONArray)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwner(
		java.lang.String owner) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName69,
					_methodParameterTypes69,
					new Object[] { ClpSerializer.translateInput(owner) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> getArtefattiByOwnerAndStatus(
		java.lang.String owner, int status) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName70,
					_methodParameterTypes70,
					new Object[] { ClpSerializer.translateInput(owner), status });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto>)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject forget(
		long liferayUserId, boolean removecontent) throws java.lang.Exception {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName71,
					_methodParameterTypes71,
					new Object[] { liferayUserId, removecontent });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof java.lang.Exception) {
				throw (java.lang.Exception)t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	private InvokableLocalService _invokableLocalService;
	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName20;
	private String[] _methodParameterTypes20;
	private String _methodName21;
	private String[] _methodParameterTypes21;
	private String _methodName23;
	private String[] _methodParameterTypes23;
	private String _methodName24;
	private String[] _methodParameterTypes24;
	private String _methodName25;
	private String[] _methodParameterTypes25;
	private String _methodName26;
	private String[] _methodParameterTypes26;
	private String _methodName27;
	private String[] _methodParameterTypes27;
	private String _methodName28;
	private String[] _methodParameterTypes28;
	private String _methodName29;
	private String[] _methodParameterTypes29;
	private String _methodName30;
	private String[] _methodParameterTypes30;
	private String _methodName31;
	private String[] _methodParameterTypes31;
	private String _methodName32;
	private String[] _methodParameterTypes32;
	private String _methodName33;
	private String[] _methodParameterTypes33;
	private String _methodName34;
	private String[] _methodParameterTypes34;
	private String _methodName35;
	private String[] _methodParameterTypes35;
	private String _methodName36;
	private String[] _methodParameterTypes36;
	private String _methodName37;
	private String[] _methodParameterTypes37;
	private String _methodName38;
	private String[] _methodParameterTypes38;
	private String _methodName39;
	private String[] _methodParameterTypes39;
	private String _methodName40;
	private String[] _methodParameterTypes40;
	private String _methodName41;
	private String[] _methodParameterTypes41;
	private String _methodName42;
	private String[] _methodParameterTypes42;
	private String _methodName43;
	private String[] _methodParameterTypes43;
	private String _methodName44;
	private String[] _methodParameterTypes44;
	private String _methodName45;
	private String[] _methodParameterTypes45;
	private String _methodName46;
	private String[] _methodParameterTypes46;
	private String _methodName47;
	private String[] _methodParameterTypes47;
	private String _methodName48;
	private String[] _methodParameterTypes48;
	private String _methodName49;
	private String[] _methodParameterTypes49;
	private String _methodName50;
	private String[] _methodParameterTypes50;
	private String _methodName51;
	private String[] _methodParameterTypes51;
	private String _methodName52;
	private String[] _methodParameterTypes52;
	private String _methodName53;
	private String[] _methodParameterTypes53;
	private String _methodName54;
	private String[] _methodParameterTypes54;
	private String _methodName55;
	private String[] _methodParameterTypes55;
	private String _methodName56;
	private String[] _methodParameterTypes56;
	private String _methodName57;
	private String[] _methodParameterTypes57;
	private String _methodName58;
	private String[] _methodParameterTypes58;
	private String _methodName59;
	private String[] _methodParameterTypes59;
	private String _methodName60;
	private String[] _methodParameterTypes60;
	private String _methodName61;
	private String[] _methodParameterTypes61;
	private String _methodName62;
	private String[] _methodParameterTypes62;
	private String _methodName63;
	private String[] _methodParameterTypes63;
	private String _methodName64;
	private String[] _methodParameterTypes64;
	private String _methodName65;
	private String[] _methodParameterTypes65;
	private String _methodName66;
	private String[] _methodParameterTypes66;
	private String _methodName67;
	private String[] _methodParameterTypes67;
	private String _methodName68;
	private String[] _methodParameterTypes68;
	private String _methodName69;
	private String[] _methodParameterTypes69;
	private String _methodName70;
	private String[] _methodParameterTypes70;
	private String _methodName71;
	private String[] _methodParameterTypes71;
}