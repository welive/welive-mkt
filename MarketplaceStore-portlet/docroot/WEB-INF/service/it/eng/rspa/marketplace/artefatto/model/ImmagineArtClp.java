/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class ImmagineArtClp extends BaseModelImpl<ImmagineArt>
	implements ImmagineArt {
	public ImmagineArtClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ImmagineArt.class;
	}

	@Override
	public String getModelClassName() {
		return ImmagineArt.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _imageId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setImageId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _imageId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("imageId", getImageId());
		attributes.put("dlImageId", getDlImageId());
		attributes.put("descrizione", getDescrizione());
		attributes.put("artefattoIdent", getArtefattoIdent());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long imageId = (Long)attributes.get("imageId");

		if (imageId != null) {
			setImageId(imageId);
		}

		Long dlImageId = (Long)attributes.get("dlImageId");

		if (dlImageId != null) {
			setDlImageId(dlImageId);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Long artefattoIdent = (Long)attributes.get("artefattoIdent");

		if (artefattoIdent != null) {
			setArtefattoIdent(artefattoIdent);
		}
	}

	@Override
	public long getImageId() {
		return _imageId;
	}

	@Override
	public void setImageId(long imageId) {
		_imageId = imageId;

		if (_immagineArtRemoteModel != null) {
			try {
				Class<?> clazz = _immagineArtRemoteModel.getClass();

				Method method = clazz.getMethod("setImageId", long.class);

				method.invoke(_immagineArtRemoteModel, imageId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getDlImageId() {
		return _dlImageId;
	}

	@Override
	public void setDlImageId(long dlImageId) {
		_dlImageId = dlImageId;

		if (_immagineArtRemoteModel != null) {
			try {
				Class<?> clazz = _immagineArtRemoteModel.getClass();

				Method method = clazz.getMethod("setDlImageId", long.class);

				method.invoke(_immagineArtRemoteModel, dlImageId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_immagineArtRemoteModel != null) {
			try {
				Class<?> clazz = _immagineArtRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_immagineArtRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtefattoIdent() {
		return _artefattoIdent;
	}

	@Override
	public void setArtefattoIdent(long artefattoIdent) {
		_artefattoIdent = artefattoIdent;

		if (_immagineArtRemoteModel != null) {
			try {
				Class<?> clazz = _immagineArtRemoteModel.getClass();

				Method method = clazz.getMethod("setArtefattoIdent", long.class);

				method.invoke(_immagineArtRemoteModel, artefattoIdent);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getImmagineArtRemoteModel() {
		return _immagineArtRemoteModel;
	}

	public void setImmagineArtRemoteModel(BaseModel<?> immagineArtRemoteModel) {
		_immagineArtRemoteModel = immagineArtRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _immagineArtRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_immagineArtRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ImmagineArtLocalServiceUtil.addImmagineArt(this);
		}
		else {
			ImmagineArtLocalServiceUtil.updateImmagineArt(this);
		}
	}

	@Override
	public ImmagineArt toEscapedModel() {
		return (ImmagineArt)ProxyUtil.newProxyInstance(ImmagineArt.class.getClassLoader(),
			new Class[] { ImmagineArt.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ImmagineArtClp clone = new ImmagineArtClp();

		clone.setImageId(getImageId());
		clone.setDlImageId(getDlImageId());
		clone.setDescrizione(getDescrizione());
		clone.setArtefattoIdent(getArtefattoIdent());

		return clone;
	}

	@Override
	public int compareTo(ImmagineArt immagineArt) {
		long primaryKey = immagineArt.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ImmagineArtClp)) {
			return false;
		}

		ImmagineArtClp immagineArt = (ImmagineArtClp)obj;

		long primaryKey = immagineArt.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{imageId=");
		sb.append(getImageId());
		sb.append(", dlImageId=");
		sb.append(getDlImageId());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", artefattoIdent=");
		sb.append(getArtefattoIdent());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.ImmagineArt");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>imageId</column-name><column-value><![CDATA[");
		sb.append(getImageId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dlImageId</column-name><column-value><![CDATA[");
		sb.append(getDlImageId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artefattoIdent</column-name><column-value><![CDATA[");
		sb.append(getArtefattoIdent());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _imageId;
	private long _dlImageId;
	private String _descrizione;
	private long _artefattoIdent;
	private BaseModel<?> _immagineArtRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}