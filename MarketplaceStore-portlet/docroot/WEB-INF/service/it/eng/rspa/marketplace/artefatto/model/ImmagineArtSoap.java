/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class ImmagineArtSoap implements Serializable {
	public static ImmagineArtSoap toSoapModel(ImmagineArt model) {
		ImmagineArtSoap soapModel = new ImmagineArtSoap();

		soapModel.setImageId(model.getImageId());
		soapModel.setDlImageId(model.getDlImageId());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setArtefattoIdent(model.getArtefattoIdent());

		return soapModel;
	}

	public static ImmagineArtSoap[] toSoapModels(ImmagineArt[] models) {
		ImmagineArtSoap[] soapModels = new ImmagineArtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ImmagineArtSoap[][] toSoapModels(ImmagineArt[][] models) {
		ImmagineArtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ImmagineArtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ImmagineArtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ImmagineArtSoap[] toSoapModels(List<ImmagineArt> models) {
		List<ImmagineArtSoap> soapModels = new ArrayList<ImmagineArtSoap>(models.size());

		for (ImmagineArt model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ImmagineArtSoap[soapModels.size()]);
	}

	public ImmagineArtSoap() {
	}

	public long getPrimaryKey() {
		return _imageId;
	}

	public void setPrimaryKey(long pk) {
		setImageId(pk);
	}

	public long getImageId() {
		return _imageId;
	}

	public void setImageId(long imageId) {
		_imageId = imageId;
	}

	public long getDlImageId() {
		return _dlImageId;
	}

	public void setDlImageId(long dlImageId) {
		_dlImageId = dlImageId;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public long getArtefattoIdent() {
		return _artefattoIdent;
	}

	public void setArtefattoIdent(long artefattoIdent) {
		_artefattoIdent = artefattoIdent;
	}

	private long _imageId;
	private long _dlImageId;
	private String _descrizione;
	private long _artefattoIdent;
}