/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.MarketplaceConfLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class MarketplaceConfClp extends BaseModelImpl<MarketplaceConf>
	implements MarketplaceConf {
	public MarketplaceConfClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return MarketplaceConf.class;
	}

	@Override
	public String getModelClassName() {
		return MarketplaceConf.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _confId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setConfId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _confId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("confId", getConfId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());
		attributes.put("type", getType());
		attributes.put("options", getOptions());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long confId = (Long)attributes.get("confId");

		if (confId != null) {
			setConfId(confId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String options = (String)attributes.get("options");

		if (options != null) {
			setOptions(options);
		}
	}

	@Override
	public long getConfId() {
		return _confId;
	}

	@Override
	public void setConfId(long confId) {
		_confId = confId;

		if (_marketplaceConfRemoteModel != null) {
			try {
				Class<?> clazz = _marketplaceConfRemoteModel.getClass();

				Method method = clazz.getMethod("setConfId", long.class);

				method.invoke(_marketplaceConfRemoteModel, confId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getKey() {
		return _key;
	}

	@Override
	public void setKey(String key) {
		_key = key;

		if (_marketplaceConfRemoteModel != null) {
			try {
				Class<?> clazz = _marketplaceConfRemoteModel.getClass();

				Method method = clazz.getMethod("setKey", String.class);

				method.invoke(_marketplaceConfRemoteModel, key);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getValue() {
		return _value;
	}

	@Override
	public void setValue(String value) {
		_value = value;

		if (_marketplaceConfRemoteModel != null) {
			try {
				Class<?> clazz = _marketplaceConfRemoteModel.getClass();

				Method method = clazz.getMethod("setValue", String.class);

				method.invoke(_marketplaceConfRemoteModel, value);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getType() {
		return _type;
	}

	@Override
	public void setType(String type) {
		_type = type;

		if (_marketplaceConfRemoteModel != null) {
			try {
				Class<?> clazz = _marketplaceConfRemoteModel.getClass();

				Method method = clazz.getMethod("setType", String.class);

				method.invoke(_marketplaceConfRemoteModel, type);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOptions() {
		return _options;
	}

	@Override
	public void setOptions(String options) {
		_options = options;

		if (_marketplaceConfRemoteModel != null) {
			try {
				Class<?> clazz = _marketplaceConfRemoteModel.getClass();

				Method method = clazz.getMethod("setOptions", String.class);

				method.invoke(_marketplaceConfRemoteModel, options);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getMarketplaceConfRemoteModel() {
		return _marketplaceConfRemoteModel;
	}

	public void setMarketplaceConfRemoteModel(
		BaseModel<?> marketplaceConfRemoteModel) {
		_marketplaceConfRemoteModel = marketplaceConfRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _marketplaceConfRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_marketplaceConfRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			MarketplaceConfLocalServiceUtil.addMarketplaceConf(this);
		}
		else {
			MarketplaceConfLocalServiceUtil.updateMarketplaceConf(this);
		}
	}

	@Override
	public MarketplaceConf toEscapedModel() {
		return (MarketplaceConf)ProxyUtil.newProxyInstance(MarketplaceConf.class.getClassLoader(),
			new Class[] { MarketplaceConf.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		MarketplaceConfClp clone = new MarketplaceConfClp();

		clone.setConfId(getConfId());
		clone.setKey(getKey());
		clone.setValue(getValue());
		clone.setType(getType());
		clone.setOptions(getOptions());

		return clone;
	}

	@Override
	public int compareTo(MarketplaceConf marketplaceConf) {
		long primaryKey = marketplaceConf.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MarketplaceConfClp)) {
			return false;
		}

		MarketplaceConfClp marketplaceConf = (MarketplaceConfClp)obj;

		long primaryKey = marketplaceConf.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{confId=");
		sb.append(getConfId());
		sb.append(", key=");
		sb.append(getKey());
		sb.append(", value=");
		sb.append(getValue());
		sb.append(", type=");
		sb.append(getType());
		sb.append(", options=");
		sb.append(getOptions());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.marketplace.artefatto.model.MarketplaceConf");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>confId</column-name><column-value><![CDATA[");
		sb.append(getConfId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>key</column-name><column-value><![CDATA[");
		sb.append(getKey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>options</column-name><column-value><![CDATA[");
		sb.append(getOptions());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _confId;
	private String _key;
	private String _value;
	private String _type;
	private String _options;
	private BaseModel<?> _marketplaceConfRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}