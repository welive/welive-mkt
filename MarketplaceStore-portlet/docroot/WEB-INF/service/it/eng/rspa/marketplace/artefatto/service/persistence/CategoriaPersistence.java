/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.Categoria;

/**
 * The persistence interface for the categoria service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see CategoriaPersistenceImpl
 * @see CategoriaUtil
 * @generated
 */
public interface CategoriaPersistence extends BasePersistence<Categoria> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CategoriaUtil} to access the categoria persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the categorias where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @return the matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBycategoryId(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the categorias where idCategoria = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idCategoria the id categoria
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @return the range of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBycategoryId(
		long idCategoria, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the categorias where idCategoria = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idCategoria the id categoria
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBycategoryId(
		long idCategoria, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first categoria in the ordered set where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findBycategoryId_First(
		long idCategoria,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the first categoria in the ordered set where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBycategoryId_First(
		long idCategoria,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last categoria in the ordered set where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findBycategoryId_Last(
		long idCategoria,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the last categoria in the ordered set where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBycategoryId_Last(
		long idCategoria,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the categorias where idCategoria = &#63; from the database.
	*
	* @param idCategoria the id categoria
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycategoryId(long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of categorias where idCategoria = &#63;.
	*
	* @param idCategoria the id categoria
	* @return the number of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public int countBycategoryId(long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the categoria where nomeCategoria = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException} if it could not be found.
	*
	* @param nomeCategoria the nome categoria
	* @return the matching categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findBycategoryName(
		java.lang.String nomeCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the categoria where nomeCategoria = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param nomeCategoria the nome categoria
	* @return the matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBycategoryName(
		java.lang.String nomeCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the categoria where nomeCategoria = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param nomeCategoria the nome categoria
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBycategoryName(
		java.lang.String nomeCategoria, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the categoria where nomeCategoria = &#63; from the database.
	*
	* @param nomeCategoria the nome categoria
	* @return the categoria that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria removeBycategoryName(
		java.lang.String nomeCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the number of categorias where nomeCategoria = &#63;.
	*
	* @param nomeCategoria the nome categoria
	* @return the number of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public int countBycategoryName(java.lang.String nomeCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the categorias where supports = &#63;.
	*
	* @param supports the supports
	* @return the matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBysupportedFormat(
		java.lang.String supports)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the categorias where supports = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param supports the supports
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @return the range of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBysupportedFormat(
		java.lang.String supports, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the categorias where supports = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param supports the supports
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findBysupportedFormat(
		java.lang.String supports, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first categoria in the ordered set where supports = &#63;.
	*
	* @param supports the supports
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findBysupportedFormat_First(
		java.lang.String supports,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the first categoria in the ordered set where supports = &#63;.
	*
	* @param supports the supports
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBysupportedFormat_First(
		java.lang.String supports,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last categoria in the ordered set where supports = &#63;.
	*
	* @param supports the supports
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findBysupportedFormat_Last(
		java.lang.String supports,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the last categoria in the ordered set where supports = &#63;.
	*
	* @param supports the supports
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching categoria, or <code>null</code> if a matching categoria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchBysupportedFormat_Last(
		java.lang.String supports,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the categorias before and after the current categoria in the ordered set where supports = &#63;.
	*
	* @param idCategoria the primary key of the current categoria
	* @param supports the supports
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria[] findBysupportedFormat_PrevAndNext(
		long idCategoria, java.lang.String supports,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Removes all the categorias where supports = &#63; from the database.
	*
	* @param supports the supports
	* @throws SystemException if a system exception occurred
	*/
	public void removeBysupportedFormat(java.lang.String supports)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of categorias where supports = &#63;.
	*
	* @param supports the supports
	* @return the number of matching categorias
	* @throws SystemException if a system exception occurred
	*/
	public int countBysupportedFormat(java.lang.String supports)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the categoria in the entity cache if it is enabled.
	*
	* @param categoria the categoria
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria);

	/**
	* Caches the categorias in the entity cache if it is enabled.
	*
	* @param categorias the categorias
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> categorias);

	/**
	* Creates a new categoria with the primary key. Does not add the categoria to the database.
	*
	* @param idCategoria the primary key for the new categoria
	* @return the new categoria
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria create(
		long idCategoria);

	/**
	* Removes the categoria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCategoria the primary key of the categoria
	* @return the categoria that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria remove(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	public it.eng.rspa.marketplace.artefatto.model.Categoria updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the categoria with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException} if it could not be found.
	*
	* @param idCategoria the primary key of the categoria
	* @return the categoria
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria findByPrimaryKey(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException;

	/**
	* Returns the categoria with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idCategoria the primary key of the categoria
	* @return the categoria, or <code>null</code> if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchByPrimaryKey(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the categorias.
	*
	* @return the categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the categorias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @return the range of categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the categorias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of categorias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the categorias from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of categorias.
	*
	* @return the number of categorias
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}