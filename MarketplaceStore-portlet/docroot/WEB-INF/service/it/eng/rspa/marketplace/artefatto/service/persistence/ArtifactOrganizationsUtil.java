/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;

import java.util.List;

/**
 * The persistence utility for the artifact organizations service. This utility wraps {@link ArtifactOrganizationsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactOrganizationsPersistence
 * @see ArtifactOrganizationsPersistenceImpl
 * @generated
 */
public class ArtifactOrganizationsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ArtifactOrganizations artifactOrganizations) {
		getPersistence().clearCache(artifactOrganizations);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ArtifactOrganizations> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ArtifactOrganizations> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ArtifactOrganizations> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ArtifactOrganizations update(
		ArtifactOrganizations artifactOrganizations) throws SystemException {
		return getPersistence().update(artifactOrganizations);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ArtifactOrganizations update(
		ArtifactOrganizations artifactOrganizations,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(artifactOrganizations, serviceContext);
	}

	/**
	* Returns all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationId_Status(organizationId, status);
	}

	/**
	* Returns a range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationId_Status(organizationId, status, start,
			end);
	}

	/**
	* Returns an ordered range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationId_Status(organizationId, status, start,
			end, orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Status_First(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_Status_First(organizationId, status,
			orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Status_First(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_Status_First(organizationId, status,
			orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Status_Last(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_Status_Last(organizationId, status,
			orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Status_Last(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_Status_Last(organizationId, status,
			orderByComparator);
	}

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByOrganizationId_Status_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_Status_PrevAndNext(artifactOrganizationsPK,
			organizationId, status, orderByComparator);
	}

	/**
	* Removes all the artifact organizationses where organizationId = &#63; and status = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganizationId_Status(long organizationId,
		int status) throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganizationId_Status(organizationId, status);
	}

	/**
	* Returns the number of artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationId_Status(long organizationId,
		int status) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByOrganizationId_Status(organizationId, status);
	}

	/**
	* Returns all the artifact organizationses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtifactId(artifactId);
	}

	/**
	* Returns a range of all the artifact organizationses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtifactId(artifactId, start, end);
	}

	/**
	* Returns an ordered range of all the artifact organizationses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtifactId(artifactId, start, end, orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByArtifactId_First(artifactId, orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtifactId_First(artifactId, orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByArtifactId_Last(artifactId, orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtifactId_Last(artifactId, orderByComparator);
	}

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByArtifactId_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByArtifactId_PrevAndNext(artifactOrganizationsPK,
			artifactId, orderByComparator);
	}

	/**
	* Removes all the artifact organizationses where artifactId = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByArtifactId(artifactId);
	}

	/**
	* Returns the number of artifact organizationses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByArtifactId(artifactId);
	}

	/**
	* Returns all the artifact organizationses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationId(organizationId);
	}

	/**
	* Returns a range of all the artifact organizationses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationId(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the artifact organizationses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationId(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_First(organizationId,
			orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByOrganizationId_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence()
				   .findByOrganizationId_PrevAndNext(artifactOrganizationsPK,
			organizationId, orderByComparator);
	}

	/**
	* Removes all the artifact organizationses where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganizationId(organizationId);
	}

	/**
	* Returns the number of artifact organizationses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganizationId(organizationId);
	}

	/**
	* Caches the artifact organizations in the entity cache if it is enabled.
	*
	* @param artifactOrganizations the artifact organizations
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations) {
		getPersistence().cacheResult(artifactOrganizations);
	}

	/**
	* Caches the artifact organizationses in the entity cache if it is enabled.
	*
	* @param artifactOrganizationses the artifact organizationses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> artifactOrganizationses) {
		getPersistence().cacheResult(artifactOrganizationses);
	}

	/**
	* Creates a new artifact organizations with the primary key. Does not add the artifact organizations to the database.
	*
	* @param artifactOrganizationsPK the primary key for the new artifact organizations
	* @return the new artifact organizations
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations create(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK) {
		return getPersistence().create(artifactOrganizationsPK);
	}

	/**
	* Removes the artifact organizations with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations remove(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence().remove(artifactOrganizationsPK);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(artifactOrganizations);
	}

	/**
	* Returns the artifact organizations with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException} if it could not be found.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByPrimaryKey(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException {
		return getPersistence().findByPrimaryKey(artifactOrganizationsPK);
	}

	/**
	* Returns the artifact organizations with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations, or <code>null</code> if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByPrimaryKey(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(artifactOrganizationsPK);
	}

	/**
	* Returns all the artifact organizationses.
	*
	* @return the artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the artifact organizationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the artifact organizationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the artifact organizationses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of artifact organizationses.
	*
	* @return the number of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ArtifactOrganizationsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ArtifactOrganizationsPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					ArtifactOrganizationsPersistence.class.getName());

			ReferenceRegistry.registerReference(ArtifactOrganizationsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ArtifactOrganizationsPersistence persistence) {
	}

	private static ArtifactOrganizationsPersistence _persistence;
}