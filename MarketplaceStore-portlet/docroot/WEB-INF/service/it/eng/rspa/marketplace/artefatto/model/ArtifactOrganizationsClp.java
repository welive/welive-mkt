/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.marketplace.artefatto.service.ArtifactOrganizationsLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ClpSerializer;
import it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eng
 */
public class ArtifactOrganizationsClp extends BaseModelImpl<ArtifactOrganizations>
	implements ArtifactOrganizations {
	public ArtifactOrganizationsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ArtifactOrganizations.class;
	}

	@Override
	public String getModelClassName() {
		return ArtifactOrganizations.class.getName();
	}

	@Override
	public ArtifactOrganizationsPK getPrimaryKey() {
		return new ArtifactOrganizationsPK(_organizationId, _artifactId);
	}

	@Override
	public void setPrimaryKey(ArtifactOrganizationsPK primaryKey) {
		setOrganizationId(primaryKey.organizationId);
		setArtifactId(primaryKey.artifactId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new ArtifactOrganizationsPK(_organizationId, _artifactId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((ArtifactOrganizationsPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("organizationId", getOrganizationId());
		attributes.put("artifactId", getArtifactId());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@Override
	public long getOrganizationId() {
		return _organizationId;
	}

	@Override
	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;

		if (_artifactOrganizationsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactOrganizationsRemoteModel.getClass();

				Method method = clazz.getMethod("setOrganizationId", long.class);

				method.invoke(_artifactOrganizationsRemoteModel, organizationId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtifactId() {
		return _artifactId;
	}

	@Override
	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;

		if (_artifactOrganizationsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactOrganizationsRemoteModel.getClass();

				Method method = clazz.getMethod("setArtifactId", long.class);

				method.invoke(_artifactOrganizationsRemoteModel, artifactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		_status = status;

		if (_artifactOrganizationsRemoteModel != null) {
			try {
				Class<?> clazz = _artifactOrganizationsRemoteModel.getClass();

				Method method = clazz.getMethod("setStatus", int.class);

				method.invoke(_artifactOrganizationsRemoteModel, status);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getArtifactOrganizationsRemoteModel() {
		return _artifactOrganizationsRemoteModel;
	}

	public void setArtifactOrganizationsRemoteModel(
		BaseModel<?> artifactOrganizationsRemoteModel) {
		_artifactOrganizationsRemoteModel = artifactOrganizationsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _artifactOrganizationsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_artifactOrganizationsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ArtifactOrganizationsLocalServiceUtil.addArtifactOrganizations(this);
		}
		else {
			ArtifactOrganizationsLocalServiceUtil.updateArtifactOrganizations(this);
		}
	}

	@Override
	public ArtifactOrganizations toEscapedModel() {
		return (ArtifactOrganizations)ProxyUtil.newProxyInstance(ArtifactOrganizations.class.getClassLoader(),
			new Class[] { ArtifactOrganizations.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ArtifactOrganizationsClp clone = new ArtifactOrganizationsClp();

		clone.setOrganizationId(getOrganizationId());
		clone.setArtifactId(getArtifactId());
		clone.setStatus(getStatus());

		return clone;
	}

	@Override
	public int compareTo(ArtifactOrganizations artifactOrganizations) {
		ArtifactOrganizationsPK primaryKey = artifactOrganizations.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtifactOrganizationsClp)) {
			return false;
		}

		ArtifactOrganizationsClp artifactOrganizations = (ArtifactOrganizationsClp)obj;

		ArtifactOrganizationsPK primaryKey = artifactOrganizations.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{organizationId=");
		sb.append(getOrganizationId());
		sb.append(", artifactId=");
		sb.append(getArtifactId());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artifactId</column-name><column-value><![CDATA[");
		sb.append(getArtifactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _organizationId;
	private long _artifactId;
	private int _status;
	private BaseModel<?> _artifactOrganizationsRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.marketplace.artefatto.service.ClpSerializer.class;
}