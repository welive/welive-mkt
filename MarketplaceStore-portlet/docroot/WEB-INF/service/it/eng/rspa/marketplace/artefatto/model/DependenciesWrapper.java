/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Dependencies}.
 * </p>
 *
 * @author eng
 * @see Dependencies
 * @generated
 */
public class DependenciesWrapper implements Dependencies,
	ModelWrapper<Dependencies> {
	public DependenciesWrapper(Dependencies dependencies) {
		_dependencies = dependencies;
	}

	@Override
	public Class<?> getModelClass() {
		return Dependencies.class;
	}

	@Override
	public String getModelClassName() {
		return Dependencies.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dependencyId", getDependencyId());
		attributes.put("artefactId", getArtefactId());
		attributes.put("dependsFrom", getDependsFrom());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long dependencyId = (Long)attributes.get("dependencyId");

		if (dependencyId != null) {
			setDependencyId(dependencyId);
		}

		Long artefactId = (Long)attributes.get("artefactId");

		if (artefactId != null) {
			setArtefactId(artefactId);
		}

		Long dependsFrom = (Long)attributes.get("dependsFrom");

		if (dependsFrom != null) {
			setDependsFrom(dependsFrom);
		}
	}

	/**
	* Returns the primary key of this dependencies.
	*
	* @return the primary key of this dependencies
	*/
	@Override
	public long getPrimaryKey() {
		return _dependencies.getPrimaryKey();
	}

	/**
	* Sets the primary key of this dependencies.
	*
	* @param primaryKey the primary key of this dependencies
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_dependencies.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the dependency ID of this dependencies.
	*
	* @return the dependency ID of this dependencies
	*/
	@Override
	public long getDependencyId() {
		return _dependencies.getDependencyId();
	}

	/**
	* Sets the dependency ID of this dependencies.
	*
	* @param dependencyId the dependency ID of this dependencies
	*/
	@Override
	public void setDependencyId(long dependencyId) {
		_dependencies.setDependencyId(dependencyId);
	}

	/**
	* Returns the artefact ID of this dependencies.
	*
	* @return the artefact ID of this dependencies
	*/
	@Override
	public long getArtefactId() {
		return _dependencies.getArtefactId();
	}

	/**
	* Sets the artefact ID of this dependencies.
	*
	* @param artefactId the artefact ID of this dependencies
	*/
	@Override
	public void setArtefactId(long artefactId) {
		_dependencies.setArtefactId(artefactId);
	}

	/**
	* Returns the depends from of this dependencies.
	*
	* @return the depends from of this dependencies
	*/
	@Override
	public long getDependsFrom() {
		return _dependencies.getDependsFrom();
	}

	/**
	* Sets the depends from of this dependencies.
	*
	* @param dependsFrom the depends from of this dependencies
	*/
	@Override
	public void setDependsFrom(long dependsFrom) {
		_dependencies.setDependsFrom(dependsFrom);
	}

	@Override
	public boolean isNew() {
		return _dependencies.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_dependencies.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _dependencies.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_dependencies.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _dependencies.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _dependencies.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_dependencies.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _dependencies.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_dependencies.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_dependencies.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_dependencies.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DependenciesWrapper((Dependencies)_dependencies.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies) {
		return _dependencies.compareTo(dependencies);
	}

	@Override
	public int hashCode() {
		return _dependencies.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.Dependencies> toCacheModel() {
		return _dependencies.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies toEscapedModel() {
		return new DependenciesWrapper(_dependencies.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies toUnescapedModel() {
		return new DependenciesWrapper(_dependencies.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _dependencies.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _dependencies.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_dependencies.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DependenciesWrapper)) {
			return false;
		}

		DependenciesWrapper dependenciesWrapper = (DependenciesWrapper)obj;

		if (Validator.equals(_dependencies, dependenciesWrapper._dependencies)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Dependencies getWrappedDependencies() {
		return _dependencies;
	}

	@Override
	public Dependencies getWrappedModel() {
		return _dependencies;
	}

	@Override
	public void resetOriginalValues() {
		_dependencies.resetOriginalValues();
	}

	private Dependencies _dependencies;
}