/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.eng.rspa.marketplace.artefatto.model.ArtefattoClp;
import it.eng.rspa.marketplace.artefatto.model.ArtifactLevelsClp;
import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizationsClp;
import it.eng.rspa.marketplace.artefatto.model.CategoriaClp;
import it.eng.rspa.marketplace.artefatto.model.DependenciesClp;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArtClp;
import it.eng.rspa.marketplace.artefatto.model.MarketplaceConfClp;
import it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntryClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eng
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"MarketplaceStore-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"MarketplaceStore-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "MarketplaceStore-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(ArtefattoClp.class.getName())) {
			return translateInputArtefatto(oldModel);
		}

		if (oldModelClassName.equals(ArtifactLevelsClp.class.getName())) {
			return translateInputArtifactLevels(oldModel);
		}

		if (oldModelClassName.equals(ArtifactOrganizationsClp.class.getName())) {
			return translateInputArtifactOrganizations(oldModel);
		}

		if (oldModelClassName.equals(CategoriaClp.class.getName())) {
			return translateInputCategoria(oldModel);
		}

		if (oldModelClassName.equals(DependenciesClp.class.getName())) {
			return translateInputDependencies(oldModel);
		}

		if (oldModelClassName.equals(ImmagineArtClp.class.getName())) {
			return translateInputImmagineArt(oldModel);
		}

		if (oldModelClassName.equals(MarketplaceConfClp.class.getName())) {
			return translateInputMarketplaceConf(oldModel);
		}

		if (oldModelClassName.equals(
					PendingNotificationEntryClp.class.getName())) {
			return translateInputPendingNotificationEntry(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputArtefatto(BaseModel<?> oldModel) {
		ArtefattoClp oldClpModel = (ArtefattoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getArtefattoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputArtifactLevels(BaseModel<?> oldModel) {
		ArtifactLevelsClp oldClpModel = (ArtifactLevelsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getArtifactLevelsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputArtifactOrganizations(
		BaseModel<?> oldModel) {
		ArtifactOrganizationsClp oldClpModel = (ArtifactOrganizationsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getArtifactOrganizationsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCategoria(BaseModel<?> oldModel) {
		CategoriaClp oldClpModel = (CategoriaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCategoriaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDependencies(BaseModel<?> oldModel) {
		DependenciesClp oldClpModel = (DependenciesClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDependenciesRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputImmagineArt(BaseModel<?> oldModel) {
		ImmagineArtClp oldClpModel = (ImmagineArtClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getImmagineArtRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputMarketplaceConf(BaseModel<?> oldModel) {
		MarketplaceConfClp oldClpModel = (MarketplaceConfClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getMarketplaceConfRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPendingNotificationEntry(
		BaseModel<?> oldModel) {
		PendingNotificationEntryClp oldClpModel = (PendingNotificationEntryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPendingNotificationEntryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.ArtefattoImpl")) {
			return translateOutputArtefatto(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.ArtifactLevelsImpl")) {
			return translateOutputArtifactLevels(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsImpl")) {
			return translateOutputArtifactOrganizations(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.CategoriaImpl")) {
			return translateOutputCategoria(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.DependenciesImpl")) {
			return translateOutputDependencies(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtImpl")) {
			return translateOutputImmagineArt(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.MarketplaceConfImpl")) {
			return translateOutputMarketplaceConf(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryImpl")) {
			return translateOutputPendingNotificationEntry(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchArtefattoException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchArtifactLevelsException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchCategoriaException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchMarketplaceConfException();
		}

		if (className.equals(
					"it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException")) {
			return new it.eng.rspa.marketplace.artefatto.NoSuchPendingNotificationEntryException();
		}

		return throwable;
	}

	public static Object translateOutputArtefatto(BaseModel<?> oldModel) {
		ArtefattoClp newModel = new ArtefattoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setArtefattoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputArtifactLevels(BaseModel<?> oldModel) {
		ArtifactLevelsClp newModel = new ArtifactLevelsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setArtifactLevelsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputArtifactOrganizations(
		BaseModel<?> oldModel) {
		ArtifactOrganizationsClp newModel = new ArtifactOrganizationsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setArtifactOrganizationsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCategoria(BaseModel<?> oldModel) {
		CategoriaClp newModel = new CategoriaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCategoriaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDependencies(BaseModel<?> oldModel) {
		DependenciesClp newModel = new DependenciesClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDependenciesRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputImmagineArt(BaseModel<?> oldModel) {
		ImmagineArtClp newModel = new ImmagineArtClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setImmagineArtRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputMarketplaceConf(BaseModel<?> oldModel) {
		MarketplaceConfClp newModel = new MarketplaceConfClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setMarketplaceConfRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPendingNotificationEntry(
		BaseModel<?> oldModel) {
		PendingNotificationEntryClp newModel = new PendingNotificationEntryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPendingNotificationEntryRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}