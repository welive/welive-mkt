/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Search. This utility wraps
 * {@link it.eng.rspa.marketplace.artefatto.service.impl.SearchServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author eng
 * @see SearchService
 * @see it.eng.rspa.marketplace.artefatto.service.base.SearchServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.impl.SearchServiceImpl
 * @generated
 */
public class SearchServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.marketplace.artefatto.service.impl.SearchServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllArtefatti(
		java.lang.String pilotID, java.lang.String artefactTypes) {
		return getService().getAllArtefatti(pilotID, artefactTypes);
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllArtefatti(
		java.lang.String pilotID, java.lang.String artefactTypes,
		java.lang.String level) {
		return getService().getAllArtefatti(pilotID, artefactTypes, level);
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllArtefattiV2(
		java.lang.String pilotID, java.lang.String artefactTypes) {
		return getService().getAllArtefattiV2(pilotID, artefactTypes);
	}

	public static com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot) throws java.lang.Exception {
		return getService()
				   .getArtefattiByKeywords(keywords, artefactTypes, pilot);
	}

	public static com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywords(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot, java.lang.String level)
		throws java.lang.Exception {
		return getService()
				   .getArtefattiByKeywords(keywords, artefactTypes, pilot, level);
	}

	public static com.liferay.portal.kernel.json.JSONObject getArtefattiByKeywordsV2(
		java.lang.String keywords, java.lang.String artefactTypes,
		java.lang.String pilot)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.lang.Exception, java.rmi.RemoteException {
		return getService()
				   .getArtefattiByKeywordsV2(keywords, artefactTypes, pilot);
	}

	public static com.liferay.portal.kernel.json.JSONObject getModelsById(
		java.lang.String[] ids) throws java.lang.Exception {
		return getService().getModelsById(ids);
	}

	public static com.liferay.portal.kernel.json.JSONObject getArtefact(
		long artefactid) {
		return getService().getArtefact(artefactid);
	}

	public static com.liferay.portal.kernel.json.JSONObject getArtefactV2(
		long artefactid) {
		return getService().getArtefactV2(artefactid);
	}

	public static com.liferay.portal.kernel.json.JSONObject createDataset(
		java.lang.String datasetId, java.lang.String title,
		java.lang.String description, java.lang.String resourceRdf,
		java.lang.String webpage, java.lang.String ccUserId,
		java.lang.String providerName, java.lang.String language)
		throws java.lang.Exception {
		return getService()
				   .createDataset(datasetId, title, description, resourceRdf,
			webpage, ccUserId, providerName, language);
	}

	public static com.liferay.portal.kernel.json.JSONObject createService(
		java.lang.String title, java.lang.String description,
		java.lang.String category, java.lang.String resourceRdf,
		java.lang.String endpoint, java.lang.String webpage,
		java.lang.String ccUserId, java.lang.String providerName,
		java.lang.String language) throws java.lang.Exception {
		return getService()
				   .createService(title, description, category, resourceRdf,
			endpoint, webpage, ccUserId, providerName, language);
	}

	public static com.liferay.portal.kernel.json.JSONObject setupArtefact(
		java.lang.String body) throws java.lang.Exception {
		return getService().setupArtefact(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateDataset(
		java.lang.String body) throws java.lang.Exception {
		return getService().updateDataset(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject newDataset_v2(
		java.lang.String body) throws java.lang.Exception {
		return getService().newDataset_v2(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject deleteartefact(
		long artefactid) throws java.lang.Exception {
		return getService().deleteartefact(artefactid);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateArtefact(
		long artefactid, java.lang.String body) throws java.lang.Exception {
		return getService().updateArtefact(artefactid, body);
	}

	public static void clearService() {
		_service = null;
	}

	public static SearchService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					SearchService.class.getName());

			if (invokableService instanceof SearchService) {
				_service = (SearchService)invokableService;
			}
			else {
				_service = new SearchServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(SearchServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(SearchService service) {
	}

	private static SearchService _service;
}