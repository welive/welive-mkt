/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class ArtifactLevelsSoap implements Serializable {
	public static ArtifactLevelsSoap toSoapModel(ArtifactLevels model) {
		ArtifactLevelsSoap soapModel = new ArtifactLevelsSoap();

		soapModel.setArtifactId(model.getArtifactId());
		soapModel.setLevel(model.getLevel());
		soapModel.setNotes(model.getNotes());

		return soapModel;
	}

	public static ArtifactLevelsSoap[] toSoapModels(ArtifactLevels[] models) {
		ArtifactLevelsSoap[] soapModels = new ArtifactLevelsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ArtifactLevelsSoap[][] toSoapModels(ArtifactLevels[][] models) {
		ArtifactLevelsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ArtifactLevelsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ArtifactLevelsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ArtifactLevelsSoap[] toSoapModels(List<ArtifactLevels> models) {
		List<ArtifactLevelsSoap> soapModels = new ArrayList<ArtifactLevelsSoap>(models.size());

		for (ArtifactLevels model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ArtifactLevelsSoap[soapModels.size()]);
	}

	public ArtifactLevelsSoap() {
	}

	public long getPrimaryKey() {
		return _artifactId;
	}

	public void setPrimaryKey(long pk) {
		setArtifactId(pk);
	}

	public long getArtifactId() {
		return _artifactId;
	}

	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;
	}

	public int getLevel() {
		return _level;
	}

	public void setLevel(int level) {
		_level = level;
	}

	public String getNotes() {
		return _notes;
	}

	public void setNotes(String notes) {
		_notes = notes;
	}

	private long _artifactId;
	private int _level;
	private String _notes;
}