/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ArtifactLevels}.
 * </p>
 *
 * @author eng
 * @see ArtifactLevels
 * @generated
 */
public class ArtifactLevelsWrapper implements ArtifactLevels,
	ModelWrapper<ArtifactLevels> {
	public ArtifactLevelsWrapper(ArtifactLevels artifactLevels) {
		_artifactLevels = artifactLevels;
	}

	@Override
	public Class<?> getModelClass() {
		return ArtifactLevels.class;
	}

	@Override
	public String getModelClassName() {
		return ArtifactLevels.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("artifactId", getArtifactId());
		attributes.put("level", getLevel());
		attributes.put("notes", getNotes());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}

		Integer level = (Integer)attributes.get("level");

		if (level != null) {
			setLevel(level);
		}

		String notes = (String)attributes.get("notes");

		if (notes != null) {
			setNotes(notes);
		}
	}

	/**
	* Returns the primary key of this artifact levels.
	*
	* @return the primary key of this artifact levels
	*/
	@Override
	public long getPrimaryKey() {
		return _artifactLevels.getPrimaryKey();
	}

	/**
	* Sets the primary key of this artifact levels.
	*
	* @param primaryKey the primary key of this artifact levels
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_artifactLevels.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the artifact ID of this artifact levels.
	*
	* @return the artifact ID of this artifact levels
	*/
	@Override
	public long getArtifactId() {
		return _artifactLevels.getArtifactId();
	}

	/**
	* Sets the artifact ID of this artifact levels.
	*
	* @param artifactId the artifact ID of this artifact levels
	*/
	@Override
	public void setArtifactId(long artifactId) {
		_artifactLevels.setArtifactId(artifactId);
	}

	/**
	* Returns the level of this artifact levels.
	*
	* @return the level of this artifact levels
	*/
	@Override
	public int getLevel() {
		return _artifactLevels.getLevel();
	}

	/**
	* Sets the level of this artifact levels.
	*
	* @param level the level of this artifact levels
	*/
	@Override
	public void setLevel(int level) {
		_artifactLevels.setLevel(level);
	}

	/**
	* Returns the notes of this artifact levels.
	*
	* @return the notes of this artifact levels
	*/
	@Override
	public java.lang.String getNotes() {
		return _artifactLevels.getNotes();
	}

	/**
	* Sets the notes of this artifact levels.
	*
	* @param notes the notes of this artifact levels
	*/
	@Override
	public void setNotes(java.lang.String notes) {
		_artifactLevels.setNotes(notes);
	}

	@Override
	public boolean isNew() {
		return _artifactLevels.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_artifactLevels.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _artifactLevels.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_artifactLevels.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _artifactLevels.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _artifactLevels.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_artifactLevels.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _artifactLevels.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_artifactLevels.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_artifactLevels.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_artifactLevels.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ArtifactLevelsWrapper((ArtifactLevels)_artifactLevels.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.ArtifactLevels artifactLevels) {
		return _artifactLevels.compareTo(artifactLevels);
	}

	@Override
	public int hashCode() {
		return _artifactLevels.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.ArtifactLevels> toCacheModel() {
		return _artifactLevels.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels toEscapedModel() {
		return new ArtifactLevelsWrapper(_artifactLevels.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.ArtifactLevels toUnescapedModel() {
		return new ArtifactLevelsWrapper(_artifactLevels.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _artifactLevels.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _artifactLevels.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_artifactLevels.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ArtifactLevelsWrapper)) {
			return false;
		}

		ArtifactLevelsWrapper artifactLevelsWrapper = (ArtifactLevelsWrapper)obj;

		if (Validator.equals(_artifactLevels,
					artifactLevelsWrapper._artifactLevels)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ArtifactLevels getWrappedArtifactLevels() {
		return _artifactLevels;
	}

	@Override
	public ArtifactLevels getWrappedModel() {
		return _artifactLevels;
	}

	@Override
	public void resetOriginalValues() {
		_artifactLevels.resetOriginalValues();
	}

	private ArtifactLevels _artifactLevels;
}