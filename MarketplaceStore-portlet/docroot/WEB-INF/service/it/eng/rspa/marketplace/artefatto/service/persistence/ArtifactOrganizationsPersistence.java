/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations;

/**
 * The persistence interface for the artifact organizations service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ArtifactOrganizationsPersistenceImpl
 * @see ArtifactOrganizationsUtil
 * @generated
 */
public interface ArtifactOrganizationsPersistence extends BasePersistence<ArtifactOrganizations> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ArtifactOrganizationsUtil} to access the artifact organizations persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId_Status(
		long organizationId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Status_First(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Status_First(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Status_Last(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Status_Last(
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63; and status = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param organizationId the organization ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByOrganizationId_Status_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long organizationId, int status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Removes all the artifact organizationses where organizationId = &#63; and status = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganizationId_Status(long organizationId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact organizationses where organizationId = &#63; and status = &#63;.
	*
	* @param organizationId the organization ID
	* @param status the status
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationId_Status(long organizationId, int status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artifact organizationses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact organizationses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact organizationses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByArtifactId(
		long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the first artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the last artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where artifactId = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByArtifactId_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Removes all the artifact organizationses where artifactId = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact organizationses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public int countByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artifact organizationses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact organizationses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact organizationses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findByOrganizationId(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the first artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the last artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching artifact organizations, or <code>null</code> if a matching artifact organizations could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact organizationses before and after the current artifact organizations in the ordered set where organizationId = &#63;.
	*
	* @param artifactOrganizationsPK the primary key of the current artifact organizations
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations[] findByOrganizationId_PrevAndNext(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK,
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Removes all the artifact organizationses where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact organizationses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the artifact organizations in the entity cache if it is enabled.
	*
	* @param artifactOrganizations the artifact organizations
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations);

	/**
	* Caches the artifact organizationses in the entity cache if it is enabled.
	*
	* @param artifactOrganizationses the artifact organizationses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> artifactOrganizationses);

	/**
	* Creates a new artifact organizations with the primary key. Does not add the artifact organizations to the database.
	*
	* @param artifactOrganizationsPK the primary key for the new artifact organizations
	* @return the new artifact organizations
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations create(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK);

	/**
	* Removes the artifact organizations with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations remove(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the artifact organizations with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException} if it could not be found.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations findByPrimaryKey(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchArtifactOrganizationsException;

	/**
	* Returns the artifact organizations with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations, or <code>null</code> if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchByPrimaryKey(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the artifact organizationses.
	*
	* @return the artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the artifact organizationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the artifact organizationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the artifact organizationses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of artifact organizationses.
	*
	* @return the number of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}