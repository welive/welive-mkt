/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DependenciesLocalService}.
 *
 * @author eng
 * @see DependenciesLocalService
 * @generated
 */
public class DependenciesLocalServiceWrapper implements DependenciesLocalService,
	ServiceWrapper<DependenciesLocalService> {
	public DependenciesLocalServiceWrapper(
		DependenciesLocalService dependenciesLocalService) {
		_dependenciesLocalService = dependenciesLocalService;
	}

	/**
	* Adds the dependencies to the database. Also notifies the appropriate model listeners.
	*
	* @param dependencies the dependencies
	* @return the dependencies that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies addDependencies(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.addDependencies(dependencies);
	}

	/**
	* Creates a new dependencies with the primary key. Does not add the dependencies to the database.
	*
	* @param dependencyId the primary key for the new dependencies
	* @return the new dependencies
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies createDependencies(
		long dependencyId) {
		return _dependenciesLocalService.createDependencies(dependencyId);
	}

	/**
	* Deletes the dependencies with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies that was removed
	* @throws PortalException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies deleteDependencies(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.deleteDependencies(dependencyId);
	}

	/**
	* Deletes the dependencies from the database. Also notifies the appropriate model listeners.
	*
	* @param dependencies the dependencies
	* @return the dependencies that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies deleteDependencies(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.deleteDependencies(dependencies);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _dependenciesLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchDependencies(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.fetchDependencies(dependencyId);
	}

	/**
	* Returns the dependencies with the primary key.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies
	* @throws PortalException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies getDependencies(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.getDependencies(dependencyId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the dependencieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> getDependencieses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.getDependencieses(start, end);
	}

	/**
	* Returns the number of dependencieses.
	*
	* @return the number of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getDependenciesesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.getDependenciesesCount();
	}

	/**
	* Updates the dependencies in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param dependencies the dependencies
	* @return the dependencies that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies updateDependencies(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dependenciesLocalService.updateDependencies(dependencies);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _dependenciesLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_dependenciesLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _dependenciesLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> getDependenciesByArtefactId(
		long artefactId) {
		return _dependenciesLocalService.getDependenciesByArtefactId(artefactId);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> getDependenciesByDependsFrom(
		long dependesFromId) {
		return _dependenciesLocalService.getDependenciesByDependsFrom(dependesFromId);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Dependencies getDependencyByIDsCouple(
		long artefactId, long dependsFromId) {
		return _dependenciesLocalService.getDependencyByIDsCouple(artefactId,
			dependsFromId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public DependenciesLocalService getWrappedDependenciesLocalService() {
		return _dependenciesLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedDependenciesLocalService(
		DependenciesLocalService dependenciesLocalService) {
		_dependenciesLocalService = dependenciesLocalService;
	}

	@Override
	public DependenciesLocalService getWrappedService() {
		return _dependenciesLocalService;
	}

	@Override
	public void setWrappedService(
		DependenciesLocalService dependenciesLocalService) {
		_dependenciesLocalService = dependenciesLocalService;
	}

	private DependenciesLocalService _dependenciesLocalService;
}