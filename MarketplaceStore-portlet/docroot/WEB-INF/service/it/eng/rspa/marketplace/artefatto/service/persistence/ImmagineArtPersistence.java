/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;

/**
 * The persistence interface for the immagine art service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see ImmagineArtPersistenceImpl
 * @see ImmagineArtUtil
 * @generated
 */
public interface ImmagineArtPersistence extends BasePersistence<ImmagineArt> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ImmagineArtUtil} to access the immagine art persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the immagine arts where imageId = &#63;.
	*
	* @param imageId the image ID
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the immagine arts where imageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param imageId the image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the immagine arts where imageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param imageId the image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByImageIdentifier(
		long imageId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByImageIdentifier_First(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the first immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByImageIdentifier_First(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByImageIdentifier_Last(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the last immagine art in the ordered set where imageId = &#63;.
	*
	* @param imageId the image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByImageIdentifier_Last(
		long imageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the immagine arts where imageId = &#63; from the database.
	*
	* @param imageId the image ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByImageIdentifier(long imageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of immagine arts where imageId = &#63;.
	*
	* @param imageId the image ID
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public int countByImageIdentifier(long imageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the immagine arts where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the immagine arts where dlImageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dlImageId the dl image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the immagine arts where dlImageId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dlImageId the dl image ID
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByDlImageIdentifier(
		long dlImageId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByDlImageIdentifier_First(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the first immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByDlImageIdentifier_First(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByDlImageIdentifier_Last(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the last immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByDlImageIdentifier_Last(
		long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the immagine arts before and after the current immagine art in the ordered set where dlImageId = &#63;.
	*
	* @param imageId the primary key of the current immagine art
	* @param dlImageId the dl image ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt[] findByDlImageIdentifier_PrevAndNext(
		long imageId, long dlImageId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Removes all the immagine arts where dlImageId = &#63; from the database.
	*
	* @param dlImageId the dl image ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDlImageIdentifier(long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of immagine arts where dlImageId = &#63;.
	*
	* @param dlImageId the dl image ID
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public int countByDlImageIdentifier(long dlImageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the immagine arts where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @return the matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the immagine arts where artefattoIdent = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefattoIdent the artefatto ident
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the immagine arts where artefattoIdent = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefattoIdent the artefatto ident
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findByArtefattoIdentifier(
		long artefattoIdent, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByArtefattoIdentifier_First(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the first immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByArtefattoIdentifier_First(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByArtefattoIdentifier_Last(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the last immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching immagine art, or <code>null</code> if a matching immagine art could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByArtefattoIdentifier_Last(
		long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the immagine arts before and after the current immagine art in the ordered set where artefattoIdent = &#63;.
	*
	* @param imageId the primary key of the current immagine art
	* @param artefattoIdent the artefatto ident
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt[] findByArtefattoIdentifier_PrevAndNext(
		long imageId, long artefattoIdent,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Removes all the immagine arts where artefattoIdent = &#63; from the database.
	*
	* @param artefattoIdent the artefatto ident
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArtefattoIdentifier(long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of immagine arts where artefattoIdent = &#63;.
	*
	* @param artefattoIdent the artefatto ident
	* @return the number of matching immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public int countByArtefattoIdentifier(long artefattoIdent)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the immagine art in the entity cache if it is enabled.
	*
	* @param immagineArt the immagine art
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt);

	/**
	* Caches the immagine arts in the entity cache if it is enabled.
	*
	* @param immagineArts the immagine arts
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> immagineArts);

	/**
	* Creates a new immagine art with the primary key. Does not add the immagine art to the database.
	*
	* @param imageId the primary key for the new immagine art
	* @return the new immagine art
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt create(
		long imageId);

	/**
	* Removes the immagine art with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt remove(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt updateImpl(
		it.eng.rspa.marketplace.artefatto.model.ImmagineArt immagineArt)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the immagine art with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException} if it could not be found.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt findByPrimaryKey(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchImmagineArtException;

	/**
	* Returns the immagine art with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param imageId the primary key of the immagine art
	* @return the immagine art, or <code>null</code> if a immagine art with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.ImmagineArt fetchByPrimaryKey(
		long imageId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the immagine arts.
	*
	* @return the immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the immagine arts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @return the range of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the immagine arts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ImmagineArtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of immagine arts
	* @param end the upper bound of the range of immagine arts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.ImmagineArt> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the immagine arts from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of immagine arts.
	*
	* @return the number of immagine arts
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}