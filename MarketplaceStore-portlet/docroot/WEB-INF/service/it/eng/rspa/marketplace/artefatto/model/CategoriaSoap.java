/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class CategoriaSoap implements Serializable {
	public static CategoriaSoap toSoapModel(Categoria model) {
		CategoriaSoap soapModel = new CategoriaSoap();

		soapModel.setIdCategoria(model.getIdCategoria());
		soapModel.setNomeCategoria(model.getNomeCategoria());
		soapModel.setSupports(model.getSupports());

		return soapModel;
	}

	public static CategoriaSoap[] toSoapModels(Categoria[] models) {
		CategoriaSoap[] soapModels = new CategoriaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CategoriaSoap[][] toSoapModels(Categoria[][] models) {
		CategoriaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CategoriaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CategoriaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CategoriaSoap[] toSoapModels(List<Categoria> models) {
		List<CategoriaSoap> soapModels = new ArrayList<CategoriaSoap>(models.size());

		for (Categoria model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CategoriaSoap[soapModels.size()]);
	}

	public CategoriaSoap() {
	}

	public long getPrimaryKey() {
		return _idCategoria;
	}

	public void setPrimaryKey(long pk) {
		setIdCategoria(pk);
	}

	public long getIdCategoria() {
		return _idCategoria;
	}

	public void setIdCategoria(long idCategoria) {
		_idCategoria = idCategoria;
	}

	public String getNomeCategoria() {
		return _nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		_nomeCategoria = nomeCategoria;
	}

	public String getSupports() {
		return _supports;
	}

	public void setSupports(String supports) {
		_supports = supports;
	}

	private long _idCategoria;
	private String _nomeCategoria;
	private String _supports;
}