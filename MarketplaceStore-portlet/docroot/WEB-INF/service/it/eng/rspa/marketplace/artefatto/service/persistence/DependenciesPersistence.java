/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.marketplace.artefatto.model.Dependencies;

/**
 * The persistence interface for the dependencies service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see DependenciesPersistenceImpl
 * @see DependenciesUtil
 * @generated
 */
public interface DependenciesPersistence extends BasePersistence<Dependencies> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DependenciesUtil} to access the dependencies persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the dependencieses where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @return the matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dependencieses where artefactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefactId the artefact ID
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dependencieses where artefactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefactId the artefact ID
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByArtefactId_First(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the first dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByArtefactId_First(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByArtefactId_Last(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the last dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByArtefactId_Last(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dependencieses before and after the current dependencies in the ordered set where artefactId = &#63;.
	*
	* @param dependencyId the primary key of the current dependencies
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies[] findByArtefactId_PrevAndNext(
		long dependencyId, long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Removes all the dependencieses where artefactId = &#63; from the database.
	*
	* @param artefactId the artefact ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByArtefactId(long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dependencieses where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public int countByArtefactId(long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the dependencieses where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @return the matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dependencieses where dependsFrom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dependsFrom the depends from
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dependencieses where dependsFrom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dependsFrom the depends from
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByDependsFrom_First(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByDependsFrom_First(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByDependsFrom_Last(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByDependsFrom_Last(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dependencieses before and after the current dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependencyId the primary key of the current dependencies
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies[] findByDependsFrom_PrevAndNext(
		long dependencyId, long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Removes all the dependencieses where dependsFrom = &#63; from the database.
	*
	* @param dependsFrom the depends from
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDependsFrom(long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dependencieses where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public int countByDependsFrom(long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByidsCouple(
		long artefactId, long dependsFrom, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the dependencies where artefactId = &#63; and dependsFrom = &#63; from the database.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the dependencies that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies removeByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the number of dependencieses where artefactId = &#63; and dependsFrom = &#63;.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public int countByidsCouple(long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the dependencies in the entity cache if it is enabled.
	*
	* @param dependencies the dependencies
	*/
	public void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies);

	/**
	* Caches the dependencieses in the entity cache if it is enabled.
	*
	* @param dependencieses the dependencieses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> dependencieses);

	/**
	* Creates a new dependencies with the primary key. Does not add the dependencies to the database.
	*
	* @param dependencyId the primary key for the new dependencies
	* @return the new dependencies
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies create(
		long dependencyId);

	/**
	* Removes the dependencies with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies remove(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	public it.eng.rspa.marketplace.artefatto.model.Dependencies updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the dependencies with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies findByPrimaryKey(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException;

	/**
	* Returns the dependencies with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies, or <code>null</code> if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByPrimaryKey(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the dependencieses.
	*
	* @return the dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the dependencieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the dependencieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the dependencieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of dependencieses.
	*
	* @return the number of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}