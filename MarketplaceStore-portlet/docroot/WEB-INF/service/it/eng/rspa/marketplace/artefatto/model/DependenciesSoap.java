/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class DependenciesSoap implements Serializable {
	public static DependenciesSoap toSoapModel(Dependencies model) {
		DependenciesSoap soapModel = new DependenciesSoap();

		soapModel.setDependencyId(model.getDependencyId());
		soapModel.setArtefactId(model.getArtefactId());
		soapModel.setDependsFrom(model.getDependsFrom());

		return soapModel;
	}

	public static DependenciesSoap[] toSoapModels(Dependencies[] models) {
		DependenciesSoap[] soapModels = new DependenciesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DependenciesSoap[][] toSoapModels(Dependencies[][] models) {
		DependenciesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DependenciesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DependenciesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DependenciesSoap[] toSoapModels(List<Dependencies> models) {
		List<DependenciesSoap> soapModels = new ArrayList<DependenciesSoap>(models.size());

		for (Dependencies model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DependenciesSoap[soapModels.size()]);
	}

	public DependenciesSoap() {
	}

	public long getPrimaryKey() {
		return _dependencyId;
	}

	public void setPrimaryKey(long pk) {
		setDependencyId(pk);
	}

	public long getDependencyId() {
		return _dependencyId;
	}

	public void setDependencyId(long dependencyId) {
		_dependencyId = dependencyId;
	}

	public long getArtefactId() {
		return _artefactId;
	}

	public void setArtefactId(long artefactId) {
		_artefactId = artefactId;
	}

	public long getDependsFrom() {
		return _dependsFrom;
	}

	public void setDependsFrom(long dependsFrom) {
		_dependsFrom = dependsFrom;
	}

	private long _dependencyId;
	private long _artefactId;
	private long _dependsFrom;
}