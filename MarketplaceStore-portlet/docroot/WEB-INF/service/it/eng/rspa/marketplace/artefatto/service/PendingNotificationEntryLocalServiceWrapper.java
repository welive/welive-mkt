/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PendingNotificationEntryLocalService}.
 *
 * @author eng
 * @see PendingNotificationEntryLocalService
 * @generated
 */
public class PendingNotificationEntryLocalServiceWrapper
	implements PendingNotificationEntryLocalService,
		ServiceWrapper<PendingNotificationEntryLocalService> {
	public PendingNotificationEntryLocalServiceWrapper(
		PendingNotificationEntryLocalService pendingNotificationEntryLocalService) {
		_pendingNotificationEntryLocalService = pendingNotificationEntryLocalService;
	}

	/**
	* Adds the pending notification entry to the database. Also notifies the appropriate model listeners.
	*
	* @param pendingNotificationEntry the pending notification entry
	* @return the pending notification entry that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry addPendingNotificationEntry(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.addPendingNotificationEntry(pendingNotificationEntry);
	}

	/**
	* Creates a new pending notification entry with the primary key. Does not add the pending notification entry to the database.
	*
	* @param notificationId the primary key for the new pending notification entry
	* @return the new pending notification entry
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry createPendingNotificationEntry(
		long notificationId) {
		return _pendingNotificationEntryLocalService.createPendingNotificationEntry(notificationId);
	}

	/**
	* Deletes the pending notification entry with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry that was removed
	* @throws PortalException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry deletePendingNotificationEntry(
		long notificationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.deletePendingNotificationEntry(notificationId);
	}

	/**
	* Deletes the pending notification entry from the database. Also notifies the appropriate model listeners.
	*
	* @param pendingNotificationEntry the pending notification entry
	* @return the pending notification entry that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry deletePendingNotificationEntry(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.deletePendingNotificationEntry(pendingNotificationEntry);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pendingNotificationEntryLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry fetchPendingNotificationEntry(
		long notificationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.fetchPendingNotificationEntry(notificationId);
	}

	/**
	* Returns the pending notification entry with the primary key.
	*
	* @param notificationId the primary key of the pending notification entry
	* @return the pending notification entry
	* @throws PortalException if a pending notification entry with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry getPendingNotificationEntry(
		long notificationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.getPendingNotificationEntry(notificationId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the pending notification entries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.PendingNotificationEntryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pending notification entries
	* @param end the upper bound of the range of pending notification entries (not inclusive)
	* @return the range of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry> getPendingNotificationEntries(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.getPendingNotificationEntries(start,
			end);
	}

	/**
	* Returns the number of pending notification entries.
	*
	* @return the number of pending notification entries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getPendingNotificationEntriesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.getPendingNotificationEntriesCount();
	}

	/**
	* Updates the pending notification entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param pendingNotificationEntry the pending notification entry
	* @return the pending notification entry that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry updatePendingNotificationEntry(
		it.eng.rspa.marketplace.artefatto.model.PendingNotificationEntry pendingNotificationEntry)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _pendingNotificationEntryLocalService.updatePendingNotificationEntry(pendingNotificationEntry);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _pendingNotificationEntryLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_pendingNotificationEntryLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _pendingNotificationEntryLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public PendingNotificationEntryLocalService getWrappedPendingNotificationEntryLocalService() {
		return _pendingNotificationEntryLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedPendingNotificationEntryLocalService(
		PendingNotificationEntryLocalService pendingNotificationEntryLocalService) {
		_pendingNotificationEntryLocalService = pendingNotificationEntryLocalService;
	}

	@Override
	public PendingNotificationEntryLocalService getWrappedService() {
		return _pendingNotificationEntryLocalService;
	}

	@Override
	public void setWrappedService(
		PendingNotificationEntryLocalService pendingNotificationEntryLocalService) {
		_pendingNotificationEntryLocalService = pendingNotificationEntryLocalService;
	}

	private PendingNotificationEntryLocalService _pendingNotificationEntryLocalService;
}