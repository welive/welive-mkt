/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ArtifactOrganizations. This utility wraps
 * {@link it.eng.rspa.marketplace.artefatto.service.impl.ArtifactOrganizationsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author eng
 * @see ArtifactOrganizationsLocalService
 * @see it.eng.rspa.marketplace.artefatto.service.base.ArtifactOrganizationsLocalServiceBaseImpl
 * @see it.eng.rspa.marketplace.artefatto.service.impl.ArtifactOrganizationsLocalServiceImpl
 * @generated
 */
public class ArtifactOrganizationsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.marketplace.artefatto.service.impl.ArtifactOrganizationsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the artifact organizations to the database. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizations the artifact organizations
	* @return the artifact organizations that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations addArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addArtifactOrganizations(artifactOrganizations);
	}

	/**
	* Creates a new artifact organizations with the primary key. Does not add the artifact organizations to the database.
	*
	* @param artifactOrganizationsPK the primary key for the new artifact organizations
	* @return the new artifact organizations
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations createArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK) {
		return getService().createArtifactOrganizations(artifactOrganizationsPK);
	}

	/**
	* Deletes the artifact organizations with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations that was removed
	* @throws PortalException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations deleteArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteArtifactOrganizations(artifactOrganizationsPK);
	}

	/**
	* Deletes the artifact organizations from the database. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizations the artifact organizations
	* @return the artifact organizations that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations deleteArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteArtifactOrganizations(artifactOrganizations);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations fetchArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchArtifactOrganizations(artifactOrganizationsPK);
	}

	/**
	* Returns the artifact organizations with the primary key.
	*
	* @param artifactOrganizationsPK the primary key of the artifact organizations
	* @return the artifact organizations
	* @throws PortalException if a artifact organizations with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations getArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK artifactOrganizationsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactOrganizations(artifactOrganizationsPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the artifact organizationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.ArtifactOrganizationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of artifact organizationses
	* @param end the upper bound of the range of artifact organizationses (not inclusive)
	* @return the range of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> getArtifactOrganizationses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactOrganizationses(start, end);
	}

	/**
	* Returns the number of artifact organizationses.
	*
	* @return the number of artifact organizationses
	* @throws SystemException if a system exception occurred
	*/
	public static int getArtifactOrganizationsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactOrganizationsesCount();
	}

	/**
	* Updates the artifact organizations in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param artifactOrganizations the artifact organizations
	* @return the artifact organizations that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations updateArtifactOrganizations(
		it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations artifactOrganizations)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateArtifactOrganizations(artifactOrganizations);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Artefatto> findArtefattoByOrganizationId_Status(
		long organizationId, int status)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			java.rmi.RemoteException {
		return getService()
				   .findArtefattoByOrganizationId_Status(organizationId, status);
	}

	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findArtifactOrganizationsByArtifactId(
		long artifactId) {
		return getService().findArtifactOrganizationsByArtifactId(artifactId);
	}

	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations> findArtifactOrganizationsByOrganizationId(
		long orgId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findArtifactOrganizationsByOrganizationId(orgId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.ArtifactOrganizations addArtifactOrganizations(
		long artifactId, long orgId, int status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addArtifactOrganizations(artifactId, orgId, status);
	}

	public static void deleteArtifactOrganizations(long artifactId) {
		getService().deleteArtifactOrganizations(artifactId);
	}

	public static void clearService() {
		_service = null;
	}

	public static ArtifactOrganizationsLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ArtifactOrganizationsLocalService.class.getName());

			if (invokableLocalService instanceof ArtifactOrganizationsLocalService) {
				_service = (ArtifactOrganizationsLocalService)invokableLocalService;
			}
			else {
				_service = new ArtifactOrganizationsLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ArtifactOrganizationsLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ArtifactOrganizationsLocalService service) {
	}

	private static ArtifactOrganizationsLocalService _service;
}