/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import it.eng.rspa.marketplace.artefatto.service.persistence.ArtifactOrganizationsPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class ArtifactOrganizationsSoap implements Serializable {
	public static ArtifactOrganizationsSoap toSoapModel(
		ArtifactOrganizations model) {
		ArtifactOrganizationsSoap soapModel = new ArtifactOrganizationsSoap();

		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setArtifactId(model.getArtifactId());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static ArtifactOrganizationsSoap[] toSoapModels(
		ArtifactOrganizations[] models) {
		ArtifactOrganizationsSoap[] soapModels = new ArtifactOrganizationsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ArtifactOrganizationsSoap[][] toSoapModels(
		ArtifactOrganizations[][] models) {
		ArtifactOrganizationsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ArtifactOrganizationsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ArtifactOrganizationsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ArtifactOrganizationsSoap[] toSoapModels(
		List<ArtifactOrganizations> models) {
		List<ArtifactOrganizationsSoap> soapModels = new ArrayList<ArtifactOrganizationsSoap>(models.size());

		for (ArtifactOrganizations model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ArtifactOrganizationsSoap[soapModels.size()]);
	}

	public ArtifactOrganizationsSoap() {
	}

	public ArtifactOrganizationsPK getPrimaryKey() {
		return new ArtifactOrganizationsPK(_organizationId, _artifactId);
	}

	public void setPrimaryKey(ArtifactOrganizationsPK pk) {
		setOrganizationId(pk.organizationId);
		setArtifactId(pk.artifactId);
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getArtifactId() {
		return _artifactId;
	}

	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	private long _organizationId;
	private long _artifactId;
	private int _status;
}