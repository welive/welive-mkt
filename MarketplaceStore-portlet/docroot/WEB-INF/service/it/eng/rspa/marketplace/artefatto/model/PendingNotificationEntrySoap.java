/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class PendingNotificationEntrySoap implements Serializable {
	public static PendingNotificationEntrySoap toSoapModel(
		PendingNotificationEntry model) {
		PendingNotificationEntrySoap soapModel = new PendingNotificationEntrySoap();

		soapModel.setNotificationId(model.getNotificationId());
		soapModel.setTimestamp(model.getTimestamp());
		soapModel.setArtefactId(model.getArtefactId());
		soapModel.setAction(model.getAction());
		soapModel.setTargetComponent(model.getTargetComponent());
		soapModel.setOptions(model.getOptions());

		return soapModel;
	}

	public static PendingNotificationEntrySoap[] toSoapModels(
		PendingNotificationEntry[] models) {
		PendingNotificationEntrySoap[] soapModels = new PendingNotificationEntrySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PendingNotificationEntrySoap[][] toSoapModels(
		PendingNotificationEntry[][] models) {
		PendingNotificationEntrySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PendingNotificationEntrySoap[models.length][models[0].length];
		}
		else {
			soapModels = new PendingNotificationEntrySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PendingNotificationEntrySoap[] toSoapModels(
		List<PendingNotificationEntry> models) {
		List<PendingNotificationEntrySoap> soapModels = new ArrayList<PendingNotificationEntrySoap>(models.size());

		for (PendingNotificationEntry model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PendingNotificationEntrySoap[soapModels.size()]);
	}

	public PendingNotificationEntrySoap() {
	}

	public long getPrimaryKey() {
		return _notificationId;
	}

	public void setPrimaryKey(long pk) {
		setNotificationId(pk);
	}

	public long getNotificationId() {
		return _notificationId;
	}

	public void setNotificationId(long notificationId) {
		_notificationId = notificationId;
	}

	public long getTimestamp() {
		return _timestamp;
	}

	public void setTimestamp(long timestamp) {
		_timestamp = timestamp;
	}

	public long getArtefactId() {
		return _artefactId;
	}

	public void setArtefactId(long artefactId) {
		_artefactId = artefactId;
	}

	public String getAction() {
		return _action;
	}

	public void setAction(String action) {
		_action = action;
	}

	public String getTargetComponent() {
		return _targetComponent;
	}

	public void setTargetComponent(String targetComponent) {
		_targetComponent = targetComponent;
	}

	public String getOptions() {
		return _options;
	}

	public void setOptions(String options) {
		_options = options;
	}

	private long _notificationId;
	private long _timestamp;
	private long _artefactId;
	private String _action;
	private String _targetComponent;
	private String _options;
}