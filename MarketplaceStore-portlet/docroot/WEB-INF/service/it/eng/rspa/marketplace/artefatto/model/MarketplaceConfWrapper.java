/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MarketplaceConf}.
 * </p>
 *
 * @author eng
 * @see MarketplaceConf
 * @generated
 */
public class MarketplaceConfWrapper implements MarketplaceConf,
	ModelWrapper<MarketplaceConf> {
	public MarketplaceConfWrapper(MarketplaceConf marketplaceConf) {
		_marketplaceConf = marketplaceConf;
	}

	@Override
	public Class<?> getModelClass() {
		return MarketplaceConf.class;
	}

	@Override
	public String getModelClassName() {
		return MarketplaceConf.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("confId", getConfId());
		attributes.put("key", getKey());
		attributes.put("value", getValue());
		attributes.put("type", getType());
		attributes.put("options", getOptions());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long confId = (Long)attributes.get("confId");

		if (confId != null) {
			setConfId(confId);
		}

		String key = (String)attributes.get("key");

		if (key != null) {
			setKey(key);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String options = (String)attributes.get("options");

		if (options != null) {
			setOptions(options);
		}
	}

	/**
	* Returns the primary key of this marketplace conf.
	*
	* @return the primary key of this marketplace conf
	*/
	@Override
	public long getPrimaryKey() {
		return _marketplaceConf.getPrimaryKey();
	}

	/**
	* Sets the primary key of this marketplace conf.
	*
	* @param primaryKey the primary key of this marketplace conf
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_marketplaceConf.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the conf ID of this marketplace conf.
	*
	* @return the conf ID of this marketplace conf
	*/
	@Override
	public long getConfId() {
		return _marketplaceConf.getConfId();
	}

	/**
	* Sets the conf ID of this marketplace conf.
	*
	* @param confId the conf ID of this marketplace conf
	*/
	@Override
	public void setConfId(long confId) {
		_marketplaceConf.setConfId(confId);
	}

	/**
	* Returns the key of this marketplace conf.
	*
	* @return the key of this marketplace conf
	*/
	@Override
	public java.lang.String getKey() {
		return _marketplaceConf.getKey();
	}

	/**
	* Sets the key of this marketplace conf.
	*
	* @param key the key of this marketplace conf
	*/
	@Override
	public void setKey(java.lang.String key) {
		_marketplaceConf.setKey(key);
	}

	/**
	* Returns the value of this marketplace conf.
	*
	* @return the value of this marketplace conf
	*/
	@Override
	public java.lang.String getValue() {
		return _marketplaceConf.getValue();
	}

	/**
	* Sets the value of this marketplace conf.
	*
	* @param value the value of this marketplace conf
	*/
	@Override
	public void setValue(java.lang.String value) {
		_marketplaceConf.setValue(value);
	}

	/**
	* Returns the type of this marketplace conf.
	*
	* @return the type of this marketplace conf
	*/
	@Override
	public java.lang.String getType() {
		return _marketplaceConf.getType();
	}

	/**
	* Sets the type of this marketplace conf.
	*
	* @param type the type of this marketplace conf
	*/
	@Override
	public void setType(java.lang.String type) {
		_marketplaceConf.setType(type);
	}

	/**
	* Returns the options of this marketplace conf.
	*
	* @return the options of this marketplace conf
	*/
	@Override
	public java.lang.String getOptions() {
		return _marketplaceConf.getOptions();
	}

	/**
	* Sets the options of this marketplace conf.
	*
	* @param options the options of this marketplace conf
	*/
	@Override
	public void setOptions(java.lang.String options) {
		_marketplaceConf.setOptions(options);
	}

	@Override
	public boolean isNew() {
		return _marketplaceConf.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_marketplaceConf.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _marketplaceConf.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_marketplaceConf.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _marketplaceConf.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _marketplaceConf.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_marketplaceConf.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _marketplaceConf.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_marketplaceConf.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_marketplaceConf.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_marketplaceConf.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new MarketplaceConfWrapper((MarketplaceConf)_marketplaceConf.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.MarketplaceConf marketplaceConf) {
		return _marketplaceConf.compareTo(marketplaceConf);
	}

	@Override
	public int hashCode() {
		return _marketplaceConf.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.MarketplaceConf> toCacheModel() {
		return _marketplaceConf.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf toEscapedModel() {
		return new MarketplaceConfWrapper(_marketplaceConf.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.MarketplaceConf toUnescapedModel() {
		return new MarketplaceConfWrapper(_marketplaceConf.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _marketplaceConf.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _marketplaceConf.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_marketplaceConf.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MarketplaceConfWrapper)) {
			return false;
		}

		MarketplaceConfWrapper marketplaceConfWrapper = (MarketplaceConfWrapper)obj;

		if (Validator.equals(_marketplaceConf,
					marketplaceConfWrapper._marketplaceConf)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public MarketplaceConf getWrappedMarketplaceConf() {
		return _marketplaceConf;
	}

	@Override
	public MarketplaceConf getWrappedModel() {
		return _marketplaceConf;
	}

	@Override
	public void resetOriginalValues() {
		_marketplaceConf.resetOriginalValues();
	}

	private MarketplaceConf _marketplaceConf;
}