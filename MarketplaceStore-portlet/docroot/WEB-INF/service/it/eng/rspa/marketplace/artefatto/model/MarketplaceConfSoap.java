/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author eng
 * @generated
 */
public class MarketplaceConfSoap implements Serializable {
	public static MarketplaceConfSoap toSoapModel(MarketplaceConf model) {
		MarketplaceConfSoap soapModel = new MarketplaceConfSoap();

		soapModel.setConfId(model.getConfId());
		soapModel.setKey(model.getKey());
		soapModel.setValue(model.getValue());
		soapModel.setType(model.getType());
		soapModel.setOptions(model.getOptions());

		return soapModel;
	}

	public static MarketplaceConfSoap[] toSoapModels(MarketplaceConf[] models) {
		MarketplaceConfSoap[] soapModels = new MarketplaceConfSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MarketplaceConfSoap[][] toSoapModels(
		MarketplaceConf[][] models) {
		MarketplaceConfSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MarketplaceConfSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MarketplaceConfSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MarketplaceConfSoap[] toSoapModels(
		List<MarketplaceConf> models) {
		List<MarketplaceConfSoap> soapModels = new ArrayList<MarketplaceConfSoap>(models.size());

		for (MarketplaceConf model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MarketplaceConfSoap[soapModels.size()]);
	}

	public MarketplaceConfSoap() {
	}

	public long getPrimaryKey() {
		return _confId;
	}

	public void setPrimaryKey(long pk) {
		setConfId(pk);
	}

	public long getConfId() {
		return _confId;
	}

	public void setConfId(long confId) {
		_confId = confId;
	}

	public String getKey() {
		return _key;
	}

	public void setKey(String key) {
		_key = key;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public String getOptions() {
		return _options;
	}

	public void setOptions(String options) {
		_options = options;
	}

	private long _confId;
	private String _key;
	private String _value;
	private String _type;
	private String _options;
}