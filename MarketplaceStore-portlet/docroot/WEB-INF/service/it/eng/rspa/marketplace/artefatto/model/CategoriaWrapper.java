/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Categoria}.
 * </p>
 *
 * @author eng
 * @see Categoria
 * @generated
 */
public class CategoriaWrapper implements Categoria, ModelWrapper<Categoria> {
	public CategoriaWrapper(Categoria categoria) {
		_categoria = categoria;
	}

	@Override
	public Class<?> getModelClass() {
		return Categoria.class;
	}

	@Override
	public String getModelClassName() {
		return Categoria.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCategoria", getIdCategoria());
		attributes.put("nomeCategoria", getNomeCategoria());
		attributes.put("supports", getSupports());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idCategoria = (Long)attributes.get("idCategoria");

		if (idCategoria != null) {
			setIdCategoria(idCategoria);
		}

		String nomeCategoria = (String)attributes.get("nomeCategoria");

		if (nomeCategoria != null) {
			setNomeCategoria(nomeCategoria);
		}

		String supports = (String)attributes.get("supports");

		if (supports != null) {
			setSupports(supports);
		}
	}

	/**
	* Returns the primary key of this categoria.
	*
	* @return the primary key of this categoria
	*/
	@Override
	public long getPrimaryKey() {
		return _categoria.getPrimaryKey();
	}

	/**
	* Sets the primary key of this categoria.
	*
	* @param primaryKey the primary key of this categoria
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_categoria.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id categoria of this categoria.
	*
	* @return the id categoria of this categoria
	*/
	@Override
	public long getIdCategoria() {
		return _categoria.getIdCategoria();
	}

	/**
	* Sets the id categoria of this categoria.
	*
	* @param idCategoria the id categoria of this categoria
	*/
	@Override
	public void setIdCategoria(long idCategoria) {
		_categoria.setIdCategoria(idCategoria);
	}

	/**
	* Returns the nome categoria of this categoria.
	*
	* @return the nome categoria of this categoria
	*/
	@Override
	public java.lang.String getNomeCategoria() {
		return _categoria.getNomeCategoria();
	}

	/**
	* Sets the nome categoria of this categoria.
	*
	* @param nomeCategoria the nome categoria of this categoria
	*/
	@Override
	public void setNomeCategoria(java.lang.String nomeCategoria) {
		_categoria.setNomeCategoria(nomeCategoria);
	}

	/**
	* Returns the supports of this categoria.
	*
	* @return the supports of this categoria
	*/
	@Override
	public java.lang.String getSupports() {
		return _categoria.getSupports();
	}

	/**
	* Sets the supports of this categoria.
	*
	* @param supports the supports of this categoria
	*/
	@Override
	public void setSupports(java.lang.String supports) {
		_categoria.setSupports(supports);
	}

	@Override
	public boolean isNew() {
		return _categoria.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_categoria.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _categoria.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_categoria.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _categoria.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _categoria.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_categoria.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _categoria.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_categoria.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_categoria.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_categoria.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CategoriaWrapper((Categoria)_categoria.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria) {
		return _categoria.compareTo(categoria);
	}

	@Override
	public int hashCode() {
		return _categoria.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.marketplace.artefatto.model.Categoria> toCacheModel() {
		return _categoria.toCacheModel();
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria toEscapedModel() {
		return new CategoriaWrapper(_categoria.toEscapedModel());
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria toUnescapedModel() {
		return new CategoriaWrapper(_categoria.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _categoria.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _categoria.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_categoria.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CategoriaWrapper)) {
			return false;
		}

		CategoriaWrapper categoriaWrapper = (CategoriaWrapper)obj;

		if (Validator.equals(_categoria, categoriaWrapper._categoria)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Categoria getWrappedCategoria() {
		return _categoria;
	}

	@Override
	public Categoria getWrappedModel() {
		return _categoria;
	}

	@Override
	public void resetOriginalValues() {
		_categoria.resetOriginalValues();
	}

	private Categoria _categoria;
}