/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.marketplace.artefatto.model.Dependencies;

import java.util.List;

/**
 * The persistence utility for the dependencies service. This utility wraps {@link DependenciesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author eng
 * @see DependenciesPersistence
 * @see DependenciesPersistenceImpl
 * @generated
 */
public class DependenciesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Dependencies dependencies) {
		getPersistence().clearCache(dependencies);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Dependencies> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Dependencies> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Dependencies> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Dependencies update(Dependencies dependencies)
		throws SystemException {
		return getPersistence().update(dependencies);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Dependencies update(Dependencies dependencies,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(dependencies, serviceContext);
	}

	/**
	* Returns all the dependencieses where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @return the matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtefactId(artefactId);
	}

	/**
	* Returns a range of all the dependencieses where artefactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefactId the artefact ID
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByArtefactId(artefactId, start, end);
	}

	/**
	* Returns an ordered range of all the dependencieses where artefactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artefactId the artefact ID
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByArtefactId(
		long artefactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByArtefactId(artefactId, start, end, orderByComparator);
	}

	/**
	* Returns the first dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByArtefactId_First(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByArtefactId_First(artefactId, orderByComparator);
	}

	/**
	* Returns the first dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByArtefactId_First(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtefactId_First(artefactId, orderByComparator);
	}

	/**
	* Returns the last dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByArtefactId_Last(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByArtefactId_Last(artefactId, orderByComparator);
	}

	/**
	* Returns the last dependencies in the ordered set where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByArtefactId_Last(
		long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByArtefactId_Last(artefactId, orderByComparator);
	}

	/**
	* Returns the dependencieses before and after the current dependencies in the ordered set where artefactId = &#63;.
	*
	* @param dependencyId the primary key of the current dependencies
	* @param artefactId the artefact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies[] findByArtefactId_PrevAndNext(
		long dependencyId, long artefactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByArtefactId_PrevAndNext(dependencyId, artefactId,
			orderByComparator);
	}

	/**
	* Removes all the dependencieses where artefactId = &#63; from the database.
	*
	* @param artefactId the artefact ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByArtefactId(long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByArtefactId(artefactId);
	}

	/**
	* Returns the number of dependencieses where artefactId = &#63;.
	*
	* @param artefactId the artefact ID
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByArtefactId(long artefactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByArtefactId(artefactId);
	}

	/**
	* Returns all the dependencieses where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @return the matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDependsFrom(dependsFrom);
	}

	/**
	* Returns a range of all the dependencieses where dependsFrom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dependsFrom the depends from
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDependsFrom(dependsFrom, start, end);
	}

	/**
	* Returns an ordered range of all the dependencieses where dependsFrom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dependsFrom the depends from
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findByDependsFrom(
		long dependsFrom, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDependsFrom(dependsFrom, start, end, orderByComparator);
	}

	/**
	* Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByDependsFrom_First(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByDependsFrom_First(dependsFrom, orderByComparator);
	}

	/**
	* Returns the first dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByDependsFrom_First(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDependsFrom_First(dependsFrom, orderByComparator);
	}

	/**
	* Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByDependsFrom_Last(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByDependsFrom_Last(dependsFrom, orderByComparator);
	}

	/**
	* Returns the last dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByDependsFrom_Last(
		long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDependsFrom_Last(dependsFrom, orderByComparator);
	}

	/**
	* Returns the dependencieses before and after the current dependencies in the ordered set where dependsFrom = &#63;.
	*
	* @param dependencyId the primary key of the current dependencies
	* @param dependsFrom the depends from
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies[] findByDependsFrom_PrevAndNext(
		long dependencyId, long dependsFrom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence()
				   .findByDependsFrom_PrevAndNext(dependencyId, dependsFrom,
			orderByComparator);
	}

	/**
	* Removes all the dependencieses where dependsFrom = &#63; from the database.
	*
	* @param dependsFrom the depends from
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByDependsFrom(long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByDependsFrom(dependsFrom);
	}

	/**
	* Returns the number of dependencieses where dependsFrom = &#63;.
	*
	* @param dependsFrom the depends from
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDependsFrom(long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDependsFrom(dependsFrom);
	}

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the matching dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence().findByidsCouple(artefactId, dependsFrom);
	}

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByidsCouple(artefactId, dependsFrom);
	}

	/**
	* Returns the dependencies where artefactId = &#63; and dependsFrom = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching dependencies, or <code>null</code> if a matching dependencies could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByidsCouple(
		long artefactId, long dependsFrom, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByidsCouple(artefactId, dependsFrom, retrieveFromCache);
	}

	/**
	* Removes the dependencies where artefactId = &#63; and dependsFrom = &#63; from the database.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the dependencies that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies removeByidsCouple(
		long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence().removeByidsCouple(artefactId, dependsFrom);
	}

	/**
	* Returns the number of dependencieses where artefactId = &#63; and dependsFrom = &#63;.
	*
	* @param artefactId the artefact ID
	* @param dependsFrom the depends from
	* @return the number of matching dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByidsCouple(long artefactId, long dependsFrom)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByidsCouple(artefactId, dependsFrom);
	}

	/**
	* Caches the dependencies in the entity cache if it is enabled.
	*
	* @param dependencies the dependencies
	*/
	public static void cacheResult(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies) {
		getPersistence().cacheResult(dependencies);
	}

	/**
	* Caches the dependencieses in the entity cache if it is enabled.
	*
	* @param dependencieses the dependencieses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> dependencieses) {
		getPersistence().cacheResult(dependencieses);
	}

	/**
	* Creates a new dependencies with the primary key. Does not add the dependencies to the database.
	*
	* @param dependencyId the primary key for the new dependencies
	* @return the new dependencies
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies create(
		long dependencyId) {
		return getPersistence().create(dependencyId);
	}

	/**
	* Removes the dependencies with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies that was removed
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies remove(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence().remove(dependencyId);
	}

	public static it.eng.rspa.marketplace.artefatto.model.Dependencies updateImpl(
		it.eng.rspa.marketplace.artefatto.model.Dependencies dependencies)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(dependencies);
	}

	/**
	* Returns the dependencies with the primary key or throws a {@link it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException} if it could not be found.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies
	* @throws it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies findByPrimaryKey(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.marketplace.artefatto.NoSuchDependenciesException {
		return getPersistence().findByPrimaryKey(dependencyId);
	}

	/**
	* Returns the dependencies with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dependencyId the primary key of the dependencies
	* @return the dependencies, or <code>null</code> if a dependencies with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.marketplace.artefatto.model.Dependencies fetchByPrimaryKey(
		long dependencyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(dependencyId);
	}

	/**
	* Returns all the dependencieses.
	*
	* @return the dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the dependencieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @return the range of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the dependencieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.DependenciesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dependencieses
	* @param end the upper bound of the range of dependencieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.marketplace.artefatto.model.Dependencies> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the dependencieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of dependencieses.
	*
	* @return the number of dependencieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DependenciesPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DependenciesPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.marketplace.artefatto.service.ClpSerializer.getServletContextName(),
					DependenciesPersistence.class.getName());

			ReferenceRegistry.registerReference(DependenciesUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DependenciesPersistence persistence) {
	}

	private static DependenciesPersistence _persistence;
}