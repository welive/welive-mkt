/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.marketplace.artefatto.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CategoriaLocalService}.
 *
 * @author eng
 * @see CategoriaLocalService
 * @generated
 */
public class CategoriaLocalServiceWrapper implements CategoriaLocalService,
	ServiceWrapper<CategoriaLocalService> {
	public CategoriaLocalServiceWrapper(
		CategoriaLocalService categoriaLocalService) {
		_categoriaLocalService = categoriaLocalService;
	}

	/**
	* Adds the categoria to the database. Also notifies the appropriate model listeners.
	*
	* @param categoria the categoria
	* @return the categoria that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria addCategoria(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.addCategoria(categoria);
	}

	/**
	* Creates a new categoria with the primary key. Does not add the categoria to the database.
	*
	* @param idCategoria the primary key for the new categoria
	* @return the new categoria
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria createCategoria(
		long idCategoria) {
		return _categoriaLocalService.createCategoria(idCategoria);
	}

	/**
	* Deletes the categoria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCategoria the primary key of the categoria
	* @return the categoria that was removed
	* @throws PortalException if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria deleteCategoria(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.deleteCategoria(idCategoria);
	}

	/**
	* Deletes the categoria from the database. Also notifies the appropriate model listeners.
	*
	* @param categoria the categoria
	* @return the categoria that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria deleteCategoria(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.deleteCategoria(categoria);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _categoriaLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria fetchCategoria(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.fetchCategoria(idCategoria);
	}

	/**
	* Returns the categoria with the primary key.
	*
	* @param idCategoria the primary key of the categoria
	* @return the categoria
	* @throws PortalException if a categoria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria getCategoria(
		long idCategoria)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.getCategoria(idCategoria);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the categorias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.marketplace.artefatto.model.impl.CategoriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categorias
	* @param end the upper bound of the range of categorias (not inclusive)
	* @return the range of categorias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> getCategorias(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.getCategorias(start, end);
	}

	/**
	* Returns the number of categorias.
	*
	* @return the number of categorias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCategoriasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.getCategoriasCount();
	}

	/**
	* Updates the categoria in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param categoria the categoria
	* @return the categoria that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria updateCategoria(
		it.eng.rspa.marketplace.artefatto.model.Categoria categoria)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _categoriaLocalService.updateCategoria(categoria);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _categoriaLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_categoriaLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _categoriaLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public it.eng.rspa.marketplace.artefatto.model.Categoria getCategoriaByName(
		java.lang.String nomeCategoria) {
		return _categoriaLocalService.getCategoriaByName(nomeCategoria);
	}

	@Override
	public java.util.List<it.eng.rspa.marketplace.artefatto.model.Categoria> getCategorie() {
		return _categoriaLocalService.getCategorie();
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CategoriaLocalService getWrappedCategoriaLocalService() {
		return _categoriaLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCategoriaLocalService(
		CategoriaLocalService categoriaLocalService) {
		_categoriaLocalService = categoriaLocalService;
	}

	@Override
	public CategoriaLocalService getWrappedService() {
		return _categoriaLocalService;
	}

	@Override
	public void setWrappedService(CategoriaLocalService categoriaLocalService) {
		_categoriaLocalService = categoriaLocalService;
	}

	private CategoriaLocalService _categoriaLocalService;
}