var BusinessRole = {
	"PROVIDER" : "Provider",
	"AUTHOR": "Author",
	"OWNER": "Owner"
}

var Type = {
	"MOBILE": "Mobile Application",
	"WEB": "Web Application",
	"REST": "RESTful Web Service",
	"SOAP": "SOAP Web Service"
}

var DescriptorType = {
	"WADL": "WADL",
	"WSDL": "WSDL",
	"SWAGGER": "SWAGGER"
}

var AccessType = {
	"U": "User",
	"C": "Client",
	"UC": "User and Client"
}

var ProtocolType = {
	"OAUTH2": "OAuth2",
	"OPENID": "OpenID",
	"CAS": "CAS",
	"BASIC": "Basic Authentication",
	"SHIBBOLETH": "Shibboleth"
}

function Offering(title, description, url) {
	this.title = title;
	this.description = description;
	this.url = url;
};

function License(name){
	this.title = name;
	this.description = new String();
	this.url = new String();
};

function Entity(){
	this.title = new String();
	this.page = new String();
	this.mbox = new String();
	this.businessRole = new String();
}

function SecurityMeasure(){
	this.title = new String();
	this.description = new String();
};

function AuthenticationMeasure(){
	
	SecurityMeasure.call(this);
	
	this.protocolType = new String();
	
	this.withIdentityProvider = ["welive"];
	this.requires = [];
}

function Permission(){
	this.accessType = new String();
	this.identifier = new String();
	this.title = new String();
	this.description = new String();
}

function Dependency(id, title){
   this.id = id;
   this.title = title;
}

function IOVariable(){
	this.description = new String();
	this.schema = new String();
	this.type = new String();
	//this.contentType = new String(); // definisce il content type per gli input form data e per l'output
}

function Parameter(){
	IOVariable.call(this);
	
	this._in = new String();
	this.name = new String();
	this.required = new Boolean();
}

function Response(){
	IOVariable.call(this);
	
	this.code = new Number();
}

function Operation(){
	this.consumes = new String();
	this.description = new String();
	this.hasInput = [];
	this.hasOutput = [];
	this.hasSecurityMeasure = null;
	this.method = new String();
	this.path = new String();
	this.produces = ["application/json"];
	this.title = new String();
}

/*Artefact*/
function Artefact(author, date, mbox, pilot){
	
	this.title = new String();
	this.description = new String();
	this._abstract = new String();
	this.pilot = pilot;
	this.created = date;
	this.page = new String();
	this.tags = [];
	this.language = new String();
	
	this.hasBusinessRole = [];
	
	this.hasLegalCondition = [];
	this.hasServiceOffering = [];
	
	var author_ = new Entity();
		author_.businessRole = BusinessRole.AUTHOR;
		author_.title = author;
		author_.mbox = mbox;
		
	this.hasBusinessRole.push(author_);
}

Artefact.prototype.remove = function(setname, k){
	var self = this;
	
	var prevset = self[setname];
	var newset = new Array();
	
	for(var i in prevset){
		if(prevset[i]._uid == k){ continue; }
		else{ newset.push(prevset[i]); }
	}
	self[setname] = newset;
};

Artefact.prototype.removeTag = function(tagname){
	
	var prevtags = this.tags;
	var newtags = new Array();
	for(var t=0; t<prevtags.length; t++){
		if(prevtags[t] == tagname){ continue; }
		else{ newtags.push(prevtags[t]); }
	}
	obj.tags = newtags;
};

Artefact.prototype.add = function(setname, value){
	var k = new Date().getTime();
	this[setname][k] = value;
	return k;
};

Artefact.prototype.addTag = function(tagname){
	this.tags.push(tagname);
};

/*Mashuppable*/
function Mashuppable(author, date, mbox, pilot){
	Artefact.call(this, author, date, mbox, pilot);
}
Mashuppable.prototype = Object.create(Artefact.prototype);
Mashuppable.prototype.constructor = Mashuppable;

/*Dataset*/
function Dataset(author, date, mbox, pilot){
	Mashuppable.call(this, author, date, mbox, pilot);
}
Dataset.prototype = Object.create(Mashuppable.prototype);
Dataset.prototype.constructor = Dataset;

/*BuildingBlock*/
function BuildingBlock(author, date, mbox, pilot, model){
	Mashuppable.call(this, author, date, mbox, pilot);
	
	this.descriptorType = new String();
	this.descriptorUrl = new String();
	this.type = new String();
	
	this.hasOperation = [];
	this.uses = [];
	if(typeof model !== 'undefined'){
		console.log(model);
		for(var k in model){
			if(k == 'created' || k == 'pilot'){ continue; }
			else if(k == 'hasBusinessRole'){
				for(var kk in model[k]){
					if(model[k][kk].businessRole.toUpperCase() == "AUTHOR") continue;
					this[k].push(model[k][kk]);
				}
			}
			//else if() {}
			else{ this[k] = model[k]; }
		}
	}
}
BuildingBlock.prototype = Object.create(Mashuppable.prototype);
BuildingBlock.prototype.constructor = BuildingBlock;


/*PublicServiceApplication*/
//function PublicServiceApplication(author, date, mbox, pilot){
//	Artefact.call(this, author, date, mbox, pilot);
//	
//	this.type = new String();
//	this.url = new String();
//	
//	this.uses = [];
//}
function PublicServiceApplication(author, date, mbox, pilot, model){
	Artefact.call(this, author, date, mbox, pilot);
	
	this.type = new String();
	this.url = new String();
	
	this.uses = [];
	if(typeof model !== 'undefined'){
		for(var k in model){
			if(k == 'created' || k == 'pilot'){ continue; }
			else if(k == 'hasBusinessRole'){
				for(var kk in model[k]){
					if(model[k][kk].businessRole.toUpperCase() == "AUTHOR") continue;
					this[k].push(model[k][kk]);
				}
			}
			else{ this[k] = model[k]; }
		}
	}
}
PublicServiceApplication.prototype = Object.create(Artefact.prototype);
PublicServiceApplication.prototype.constructor = PublicServiceApplication;


function executeFunctionByName(functionName, context, arg) {
	try{ 
		return context[functionName](arg); 
	}
	catch(error){ 
		console.error(error); 
	}
}

function yuiTojQuery(yuiNode){
	return $(yuiNode.getDOMNode());
}

function getProgressBar(yuiNode){
	var jQueryNode = yuiTojQuery(yuiNode);
	return jQueryNode.closest('.upload-wrapper').find('.progress');
}

function showProgressBar(yuiNode){
	var progressbar = getProgressBar(yuiNode);
	progressbar.removeClass('hide');
}

function hideProgressBar(yuiNode){
	var progressbar = getProgressBar(yuiNode);
	progressbar.addClass('hide');
}

function uploadFileClickHandler(){
	var input = $(this).closest('.upload-wrapper').find('.upload-image input[type=file]').first();
	input.trigger('click');
}

function setupPushpins(container){
	if($( document ).width()>600){
		var wrapper = container.parent();
		wrapper.css("height", "1px");
		
		var dynamicWidth = wrapper.width();
		var rangebottom = wrapper.closest('.row').offset().top + wrapper.closest('.row').outerHeight(true);
		var pinnableHeight = container.outerHeight(true);
		container.pushpin({ 
			top: wrapper.offset().top,
			offset: 40,
			bottom: (rangebottom - pinnableHeight)
		});
		
		wrapper.find('.pin-top').width(dynamicWidth);
		wrapper.find('.pinned').width(dynamicWidth);
		wrapper.find('.pin-bottom').width(dynamicWidth);
	}
}

var namespace = "";
$(document).ready(function(){
	namespace = $('#portletnamespace').val();
});

/*Prevents the submission of input values longer than expressed max-length */
$(document).on('keypress', 'input', function(event){
	var maxlength = $(this).attr('length');
	if(!(typeof maxlength === 'undefined') && $(this).val().length>=maxlength){
		return false;
	}
});

function htmlDecode(value){ 
	return $('<div/>').html(value).text(); 
}

function refreshPushpins(){
//	setupPushpins($('#rightcontainer'));
	setupPushpins($('#leftcontainer'));
}

/* Disable breadcrumb to avoid page loading resulting in parameter loss and JS error */
function disableLastBreadcrumb(){
	
	var crumb = $('.active.last.breadcrumb-truncate');
	crumb.removeClass("active");
	crumb.find('a').removeAttr('href');
}