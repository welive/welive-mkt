function updateBusinessRole(e){
	
	var roleid = e.attr('data-role');
	var role = BusinessRole[roleid];
	
	var entity = new Entity();
	entity.title = e.val();
	entity.businessRole = role;
	
	var brid = e.attr('data-brid');
	if (typeof brid !== typeof undefined && brid !== false) {
		//is an update
		//remove the old element
		obj.hasBusinessRole.splice(brid, 1);
	}
	
	if(entity.title.trim().length > 0){
		var new_length = obj.hasBusinessRole.push(entity);
		var pos = new_length -1;
		e.attr('data-brid', pos);
	}
	else{ e.removeAttr('data-brid'); }
	
}

function show_modal_message(msg, iserror){
	var modal = $('#modal-confirm');
	modal.find('.modal-content .confirmmessage').text(msg);
	
	if(iserror){
		modal.find('.modal-content .erroricon').removeClass('hide');
		modal.find('.modal-content h5.green-text').removeClass('green-text').addClass('red-text');
		modal.find('.modal-content .confirmationicon').addClass('hide');
	}
	else{
		modal.find('.modal-content .erroricon').addClass('hide');
		modal.find('.modal-content h5.red-text').removeClass('red-text').addClass('green-text');
		modal.find('.modal-content .confirmationicon').removeClass('hide');
	}
	
	$('#modal-confirm-trigger').click();
}

function msgLevel(level) {
	msg = "If you submit this artefact it will be evaluated with a Level #.";	
	msg = msg.replace("#",level);
	return msg;
}

function show_level_message(problems,level){
	$('#missing-fields').empty();
	var ul = document.getElementById("missing-fields");
	var br = document.createElement("br");
	var modal = $('#modal-level-confirm');
	var msg = msgLevel(level);
	modal.find('.modal-content .confirmmessage').text(msg);
	
	for(var i = 0; i< problems.length; i++) {
		 var li = document.createElement("li");
		 var tmp = '#'+problems[i];
		 var tmp = ".missingfields "+tmp;
		 var text = $(''+tmp).text();
		  li.appendChild(document.createTextNode(text));
		  ul.appendChild(li);
	}

	$('#modal-level-confirm-trigger').click();
}

function show_confirmation_message(msg) {
	
	return confirm(msg);
}

/*
function show_confirmation_message(msg) {
	
	//console.log(confirm(msg));
	var modal = $('#modal-confirm');
	modal.find('.modal-content .confirmmessage').text(msg);
	
	modal.find('.modal-content .erroricon').removeClass('hide');
	modal.find('.modal-content h5.green-text').removeClass('green-text').addClass('red-text');
	modal.find('.modal-content .confirmationicon').addClass('hide');
	
	$('#modal-confirm-trigger').click();
	
	return true;
}

*/
/*
function setDefaultOutput() {
	//If an operation does not contain any output point, insert a 200 ok Output
	var defaultOutput = new Response();
	
	defaultOutput.code = "200";
	defaultOutput.schema = "{}";
	defaultOutput.description = "";
	return defaultOutput;
} 
*/

function isvalid_operations(){
	
	if(isbb && obj.hasOperation.length <= 0){
		/*
		var msg = $('.errormsgs #nooperation').text();
		show_modal_message(msg, true);
		
		scrollto($('#add-intpoint'), true);
		*/
		return false;
	}
	else{
		for(var i in obj.hasOperation){
			var curr = obj.hasOperation[i];
			if(typeof curr.title === 'undefined' || curr.title == null || curr.title.trim().length<=0){
				var msg = $('.errormsgs #nooperationname').text();
				show_modal_message(msg, true);
				
				var domnode = $('#operations-items-list').find('blockquote[data-itemid="'+curr._uid+'"]').find('.optitle');
				scrollto(domnode, true);
				
				return false;
			}
			if(typeof curr.method === 'undefined' || curr.method == null || curr.method.trim().length<=0){
				var msg = $('.errormsgs #nooperationmethod').text();
				show_modal_message(msg, true);
				
				var domnode = $('#operations-items-list').find('blockquote[data-itemid="'+curr._uid+'"]').find('.opmethod');
				scrollto(domnode, true);
				
				return false;
			}
			if(typeof curr.endpoint === 'undefined' || curr.endpoint == null ||  curr.endpoint.trim().length<=0){
				var msg = $('.errormsgs #nooperationpath').text();
				show_modal_message(msg, true);
				
				var domnode = $('#operations-items-list').find('blockquote[data-itemid="'+curr._uid+'"]').find('.oppath');
				scrollto(domnode, true);
				
				return false;
			} else {
				
				var re = new RegExp("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
				
				curr.endpoint = curr.endpoint.replace('/\</g','{');
				curr.endpoint = curr.endpoint.replace('/\>/g','}');
				
				if (!re.test(curr.endpoint) || curr.endpoint.indexOf('<') >= 0 || curr.endpoint.indexOf('>') >=0) {
				    console.log("Invalid URL");
				    //TODO new entry nel dizionario
				    show_modal_message("The operation's endpoint must be valid", true);
				    //alert("Please, insert a valid URL");
				    return false;
				} 
			}
			
			if(typeof curr.hasOutput === 'undefined' || curr.hasOutput == null || curr.hasOutput.length<=0){
				/*
				var msg = $('.errormsgs #nooperationoutput').text();
				show_modal_message(msg, true);
				
				var domnode = $('#operations-items-list').find('blockquote[data-itemid="'+curr._uid+'"]').find('.outputs[href="#modal2"]');
				scrollto(domnode, false);
				*/
				/*
				curr.hasOutput = new Array();				
				curr.hasOutput.push(setDefaultOutput());
				appendResponse(setDefaultOutput());				
				console.log("Default Output inserted");
				*/
			}
		}
	}
	
	return true;
}


function isvalid_offerings(){
	for(var i in obj.hasServiceOffering){
		var curr = obj.hasServiceOffering[i];
		if(typeof curr.title === 'undefined' || curr.title.trim().length<=0){
			//Se oltre al titolo mancano tutte le altre informazioni
			if((typeof curr.description === 'undefined' || curr.description == null || curr.description.trim().length<=0 )
					&& (typeof curr.url === 'undefined' || curr.url == null || curr.url.trim().length<=0)){
				//Elimino la entry
				obj.hasServiceOffering.splice(i,1);
			}
			else{
				//Se manca solo il titolo mando un messaggio di errore
				var msg = $('.errormsgs #noofferingtitle').text();
				show_modal_message(msg, true);
				
				var domnode = $('#offerings-items-list').find('blockquote[data-itemid="'+curr._uid+'"]').find('input[name="title"]');
				scrollto(domnode, true);
				
				return false;
			}
		}
	}
	
	return true;
}


function isvalid_licenses(){
	for(var i in obj.hasLegalCondition){
		var curr = obj.hasLegalCondition[i];
		if(typeof curr.title === 'undefined'|| curr.title == null|| curr.title.trim().length<=0){
			obj.hasLegalCondition.splice(i,1);
		}
	}
	
	return true;
}

function ninofun(e){
	var form = $(e);
	fixConsumesProduces();
	form.find('.artid').val(idArtefatto);
	form.find('.model').val(JSON.stringify(obj));
}


function isSecure() {	 
	if(isbb && obj.hasOperation.length <= 0) {
		return false;
	} 
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		if(curr.hasSecurityMeasure != null && typeof obj.hasOperation.hasSecurityMeasure !== undefined) 
				return true;
	}
	
	return false;
}

function levelCalculator(levels) {
	var ArtefactLevel = 0;
		
	for (var i = 0; i< levels.length; i++) {
		if(levels[i])
			ArtefactLevel++;
		else 
			break;			
	}	
	
	return ArtefactLevel;
}

function confirmProblems(problems) {
	//Message Composition
	var msg = $('.errormsgs #artifact-confirm').text();
	/*for(var i = 0; i< problems.length; i++) {
		msg+="\n"+problems[i]+"  ";
	}*/
	
	return msg;
}



function ismodelvalid(e){
	var form = $(e);
	console.log(JSON.stringify(obj));
	
	$('#mainactions button').prop('disabled', true);
	form.find('button').find('.fa-spin').removeClass('hide');
	
	//The title is a requirement
	if(typeof obj.title === 'undefined' || obj.title == null ||  obj.title.trim().length <= 0){
		var msg = $('.errormsgs #notitle').text();
		show_modal_message(msg, true);
		
		scrollto($('#metamodel-field-title'), true);
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	

	if(typeof obj.type === 'undefined' || obj.type == null ||  obj.type.trim().length <= 0){
		var msg = $('.errormsgs #notype').text();
		show_modal_message(msg, true);
		
		scrollto($('#metamodel-field-type'), true);
		
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}

	
	if(typeof obj.language === 'undefined' || obj.language == null ||  obj.language.trim().length <= 0){
		var msg = $('.errormsgs #nolang').text();
		show_modal_message(msg, true);		
		scrollto($('#metamodel-field-language'), true);		
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}


	
	if(isbb && !isvalid_operations()){
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	
	if(ispsa && (typeof obj.url === 'undefined' || obj.url == null ||  obj.url.trim().length <= 0)){
		var msg = $('.errormsgs #nourl').text();
		show_modal_message(msg, true);
		
		scrollto($('#metamodel-field-url'), true);
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;

	}
	
	
	if(!isvalid_offerings()){
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	
	if(!isvalid_licenses()){
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	
	form.find('.artid').val(idArtefatto);
	form.find('.model').val(JSON.stringify(obj));
	
	console.log("form "+form.attr('id') +" is valid");

	return true; 
}

function fireSubmit() {
//	$('#publishform').submit(function(ev) {
//		console.log("FIRE!!");
//	    ev.preventDefault(); 
//	    this.submit();
//	});
	fixAndSend();
	
	$('#publishform').removeAttr('onsubmit');
	$('#publish-click').click();
//	$('#publishform').unbind('submit');
//	$('#publishform').submit(function(ev) {
//		console.log("FIRE!!");
//	    ev.preventDefault(); 
//	    this.submit();
//	});
}

function isProviderValid() {	
	for(var i in obj.hasBusinessRole) {
		var entry = obj.hasBusinessRole[i];
		if(entry.businessRole == "Provider") {
			return true;
		}
	}
	return false;
}

function fixBody() {
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		for(var j in curr.hasInput) {
			var par = curr.hasInput[j];
			if(par._in == 'body' && (curr.method == "POST" || curr.method == "PUT" || curr.method == "DELETE" || curr.method == "PATCH" || curr.method =="UPDATE")) {
				par.name ="body";
			}
		}
	}
}

function fixSecurity() {
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i]; 
		if(curr.hasSecurityMeasure != null && (curr.hasSecurityMeasure.protocolType == "OAUTH2" || curr.hasSecurityMeasure.protocolType == "BASIC")) {
			var securityProtocol = curr.hasSecurityMeasure.protocolType;
			var msg = "";
			if(securityProtocol=="BASIC") {
				msg="\"Basic \" + a user access token.";
			}else {
				msg="\"Bearer \" + a user access token."; 
			}
				
			var flag = 0;
			for(var j in curr.hasInput) {
				var currInput = curr.hasInput[j];
				if(currInput.name.toLowerCase() == "authorization") flag = 1;
			}
			if(flag == 0) {
				console.log("Inserisco l'header auth");
				var p = new Parameter();
				p._in = "header";
				p.name = "Authorization";
				p.required = true;
				p.type ="string";
				p.schema = "{}";
				p.description = msg;
				
				obj.hasOperation[i].hasInput.push(p);
			}
		}	
			
	}
}

function fixConsumesProduces() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		
		
		if(curr.consumes == "other") {
			obj.hasOperation[i].consumes = "";
			if(typeof curr.other_consumes !== 'undefined') {
				if(curr.other_consumes.trim().length > 0)  {
					obj.hasOperation[i].consumes = curr.other_consumes.trim();					
				}
				delete obj.hasOperation[i].other_consumes;
			} 
		}
		

		if(curr.produces.includes("other")) {			
			var i = curr.produces.indexOf("other");				
			if(i != -1)
				curr.produces.splice(i, 1);
			if(typeof curr.other_produces !== 'undefined') {				
				if(curr.other_produces.trim().length > 0)  {
					var othArr = curr.other_produces.trim().split(";");

					//curr.produces.push(curr.other_produces.trim());
					curr.produces = curr.produces.concat(othArr);
				}
				delete curr.other_produces;
			} 
		}

	}
	
}

function isInputVcCompliant() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		for(var j in curr.hasInput) {
			var par = curr.hasInput[j];
			if(par._in == 'formData') {
				return false;
			}
		}
	}

	
	return true;
}


function isBodyVcCompliant() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		for(var j in curr.hasInput) {
			var par = curr.hasInput[j];
			if(par._in == 'body' && par.contentType == "other") {
				return false;
			}
		}
	}
	
	return true;	
}

function isBodyVcCompliantV2() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		if(curr.consumes == 'other' || curr.consumes == 'multipart/form-data' || curr.consumes == 'application/x-www-form-urlencoded') {
			return false;
		}
	}
	
	return true;	
}

function isOutputVcCompliant() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		for(var j in curr.hasOutput) {
			var par = curr.hasOutput[j];
			if(par.contentType != "application/json" && par.contentType != "application/xml" && par.contentType != "text") {
				return false;
			}
		}
	}
	
	return true;	
}

function isOutputVcCompliantV2() {
	
	for(var i in obj.hasOperation) {
		var curr = obj.hasOperation[i];
		for(var j in curr.produces) {
			var par = curr.produces[j];
			if(par == 'other' || par == 'application/octet-stream') {
				return false;
			}
		}
	}
	
	return true;	
}


function isBbValid(e) {
	var form = $(e);
	var levelsCheck = Array.apply(null, Array(5)).map(Number.prototype.valueOf,1); // Array initializzation [1,1,1,1,1]
	var problems = [];
	
	//The title is a blocking-requirement
	if(typeof obj.title === 'undefined' || obj.title == null ||  obj.title.trim().length <= 0){
		var msg = $('.errormsgs #notitle').text();
		show_modal_message(msg, true);
		
		scrollto($('#metamodel-field-title'), true);
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	
	if(obj.type != "RESTful Web Service" && obj.type != "SOAP Web Service" ) {
		//Type is a requirement for a Level2 Artefact
		console.log("LEVEL 2 PROBLEM: PROTOCOL MISSING");
		levelsCheck[1] = 0;	
		problems.push("protocol-missing");
	}
	
	if(typeof obj.language === 'undefined' || obj.language == null ||  obj.language.trim().length <= 0) {
		//Language is a requirement for a Level3 Artefact
		console.log("LEVEL 3 PROBLEM: LANGUAGE MISSING");
		levelsCheck[2] = 0;
		problems.push("language-missing");
	}
	
	if(typeof obj.description === 'undefined' || obj.description == null ||  obj.description.trim().length <= 0) {
		//Language is a requirement for a Level3 Artefact
		console.log("LEVEL 3 PROBLEM: DESCRIPTION MISSING");
		levelsCheck[2] = 0;
		problems.push("description-missing");
	}
	
	//TODO Provider Check
	if(!isProviderValid()) {
		//Language is a requirement for a Level3 Artefact
		console.log("LEVEL 3 PROBLEM: PROVIDER MISSING");
		levelsCheck[2] = 0;
		problems.push("provider-missing");
	}
	
	if( obj.page.length > 0 ){	
		var re = new RegExp("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
		if (!re.test(obj.page)) {
		    console.log("Invalid URL");
		    //TODO new entry nel dizionario
		    show_modal_message("The documentation URL must be valid", true);
		    //alert("Please, insert a valid URL");
		    return false;
		} 
		//The Url, if filled, must be valid
	}	
	/*		
	if(!isSecure()) {
		//A Security Protocol is a requirement for a Level3 Artefact
		console.log("LEVEL 3 PROBLEM: SECURITY PROTOCOL MISSING");
		levelsCheck[2] = 0;
		problems.push("security-missing");
	}
	*/
	
	if(!isvalid_operations()){
		//Basics Operations are a requirements for a Level3 Artefact
		console.log("LEVEL 3 PROBLEM: BASIC OPERATION MISSING");
		levelsCheck[2] = 0;
		problems.push("basic-operation-missing");
		//If operations are presents , but they're invalid
		if(obj.hasOperation.length > 0) return false;
	}
	
	
	//Offerings and licenses are not a requirements but ,if present, they must be valid
	if(!isvalid_offerings()){
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	
	if(!isvalid_licenses()){
		form.find('button .fa-spin').addClass('hide');
		$('#mainactions').find('button').prop('disabled', false)
		return false;
	}
	/*
	if(!isInputVcCompliant()) {
		levelsCheck[2] = 0;
		console.log("LEVEL 3 PROBLEM: FORM DATA DETECTED");		
		problems.push("form-data-detected");	
	}
	*/
	if(!isBodyVcCompliantV2()) {
		levelsCheck[2] = 0;
		console.log("LEVEL 3 PROBLEM: INCOMPATIBLE BODY CONTENT TYPE DETECTED");		
		problems.push("input-content-type-incompatible");	
	}
	
	if(!isOutputVcCompliantV2()) {
		
		levelsCheck[2] = 0;
		console.log("LEVEL 3 PROBLEM: INCOMPATIBLE OUTPUT CONTENT TYPE DETECTED");
		problems.push("output-content-type-incompatible");	
	}
	
	//----------OTHER SPECS----------------
		levelsCheck[3] = 0;
		levelsCheck[4] = 0;
	//_____________________________________
	
	console.log("ARTEFACT LEVELS ARRAY: "+levelsCheck);
	console.log("ARTEFACT LEVEL: " + levelCalculator(levelsCheck));
	console.log("--JSON--" + JSON.stringify(obj) + "--FINE--");
	
	form.find('.artid').val(idArtefatto);
	form.find('.model').val(JSON.stringify(obj));
	form.find('.level').val(levelCalculator(levelsCheck));
	
	//Confirmation Message composition
	var msg = "";
	if(problems.length>0) msg = confirmProblems(problems);
	var artLevel = levelCalculator(levelsCheck);
	if(artLevel<3){ 
		show_level_message(problems,artLevel);
		return false;
	} else {
		//Fix
		fixAndSend();
		return true;
	}	
	
}


function fixAndSend() {
	var form = $("#publishform");
	
	fixConsumesProduces();
	fixBody();
	fixSecurity();
	form.find('.model').val(JSON.stringify(obj));
}

function isValid(e) {
	return isbb ? isBbValid(e) : ismodelvalid(e);
}


function scrollto(domnode, highlight){
	$("html, body").animate({scrollTop: domnode.offset().top}, '1000');
	
	domnode.focus();
	if(highlight){
		if(domnode.is('input'))
			domnode.addClass('invalid');
		else if(domnode.is('button, a'))
			domnode.click();
		else if(domnode.is('select'))
			domnode.css('border-color', 'red');
	}
}