function updateBreadcrumbs(pagename){
	
	var breadcrumbs = jQuery('#breadcrumbs').find('li');
	var crumb = breadcrumbs.last();
	
	if(crumb.find('a').html() == pagename)
		return;
	
	var newCrumb = crumb.clone(); 
	crumb.removeClass('only');
	crumb.removeClass('active');
	crumb.removeClass('last');
	
	var newCrumbAnchor = newCrumb.find('a');
	newCrumbAnchor.attr('href', '#');
	newCrumbAnchor.html(pagename);
	
	breadcrumbs.parent().append(newCrumb);
	
}

function createBadge(artefact){
	var template = jQuery('#template').first();
    var li = template.clone();
    li.removeAttr("id");
    li.css('margin-top', '20px'); 
    li.css('opacity', '0');
    li.find('a').attr('href',artefact.url);
    li.find('.cover_art').attr('src', artefact.imgUrl);
    li.find('.title_art').html(artefact.title);
    li.find('.type').html(artefact.type+"&nbsp;");
    var ratingsection = li.find('.rating_art');
    var fastar = ratingsection.find(".fa-star").clone();
    var fastaro = ratingsection.find(".fa-star-o").clone();
    ratingsection.empty();
    for(var i=0; i<artefact.score; i++){
    	ratingsection.append("&nbsp;");
    	ratingsection.append(fastar.clone());
    }
    var remainder = 5-artefact.score;
	for(var i=0; i<remainder; i++){
		ratingsection.append("&nbsp;");
		ratingsection.append(fastaro.clone());
	}
	
	if (artefact.level > 0){
		
		var levImg = li.find('.level_img');
		levImg.removeClass('hide');
		levImg.attr('src', levImg.data('contextpath')+'\\icons\\'+artefact.level+'_level.png');
		levImg.attr('data-tooltip',  levImg.data('tooltipbase')+" "+artefact.level);
	}
	return li;
}

function appendMoreArtefacts(json){

	var artefactsArray = json.artefacts;
	
	jQuery.each(artefactsArray, function(index, artefact) {
		var li = createBadge(artefact);
		jQuery('#recentList').append(li);
		li.animate({ 'opacity': 1,  'margin-top': "10px" }, 500);
	});
}

function appendRecommendedArtefacts(json){
	var artefactsArray = json.artefacts;
	if(artefactsArray.length==0){
		$('#no-recommended-alert').animate({ 'opacity': 1}, 300);
		return;
	}

	jQuery('#no-recommended-alert').animate({ 'opacity': 0}, 300);
	jQuery.each(artefactsArray, function(index, artefact) {
		var li = createBadge(artefact);
		jQuery('#recommended').append(li);
		li.animate({ 'opacity': 1,  'margin-top': "10px" }, 500);
	});
}

function onChangeType(anchor){
	jQuery('.tab').addClass('disabled');
	type = jQuery(anchor).attr('data-type');
	reinit(recommendationsEnabled);
	jQuery('.tab').removeClass('disabled');
}

function loadArtefacts(url, start, isSscroll){
	if(!ajaxAllowed){
		return;
	}
	
	
	$('.recent-section .async-load-spinner').removeClass('hide');
	var namespace = jQuery('#portletnamespace').val();
	
	if (isSscroll==true)
		ajaxAllowed=false;
		
	var input = {};
		input[namespace+"start_id"] = start;
		input[namespace+"types"] = types;
		input[namespace+"action"] = 'moreArtefacts';
		input[namespace+"pilots"] = selectedPilots;
		input[namespace+"orderby"] = orderby;
		input[namespace+"searchby"] = searchby;
	
	/*with this control, if start a new Ajax request, the earlier is aborted */	
	if(!(recent_request == null)){
		recent_request.abort();
		recent_request = null;
	}
		
	recent_request = jQuery.post(url,input)
		.done(function(response){
				var resp = JSON.parse(response);
				if(resp.empty){
					$('#no-artefacts-alert').animate({ 'opacity': 1}, 300);
				}
				finished = resp.finished;
				start_index = resp.next_index;
				$('.recent-section .async-load-spinner').addClass('hide');
				appendMoreArtefacts(resp);
				ajaxAllowed = true;
				recent_request = null;
				$('.tooltipped').tooltip({delay: 50});
		});
}

var recommendationinvoked = false;

function getRecommendations() {
	
	setTimeout(function(){ 
					if(!recommendationinvoked){
						recommendationinvoked = true;
						loadRecommendedArtefacts(null, null);
					}
				}, 
				10000);
	
    if (navigator.geolocation) { 
    	navigator.geolocation.getCurrentPosition(geolocatedRecommendation, simpleRecommendation, {timeout: 10000}); 
    } 
    else { console.warn("Geolocation is not supported by this browser."); }
}

function geolocatedRecommendation(position) {
		recommendationinvoked = true;
		var data = new Object();
		data[namespace+'action'] = 'updatelkl';
		data[namespace+'lat'] = position.coords.latitude;
		data[namespace+'lng'] = position.coords.longitude;
		
		jQuery.ajax(ajaxUrl, {
		    'data': data,
		    'type': 'POST'
		});
		
		loadRecommendedArtefacts(position.coords.latitude, position.coords.longitude);
}

function simpleRecommendation(error) {
	if(!recommendationinvoked){
		recommendationinvoked = true;
		switch(error.code) {
	        case error.PERMISSION_DENIED:
	            console.warn("User denied the request for Geolocation.");
	            break;
	        case error.POSITION_UNAVAILABLE:
	        	console.warn("Location information is unavailable.");
	            break;
	        case error.TIMEOUT:
	        	console.warn("The request to get user location timed out.");
	            break;
	        case error.UNKNOWN_ERROR:
	        	console.warn("An unknown error occurred.");
	            break;
	    }
		loadRecommendedArtefacts(null, null);
	}
}

/*save the filter value in the sessionStorage, so if I enter in a details and go back, the same filters are selected*/
function setFilterInStorage(){
	
	sessionStorage.removeItem("typesBB");
	sessionStorage.setItem("typesBB",  $('#cat1').is(":checked"));
	
	sessionStorage.removeItem("typesPSA");
	sessionStorage.setItem("typesPSA",  $('#cat2').is(":checked"));
	
	sessionStorage.removeItem("typesDataset");
	sessionStorage.setItem("typesDataset",  $('#cat3').is(":checked"));
	
	sessionStorage.removeItem("forYou");
	sessionStorage.setItem("forYou",  $('#forYou').is(":checked"));

	sessionStorage.removeItem("pilotBilbao");
	sessionStorage.setItem("pilotBilbao",  $('#esPilot').is(":checked"));
	
	sessionStorage.removeItem("pilotUusimaa");
	sessionStorage.setItem("pilotUusimaa",  $('#fiPilot').is(":checked"));
	
	sessionStorage.removeItem("pilotNovisad");
	sessionStorage.setItem("pilotNovisad",  $('#srPilot').is(":checked"));
	
	sessionStorage.removeItem("pilotTrento");
	sessionStorage.setItem("pilotTrento",  $('#itPilot').is(":checked"));
}


/*check the filter value in the sessionStorage, so if I enter in a details and go back, the same filters are selected*/
function checkFilterInStorage(){

	if (sessionStorage.getItem('typesBB')){//check the first for all
		
		var pilotsSeStor = sessionStorage.getItem('pilots');
		 
		if (sessionStorage.getItem('typesBB')=="true"){
			 $('#cat1').prop('checked', true);
		}else if (sessionStorage.getItem('typesBB')=="false"){
			$('#cat1').prop('checked', false);
		}
		
		if (sessionStorage.getItem('typesPSA')=="true"){
			 $('#cat2').prop('checked', true);
		}else if (sessionStorage.getItem('typesPSA')=="false"){
			$('#cat2').prop('checked', false);
		}
		
		if (sessionStorage.getItem('typesDataset')=="true"){
			 $('#cat3').prop('checked', true);
		}else if (sessionStorage.getItem('typesDataset')=="false"){
			$('#cat3').prop('checked', false);
		}
		
		if (sessionStorage.getItem('forYou')=="true"){
			 $('#forYou').prop('checked', true);
		}else if (sessionStorage.getItem('forYou')=="false"){
			$('#forYou').prop('checked', false);
		}
		
		if (sessionStorage.getItem('pilotBilbao')=="true"){
			 $('#esPilot').prop('checked', true);
		}else if (sessionStorage.getItem('pilotBilbao')=="false"){
			 $('#esPilot').prop('checked', false);
		}
		
		if (sessionStorage.getItem('pilotUusimaa')=="true"){
			 $('#fiPilot').prop('checked', true);
		}else if (sessionStorage.getItem('pilotUusimaa')=="false"){
			 $('#fiPilot').prop('checked', false);
		}
		
		if (sessionStorage.getItem('pilotNovisad')=="true"){
			 $('#srPilot').prop('checked', true);
		}else if (sessionStorage.getItem('pilotNovisad')=="false"){
			 $('#srPilot').prop('checked', false);
		}
		
		if (sessionStorage.getItem('pilotTrento')=="true"){
			 $('#itPilot').prop('checked', true);
		}else if (sessionStorage.getItem('pilotTrento')=="false"){
			 $('#itPilot').prop('checked', false);
		}
	}/*if it's false, the it's the first time on the page */
	
}

function selectAll(formselector){
	$(formselector).find(':input').each(function(e){	
		if(!$(this).prop('disabled')){
			$( this ).prop('checked', true).change();
		}
	});
}


function deselectAll (formselector){
	$(formselector).find(':input').each(function(e){	
		if(!$(this).prop('disabled')){  
			  $( this ).prop('checked', false).change();
			}
		});
}




